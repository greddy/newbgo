#ifndef _pixel_ConfigurationInfoSpaceHandlerTKFEC_h_
#define _pixel_ConfigurationInfoSpaceHandlerTKFEC_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>
#include "pixel/utils/ConfigurationInfoSpaceHandler.h"
#include "pixel/utils/WebServer.h"

#include "xdaq/Application.h"

namespace xdaq {
  class Application;
}

namespace pixel {
  namespace utils {
    class Monitor;
    class WebServer;
  }
}

namespace pixel {
    class ConfigurationInfoSpaceHandlerTKFEC : public pixel::utils::ConfigurationInfoSpaceHandler
    {

    public:
      ConfigurationInfoSpaceHandlerTKFEC(xdaq::Application &xdaqApp, std::string const &name = "pixel-application-config");
      virtual ~ConfigurationInfoSpaceHandlerTKFEC();

    protected:
      virtual void registerItemSetsWithMonitor(pixel::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(pixel::utils::WebServer &webServer, pixel::utils::Monitor &monitor, std::string const &forceTabName);
      virtual std::string formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };
} // namespace pixel
#endif // _pixel_PixelTKFECSupervisor_ConfigurationInfoSpaceHandlerTKFEC_h_
