#ifndef _pixel_FPixDCDCSummaryHandler_h_
#define _pixel_FPixDCDCSummaryHandler_h_


#include <string>

#include "pixel/utils/InfoSpaceHandler.h"

namespace pixel {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace xdaq {
  class Application;
}

namespace pixel {
    class FPixDCDCSummaryHandler : public pixel::utils::InfoSpaceHandler
    {

    public:
      FPixDCDCSummaryHandler(xdaq::Application& xdaqApp, pixel::utils::InfoSpaceUpdater* updater);
      virtual ~FPixDCDCSummaryHandler();
      const uint MAXPG = 208;
      const uint MAXDCU = 96;
      const uint MAXDDR = 64;
      std::vector<std::string> itemSetNameVec_dcdc;
      std::vector<std::string> itemSetNameVec_ccu;
      std::vector<std::string> itemSetNameVec_dcu;
      
      std::vector<std::string> tabs_dcdc;
      std::vector<std::string> tabs_ccu;
      std::vector<std::string> tabs_dcu;
      virtual void displayMessage(pixel::utils::Monitor& monitor,std::string itemSetName,std::vector<std::string> UpdateMessage, uint counter);  
    protected:
      virtual void registerItemSetsWithMonitor(pixel::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
                                                 pixel::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };
} // namespace pixel

#endif // _pixel_FPixDCDCSummaryHandler_h_
