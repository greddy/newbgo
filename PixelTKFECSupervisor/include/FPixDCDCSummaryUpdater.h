#ifndef _pixel_FPixDCDCSummaryUpdater_h_
#define _pixel_FPixDCDCSummaryUpdater_h_

#include "pixel/utils/HwInfoSpaceUpdaterBase.h"

namespace pixel {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace pixel {

    class TCADeviceTKFEC;

    class FPixDCDCSummaryUpdater : public pixel::utils::HwInfoSpaceUpdaterBase
    {

    public:
      FPixDCDCSummaryUpdater(pixel::utils::XDAQAppBase& xdaqApp, TCADeviceTKFEC const& hw);

      virtual bool updateInfoSpaceItem(pixel::utils::InfoSpaceItem& item,
                                       pixel::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      TCADeviceTKFEC const& getHw() const;
      TCADeviceTKFEC const& hw_;

    };
} // namespace pixel

#endif // _pixel_FPixDCDCSummaryUpdater_h_
