#ifndef _pixel_TCADeviceTKFEC_h_
#define _pixel_TCADeviceTKFEC_h_
//#ifndef _pixel_PixelTKFECSupervisor_TCADeviceTKFEC_h_
//#define _pixel_PixelTKFECSupervisor_TCADeviceTKFEC_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>
#include "pixel/hwlayertca/TCADeviceBase.h"
#include "pixel/hwlayertca/TCACarrierBase.h"
#include "pixel/hwlayertca/TCACarrierFC7.h"

namespace pixel {
  //namespace PixelTKFECSupervisor {
    class TCADeviceTKFEC : public pixel::hwlayertca::TCADeviceBase
    {

      public:
        TCADeviceTKFEC();
        virtual ~TCADeviceTKFEC();

        //unsigned short ipmNumber() const;
        //void setIPMNumber(unsigned short const iciNumber);

        //void configureTriggerAlignment() const;
        //void configureExternalTriggerSettings() const;


      protected:
        //virtual std::string regNamePrefixImpl() const;

        //virtual bool bootstrapDoneImpl() const;
        //virtual void runBootstrapImpl() const;

      private:
        //unsigned short ipmNumber_;

    };
  //} // namespace PixelTKFECSupervisor
} // namespace pixel

#endif // _pixel_TCADeviceTKFEC_h_
