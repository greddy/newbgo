#ifndef _pixel_PixelTKFECSupervisor_h_
#define _pixel_PixelTKFECSupervisor_h_

#include "pixel/utils/XDAQAppWithFSMPixelBase.h"
#include "pixel/utils/SOAPER.h"
#include "pixel/utils/SOAPMsgHistoryHandler.h"
#include "pixel/utils/LOGMsgHistoryHandler.h"

#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelSupervisorConfiguration/include/PixelTKFECSupervisorConfiguration.h"
#include "utils/include/pixel/utils/Timer.h"
#include "PixelUtilities/PixelDCSUtilities/include/PixelPowerMap4602.h"
#include "PixelCalibrations/include/PixelTKFECCalibrationBase.h"
#include "PixelUtilities/PixelTKFECDataTools/include/PortCardDCU.h"

//Browser stuff
#include "PixelTKFECSupervisor/include/ConfigurationInfoSpaceHandlerTKFEC.h"
#include "PixelTKFECSupervisor/include/HwStatusInfoSpaceHandler.h"
#include "PixelTKFECSupervisor/include/HwStatusInfoSpaceUpdater.h"
#include "PixelTKFECSupervisor/include/TCADeviceTKFEC.h"
#include "PixelTKFECSupervisor/include/FPixDCDCSummaryHandler.h"
#include "PixelTKFECSupervisor/include/FPixDCDCSummaryUpdater.h"

#include "pixel/utils/FSMStateSummaryHandler.h"

#include "pixel/utils/FSMStateSummaryUpdater.h"


#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/BSem.h"

namespace xdaq {
class ApplicationStub;
}

namespace pixel {
  namespace utils {
    class FSMStateSummaryHandler;
    class SOAPMsgHistoryHandler;
    class LOGMsgHistoryHandler;
  }
}

namespace pixel {
  class HwStatusInfoSpaceHandler;
  class HwStatusInfoSpaceUpdater;
  class FPixDCDCSummaryHandler;
  class FPixDCDCSummaryUpdater;
  class FSMStateSummaryUpdater;

  class PixelTKFECSupervisor : public pixel::utils::XDAQAppWithFSMPixelBase,
                               public PixelTKFECSupervisorConfiguration {

  public:
    XDAQ_INSTANTIATOR();

    PixelTKFECSupervisor(xdaq::ApplicationStub* stub);
    virtual ~PixelTKFECSupervisor();

    bool ReadDCU_workloop(toolbox::task::WorkLoop *w1);
    bool CheckDCDC_workloop(toolbox::task::WorkLoop *w1);
    void submit_workloops();
    void start_workloops();
    void stop_workloops();
    void remove_workloops();

    xoap::MessageReference Connection_Check(xoap::MessageReference msg);
    xoap::MessageReference Recover(xoap::MessageReference msg);

    virtual void initializeActionImpl(toolbox::Event::Reference event);
    virtual void configureActionImpl(toolbox::Event::Reference event);
    virtual void startActionImpl(toolbox::Event::Reference event);
    virtual void stopActionImpl(toolbox::Event::Reference event);
    virtual void pauseActionImpl(toolbox::Event::Reference event);
    virtual void resumeActionImpl(toolbox::Event::Reference event);
    virtual void haltActionImpl(toolbox::Event::Reference event);
    virtual void reconfigureActionImpl(toolbox::Event::Reference event);

    virtual void transitionHaltedToConfiguringActionImpl(toolbox::Event::Reference event);
    virtual void fixSoftErrorActionImpl(toolbox::Event::Reference event);
    virtual void resumeFromSoftErrorActionImpl(toolbox::Event::Reference event);

    std::vector<pixel::utils::FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const &commandName) const;

    xoap::MessageReference changeStateImpl(xoap::MessageReference msg);

    xoap::MessageReference PIAReset (xoap::MessageReference msg);

    // Calibration
    xoap::MessageReference TKFECCalibrations(xoap::MessageReference msg);
    xoap::MessageReference beginCalibration(xoap::MessageReference msg);
    xoap::MessageReference endCalibration(xoap::MessageReference msg);

    // Low Level Commands
    xoap::MessageReference SetDelay(xoap::MessageReference msg);
    xoap::MessageReference SetDelayEnMass(xoap::MessageReference msg);
    xoap::MessageReference SetAOHGainEnMass(xoap::MessageReference msg);
    xoap::MessageReference SetAOHBiasEnMass(xoap::MessageReference msg);
    xoap::MessageReference SetAOHBiasOneChannel(xoap::MessageReference msg);

  private:
    void createFecAccess(int argc, char **argv, int *cnt, unsigned int slot);
    void portcardI2CDevice(FecAccess *fecAccess, tscType8 fecAddress, tscType8 ringAddress, tscType8 ccuAddress, tscType8 channelAddress, tscType8 deviceAddress, enumDeviceType modeType, unsigned int value);
    int portcardI2CDeviceRead(FecAccess *fecAccess, tscType8 fecAddress, tscType8 ringAddress, tscType8 ccuAddress, tscType8 channelAddress, tscType8 deviceAddress, enumDeviceType modeType);
    void cleanupGlobalConfigData();
    bool DCDCCommand(int slot, int ring, int ccu, std::string dcdcname, int updatemode, int retries);
    bool programPortcards(bool checkReadBack = false, bool programFixSoftErrorSubset = false);
    bool SetAOHBias(std::string portCardName, unsigned int AOHNumber, unsigned int AOHBiasValue);
    bool powercycleDCDCs();
    void onOffPIAchannels(int slot,int ring,int ccu, bool enable);
    bool SetPortCardSetting(const pos::PixelPortCardConfig* portCardConfig, unsigned int deviceAddress, unsigned int settingValue);
    //void FPixDCDCSummary(xgi::Input* in, xgi::Output* out) throw (xgi::exception::Exception);
    bool readDCU(int retries);
    bool checkDCDC(int retries);
    std::pair<std::string, std::string> getDCDCstatus(unsigned int slot, unsigned int ring, unsigned int ccu, std::string dcdcname, PiaChannelAccess* piaChannelAccessPg, PiaChannelAccess* piaChannelAccessEn);
    void initialize_dcdc_dcu_tables();
    void disablePIAchannels(int slot, int ring, int ccu);
    void enablePIAchannels(int slot, int ring, int ccu);
    bool disableDCDCs_BPix_BmI_SEC7_LYR34();

    std::string runNumber_;
    std::string outputDir_;
    log4cplus::Logger &sv_logger_;
    //std::auto_ptr<pixel::utils::LoggerHistory> loggerHist_;
    bool extratimers_;
    pixel::utils::Timer GlobalTimer_;
    toolbox::BSem *hardwarelock_;
    bool suppressHardwareError_;

    std::set<std::string> fixPortcards_;
    std::set<std::string> fixModules_;
    std::unique_ptr<pixel::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
    std::unique_ptr<pixel::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;
    std::unique_ptr<pixel::FPixDCDCSummaryUpdater> FPixDCDCSummaryUpdaterP_;
    std::unique_ptr<pixel::FPixDCDCSummaryHandler> FPixDCDCSummaryP_;
    std::unique_ptr<pixel::FSMStateSummaryUpdater> FSMStateSummaryUpdaterP_;
    std::unique_ptr<pixel::utils::FSMStateSummaryHandler> FSMStateSummaryP_;
    std::unique_ptr<pixel::utils::SOAPMsgHistoryHandler> TKFECSOAPMsgHistoryP_;
    std::unique_ptr<pixel::utils::LOGMsgHistoryHandler> TKFECLOGMsgHistoryP_;

    bool setMessage(xoap::MessageReference const &msg, bool received);

    // Application Descriptors
    PixelTKFECCalibrationBase *theTKFECCalibrationBase_;
    const xdaq::ApplicationDescriptor *PixelDCStoTKFECDpInterface_;
    const xdaq::ApplicationDescriptor *PixelDCSFSMInterface_;
    const xdaq::ApplicationDescriptor *PixelSupervisor_;
    PixelPowerMap4602 powerMap_;

    // Worloop related variables
    bool doDCULoop_;
    bool doCCUCheckLoop_;
    bool doDCDCCheckLoop_;
    bool printReadBack_;                     //debug use only; prints written and readback data
    xdata::Integer readDCU_workloop_usleep_; // the number of microseconds to sleep ReadDCU_workloop
    xdata::Integer readDCU_workloop_sleep_;
    xdata::Integer checkCCU_workloop_sleep_;
    xdata::Integer checkDCDC_workloop_sleep_;

    toolbox::BSem *dculock_;
    toolbox::BSem *dcdclock_;
    bool workloopContinue_;
    bool workloopContinueCCU_;
    bool workloopContinueDCDC_;
    bool workloopContinueRC_;
    int timesCheckDCUcalled;
    int timesCheckCCUcalled;
    int timesCheckDCDCcalled;
    bool doIteerativeCCUreset_;
    bool doIteerativeCCUreset_forDCU_;

    std::vector<std::pair<std::string, unsigned int> > aliasesAndKeys_;

    // DCU Workloop
    toolbox::task::WorkLoop *workloop_;
    toolbox::task::ActionSignature *readDCU_;

    // DCDC Check Workloop
    toolbox::task::WorkLoop *dcdc_workloop_;
    toolbox::task::ActionSignature *checkDCDC_;
    std::map< std::pair<std::string, std::string>, boost::tuple<unsigned int, unsigned int, unsigned int> > dcdc_list;
    std::vector< boost::tuple<unsigned int, unsigned int, unsigned int> > ccu_list;

  protected:
    void setupInfoSpaces();

    virtual TCADeviceTKFEC& getHw() const;

    void hwConnectImpl();
    void hwReleaseImpl();
};

} // namespace pixel

#endif // _pixel_PixelTKFECSupervisor_
