#ifndef _pixel_HwStatusInfoSpaceUpdater_h_
#define _pixel_HwStatusInfoSpaceUpdater_h_

#include "pixel/utils/HwInfoSpaceUpdaterBase.h"

namespace pixel {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace pixel {
    class TCADeviceTKFEC;

    class HwStatusInfoSpaceUpdater : public pixel::utils::HwInfoSpaceUpdaterBase
    {

    public:
      HwStatusInfoSpaceUpdater(pixel::utils::XDAQAppBase& xdaqApp,
                               TCADeviceTKFEC const& hw);
      virtual ~HwStatusInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(pixel::utils::InfoSpaceItem& item,
                                       pixel::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      TCADeviceTKFEC const& getHw() const;

      TCADeviceTKFEC const& hw_;

    };
} // namespace pixel

#endif // _pixel_HwStatusInfoSpaceUpdater_h_
