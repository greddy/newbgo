#ifndef _pixel_HwStatusInfoSpaceHandler_h_
#define _pixel_HwStatusInfoSpaceHandler_h_

#include <string>

#include "pixel/utils/InfoSpaceHandler.h"

namespace pixel {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace xdaq {
  class Application;
}

namespace pixel {
    class HwStatusInfoSpaceHandler : public pixel::utils::InfoSpaceHandler
    {

    public:
      HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp, pixel::utils::InfoSpaceUpdater* updater);
      virtual ~HwStatusInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(pixel::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
                                                 pixel::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };
} // namespace pixel

#endif // _pixel_PixelTKFECSupervisor_HwStatusInfoSpaceHandler_h_
