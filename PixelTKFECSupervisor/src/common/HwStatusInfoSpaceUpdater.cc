#include "PixelTKFECSupervisor/include/HwStatusInfoSpaceUpdater.h"
#include "PixelTKFECSupervisor/include/TCADeviceTKFEC.h"
#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceItem.h"

#include <string>

pixel::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(pixel::utils::XDAQAppBase& xdaqApp, pixel::TCADeviceTKFEC const& hw) :
  pixel::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  hw_(hw) {
}

pixel::HwStatusInfoSpaceUpdater::~HwStatusInfoSpaceUpdater() {
}

bool pixel::HwStatusInfoSpaceUpdater::updateInfoSpaceItem(pixel::utils::InfoSpaceItem& item,
                                                          pixel::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  //pixel::PixelTKFECSupervisor::TCADeviceTKFEC const& hw = getHw();
  pixel::TCADeviceTKFEC const& hw = getHw();
  //if (hw.isReadyForUse()) {
  std::string name = item.name();
  pixel::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
  if (updateType == pixel::utils::InfoSpaceItem::PROCESS) {
      // The 'PROCESS' update type means that there is something
      // special to the variable. Figure out what to do based on the
      // variable name.
    if (name == "ttc_clock_up") {
      bool const newVal = hw.isTTCClockUp();
      infoSpaceHandler->setBool(name, newVal);
      updated = true;
    }
    else if (name == "ttc_clock_stable") {
      bool const newVal = hw.isTTCClockStable();
      infoSpaceHandler->setBool(name, newVal);
      updated = true;
    }
    else if (name == "pm_state") {
      // uint32_t newVal = hw.getPMState(); //Doesn't exist yet
      // infoSpaceHandler->setUInt32(name, newVal);
      // updated = true;
    }
  }
  if (!updated) {
    updated = pixel::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
  }
  if (updated) {
    item.setValid();
  }
  else {
    item.setInvalid();
  }
  return updated;
}

//pixel::PixelTKFECSupervisor::TCADeviceTKFEC const&
//pixel::PixelTKFECSupervisor::HwStatusInfoSpaceUpdater::getHw() const {

pixel::TCADeviceTKFEC const& pixel::HwStatusInfoSpaceUpdater::getHw() const {
  return hw_;
}
