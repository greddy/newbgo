#include "PixelTKFECSupervisor/include/TCADeviceTKFEC.h"

//pixel::PixelTKFECSupervisor::TCADeviceTKFEC::TCADeviceTKFEC()
pixel::TCADeviceTKFEC::TCADeviceTKFEC()
    :pixel::hwlayertca::TCADeviceBase(std::unique_ptr<pixel::hwlayertca::TCACarrierBase>(new pixel::hwlayertca::TCACarrierFC7(hwDevice_))){
}

pixel::TCADeviceTKFEC::~TCADeviceTKFEC(){
//pixel::PixelTKFECSupervisor::TCADeviceTKFEC::~TCADeviceTKFEC(){
}

// unsigned short pixel::PixelTKFECSupervisor::TCADeviceTKFEC::ipmNumber() const {
//   return ipmNumber_;
// }

// void pixel::PixelTKFECSupervisor::TCADeviceTKFEC::setIPMNumber(unsigned short const ipmNumber) {
//   ipmNumber_ = ipmNumber;
// }

// std::string pixel::PixelTKFECSupervisor::TCADeviceTKFEC::regNamePrefixImpl() const {
//   // All the iPM registers start with 'ipmX.', where 'X' is the iPM
//   // number in the LPM (i.e., [1, 2]).
//   std::stringstream regNamePrefix;
//   regNamePrefix << "ipm" << ipmNumber() << ".";
//   return regNamePrefix.str();
// }

// bool pixel::PixelTKFECSupervisor::TCADeviceTKFEC::bootstrapDoneImpl() const {
//   pixel::hwlayertca::BootstrapHelper h(*this);
//   return h.bootstrapDone();
// }

// void pixel::PixelTKFECSupervisor::TCADeviceTKFEC::runBootstrapImpl() const {
//   pixel::hwlayertca::BootstrapHelper h(*this);
//   h.runBootstrap();
//   // Reset the backplane link to the CPM.
//   hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
//   hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x1);
//   hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
// }
//
// void pixel::PixelTKFECSupervisor::TCADeviceTKFEC::configureTriggerAlignment() const {
//   // This one is not really trigger alignment, but it aligns the BX
//   // number reported for each trigger in the DAQ event record.
//   std::string regName = "main.orbit_alignment_config.daq_record_adjust";
//   writeRegister(regName, 0xd6e);
//
//   //----------
//
//   // BX-alignment for cyclic triggers.
//   regName = "main.int_fw_alignment_settings.cyclic_trigger_adjust";
//   writeRegister(regName, 0x7a);
//
//   // BX-alignment for the bunch-mask.
//   regName = "main.int_fw_alignment_settings.bunch_mask_adjust";
//   writeRegister(regName, 0x7b);
//
//   // BX-alignment for the sequence triggers.
//   regName = "bgo_trains.bgo_train_config.calib_trigger_adjust";
//   writeRegister(regName, 0x7b);
// }

// void pixel::PixelTKFECSupervisor::TCADeviceTKFEC::configureExternalTriggerSettings() const {
// }
