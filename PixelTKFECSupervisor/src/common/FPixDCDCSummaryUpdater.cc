#include "PixelTKFECSupervisor/include/FPixDCDCSummaryUpdater.h"

#include <string>

#include "PixelTKFECSupervisor/include/TCADeviceTKFEC.h"
#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/Utils.h"

pixel::FPixDCDCSummaryUpdater::FPixDCDCSummaryUpdater(pixel::utils::XDAQAppBase& xdaqApp, TCADeviceTKFEC const& hw) :
  pixel::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw),
  hw_(hw) {
}

bool pixel::FPixDCDCSummaryUpdater::updateInfoSpaceItem(pixel::utils::InfoSpaceItem& item,
                                                        pixel::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  TCADeviceTKFEC const& hw = getHw();
  bool updated = false;
  if (hw.isHwConnected()) {
    //std::string name = item.name();
    //pixel::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
  }
  if (!updated) {
    updated = pixel::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
  }
  if (updated) {
    item.setValid();
  }
  else {
    item.setInvalid();
  }

  return updated;
}

pixel::TCADeviceTKFEC const& pixel::FPixDCDCSummaryUpdater::getHw() const {
  return hw_;
}
