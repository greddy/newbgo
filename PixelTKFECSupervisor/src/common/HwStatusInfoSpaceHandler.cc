#include "PixelTKFECSupervisor/include/HwStatusInfoSpaceHandler.h"

#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/WebServer.h"

pixel::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp, pixel::utils::InfoSpaceUpdater* updater) :
InfoSpaceHandler(xdaqApp, "pixel-hw-status-tkfec", updater) //This will probably need to be changed
{
  // Hardware status. (keeping these the same for now)
  createBool("ttc_clock_stable", false, "true/false", pixel::utils::InfoSpaceItem::PROCESS);
  createBool("ttc_clock_up", false, "true/false", pixel::utils::InfoSpaceItem::PROCESS);
  createUInt32("main.bc_alignment_status.orbit_marker_absent_count");
  createUInt32("main.bc_alignment_status.bc_counter_realignment_count");
  createUInt32("main.bc_alignment_status.system_bc_counter_realignment_count");

  // The overall partition controller state (in the firmware).
  createUInt32("pm_state", 0, "", pixel::utils::InfoSpaceItem::PROCESS);
}

pixel::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler() {
}

void pixel::HwStatusInfoSpaceHandler::registerItemSetsWithMonitor(pixel::utils::Monitor& monitor) {

  // Hardware status.
  std::string itemSetName = "itemset-hw-status";
  monitor.newItemSet(itemSetName);

  // Clock monitoring.
  //monitor.addItem(itemSetName, "ttc_clock_stable", "TTC clock stable", this);
  //monitor.addItem(itemSetName, "ttc_clock_up", "TTC clock PLL locked", this);

  // Orbit monitoring.
  //monitor.addItem(itemSetName, "main.bc_alignment_status.orbit_marker_absent_count", "Orbit (main and system) realignment count - orbit marker absent", this);
  //monitor.addItem(itemSetName, "main.bc_alignment_status.bc_counter_realignment_count", "Main orbit realignment count - orbit marker too early/late", this);
  //monitor.addItem(itemSetName, "main.bc_alignment_status.system_bc_counter_realignment_count", "System orbit realignment count - orbit marker too early/late", this);

  // Partition manager (firmware) status.
  //itemSetName = "itemset-pm-state";
  //monitor.newItemSet(itemSetName);
  //monitor.addItem(itemSetName, "pm_state", "Running state", this);
}

void pixel::HwStatusInfoSpaceHandler::registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
                                                                    pixel::utils::Monitor& monitor,
                                                                    std::string const& forceTabName)
  {
    std::string const tabName = forceTabName.empty() ? "Hardware status" : forceTabName;
    // webServer.registerTab(tabName, "Hardware information", 3);
    //webServer.registerTable("Hardware status", "Hardware status info", monitor, "itemset-hw-status", tabName);
    //webServer.registerTable("Running state", "Firmware PM running state", monitor, "itemset-pm-state", tabName);
  }

  std::string pixel::HwStatusInfoSpaceHandler::formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const {
    std::string res = pixel::utils::escapeAsJSONString(pixel::utils::InfoSpaceHandler::kInvalidItemString);
    if (item->isValid()) {
      std::string name = item->name();
      // For everything else simply call the generic formatter.
      res = InfoSpaceHandler::formatItem(item);
    }
    return res;
  }
