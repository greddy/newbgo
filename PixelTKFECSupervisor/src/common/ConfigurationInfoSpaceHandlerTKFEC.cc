#include "PixelTKFECSupervisor/include/ConfigurationInfoSpaceHandlerTKFEC.h"

#include <stddef.h>
#include <string>

#include "toolbox/string.h"

#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/WebServer.h"



//pixel::PixelTKFECSupervisor::ConfigurationInfoSpaceHandlerTKFEC::ConfigurationInfoSpaceHandlerTKFEC(xdaq::Application &xdaqApp, std::string const &name) :
pixel::ConfigurationInfoSpaceHandlerTKFEC::ConfigurationInfoSpaceHandlerTKFEC(xdaq::Application &xdaqApp, std::string const &name) :
pixel::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
  createUInt32("ipmNumber", 1, "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createUInt32("fedId", 0, "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createString("fedEnableMask", "emptyTestString", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  

  // Configurable bools
  createBool("get_pgbit_status", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("doDCDCCheckLoop", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createUInt32("checkDCDC_workloop_sleep", 1000000, "", pixel::utils::InfoSpaceItem::NOUPDATE, true); //in microseconds
  createBool("workloopContinueDCDC", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("doIteerativeCCUreset", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("doIteerativeCCUreset_forDCU", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  

  createBool("doDCULoop", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("printReadBack", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("workloopContinue", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("debugBool", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("skip_lv_check", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("do_force_ccu_readout", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("resume_do_portcard_recovery", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("ser_do_portcard_recovery", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("ser_do_powercycle_dcdc", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createBool("extraTimers", false, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  createUInt32("readDCU_workloop_sleep", 1000000, "", pixel::utils::InfoSpaceItem::NOUPDATE, true); //in microseconds
}

pixel::ConfigurationInfoSpaceHandlerTKFEC::~ConfigurationInfoSpaceHandlerTKFEC() { }

void pixel::ConfigurationInfoSpaceHandlerTKFEC::registerItemSetsWithMonitor(pixel::utils::Monitor& monitor) {
  pixel::utils::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(monitor);

  monitor.newItemSet("tkfec-config-bools");
  monitor.addItem("tkfec-config-bools", "get_pgbit_status", "Get PG status", this);
  monitor.addItem("tkfec-config-bools", "doDCDCCheckLoop", "Check DCDC Loop", this);
  monitor.addItem("tkfec-config-bools", "checkDCDC_workloop_sleep", "Check DCDC Workloop Sleep", this);
  monitor.addItem("tkfec-config-bools", "workloopContinueDCDC", "Continue DCDC Workloop", this);
  monitor.addItem("tkfec-config-bools", "doIteerativeCCUreset", "Iterative CCU reset", this);
  monitor.addItem("tkfec-config-bools", "doIteerativeCCUreset_forDCU", "Iterative CCU reset for DCU", this);


  monitor.addItem("tkfec-config-bools", "doDCULoop", "DCU Loop", this);
  monitor.addItem("tkfec-config-bools", "printReadBack", "Print Readback", this);


  monitor.addItem("tkfec-config-bools", "workloopContinue", "Continue Workloop", this);
  monitor.addItem("tkfec-config-bools", "debugBool", "Debugging bool", this);
  monitor.addItem("tkfec-config-bools", "skip_lv_check", "Ignore Detector Response", this);
  monitor.addItem("tkfec-config-bools", "do_force_ccu_readout", "Force CCU Readout", this);
  monitor.addItem("tkfec-config-bools", "resume_do_portcard_recovery", "Resume Portcard Recovery", this);
  monitor.addItem("tkfec-config-bools", "ser_do_portcard_recovery", "SER Do Portcard Recovery", this);
  monitor.addItem("tkfec-config-bools", "ser_do_powercycle_dcdc", "SER Do Powercycle DCDC", this);
  monitor.addItem("tkfec-config-bools", "extraTimers", "Extra timers", this);
  monitor.addItem("tkfec-config-bools", "readDCU_workloop_sleep", "Read DCU Workloop sleep", this);

}

void
pixel::ConfigurationInfoSpaceHandlerTKFEC::registerItemSetsWithWebServer(pixel::utils::WebServer &webServer,
                                                                         pixel::utils::Monitor &monitor,
                                                                         std::string const &forceTabName) {

  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;
  webServer.registerTab(tabName, "TKFEC configuration",   2);
  pixel::utils::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(webServer, monitor, tabName);
  webServer.registerTable("TK FEC Configurable Options", "", monitor, "tkfec-config-bools", tabName);
}

std::string pixel::ConfigurationInfoSpaceHandlerTKFEC::formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const {
  std::string res = pixel::utils::escapeAsJSONString(pixel::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid()) {
    // std::string name = item->name();
    // if (name == "fedEnableMask") {
    //   // This thing can become annoyingly long. The idea here is
    //   // to insert some zero-width spaces in order to make it
    //   // break into pieces when formatted in the web interface.
    //   // NOTE: Not very efficient/pretty, but it works.
    //   std::string const valRaw = getString(name);
    //   std::string const valNew = pixel::utils::chunkifyFEDVector(valRaw);
    //   res = pixel::utils::escapeAsJSONString(valNew);
    // }
    // else {
      // For everything else simply call the generic formatter.
      res = InfoSpaceHandler::formatItem(item);
    // }
  }
  return res;
}
