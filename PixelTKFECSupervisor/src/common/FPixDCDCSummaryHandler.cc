#include "PixelTKFECSupervisor/include/FPixDCDCSummaryHandler.h"

#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/WebServer.h"

#include "toolbox/string.h"



pixel::FPixDCDCSummaryHandler::FPixDCDCSummaryHandler(xdaq::Application& xdaqApp, pixel::utils::InfoSpaceUpdater* updater) :
InfoSpaceHandler(xdaqApp, "pixel-dcdc-summary-tkfec", updater) //This will probably need to be changed
{
  // createBool("test_bool", true, "true/false", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  // createDouble("test_double", 17.5, "", pixel::utils::InfoSpaceItem::NOUPDATE, true);

  itemSetNameVec_dcdc = {"itemset-log-dcdc"};
  itemSetNameVec_ccu = {"itemset-log-ccu"};
  itemSetNameVec_dcu = {"itemset-log-dcu"};

  //Add all the tabs I need
  tabs_dcdc = {"PG_name","dcdc","EN_status","PG_status"};
  tabs_ccu = {"Ring","CCU","pia_register","ddr","data"};
  tabs_dcu = {"portcard","vaa","vdd","rtd2","rtd3","aoh","vpc","vbg","ts"};
  
  for(std::vector<std::string> :: iterator it = tabs_dcdc.begin(); it != tabs_dcdc.end(); ++it){
    createString("header-"+*it, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  }
  for(std::vector<std::string> :: iterator it = tabs_ccu.begin(); it != tabs_ccu.end(); ++it){
    createString("header-"+*it, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  }
  for(std::vector<std::string> :: iterator it = tabs_dcu.begin(); it != tabs_dcu.end(); ++it){
    createString("header-"+*it, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
  }

  

  // Adding rows

  for (size_t i=1; i<=MAXPG; i++){
    const std::string index = pixel::utils::to_string(i);
    itemSetNameVec_dcdc.push_back("itemset-log-dcdc"+index);
    for(std::vector<std::string> :: iterator it = tabs_dcdc.begin(); it != tabs_dcdc.end(); ++it){
      createString("log-"+*it+"_msg" +index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    }    
  }
  for (size_t i=1; i<=MAXDDR; i++){
    const std::string index = pixel::utils::to_string(i);
    itemSetNameVec_ccu.push_back("itemset-log-ccu"+index);
    for(std::vector<std::string> :: iterator it = tabs_ccu.begin(); it != tabs_ccu.end(); ++it){
      createString("log-"+*it+"_msg" +index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    }
  }
  for (size_t i=1; i<=MAXDCU; i++){
    const std::string index = pixel::utils::to_string(i);
    itemSetNameVec_dcu.push_back("itemset-log-dcu"+index);
    for(std::vector<std::string> :: iterator it = tabs_dcu.begin(); it != tabs_dcu.end(); ++it){
      createString("log-"+*it+"_msg" +index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    }
  }
  
  
}

pixel::FPixDCDCSummaryHandler::~FPixDCDCSummaryHandler() {
}

void pixel::FPixDCDCSummaryHandler::registerItemSetsWithMonitor(pixel::utils::Monitor& monitor) {

  // Hardware status.
  //std::string itemSetNameDCDC = "itemset-dcdc-summary";

  monitor.newItemSet(itemSetNameVec_dcdc.at(0));
  monitor.newItemSet(itemSetNameVec_dcu.at(0));
  monitor.newItemSet(itemSetNameVec_ccu.at(0));
  
  for(std::vector<std::string> :: iterator it = tabs_dcdc.begin(); it != tabs_dcdc.end(); ++it){
    monitor.addItem(itemSetNameVec_dcdc.at(0), "header-"+*it, *it,   this);
  }
  for(std::vector<std::string> :: iterator it = tabs_dcu.begin(); it != tabs_dcu.end(); ++it){
    monitor.addItem(itemSetNameVec_dcu.at(0), "header-"+*it, *it,   this);
  }
  for(std::vector<std::string> :: iterator it = tabs_ccu.begin(); it != tabs_ccu.end(); ++it){
    monitor.addItem(itemSetNameVec_ccu.at(0), "header-"+*it, *it,   this);
  }

  for (size_t i=1; i<=MAXPG; i++){
    const std::string index = pixel::utils::to_string(i);
    monitor.newItemSet(itemSetNameVec_dcdc.at(i));
    for(std::vector<std::string> :: iterator it = tabs_dcdc.begin(); it != tabs_dcdc.end(); ++it){
      monitor.addItem(itemSetNameVec_dcdc.at(i), "log-"+*it+"_msg" +index, "", this);
    }
  }
  
  for (size_t i=1; i<=MAXDDR; i++){
    const std::string index = pixel::utils::to_string(i);
    monitor.newItemSet(itemSetNameVec_ccu.at(i));
    for(std::vector<std::string> :: iterator it = tabs_ccu.begin(); it != tabs_ccu.end(); ++it){
      monitor.addItem(itemSetNameVec_ccu.at(i), "log-"+*it+"_msg" +index, "", this);
    }
  }

  for (size_t i=1; i<=MAXDCU; i++){
    const std::string index = pixel::utils::to_string(i);
    monitor.newItemSet(itemSetNameVec_dcu.at(i));
    for(std::vector<std::string> :: iterator it = tabs_dcu.begin(); it != tabs_dcu.end(); ++it){
      monitor.addItem(itemSetNameVec_dcu.at(i), "log-"+*it+"_msg" +index, "", this);
    }
  }
    
}

void pixel::FPixDCDCSummaryHandler::registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
  pixel::utils::Monitor& monitor,
  std::string const& forceTabName)
  {
    std::string const tabName = forceTabName.empty() ? "DCDC Summary" : forceTabName;

    webServer.registerTab(tabName, "", 2);
    webServer.registerTab(tabName, "FPix DCDC Summary", 3);
    webServer.registerMultiColTable("DCDC summary", "", monitor, itemSetNameVec_dcdc, tabName);
    webServer.registerMultiColTable("CCU and DDR  summary", "", monitor,itemSetNameVec_ccu, tabName);
    webServer.registerMultiColTable("DCU summary", "", monitor,itemSetNameVec_dcu, tabName);


  }

  std::string pixel::FPixDCDCSummaryHandler::formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const {
    std::string res = pixel::utils::escapeAsJSONString(pixel::utils::InfoSpaceHandler::kInvalidItemString);
    if (item->isValid()) {
      std::string name = item->name();
      if (name == "pm_state") {
        uint32_t const value = getUInt32(name);
        //tcds::definitions::PM_STATE const valueEnum = static_cast<tcds::definitions::PM_STATE>(value);
        //res = tcds::utils::escapeAsJSONString(tcds::pm::pmStateToString(valueEnum));
      }
      else {
        // For everything else simply call the generic formatter.
        res = InfoSpaceHandler::formatItem(item);
      }
    }
    return res;
  }

void
pixel::FPixDCDCSummaryHandler::displayMessage(pixel::utils::Monitor& monitor,std::string itemSetName, std::vector<std::string> UpdateMessage, uint counter) {

  std::vector<std::string> tab_set;

  if (itemSetName == "itemset-log-dcdc")
    tab_set = tabs_dcdc;
  else if (itemSetName == "itemset-log-ccu")
    tab_set = tabs_ccu;
  else if (itemSetName == "itemset-log-dcu")
    tab_set = tabs_dcu;
  else 
    std::cout << "ERROR: Itemset not valid" << std::endl;

  const std::string index = pixel::utils::to_string(counter);
  uint tab_count = 0;
  for(std::vector<std::string> :: iterator it = tab_set.begin(); it != tab_set.end(); ++it){
    setString("log-"+*it+"_msg" +index,UpdateMessage.at(tab_count));
    tab_count++;
  }
}
