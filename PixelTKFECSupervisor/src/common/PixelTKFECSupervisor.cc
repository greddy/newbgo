#include "PixelTKFECSupervisor/include/PixelTKFECSupervisor.h"

#include "PixelTKFECSupervisor/include/TCADeviceTKFEC.h"
#include "PixelTKFECSupervisor/include/ConfigurationInfoSpaceHandlerTKFEC.h"
#include "PixelSupervisorConfiguration/include/PixelTKFECSupervisorConfiguration.h"
#include "PixelCalibrations/include/PixelTKFECCalibrationBase.h"
#include "PixelCalibrations/include/PixelCalibrationFactory.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelTKFECSupervisor/include/exception/Exception.h"
#include "PixelUtilities/PixelTKFECDataTools/include/PortCardDCU.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortCardSettingNames.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/exception/Handler.h"
#include "pixel/utils/LogMacros.h"
#include "pixel/utils/Utils.h"


const bool debug = false;                 // some additinal debuging messages
const bool skip_lv_check = false;               // skip LV check flag
const bool do_force_ccu_readout = false; // force reading of CCU for each workloop (also force reset if enabled)
const bool ser_do_portcard_recovery = true; // reprogram portcards for ser recovery
const bool ser_do_powercycle_dcdc = false;  // powercycle dcdc
const bool resume_do_portcard_recovery = false; // reprogram portcards for resume
const bool get_pgbit_status = true; // Get PG Bit status in DCDC workloop
bool short_sleep = false;

using namespace std;
using namespace pos;



XDAQ_INSTANTIATOR_IMPL(pixel::PixelTKFECSupervisor)

pixel::PixelTKFECSupervisor::PixelTKFECSupervisor(xdaq::ApplicationStub *const stub)
    : pixel::utils::XDAQAppWithFSMPixelBase(stub, std::unique_ptr<pixel::hwlayer::DeviceBase>(new pixel::TCADeviceTKFEC())),
    PixelTKFECSupervisorConfiguration(&runNumber_, &outputDir_),
    sv_logger_(getApplicationLogger()),
    hwStatusInfoSpaceUpdaterP_(nullptr),
    hwStatusInfoSpaceP_(nullptr),
    FPixDCDCSummaryUpdaterP_(nullptr),
    FPixDCDCSummaryP_(nullptr),
    FSMStateSummaryUpdaterP_(nullptr),
    FSMStateSummaryP_(nullptr),
    TKFECSOAPMsgHistoryP_(nullptr),
    TKFECLOGMsgHistoryP_(nullptr)
  {
      // Create the InfoSpace holding all configuration information.
      cfgInfoSpaceP_ = std::unique_ptr<pixel::ConfigurationInfoSpaceHandlerTKFEC>(new pixel::ConfigurationInfoSpaceHandlerTKFEC(*this));
      crate_ = this->getApplicationDescriptor()->getInstance();

      //make sure the configurations are initialised
      theGlobalKey_ = 0;
      theTKFECConfiguration_ = 0;
      theNameTranslation_ = 0;
      theFECConfiguration_ = 0;
      fecAccess_ = 0;
      thePortcardMap_ = 0;
      theCalibObject_ = 0;
      theGlobalDelay25_ = 0;
      extratimers_ = true;
      GlobalTimer_.setName("PixelTKFECSupervisor");
      GlobalTimer_.setVerbose(true);
      suppressHardwareError_ = false;
      hardwarelock_ = new toolbox::BSem(toolbox::BSem::FULL, true);

      // Initializing Application Descriptors
      PixelDCStoTKFECDpInterface_ = 0;
      PixelDCSFSMInterface_ = 0;

      // Initialize workloop related variables
      readDCU_workloop_usleep_ = 1000000;
      readDCU_workloop_sleep_ = 60;
      checkCCU_workloop_sleep_ = 60;
      checkDCDC_workloop_sleep_ = 600; //The following was modified by NDS on 08/08/22 08AUG22: changed from 60.
      dculock_ = new toolbox::BSem(toolbox::BSem::FULL, true);
      dcdclock_ = new toolbox::BSem(toolbox::BSem::FULL, true);
      workloopContinue_ = false;
      workloopContinueDCDC_ = false; //The following was modified by NDS on 08/10/22: changed from true.
      timesCheckDCUcalled = 0;
      timesCheckDCDCcalled = 0;
      doIteerativeCCUreset_ = true;
      doIteerativeCCUreset_forDCU_ = true;


      //Initialize the DCU Workloop

      printReadBack_ = false; //set to true to dump programmed & readback data
      doDCULoop_ = false;     // Use the DCU readout loop
      if (doDCULoop_) {
        workloop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop("PixelTKFECSupervisorWorkLoop", "waiting");
        readDCU_ = toolbox::task::bind(this, &PixelTKFECSupervisor::ReadDCU_workloop, "ReadDCU_workloop");
      }

      // Check DCDC status
      doDCDCCheckLoop_ = false; //The following was modified by NDS on 08/10/22: changed from true.
      if (doDCDCCheckLoop_) {
        dcdc_workloop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop("PixelTKFECSupervisorWorkLoop", "waiting");
        checkDCDC_ = toolbox::task::bind(this, &PixelTKFECSupervisor::CheckDCDC_workloop, "CheckDCDC_workloop");
      }

      // To Check Connection
      xoap::bind<PixelTKFECSupervisor>(this, &PixelTKFECSupervisor::Connection_Check, "Connection_Check", XDAQ_NS_URI);

      // Recover
      xoap::bind<PixelTKFECSupervisor>(this, &PixelTKFECSupervisor::Recover, "Recover", XDAQ_NS_URI);

      //Binding SOAP Callback to Low Level Commands
      xoap::bind<PixelTKFECSupervisor>(this, &PixelTKFECSupervisor::SetDelay, "SetDelay", XDAQ_NS_URI);
      xoap::bind<PixelTKFECSupervisor>(this, &PixelTKFECSupervisor::SetDelayEnMass, "SetDelayEnMass", XDAQ_NS_URI);
      xoap::bind<PixelTKFECSupervisor>(this, &PixelTKFECSupervisor::SetAOHGainEnMass, "SetAOHGainEnMass", XDAQ_NS_URI);
      xoap::bind<PixelTKFECSupervisor>(this, &PixelTKFECSupervisor::SetAOHBiasEnMass, "SetAOHBiasEnMass", XDAQ_NS_URI);
      xoap::bind<PixelTKFECSupervisor>(this, &PixelTKFECSupervisor::SetAOHBiasOneChannel, "SetAOHBiasOneChannel", XDAQ_NS_URI);
      xoap::bind<PixelTKFECSupervisor>(this, &PixelTKFECSupervisor::TKFECCalibrations, "Delay25", XDAQ_NS_URI);

}


pixel::PixelTKFECSupervisor::~PixelTKFECSupervisor() {
}

pixel::TCADeviceTKFEC& pixel::PixelTKFECSupervisor::getHw() const
{
  return static_cast<pixel::TCADeviceTKFEC&>(*hwP_.get()); //hwP_ in XDAQAppBase.h
}


void pixel::PixelTKFECSupervisor::hwConnectImpl() {
  //pixel::hwutilstca::tcaDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void pixel::PixelTKFECSupervisor::hwReleaseImpl() {
}


void pixel::PixelTKFECSupervisor::setupInfoSpaces() {
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

 //hwStatusInfoSpaceUpdaterP_ = std::auto_ptr<pixel::HwStatusInfoSpaceUpdater>(new pixel::HwStatusInfoSpaceUpdater(*this, getHw()));
 //hwStatusInfoSpaceP_ = std::auto_ptr<pixel::HwStatusInfoSpaceHandler>(new pixel::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
 FPixDCDCSummaryUpdaterP_ = std::unique_ptr<pixel::FPixDCDCSummaryUpdater>(new pixel::FPixDCDCSummaryUpdater(*this, getHw()));
 FPixDCDCSummaryP_ = std::unique_ptr<pixel::FPixDCDCSummaryHandler>(new pixel::FPixDCDCSummaryHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
 FSMStateSummaryUpdaterP_ = std::unique_ptr<pixel::FSMStateSummaryUpdater>(new pixel::FSMStateSummaryUpdater(*this));
 FSMStateSummaryP_ = std::unique_ptr<pixel::utils::FSMStateSummaryHandler>(new pixel::utils::FSMStateSummaryHandler(*this, FSMStateSummaryUpdaterP_.get()));
 TKFECSOAPMsgHistoryP_ = std::unique_ptr<pixel::utils::SOAPMsgHistoryHandler>(new pixel::utils::SOAPMsgHistoryHandler(*this, hwStatusInfoSpaceUpdaterP_.get(), static_cast<pixel::utils::SOAPER*>(this), "tkfec-soap-msg-history"));
 TKFECLOGMsgHistoryP_ = std::unique_ptr<pixel::utils::LOGMsgHistoryHandler>(new pixel::utils::LOGMsgHistoryHandler(*this, hwStatusInfoSpaceUpdaterP_.get(), static_cast<log4cplus::LoggerHistory*>(appender.get()), "tkfec-log-msg-history"));
 static_cast<log4cplus::LoggerHistory*>(appender.get())->registerInfoSpaceHandler(TKFECLOGMsgHistoryP_.get(), &monitor_);

 // Order of tabs on webpage
 appStateInfoSpace_.registerItemSets(monitor_, webServer_);
 FSMStateSummaryP_->registerItemSets(monitor_, webServer_);
 cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
 TKFECSOAPMsgHistoryP_->registerItemSets(monitor_, webServer_);
 FPixDCDCSummaryP_->registerItemSets(monitor_, webServer_);

 //hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
 TKFECLOGMsgHistoryP_->registerItemSets(monitor_, webServer_);

 cfgInfoSpaceP_->setBool("doDCULoop",doDCULoop_);
 cfgInfoSpaceP_->setBool("workloopContinue",workloopContinue_);
 cfgInfoSpaceP_->setBool("debugBool",debug);
 cfgInfoSpaceP_->setBool("skip_lv_check",skip_lv_check);
 cfgInfoSpaceP_->setBool("do_force_ccu_readout",do_force_ccu_readout);
 cfgInfoSpaceP_->setBool("resume_do_portcard_recovery",resume_do_portcard_recovery);
 cfgInfoSpaceP_->setBool("ser_do_portcard_recovery",ser_do_portcard_recovery);
 cfgInfoSpaceP_->setBool("ser_do_powercycle_dcdc",ser_do_powercycle_dcdc);
 cfgInfoSpaceP_->setBool("extraTimers",extratimers_);
 cfgInfoSpaceP_->setUInt32("readDCU_workloop_sleep",readDCU_workloop_usleep_);

 cfgInfoSpaceP_->setBool("get_pgbit_status",get_pgbit_status);
 cfgInfoSpaceP_->setBool("doDCDCCheckLoop",doDCDCCheckLoop_);
 cfgInfoSpaceP_->setBool("doIteerativeCCUreset",doIteerativeCCUreset_);
 cfgInfoSpaceP_->setBool("doIteerativeCCUreset_forDCU",doIteerativeCCUreset_forDCU_);

 std::set<std::string> allFSMStatesSet = listOfAllStates();
 allFSMStatesSet.erase("Fail");
 std::vector<std::string> allFSMStatesVec( allFSMStatesSet.begin(), allFSMStatesSet.end() );
 FSMStateSummaryP_->setStringVec("allStates", allFSMStatesVec);

 // pixel::utils::XDAQAppWithFSMPixelBase& AppBase = dynamic_cast<pixel::utils::XDAQAppWithFSMPixelBase&>(xdaqApp_);
 // std::set<std::string> allFSMStatesSet = AppBase.listOfAllStates();
 // allFSMStatesSet.erase("Fail");
 // std::vector<std::string> allFSMStatesVec( allFSMStatesSet.begin(), allFSMStatesSet.end() );
 // allFSMStatesVec.erase( remove(allFSMStatesVec.begin(), allFSMStatesVec.end(), "Done"), allFSMStatesVec.end());
 // infoSpaceHandler->setStringVec(name, allFSMStatesVec);
 // updated = true;


 //Attempt to grab all possible global keys

 // aliasesAndKeys_ = PixelConfigInterface::getAliases();
 // for (std::vector<std::pair<std::string, unsigned int> >::iterator i_aliasesAndKeys = aliasesAndKeys_.begin();
 //      i_aliasesAndKeys != aliasesAndKeys_.end(); ++i_aliasesAndKeys) {
 //     std::string alias = i_aliasesAndKeys->first; //This is the name of the configuration
 //     unsigned int alias_key = i_aliasesAndKeys->second; //I presume this is the key. It goes from 0 to 33
 //     bool aliasIsGood = true;
 //     // aliasIsGood = isAliasOK(alias); //EM cannot get this to work yet
 //       if ((alias[0] != 'T' || alias[1] != 'T' || alias[2] != 'C') && aliasIsGood) {
 //         // *out << "  <option value=\"" << alias << "\">" << alias << "</option>" << std::endl;
 //         // cout<<"Alias? "<<alias<<endl;
 //         // cout<<"Key? "<<alias_key<<endl;
 //         // cout<<"\"<tr> <td>"<<alias<<"</td> <td>"<<alias_key<<"</td> </tr>\" +\n";
 //     }
 // }
}

xoap::MessageReference pixel::PixelTKFECSupervisor::Connection_Check(xoap::MessageReference msg){

    LOG4CPLUS_INFO(sv_logger_,"Connection Valid");
    xoap::MessageReference reply = makeCommandSOAPReply(msg);
    return reply;
}

void pixel::PixelTKFECSupervisor::initializeActionImpl(toolbox::Event::Reference event){

    LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::initializeActionImpl- Entering function");
    std::string const msg_info_whi = "--- INITIALIZE ---";
    LOG4CPLUS_INFO(sv_logger_, msg_info_whi);

    //needed for delay25 calibration
    try {
        std::set<const xdaq::ApplicationDescriptor *> set_PixelFECSupervisors = getApplicationContext()->getDefaultZone()->getApplicationGroup("daq")->getApplicationDescriptors("PixelFECSupervisor");
        for (std::set<const xdaq::ApplicationDescriptor *>::iterator i_set_PixelFECSupervisor = set_PixelFECSupervisors.begin(); i_set_PixelFECSupervisor != set_PixelFECSupervisors.end(); ++i_set_PixelFECSupervisor) {
            PixelFECSupervisors_.insert(make_pair((*i_set_PixelFECSupervisor)->getInstance(), *(i_set_PixelFECSupervisor)));
        }
    }
    catch (xdaq::exception::Exception &e) {
        std::string const msg_warn_uxn = "No PixelFECSupervisor(s) found in the \"daq\" group.";
        LOG4CPLUS_WARN(sv_logger_, msg_warn_uxn);
        XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_warn_uxn, e);
        this->notifyQualified("fatal", f);
        LOG4CPLUS_ERROR(sv_logger_, "No PixelFECSupervisor(s) found in the \"daq\" group.");
    }

    // Detect PixelSupervisor
    try {
        PixelSupervisor_ = getApplicationContext()->getDefaultZone()->getApplicationGroup("daq")->getApplicationDescriptor("PixelSupervisor", 0);
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::initializeActionImpl - Instance 0 of PixelSupervisor found.");
    }
    catch (xdaq::exception::Exception &e) {
        LOG4CPLUS_ERROR(sv_logger_, "PixelTKFECSupervisor::initializeActionImpl - Instance 0 of PixelSupervisor not found!");
    }

    // Detect PixelDCStoTKFECDpInterface
    try {
        PixelDCStoTKFECDpInterface_ = getApplicationContext()->getDefaultZone()->getApplicationGroup("dcs")->getApplicationDescriptor("PixelDCStoTrkFECDpInterface", 0);
    }
    catch (xdaq::exception::Exception &e) {
        LOG4CPLUS_ERROR(sv_logger_, "PixelTKFECSupervisor::initializeActionImpl - Instance 0 of PixelDCStoTKFECInterface not found!");
    }

    // Detect PixelDCSFSMInterface
    try{
        PixelDCSFSMInterface_ = getApplicationContext()->getDefaultZone()->getApplicationGroup("dcs")->getApplicationDescriptor("PixelDCSFSMInterface", 0);
        LOG4CPLUS_INFO(sv_logger_,"PixelTKFECSupervisor::initializeActionImpl - Instance 0 of PixelDCSFSMInterface found.");
    }
    catch (xdaq::exception::Exception &e) {
        LOG4CPLUS_INFO(sv_logger_,"PixelTKFECSupervisor::initializeActionImpl - Instance 0 of PixelDCSFSMInterface not found. Automatic Detector Startup procedure will not be followed. The detector must be powered on manually.");
    }

    // Query PixelDCSFSMInterface with a SOAP message. Update member data after parsing reply.
    if (PixelDCSFSMInterface_ != 0) {
        ////Attribute_Vector parametersToSend(3, Attribute());
        ////parametersToSend[0].name_ = "name";
        ////parametersToSend[0].value_ = "PixelTKFECSupervisor";
        ////parametersToSend[1].name_ = "type";
        ////parametersToSend[1].value_ = "TrkFEC";
        ////parametersToSend[2].name_ = "instance";
        ////parametersToSend[2].value_ = pixel::utils::to_string(crate_);
        Attributes parametersToSend;
        parametersToSend["name"] = "PixelTKFECSupervisor";
        parametersToSend["type"] = "TrkFEC";
        parametersToSend["instance"] = pixel::utils::to_string(crate_);
        LOG4CPLUS_INFO(sv_logger_,"PixelTKFECSupervisor::initializeActionImpl - about to send SOAP to PixelDCSFSMInterface");
        xoap::MessageReference fsmStateResponse = sendWithSOAPReply(PixelDCSFSMInterface_, "fsmStateRequest", parametersToSend);
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::initializeActionImpl - received SOAP from PixelDCSFSMInterface");
        std::stringstream msg_info_dcssoap;
        fsmStateResponse->writeTo(msg_info_dcssoap);
        LOG4CPLUS_INFO(sv_logger_, msg_info_dcssoap.str());

        xoap::SOAPEnvelope envelope = fsmStateResponse->getSOAPPart().getEnvelope();
        xoap::SOAPName notificationName = envelope.createName("fsmStateResponse");
        std::vector<xoap::SOAPElement> notifications = envelope.getBody().getChildElements(notificationName);
        for (unsigned int j = 0; j < notifications.size(); ++j) {
            xoap::SOAPName stateNameSOAP = envelope.createName("state");
            xoap::SOAPName partitionName = envelope.createName("partition");
            std::vector<xoap::SOAPElement> stateList = notifications.at(j).getChildElements(stateNameSOAP);
            for (unsigned int i = 0; i < stateList.size(); ++i) {
                std::string powerCoordinate = stateList.at(i).getAttributeValue(partitionName);
                std::string fsmState = stateList.at(i).getValue();
                LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::initializeActionImpl - powerCoordinate = "+powerCoordinate+", fsmState = "+fsmState);
                try {
                    powerCoordinate = powerCoordinate.substr(0, 8);
                }
                catch (exception &e) {
                    LOG4CPLUS_ERROR(sv_logger_, "power Coordinate too short - not >=8 characters ?");
                    XCEPT_RAISE(xcept::Exception, "power Coordinate too short - not >=8 characters ?");
                }
                if (fsmState == "LV_OFF")
                    powerMap_.setVoltage(powerCoordinate, LV_OFF, std::cout);
                else if (fsmState == "LV_ON")
                    powerMap_.setVoltage(powerCoordinate, LV_ON, std::cout);
                else
                   LOG4CPLUS_WARN(sv_logger_, "PixelTKFECSupervisor::initializeActionImpl - "+fsmState+" not recognized!");
            }
        }
    }

    std::string const msg_info_ugd = "--- INITIALIZATION DONE ---";
    LOG4CPLUS_INFO(sv_logger_, msg_info_ugd);
}

void pixel::PixelTKFECSupervisor::transitionHaltedToConfiguringActionImpl(toolbox::Event::Reference event){

    if (extratimers_){
        GlobalTimer_.start("Start of TKFEC configuration");
        GlobalTimer_.printTime("transitionHaltedToConfiguringActionImpl -- Enter");
    }

    // check Global Key
    std::string global_key = cfgInfoSpaceP_->getString("GlobalKey");

    if (global_key=="-1"){
        LOG4CPLUS_ERROR(sv_logger_,"Global Key not provided for command Configure");
        XCEPT_RAISE(xcept::Exception, "Global Key not provided for Configure command");
    }
    theGlobalKey_ = new pos::PixelConfigKey(atoi((global_key).c_str()));
    if (theGlobalKey_ == 0) {
        std::string const msg_error_etn = "Failure to create GlobalKey";
        LOG4CPLUS_ERROR(sv_logger_,msg_error_etn);
        XCEPT_RAISE(xcept::Exception, msg_error_etn);
    }
    LOG4CPLUS_INFO(sv_logger_,"transitionHaltedToConfiguringActionImpl -- Global Key : "+global_key);

    if (mapNamePortCard_.size() != 0 || thePortcardMap_ != 0)
        cleanupGlobalConfigData();

    if (extratimers_)
        GlobalTimer_.printTime("transitionHaltedToConfiguringActionImpl -- get calib");

    try { //access configuration data
        PixelConfigInterface::get(theCalibObject_, "pixel/calib/", *theGlobalKey_);

        if (extratimers_)
            GlobalTimer_.printTime("transitionHaltedToConfiguringActionImpl -- build ROC and module lists");
        // Build ROC and module lists.
        PixelConfigInterface::get(theNameTranslation_, "pixel/nametranslation/", *theGlobalKey_);

        if (dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_) != 0) {
            pos::PixelDetectorConfig *detconfig = 0; // temporary, used only to build ROC and module lists
            PixelConfigInterface::get(detconfig, "pixel/detconfig/", *theGlobalKey_);
            if (detconfig == 0)
                XCEPT_RAISE(toolbox::fsm::exception::Exception, "[PixelTKFECSupervisor::transitionHaltedToConfiguringActionImpl] The detconfig could not be loaded");
            (dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_))->buildROCAndModuleLists(theNameTranslation_, detconfig);
            delete detconfig;
        }

        if (extratimers_)
            GlobalTimer_.printTime("transitionHaltedToConfiguringActionImpl -- read other files");

        //needed for delay25 calibration
        PixelConfigInterface::get(theFECConfiguration_, "pixel/fecconfig/", *theGlobalKey_);
        PixelConfigInterface::get(theTKFECConfiguration_, "pixel/tkfecconfig/", *theGlobalKey_);
        PixelConfigInterface::get(thePortcardMap_, "pixel/portcardmap/", *theGlobalKey_);
        PixelConfigInterface::get(thePowerMap_, "pixel/powermap/", *theGlobalKey_);
        PixelConfigInterface::get(theDCDCConverterConfig_, "pixel/dcdc/", *theGlobalKey_);
        PixelConfigInterface::get(theGlobalDelay25_, "pixel/globaldelay25/", *theGlobalKey_);
        if (theGlobalDelay25_ == 0)
            LOG4CPLUS_INFO(sv_logger_,"Global delay in Delay25 is not specified. Using only the default Delay25 settings.");
    }
    catch (toolbox::fsm::exception::Exception &e) {
        throw;
    }
    catch (std::exception &e) { //translate std::exception to the correct type
        XCEPT_RAISE(toolbox::fsm::exception::Exception, string(e.what()));
    }

    if (extratimers_)
        GlobalTimer_.printTime("transitionHaltedToConfiguring -- done!");

    //check for failure
    //can't test theGlobalDelay25_ or theCalibObject_ because it is allowed to be undefined
    if (theNameTranslation_ == 0 || theFECConfiguration_ == 0 || theTKFECConfiguration_ == 0 || thePortcardMap_ == 0)
        XCEPT_RAISE(toolbox::fsm::exception::Exception, "Failure to load configuration data in PixelTKFECSupervisor::transitionHaltedToConfiguring");

    //this exception will be automatically caught and transition us to the Error state

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::transitionHaltedToConfiguringActionImpl(event);
}

void pixel::PixelTKFECSupervisor::configureActionImpl(toolbox::Event::Reference event){

    if (extratimers_)
        GlobalTimer_.printTime("configActionImpl -- Enter");

    bool isfailure = false;
    pos::PixelDetectorConfig *detconfig = 0;

    try{ //load configuration data and access hardware

        bool proceed = true;
        PixelConfigInterface::get(detconfig, "pixel/detconfig/", *theGlobalKey_);
        if (detconfig == 0)
            XCEPT_RAISE(xdaq::exception::Exception, "Failed to load detconfig!");

        //Use the presence of PixelDCSFSMInterface to trigger the automatic detector startup
        if (PixelDCSFSMInterface_ != 0) {
            std::string const msg_debug_pfh = "[PixelTKFECSupervisor::configActionImpl] PixelDCSFSMInterface detected. Automatic detector startup.";
            LOG4CPLUS_INFO(sv_logger_, msg_debug_pfh);
            const std::set<std::string> &portcards = thePortcardMap_->portcards(detconfig);
            std::set<std::string>::const_iterator iportcard;
            LOG4CPLUS_INFO(sv_logger_,"Number of portcards to configure:"+portcards.size());

            for (iportcard = portcards.begin(); iportcard != portcards.end(); iportcard++) {
                std::string powerCoordinate = iportcard->substr(0, 8);
                LOG4CPLUS_DEBUG(sv_logger_,"PixelTKFECSupervisor::configActionImpl - Portcard "+*iportcard+" has power coordinate "+powerCoordinate);
                BiVoltage power = powerMap_.getVoltage(powerCoordinate, std::cout);
                std::stringstream msg_info_power;
                msg_info_power<<powerCoordinate<<" "<<power<<"\n";
                LOG4CPLUS_DEBUG(sv_logger_, msg_info_power.str());

                if (power == LV_OFF) {
                    LOG4CPLUS_INFO(sv_logger_,"[PixelTKFECSupervisor::configActionImpl] The voltage for "+powerCoordinate+" is OFF!");
                    proceed = false;
                }
                if (power == LV_ON) {
                    LOG4CPLUS_INFO(sv_logger_,"[PixelTKFECSupervisor::configActionImpl] The voltage for "+powerCoordinate+" is ON.");
                }
            }
        }
        if (skip_lv_check){
            if (!proceed) {
                std::string const msg_trace_oyy = "Ignoring OFF power status and proceeding";
                LOG4CPLUS_TRACE(sv_logger_, msg_trace_oyy);
            }
            proceed = true;
        }

        std::string const msg_trace_oym = "configActionImpl -- configure portcards";
        LOG4CPLUS_TRACE(sv_logger_, msg_trace_oym);

        if (extratimers_)
            GlobalTimer_.printTime("configActionImpl -- Power check finished");

        if (proceed) {

            const long loop = 5;
            const unsigned long tms = 50000; // wait tms microseconds FIXME Atanu 0 to 1s
            string fecAccessType = "unset";
            bool fack = true;
            const std::set<std::string> &portcards = thePortcardMap_->portcards(detconfig);
            unsigned int slot = 9999;
            unsigned int ring = 9999;
            static bool ringInit[9] = { false, false, false, false, false, false, false, false, false }; // has the ring been reset
            int np = 0;

            //this first loop loads the portcard information from the database
            //it also creates the fecAccess and resets the control rings
            map<unsigned int, set<pair<unsigned int, bool> > > ccuRingMap;
            set<std::string>::const_iterator iiportcard;

            for (iiportcard = portcards.begin(); iiportcard != portcards.end(); iiportcard++){ // Cycle over portcards

                LOG4CPLUS_INFO(sv_logger_,"config portcard "+*iiportcard);
                pos::PixelPortCardConfig *tempPortCard = 0;
                PixelConfigInterface::get(tempPortCard, "pixel/portcard/" + *iiportcard, *theGlobalKey_);

                if (extratimers_)
                    GlobalTimer_.printTime("configActionImpl -- after get");
                if(tempPortCard == 0)
                    XCEPT_RAISE(xdaq::exception::Exception, "Failed to load portcard!");

                const std::string TKFECID = tempPortCard->getTKFECID();
                if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_)
                    continue;

                slot = theTKFECConfiguration_->addressFromTKFECID(TKFECID);
                ring = tempPortCard->getringAddress();
                mapNamePortCard_[*iiportcard] = tempPortCard;

                np++;
                const std::string type = theTKFECConfiguration_->typeFromTKFECID(TKFECID);
                if (type != fecAccessType && fecAccessType != "unset")
                    XCEPT_RAISE(xcept::Exception, "Failed to configure TKFEC; incorrect fecAccessType");

                //creates the fecAccess object
                if (np == 1) {
                    LOG4CPLUS_INFO(sv_logger_, "[PixelTKFECSupervisor::configActionImpl] Will initialize slot:" << slot);
                    if (fecAccess_ == 0) {

                        if (extratimers_)
                            GlobalTimer_.printTime("configActionImpl -- Will create FEC access");
                        LOG4CPLUS_INFO(sv_logger_, "First device, will create Fec Access");

                        // Create the FEC Access
                        try{
                            if (type == "CTA") {
                                std::string dummyConnections = "file://";
                                dummyConnections += getenv("ENV_CMS_TK_ONLINE_ROOT");
                                dummyConnections += "/generic/config/connections.xml";
                                std::string addressTablePath = "file://";
                                addressTablePath += getenv("ENV_CMS_TK_ONLINE_ROOT");
                                addressTablePath += "/generic/config/address_mfec.xml";

                                fecAccess_ = FecAccess::createUtcaFecAccess(dummyConnections,TKFECID,theTKFECConfiguration_->URIFromTKFECID(TKFECID),addressTablePath,100, false, false);
                                if (!fecAccess_)
                                    LOG4CPLUS_ERROR(sv_logger_, "Creation of FecAccess failed. fecAccess pointer null.");
                            } // if type CTA
                            else
                                XCEPT_RAISE(xcept::Exception, "Failed to configure TKFEC; only type CTA is allowed");
                            fecAccessType = type;
                        }
                        catch (FecExceptionHandler& e) {
                            LOG4CPLUS_ERROR(sv_logger_, "------------ Exception ----------");
                            LOG4CPLUS_ERROR(sv_logger_, e.what());
                            LOG4CPLUS_ERROR(sv_logger_, "---------------------------------");
                            XCEPT_RAISE(xdaq::exception::Exception, "Failed to create FEC access");
                        }
                    } // if fecAccess_
                    else{
                        LOG4CPLUS_INFO(sv_logger_, "First device, Fec Access already exists");
                    }

                    // Set the options for the FecAccess
                    fecAccess_->setForceAcknowledge(fack);
                    fecAccess_->seti2cChannelSpeed(tempPortCard->geti2cSpeed());

                    if (extratimers_)
                        GlobalTimer_.printTime("configActionImpl -- After speed and ack programming");

                    // Enable the device driver counters
                    if (getErrorCounterFlag()) {
                        fprintf(getStdchan(), "------------------------------------ Error counting start\ntimestamp=%ld\n", time(NULL));
                    }

                } // if np

                //send reset, check if ring has been reset already
                if (!ringInit[(ring)]) { // rings go from 1-8
                    LOG4CPLUS_INFO(sv_logger_, " Reset slot/mfec "+pixel::utils::to_string(slot)+"/"+pixel::utils::to_string(ring));
                    resetPlxFec(fecAccess_, slot, ring, loop, tms);
                    ringInit[ring] = true;
                }
                else
                    LOG4CPLUS_DEBUG(sv_logger_, "NOT RESETTING slot/mfec "+pixel::utils::to_string(slot)+"/"+pixel::utils::to_string(ring)+"since it was already done");
                if (extratimers_)
                    GlobalTimer_.printTime("configActionImpl -- After resetPlxFec");

                // this would be where we pull a STATUS flag for the CCU out of the configuration data
                // for now insert true for everything
                // for testing, we could arbitrarily insert 'false' for one ccu
                ccuRingMap[tempPortCard->getringAddress()].insert(make_pair(tempPortCard->getccuAddress(), true));
                //using a std::set should make a list of 8 CCU addresses for the BPix and 4 for the fpix

                if (extratimers_)
                    GlobalTimer_.printTime("configActionImpl -- After ccuRingMap");
            } // end portcard loop

            // Each TKFEC controls several CCU rings (4 for FPix, 4 for BPix)
            // Each CCU ring has either 4 (FPix) or 8 (BPix) CCUs plus the dummy
            // the topology of the CCU rings happens to be in decreasing address order
            // so we will put them in a set and iterate from the end

            if (extratimers_)
                GlobalTimer_.printTime("configActionImpl -- Finish FecAccess part");

            // PIAReset
            ////Attribute_Vector parametersXgi(1, Attribute());
            ////parametersXgi.at(0).name_ = "resetParam";
            ////parametersXgi.at(0).value_ = "all";
            Attributes parametersXgi;
            parametersXgi["resetParam"] = "all";
            std::string PIAReset_command = "PIAReset";
            xoap::MessageReference reset_msg = makeSOAPMessageReference(PIAReset_command, parametersXgi);
            PIAReset(reset_msg);
            LOG4CPLUS_INFO(sv_logger_, "Execute pia reset before portcard configuration ");

            // Now really program portcards
            LOG4CPLUS_INFO(sv_logger_, "Number of portcards to configure:"+pixel::utils::to_string(portcards.size()));
            sleep(5);

	    //Disable the DCDCs of BPix_BmI_SEC7_LYR3/4 which cannot be programmed due to a non-locked QPLL
            //If this portcard is recovered, remove the following line
            bool dcdcDisableStatus = disableDCDCs_BPix_BmI_SEC7_LYR34();


            bool status = programPortcards(true);
            if (status) {
                LOG4CPLUS_ERROR(sv_logger_, "Error in portcard config");
                isfailure = true;
            }
            if (extratimers_)
                GlobalTimer_.printTime("configActionImpl -- configure portcards done!");
            suppressHardwareError_ = false;

            if (isfailure) { //go to error
                try{
                    if (!skip_lv_check)
                        XCEPT_RAISE(xcept::Exception, "Failed to configure TKFEC");
                }
                catch (toolbox::fsm::exception::Exception &e) {
                    XCEPT_RETHROW(xoap::exception::Exception, "Invalid Command.", e);
                }
            }

        } // if proceed
    } //end of the BIG try-
    catch (FecExceptionHandler &e) {
        std::string const msg_error_thv = "Failed to configure TKFEC (is the power off?); exception: " + string(e.what());
        LOG4CPLUS_ERROR(sv_logger_, msg_error_thv);
        pixel::PixelTKFECSupervisorException fecexception_handler("FecExceptionHandler", "module", msg_error_thv, 1865, "PixelTKFECSupervisor::configureActionImpl(toolbox::fsm::FiniteStateMachine&)");
        XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_thv, fecexception_handler);
        this->notifyQualified("fatal", f);
        isfailure = true;
    }
    catch (std::exception &e) { //failure to load config data raises std::exception (not xcept::Exception)
        std::string const msg_error_gpv = "Failed to configure TKFEC; exception: " + string(e.what());
        LOG4CPLUS_ERROR(sv_logger_, msg_error_gpv);
        std::exception *error_ptr = &e;
        pixel::PixelTKFECSupervisorException *new_exception = dynamic_cast<pixel::PixelTKFECSupervisorException *>(error_ptr);
        XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_gpv, *new_exception);
        this->notifyQualified("fatal", f);
        isfailure = true;
    }
    
    if (theCalibObject_ == 0) { // This must be a Physics Run
    }
    else{
        //Create pointers to give to the calibration object
        PixelTKFECSupervisorConfiguration *pixTKFECSupConfPtr = dynamic_cast<PixelTKFECSupervisorConfiguration *>(this);
        
        SOAPER *soaperPtr = dynamic_cast<SOAPER *>(this);
        theTKFECCalibrationBase_ = 0;
        std::string mode = theCalibObject_->mode();
        PixelCalibrationFactory calibFactory;
        std::cout<<"pixel::PixelTKFECSupervisor::configureActionImpl() -- mode "<<mode<<std::endl;
        theTKFECCalibrationBase_ = calibFactory.getTKFECCalibration(mode, pixTKFECSupConfPtr, soaperPtr);
    }

    if (isfailure) {
        try {
            if (!skip_lv_check)
                XCEPT_RAISE(xcept::Exception, "Failed to configure TKFEC");
        }
        catch (toolbox::fsm::exception::Exception &e2) {
            std::string const msg_fatal_gqc = "Failed to transition to Failed state!";
            LOG4CPLUS_FATAL(sv_logger_, msg_fatal_gqc);
            XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_fatal_gqc, e2);
            this->notifyQualified("fatal", f);
        }
    }

    //Creating list of DCDCs
    std::vector<pos::PixelModuleName> modules = detconfig->getModuleList();
    ccu_list.clear();
    for (int i=0;  i<(int)modules.size(); i++){
        std::pair< std::string, std::string > dcdcpgname = thePowerMap_->dcdcAndPowergroup(modules[i]);
        std::string portcard_name_temp = *(thePortcardMap_->portcards(modules[i]).begin());
        pos::PixelPortCardConfig* tempPortCard = 0;
        PixelConfigInterface::get(tempPortCard, "pixel/portcard/" + portcard_name_temp, *theGlobalKey_);
        const std::string& TKFECID = tempPortCard->getTKFECID();
        if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_)
            continue;
        boost::tuple<unsigned int, unsigned int, unsigned int> dcdc_info = boost::make_tuple(theTKFECConfiguration_->addressFromTKFECID(TKFECID), tempPortCard->getringAddress(), tempPortCard->getccuAddress());
        if (dcdc_list.find(dcdcpgname)==dcdc_list.end())
            dcdc_list[dcdcpgname] = dcdc_info;
        bool ccu_match = 0;
        for (int i=0; i<(int)ccu_list.size(); i++){
            if (boost::get<0>(dcdc_info)==boost::get<0>(ccu_list[i]) && boost::get<1>(dcdc_info)==boost::get<1>(ccu_list[i]) && boost::get<2>(dcdc_info)==boost::get<2>(ccu_list[i])){
                ccu_match = 1;
                break;
            }
        }
        if (!ccu_match)
            ccu_list.push_back(dcdc_info);
    }
    initialize_dcdc_dcu_tables();
    delete detconfig;

    // Do stuff previously done in stateConfigured

    //test for theCalibObject in order to decide if we are in physics mode
    // Submit workloops
    if (theCalibObject_ == 0) {
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::configureActionImpl");
        submit_workloops();
    }

    if (extratimers_) {
        GlobalTimer_.stop("End of TKFEC configuration");
        LOG4CPLUS_INFO(sv_logger_, "TKFEC configuration took "+pixel::utils::to_string(GlobalTimer_.tottime()));
        GlobalTimer_.reset();
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::configureActionImpl(event);
    return;
}

void pixel::PixelTKFECSupervisor::startActionImpl(toolbox::Event::Reference event){

    std::string const msg_info_etx = "-- START --";
    LOG4CPLUS_INFO(sv_logger_, msg_info_etx);

    runNumber_ = pixel::utils::to_string(cfgInfoSpaceP_->getUInt32("runNumber"));
    if (runNumber_ == "0"){
        LOG4CPLUS_ERROR(sv_logger_,"Run Number not provided for command Start");
        XCEPT_RAISE(xcept::Exception, "Run Number not provided for Start command");
    }
    LOG4CPLUS_INFO(sv_logger_,"PixelTKFECSupervisor::startActionImpl -- Run Number : "+runNumber_);
    setupOutputDir();

    std::string const msg_info_eff = "Start Run " + runNumber_;
    LOG4CPLUS_INFO(sv_logger_, msg_info_eff);
    suppressHardwareError_ = false;

    // Start workloops
    if (theCalibObject_ == 0) {
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::startActionImpl");
        start_workloops();
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::startActionImpl(event);
    return;
}

void pixel::PixelTKFECSupervisor::stopActionImpl(toolbox::Event::Reference event){

    std::string const msg_debug_kqg = "-- STOP --";
    LOG4CPLUS_DEBUG(sv_logger_, msg_debug_kqg);

    // Stop workloops
    if (theCalibObject_ == 0) {
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::stopActionImpl");
        stop_workloops();
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::stopActionImpl(event);
    return;
}

void pixel::PixelTKFECSupervisor::pauseActionImpl(toolbox::Event::Reference event){

    pixel::utils::Timer pausetimer;
    pausetimer.start();

     // Stop workloops
    if (theCalibObject_ == 0) {
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::pauseActionImpl");
        stop_workloops();
    }

    pausetimer.stop();

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::pauseActionImpl(event);
    return;
}

void pixel::PixelTKFECSupervisor::resumeActionImpl(toolbox::Event::Reference event){

    if (resume_do_portcard_recovery){
        try{
            // do reporgamming
            LOG4CPLUS_INFO(sv_logger_, "RESUME: Will reprogram portcrads");
            bool status = programPortcards(false);
            if (status) {
                std::string const msg_error_ekf = "Resume - Error in portcard programming ";
                LOG4CPLUS_WARN(sv_logger_, msg_error_ekf);
            }
        }
        catch (...) { //exceptions might be thrown by startupHVCheck
            std::string const msg_error_huc = "Resume - Error in portcard programming ";
            LOG4CPLUS_ERROR(sv_logger_, msg_error_huc);
            pixel::PixelTKFECSupervisorException trivial_exception("pixel::PixelTKFECSupervisorException", "module", msg_error_huc, 958, "PixelTKFECSupervisor::Resume(xoap::MessageReference)");
            XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_huc, trivial_exception);
            this->notifyQualified("fatal", f);
        }
    }
    suppressHardwareError_ = false;

    // Start workloops
    if (theCalibObject_ == 0) {
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::resumeActionImpl" );
        start_workloops();
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::resumeActionImpl(event);
    return;
}

void pixel::PixelTKFECSupervisor::haltActionImpl(toolbox::Event::Reference event){

    std::string const msg_debug_ejv = "-- HALT --";
    LOG4CPLUS_DEBUG(sv_logger_, msg_debug_ejv);

    // Remove workloops
    if (theCalibObject_ == 0) {
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::haltActionImpl" );
        remove_workloops();
    }

    cleanupGlobalConfigData();

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::haltActionImpl(event);
    return;
}

xoap::MessageReference pixel::PixelTKFECSupervisor::Recover(xoap::MessageReference msg){

    std::string const msg_info_fia = "-- RECOVER --";
    LOG4CPLUS_INFO(sv_logger_, msg_info_fia);

    if (pixel::utils::XDAQAppWithFSMPixelBase::getCurrentStateName() != "Failed")
        return makeSOAPMessageReference("RecoverFailed");

    // Remove workloops
    if (theCalibObject_ == 0) {
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::Recover" );
        remove_workloops();
    }

    //we should cleanup the fecAccess_ here, in case creating it was the source of failure
    delete fecAccess_;
    fecAccess_ = 0;

    cleanupGlobalConfigData();

    xoap::MessageReference reply = makeSOAPMessageReference("RecoverDone");
    try {
        pixel::utils::XDAQAppWithFSMPixelBase::halt();
    }
    catch (toolbox::fsm::exception::Exception &e) {
        std::string const msg_error_xsj = "Failed to transition to Halted state! message: " + string(e.what());
        LOG4CPLUS_ERROR(sv_logger_, msg_error_xsj);
        XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_xsj, e);
        this->notifyQualified("fatal", f);
        reply = makeSOAPMessageReference("RecoverFailed");
    }

    std::string const msg_info_txn = "-- Exit RECOVER --";
    LOG4CPLUS_INFO(sv_logger_, msg_info_txn);
    return reply;
}

void pixel::PixelTKFECSupervisor::reconfigureActionImpl(toolbox::Event::Reference event){

    std::string const msg_info_ykh = "-- RECONFIGURE --";
    LOG4CPLUS_INFO(sv_logger_, msg_info_ykh);

    // Stop workloops
    if (theCalibObject_ == 0) {
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::reconfigureActionImpl" );
        stop_workloops();
    }

    bool isfailure = false;

    try{
        // check Global Key
        std::string global_key = cfgInfoSpaceP_->getString("GlobalKey");
        if (global_key=="-1"){
            LOG4CPLUS_ERROR(sv_logger_,"PixelTKFECSupervisor::reconfigureActionImpl - Global Key not provided for command Reconfigure");
            XCEPT_RAISE(xcept::Exception, "PixelTKFECSupervisor::reconfigureActionImpl - Global Key not provided for Reconfigure command");
        }
        pos::PixelConfigKey *newGlobalKey = new pos::PixelConfigKey(atoi((global_key).c_str()));
        if (newGlobalKey == 0) {
            std::string const msg_error_oha = "PixelTKFECSupervisor::reconfigureActionImpl - Failure to create GlobalKey";
            LOG4CPLUS_ERROR(sv_logger_,msg_error_oha);
            XCEPT_RAISE(xcept::Exception, msg_error_oha);
        }
        LOG4CPLUS_INFO(sv_logger_,"ReconfigureActionImpl -- Global Key : "+global_key);

        pos::PixelGlobalDelay25 *newGlobalDelay25 = 0;
        pos::PixelPortCardConfig *tempPortCard = 0;
        PixelConfigInterface::get(newGlobalDelay25, "pixel/globaldelay25/", *newGlobalKey);
        if (newGlobalDelay25 == 0)
            XCEPT_RAISE(xdaq::exception::Exception, "PixelTKFECSupervisor::reconfigureActionImpl - The globalDelay25 object is null!");

        enumDeviceType modeType = PHILIPS;
        //loop over portcards
        map<string, pos::PixelPortCardConfig *>::const_iterator iportcard = mapNamePortCard_.begin();
        for (; iportcard != mapNamePortCard_.end(); ++iportcard) {
            const std::string &name = iportcard->first;
            PixelConfigInterface::get(tempPortCard, "pixel/portcard/" + name, *newGlobalKey);

            const string TKFECID = tempPortCard->getTKFECID();

            if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_){
                LOG4CPLUS_ERROR(sv_logger_, "PixelTKFECSupervisor::reconfigureActionImpl - Mismatch in crate number in portcard configuration");
                XCEPT_RAISE(xcept::Exception, "PixelTKFECSupervisor::reconfigureActionImpl - Mismatch in crate number in portcard configurationi");
            }
            const unsigned int TKFECAddress = theTKFECConfiguration_->addressFromTKFECID(TKFECID);
            //need to set SCL, TRG, SDA
            vector<pair<unsigned int, unsigned int> > toProgram;
            unsigned int deviceAddress;
            unsigned int value = 0;
            //get the global delay offset:
            unsigned int global_offset = newGlobalDelay25->getDelay(0);

            deviceAddress = tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_Delay25_SCL);
            value = tempPortCard->getdeviceValuesForSetting(pos::PortCardSettingNames::k_Delay25_SCL);
            LOG4CPLUS_INFO(sv_logger_, "SCL delay for device " + tempPortCard->getPortCardName() + " value hex " + pixel::utils::hex_to_string(value) + " global delay " + pixel::utils::hex_to_string(global_offset));
            value = newGlobalDelay25->getDelay(value);
            LOG4CPLUS_INFO(sv_logger_, "SCL delay for device after adding " + tempPortCard->getPortCardName() + " value hex " + pixel::utils::hex_to_string(value) + " global delay " + pixel::utils::hex_to_string(global_offset));
            toProgram.push_back(make_pair(deviceAddress, value));

            deviceAddress = tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_Delay25_TRG);
            value = tempPortCard->getdeviceValuesForSetting(pos::PortCardSettingNames::k_Delay25_TRG);
            LOG4CPLUS_INFO(sv_logger_, "TRG delay for device " + tempPortCard->getPortCardName() + " value hex " + pixel::utils::hex_to_string(value) + " global delay " + pixel::utils::hex_to_string(global_offset));
            value = newGlobalDelay25->getDelay(value);
            LOG4CPLUS_INFO(sv_logger_, "TRG delay for device after adding " + tempPortCard->getPortCardName() + " value hex " + pixel::utils::hex_to_string(value) + " global delay " + pixel::utils::hex_to_string(global_offset));
            toProgram.push_back(make_pair(deviceAddress, value));

            deviceAddress = tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_Delay25_SDA);
            value = tempPortCard->getdeviceValuesForSetting(pos::PortCardSettingNames::k_Delay25_SDA);
            LOG4CPLUS_INFO(sv_logger_, "SDA delay for device " + tempPortCard->getPortCardName() + " value hex " + pixel::utils::hex_to_string(value) + " global delay " + pixel::utils::hex_to_string(global_offset));
            value = newGlobalDelay25->getCyclicDelay(value);
            LOG4CPLUS_INFO(sv_logger_, "SDA delay for device after adding " + tempPortCard->getPortCardName() + " value hex " + pixel::utils::hex_to_string(value) + " global delay " + pixel::utils::hex_to_string(global_offset));
            toProgram.push_back(make_pair(deviceAddress, value));

            for (vector<pair<unsigned int, unsigned int> >::const_iterator i = toProgram.begin(); i != toProgram.end(); ++i) {
                try {
                    portcardI2CDevice(fecAccess_,TKFECAddress,tempPortCard->getringAddress(),tempPortCard->getccuAddress(),tempPortCard->getchannelAddress(),i->first,modeType,i->second);
                    unsigned int readBack = portcardI2CDeviceRead(fecAccess_,TKFECAddress,tempPortCard->getringAddress(),tempPortCard->getccuAddress(),tempPortCard->getchannelAddress(),i->first,modeType);
                    if ((i->second) != readBack) {
                        std::string const msg_error_cco = "Programming Error in Reconfigure! Read back != value";
                        LOG4CPLUS_ERROR(sv_logger_, msg_error_cco);
                        std::stringstream msg_debug_portcard_reconfigure;
                        msg_debug_portcard_reconfigure<<"ccu address=0x" << std::hex << tempPortCard->getccuAddress()<<" chan=0x" << tempPortCard->getchannelAddress() << std::dec;
                        msg_debug_portcard_reconfigure<<" deviceAddress = 0x" << std::hex << i->first << ", value = 0x" << i->second << std::dec << ", flag = " << 1 << "\n";
                        msg_debug_portcard_reconfigure<<"read back = 0x " << std::hex << readBack << std::dec << endl;
                        LOG4CPLUS_INFO(sv_logger_, msg_debug_portcard_reconfigure.str());
                    }
                }
                catch (FecExceptionHandler &e) {
                    resetPlxFec(fecAccess_, TKFECAddress, tempPortCard->getringAddress(), 1, 0);
                    portcardI2CDevice(fecAccess_,TKFECAddress,tempPortCard->getringAddress(),tempPortCard->getccuAddress(),tempPortCard->getchannelAddress(),i->first,modeType,i->second);
                    unsigned int readBack = portcardI2CDeviceRead(fecAccess_,TKFECAddress,tempPortCard->getringAddress(),tempPortCard->getccuAddress(),tempPortCard->getchannelAddress(),i->first,modeType);
                    if ((i->second) != readBack) {
                        std::string const msg_error_cco = "Programming Error in Reconfigure! Read back != value";
                        LOG4CPLUS_ERROR(sv_logger_, msg_error_cco);
                        std::stringstream msg_debug_portcard_reconfigure;
                        msg_debug_portcard_reconfigure<<"ccu address=0x" << std::hex << tempPortCard->getccuAddress()<<" chan=0x" << tempPortCard->getchannelAddress() << std::dec;
                        msg_debug_portcard_reconfigure<<" deviceAddress = 0x" << std::hex << i->first << ", value = 0x" << i->second << std::dec << ", flag = " << 1 << "\n";
                        msg_debug_portcard_reconfigure<<"read back = 0x " << std::hex << readBack << std::dec << endl;
                        LOG4CPLUS_INFO(sv_logger_, msg_debug_portcard_reconfigure.str());
                    }
                }
            }
        }
        //if we get this far, then we've successfully reprogrammed the hardware
        delete theGlobalKey_;
        delete theGlobalDelay25_;
        delete tempPortCard;
        theGlobalKey_ = newGlobalKey;
        theGlobalDelay25_ = newGlobalDelay25;
    }
    catch (FecExceptionHandler &e) {
        std::string const msg_error_xdtt = "Failed to configure TKFEC (is the power off?); exception: " + string(e.what());
        LOG4CPLUS_ERROR(sv_logger_, msg_error_xdtt);
        pixel::PixelTKFECSupervisorException fecexception_handler("FecExceptionHandler", "module", msg_error_xdtt, 1865, "PixelTKFECSupervisor::reconfigureActionImpl(toolbox::fsm::FiniteStateMachine&)");
        XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_xdtt, fecexception_handler);
        this->notifyQualified("fatal", f);
        isfailure = true;
    }
    catch (std::exception &e) {
        std::string const msg_error_xdt = "Reconfigure failed with exception: " + string(e.what());
        LOG4CPLUS_ERROR(sv_logger_, msg_error_xdt);
        std::exception *error_ptr = &e;
        pixel::PixelTKFECSupervisorException *new_exception = dynamic_cast<pixel::PixelTKFECSupervisorException *>(error_ptr);
        XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_xdt, *new_exception);
        this->notifyQualified("fatal", f);
        isfailure = true;
    }

    if (isfailure) {
        try {
            XCEPT_RAISE(xcept::Exception, "Failed to reconfigure TKFEC");
        }
        catch (toolbox::fsm::exception::Exception &e2) {
            std::string const msg_fatal_gqcc = "Failed to transition to Failed state!";
            LOG4CPLUS_FATAL(sv_logger_, msg_fatal_gqcc);
            XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_fatal_gqcc, e2);
            this->notifyQualified("fatal", f);
        }
    }

    // Start workloops
    if (theCalibObject_ == 0) {
        LOG4CPLUS_INFO(sv_logger_, "PixelTKFECSupervisor::reconfigureActionImpl" );
        start_workloops();
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::reconfigureActionImpl(event);
    return;
}

void pixel::PixelTKFECSupervisor::createFecAccess(int argc, char **argv, int *cnt, unsigned int slot) {
//   fecAccess_ = FecAccess::createFecAccess (argc, argv, cnt, false);
//   if (!fecAccess_) {
//     XCEPT_RAISE(xdaq::exception::Exception,"Problem in createFecAccess");
//   }
//   setFecType (fecAccess_->getFecBusType());
}


void pixel::PixelTKFECSupervisor::portcardI2CDevice(FecAccess *fecAccess, tscType8 fecAddress, tscType8 ringAddress, tscType8 ccuAddress, tscType8 channelAddress, tscType8 deviceAddress, enumDeviceType modeType, unsigned int value) {
  std::stringstream msg_debug_portcard;
  std::stringstream msg_error_portcard;
  //msg_debug_portcard<< " In TKFECSupervisor function portcardI2CDevice: \n fecAddress: "<<(int)fecAddress<<" ringAddress: "<< (int)ringAddress<<std::hex
  //<<", ccuAddress: "<<(int)ccuAddress<<", channelAddress: "<<(int)channelAddress<<", deviceAddress: "<<(int)deviceAddress<<std::dec
  //<<", value: "<<value<<"\n";
  keyType index = buildCompleteKey(fecAddress,ringAddress,ccuAddress,channelAddress,deviceAddress);
  //msg_debug_portcard<<" Index is:"<<index<<"\n";
  //msg_debug_portcard<<" modeType is:"<<modeType<<"\n";
  //msg_debug_portcard<<" MODE_SHARE is:"<<MODE_SHARE<<"\n";
  //LOG4CPLUS_DEBUG(sv_logger_,msg_debug_portcard.str());
  try { fecAccess_->addi2cAccess(index, modeType, MODE_SHARE);}
  catch (FecExceptionHandler& e) {
    LOG4CPLUS_ERROR(sv_logger_,e.what());
    //if (errorCounterFlag_) displayStatus(&e, 0, fecAccess_, stdchan_);
    fecAccess_->removei2cAccess(index); //Test this
    throw;
  }

  //msg_debug_portcard.str("");
  //msg_debug_portcard.clear();
  try {
    //msg_debug_portcard<<"Value of the device : 0x"<<fecAccess->read(index)<<"\n";
    //msg_debug_portcard<<"Writing 0x"<<value<<" to the device...\n";
    fecAccess_->write(index, value);
    //msg_debug_portcard << "Check... => new value of the device : 0x"<<fecAccess_->read(index)<<"\n";
    //LOG4CPLUS_DEBUG(sv_logger_,msg_debug_portcard.str());
  }
  catch (FecExceptionHandler& e) {
    LOG4CPLUS_ERROR(sv_logger_,e.what());
    //if (errorCounterFlag_)displayStatus(&e,0,fecAccess_,stdchan_);
    if (e.getFecRingRegisters() != NULL) FecRingRegisters::displayAllRegisters ( *(e.getFecRingRegisters()) );

    // Original frame
    if (e.getFAck() != NULL) LOG4CPLUS_ERROR(sv_logger_, FecRingRegisters::decodeFrame(e.getFAck()) ); //not sure if this works yet
    if (e.getDAck() != NULL) LOG4CPLUS_ERROR(sv_logger_, FecRingRegisters::decodeFrame(e.getDAck()) );

    fecAccess_->removei2cAccess(index);
    throw;
  }
  fecAccess_->removei2cAccess(index);
}

int pixel::PixelTKFECSupervisor::portcardI2CDeviceRead(FecAccess *fecAccess, tscType8 fecAddress, tscType8 ringAddress, tscType8 ccuAddress, tscType8 channelAddress, tscType8 deviceAddress, enumDeviceType modeType) {
  int value=-1;
  std::stringstream msg_debug_portcardRead;
  std::stringstream msg_error_portcardRead;
  keyType index = buildCompleteKey(fecAddress,ringAddress,ccuAddress,channelAddress,deviceAddress);
  try { fecAccess_->addi2cAccess (index, modeType,MODE_SHARE);}
  catch (FecExceptionHandler& e) {
    msg_error_portcardRead<<e.what();
    //if (errorCounterFlag_) displayStatus(&e, 0, fecAccess_, stdchan_);
    msg_error_portcardRead<< "ERROR IN: fecAddress, ringAddress, ccuAddress, channelAddress, deviceAddress, value:"
    <<(int)fecAddress<<" "<<(int)ringAddress<<std::hex<<" "<<(int)ccuAddress<<" "
    <<(int)channelAddress<<" "<<(int)deviceAddress<<std::dec<<" "<<value<<"\n";

    LOG4CPLUS_ERROR(sv_logger_,msg_error_portcardRead.str());
    fecAccess_->removei2cAccess(index); //Test
    throw;
  }
  msg_error_portcardRead.str("");
  msg_error_portcardRead.clear();
  try{
    value=fecAccess_->read(index);
  }
  catch (FecExceptionHandler& e) {
    msg_error_portcardRead<<e.what();
    //if (errorCounterFlag_) displayStatus(&e,0,fecAccess_,stdchan_);
    if (e.getFecRingRegisters() != NULL) FecRingRegisters::displayAllRegisters( *(e.getFecRingRegisters()) ); // display all registers

    // Original frame
    if (e.getFAck() != NULL) msg_error_portcardRead<<FecRingRegisters::decodeFrame(e.getFAck())<<"\n";
    if (e.getDAck() != NULL) msg_error_portcardRead<<FecRingRegisters::decodeFrame(e.getDAck())<<"\n";

    msg_error_portcardRead<< "ERROR IN: fecAddress, ringAddress, ccuAddress, channelAddress, deviceAddress, value:"
    <<(int)fecAddress<<" "<<(int)ringAddress<<std::hex<<" "<<(int)ccuAddress<<" "
    <<(int)channelAddress<<" "<<(int)deviceAddress<<std::dec<<" "<<value<<std::endl;

    LOG4CPLUS_ERROR(sv_logger_,msg_error_portcardRead.str());
    fecAccess_->removei2cAccess(index); //To be tested
    throw;
  }
  fecAccess_->removei2cAccess(index);
  if (value == -1) XCEPT_RAISE(xdaq::exception::Exception,"Problem in portcardI2CDeviceRead. Value is -1");
  return value;
}

//Rewrite?
void pixel::PixelTKFECSupervisor::cleanupGlobalConfigData() {
  std::map<std::string, pos::PixelPortCardConfig*>::iterator iter = mapNamePortCard_.begin();

  for (;iter!=mapNamePortCard_.end();++iter) delete iter->second;

  mapNamePortCard_.clear();

  delete thePortcardMap_; thePortcardMap_=0;
  delete theGlobalKey_; theGlobalKey_=0;
  delete theTKFECConfiguration_; theTKFECConfiguration_=0;
  delete theCalibObject_; theCalibObject_=0;
  delete theNameTranslation_; theNameTranslation_=0;
  delete theFECConfiguration_; theFECConfiguration_=0;
  delete theGlobalDelay25_; theGlobalDelay25_=0;
}


bool pixel::PixelTKFECSupervisor::DCDCCommand(int slot, int ring, int ccu, std::string dcdcname, int updatemode, int retries) {
  bool success = true; bool checkpg = false;
  std::pair< int , std::bitset<8> > enaddress = theDCDCConverterConfig_->getEnChannelAndBit(dcdcname);
  std::pair< int , std::bitset<8> > pgaddress = theDCDCConverterConfig_->getPgChannelAndBit(dcdcname);
  std::string ccuconfiguration = theDCDCConverterConfig_->getConfiguration(dcdcname);
  std::bitset<8> pgpattern = theDCDCConverterConfig_->getAllPgBits(ccuconfiguration,pgaddress.first);
  std::bitset<8> enpattern = theDCDCConverterConfig_->getAllEnBits(ccuconfiguration,enaddress.first);

  std::stringstream msg_dcdcinfo;
  msg_dcdcinfo << "\nDCDCCommand(): \n";
  msg_dcdcinfo << dcdcname << " " << "updated to ";
  updatemode>0 ? msg_dcdcinfo<<"on\n" : msg_dcdcinfo<<"off\n";

  msg_dcdcinfo << "slot: " << slot << ", ring: " << ring << ", ccu: 0x" <<std::hex<< ccu <<std::dec<< "\n";
  msg_dcdcinfo << "EN: piachannel: 0x" <<std::hex<< enaddress.first <<std::dec<< ", bit: " << enaddress.second.to_string() << "\n";
  msg_dcdcinfo << "EN: pattern: " << enpattern.to_string() << "\n";
  msg_dcdcinfo << "PG: piachannel: 0x" <<std::hex<< pgaddress.first <<std::dec<< ", bit: " << pgaddress.second.to_string() << "\n";
  msg_dcdcinfo << "PG: pattern: " << pgpattern.to_string() << "\n";
  LOG4CPLUS_INFO(sv_logger_, msg_dcdcinfo.str());

  PiaChannelAccess* piaChannelAccessEn = NULL;
  PiaChannelAccess* piaChannelAccessPg = NULL;

  try {
    piaChannelAccessEn = new PiaChannelAccess(fecAccess_,buildCompleteKey(slot, ring, ccu, enaddress.first, 0)); //create pia access
    if (enaddress.first!=pgaddress.first) piaChannelAccessPg = new PiaChannelAccess(fecAccess_,buildCompleteKey(slot, ring, ccu, pgaddress.first, 0)); //create pia access
    else piaChannelAccessPg = piaChannelAccessEn;

    keyType enableKey=piaChannelAccessEn->getKey();
    keyType pgoodKey=piaChannelAccessPg->getKey();

    unsigned int pgbits   = pgaddress.second.to_ulong();
    unsigned int allpgbits   = pgpattern.to_ulong();
    unsigned int invallpgbits = 0xFF ^ allpgbits;

    unsigned int enbits   = enaddress.second.to_ulong();
    unsigned int invenbits = 0xFF ^ enbits;
    unsigned int allenbits   = enpattern.to_ulong();
    unsigned int enpattern = allenbits & invenbits;

    if (checkpg) {
      std::stringstream msg_pgstatus;
      fecAccess_->setPiaChannelDDR(pgoodKey, invallpgbits); //Set just the two pins we want to input for pgood

      //Read the pgood bit to check state before doing anything
      unsigned int initPgoodVal = fecAccess_->getPiaChannelDataReg(pgoodKey);
      msg_pgstatus << "\nInitial pia pg channel state = 0x" <<std::hex<< initPgoodVal <<std::dec<<"\n";
      bool initPgood = ((initPgoodVal & pgbits) == pgbits);
      msg_pgstatus << "Initial pgoodVal for DCDC converter: "<<(initPgood ? "PGOOD\n" : "NOT PGOOD\n");
      LOG4CPLUS_INFO(sv_logger_, msg_pgstatus.str());
    }

    std::stringstream msg_enstatus;
    fecAccess_->setPiaChannelDDR(enableKey, allenbits);
    if(short_sleep)
        usleep(1);
    else
        usleep(5000);
    unsigned int initEnableVal = fecAccess_->getPiaChannelDataReg(enableKey); // JMTBAD the two lines below ere using the pgood values???
    if (updatemode>0) { // update to ON
      msg_enstatus << "\ninitial data register value: " <<std::hex<< (int)fecAccess_->getPiaChannelDataReg(enableKey) <<std::dec<< "\n";
      msg_enstatus << "set data register value:     " <<std::hex<< (int)(initEnableVal & enpattern) <<std::dec<< "\n";
      fecAccess_->setPiaChannelDataReg(enableKey, initEnableVal & enpattern);
      msg_enstatus << "final data register value:   " <<std::hex<< (int)fecAccess_->getPiaChannelDataReg(enableKey) <<std::dec<< "\n";
    }
    else if (updatemode<0) { // update to OFF
      msg_enstatus<< "\ninitial data register value: " <<std::hex<< (int)fecAccess_->getPiaChannelDataReg(enableKey) <<std::dec<< "\n";
      msg_enstatus<< "set data register value:     " <<std::hex<< (int)((initEnableVal | ~enpattern) & allenbits) <<std::dec<< "\n";
      fecAccess_->setPiaChannelDataReg(enableKey, (initEnableVal | ~enpattern) & allenbits);
      msg_enstatus<< "final data register value:   " <<std::hex<< (int)fecAccess_->getPiaChannelDataReg(enableKey) <<std::dec<< "\n";
    }
    else msg_enstatus << "DCDCCommand: nothing happened!\n";
    LOG4CPLUS_INFO(sv_logger_, msg_enstatus.str());

    if (checkpg) {
      std::stringstream msg_pgstatus;
      if(short_sleep)
        usleep(1); // Sleep 5 ms before reading back the pgood bit
      else
        usleep(5000); // Sleep 5 ms before reading back the pgood bit

      // Read back the pgood bit and report status
      unsigned int pgoodVal = fecAccess_->getPiaChannelDataReg(pgoodKey);
      msg_pgstatus << "\npia pg channel state = 0x" <<std::hex<< pgoodVal <<std::dec<<"\n";
      bool pgood = ((pgoodVal & pgbits) == pgbits);
      msg_pgstatus<< "pgoodVal = 0x" <<std::hex<< pgoodVal <<std::dec<< (pgood ? " = PGOOD!\n" : " = NOT PGOOD\n");
      LOG4CPLUS_INFO(sv_logger_, msg_pgstatus.str());
    }

    delete(piaChannelAccessEn);
    if (enaddress.first!=pgaddress.first) delete(piaChannelAccessPg);
  }

  catch (FecExceptionHandler& e) {
    LOG4CPLUS_ERROR(sv_logger_, "Exception caught when doing PIA access:\n" + e.what());
    LOG4CPLUS_INFO(sv_logger_, "will retry");

    if (piaChannelAccessEn) delete(piaChannelAccessEn);
    if (piaChannelAccessPg) delete(piaChannelAccessPg);

    if (retries>0) {
      resetPlxFec(fecAccess_, slot, ring, 1, 0);
      return DCDCCommand(slot, ring, ccu, dcdcname, updatemode, --retries);
    }
    success = false;
  }
  LOG4CPLUS_INFO(sv_logger_, "status dcdc "+pixel::utils::to_string(success)+"\n");
  return success;
}


bool  pixel::PixelTKFECSupervisor::programPortcards(bool checkReadBack, bool programFixSoftErrorSubset) {

  pixel::utils::Timer totalTimer,i2cTimer, resetTimer, readTimer;
  std::stringstream msg_programPortcards;
  std::stringstream msg_programPortcards_error;
  std::stringstream msg_programPortcards_debug;

  if (extratimers_) {
    totalTimer.setName("Total");
    i2cTimer.setName("i2write");
    resetTimer.setName("resets");
    readTimer.setName("i2readback");
  }
  const long loop = 5;         // number of resets FIXME ATANU 1 to 5
  const unsigned long tms = 50000; // wait tms microseconds after reset FIXME ATANU 0 to 50000

  unsigned int ring =9999;
  static bool ringInit[9] = {false,false,false,false,false,false,false,false,false}; // has the ring been reset
  enumDeviceType modeType = PHILIPS;
  //bool problem = false;  // return true if  readback != programmed value
  bool isFailure = false; // or if we catch an exception programming

  totalTimer.start();
  msg_programPortcards<<"Number of portcards to configure: "<<std::dec<< mapNamePortCard_.size()<<std::endl;
  INFO(msg_programPortcards.str());

  //loop over portcards
  std::map<std::string, pos::PixelPortCardConfig*>::const_iterator iportcard=mapNamePortCard_.begin();
  for (; iportcard != mapNamePortCard_.end(); ++iportcard) {
    //proceed only with problematic portcards if we are in error recovery
    if (programFixSoftErrorSubset && fixPortcards_.find(iportcard->first)==fixPortcards_.end() ) continue;

    //since we've already built mapNamePortCard to only have the portcards on this tkfec, then this better not fail!
    pos::PixelPortCardConfig* tempPortCard = iportcard->second;
    const std::string TKFECID = tempPortCard->getTKFECID();
    if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_)
      XCEPT_RAISE(xdaq::exception::Exception,"Problem in programPortcards: crate");
    const unsigned int TKFECAddress = theTKFECConfiguration_->addressFromTKFECID(TKFECID);

    // Try the PLX reset , do it only once per ring (or should we do it for each portcard?)
    ring = tempPortCard->getringAddress();
    if ( !ringInit[ring]) {  // rings go from 1-8
      INFO("Reset slot/mfec "+pixel::utils::to_string(TKFECAddress)+"/"+pixel::utils::to_string(ring)+"\n");
      ringInit[ring]=true;  //comment this to get one reset per portcard programming
    }

    INFO("Will now configure portcard:"+tempPortCard->getPortCardName()+"\n");

    for (int i=-5;i<(signed int)(tempPortCard->getdevicesize());i++) {  // num of devices
      LOG4CPLUS_DEBUG(sv_logger_, "Device size "+pixel::utils::to_string(tempPortCard->getdevicesize())+"\n");
      unsigned int deviceAddress;
      unsigned int value;
      if(short_sleep)
        usleep(1); //necessary? kme 07.07.17 was 10000! dominates programming time.
      else
        usleep(5000); // Sleep 5 ms before reading back the pgood bit


      //Rewrite?
      // 5 initialization settings, and then the settings from the configuration file.
      if (i==-5) {
        deviceAddress = tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_DOH_Dummy); // turn off unused laser
        value = 0x0;
      }
      else if (i==-4) {
        deviceAddress = tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_PLL_CTR1);
        value = 0x8;
      }
      else if (i==-3) {
        deviceAddress = tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_PLL_CTR1);
        value = 0x0;
      }
      else if (i==-2) {
        deviceAddress = tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_PLL_CTR2); // set PLL_CTR4or5 to PLL_CTR5
        value = 0x20;
      }
      else if (i==-1) {
        deviceAddress = tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_PLL_CTR4or5);
        value = 0x0;
      }

      else {
        deviceAddress=tempPortCard->getdeviceAddress(i);
        value=tempPortCard->getdeviceValues(i);

        if (theGlobalDelay25_!=0 && theCalibObject_==0) { // This must be a Physics Run
          // SCL and TRG do not wrap around, if the desired value is out of range, no global delay is applied
          // i.e. getDelay(value) just returns the original 'value'
          if ( (deviceAddress==tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_Delay25_SCL)) ||
            (deviceAddress==tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_Delay25_TRG)) ) {
            std::bitset<7> stripedvalue_bitset (value);
            stripedvalue_bitset.set(6,0);

            INFO("Delay for device "+tempPortCard->getPortCardName()+" i: "+pixel::utils::to_string(i)+" value "+pixel::utils::to_string(value)+" value std::hex "+pixel::utils::hex_to_string(value)+" striped value "+pixel::utils::to_string(stripedvalue_bitset.to_ulong())+" striped value std::hex "+pixel::utils::hex_to_string(stripedvalue_bitset.to_ulong()));
            value=theGlobalDelay25_->getDelay(value);
            stripedvalue_bitset=value;
            stripedvalue_bitset.set(6,0);
            INFO("Delay after adding global for device "+tempPortCard->getPortCardName()+" i: "+pixel::utils::to_string(i)+" value "+pixel::utils::to_string(value)+" value std::hex "+pixel::utils::hex_to_string(value)+" striped value "+pixel::utils::to_string(stripedvalue_bitset.to_ulong())+" striped value std::hex "+pixel::utils::hex_to_string(stripedvalue_bitset.to_ulong()));
          }

          LOG4CPLUS_DEBUG(sv_logger_,"Check SDA\n");
          // SDA wraps around, it is always applied regardless if SCL/TRG were changed or not
          if (deviceAddress==tempPortCard->getdeviceAddressForSetting(pos::PortCardSettingNames::k_Delay25_SDA)) value=theGlobalDelay25_->getCyclicDelay(value);
        }
      }

      msg_programPortcards_debug<< "fecAddress, ringAddress, ccuAddress, channelAddress, deviceAddress, value: "
      <<(int)TKFECAddress<<" "<<(int)tempPortCard->getringAddress()<<std::hex<<" "<<(int)tempPortCard->getccuAddress()<<" "
      <<(int)tempPortCard->getchannelAddress()<<" "<<(int)deviceAddress<<std::dec<<" \n";
      LOG4CPLUS_DEBUG(sv_logger_,msg_programPortcards_debug.str());

      msg_programPortcards.str("");
      msg_programPortcards.clear();

      i2cTimer.start();
      if (!(TKFECID == "tkfec_bpix_BpI" && (int)TKFECAddress == 0x0 && (int)tempPortCard->getringAddress() == 0x3 && (int)tempPortCard->getccuAddress() == 0x77 && (int)tempPortCard->getchannelAddress() == 0x11 && ((int)deviceAddress == 0x40 || (int)deviceAddress == 0x41 || (int)deviceAddress == 0x43))){
      try{
	std::cout << "MY UNIQUE STRING fecAddress, ringAddress, ccuAddress, channelAddress, deviceAddress, value: " <<(int)TKFECAddress<<" "<<(int)tempPortCard->getringAddress()<<std::hex<<" "<<(int)tempPortCard->getccuAddress()<<" " <<(int)tempPortCard->getchannelAddress()<<" "<<(int)deviceAddress<<std::dec<<std::endl;

	if(tempPortCard->getccuAddress() == 0x77)
	  std::cout << "ciao" << std::endl;

	portcardI2CDevice(fecAccess_,TKFECAddress,tempPortCard->getringAddress(),tempPortCard->getccuAddress(),tempPortCard->getchannelAddress(),deviceAddress,modeType,value);

      }
      catch (FecExceptionHandler& e) {
        msg_programPortcards_error<<std::hex
        <<"\nring address = "<<tempPortCard->getringAddress()
        <<"\nCCU address  = "<<tempPortCard->getccuAddress()
        <<"\nChannel address = "<<tempPortCard->getchannelAddress()
        <<"\ndevice address  = "<<deviceAddress
        <<"\nvalue = "<<value<<std::dec<<std::endl;
        ERROR(msg_programPortcards_error.str());
        INFO("Resetting ring for a retry...\n");
        resetPlxFec(fecAccess_, TKFECAddress, ring, loop, tms); //try to recover with a reset
        if(short_sleep)
          usleep(10); //appears to be necessary for reprogramming to work. min time = ?
        else
          usleep(5000); //appears to be necessary for reprogramming to work. min time = ?
        try { // retry the programming ===
          portcardI2CDevice(fecAccess_,TKFECAddress,tempPortCard->getringAddress(),tempPortCard->getccuAddress(),tempPortCard->getchannelAddress(),deviceAddress,modeType,value);
          INFO("...worked this time!\n");
        }
        catch (FecExceptionHandler & e2) { // failed yet again
          ERROR("...retry failed! Giving up on this portcardI2CDevice() call\n");
          resetPlxFec ( fecAccess_, TKFECAddress, ring, loop, tms ); //try to recover with a reset
          isFailure = true;
        }
        catch (...) {
          ERROR("Caught an unknown exception in pixel::PixelTKFECSupervisor::programPortcard during retry after reset!!\n");
          isFailure=true;
        } //nested try-catch for retry ===
      }
      catch (...) {
        ERROR("Caught an unknown exception in pixel::PixelTKFECSupervisor::programPortcard!\n");
        isFailure = true;
      }
      }
      i2cTimer.stop();
      readTimer.start();

      msg_programPortcards_error.str("");
      msg_programPortcards_error.clear();

      if (checkReadBack) { // Now readback
        unsigned int readBack=0;
	if (!(TKFECID == "tkfec_bpix_BpI" && (int)TKFECAddress == 0x0 && (int)tempPortCard->getringAddress() == 0x3 && (int)tempPortCard->getccuAddress() == 0x77 && (int)tempPortCard->getchannelAddress() == 0x11 && ((int)deviceAddress == 0x40 || (int)deviceAddress == 0x41 || (int)deviceAddress == 0x43))){
        try{
          readBack = portcardI2CDeviceRead(fecAccess_,TKFECAddress,tempPortCard->getringAddress(),tempPortCard->getccuAddress(),tempPortCard->getchannelAddress(),deviceAddress,modeType);
        }
        catch (FecExceptionHandler& e) {
          msg_programPortcards_error<<std::hex
          <<"\nring address = "<<tempPortCard->getringAddress()
          <<"\nCCU address = "<<tempPortCard->getccuAddress()
          <<"\nChannel address = "<<tempPortCard->getchannelAddress()
          <<"\ndevice address = "<<deviceAddress<<std::dec
          <<"\nvalue = "<<value<<std::endl;
          msg_programPortcards_error<<"Resetting ring and retry...\n";
          ERROR( msg_programPortcards_error.str());
          resetPlxFec(fecAccess_, TKFECAddress, ring, loop, tms); //try to recover with a reset
          if(short_sleep)
            usleep(10); //appears to be necessary for reprogramming to work. min time = ?
          else
            usleep(5000); //appears to be necessary for reprogramming to work. min time = ?
          try { // retry the readback ===
            readBack = portcardI2CDeviceRead(fecAccess_,TKFECAddress,tempPortCard->getringAddress(),tempPortCard->getccuAddress(),tempPortCard->getchannelAddress(),deviceAddress,modeType);
            INFO("...worked this time!\n");
          }
          catch (FecExceptionHandler& e2) { // failed yet again
            ERROR("...retry failed! Giving up on this portcardI2CDeviceRead() call\n");
            resetPlxFec(fecAccess_, TKFECAddress, ring, loop, tms); //try to recover with a reset
            isFailure = true; //can't rethrow; set a flag and return
          }
          catch (...) {
            ERROR("Caught an unknown exception in pixel::PixelTKFECSupervisor::programPortcard during readback retry after reset!!\n");
            isFailure=true;
          } //nested try-catch for retry ===
        }
        catch (...) {
          ERROR("Caught an unknown exception in pixel::PixelTKFECSupervisor::programPortcard!\n");
          isFailure = true;
        }
	}
        // check readback value to see it matches
        bool problem = (value!=readBack);

        // here we ignore PLL CTR1 because some bits are "write-only" and will always read 0 and DELAY25 GCR where the reset bit reads always 0
        if ( deviceAddress == tempPortCard->getdeviceAddressForSetting("PLL_CTR1") || deviceAddress == tempPortCard->getdeviceAddressForSetting("Delay25_GCR") ) problem = false; //Has to be a better way to streamline this

        msg_programPortcards.str("");
        msg_programPortcards.clear();
        if (problem) {
          ERROR(" Programming ERROR! Data read back != desired value.\n");

          msg_programPortcards<< "ring address=0x"<<std::hex<<tempPortCard->getringAddress()<<std::dec;
          msg_programPortcards<< " ccu address=0x" <<std::hex<< tempPortCard->getccuAddress()
          << " chan=0x" << tempPortCard->getchannelAddress()<<std::dec;
          msg_programPortcards<< " deviceAddress = 0x" <<std::hex<< deviceAddress << ", value = 0x" << value <<std::dec;
          msg_programPortcards<< "\nread back = 0x " <<std::hex<< readBack <<std::dec<<std::endl;

          INFO( msg_programPortcards.str());
        }  //end of problem or printout
      }// if readback
      readTimer.stop();
    } // loop over devices
    if (extratimers_) GlobalTimer_.printTime("PixelTKFECSupervisor::configureActionImpl -- portcard done");
  } // portcard loop

  totalTimer.stop();
  if (extratimers_) {
    std::stringstream timer_stats;
    timer_stats << totalTimer.printStats() << resetTimer.printStats() << i2cTimer.printStats() << readTimer.printStats();
    INFO( timer_stats.str());
  }
  return (isFailure);
}


bool  pixel::PixelTKFECSupervisor::powercycleDCDCs() {
  bool returnstatus=true;
  std::map< std::pair< std::string, std::string>, unsigned int> slots;
  std::map< std::pair< std::string, std::string>, unsigned int> rings;
  std::map< std::pair< std::string, std::string>, unsigned int> ccus;
  std::map< std::pair< std::string, std::string>, std::vector<std::string> > modules;
  std::set<std::string>::const_iterator module = fixModules_.begin();
  for (;module!=fixModules_.end();module++) {
    pos::PixelModuleName mod(*module);
    if (mod.modulename().find("FPix") != std::string::npos) return (returnstatus);

    std::string portcard = *(thePortcardMap_->portcards(mod).begin()); // take the first portcard - there should anyhow only be one
    pos::PixelPortCardConfig* tempPortCard = 0;
    PixelConfigInterface::get(tempPortCard, "pixel/portcard/" + portcard, *theGlobalKey_);
    const std::string& TKFECID = tempPortCard->getTKFECID();
    unsigned int slot = theTKFECConfiguration_->addressFromTKFECID(TKFECID);
    unsigned int ring = tempPortCard->getringAddress();
    unsigned int ccu = tempPortCard->getccuAddress();
    std::pair< std::string, std::string > dcdcpgname = thePowerMap_->dcdcAndPowergroup(mod);
    if (ccus.find(dcdcpgname)==ccus.end()) {
      ccus[dcdcpgname]=ccu;
      rings[dcdcpgname]=ring;
      slots[dcdcpgname]=slot;
      modules[dcdcpgname]=std::vector<std::string>();
      modules[dcdcpgname].push_back(*module);
    }
    else modules[dcdcpgname].push_back(*module);
  }

  std::map< std::pair< std::string, std::string>, unsigned int>::const_iterator converter=ccus.begin();
  for (;converter!=ccus.end();converter++) {
    std::pair< std::string, std::string> dcdcpgname = converter->first;
    unsigned char ccu = converter->second;
    unsigned char slot = slots.find(dcdcpgname)->second;
    unsigned char ring = rings.find(dcdcpgname)->second;
    std::stringstream msg;
    msg << "powercycleDCDCs(): powercycling DCDC converter for the following modules: ";
    std::vector<std::string>::const_iterator m = modules.find(dcdcpgname)->second.begin();
    std::vector<std::string>::const_iterator mend = modules.find(dcdcpgname)->second.end();
    for (; m!=mend; m++) msg << *m  << " ";
    LOG4CPLUS_INFO(sv_logger_, msg.str());

    try{
      resetPlxFec(fecAccess_, slot, ring, 1, 0);
    }
    catch (FecExceptionHandler& e) {
      LOG4CPLUS_ERROR(sv_logger_, "Exception caught when doing PIA access:\n"+e.what());
    }
    catch (...) {
      LOG4CPLUS_ERROR(sv_logger_, "An unknown exception occurred during PIA access.");
    }

    if(!short_sleep)
      usleep(10000);
    returnstatus=returnstatus && DCDCCommand(slot, ring, ccu, dcdcpgname.first, -1, 2);
    if(!short_sleep)
      sleep(2);
    returnstatus=returnstatus && DCDCCommand(slot, ring, ccu, dcdcpgname.first, 1, 0);
  }
  return(returnstatus);
}


void pixel::PixelTKFECSupervisor::onOffPIAchannels(int slot,int ring,int ccu, bool enable) {
  std::stringstream msg;
  keyType index = buildCompleteKey(slot,ring,ccu,0,0);
  int currentval = fecAccess_->getCcuCRE(index);
  msg<<"current CCU CRE "<<currentval<<"\n";
  int newval = currentval & 0xF0FFFF; // diable all PIA channels
  if (enable) newval = currentval | 0x0F0000; //enable
  msg<<"new CCU CRE "<<newval<<"\n";
  LOG4CPLUS_DEBUG(sv_logger_, msg.str());
  fecAccess_->setCcuCRE (index, newval);
}



xoap::MessageReference pixel::PixelTKFECSupervisor::PIAReset (xoap::MessageReference msg) {
  std::stringstream omsg;
  xoap::MessageReference reply = makeCommandSOAPReply(msg,"PIAResetDone","PIAReset");


  ////Attribute_Vector parameters(1, Attribute());
  ////parameters.at(0).name_="resetParam";
  ////pixel::utils::soap::Receive(msg,parameters);
  ////std::string resetParam_ = parameters.at(0).value_;
  std::vector<std::string> parameterNames (1, "resetParam");
  std::string resetParam_ = getSomeSOAPCommandAttributes(msg, parameterNames)["resetParam"];
  //std::string resetParam_ = pixel::utils::soap::extractSOAPCommandParameterString(msg,"resetParam");

  long loop = 1;
  unsigned long tms  = 0;
  unsigned int piaResetValue = 0xFF;
  unsigned int piaChannel    = 0x30;

  PixelConfigInterface::get(theTKFECConfiguration_, "pixel/tkfecconfig/", *theGlobalKey_);
  if (theTKFECConfiguration_==0) XCEPT_RAISE(xdaq::exception::Exception,"Problem in PIAReset: theTKFECConfiguration_==0");
  PixelConfigInterface::get(thePortcardMap_,"pixel/portcardmap/", *theGlobalKey_);
  if (theGlobalKey_==0) XCEPT_RAISE(xdaq::exception::Exception,"Problem in PIAReset: theGlobalKey_==0");
  pos::PixelDetectorConfig* detconfig=0; // temporary, used only to build ROC and module lists
  PixelConfigInterface::get(detconfig, "pixel/detconfig/", *theGlobalKey_);
  if (detconfig==0) XCEPT_RAISE(xdaq::exception::Exception,"Problem in PIAReset: detconfig==0");
  const std::set<std::string>& portcards=thePortcardMap_->portcards(detconfig);
  delete detconfig;

  std::set<std::pair<unsigned int,unsigned int> > ccu_ring_set;
  std::set<std::string>::const_iterator iportcard=portcards.begin();
  for (iportcard=portcards.begin();iportcard!=portcards.end();iportcard++) {
    pos::PixelPortCardConfig* tempPortCard=0;
    PixelConfigInterface::get(tempPortCard,"pixel/portcard/"+*iportcard, *theGlobalKey_);
    if (tempPortCard==0) XCEPT_RAISE(xdaq::exception::Exception,"Problem in PIAReset: tempPortCard==0");

    const std::string TKFECID = tempPortCard->getTKFECID();
    if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_) continue;
    const unsigned int TKFECAddress = theTKFECConfiguration_->addressFromTKFECID(TKFECID);
    unsigned int ringAddress = tempPortCard->getringAddress();
    unsigned int ccuAddress = tempPortCard->getccuAddress();

    std::pair<unsigned int,unsigned int> ccu_ring = std::make_pair(ccuAddress,ringAddress);
    if (ccu_ring_set.find(ccu_ring) != ccu_ring_set.end()) continue;
    ccu_ring_set.insert(ccu_ring);

    onOffPIAchannels(TKFECAddress,ringAddress,ccuAddress, true);

    //Do Pia Resets
    omsg<<"fecaccess = "<<fecAccess_<<"; PORTCARD = "<<*iportcard<<"; TKFECAddress = "<<TKFECAddress<<"; ringAddress = "<<ringAddress<<"; ccuAddress = "<<ccuAddress<<std::endl;
    omsg<<"resetParam_ = "<<resetParam_<<"\n";
    LOG4CPLUS_INFO(sv_logger_,omsg.str());
    if (iportcard->find("BPix") == std::string::npos && resetParam_ =="all" ) return reply;
    if (resetParam_ =="all") piaResetValue = 0xFF;
    else if (resetParam_ =="roc") piaResetValue = 0x1;  //BPIX
    else if (resetParam_ =="aoh") piaResetValue = 0x2;  //BPIX
    else if (resetParam_ =="doh") piaResetValue = 0x4;
    else if (resetParam_ =="res1") piaResetValue = 0x8;
    else if (resetParam_ =="res2") piaResetValue = 0x10;
    else if (resetParam_ =="fpixroc") piaResetValue = 0x2A; //FPIX 0x2A bits 1, 3, 5
    else if (resetParam_ =="fpixdevice") piaResetValue = 0x15; //FPIX 0x15 bits 0, 2, 4 : to AOH, DOH, Delay25, DCU

    testPIAResetfunctions ( fecAccess_, TKFECAddress, ringAddress, ccuAddress, piaChannel, piaResetValue, 1, 10000, loop, tms );
    onOffPIAchannels(TKFECAddress,ringAddress,ccuAddress,false);
  }
  return reply;
}

xoap::MessageReference pixel::PixelTKFECSupervisor::TKFECCalibrations(xoap::MessageReference msg) {
  ////xoap::MessageReference reply;
  return theTKFECCalibrationBase_->execute(msg);
  ////reply = pixel::utils::soap::makeCommandSOAPReply(msg);
  ////return reply;
}

xoap::MessageReference pixel::PixelTKFECSupervisor::beginCalibration(xoap::MessageReference msg) {
  if (theTKFECCalibrationBase_!=0) return theTKFECCalibrationBase_->beginCalibration(msg);
  else ERROR("No TKFECCalibarationBase");
  return makeSOAPMessageReference("pixel::PixelTKFECSupervisor::beginCalibration Default");
}

xoap::MessageReference pixel::PixelTKFECSupervisor::endCalibration(xoap::MessageReference msg) {
  if (theTKFECCalibrationBase_!=0) return theTKFECCalibrationBase_->endCalibration(msg);
  return makeSOAPMessageReference("pixel::PixelTKFECSupervisor::endCalibration Default");
}

//Use more often
bool pixel::PixelTKFECSupervisor::SetPortCardSetting(const pos::PixelPortCardConfig* portCardConfig, unsigned int deviceAddress, unsigned int settingValue) {
  if (fecAccess_==0) XCEPT_RAISE(xdaq::exception::Exception,"Problem in SetPortCardSetting: fecAccess_==0");
  const std::string TKFECID = portCardConfig->getTKFECID();
  if ( theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_ ) return false;
  const unsigned int TKFECAddress = theTKFECConfiguration_->addressFromTKFECID(TKFECID);
  enumDeviceType modeType = PHILIPS;
  portcardI2CDevice(fecAccess_, TKFECAddress, portCardConfig->getringAddress(), portCardConfig->getccuAddress(), portCardConfig->getchannelAddress(), deviceAddress, modeType, settingValue);
  return true;
}


//Rewrite eventually
/*
void pixel::PixelTKFECSupervisor::FPixDCDCSummary(xgi::Input* in, xgi::Output* out)  {
  *out << "<h3>DCDC and QPLL summary for crate " << crate_ << "</h3>\n";

  // slot, ring, ccu, piachannel 31-33
  unsigned ddr [1][4][4][3] = {{{{0}}}};
  unsigned data[1][4][4][3] = {{{{0}}}};
  bool ring_rd[4] = { false };
  const unsigned ccus[4] = {0x7b, 0x7c, 0x7e, 0x7d}; // order for portcards 1 2 3 4 in phi

  const unsigned slot = 0;
  for (int myring = 0; myring < 4; myring ++) {
    for (int ccu = 0; ccu < 4; ++ccu) {
      for (unsigned i = 0; i < 3; ++i) {
        keyType key = buildCompleteKey(slot, myring, ccus[ccu], (0x31 + i), 0); // is this a macro? why is compiler complaining about 0x31 + i needs parens
        try {
          fecAccess_->addPiaAccess(key, MODE_SHARE); // JMTBAD use PiaChannelAccess
          ddr [slot][myring][ccu][i] = fecAccess_->getPiaChannelDDR(key);
          data[slot][myring][ccu][i] = fecAccess_->getPiaChannelDataReg(key);
          fecAccess_->removePiaAccess(key);
        }
        catch (FecExceptionHandler& e) {LOG4CPLUS_ERROR(sv_logger_,"Exception caught when doing PIA access: "+e.what() );}
      }
    } //ccu loop
  } //ring loop
  std::map<std::string, std::pair<unsigned, unsigned> > enable_pgood;
  std::map<std::string, bool> qpll;

  for (std::map<std::string, pos::PixelPortCardConfig*>::const_iterator it = mapNamePortCard_.begin(), ite = mapNamePortCard_.end(); it != ite; ++it) {
    const std::string& pc_name = it->first;
    pos::PixelPortCardConfig* pc = it->second;
    const std::string TKFECID = pc->getTKFECID();

    if ( theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_ ) continue;

    const unsigned slot = theTKFECConfiguration_->addressFromTKFECID(TKFECID);
    const unsigned ring = pc->getringAddress();
    const unsigned ccu_addr  = pc->getccuAddress();
    int ccu = -1;
    if      (ccu_addr == 0x7b) ccu = 0;
    else if (ccu_addr == 0x7c) ccu = 1;
    else if (ccu_addr == 0x7e) ccu = 2;
    else if (ccu_addr == 0x7d) ccu = 3;
    //assert(slot == 0 && ring < 4 && ccu != -1); //Remove assert
    if (slot != 0 || ring >= 4 || ccu == -1) XCEPT_RAISE(xdaq::exception::Exception,"Problem in FPixDCDCSummary: slot, ring, or ccu");

    const int disk = pc_name[10] - '0'; // only works for "FPix_BmI_DX..."
    //assert(disk >= 1 && disk <= 3); //Remove assert
    if (disk < 1 || disk > 3) XCEPT_RAISE(xdaq::exception::Exception,"Problem in FPixDCDCSummary: disk");
    ring_rd[ring]=true;
    unsigned enable = 0;
    unsigned pgood = 0;
    if (disk == 1) {
      enable = data[slot][ring][ccu][0] & 0x3;
      pgood  = data[slot][ring][ccu][1] & 0xf;
      qpll[pc_name] = data[slot][ring][ccu][2] & 0x10;
    }
    else if (disk == 2) {
      enable = (data[slot][ring][ccu][0] >> 2) & 0x3;
      pgood  = data[slot][ring][ccu][1] >> 4;
      qpll[pc_name] = data[slot][ring][ccu][2] & 0x20;
    }
    else if (disk == 3) {
      enable = (data[slot][ring][ccu][0] >> 4) & 0x3;
      pgood  = data[slot][ring][ccu][2] & 0xf;
      qpll[pc_name] = data[slot][ring][ccu][2] & 0x40;
    }

    enable_pgood[pc_name] = std::make_pair(enable, pgood);
  }

  enum PIARegisterType{
    GoodAndBad=1,
    OnAndOff=2,
    EnableAndDisable=3
  };

  const long N_PIACHANNELS = 3;
  const long N_PIABITS = 8;

  enum PIA_CHANNELS{
    PIA0x31 = 0 ,
    PIA0x32 = 1 ,
    PIA0x33 = 2
  };

  PIARegisterType regType[ N_PIACHANNELS ][ N_PIABITS];
  regType[PIA0x31][0]=EnableAndDisable;
  regType[PIA0x31][1]=EnableAndDisable;
  regType[PIA0x31][2]=EnableAndDisable;
  regType[PIA0x31][3]=EnableAndDisable;
  regType[PIA0x31][4]=EnableAndDisable;
  regType[PIA0x31][5]=EnableAndDisable;
  regType[PIA0x31][6]=GoodAndBad;
  regType[PIA0x31][7]=GoodAndBad;

  regType[PIA0x32][0]=OnAndOff;
  regType[PIA0x32][1]=OnAndOff;
  regType[PIA0x32][2]=OnAndOff;
  regType[PIA0x32][3]=OnAndOff;
  regType[PIA0x32][4]=OnAndOff;
  regType[PIA0x32][5]=OnAndOff;
  regType[PIA0x32][6]=OnAndOff;
  regType[PIA0x32][7]=OnAndOff;

  regType[PIA0x33][0]=OnAndOff;
  regType[PIA0x33][1]=OnAndOff;
  regType[PIA0x33][2]=OnAndOff;
  regType[PIA0x33][3]=OnAndOff;
  regType[PIA0x33][4]=GoodAndBad;
  regType[PIA0x33][5]=GoodAndBad;
  regType[PIA0x33][6]=GoodAndBad;
  regType[PIA0x33][7]=GoodAndBad;

  string MessageMatrix[3][8];
  MessageMatrix[PIA0x31][0] = "J11 dcdc converter pair setting on P1";
  MessageMatrix[PIA0x31][1] = "J12 dcdc converter pair setting on P1";
  MessageMatrix[PIA0x31][2] = "J11 dcdc converter pair setting on P2";
  MessageMatrix[PIA0x31][3] = "J12 dcdc converter pair setting on P2";
  MessageMatrix[PIA0x31][4] = "J11 dcdc converter pair setting on P3";
  MessageMatrix[PIA0x31][5] = "J12 dcdc converter pair setting on P3";
  MessageMatrix[PIA0x31][6] = "always 1";
  MessageMatrix[PIA0x31][7] = "always 1";

  MessageMatrix[PIA0x32][0] = "bit 0 dcdc power_Good from P1";
  MessageMatrix[PIA0x32][1] = "bit 1 dcdc power_Good from P1";
  MessageMatrix[PIA0x32][2] = "bit 2 dcdc power_Good from P1";
  MessageMatrix[PIA0x32][3] = "bit 3 dcdc power_Good from P1";
  MessageMatrix[PIA0x32][4] = "bit 4 dcdc power_Good from P2";
  MessageMatrix[PIA0x32][5] = "bit 5 dcdc power_Good from P2";
  MessageMatrix[PIA0x32][6] = "bit 6 dcdc power_Good from P2";
  MessageMatrix[PIA0x32][7] = "bit 7 dcdc power_Good from P2";

  MessageMatrix[PIA0x33][0] = "bit 0 dcdc power_Good from P3";
  MessageMatrix[PIA0x33][1] = "bit 1 dcdc power_Good from P3";
  MessageMatrix[PIA0x33][2] = "bit 2 dcdc power_Good from P3";
  MessageMatrix[PIA0x33][3] = "bit 3 dcdc power_Good from P3";
  MessageMatrix[PIA0x33][4] = "qpll lock for P1";
  MessageMatrix[PIA0x33][5] = "qpll lock for P2";
  MessageMatrix[PIA0x33][6] = "qpll lock for P3";
  MessageMatrix[PIA0x33][7] = "always 1 in my experience but I'm not sure what it's hooked to";
  *out << "raw pia data:<br>\n";
  *out << std::hex;
  for (int slot = 0; slot < 1; ++slot) {
    for (int ring = 0; ring < 4; ++ring) {
      if (ring_rd[ring]) {
        *out << "slot " << slot << " ring " << ring << "<br>\n";
        for (int ccu = 0; ccu < 4; ++ccu) {
          *out << "ccu 0x" << ccus[ccu] << "<br>\n";
          *out << "<table><tr><td>pia</td><td>0x31</td><td>0x32</td><td>0x33</td></tr>\n";
          *out << "<tr><td>DDR</td>";
          for (int i = 0; i < N_PIACHANNELS; ++i)
          *out << "<td>" << ddr[slot][ring][ccu][i] << "</td>";
          *out << "</tr>\n";
          *out << "<tr><td>data</td>";
          for (int i = 0; i < N_PIACHANNELS; ++i)
          *out << "<td>" << data[slot][ring][ccu][i] << "</td>";
          *out << "</tr>\n";
          *out << "</table>\n";
          for (int channelnumber = 0; channelnumber < N_PIACHANNELS; channelnumber++) {
            bool *M = new bool[8];
            for (int i = 0; i < 8; i++) {
              M[i] = data[slot][ring][ccu][channelnumber] & (1 << i);
            }

            *out <<"<p>";
            for (int i = 0; i < 8; i++)
            *out << M[8-1-i];
            *out << "<br/>";
            *out << "</p>";

            *out <<"<p>";
            for (int bit = 0; bit < N_PIABITS; bit++) {
              string StatusPIA="";
              if (regType[channelnumber][bit]==OnAndOff) StatusPIA=M[bit]? "On":"Off";
              else if (regType[channelnumber][bit]==GoodAndBad) StatusPIA=M[bit]? "Good":"bad";
              else StatusPIA=M[bit]? "Disable":"Enable";
              *out << "Channel=" << channelnumber << ";bit=" << bit << ";value=" << (M[bit]) << ":" << (MessageMatrix[channelnumber][bit].c_str()) << " = "<< StatusPIA << "." << "<br/>";
            }
            *out <<"</p>";
          }
        } // end loop over ccu
      } //end if
    } // end loop over ring
  } // end loop over slot
}
*/

void pixel::PixelTKFECSupervisor::initialize_dcdc_dcu_tables(){
    uint counterccu = 1;
    uint counterportcard = 1;
    for (int i=0; i<(int)ccu_list.size(); i++){
        unsigned int slot = boost::get<0>(ccu_list[i]);
        unsigned int ring = boost::get<1>(ccu_list[i]);
        unsigned int ccu = boost::get<2>(ccu_list[i]);
        int pia_register = 0x30;
        for (int j=0; j<4; j++){
            std::vector<std::string> msg_info_ccu_config;
            if (j==0)
                msg_info_ccu_config = {pixel::utils::to_string(ring),"0x"+pixel::utils::hex_to_string(ccu),"0x"+pixel::utils::hex_to_string(pia_register),"",""};
            else
                msg_info_ccu_config = {"","","0x"+pixel::utils::hex_to_string(pia_register),"",""};
            FPixDCDCSummaryP_->displayMessage(monitor_,"itemset-log-ccu",msg_info_ccu_config,counterccu);
            pia_register += 0x1;
            counterccu+=1;
        }

        for (map<string, pos::PixelPortCardConfig *>::iterator portcard = mapNamePortCard_.begin(); portcard != mapNamePortCard_.end(); ++portcard){
            std::string portcard_name = portcard->first;
            if (portcard->first.find("BPix", 0) != std::string::npos)
                continue;
            std::string TKFECID = portcard->second->getTKFECID();
            if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_)
                continue;
            unsigned int fecSlot = theTKFECConfiguration_->addressFromTKFECID(TKFECID);
            unsigned int ringSlot = portcard->second->getringAddress();
            unsigned int ccuAddress = portcard->second->getccuAddress();
            if (fecSlot!=boost::get<0>(ccu_list[i]) || ringSlot!=boost::get<1>(ccu_list[i]) || ccuAddress!=boost::get<2>(ccu_list[i]))
                continue;
            std::vector<std::string> msg_info_dcu_config_bottom;
            msg_info_dcu_config_bottom = {portcard_name+" Bottom", "", "", "", "", "", "", "", ""};
            FPixDCDCSummaryP_->displayMessage(monitor_,"itemset-log-dcu",msg_info_dcu_config_bottom,counterportcard);
            counterportcard+=1;
            std::vector<std::string> msg_info_dcu_config_top;
            msg_info_dcu_config_top = {portcard_name+" Top", "", "", "", "", "", "", "", ""};
            FPixDCDCSummaryP_->displayMessage(monitor_,"itemset-log-dcu",msg_info_dcu_config_top,counterportcard);
            counterportcard+=1;
        }
    }

    uint counterdcdc = 1;
    for (std::map< std::pair<std::string, std::string>, boost::tuple<unsigned int, unsigned int, unsigned int> >::iterator dcdc=dcdc_list.begin(); dcdc!=dcdc_list.end(); ++dcdc){
        std::string dcdc_name = dcdc->first.first;
        std::string pg_name = dcdc->first.second;
        std::vector<std::string> msg_info_dcdc_config;
        msg_info_dcdc_config={pg_name,dcdc_name,"",""};
        FPixDCDCSummaryP_->displayMessage(monitor_,"itemset-log-dcdc",msg_info_dcdc_config,counterdcdc);
        counterdcdc+=1;
    }
}

bool pixel::PixelTKFECSupervisor::checkDCDC(int retries) {
    bool success = true;
    
    // Getting values of PIA Data Registers for all CCUs
    std::vector< std::map<int, unsigned int> > ccu_data_list;
    uint counterccu =1;
    for (int i=0; i<(int)ccu_list.size(); i++){
        unsigned int slot = boost::get<0>(ccu_list[i]);
        unsigned int ring = boost::get<1>(ccu_list[i]);
        unsigned int ccu = boost::get<2>(ccu_list[i]);
        std::vector<PiaChannelAccess*> piaChannelAccess_list;
        for (int j=0; j<4; j++)
            piaChannelAccess_list.push_back(NULL);
        try {
            // Getting PIA data values
            std::map<int, unsigned int> ccu_data;
            std::map<int, unsigned int> ccu_ddr;
            onOffPIAchannels(slot,ring,ccu, true);
            int pia_register = 0x30;
            for (int j=0; j<(int)piaChannelAccess_list.size(); j++){
                piaChannelAccess_list[j] = new PiaChannelAccess(fecAccess_,buildCompleteKey(slot, ring, ccu, pia_register, 0)); //create pia access
                keyType Key=piaChannelAccess_list[j]->getKey();
                ccu_data[pia_register] = fecAccess_->getPiaChannelDataReg(Key);
                ccu_ddr[pia_register] = fecAccess_->getPiaChannelDDR(Key);
                delete(piaChannelAccess_list[j]);
    
                std::vector<std::string> msg_info_ccu;
                //Only update the CCU and Ring if they change
                if (j==0)
                    msg_info_ccu={pixel::utils::to_string(ring),"0x"+pixel::utils::hex_to_string(ccu),"0x"+pixel::utils::hex_to_string(pia_register),"0x"+pixel::utils::hex_to_string(ccu_ddr[pia_register]),"0x"+pixel::utils::hex_to_string(ccu_data[pia_register])};		
                else
                    msg_info_ccu={"","","0x"+pixel::utils::hex_to_string(pia_register),"0x"+pixel::utils::hex_to_string(ccu_ddr[pia_register]),"0x"+pixel::utils::hex_to_string(ccu_data[pia_register])};

                FPixDCDCSummaryP_->displayMessage(monitor_,"itemset-log-ccu",msg_info_ccu,counterccu);
                pia_register += 0x1;
                counterccu+=1;
            }
            onOffPIAchannels(slot,ring,ccu, false);
            ccu_data_list.push_back(ccu_data);
        }
        catch (FecExceptionHandler & e) {
            for (int j=0; j<(int)piaChannelAccess_list.size(); j++){
                if (piaChannelAccess_list[j])
                    delete(piaChannelAccess_list[j]);
            }
            LOG4CPLUS_ERROR(sv_logger_, "Exception caught when doing PIA access:\n" + e.what());
            LOG4CPLUS_INFO(sv_logger_, "will retry with iterative ccu reset");
            if (retries>0) {
                resetPlxFec(fecAccess_, slot, ring, 5, 10);
                return checkDCDC(--retries);
            }
            success = false;
        }
    }
    uint counterdcdc = 1;
    //  Displaying status of DCDCs for which there are corresponding modules in the detconfig
    for (std::map< std::pair<std::string, std::string>, boost::tuple<unsigned int, unsigned int, unsigned int> >::iterator dcdc=dcdc_list.begin(); dcdc!=dcdc_list.end(); ++dcdc){
        std::string dcdc_name = dcdc->first.first;
        std::string pg_name = dcdc->first.second;
        unsigned int slot = boost::get<0>(dcdc->second);
        unsigned int ring = boost::get<1>(dcdc->second);
        unsigned int ccu = boost::get<2>(dcdc->second);

        std::string en_status = "EN Bit NOT READ";
        std::string pg_status = "PG Bit NOT READ";

        int ccu_index = -9999;
        for (int i=0; i<(int)ccu_list.size(); i++){
            if (slot==boost::get<0>(ccu_list[i]) && ring==boost::get<1>(ccu_list[i]) && ccu==boost::get<2>(ccu_list[i])){
                 ccu_index = i;
                break;
            }
        }
        if (success){
            if (ccu_index==-9999)
                LOG4CPLUS_INFO(sv_logger_, "CCU for DCDC doesn't match");
            else{
                if (get_pgbit_status){
                    std::pair< int , std::bitset<8> > pgaddress = theDCDCConverterConfig_->getPgChannelAndBit(dcdc_name);
                    unsigned int pgbits = pgaddress.second.to_ulong();
                    unsigned int PgoodVal = ccu_data_list[ccu_index][pgaddress.first];
                    bool Pgood = ((PgoodVal & pgbits) == pgbits);
                    if (Pgood)
                        pg_status = "PG Bit Good";
                    else
                        pg_status = "<p style=\"background-color:#fa0c1c;\">PG Bit Bad</p>";
                        //pg_status = "PG Bit Bad";
                }

                std::pair< int , std::bitset<8> > enaddress = theDCDCConverterConfig_->getEnChannelAndBit(dcdc_name);
                unsigned int enbits = enaddress.second.to_ulong();
                bool read_enable = true;
                if (read_enable){
                    unsigned int EnableVal = ccu_data_list[ccu_index][enaddress.first];
                    bool Disable = ((EnableVal & enbits) == enbits);
                    if (Disable)
                        en_status = "<p style=\"background-color:#fa0c1c;\">Disabled</p>";
                    else
                        en_status = "Enabled";
                }
            }
        }

        std::vector<std::string> msg_info_dcdc;
        msg_info_dcdc={pg_name,dcdc_name,en_status,pg_status};
        FPixDCDCSummaryP_->displayMessage(monitor_,"itemset-log-dcdc",msg_info_dcdc,counterdcdc);
        counterdcdc+=1;
    }

    return success;
}

bool
pixel::PixelTKFECSupervisor::CheckDCDC_workloop(toolbox::task::WorkLoop *w1) {
    //For giving the feds enough time to automask channels
    double sleepcheckdcdc_workloop = checkDCDC_workloop_sleep_; // in seconds
    double sleepunits = 1e5; // in microseconds
    if (timesCheckDCDCcalled < (sleepcheckdcdc_workloop * 1e6 / sleepunits)) {
        usleep(sleepunits);
        timesCheckDCDCcalled++;
        return true;
    }
    else {
        timesCheckDCDCcalled = 0;
    }

    LOG4CPLUS_INFO(sv_logger_, "ENTER CheckDCDC WorkLoop");

    int nretries = 0;
    if (doIteerativeCCUreset_)
        nretries = 2;

    dcdclock_->take();
    std::string const msg_debug_srt = "START CheckDCDC WorkLoop";
    LOG4CPLUS_INFO(sv_logger_, msg_debug_srt);
    std::string booltostr = (checkDCDC(nretries) ? "Success" : "Failed");
    std::string const msg_func_sta = "DCDC Workloop Status: " + booltostr;
    LOG4CPLUS_INFO(sv_logger_, msg_func_sta);
    if(booltostr=="Failed") LOG4CPLUS_INFO(sv_logger_, msg_func_sta);
    std::string const msg_debug_end = "END CheckDCDC WorkLoop";
    LOG4CPLUS_INFO(sv_logger_, msg_debug_end);
    dcdclock_->give();
    return true;
}

bool 
pixel::PixelTKFECSupervisor::readDCU(int retries) {
    bool success = true;

    try {
        hardwarelock_->take();

        PortCard::DCU::Mode mode = PortCard::DCU::LIR; 
        // LIR mode : refereence value for ADC is ground
        // HIR mode : reference value for ADS is VDD (~2.5V)
        // We always use LIR mode

        unsigned i2cAddress[2];
        i2cAddress[0] = 0x50; // Bottom
        i2cAddress[1] = 0x60; // Top
        std::string strBPix = "BPix";
        uint counterportcard = 1;

        for (int i=0; i<(int)ccu_list.size(); i++){  // begin CCU loop

            // Find portcards connected to this CCU only
            for (map<string, pos::PixelPortCardConfig *>::iterator portcard = mapNamePortCard_.begin(); portcard != mapNamePortCard_.end(); ++portcard){
                std::string portcard_name = portcard->first;
                // make sure this is not BPix
                if (portcard->first.find(strBPix, 0) != std::string::npos)
                    continue;

                std::string TKFECID = portcard->second->getTKFECID();
                if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_){
                    LOG4CPLUS_ERROR(sv_logger_, "TKFEDID mismatch");
                    XCEPT_RAISE(xcept::Exception, "TKFEDID mismatch");
                }

                unsigned int fecSlot = theTKFECConfiguration_->addressFromTKFECID(TKFECID);
                unsigned int ringSlot = portcard->second->getringAddress();
                unsigned int ccuAddress = portcard->second->getccuAddress();
                unsigned int i2cChannel = portcard->second->getchannelAddress();

                if (fecSlot!=boost::get<0>(ccu_list[i]) || ringSlot!=boost::get<1>(ccu_list[i]) || ccuAddress!=boost::get<2>(ccu_list[i]))
                    continue;

                // Loop over Bottom and Top
                for (int j=0; j<2; j++){
                    try{
                        dcuAccess *dcuaccess = new dcuAccess(fecAccess_, fecSlot, ringSlot, ccuAddress, i2cChannel, i2cAddress[j]);

                        unsigned adc[8];
                        unsigned cregLIR = 0x88;
                        unsigned cregHIR = 0x80;

                        for (unsigned channel = 0; channel < 8; ++channel) {
                            if (mode == PortCard::DCU::LIR){
                                dcuaccess->setDcuCREG(cregLIR | channel);
                            }
                            else{
                                dcuaccess->setDcuCREG(cregHIR | channel);
                            }
                            unsigned highbits = dcuaccess->getDcuSHREG();
                            unsigned lowbits = dcuaccess->getDcuLREG();
                            adc[channel] = lowbits | ((highbits & 0xf) << 8);
                        }
                        delete dcuaccess;

                        
                        std::vector<std::string> msg_info_dcu;
                        std::string direction;

                        if (j==0)
                            direction = " Bottom";
                        else
                            direction = " Top";

                        msg_info_dcu = {portcard_name+direction,pixel::utils::to_string(adc[0]),pixel::utils::to_string(adc[1]),pixel::utils::to_string(adc[2]),pixel::utils::to_string(adc[3]),pixel::utils::to_string(adc[4]),pixel::utils::to_string(adc[5]),pixel::utils::to_string(adc[6]),pixel::utils::to_string(adc[7])};
                        FPixDCDCSummaryP_->displayMessage(monitor_,"itemset-log-dcu",msg_info_dcu,counterportcard);
                        counterportcard+=1;
                    }
                    catch (FecExceptionHandler & e) {
                        LOG4CPLUS_ERROR(sv_logger_, "Exception caught when doing DCU access:\n" + e.what());
                        LOG4CPLUS_INFO(sv_logger_, "will retry with iterative ccu reset");
                        if (retries>0) {
                            resetPlxFec(fecAccess_, fecSlot, ringSlot, 5, 10);
                            return readDCU(--retries);
                        }
                        success = false;
                    }
                } // end Bottom and Top loop
            } // end Portcard loop
        } // end CCU loop

        hardwarelock_->give();
    }
    catch (FecExceptionHandler &e) {
        if (hardwarelock_->value() == toolbox::BSem::EMPTY)
            hardwarelock_->give();
        std::string const msg_warn_gwf = "Caught hardware access exception in readDCU: " + string(e.what());
        LOG4CPLUS_WARN(sv_logger_, msg_warn_gwf);
        pixel::PixelTKFECSupervisorException fechandler_exception("FecHandlerException", "module", msg_warn_gwf, 3552, "PixelTKFECSupervisor::readDCU(int retries)");
        XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_warn_gwf, fechandler_exception);
        this->notifyQualified("fatal", f);
        success = false;
    }
    catch (...) {
        if (hardwarelock_->value() == toolbox::BSem::EMPTY)
            hardwarelock_->give();
        std::string const msg_warn_akv = "Caught unknown exception in readDCU";
        LOG4CPLUS_WARN(sv_logger_, msg_warn_akv);
        pixel::PixelTKFECSupervisorException trivial_exception("FecHandlerException", "module", msg_warn_akv, 3561, "PixelTKFECSupervisor::readDCU(int retries)");
        XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_warn_akv, trivial_exception);
        this->notifyQualified("fatal", f);
        success = false;
    }

    return success;
}

bool
pixel::PixelTKFECSupervisor::ReadDCU_workloop(toolbox::task::WorkLoop *w1) {

    //For giving the feds enough time to automask channels
    double sleepcheckdcu_workloop = readDCU_workloop_sleep_; // in seconds
    double sleepunits = 1e5; // in microseconds
    if (timesCheckDCUcalled < (sleepcheckdcu_workloop * 1e6 / sleepunits)) {
        usleep(sleepunits);
        timesCheckDCUcalled++;
        return true;
    }
    else {
        timesCheckDCUcalled = 0;
    }
    
    int nretries = 0;
    if (doIteerativeCCUreset_forDCU_)
        nretries = 10;

    dculock_->take();
    std::string const msg_debug_srt = "START ReadDCU WorkLoop";
    LOG4CPLUS_INFO(sv_logger_, msg_debug_srt);
    std::string booltostr = (readDCU(nretries) ? "Success" : "Failed");
    std::string const msg_func_sta = "DCU Workloop Status: " + booltostr;
    LOG4CPLUS_INFO(sv_logger_, msg_func_sta);
    std::string const msg_debug_end = "END ReadDCU WorkLoop";
    LOG4CPLUS_INFO(sv_logger_, msg_debug_end);
    dculock_->give();
    return true;
}

void
pixel::PixelTKFECSupervisor::submit_workloops(){
    if (doDCULoop_) {
        dculock_->take();
        workloopContinue_ = true;
        dculock_->give();
        workloop_->submit(readDCU_);
        LOG4CPLUS_INFO(sv_logger_, "DCU check workloop submitted");
    }
    if (doDCDCCheckLoop_) {
        dcdclock_->take();
        workloopContinueDCDC_ = true;
        dcdclock_->give();
        dcdc_workloop_->submit(checkDCDC_);
        LOG4CPLUS_INFO(sv_logger_, "DCDC check workloop submitted");
    }
}

void
pixel::PixelTKFECSupervisor::start_workloops(){
    if (doDCULoop_) {
        dculock_->take();
        workloopContinue_ = true;
        dculock_->give();
        if (!workloop_->isActive())
            workloop_->activate();
        LOG4CPLUS_INFO(sv_logger_, "DCU check workloop activated");
    }
    if (doDCDCCheckLoop_) {
        dcdclock_->take();
        workloopContinueDCDC_ = true;
        dcdclock_->give();
        if (!dcdc_workloop_->isActive())
            dcdc_workloop_->activate();
        LOG4CPLUS_INFO(sv_logger_, "DCDC check workloop activated");
    }
}

void
pixel::PixelTKFECSupervisor::stop_workloops(){
    if (doDCULoop_) {
        dculock_->take();
        workloopContinue_ = false;
        dculock_->give();
        if (workloop_->isActive())
            workloop_->cancel();
        LOG4CPLUS_INFO(sv_logger_, "DCU check workloop cancelled");
    }
    if (doDCDCCheckLoop_) {
        dcdclock_->take();
        workloopContinueDCDC_ = true;
        dcdclock_->give();
        if (dcdc_workloop_->isActive())
            dcdc_workloop_->cancel();
        LOG4CPLUS_INFO(sv_logger_, "DCDC check workloop cancelled");
    }
}

void
pixel::PixelTKFECSupervisor::remove_workloops(){
    if (doDCULoop_) {
        dculock_->take();
        workloopContinue_ = false;
        dculock_->give();
        if (workloop_->isActive())
            workloop_->cancel();
        try {
            workloop_->remove(readDCU_);
        }
        catch (xcept::Exception &e) { //will happen if the task was never submitted
            std::string const msg_error_ogq = "Failed to remove DCU workloop: " + string(e.what());
            LOG4CPLUS_DEBUG(sv_logger_, msg_error_ogq);
            XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_ogq, e);
            this->notifyQualified("fatal", f);
        }
        LOG4CPLUS_INFO(sv_logger_, "DCU check workloop removed");
    }
    if (doDCDCCheckLoop_) {
        dcdclock_->take();
        workloopContinueDCDC_ = false;
        dcdclock_->give();
        if (dcdc_workloop_->isActive())
            dcdc_workloop_->cancel();
        try {
            dcdc_workloop_->remove(checkDCDC_);
        }
        catch (xcept::Exception &e) { //will happen if the task was never submitted
            std::string const msg_error_ogqq = "Failed to remove DCDC workloop: " + string(e.what());
            LOG4CPLUS_DEBUG(sv_logger_, msg_error_ogqq);
            XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_ogqq, e);
            this->notifyQualified("fatal", f);
        }
        LOG4CPLUS_INFO(sv_logger_, "DCDC check workloop removed");
    }
}


void
pixel::PixelTKFECSupervisor::fixSoftErrorActionImpl(toolbox::Event::Reference event) {
    INFO("--- FixingSoftError ---");

    //some timers
    pixel::utils::Timer fixingSoftErrorTimer(&sv_logger_);
    fixingSoftErrorTimer.setVerbose(true);
    fixingSoftErrorTimer.setName("FixingSoftError");
    fixingSoftErrorTimer.start("FixingSoftError: start");
    pixel::utils::Timer fixingSoftErrorTimerPortCards(&sv_logger_);
    fixingSoftErrorTimerPortCards.setVerbose(true);
    fixingSoftErrorTimerPortCards.setName("FixingSoftError");
    pixel::utils::Timer fixingSoftErrorTimerPowercycling(&sv_logger_);
    fixingSoftErrorTimerPowercycling.setVerbose(true);
    fixingSoftErrorTimerPowercycling.setName("FixingSoftError");

    try {

        if (theCalibObject_ == 0) {
            INFO("PixelTKFECSupervisor::fixingSoftErrorActionImpl stop workloops");
            stop_workloops();
        }

        if(!ser_do_powercycle_dcdc){
          INFO("ATTENTION!!!!! POWERCYCLING IS DISABLED!!!!!!");
        }

        if(ser_do_powercycle_dcdc){
          //Powercycling of DCDC converters
          try {
              fixingSoftErrorTimerPowercycling.start("fixingSoftErrorActionImpl: start powercycling");
              INFO(" fixingSoftErrorActionImpl: Will powercycle DCDC converters");
              bool status = powercycleDCDCs();
              //bool status=true;
              fixingSoftErrorTimerPowercycling.stop("fixingSoftErrorActionImpl: stop powercycling");
              if (!status) {
                  WARN( "fixingSoftErrorActionImpl - Error in powercycling DCDC converters status not true");
              }
          }
          catch (FecExceptionHandler &e) { //exceptions
              std::string const msg = "fixingSoftErrorActionImpl - Error in powercycling DCDC converters " + e.what();
              ERROR( msg);
              pixel::PixelTKFECSupervisorException trivial_exception("FecHandlerException", "module", msg, __LINE__, "PixelTKFECSupervisor::fixingSoftErrorActionImpl(toolbox::fsm::FiniteStateMachine&)");
              if(!skip_lv_check){
                  XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg, trivial_exception);
                  this->notifyQualified("fatal", f);
              }
              //diagService_->reportError("Failed to Resume run with exception: "+string(e.what()),DIAGERROR);
              //reply = MakeSOAPMessageReference("ResumeFailed");
              //FIXME maybe we should transition to the Error state in case this happens?
          }
        }

        if(ser_do_portcard_recovery){
          //Programming of port cards
          try {
              fixingSoftErrorTimerPortCards.start("fixingSoftErrorActionImpl: start portcards");
              // do reporgamming
              INFO(" fixingSoftErrorActionImpl: Will reprogram portcards ");
              //bool status = programPortcards(false, true);
              fixingSoftErrorTimerPortCards.stop("fixingSoftErrorActionImpl: stop portcards");
              bool status = programPortcards(false, true);

              if (status) {
                  std::string const msg_error_ies = "fixingSoftErrorActionImpl - Error in portcard programming ";
                  WARN( msg_error_ies);
              }
          }
          catch (...) { //exceptions
              std::string const msg_error_mlf = "fixingSoftErrorActionImpl - Error in portcard programming ";
              ERROR( msg_error_mlf);
              pixel::PixelTKFECSupervisorException trivial_exception("FecHandlerException", "module", msg_error_mlf, 3825, "PixelTKFECSupervisor::fixingSoftErrorActionImpl(toolbox::fsm::FiniteStateMachine&)");
              if(!skip_lv_check){
                  XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_mlf, trivial_exception);
                  this->notifyQualified("fatal", f);
              }
              //diagService_->reportError("Failed to Resume run with exception: "+string(e.what()),DIAGERROR);
              //reply = MakeSOAPMessageReference("ResumeFailed");
              //FIXME maybe we should transition to the Error state in case this happens?
          }
        }
        suppressHardwareError_ = false;
    }
    catch (FecExceptionHandler &e) {
        std::string const msg_error_skq = "Failed to configure TKFEC (is the power off?); exception: " + string(e.what());
        ERROR( msg_error_skq);
        pixel::PixelTKFECSupervisorException fechandler_exception("FecHandlerException", "module", msg_error_skq, 3883, "PixelTKFECSupervisor::stateFixingSoftError(toolbox::fsm::FiniteStateMachine&)");
        if(!skip_lv_check){
            XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_skq, fechandler_exception);
            this->notifyQualified("fatal", f);
        }
    }
    catch (std::exception &e) { //failure to load config data raises std::exception (not xcept::Exception)
        std::string const msg_error_igo = "Failed to configure TKFEC; exception: " + string(e.what());
        ERROR( msg_error_igo);
        std::exception *error_ptr = &e;
        pixel::PixelTKFECSupervisorException *new_exception = dynamic_cast<pixel::PixelTKFECSupervisorException *>(error_ptr);
        if(!skip_lv_check){
            XCEPT_DECLARE_NESTED(pixel::PixelTKFECSupervisorException, f, msg_error_igo, *new_exception);
            this->notifyQualified("fatal", f);
        }
    }

    fixingSoftErrorTimerPortCards.printStats();
    fixingSoftErrorTimerPowercycling.printStats();
    fixingSoftErrorTimer.stop("FixingSoftError: stop");
    fixingSoftErrorTimer.printStats();

    std::string const msg_info_ksm = "--- Exit PixelTKFECSupervisor::stateFixingSoftError --- " + pixel::utils::to_string(fixingSoftErrorTimer.tottime());
    INFO(msg_info_ksm);

    INFO("--- FixingSoftError DONE ---");
}


void
pixel::PixelTKFECSupervisor::resumeFromSoftErrorActionImpl(toolbox::Event::Reference event){
    if (theCalibObject_ == 0) {
        INFO("PixelTKFECSupervisor::fixingSoftErrorActionImpl start workloops");
        start_workloops();
    }
}

/**
 * (Overload - XDAQAppBase)
 * Define what we expect in terms of parameters for each SOAP FSM command.
 */
std::vector<pixel::utils::FSMSOAPParHelper>
pixel::PixelTKFECSupervisor::expectedFSMSoapParsImpl(std::string const &commandName) const {
    std::vector<pixel::utils::FSMSOAPParHelper> params;

    if (commandName == "FixSoftError") {
        params.push_back(pixel::utils::FSMSOAPParHelper(commandName, "portcard", false));
        params.push_back(pixel::utils::FSMSOAPParHelper(commandName, "module", false));
    }

    return params;
}
/**
 * (Overload - XDAQAppWithFSMPixelBase)
 */
xoap::MessageReference
pixel::PixelTKFECSupervisor::changeStateImpl(xoap::MessageReference msg) {
    // Extract the command name from the SOAP message
    string commandName = "undefined";
    try {
        commandName = extractSOAPCommandName(msg);
    }
    catch (xcept::Exception &err) {
        string const msgBase = "Failed to extract command name from SOAP message";
        ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
        string errMsg = msgBase + ": '" + err.message() + "'.";

        // Somehow we don't understand the SOAP message. Log an error
        // and flag that we are having trouble.
        string histMsg = "Ignoring ununderstood FSM SOAP command '" + commandName + "'";
        appStateInfoSpace_.addHistoryItem(histMsg);

        ERROR(histMsg + " " + errMsg);
        xoap::MessageReference reply =
            makeSOAPFaultReply(msg,
                               pixel::utils::SOAPFaultCodeSender, // faultCode,
                               histMsg.c_str(), //faultString,
                               errMsg.c_str()); //faultDetail);
        return reply;
    }
    try {
        if(commandName=="FixSoftError"){
            // Extract the names and values of the variables
            ////map<string, vector<string> > elements = pixel::utils::soap::receiveVector(msg);
            Variables elements = getSOAPCommandVariables(msg);

            // Portcards
            fixPortcards_.clear();
            for(vector<string>::iterator  val = elements["portcard"].begin(); val != elements["portcard"].end(); val++){
                fixPortcards_.insert(*val);
                WARN("Received the following portcard with FixSoftError message: " << *val);
            }
            // Modules
            fixModules_.clear();
            for(vector<string>::iterator val = elements["module"].begin(); val != elements.at("module").end(); val++){
                fixModules_.insert(*val);
                WARN("Received the following modules with FixSoftError message: " << *val);
            }
            // Unknown elements
            for(Variables::iterator element = elements.begin(); element != elements.end(); element++){
                if(element->first != "portcard" && element->first != "module"){
                    for(vector<string>::iterator val = element->second.begin(); val != element->second.end(); val++){
                        WARN("Received an unknown element with FixSoftError message: " << element->first << ": " << *val);
                    }
                }
            }
        }
    } catch (xcept::Exception &err) {
        std::string const msgBase = "Failed to extract parameters from name from SOAP message";
        ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
    }

    return pixel::utils::XDAQAppWithFSMPixelBase::changeStateImpl(msg);
}


bool
pixel::PixelTKFECSupervisor::setMessage(xoap::MessageReference const &msg, bool received) {
    bool inserted = pixel::utils::SOAPER::setMessage(msg, received);
    if (inserted)
        TKFECSOAPMsgHistoryP_->displaySOAPMessage(monitor_);
    return inserted;
}
xoap::MessageReference
pixel::PixelTKFECSupervisor::SetDelay(xoap::MessageReference msg) { // {
    vector<string> namesParameters;
    namesParameters.push_back("PortCardName");
    namesParameters.push_back("Delay");
    namesParameters.push_back("Value");
    namesParameters.push_back("Update");
    namesParameters.push_back("Write");
    Attributes parametersReceived = getSomeSOAPCommandAttributes(msg, namesParameters);
    ////Attributes parametersReceived = getSOAPCommandAttributes(msg);

    /****************************************
     *            Set the Delays            *
     ****************************************/

    string portCardName = parametersReceived["PortCardName"];
    map<string, pos::PixelPortCardConfig *>::iterator iter = mapNamePortCard_.find(portCardName);

    if (iter == mapNamePortCard_.end()) {
        ERROR("Looking for portcard: " << portCardName << " mapNamePortCard_ size=" << mapNamePortCard_.size());

        for (map<string, pos::PixelPortCardConfig *>::iterator i=mapNamePortCard_.begin(); i!=mapNamePortCard_.end(); i++)
            ERROR("mapNamePortCards:" << i->first);

        XCEPT_RAISE(pixel::PixelTKFECSupervisorException, "PixelTKFECSupervisor::SetAOHBias -  Port card " + portCardName + " not found!");
    }


    string TKFECID = iter->second->getTKFECID();
    if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_) {
        string msg_err = "PixelTKFECSupervisor::SetDelay - Mismatch in crate number in portcard configuration";
        ERROR(msg_err);
        XCEPT_RAISE(xcept::Exception, msg_err);
    }

    unsigned int deviceAddress;
    try {
        deviceAddress = iter->second->getdeviceAddressForSetting(pos::PortCardSettingNames::k_Delay25.at(parametersReceived["Delay"]));
    }
    catch (...) {
        string msg_err_type = "PixelTKFECSupervisor::SetDelay - Register "+parametersReceived["Delay"];
        ERROR(msg_err_type);
        XCEPT_RAISE(pixel::PixelTKFECSupervisorException, msg_err_type);
    }

    unsigned int deviceValues = atoi(parametersReceived["Value"].c_str());

    if (fecAccess_==0) XCEPT_RAISE(xdaq::exception::Exception, "Problem in SetDelay: fecAccess_==0");

    portcardI2CDevice(fecAccess_,
                      theTKFECConfiguration_->addressFromTKFECID(TKFECID),
                      iter->second->getringAddress(),
                      iter->second->getccuAddress(),
                      iter->second->getchannelAddress(),
                      deviceAddress,
                      PHILIPS,
                      deviceValues);

    /****************************************
     *            Set the Delays            *
     ****************************************/

    if (atoi(parametersReceived["Update"].c_str()))
        iter->second->setdeviceValues(deviceAddress, deviceValues);

    if (atoi(parametersReceived["Write"].c_str()))
        iter->second->writeASCII(outputDir_ + "/");

    return makeSOAPMessageReference("SetDelayDone");
}

xoap::MessageReference
pixel::PixelTKFECSupervisor::SetDelayEnMass(xoap::MessageReference msg) {
    // Extract all attributes
    vector<string> namesParameters;
    namesParameters.push_back("Delay25Setting");
    namesParameters.push_back("DelayType");
    namesParameters.push_back("Update");
    namesParameters.push_back("Write");
    Attributes parametersReceived = getSomeSOAPCommandAttributes(msg, namesParameters);

    const bool update = atoi(parametersReceived["Update"].c_str()),
               write = atoi(parametersReceived["Write"].c_str());
    const unsigned int delaySetting = atoi(parametersReceived["Delay25Setting"].c_str());

    for (map<string, pos::PixelPortCardConfig *>::iterator iter = mapNamePortCard_.begin(); iter != mapNamePortCard_.end(); iter++) {
        const string TKFECID = iter->second->getTKFECID();

        if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_) {
            string msg_err = "PixelTKFECSupervisor::SetDelayEnMass - Mismatch in crate number in portcard configuration";
            ERROR(msg_err);
            XCEPT_RAISE(xcept::Exception, msg_err);
        }

        unsigned int deviceAddress;
        try {
            deviceAddress = iter->second->getdeviceAddressForSetting(pos::PortCardSettingNames::k_Delay25.at(parametersReceived["DelayType"]));
        }
        catch (...) {
            string msg_err_type = "PixelTKFECSupervisor::SetDelayEnMass - Register "+parametersReceived["DelayType"];
            ERROR(msg_err_type);
            XCEPT_RAISE(pixel::PixelTKFECSupervisorException, msg_err_type);
        }

        if (fecAccess_==0) XCEPT_RAISE(xdaq::exception::Exception, "Problem in SetDelayEnMass: fecAccess_==0");

        portcardI2CDevice(fecAccess_,
                          theTKFECConfiguration_->addressFromTKFECID(TKFECID),
                          iter->second->getringAddress(),
                          iter->second->getccuAddress(),
                          iter->second->getchannelAddress(),
                          deviceAddress,
                          PHILIPS,
                          delaySetting);

        if (update)
            iter->second->setdeviceValues(deviceAddress, delaySetting);

        if (write)
            iter->second->writeASCII(outputDir_ + "/");
    }

    return makeSOAPMessageReference("SetDelayEnMassDone");
}

xoap::MessageReference
pixel::PixelTKFECSupervisor::SetAOHGainEnMass(xoap::MessageReference msg) { // {
    vector<string> namesParameters;
    namesParameters.push_back("AOHGain");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, namesParameters);
    const unsigned int AOHGain = atoi(parameters["AOHGain"].c_str());

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    ////assert(tempCalibObject != 0);
    if (tempCalibObject == 0) {
        string msg_err_calib = "PixelTKFECSupervisor::SetAOHGainEnMass - Calib object is not specified!";
        ERROR(msg_err_calib);
        XCEPT_RAISE(pixel::PixelTKFECSupervisorException, msg_err_calib);
    }

    // List of port cards and addresses to write.  The new values are stored in the PixelPortCardConfig objects.
    vector<pair<pos::PixelPortCardConfig, set<unsigned int> > > newSettings; ////.// MAP?
    //                      port card                     address

    // Fill the list of port cards and addresses to write.
    const set<pos::PixelChannel> &channelsToCalibrate = tempCalibObject->channelList();

    for (std::set<pos::PixelChannel>::const_iterator channelsToCalibrate_itr = channelsToCalibrate.begin(); channelsToCalibrate_itr != channelsToCalibrate.end(); channelsToCalibrate_itr++) {
        const pair<string, int> portCardAndAOH = thePortcardMap_->PortCardAndAOH(*channelsToCalibrate_itr);
        const string portCardName = portCardAndAOH.first,
                     tbmname = (*channelsToCalibrate_itr).TBMChannelStringFull();

        // This is to escape A2/B2 channels (for L1/2)
        if (tbmname.find("2") == string::npos) {
            if (portCardName == "none") {
                string msg_err_pcn = "PixelTKFECSupervisor::SetAOHGainEnMass - Port card name is not specified!";
                ERROR(msg_err_pcn);
                XCEPT_RAISE(pixel::PixelTKFECSupervisorException, msg_err_pcn);
            }

            //PixelPortCardConfig* tempPortCard=mapNamePortCard_[portCardName];
            pos::PixelPortCardConfig *tempPortCard = 0;
            PixelConfigInterface::get(tempPortCard, "pixel/portcard/" + portCardName, *theGlobalKey_);
            if (tempPortCard==0) XCEPT_RAISE(toolbox::fsm::exception::Exception, "[PixelTKFECSupervisor::SetAOHGainEnMass] The portcard could not be loaded");

            if (theTKFECConfiguration_->crateFromTKFECID(tempPortCard->getTKFECID()) == crate_) {
                const int AOHNumber = portCardAndAOH.second;

                map<string, pos::PixelPortCardConfig *>::const_iterator mapNamePortCard_itr = mapNamePortCard_.find(portCardName);
                if (mapNamePortCard_itr == mapNamePortCard_.end()) {
                    string msg_err_pcn = "PixelTKFECSupervisor::SetAOHGainEnMass -  Port card " + portCardName + " not found!";
                    ERROR(msg_err_pcn);
                    XCEPT_RAISE(pixel::PixelTKFECSupervisorException, msg_err_pcn);
                }

                const pos::PixelPortCardConfig *thisPortCardConfig = mapNamePortCard_itr->second;

                // Look for this port card in the list of new settings, or add it to the list.
                vector<pair<pos::PixelPortCardConfig, set<unsigned int> > >::iterator portCardToChange;
                bool foundConfig = false;
                for (vector<pair<pos::PixelPortCardConfig, set<unsigned int> > >::iterator portcard_itr = newSettings.begin(); portcard_itr != newSettings.end(); portcard_itr++) {
                    if (portcard_itr->first.getPortCardName() == thisPortCardConfig->getPortCardName()) {
                        portCardToChange = portcard_itr;
                        foundConfig = true;
                        break;
                    }
                }
                if (!foundConfig) {
                    portCardToChange = newSettings.insert(newSettings.end(), make_pair(*thisPortCardConfig, set<unsigned int>()));
                    portCardToChange->first.setPortCardName(portCardName); // ensure that there will be no problems if the name stored in the configuration file doesn't match
                }

                // Change the AOH gain and record the address for this AOH.
                portCardToChange->first.setAOHGain(AOHNumber, AOHGain, tbmname);
                ////const unsigned int addressToAdd = portCardToChange->first.AOHGainAddressFromAOHNumber(AOHNumber);
                portCardToChange->second.insert(portCardToChange->first.AOHGainAddressFromAOHNumber(AOHNumber));
            }
        }
    }

    // Write the list of new settings to the hardware.
    for (vector<pair<pos::PixelPortCardConfig, set<unsigned int> > >::const_iterator portcard_itr = newSettings.begin(); portcard_itr != newSettings.end(); portcard_itr++) {
        const pos::PixelPortCardConfig &portcard = portcard_itr->first;
        const set<unsigned int> &addressesOnPortcard = portcard_itr->second;

        for (set<unsigned int>::const_iterator address_itr = addressesOnPortcard.begin(); address_itr != addressesOnPortcard.end(); address_itr++)
            SetPortCardSetting(&portcard,  *address_itr, portcard.getdeviceValuesForAddress(*address_itr));
    }

    return makeSOAPMessageReference("SetAOHGainEnMassDone");
}

bool
pixel::PixelTKFECSupervisor::SetAOHBias(std::string portCardName, unsigned int AOHNumber, unsigned int AOHBiasValue) {
    map<string, pos::PixelPortCardConfig *>::const_iterator mapNamePortCard_itr = mapNamePortCard_.find(portCardName);
    if (mapNamePortCard_itr == mapNamePortCard_.end()) {
        string msg_err_pcn = "PixelTKFECSupervisor::SetAOHBias -  PortCard " + portCardName + " not found!";
        ERROR(msg_err_pcn);
        XCEPT_RAISE(pixel::PixelTKFECSupervisorException, msg_err_pcn);
    }
    const pos::PixelPortCardConfig *thisPortCardConfig = mapNamePortCard_itr->second;

    return SetPortCardSetting(thisPortCardConfig,
                              thisPortCardConfig->AOHBiasAddressFromAOHNumber(AOHNumber),
                              AOHBiasValue);
}

xoap::MessageReference
pixel::PixelTKFECSupervisor::SetAOHBiasEnMass(xoap::MessageReference msg) { // {
    vector<string> namesParameters;
    namesParameters.push_back("AOHBias");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, namesParameters);
    const unsigned int AOHBias = atoi(parameters["AOHBias"].c_str());

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    if (tempCalibObject == 0) {
        string msg_err_calib = "PixelTKFECSupervisor::SetAOHGainEnMass - Calib object is not specified!";
        ERROR(msg_err_calib);
        XCEPT_RAISE(pixel::PixelTKFECSupervisorException, msg_err_calib);
    }

    const set<pos::PixelModuleName> &aModule_string = tempCalibObject->moduleList();

    for (set<pos::PixelModuleName>::iterator aModule_itr = aModule_string.begin(); aModule_itr != aModule_string.end(); ++aModule_itr) {
        const set<pair<string, int> > portCardAndAOHsOnThisModule = thePortcardMap_->PortCardAndAOHs(*aModule_itr);
        // Loop over all portcard-AOH pairs attached to this module (either 1 or 2)
        for (set<pair<string, int> >::const_iterator portCardAndAOH_itr = portCardAndAOHsOnThisModule.begin(); portCardAndAOH_itr != portCardAndAOHsOnThisModule.end(); ++portCardAndAOH_itr) {
            string portCardName = portCardAndAOH_itr->first;
            pos::PixelPortCardConfig *tempPortCard = mapNamePortCard_[portCardName];

            if (tempPortCard != 0 && theTKFECConfiguration_->crateFromTKFECID(tempPortCard->getTKFECID()) == crate_)
                    SetAOHBias(portCardName, portCardAndAOH_itr->second, AOHBias);
        }
    }

    return makeSOAPMessageReference("SetAOHBiasEnMassDone");
}

xoap::MessageReference
pixel::PixelTKFECSupervisor::SetAOHBiasOneChannel(xoap::MessageReference msg) { // {
    vector<string> namesParameters;
    namesParameters.push_back("PortCardName");
    namesParameters.push_back("AOHNumber");
    namesParameters.push_back("AOHBias");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, namesParameters);

    SetAOHBias(parameters["PortCardName"],
               atoi(parameters["AOHNumber"].c_str()),
               atoi(parameters["AOHBias"].c_str()));

    return makeSOAPMessageReference("SetAOHBiasOneChannelDone");
}


/*Method to disable the DCDC converters of BPix_BmI_SECY_LYR3/4
    At the time of writing (28 June 2023) these layers have a QPLL which is not locked,
    preventing configuration of these modules. In order to prevent damage to the pixel amplifiers,
    we will disable the corresponding DCDCs.
    Returns true if all DCDCs were successfully disabled, false otherwise
*/
bool pixel::PixelTKFECSupervisor::disableDCDCs_BPix_BmI_SEC7_LYR34()
{

  // This section checks that this instance of PixelTKFECSupervisor controls BPix_BmI_SEC7_LYR34 and not e.g. BpI
  // Without this check, this method will also disable BPix_BpI_SEC2 DCDCs
  std::stringstream instanceInfo;
  PixelConfigInterface::get(theTKFECConfiguration_, "pixel/tkfecconfig/", *theGlobalKey_);
  if (theTKFECConfiguration_==0) XCEPT_RAISE(xdaq::exception::Exception,"Problem in PIAReset: theTKFECConfiguration_==0");
  PixelConfigInterface::get(thePortcardMap_,"pixel/portcardmap/", *theGlobalKey_);
  if (theGlobalKey_==0) XCEPT_RAISE(xdaq::exception::Exception,"Problem in PIAReset: theGlobalKey_==0");
  pos::PixelDetectorConfig* detconfig=0; 
                                                                                                                                                                                                                        
  PixelConfigInterface::get(detconfig, "pixel/detconfig/", *theGlobalKey_);
  if (detconfig==0) XCEPT_RAISE(xdaq::exception::Exception,"Problem in PIAReset: detconfig==0");
  const std::set<std::string>& portcards=thePortcardMap_->portcards(detconfig);
  delete detconfig;

  std::set<std::string>::const_iterator iportcard=portcards.begin();
  pos::PixelPortCardConfig* tempPortCard=0;
  PixelConfigInterface::get(tempPortCard,"pixel/portcard/"+*iportcard, *theGlobalKey_);
  if (tempPortCard==0) XCEPT_RAISE(xdaq::exception::Exception,"Problem in PIAReset: tempPortCard==0");
  const std::string TKFECID = tempPortCard->getTKFECID();
  if (theTKFECConfiguration_->crateFromTKFECID(TKFECID) != crate_)
    {
      instanceInfo << "Crate did not match expected value. Will NOT try to disable DCDCs of BPix_BmI_SEC7_LYR34 in this instance." << std::endl;
      LOG4CPLUS_INFO(sv_logger_, instanceInfo.str());
      return false;
    }
  // end check of instance


  //Now that we've verified that we are of the correct instance, disable the DCDCs

  //DCDC names as found in /pixelscratch/pixelscratch/config/Pix/powermap/0/powermap.dat
  std::vector<std::string> dcdcNames;
  dcdcNames.push_back("BPix_DCDC4");
  dcdcNames.push_back("BPix_DCDC5");
  dcdcNames.push_back("BPix_DCDC6");
  dcdcNames.push_back("BPix_DCDC7");
  dcdcNames.push_back("BPix_DCDC11");
  dcdcNames.push_back("BPix_DCDC12");
  dcdcNames.push_back("BPix_DCDC13");

  std::stringstream disableStatus;
  bool disabledAll = true;
  for (size_t i = 0; i < dcdcNames.size(); i++)
    {
      //Arguments are: slot (aka FEC), ring, CCU, dcdcname, <0 for turn OFF, # of retries                            
      bool disabled = DCDCCommand(0x0, 0x3, 0x78, std::string(dcdcNames[i]), -1, 2);
      if (disabled)
	disableStatus << "Successfully disabled DCDC " << std::string(dcdcNames[i]) << std::endl;
      else
	disableStatus<< "Failed to  disabled DCDC " << std::string(dcdcNames[i]) << std::endl;

      disabledAll = disabledAll && disabled;
    }

  //Write the appropriate logging severity
  if (disabledAll)
    LOG4CPLUS_INFO(sv_logger_, disableStatus.str());
  else
    LOG4CPLUS_ERROR(sv_logger_, disableStatus.str());
    
  return disabledAll;

}
