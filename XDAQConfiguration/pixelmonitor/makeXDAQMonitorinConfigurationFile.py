import os
import sys
import socket

#fedconfig = "/pixelscratch/pixelscratch/config/Pix/fedconfig/0/fedconfig.dat"
#fecconfig = "/pixelscratch/pixelscratch/config/Pix/fecconfig/0/fecconfig.dat"
#tkfecconfig = "/pixelscratch/pixelscratch/config/Pix/tkfecconfig/0/tkfecconfig.dat"

buildhome = xdaqroot=os.getenv('BUILD_HOME','')
if buildhome == '':
    print ("please set BUILD_HOME")
    exit()
if sys.argv[1] not in ('usc', 'cln'):
    sys.exit("Argument incorrect! (usc, cln)")

if sys.argv[1] == 'usc':
    ## USC FEDs
    fedconnection = buildhome+"/pixel/PixelMonitor/xml/USC_FED_Connections.xml"
    output = os.path.join(buildhome, "pixel/PixelRun/runpixelmonitor_usc.xml")
elif sys.argv[1] == 'cln':
    ## CLN FEDS
    fedconnection = buildhome+"/pixel/PixelMonitor/xml/CLN_FED_Connections.xml"
    output = os.path.join(buildhome, "pixel/PixelRun/runpixelmonitor_cln.xml")

hostname = socket.gethostname()
if sys.argv[1] == 'usc':
    monitoringport = "1977"
elif sys.argv[1] == 'cln':
    monitoringport = "1978"


chbsrvdict = { "bridge-s1g01-18" : "srv-s2b18-28-01",
               "bridge-s1g01-27" : "srv-s2b18-29-01",
               "bridge-s1g01-36" : "srv-s2b18-30-01",
               "bridge-s1g01-45" : "srv-s2b18-31-01",
               "bridge-s1g03-45" : "srv-s2b18-37-01",
               "bridge-s1g03-36" : "srv-s2b18-34-01",
               "bridge-s1g03-27" : "srv-s2b18-33-01",
               "bridge-s1g03-18" : "srv-s2b18-32-01",
               "bridge-s1g04-45" : "srv-s2b18-41-01",
               "bridge-s1g04-36" : "srv-s2b18-40-01",
               "bridge-s1g04-27" : "srv-s2b18-39-01",
               "bridge-s1g04-18" : "srv-s2b18-38-01",
               "bridge-pxcln-09" : "srv-c2d11-08-01"}

fedrackapp = """
    <xc:Application class="PixelMonitor::rack::FEDRack" service="fedmonitor" id="500" instance="0" network="local" group="daq">
        <properties xmlns="urn:xdaq-application:PixelMonitor::rack::FEDRack" xsi:type="soapenc:Struct">
            <lasURL xsi:type="xsd:string">xaas-pixel.cms:9945</lasURL>
            <lasServiceName xsi:type="xsd:string">xmaslas2g</lasServiceName>
            <flashlistName xsi:type="xsd:string">FEDMonitor</flashlistName>
            <monitoringInterval xsi:type="xsd:string">PT2S</monitoringInterval>
        </properties>
    </xc:Application>
"""

fedmonitorapp = """
   <xc:Application class="PixelMonitor::pixfed::FEDMonitor" service="fedmonitor" id="APPID" instance="0" network="local" group="daq">
        <properties xmlns="urn:xdaq-application:PixelMonitor::pixfed::FEDMonitor" xsi:type="soapenc:Struct">
            <connectionName xsi:type="xsd:string">FEDID</connectionName>
            <connectionsFileName xsi:type="xsd:string">"""+fedconnection+"""</connectionsFileName>
            <monitoringInterval xsi:type="xsd:string">PT2S</monitoringInterval>
        </properties>
    </xc:Application>
"""

modules = """
    <xc:Module>/opt/xdaq/lib/libb2inutils.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libconfig.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/liblog4cplus.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libtoolbox.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libxalan-c.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libxdaq.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libxdata.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libxgi.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libxoap.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libxoapfilter.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libsentinelutils.so</xc:Module>

    <xc:Module>/opt/cactus/lib/libcactus_extern_pugixml.so</xc:Module>
    <xc:Module>/opt/cactus/lib/libcactus_uhal_uhal.so</xc:Module>

    <xc:Module>/opt/xdaq/lib/libtcdsexception.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libpixelmonitorhwlayer.so</xc:Module>"""

if sys.argv[1] == 'usc':
    modules += """
    <xc:Module>/opt/xdaq/lib/libpixelmonitorutilslayer.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libpixelmonitorpixfed.so</xc:Module>
    <xc:Module>/opt/xdaq/lib/libpixelmonitorrack.so</xc:Module>

"""
elif sys.argv[1] == 'cln':
    modules += """
    <xc:Module>"""+buildhome+"""/pixel/PixelMonitor/utilslayer/lib/linux/x86_64_centos7/libpixelmonitorutilslayer.so</xc:Module>
    <xc:Module>"""+buildhome+"""/pixel/PixelMonitor/pixfed/lib/linux/x86_64_centos7/libpixelmonitorpixfed.so</xc:Module>
    <xc:Module>"""+buildhome+"""/pixel/PixelMonitor/rack/lib/linux/x86_64_centos7/libpixelmonitorrack.so</xc:Module>

"""

fedconnectionfile = file(fedconnection, 'r')
feds = {}
for line in fedconnectionfile:
    line = line.strip()
    if len(line.split())<4: continue
    chb = line.split()[2].split(':')[1][2:]
    if chb not in feds.keys():
        feds[chb] = []
    fedid = line.split()[1][4:-1]
    app = fedmonitorapp.replace("FEDID",fedid).replace("APPID",fedid[3:])
    feds[chb].append(app)
fedconnectionfile.close()

xdaqconfigfile = file(output,"w")
xdaqconfigfile.write('<?xml version="1.0"?>\n')
xdaqconfigfile.write('<xc:Partition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30">\n\n')

if hostname not in [chbsrvdict[c] for c in feds.keys()]:
    xdaqconfigfile.write('<xc:Context id="0"'+' url="http://'+hostname+'.cms:'+monitoringport+'/">\n')
    xdaqconfigfile.write(fedrackapp)
    xdaqconfigfile.write(modules)
    xdaqconfigfile.write('</xc:Context>\n\n')

for crate, chb in enumerate(chbsrvdict.keys()):
    if chb not in feds.keys():
        continue
    xdaqconfigfile.write('<xc:Context id="'+str(crate+1)+'" url="http://'+chbsrvdict[chb]+'.cms:'+monitoringport+'/">\n')
    if hostname == chbsrvdict[chb]:
        xdaqconfigfile.write(fedrackapp)
    for fed in feds[chb]:
        xdaqconfigfile.write(fed)
    xdaqconfigfile.write(modules)
    xdaqconfigfile.write('</xc:Context>\n\n')

xdaqconfigfile.write('</xc:Partition>\n')
xdaqconfigfile.close()
