import os

fedconfig = "/pixelscratch/pixelscratch/config/Pix/fedconfig/0/fedconfig.dat"
buildhome = xdaqroot=os.getenv('BUILD_HOME','')

if buildhome=='':
    print("please set BUILD_HOME")
    exit()

addresstable = os.path.join(buildhome, "pixel/PixelFEDInterface/dat/address_table.xml")

connectionfile = file("fedconnection.xml","w")
connectionfile.write('<?xml version="1.0" encoding="UTF-8"?>\n')
connectionfile.write('<connections>\n')

configfile = file(fedconfig, "r")
for line in configfile:
    entry = '<connection id="FEDID" uri="URIADDRESS" address_table="file://'+addresstable+'"/>\n'
    line = line.strip()
    if len(line)==0: continue
    if(line[0]==" " or line[0]=="#"): continue
    fedid = line.split()[0]
    feduri = line.split()[4]
    entry = entry.replace("ID",fedid)
    entry = entry.replace("URIADDRESS",feduri)
    connectionfile.write(entry)
configfile.close()

connectionfile.write('</connections>\n')
connectionfile.close()
