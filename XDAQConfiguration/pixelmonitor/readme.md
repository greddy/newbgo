1. create a FED connection file first _if necessary_.
    
    **NOTE**: Normally connection file has already been created in `{BUILD_HOME}/pixel/PixelMonitor/xml/[USC_FED_Connections.xml, CLN_FED_Connections.xml]`.  
    These does not usually need to change unless `fedconfig` configuration file has been changed.  
    If needed, this script can help making a connection file, `python makeFEDConnectionFile.py`. **Read FIRST**, you may need to change it.  
2. create a XDAQ configuration:
    
    `python makeXDAQMonitorinConfigurationFile.py usc` or  
    `python makeXDAQMonitorinConfigurationFile.py cln`.
3. start xdaq with:
    
    `${XDAQ_ROOT}/bin/xdaq.sh -h 1977 -z pixel -c ${BUILD_HOME}/pixel/PixelRun/runpixelmonitor_usc.xml` or    
    `${XDAQ_ROOT}/bin/xdaq.sh -h 1978 -z pixel -c ${BUILD_HOME}/pixel/PixelRun/runpixelmonitor_cln.xml`  
    There is also a shellscript to make it easier here: `~wsi/bin/start-fedrack` or `~wsi/bin/start-test-fedrack`
