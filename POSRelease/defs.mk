#
# Local installation
# Default: destination specified by POS_INSTALL_PATH
# Custom location: make install INSTALL_PREFIX=/INSTALL_TARGET_PATH
#
# Building RPMs
# In future XDAQ: make INSTALL_CROSS_PLATFORM=no INSTALL_PATH=/RPM_TARGET_PATH
# Default: /opt/xdaq
#
# NOTE: in older XDAQ, $(XDAQ_PLATFORM) is inserted in INSTALL_PREFIX path.
#
ifndef INSTALL_PREFIX
INSTALL_PREFIX = $(shell echo ${POS_INSTALL_PATH})
endif

# number of processors, used as -j $(NPROC)
NPROC = $(shell nproc)
