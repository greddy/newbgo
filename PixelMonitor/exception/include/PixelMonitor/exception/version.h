#ifndef _tcds_exception_version_h_
#define _tcds_exception_version_h

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSEXCEPTION_VERSION_MAJOR 4
#define TCDSEXCEPTION_VERSION_MINOR 0
#define TCDSEXCEPTION_VERSION_PATCH 0

// If any previous versions available:
// #define TCDSEXCEPTION_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSEXCEPTION_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSEXCEPTION_VERSION_CODE PACKAGE_VERSION_CODE(TCDSEXCEPTION_VERSION_MAJOR, TCDSEXCEPTION_VERSION_MINOR, TCDSEXCEPTION_VERSION_PATCH)
#ifndef TCDSEXCEPTION_PREVIOUS_VERSIONS
#define TCDSEXCEPTION_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSEXCEPTION_VERSION_MAJOR, TCDSEXCEPTION_VERSION_MINOR, TCDSEXCEPTION_VERSION_PATCH)
#else
#define TCDSEXCEPTION_FULL_VERSION_LIST TCDSEXCEPTION_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSEXCEPTION_VERSION_MAJOR, TCDSEXCEPTION_VERSION_MINOR, TCDSEXCEPTION_VERSION_PATCH)
#endif

namespace tcdsexception {

const std::string package = "tcdsexception";
const std::string versions = TCDSEXCEPTION_FULL_VERSION_LIST;
const std::string description = "CMS TCDS helper software.";
const std::string authors = "Jeroen Hegeman";
const std::string summary = "CMS TCDS helper software.";
const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
config::PackageInfo getPackageInfo();
void checkPackageDependencies();
std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace tcdsexception

#endif
