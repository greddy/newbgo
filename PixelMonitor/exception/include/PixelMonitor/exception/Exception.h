#ifndef _PixelMonitor_exception_Exception_h_
#define _PixelMonitor_exception_Exception_h_

#include <string>

#include "xcept/Exception.h"

// Mimick XCEPT_DEFINE_EXCEPTION from xcept/Exception.h.
#define PIXELMONITOR_DEFINE_EXCEPTION(EXCEPTION_NAME)                                                                                                                                                                         \
    namespace PixelMonitor {                                                                                                                                                                                                  \
    namespace exception {                                                                                                                                                                                                     \
    class EXCEPTION_NAME : public PixelMonitor::exception::Exception {                                                                                                                                                        \
      public:                                                                                                                                                                                                                 \
        EXCEPTION_NAME(std::string name, std::string message, std::string module, int line, std::string function) : PixelMonitor::exception::Exception(name, message, module, line, function) {};                             \
        EXCEPTION_NAME(std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception &err) : PixelMonitor::exception::Exception(name, message, module, line, function, err) {}; \
    };                                                                                                                                                                                                                        \
    }                                                                                                                                                                                                                         \
    }

// And a little helper to save us some typing.
#define PIXELMONITOR_DEFINE_ALARM(ALARM_NAME) \
    PIXELMONITOR_DEFINE_EXCEPTION(ALARM_NAME)

namespace PixelMonitor {
namespace exception {

class Exception : public xcept::Exception {
  public:
    Exception(std::string name,
              std::string message,
              std::string module,
              int line,
              std::string function);
    Exception(std::string name,
              std::string message,
              std::string module,
              int line,
              std::string function,
              xcept::Exception &err);
};

} // namespace exception
} // namespace PixelMonitor

// The PIXELMONITOR exceptions.
PIXELMONITOR_DEFINE_EXCEPTION(BRILDAQProblem)
PIXELMONITOR_DEFINE_EXCEPTION(ConfigurationParseProblem)
PIXELMONITOR_DEFINE_EXCEPTION(ConfigurationProblem)
PIXELMONITOR_DEFINE_EXCEPTION(ConfigurationValidationProblem)
PIXELMONITOR_DEFINE_EXCEPTION(FSMTransitionProblem)
PIXELMONITOR_DEFINE_EXCEPTION(HardwareProblem)
PIXELMONITOR_DEFINE_EXCEPTION(HwLeaseRenewalProblem)
PIXELMONITOR_DEFINE_EXCEPTION(I2CBusBusy)
PIXELMONITOR_DEFINE_EXCEPTION(I2CProblem)
PIXELMONITOR_DEFINE_EXCEPTION(NibbleDAQProblem)
PIXELMONITOR_DEFINE_EXCEPTION(MonitoringProblem)
PIXELMONITOR_DEFINE_EXCEPTION(RCMSNotificationError)
PIXELMONITOR_DEFINE_EXCEPTION(RuntimeProblem)
PIXELMONITOR_DEFINE_EXCEPTION(SOAPFormatProblem)
PIXELMONITOR_DEFINE_EXCEPTION(SOAPCommandProblem)
PIXELMONITOR_DEFINE_EXCEPTION(SoftwareProblem)
PIXELMONITOR_DEFINE_EXCEPTION(TTCClockProblem)
PIXELMONITOR_DEFINE_EXCEPTION(TTCOrbitProblem)
PIXELMONITOR_DEFINE_EXCEPTION(TTCStreamProblem)
PIXELMONITOR_DEFINE_EXCEPTION(ValueError)

// The PIXELMONITOR alarms.
PIXELMONITOR_DEFINE_ALARM(BRILDAQFailureAlarm)
PIXELMONITOR_DEFINE_ALARM(FreqMonFailureAlarm)
PIXELMONITOR_DEFINE_ALARM(MonitoringFailureAlarm)
PIXELMONITOR_DEFINE_ALARM(NibbleDAQFailureAlarm)
PIXELMONITOR_DEFINE_ALARM(PhaseMonFailureAlarm)

#endif // _PixelMonitor_exception_Exception_h_
