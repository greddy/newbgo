#include "PixelMonitor/exception/Exception.h"

PixelMonitor::exception::Exception::Exception(std::string name,
                                              std::string message,
                                              std::string module,
                                              int line,
                                              std::string function)
    : xcept::Exception(name, message, module, line, function) {
}

PixelMonitor::exception::Exception::Exception(std::string name,
                                              std::string message,
                                              std::string module,
                                              int line,
                                              std::string function,
                                              xcept::Exception &err)
    : xcept::Exception(name, message, module, line, function, err) {
}
