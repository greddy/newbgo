#include "PixelMonitor/exception/version.h"

#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(tcdsexception)

void
tcdsexception::checkPackageDependencies() {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(xcept);
}

std::set<std::string, std::less<std::string> >
tcdsexception::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, xcept);

    return dependencies;
}
