#include "PixelMonitor/pixfed/FEDConfigurationInfoSpaceHandler.h"

#include <cstdlib>

#include "toolbox/string.h"

#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::pixfed::FEDConfigurationInfoSpaceHandler::FEDConfigurationInfoSpaceHandler(xdaq::Application *xdaqApp)
    : PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler(xdaqApp) {
    createString("connectionName",
                 "FED41",
                 "",
                 PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE,
                 true);
    createString("connectionsFileName",
                 "file://" + std::string(std::getenv("BUILD_HOME")) + "/pixel/PixelMonitor/xml/pixfed_connections.xml",
                 "",
                 PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE,
                 true);
    //createString("connectionURI",
    //             "chtcp-2.0://cmsuppixch:10203?target=devfed:50001",
    //             "",
    //             PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE,
    //             true);
    //createString("addressTable",
    //             "file://"+std::string(std::getenv("BUILD_HOME"))+"/pixel/PixelMonitor/dat/address_table.xml",
    //             "",
    //             PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE,
    //             true);
}

PixelMonitor::pixfed::FEDConfigurationInfoSpaceHandler::~FEDConfigurationInfoSpaceHandler() {
}

void
PixelMonitor::pixfed::FEDConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(PixelMonitor::utilslayer::Monitor &monitor) {
    PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(monitor);
    monitor.addItem("Application configuration",
                    "connectionName",
                    "uTCA HW connected",
                    this);
    monitor.addItem("Application configuration",
                    "connectionsFileName",
                    "FEDs connection file",
                    this);
    //monitor.addItem("Application configuration",
    //                "connectionURI",
    //                "HW URI",
    //                this);
    //monitor.addItem("Application configuration",
    //                "addressTable",
    //                "Full path of address table of HW",
    //                this);
}

std::string
PixelMonitor::pixfed::FEDConfigurationInfoSpaceHandler::formatItem(PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec::const_iterator const &item) const {
    std::string res = PixelMonitor::utilslayer::escapeAsJSONString(PixelMonitor::utilslayer::InfoSpaceHandler::kInvalidItemString_);
    //std::string name = item->name();
    //std::string const val = getString(name);
    //res = PixelMonitor::utilslayer::escapeAsJSONString(val);
    res = InfoSpaceHandler::formatItem(item);
    return res;
}
