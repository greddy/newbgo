#include "PixelMonitor/pixfed/PixFEDInfoSpaceUpdater.h"

#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <iomanip>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "PixelMonitor/pixfed/FEDMonitor.h"
#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/utilslayer/InfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/InfoSpaceItem.h"
#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::pixfed::PixFEDInfoSpaceUpdater::PixFEDInfoSpaceUpdater(PixelMonitor::hwlayer::HwDeviceTCA const &hw,
                                                                     PixelMonitor::pixfed::FEDMonitor &xdaqApp)
    : PixelMonitor::utilslayer::InfoSpaceUpdater(),
      hw_(hw),
      xdaqApp_(xdaqApp) {
}

PixelMonitor::pixfed::PixFEDInfoSpaceUpdater::~PixFEDInfoSpaceUpdater() {
}

void
PixelMonitor::pixfed::PixFEDInfoSpaceUpdater::updateInfoSpaceImpl(PixelMonitor::utilslayer::InfoSpaceHandler *const infoSpaceHandler) {
    PixelMonitor::hwlayer::HwDeviceTCA const &hw = getHw();
    if (hw.isHwConnected()) {
        PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec &items = infoSpaceHandler->getItems();
        PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec::iterator iter;

        for (iter = items.begin(); iter != items.end(); ++iter) {
            try {
                updateInfoSpaceItem(*iter, infoSpaceHandler);
            }
            catch (...) {
                iter->setInvalid();
                throw;
            }
        }

        // Now sync everything from the cache to the InfoSpace itself.
        infoSpaceHandler->writeInfoSpace();
    } else {
        infoSpaceHandler->setInvalid();
    }
}

bool
PixelMonitor::pixfed::PixFEDInfoSpaceUpdater::updateInfoSpaceItem(PixelMonitor::utilslayer::InfoSpaceItem &item,
                                                                  PixelMonitor::utilslayer::InfoSpaceHandler *const infoSpaceHandler) {
    bool updated = false;
    PixelMonitor::pixfed::FEDReader reader(getHw());
    if (hw_.isHwConnected()) {
        std::string name = item.name();
        PixelMonitor::utilslayer::InfoSpaceItem::UpdateType updateType = item.updateType();
        PixelMonitor::utilslayer::InfoSpaceItem::ItemType itemType = item.type();
        if (updateType == PixelMonitor::utilslayer::InfoSpaceItem::HW32) {
            // TODO need to be changed later!
            if (itemType == PixelMonitor::utilslayer::InfoSpaceItem::UINT32) {
                // ------------- UPDATE ERROR FOR CHANNELS -------------
                if (name == "TransitionsToTTSReady") {
                    infoSpaceHandler->setUInt32(name, reader.getTransitionsToTTSReady());
                } else if (name == "TransitionsToTTSBusy") {
                    infoSpaceHandler->setUInt32(name, reader.getTransitionsToTTSBusy());
                } else if (name == "TransitionsToTTSOOS") {
                    infoSpaceHandler->setUInt32(name, reader.getTransitionsToTTSOOS());
                } else if (name == "TransitionsToTTSWarn") {
                    infoSpaceHandler->setUInt32(name, reader.getTransitionsToTTSWarn());
                } else if (name == "L1ACount") {
                    infoSpaceHandler->setUInt32(name, reader.getL1ACount());
                }

                // ------------- UPDATE PLL STATUS -------------
                else if (name == "PLL_400MHz") {
                    infoSpaceHandler->setUInt32(name, reader.getPLLlockedInfo()[0]);
                } else if (name == "PLL_200MHz") {
                    infoSpaceHandler->setUInt32(name, reader.getPLLlockedInfo()[1]);
                } else if (name == "PLL_200MHz_idelay") {
                    infoSpaceHandler->setUInt32(name, reader.getPLLlockedInfo()[2]);
                }

                // ------------- UPDATE TOTAL ERROR STATUS -------------
                else if (name == "DisabledChTot") {
                    unsigned long channelEnableStatusUl = strtoul(infoSpaceHandler->getString("channelEnableStatus").c_str(), NULL, 2);
                    std::bitset<48> tmp(channelEnableStatusUl);
                    infoSpaceHandler->setUInt32(name,
                                                tmp.count());
                } else if (name == "TbmMaskChTot") {
                    infoSpaceHandler->setUInt32(name,
                                                reader.getTBMMask().count());
                } else if (name == "ChMaskedTot") {
                    infoSpaceHandler->setUInt32(name,
                                                reader.getChannelMask().count());
                } else if (name == "EvtErrNumTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getEvtErrNumVec()));
                } else if (name == "EvtTmoNumTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getEvtTmoNumVec()));
                } else if (name == "CntEvtRsyTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getCntEvtRsyVec()));
                } else if (name == "CntTbmHidTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getCntTbmHidVec()));
                } else if (name == "CntTrlErrTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getCntTrlErrVec()));
                } else if (name == "PkamRstTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getPkamRstVec()));
                } else if (name == "NoTknPssTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getNoTknPssVec()));
                } else if (name == "RocErrNumTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getRocErrNumVec()));
                } else if (name == "OvfNumTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getOvfNumVec()));
                } else if (name == "TbmAtoRstTot") {
                    infoSpaceHandler->setUInt32(name,
                                                PixelMonitor::pixfed::sumVecOfUint32(reader.getTbmAtoRstVec()));
                }

                updated = true;
            }
            // else if (itemType == PixelMonitor::utilslayer::InfoSpaceItem::BOOL)
            // {
            //     InfoSpaceHandler->setBool(name, (tmp != 0));
            //     updated = true;
            // }
        } else if (updateType == PixelMonitor::utilslayer::InfoSpaceItem::HW64) {
            if (itemType == PixelMonitor::utilslayer::InfoSpaceItem::UINT64) {
                // ------------- UPDATE TTS TIMES -------------
                if (name == "TimeInTTSReady") {
                    infoSpaceHandler->setUInt64(name, reader.getTimeInTTSReady());
                } else if (name == "TimeInTTSBusy") {
                    infoSpaceHandler->setUInt64(name, reader.getTimeInTTSBusy());
                } else if (name == "TimeInTTSOOS") {
                    infoSpaceHandler->setUInt64(name, reader.getTimeInTTSOOS());
                } else if (name == "TimeInTTSWarn") {
                    infoSpaceHandler->setUInt64(name, reader.getTimeInTTSWarn());
                }
                updated = true;
            }
        } else if (updateType == PixelMonitor::utilslayer::InfoSpaceItem::PROCESS) {
            if (itemType == PixelMonitor::utilslayer::InfoSpaceItem::STRING) {
                // ------------- UPDATE TTS STATE -------------
                if (name == "BoardId") {
                    infoSpaceHandler->setString(name, reader.getBoardId());
                } else if (name == "RevId") {
                    infoSpaceHandler->setString(name, reader.getRevId());
                } else if (name == "BoardCode") {
                    infoSpaceHandler->setString(name, reader.getBoardCode());
                } else if (name == "MACAddress") {
                    infoSpaceHandler->setString(name, reader.getMACAddress());
                } else if (name == "FWIPHCVersion") {
                    infoSpaceHandler->setString(name, reader.getFwIPHCVersion());
                } else if (name == "FWIPHCDate") {
                    infoSpaceHandler->setString(name, reader.getFwIPHCDate());
                } else if (name == "FWHEPHYVersion") {
                    infoSpaceHandler->setString(name, reader.getFwHEPHYVersion());
                } else if (name == "FWHEPHYDate") {
                    infoSpaceHandler->setString(name, reader.getFwHEPHYDate());
                } else if (name == "TTSState") {
                    infoSpaceHandler->setString(name, reader.getTTSState());
                } else if (name == "connectionName") {
                    std::string res = xdaqApp_.getConfigurationInfoSpaceHandler().getString("connectionName").substr(3, -1);
                    infoSpaceHandler->setString(name, res);
                } else if (name == "stateName") {
                    std::string res = xdaqApp_.getApplicationStateInfoSpaceHandler().getString("stateName");
                    infoSpaceHandler->setString(name, res);
                } else if (name == "lid") {
                    std::string res = toolbox::toString("%d", xdaqApp_.getApplicationDescriptor()->getLocalId());
                    infoSpaceHandler->setString(name, res);
                } else if (name == "tbmMask") {
                    std::bitset<48> tbmmask_ = reader.getTBMMask();

                    // if TBM_MASK is inconsistent with previous..
                    if (tbmmask_.to_string() != infoSpaceHandler->getString(name)) {
                        unsigned long prevTbmMaskUl = strtoul(infoSpaceHandler->getString(name).c_str(), NULL, 2);
                        std::bitset<48> prevTbmmask_(prevTbmMaskUl);
                        std::bitset<48> altered_ = tbmmask_ ^ prevTbmmask_;
                        // logging..
                        std::string const fedid = xdaqApp_.getConfigurationInfoSpaceHandler().getString("connectionName");
                        std::string const prevMsg = fedid + " TBM Mask was: " + prevTbmmask_.to_string();
                        LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), prevMsg);
                        xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(prevMsg);
                        for (std::size_t i = 0; i != altered_.size(); ++i) {
                            // mask bit changed AND channged to MASK
                            if (altered_[i] && tbmmask_[i]) {
                                std::stringstream tmp;
                                tmp << "Channel " << std::setfill('0') << std::setw(2) << (i + 1) << " is masked in TBM_MASK";
                                LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), tmp.str());
                                xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(tmp.str());
                            }
                        }
                        std::string const postMsg = fedid + " TBM Mask now: " + tbmmask_.to_string();
                        LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), postMsg);
                        xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(postMsg);

                        infoSpaceHandler->setString(name, tbmmask_.to_string());
                    }
                } else if (name == "channelMask") {
                    std::bitset<48> channelmask_ = reader.getChannelMask();

                    // if MASK_CH is inconsistent with previous..
                    if (channelmask_.to_string() != infoSpaceHandler->getString(name)) {
                        // finding the differences
                        unsigned long prevChannelmaskUl = strtoul(infoSpaceHandler->getString(name).c_str(), NULL, 2);
                        std::bitset<48> prevChannelmask_(prevChannelmaskUl);
                        unsigned long tbmmaskUl = strtoul(infoSpaceHandler->getString("tbmMask").c_str(), NULL, 2);
                        std::bitset<48> tbmmask_(tbmmaskUl);
                        std::bitset<48> altered_ = channelmask_ ^ prevChannelmask_;
                        // logging..
                        std::string const fedid = xdaqApp_.getConfigurationInfoSpaceHandler().getString("connectionName");
                        std::string const prevMsg = fedid + " CHs Mask was: " + prevChannelmask_.to_string();
                        LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), prevMsg);
                        xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(prevMsg);
                        for (std::size_t i = 0; i != altered_.size(); ++i) {
                            // mask bit changed and changed to MASK
                            if (altered_[i] && channelmask_[i]) {
                                std::stringstream tmp;
                                tmp << "Channel " << std::setfill('0') << std::setw(2) << (i + 1) << " is masked MASK_CH";
                                LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), tmp.str());
                                xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(tmp.str());
                            }
                            // mask bit changed AND changed to UNMASK AND tbmbit was UNMASK
                            if (altered_[i] && prevChannelmask_[i] && !tbmmask_[i]) {
                                std::stringstream tmp;
                                tmp << "Channel " << std::setfill('0') << std::setw(2) << (i + 1) << " is recovered";
                                LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), tmp.str());
                                xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(tmp.str());
                            }
                        }
                        std::string const postMsg = fedid + " CHs Mask now: " + channelmask_.to_string();
                        LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), postMsg);
                        xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(postMsg);

                        // write into infospace
                        infoSpaceHandler->setString(name, channelmask_.to_string());
                    }
                } else if (name == "fedErr_info") {
                    std::string res;
                    for (uint32_t i = 1; i <= 48; ++i) {
                        if (!res.empty()) {
                            res += ", ";
                        }
                        res += "{";

                        std::stringstream ss;
                        ss << "CH" << std::setfill('0') << std::setw(2) << i;
                        res +=
                            PixelMonitor::utilslayer::escapeAsJSONString("chName") + ": " +
                            PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getNGoodPhaseOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("ngp") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        unsigned long tmpUl = strtoul(infoSpaceHandler->getString("channelEnableStatus").c_str(), NULL, 2);
                        std::bitset<48> tmp(tmpUl);
                        ss << tmp[i - 1];
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("me") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getTBMMask()[i - 1];
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("mt") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getChannelMask()[i - 1];
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("mc") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getEvtErrNumOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("ene") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getEvtTmoNumOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("etoe") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getCntTrlErrOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("mte") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getOvfNumOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("ofn") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getNoTknPssOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("ntp") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getCntEvtRsyOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("lb4t") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getRocErrNumOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("re") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getTbmAtoRstOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("tar") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getCntTbmHidOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("the") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getPkamRstOfChannel(i);
                        res += ", " +
                               PixelMonitor::utilslayer::escapeAsJSONString("tpr") + ": " +
                               PixelMonitor::utilslayer::escapeAsJSONString(ss.str());
                        ss.str("");

                        // End of JSON
                        res += "}";
                    }

                    res = "[" + res + "]";

                    infoSpaceHandler->setString(name, res);
                }
                updated = true;
            }
        }
        if (!updated && updateType != PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE) {
            std::string msg = toolbox::toString("Updating not implemented for item with name '%s'.",
                                                item.name().c_str());
            XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
        }
    }

    if (updated) {
        item.setValid();
    } else {
        item.setInvalid();
    }

    return updated;
}

PixelMonitor::hwlayer::HwDeviceTCA const &
PixelMonitor::pixfed::PixFEDInfoSpaceUpdater::getHw() const {
    return hw_;
}

uint32_t
PixelMonitor::pixfed::sumVecOfUint32(std::vector<uint32_t> vec) {
    uint32_t res(0);
    for (std::vector<uint32_t>::iterator iter = vec.begin();
         iter != vec.end();
         ++iter) {
        res += *iter;
    }
    return res;
}
