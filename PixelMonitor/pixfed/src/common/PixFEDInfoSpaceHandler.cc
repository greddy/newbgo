#include "PixelMonitor/pixfed/PixFEDInfoSpaceHandler.h"

#include <stdint.h>
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>

#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"
#include "PixelMonitor/utilslayer/WebServer.h"
#include "PixelMonitor/pixfed/WebTableFedErrInfo.h"

PixelMonitor::pixfed::PixFEDInfoSpaceHandler::PixFEDInfoSpaceHandler(xdaq::Application *xdaqApp,
                                                                     PixelMonitor::utilslayer::InfoSpaceUpdater *updater)
    : PixelMonitor::utilslayer::InfoSpaceHandler(xdaqApp, "pixel-fed-monitor", updater) {
    // monitored in configurationInfoSpace already, create here for
    // flashlist convinience to read from single infospace.
    createString("connectionName", "");
    // DAQ STATUS : PROCESS
    createString("BoardId", "-");
    createString("RevId", "-");
    createString("BoardCode", "-");
    createString("MACAddress", "-");
    createString("FWIPHCVersion", "-");
    createString("FWHEPHYVersion", "-");
    createString("FWIPHCDate", "-");
    createString("FWHEPHYDate", "-");

    createString("stateName", "Halted");
    createString("lid", "-");
    createString("TTSState", "-");

    createString("tbmMask", "");
    createString("channelMask", "");
    createString("channelEnableStatus", "");

    // DAQ STATUS : HW64
    createUInt64("TimeInTTSReady", 0);
    createUInt64("TimeInTTSBusy", 0);
    createUInt64("TimeInTTSOOS", 0);
    createUInt64("TimeInTTSWarn", 0);

    // DAQ STATUS : HW32
    createUInt32("TransitionsToTTSReady", 0);
    createUInt32("TransitionsToTTSBusy", 0);
    createUInt32("TransitionsToTTSOOS", 0);
    createUInt32("TransitionsToTTSWarn", 0);
    createUInt32("L1ACount", 0);

    createUInt32("PLL_400MHz", 0);
    createUInt32("PLL_200MHz", 0);
    createUInt32("PLL_200MHz_idelay", 0);

    createUInt32("DisabledChTot", 0);
    createUInt32("TbmMaskChTot", 0);
    createUInt32("ChMaskedTot", 0);
    createUInt32("EvtErrNumTot", 0);
    createUInt32("EvtTmoNumTot", 0);
    createUInt32("CntEvtRsyTot", 0);
    createUInt32("CntTbmHidTot", 0);
    createUInt32("CntTrlErrTot", 0);

    createUInt32("PkamRstTot", 0);
    createUInt32("NoTknPssTot", 0);
    createUInt32("RocErrNumTot", 0);
    createUInt32("OvfNumTot", 0);
    createUInt32("TbmAtoRstTot", 0);

    // FED Error Table string
    createString("fedErr_info", "", "json-object");
}

PixelMonitor::pixfed::PixFEDInfoSpaceHandler::~PixFEDInfoSpaceHandler() {
}

void
PixelMonitor::pixfed::PixFEDInfoSpaceHandler::registerItemSetsWithMonitor(PixelMonitor::utilslayer::Monitor &monitor) {
    // ------------- HW INFO -------------
    std::string itemSetName_0 = "fed-hw-info";
    monitor.newItemSet(itemSetName_0);

    monitor.addItem(itemSetName_0, "connectionName", "FED ID", this);
    monitor.addItem(itemSetName_0, "BoardId", "Board Base", this);
    monitor.addItem(itemSetName_0, "RevId", "Rev ID", this);
    monitor.addItem(itemSetName_0, "BoardCode", "Board Type", this);
    monitor.addItem(itemSetName_0, "MACAddress", "MAC Address", this);
    monitor.addItem(itemSetName_0, "FWIPHCVersion", "IPHC FW Version", this);
    monitor.addItem(itemSetName_0, "FWIPHCDate", "IPHC FW Date", this);
    monitor.addItem(itemSetName_0, "stateName", "Enable?", this);
    monitor.addItem(itemSetName_0, "lid", "LocalId", this);

    //monitor.addItem(itemSetName_0, "FWHEPHYVersion", "HEPHY FW Version", this);
    //monitor.addItem(itemSetName_0, "FWHEPHYDate", "HEPHY FW Date", this);

    // ------------- TTS INFO -------------
    std::string itemSetName_1 = "fed-tts-times";
    monitor.newItemSet(itemSetName_1);

    monitor.addItem(itemSetName_1, "TTSState", "TTS State", this);
    monitor.addItem(itemSetName_1, "TimeInTTSReady", "In RDY Times", this,
                    "Times in TTS Ready state");
    monitor.addItem(itemSetName_1, "TimeInTTSBusy", "In BSY Times", this,
                    "Times in TTS Busy state");
    monitor.addItem(itemSetName_1, "TimeInTTSOOS", "In OOS Times", this,
                    "Times in TTS out-of-sync state");
    monitor.addItem(itemSetName_1, "TimeInTTSWarn", "In WRN Times", this,
                    "Times in TTS Warn state");

    monitor.addItem(itemSetName_1, "TransitionsToTTSReady", "Transition to RDY", this,
                    "Times transited to Ready");
    monitor.addItem(itemSetName_1, "TransitionsToTTSBusy", "Transition to BSY", this,
                    "Times transited to Busy");
    monitor.addItem(itemSetName_1, "TransitionsToTTSOOS", "Transition to OOS", this,
                    "Times transited to out-of-sync");
    monitor.addItem(itemSetName_1, "TransitionsToTTSWarn", "Transition to Warn", this,
                    "Times transited to Warn");
    monitor.addItem(itemSetName_1, "L1ACount", "L1A counter", this,
                    "");

    // ------------- PLL INFO -------------
    std::string itemSetName_2 = "fed-pll-lock";
    monitor.newItemSet(itemSetName_2);

    monitor.addItem(itemSetName_2, "PLL_400MHz", "PLL 400MHz locked?", this);
    monitor.addItem(itemSetName_2, "PLL_200MHz", "PLL 200MHz locked?", this);
    monitor.addItem(itemSetName_2, "PLL_200MHz_idelay", "PLL 200MHz idelay locked?", this);

    // ------------- TOTAL ERROR INFO -------------
    std::string itemSetName_3 = "fed-err-total";
    monitor.newItemSet(itemSetName_3);

    monitor.addItem(itemSetName_3, "DisabledChTot", "Disabled channels", this, "Disabled in detectconfig.dat");
    monitor.addItem(itemSetName_3, "TbmMaskChTot", "TBM Masked", this, "TBM masked in total, including those disabled");
    monitor.addItem(itemSetName_3, "ChMaskedTot", "MASK_CH Masked", this, "MASK_CH masked in total, including those get TBM masked");
    monitor.addItem(itemSetName_3, "EvtErrNumTot", "Event errors", this);
    monitor.addItem(itemSetName_3, "EvtTmoNumTot", "Event time-out errors", this);
    monitor.addItem(itemSetName_3, "CntEvtRsyTot", "Left-behind-4-times errors", this);
    monitor.addItem(itemSetName_3, "CntTbmHidTot", "TBM header errors", this);
    monitor.addItem(itemSetName_3, "CntTrlErrTot", "Missing trailer errors", this);
    monitor.addItem(itemSetName_3, "PkamRstTot", "TBM PKAM Resets", this);
    monitor.addItem(itemSetName_3, "NoTknPssTot", "No Token Pass Events", this);
    monitor.addItem(itemSetName_3, "RocErrNumTot", "ROC Errors", this);
    monitor.addItem(itemSetName_3, "OvfNumTot", "Overflow number", this);
    monitor.addItem(itemSetName_3, "TbmAtoRstTot", "TBM Auto Resets", this);

    // ------------- ERROR TABLE -------------
    std::string const itemSetName_4 = "fe-itemset";
    monitor.newItemSet(itemSetName_4);
    monitor.addItem(itemSetName_4, "fedErr_info", "FED error table", this);
}

void
PixelMonitor::pixfed::PixFEDInfoSpaceHandler::registerItemSetsWithWebServer(PixelMonitor::utilslayer::WebServer &webServer,
                                                                            PixelMonitor::utilslayer::Monitor &monitor) {
    // ------------- REGISTER TAB 0 -------------
    std::string const tabName_0 = "FED STATUS";
    std::string const disc_0 = "FED Status in general";
    webServer.registerTab(tabName_0,
                          disc_0, // descriptioni eg. FED Status in general
                          4);

    // ------------- REGISTER TABLES -------------
    webServer.registerTable("Hardware Identifiers",         // title
                            "General hardware information", // description
                            monitor,
                            "fed-hw-info", // setname
                            tabName_0);
    webServer.registerTable("TTS Table",
                            "TTS state and information",
                            monitor,
                            "fed-tts-times",
                            tabName_0);
    webServer.registerTable("Error total count",
                            "Total error counts add up; channels out of 48",
                            monitor,
                            "fed-err-total",
                            tabName_0);
    webServer.registerTable("PLL Lock",
                            "PLL locked status",
                            monitor,
                            "fed-pll-lock",
                            tabName_0);

    // ------------- REGISTER TAB 1 -------------
    std::string const tabName_1 = "ERROR INFO";
    std::string const desc_1 = "Error information for each channel.<br/>\
			      <ul> \
			        <li><b>Number of Good Phases</b>: Out of 32.</li> \
                    <li>ME     <b>Disabled in detectconfig.dat</b>: \"1\" is disabled </li> \
                    <li>MT     <b>Masked in TBM</b>: <sup>TBM_MASK</sup> \"1\" is masked </li> \
                    <li>MC     <b>Masked in Channel</b>: <sup>MASK_STATUS_CH</sup> \"1\" is masked </li> \
			        <li><b>Event Number Error</b>: Total number of event number errors for the channel (resets at start of run). <sup>pixfed_stat_regs.diag_cnt.ENE.tbm_ch_{1..48}</sup></li> \
			        <li><b>Event Time-out Error</b>: Total number of timeouts. <sup>pixfed_stat_regs.diag_cnt.TO.tbm_ch_{1..48}</sup></li> \
			        <li><b>Missing Trailer Error</b>: Counts if there is a missing trailer. <sup>pixfed_stat_regs.cnt_trailer_err.tbm_ch_{1..48}</sup></li> \
                    <li><b>Overflows</b>: Total number of overflows for this channel. <sup>pixfed_stat_regs.diag_cnt.OVF.tbm_ch_{1..48}</sup></li> \
                    <li><b>No Token Pass</b>: Total number of no token pass events for this channel. <sup>pixfed_stat_regs.diag_cnt.NTP.tbm_ch_{1..48}</sup></li> \
			        <li>LB4T   <b>Left Behind 4 Times</b>: If the FED is behind the actual event number four times in a row. The FED will issue a timeout and increment the counter by 1. <sup>pixfed_stat_regs.cnt_evtResyncAheadBy1.tbm_ch_{1..48}</sup></li> \
                    <li><b>ROC Errors</b>: Total number of ROC errors for this channel. <sup>pixfed_stat_regs.diag_cnt.ROC_NBR_ERR.tbm_ch_{1..48}</sup></li> \
                    <li><b>TBM Auto Resets</b>: Total number of TBM auto reset detected. <sup>pixfed_stat_regs.diag_cnt.AUTORESET.tbm_ch_{1..48}</sup></li> \
			        <li><b>TBM Header Error</b>: Counts if the first word of the TBM FIFO is not a header. <sup>pixfed_stat_regs.cnt_TBM_H_ID_err.tbm_ch_{1..48}</sup></li> \
                    <li><b>TBM PKAM Resets</b>: Total number of TBM PKAM reset detected. <sup>pixfed_stat_regs.diag_cnt.PKAM.tbm_ch_{1..48}</sup></li> \
			      </ul>";

    webServer.registerTab(tabName_1,
                          desc_1,
                          1);

    // ------------- REGISTER TABLES -------------
    webServer.registerWebObject<WebTableFedErrInfo>("FED Error Table",
                                                    "I am your beloved error.",
                                                    monitor,
                                                    "fe-itemset",
                                                    tabName_1);
}

std::string
PixelMonitor::pixfed::PixFEDInfoSpaceHandler::formatItem(PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec::const_iterator const &item) const {
    std::string res = PixelMonitor::utilslayer::escapeAsJSONString(kInvalidItemString_);
    if (item->isValid()) {
        std::string name = item->name();
        if (name == "fedErr_info") {
            res = getString(name);
        } else {
            res = InfoSpaceHandler::formatItem(item);
        }
    }
    return res;
}
