#include "PixelMonitor/pixfed/FEDMonitor.h"

#include <algorithm>
#include <map>
#include <stdint.h>
#include <string>
#include <utility>
#include <unistd.h>
#include <cstdlib>

#include "hyperdaq/framework/Layout.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"
#include "xgi/Method.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/hwlayer/HwDeviceTCA.h"
#include "PixelMonitor/hwlayer/RegisterInfo.h"
#include "PixelMonitor/hwlayer/Utils.h"

#include "PixelMonitor/pixfed/PixFEDInfoSpaceHandler.h"
#include "PixelMonitor/pixfed/PixFEDInfoSpaceUpdater.h"

#include "PixelMonitor/utilslayer/LogMacros.h"
#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/SOAPUtils.h"
#include "PixelMonitor/utilslayer/WebServer.h"
#include "PixelMonitor/utilslayer/Utils.h"

XDAQ_INSTANTIATOR_IMPL(PixelMonitor::pixfed::FEDMonitor);

PixelMonitor::pixfed::FEDMonitor::FEDMonitor(xdaq::ApplicationStub *stub) try
    : PixelMonitor::utilslayer::XDAQAppWithFSMBasic(stub,
                                                    std::unique_ptr<PixelMonitor::hwlayer::HwDeviceTCA>(new PixelMonitor::hwlayer::HwDeviceTCA())),
      fedUpdaterP_(0),
      fedHandlerP_(0) {
    // create the InfoSpace holding all configuration information
    cfgInfoSpaceP_ =
        std::unique_ptr<PixelMonitor::pixfed::FEDConfigurationInfoSpaceHandler>(new PixelMonitor::pixfed::FEDConfigurationInfoSpaceHandler(this));
    // Make sure the correct default hardware configuration file is found
    // cfgInfoSpaceP_->setString("defaultConfigurationFilePath",
    //                           "$(XDAQ_ROOT)/etc/PixelMonitor/pixfed/hw_cfg_default.txt");
}
catch (PixelMonitor::exception::Exception const &err) {
    std::string msgBase = "Something went wrong instantiating the FEDMonitor";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
}

PixelMonitor::pixfed::FEDMonitor::~FEDMonitor() {
    hwReleaseImpl();
}

void
PixelMonitor::pixfed::FEDMonitor::setupInfoSpaces() {
    // Make sure the configuration settings are up-to-date
    cfgInfoSpaceP_->readInfoSpace();

    fedUpdaterP_ = fedInfoSpaceUpdaterPtr(new fedInfoSpaceUpdater(getHw(),
                                                                  *this));
    fedHandlerP_ = fedInfoSpaceHandlerPtr(new fedInfoSpaceHandler(this, fedUpdaterP_.get()));

    // Register all InfoSpaceItems with the Monitor
    cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
    appStateInfoSpace_.registerItemSets(monitor_, webServer_);
    fedHandlerP_->registerItemSets(monitor_, webServer_);

    std::string const fedid = cfgInfoSpaceP_->getString("connectionName");
    //if(const char* env_p = std::getenv("BUILD_HOME"))
    if (const char *env_p = std::getenv("XDAQ_ROOT")) {
        //std::string const cmd = toolbox::toString("%s/pixel/PixelMonitor/utilslayer/scripts/getDisabledChFromDetconfig.py %s &",
        std::string const cmd = toolbox::toString("%s/bin/getDisabledChFromDetconfig.py %s &",
                                                  env_p, fedid.substr(3, -1).c_str());
        std::string output = PixelMonitor::utilslayer::exec(cmd.c_str());
        // remove annoying trailer '\n' character for JSON
        output.erase(std::remove(output.begin(), output.end(), '\n'), output.end());
        fedHandlerP_->setString("channelEnableStatus", output, true);
        fedHandlerP_->setString("tbmMask", output, true);
        std::string const msg = fedid + "Enable: " + output;
        INFO(msg);
        this->getApplicationStateInfoSpaceHandler().addHistoryItem(msg);
    } else {
        ERROR("Environment variable XDAQ_ROOT is not set!!");
    }
}

PixelMonitor::hwlayer::HwDeviceTCA &
PixelMonitor::pixfed::FEDMonitor::getHw() const {
    return static_cast<PixelMonitor::hwlayer::HwDeviceTCA &>(*hwP_.get());
}

void
PixelMonitor::pixfed::FEDMonitor::hwConnectImpl() {
    PixelMonitor::utilslayer::HwDeviceConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
PixelMonitor::pixfed::FEDMonitor::hwReleaseImpl() {
    getHw().hwRelease();
    fedHandlerP_->setString("stateName", "Halted", true);
}
