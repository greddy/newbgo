#include "PixelMonitor/pixfed/FEDReader.h"

#include <algorithm>
#include <iterator>

#include "toolbox/string.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/hwlayer/Utils.h"

PixelMonitor::pixfed::FEDReader::FEDReader(PixelMonitor::hwlayer::HwDeviceTCA const &hw)
    : hw_(hw) {
}

PixelMonitor::hwlayer::HwDeviceTCA const &
PixelMonitor::pixfed::FEDReader::getHw() const {
    return hw_;
}

PixelMonitor::pixfed::FEDReader::~FEDReader() {
}

bool
PixelMonitor::pixfed::FEDReader::isHwConnected() const {
    return hw_.isHwConnected();
}

std::string
PixelMonitor::pixfed::FEDReader::getBoardId() const {
    uint32_t val = hw_.readRegister("board_id");
    std::string res = PixelMonitor::hwlayer::uint32ToString(val);
    return res;
}

std::string
PixelMonitor::pixfed::FEDReader::getRevId() const {
    uint32_t val = hw_.readRegister("rev_id");
    std::string res = PixelMonitor::hwlayer::uint32ToString(val);
    return res;
}

std::string
PixelMonitor::pixfed::FEDReader::getBoardCode() const {
    std::stringstream ss;
    uint32_t val = hw_.readRegister("pixfed_stat_regs.user_ascii_code_01to04");
    ss << PixelMonitor::hwlayer::uint32ToString(val);
    val = hw_.readRegister("pixfed_stat_regs.user_ascii_code_05to08");
    ;
    ss << PixelMonitor::hwlayer::uint32ToString(val);
    return ss.str();
}

std::string
PixelMonitor::pixfed::FEDReader::getMACAddress() const {
    std::stringstream ss;
    ss << std::hex << std::setfill('0')
       << std::setw(2) << hw_.readRegister("mac_b5") << ":"
       << std::setw(2) << hw_.readRegister("mac_b4") << ":"
       << std::setw(2) << hw_.readRegister("mac_b3") << ":"
       << std::setw(2) << hw_.readRegister("mac_b2") << ":"
       << std::setw(2) << hw_.readRegister("mac_b1") << ":"
       << std::setw(2) << hw_.readRegister("mac_b0")
       << std::setfill(' ') << std::dec;
    return ss.str();
}

std::string
PixelMonitor::pixfed::FEDReader::getFwIPHCVersion() const {
    return getFWInfo("IPHC")[0];
}

std::string
PixelMonitor::pixfed::FEDReader::getFwHEPHYVersion() const {
    return getFWInfo("HEPHY")[0];
}

std::string
PixelMonitor::pixfed::FEDReader::getFwIPHCDate() const {
    return getFWInfo("IPHC")[1];
}

std::string
PixelMonitor::pixfed::FEDReader::getFwHEPHYDate() const {
    return getFWInfo("HEPHY")[1];
}

std::vector<std::string>
PixelMonitor::pixfed::FEDReader::getFWInfo(std::string ins) const {
    std::vector<std::string> res;
    if (ins == "IPHC") {
        std::stringstream ss;
        uint32_t val = hw_.readRegister("pixfed_stat_regs.user_iphc_fw_id.fw_ver_nb");
        ss << val << ".";
        val = hw_.readRegister("pixfed_stat_regs.user_iphc_fw_id.archi_ver_nb");
        ss << val;
        res.push_back(ss.str());

        ss.str("");

        val = hw_.readRegister("pixfed_stat_regs.user_iphc_fw_id.fw_ver_day");
        ss << val << "/";
        val = hw_.readRegister("pixfed_stat_regs.user_iphc_fw_id.fw_ver_month");
        ss << val << "/";
        val = hw_.readRegister("pixfed_stat_regs.user_iphc_fw_id.fw_ver_year");
        ss << val;
        res.push_back(ss.str());

        return res;
    } else if (ins == "HEPHY") {
        std::stringstream ss;
        uint32_t val = hw_.readRegister("pixfed_stat_regs.user_hephy_fw_id.fw_ver_nb");
        ss << val << ".";
        val = hw_.readRegister("pixfed_stat_regs.user_hephy_fw_id.archi_ver_nb");
        ss << val;
        res.push_back(ss.str());

        ss.str("");

        val = hw_.readRegister("pixfed_stat_regs.user_hephy_fw_id.fw_ver_day");
        ss << val << "/";
        val = hw_.readRegister("pixfed_stat_regs.user_hephy_fw_id.fw_ver_month");
        ss << val << "/";
        val = hw_.readRegister("pixfed_stat_regs.user_hephy_fw_id.fw_ver_year");
        ss << val;
        res.push_back(ss.str());

        return res;
    } else {
        std::string msg = "Firmware version unavailabel, has to be \
                           either 'IPHC' or 'HEPHY'.";
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    }
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getPLLlockedInfo() const {
    std::vector<uint32_t> res;
    res.push_back(hw_.readRegister("hephy_firmware_version.lockedPLL400"));
    res.push_back(hw_.readRegister("hephy_firmware_version.lockedPLL200"));
    res.push_back(hw_.readRegister("hephy_firmware_version.lockedPLL200idelay"));
    res.push_back(hw_.readRegister("hephy_firmware_version.TTCready"));
    return res;
}

uint64_t
PixelMonitor::pixfed::FEDReader::getTimeInTTSReady() const {
    uint32_t low = hw_.readRegister("pixfed_stat_regs.tts.elapsed_time_RDY_15to0");
    uint32_t hi = hw_.readRegister("pixfed_stat_regs.tts.elapsed_time_RDY_47to16");
    uint64_t res = (hi << 16) | low;
    return res;
}

uint64_t
PixelMonitor::pixfed::FEDReader::getTimeInTTSBusy() const {
    uint32_t low = hw_.readRegister("pixfed_stat_regs.tts.elapsed_time_BUSY_15to0");
    uint32_t hi = hw_.readRegister("pixfed_stat_regs.tts.elapsed_time_BUSY_47to16");
    uint64_t res = (hi << 16) | low;
    return res;
}

uint64_t
PixelMonitor::pixfed::FEDReader::getTimeInTTSOOS() const {
    uint32_t low = hw_.readRegister("pixfed_stat_regs.tts.elapsed_time_OOS_15to0");
    uint32_t hi = hw_.readRegister("pixfed_stat_regs.tts.elapsed_time_OOS_47to16");
    uint64_t res = (hi << 16) | low;
    return res;
}

uint64_t
PixelMonitor::pixfed::FEDReader::getTimeInTTSWarn() const {
    uint32_t low = hw_.readRegister("pixfed_stat_regs.tts.elapsed_time_WARN_15to0");
    uint32_t hi = hw_.readRegister("pixfed_stat_regs.tts.elapsed_time_WARN_47to16");
    uint64_t res = (hi << 16) | low;
    return res;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getTransitionsToTTSReady() const {
    return hw_.readRegister("pixfed_stat_regs.tts.transitions_to_RDY");
}

uint32_t
PixelMonitor::pixfed::FEDReader::getTransitionsToTTSBusy() const {
    return hw_.readRegister("pixfed_stat_regs.tts.transitions_to_BUSY");
}

uint32_t
PixelMonitor::pixfed::FEDReader::getTransitionsToTTSOOS() const {
    return hw_.readRegister("pixfed_stat_regs.tts.transitions_to_OOS");
}

uint32_t
PixelMonitor::pixfed::FEDReader::getTransitionsToTTSWarn() const {
    return hw_.readRegister("pixfed_stat_regs.tts.transitions_to_WARN");
}

uint32_t
PixelMonitor::pixfed::FEDReader::getL1ACount() const {
    uint32_t tmp = hw_.readRegister("pixfed_stat_regs.L1A_counter.L1ACnt");
    return tmp & 0xFFFFFF;
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getNGoodPhaseVec() const {
    std::vector<uint32_t> val = hw_.readBlock("idel_individual_stat_block");
    std::vector<uint32_t> nGoodPhaseVec;
    for (uint32_t i = 0; i < 24; ++i) // val.size()/4 -> 48, using only first 24 now
    {
        uint32_t nGoodPhase = 32 - std::bitset<32>(val[i * 4 + 1]).count();
        nGoodPhaseVec.push_back(nGoodPhase);
        nGoodPhaseVec.push_back(nGoodPhase); // 2 channels share the same phase
    }
    return nGoodPhaseVec;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getNGoodPhaseOfChannel(uint32_t chId) const {
    return getNGoodPhaseVec()[chId - 1];
}

std::string
PixelMonitor::pixfed::FEDReader::getTTSState() const {
    std::string ttsState;
    uint32_t cWord = hw_.readRegister("pixfed_stat_regs.tts.word");
    cWord = cWord & 0x0000000F;
    if (cWord == 8) {
        ttsState = "RDY";
    } else if (cWord == 4) {
        ttsState = "BSY";
    } else if (cWord == 2) {
        ttsState = "OOS";
    } else if (cWord == 1) {
        ttsState = "OVF";
    } else if (cWord == 0) {
        ttsState = "DIC";
    } else if (cWord == 12) {
        ttsState = "ERR";
    }
    return ttsState;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getEvtErrNumOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.diag_cnt.ENE.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return (hw_.readRegister(ss.str()));
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getEvtErrNumVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getEvtErrNumOfChannel(i));
    }
    return val;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getEvtTmoNumOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.diag_cnt.TO.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return (hw_.readRegister(ss.str()));
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getEvtTmoNumVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getEvtTmoNumOfChannel(i));
    }
    return val;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getCntEvtRsyOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.cnt_evtResyncAheadBy1.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return hw_.readRegister(ss.str());
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getCntEvtRsyVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getCntEvtRsyOfChannel(i));
    }
    return val;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getCntTbmHidOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.cnt_TBM_H_ID_err.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return hw_.readRegister(ss.str());
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getCntTbmHidVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getCntTbmHidOfChannel(i));
    }
    return val;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getCntTrlErrOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.cnt_trailer_err.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return hw_.readRegister(ss.str());
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getCntTrlErrVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getCntTrlErrOfChannel(i));
    }
    return val;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getPkamRstOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.diag_cnt.PKAM.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return hw_.readRegister(ss.str());
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getPkamRstVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getPkamRstOfChannel(i));
    }
    return val;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getNoTknPssOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.diag_cnt.NTP.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return hw_.readRegister(ss.str());
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getNoTknPssVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getNoTknPssOfChannel(i));
    }
    return val;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getRocErrNumOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.diag_cnt.ROC_NBR_ERR.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return hw_.readRegister(ss.str());
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getRocErrNumVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getRocErrNumOfChannel(i));
    }
    return val;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getOvfNumOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.diag_cnt.OVF.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return hw_.readRegister(ss.str());
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getOvfNumVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getOvfNumOfChannel(i));
    }
    return val;
}

uint32_t
PixelMonitor::pixfed::FEDReader::getTbmAtoRstOfChannel(uint32_t chId) const {
    std::stringstream ss;
    ss << "pixfed_stat_regs.diag_cnt.AUTORESET.tbm_ch_";
    ss << std::setfill('0') << std::setw(2) << chId;
    return hw_.readRegister(ss.str());
}

std::vector<uint32_t>
PixelMonitor::pixfed::FEDReader::getTbmAtoRstVec() const {
    std::vector<uint32_t> val;
    for (uint32_t i = 1; i < 49; ++i) {
        val.push_back(getTbmAtoRstOfChannel(i));
    }
    return val;
}

std::bitset<48>
PixelMonitor::pixfed::FEDReader::getMaskFromTwoRegs(std::string str0_32,
                                                    std::string str0_16) const {
    uint32_t cWord0 = hw_.readRegister(str0_32);
    uint32_t cWord1 = hw_.readRegister(str0_16);
    std::bitset<48> res0(cWord0);
    std::bitset<48> res1(cWord1);
    res1 <<= 32;
    res0 |= res1;
    return res0;
}

/*
std::bitset<48>
PixelMonitor::pixfed::FEDReader::getMaskStatus() const
{
    std::bitset<48> tbmMask = getMaskFromTwoRegs("pixfed_ctrl_regs.TBM_MASK_1",
                                                 "pixfed_ctrl_regs.TBM_MASK_2");
    std::bitset<48> maskStatus = getMaskFromTwoRegs("pixfed_stat_regs.MASK_STATUS_CH_32to01",
                                                    "pixfed_stat_regs.MASK_STATUS_CH_48to33");
    return (tbmMask | maskStatus); // "1" is masked
}

uint32_t
PixelMonitor::pixfed::FEDReader::getMaskStatusOfChannel(uint32_t chId) const
{
    return uint32_t(getMaskStatus()[chId-1]);
}
*/

std::bitset<48>
PixelMonitor::pixfed::FEDReader::getTBMMask() const {
    return getMaskFromTwoRegs("pixfed_ctrl_regs.TBM_MASK_1",
                              "pixfed_ctrl_regs.TBM_MASK_2");
}

std::bitset<48>
PixelMonitor::pixfed::FEDReader::getChannelMask() const {
    return getMaskFromTwoRegs("pixfed_stat_regs.MASK_STATUS_CH_32to01",
                              "pixfed_stat_regs.MASK_STATUS_CH_48to33");
}
