#include "PixelMonitor/pixfed/WebTableFedErrInfo.h"

#include <sstream>
#include <string>
#include <vector>

#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"
#include "PixelMonitor/utilslayer/WebObject.h"

PixelMonitor::pixfed::WebTableFedErrInfo::WebTableFedErrInfo(std::string const &name,
                                                             std::string const &description,
                                                             PixelMonitor::utilslayer::Monitor const &monitor,
                                                             std::string const &itemSetName,
                                                             std::string const &tabName,
                                                             size_t const colSpan)
    : PixelMonitor::utilslayer::WebObject(name, description, monitor, itemSetName, tabName, colSpan) {
}

std::string
PixelMonitor::pixfed::WebTableFedErrInfo::getHTMLString() const {
    std::stringstream res;
    res << "<div class=\"tcds-item-table-wrapper\">"
        << "\n";

    res << "<p class=\"tcds-item-table-title\">"
        << getName()
        << "</p>";
    res << "\n";

    res << "<p class=\"tcds-item-table-description\">"
        << getDescription()
        << "</p>";
    res << "\n";

    // And now the actual table contents.
    PixelMonitor::utilslayer::Monitor::StringPairVector items =
        monitor_.getFormattedItemSet(itemSetName_);
    // ASSERT ASSERT ASSERT
    assert(items.size() == 1);
    // ASSERT ASSERT ASSERT end

    std::string const itemName = items.at(0).first;
    std::string const tmp = "[\"" + itemSetName_ + "\"][\"" + itemName + "\"]";
    std::string const invalidVal = PixelMonitor::utilslayer::WebObject::kStringInvalidItem;

    res << "<script type=\"text/x-dot-template\">";
    // Case 0: invalid item ('-').
    res << "{{? it" << tmp << " == '" << invalidVal << "'}}"
        << "<span>" << invalidVal << "</span>"
        << "\n";
    // Case 1: a (hopefully non-empty) array.
    res << "{{??}}"
        << "\n"
        << "<table id=\"pixfedTable\" class=\"xdaq-table tcds-item-table sortable pixelmonitor-table-compact\">"
        << "\n"
        << "<thead>"
        << "<tr>"
        << "<th>Name</th>"
        << "<th>NumOfGoodPhase</th>"
        << "<th>ME</th>"
        << "<th>MT</th>"
        << "<th>MC</th>"
        << "<th>EvtNumErr</th>"
        << "<th>EvtTOErr</th>"
        << "<th>MissTrailerErr</th>"
        << "<th>OverFlowNum</th>"
        << "<th>NoTokenPass</th>"
        << "<th>LB4T</th>"
        << "<th>ROC Error</th>"
        << "<th>TBM AutoReset</th>"
        << "<th>TBM Header Err</th>"
        << "<th>TBM PKAM Reset</th>"
        << "</tr>"
        << "</thead>"
        << "\n"
        << "<tbody>"
        << "\n"
        << "{{~it" << tmp << " :value:index}}"
        << "\n"
        << "<tr>"
        << "\n"
        << "<td class=\"firstColumn\">{{=value[\"chName\"]}}</td>"
        << "<td>{{=value[\"ngp\"]}}/32</td>"
        << "<td class=\"me\">{{=value[\"me\"]}}</td>"
        << "<td class=\"mt\">{{=value[\"mt\"]}}</td>"
        << "<td class=\"mc\">{{=value[\"mc\"]}}</td>"
        << "<td>{{=value[\"ene\"]}}</td>"
        << "<td>{{=value[\"etoe\"]}}</td>"
        << "<td>{{=value[\"mte\"]}}</td>"
        << "<td>{{=value[\"ofn\"]}}</td>"
        << "<td>{{=value[\"ntp\"]}}</td>"
        << "<td>{{=value[\"lb4t\"]}}</td>"
        << "<td>{{=value[\"re\"]}}</td>"
        << "<td>{{=value[\"tar\"]}}</td>"
        << "<td>{{=value[\"the\"]}}</td>"
        << "<td>{{=value[\"tpr\"]}}</td>"
        << "\n"
        << "</tr>"
        << "\n"
        << "{{~}}"
        << "\n"
        << "</tbody>"
        << "\n"
        << "</table>"
        << "\n";
    res << "{{?}}"
        << "\n";
    res << "</script>"
        << "\n"
        << "<div class=\"target\"></div>"
        << "\n";

    return res.str();
}
