#ifndef _PixelMonitor_pixfed_FEDReader_h
#define _PixelMonitor_pixfed_FEDReader_h

#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <bitset>
#include <cstdlib>

#include "PixelMonitor/hwlayer/IHwDevice.h"
#include "PixelMonitor/hwlayer/RegisterInfo.h"
#include "PixelMonitor/hwlayer/HwDeviceTCA.h"

namespace PixelMonitor {
namespace hwlayer {
class HwDeviceTCA;
}
}

namespace PixelMonitor {
namespace pixfed {
class FEDReader {
  public:
    FEDReader(PixelMonitor::hwlayer::HwDeviceTCA const &hw);
    virtual ~FEDReader();

    bool isHwConnected() const;
    std::string getBoardId() const;
    std::string getRevId() const;
    std::string getBoardCode() const;
    std::string getMACAddress() const;
    std::string getFwIPHCVersion() const;
    std::string getFwHEPHYVersion() const;
    std::string getFwIPHCDate() const;
    std::string getFwHEPHYDate() const;

    std::vector<uint32_t> getNGoodPhaseVec() const;
    uint32_t getNGoodPhaseOfChannel(uint32_t chId) const;
    std::vector<uint32_t> getPLLlockedInfo() const;
    uint64_t getTimeInTTSReady() const;
    uint64_t getTimeInTTSBusy() const;
    uint64_t getTimeInTTSOOS() const;
    uint64_t getTimeInTTSWarn() const;
    uint32_t getTransitionsToTTSReady() const;
    uint32_t getTransitionsToTTSBusy() const;
    uint32_t getTransitionsToTTSOOS() const;
    uint32_t getTransitionsToTTSWarn() const;
    uint32_t getL1ACount() const;
    uint32_t getEvtErrNumOfChannel(uint32_t chId) const;
    uint32_t getEvtTmoNumOfChannel(uint32_t chId) const;
    uint32_t getCntEvtRsyOfChannel(uint32_t chId) const;
    uint32_t getCntTbmHidOfChannel(uint32_t chId) const;
    uint32_t getCntTrlErrOfChannel(uint32_t chId) const;
    uint32_t getMaskStatusOfChannel(uint32_t chId) const;
    uint32_t getPkamRstOfChannel(uint32_t chId) const;
    uint32_t getNoTknPssOfChannel(uint32_t chId) const;
    uint32_t getRocErrNumOfChannel(uint32_t chId) const;
    uint32_t getOvfNumOfChannel(uint32_t chId) const;
    uint32_t getTbmAtoRstOfChannel(uint32_t chId) const;

    std::vector<uint32_t> getEvtErrNumVec() const;
    std::vector<uint32_t> getEvtTmoNumVec() const;
    std::vector<uint32_t> getCntEvtRsyVec() const;
    std::vector<uint32_t> getCntTbmHidVec() const;
    std::vector<uint32_t> getCntTrlErrVec() const;
    std::bitset<48> getMaskStatus() const;
    std::bitset<48> getTBMMask() const;
    std::bitset<48> getChannelMask() const;
    std::vector<uint32_t> getPkamRstVec() const;
    std::vector<uint32_t> getNoTknPssVec() const;
    std::vector<uint32_t> getRocErrNumVec() const;
    std::vector<uint32_t> getOvfNumVec() const;
    std::vector<uint32_t> getTbmAtoRstVec() const;

    std::string getTTSState() const;

  protected:
    std::vector<std::string> getFWInfo(std::string ins) const;

  private:
    PixelMonitor::hwlayer::HwDeviceTCA const &getHw() const;
    PixelMonitor::hwlayer::HwDeviceTCA const &hw_;
    std::bitset<48> getMaskFromTwoRegs(std::string str0_32, std::string str0_16) const;
};
} // namespace hwlayer
} // namespace PixelMonitor
#endif // _PixelMonitor_pixfed_FEDReader_h
