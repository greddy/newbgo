#ifndef _PixelMonitor_pixfed_PixFEDInfoSpaceHandler_h_
#define _PixelMonitor_pixfed_PixFEDInfoSpaceHandler_h_

#include <string>

#include "PixelMonitor/utilslayer/InfoSpaceHandler.h"
#include "PixelMonitor/pixfed/FEDReader.h"

namespace PixelMonitor {
namespace utilslayer {
class InfoSpaceUpdater;
class Monitor;
class WebServer;
}
}

namespace xdaq {
class Application;
}

namespace PixelMonitor {
namespace pixfed {

class PixFEDInfoSpaceUpdater;
class FEDReader;

class PixFEDInfoSpaceHandler : public PixelMonitor::utilslayer::InfoSpaceHandler {

  public:
    PixFEDInfoSpaceHandler(xdaq::Application *xdaqApp,
                           PixelMonitor::utilslayer::InfoSpaceUpdater *updater);

    virtual ~PixFEDInfoSpaceHandler();

  private:
    virtual void registerItemSetsWithMonitor(PixelMonitor::utilslayer::Monitor &monitor);
    virtual void registerItemSetsWithWebServer(PixelMonitor::utilslayer::WebServer &webServer,
                                               PixelMonitor::utilslayer::Monitor &monitor);
    virtual std::string formatItem(PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec::const_iterator const &item) const;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_pixfed_PixFEDInfoSpaceHandler_h_
