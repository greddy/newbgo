#ifndef _PixelMonitor_pixfed_version_h_
#define _PixelMonitor_pixfed_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define PIXELMONITORPIXFED_VERSION_MAJOR 1
#define PIXELMONITORPIXFED_VERSION_MINOR 3
#define PIXELMONITORPIXFED_VERSION_PATCH 8

// If any previous versions available:
#define PIXELMONITORPIXFED_PREVIOUS_VERSIONS "1.3.7"
// else:
// #undef PIXELMONITORPIXFED_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define PIXELMONITORPIXFED_VERSION_CODE PACKAGE_VERSION_CODE(PIXELMONITORPIXFED_VERSION_MAJOR, PIXELMONITORPIXFED_VERSION_MINOR, PIXELMONITORPIXFED_VERSION_PATCH)
#ifndef PIXELMONITORPIXFED_PREVIOUS_VERSIONS
#define PIXELMONITORPIXFED_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PIXELMONITORPIXFED_VERSION_MAJOR, PIXELMONITORPIXFED_VERSION_MINOR, PIXELMONITORPIXFED_VERSION_PATCH)
#else
#define PIXELMONITORPIXFED_FULL_VERSION_LIST PIXELMONITORPIXFED_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PIXELMONITORPIXFED_VERSION_MAJOR, PIXELMONITORPIXFED_VERSION_MINOR, PIXELMONITORPIXFED_VERSION_PATCH)
#endif

namespace pixelmonitorpixfed {

const std::string package = "pixelmonitorpixfed";
const std::string versions = PIXELMONITORPIXFED_FULL_VERSION_LIST;
const std::string description = "CMS PIXELMONITOR helper software.";
const std::string authors = "Weinan Si";
const std::string summary = "CMS PixelMonitor helper software";
const std::string link = "-";
config::PackageInfo getPackageInfo();
void checkPackageDependencies();
std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace pixelmonitorpixfed

#endif
