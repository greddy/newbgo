#ifndef _PixelMonitor_pixfed_FEDConfigurationInfoSpaceHandler_h_
#define _PixelMonitor_pixfed_FEDConfigurationInfoSpaceHandler_h_

#include <string>
#include <vector>

#include "PixelMonitor/utilslayer/ConfigurationInfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/InfoSpaceHandler.h"

namespace PixelMonitor {
namespace utilslayer {
class Monitor;
}
}

namespace xdaq {
class Application;
}

namespace PixelMonitor {
namespace pixfed {

class FEDConfigurationInfoSpaceHandler : public PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler {

  public:
    FEDConfigurationInfoSpaceHandler(xdaq::Application *xdaqApp);
    virtual ~FEDConfigurationInfoSpaceHandler();

  protected:
    virtual void registerItemSetsWithMonitor(PixelMonitor::utilslayer::Monitor &monitor);
    virtual std::string formatItem(PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec::const_iterator const &item) const;
};

} // namespace pixfed
} // namespace PixelMonitor

#endif // _PixelMonitor_pixfed_FEDConfigurationInfoSpaceHandler_h_
