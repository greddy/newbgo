#ifndef _PixelMonitor_pixfed_WebTableFedErrInfo_h
#define _PixelMonitor_pixfed_WebTableFedErrInfo_h

#include <cstddef>
#include <string>

#include "PixelMonitor/utilslayer/WebObject.h"

namespace PixelMonitor {
namespace utilslayer {
class Monitor;
}

namespace pixfed {

class WebTableFedErrInfo : public PixelMonitor::utilslayer::WebObject {
  public:
    WebTableFedErrInfo(std::string const &name,
                       std::string const &description,
                       PixelMonitor::utilslayer::Monitor const &monitor,
                       std::string const &itemSetName,
                       std::string const &tabName,
                       size_t const colSpan);

    std::string getHTMLString() const;
};

} // namespace pixfed
} // namespace PixelMonitor

#endif // _PixelMonitor_pixfed_WebTableFedErrInfo_h
