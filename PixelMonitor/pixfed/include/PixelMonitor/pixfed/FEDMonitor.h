#ifndef _PixelMonitor_pixfed_FEDMonitor_h_
#define _PixelMonitor_pixfed_FEDMonitor_h_

#include <memory>
#include <string>
#include <vector>

#include "toolbox/Event.h"
#include "xoap/MessageReference.h"

#include "PixelMonitor/hwlayer/HwDeviceTCA.h"

#include "PixelMonitor/utilslayer/XDAQAppWithFSMBasic.h"
#include "PixelMonitor/utilslayer/Utils.h"

#include "PixelMonitor/pixfed/FEDConfigurationInfoSpaceHandler.h"

namespace xdaq {
class ApplicationStub;
}

namespace xgi {
class Input;
class Output;
}

namespace PixelMonitor {
namespace hwlayer {
class RegisterInfo;
}
}

namespace PixelMonitor {
namespace pixfed {

class PixFEDInfoSpaceHandler;
class PixFEDInfoSpaceUpdater;

class FEDMonitor : public PixelMonitor::utilslayer::XDAQAppWithFSMBasic {
    typedef std::unique_ptr<PixelMonitor::pixfed::PixFEDInfoSpaceUpdater> fedInfoSpaceUpdaterPtr;
    typedef std::unique_ptr<PixelMonitor::pixfed::PixFEDInfoSpaceHandler> fedInfoSpaceHandlerPtr;
    typedef PixelMonitor::pixfed::PixFEDInfoSpaceUpdater fedInfoSpaceUpdater;
    typedef PixelMonitor::pixfed::PixFEDInfoSpaceHandler fedInfoSpaceHandler;

  public:
    XDAQ_INSTANTIATOR();

    FEDMonitor(xdaq::ApplicationStub *stub);
    virtual ~FEDMonitor();

  protected:
    virtual void setupInfoSpaces();

    virtual PixelMonitor::hwlayer::HwDeviceTCA &getHw() const;

    virtual void hwConnectImpl();
    virtual void hwReleaseImpl();

  private:
    // InfoSpaces and corresponding InfoSpaceUpdaters
    fedInfoSpaceUpdaterPtr fedUpdaterP_;
    fedInfoSpaceHandlerPtr fedHandlerP_;
};
} // namespace pixfed
} // namespace PixelMonitor

#endif // _PixelMonitor_pixfed_FEDMonitor_h_
