#ifndef _PixelMonitor_pixfed_PixFEDInfoSpaceUpdater_h_
#define _PixelMonitor_pixfed_PixFEDInfoSpaceUpdater_h_

#include "PixelMonitor/utilslayer/InfoSpaceUpdater.h"
#include "PixelMonitor/hwlayer/HwDeviceTCA.h"
#include "PixelMonitor/pixfed/FEDReader.h"

namespace PixelMonitor {
namespace utilslayer {
class InfoSpaceHandler;
class InfoSpaceItem;
}
}

namespace PixelMonitor {
namespace hwlayer {
class HwDeviceTCA;
}
}

namespace PixelMonitor {
namespace pixfed {

class FEDReader;
class FEDMonitor;
class PixFEDInfoSpaceUpdater : public PixelMonitor::utilslayer::InfoSpaceUpdater {
  public:
    PixFEDInfoSpaceUpdater(PixelMonitor::hwlayer::HwDeviceTCA const &hw,
                           PixelMonitor::pixfed::FEDMonitor &xdaqApp);
    virtual ~PixFEDInfoSpaceUpdater();
    virtual bool updateInfoSpaceItem(PixelMonitor::utilslayer::InfoSpaceItem &item,
                                     PixelMonitor::utilslayer::InfoSpaceHandler *const infoSpaceHandler);

  protected:
    PixelMonitor::hwlayer::HwDeviceTCA const &getHw() const;
    virtual void updateInfoSpaceImpl(PixelMonitor::utilslayer::InfoSpaceHandler *const infoSpaceHandler);

  private:
    PixelMonitor::hwlayer::HwDeviceTCA const &hw_;
    PixelMonitor::pixfed::FEDMonitor &xdaqApp_;
};

uint32_t sumVecOfUint32(std::vector<uint32_t> vec);
} //namespace pixfed
} // namespace PixelMonitor

#endif // _PixelMonitor_pixfed_PixFEDInfoSpaceUpdater_h_
