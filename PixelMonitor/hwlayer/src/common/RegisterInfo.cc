#include "PixelMonitor/hwlayer/RegisterInfo.h"

PixelMonitor::hwlayer::RegisterInfo::RegisterInfo(std::string const &name,
                                                  bool const isReadable,
                                                  bool const isWritable)
    : name_(name),
      isReadable_(isReadable),
      isWritable_(isWritable) {
}

PixelMonitor::hwlayer::RegisterInfo::~RegisterInfo() {
}

std::string
PixelMonitor::hwlayer::RegisterInfo::name() const {
    return name_;
}

bool
PixelMonitor::hwlayer::RegisterInfo::isReadable() const {
    return isReadable_;
}

bool
PixelMonitor::hwlayer::RegisterInfo::isWritable() const {
    return isWritable_;
}
