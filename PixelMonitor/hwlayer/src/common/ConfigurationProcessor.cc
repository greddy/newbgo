#include "PixelMonitor/hwlayer/ConfigurationProcessor.h"

PixelMonitor::hwlayer::ConfigurationProcessor::ConfigurationProcessor() {
}

PixelMonitor::hwlayer::ConfigurationProcessor::~ConfigurationProcessor() {
}

PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec
PixelMonitor::hwlayer::ConfigurationProcessor::parse(std::string const &configurationString) const {
    return parseImpl(configurationString);
}

std::string
PixelMonitor::hwlayer::ConfigurationProcessor::compose(PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec const &hwConfiguration) const {
    return composeImpl(hwConfiguration);
}
