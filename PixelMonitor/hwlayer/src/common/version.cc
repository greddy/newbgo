#include "PixelMonitor/hwlayer/version.h"
#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"

#include "PixelMonitor/exception/version.h"

GETPACKAGEINFO(pixelmonitorhwlayer)

void
pixelmonitorhwlayer::checkPackageDependencies() {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(tcdsexception);
    CHECKDEPENDENCY(toolbox);
    CHECKDEPENDENCY(xcept);
}

std::set<std::string, std::less<std::string> >
pixelmonitorhwlayer::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, tcdsexception);
    ADDDEPENDENCY(dependencies, toolbox);
    ADDDEPENDENCY(dependencies, xcept);

    return dependencies;
}
