#ifndef _PixelMonitor_hwlayer_RegisterInfo_h_
#define _PixelMonitor_hwlayer_RegisterInfo_h_

#include <string>
#include <vector>

namespace PixelMonitor {
namespace hwlayer {

class RegisterInfo {

  public:
    typedef std::vector<PixelMonitor::hwlayer::RegisterInfo> RegInfoVec;

    struct RegInfoNameMatches {
        std::string const nameToMatch_;
        RegInfoNameMatches(std::string const &nameToMatch)
            : nameToMatch_(nameToMatch) {
        }
        bool operator()(RegisterInfo const &regInfo) const {
            return (regInfo.name() == nameToMatch_);
        }
    };

    // NOTE: This class has to span both uTCA and VME, that is: both
    // uhal and HAL. So it can only contain register properties
    // shared between these two systems.
    RegisterInfo(std::string const &name,
                 bool const isReadable,
                 bool const isWritable);
    ~RegisterInfo();

    std::string name() const;
    bool isReadable() const;
    bool isWritable() const;

  private:
    std::string name_;
    bool isReadable_;
    bool isWritable_;
};

} // namespace hwlayer
} // namespace PixelMonitor

#endif // _PixelMonitor_hwlayer_RegisterInfo_h_
