#ifndef _PixelMonitor_hwlayer_HwDeviceTCA_h_
#define _PixelMonitor_hwlayer_HwDeviceTCA_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

#include "PixelMonitor/hwlayer/IHwDevice.h"
#include "PixelMonitor/hwlayer/RegisterInfo.h"
// #include "PixelMonitor/hwlayer/ConfigurationProcessor.h"

namespace uhal {
class HwInterface;
}

namespace PixelMonitor {
namespace hwlayer {

/**
     * Hardware access class for (u)TCA devices.
     */
class HwDeviceTCA : public PixelMonitor::hwlayer::IHwDevice {
  public:
    typedef std::pair<PixelMonitor::hwlayer::RegisterInfo, std::vector<uint32_t> > RegContentsPair;
    typedef std::vector<RegContentsPair> RegContentsVec;

    HwDeviceTCA();
    virtual ~HwDeviceTCA();

    void hwConnect(std::string const &connectionName,
                   std::string const &connectionURI,
                   std::string const &addressTableFn);
    void hwConnect(std::string const &connectionsFileName,
                   std::string const &connectionName);
    void hwRelease();

    bool isHwConnected() const;

    uint32_t readRegister(std::string const &regName) const;
    void writeRegister(std::string const &regName,
                       uint32_t const regVal) const;
    std::vector<uint32_t> readBlock(std::string const &regName,
                                    uint32_t const nWords) const;
    std::vector<uint32_t> readBlock(std::string const &regName) const;
    void writeBlock(std::string const &regName,
                    std::vector<uint32_t> const regVal) const;

    uint32_t readModifyWriteRegister(std::string const &regName,
                                     uint32_t const regVal) const;

    uint32_t getBlockSize(std::string const &regName) const;

    std::vector<std::string> getRegisterNames() const;
    PixelMonitor::hwlayer::RegisterInfo::RegInfoVec getRegisterInfos() const;

    // void writeHardwareConfiguration(PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const;
    virtual RegContentsVec readHardwareConfiguration(PixelMonitor::hwlayer::RegisterInfo::RegInfoVec const &regInfos) const;

    virtual PixelMonitor::hwlayer::RegDumpVec dumpRegisterContents() const;

  protected:
    uhal::HwInterface &getHwInterface() const;

  private:
    std::unique_ptr<uhal::HwInterface> hwP_;
};

} // namespace hwlayer
} // namespace PixelMonitor

#endif // _PixelMonitor_hwlayer_HwDeviceTCA_h_
