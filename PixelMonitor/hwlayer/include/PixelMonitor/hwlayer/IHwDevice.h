#ifndef _PixelMonitor_hwlayer_IHwDevice_h_
#define _PixelMonitor_hwlayer_IHwDevice_h_

#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

namespace PixelMonitor {
namespace hwlayer {

typedef std::pair<std::string, std::vector<uint32_t> > RegDumpPair;
typedef std::vector<RegDumpPair> RegDumpVec;

/**
     * Interface class for all hardware access classes.
     */
class IHwDevice {

  public:
    /**
         Generic read/write access.
       */
    virtual uint32_t readRegister(std::string const &regName) const = 0;
    virtual void writeRegister(std::string const &regName,
                               uint32_t const regVal) const = 0;

    virtual std::vector<uint32_t> readBlock(std::string const &regName,
                                            uint32_t const nWords) const = 0;
    virtual void writeBlock(std::string const &regName,
                            std::vector<uint32_t> const regVals) const = 0;

    virtual RegDumpVec dumpRegisterContents() const = 0;

  protected:
    /**
       * @note
       * Protected constructor since this is an abstract base class.
       */
    IHwDevice() {};

    /**
       * @note
       * Protected, non-virtual destructor since this class is not
       * intended for polymophic destruction.
       */
    ~IHwDevice() {};
};

} // namespace hwlayer
} // namespace PixelMonitor

#endif // _PixelMonitor_hwlayer_IHwDevice_h_
