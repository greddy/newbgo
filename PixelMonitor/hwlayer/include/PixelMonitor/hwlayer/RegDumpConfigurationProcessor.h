#ifndef _PixelMonitor_hwlayer_RegDumpConfigurationProcessor_h_
#define _PixelMonitor_hwlayer_RegDumpConfigurationProcessor_h_

#include <functional>
#include <string>

#include "PixelMonitor/hwlayer/ConfigurationProcessor.h"

namespace PixelMonitor {
namespace hwlayer {

class RegDumpConfigurationProcessor : public ConfigurationProcessor {

  public:
    static char const kCommentChar = '#';

    RegDumpConfigurationProcessor();
    virtual ~RegDumpConfigurationProcessor();

  protected:
    virtual RegValVec parseImpl(std::string const &configurationString) const;
    virtual std::string composeImpl(PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec const &hwConfiguration) const;

  private:
    struct stringCmp : std::binary_function<std::string, std::string, bool> {
        bool operator()(std::string const &a, std::string const &b) {
            return (a.size() < b.size());
        }
    };
};

} // namespace hwlayer
} // namespace PixelMonitor

#endif // _PixelMonitor_hwlayer_RegDumpConfigurationProcessor_h_
