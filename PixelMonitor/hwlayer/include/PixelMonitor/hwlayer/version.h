#ifndef _PixelMonitor_hwlayer_version_h_
#define _PixelMonitor_hwlayer_version_h

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define PIXELMONITORHWLAYER_VERSION_MAJOR 1
#define PIXELMONITORHWLAYER_VERSION_MINOR 0
#define PIXELMONITORHWLAYER_VERSION_PATCH 0

// If any previous versions available:
// #define PIXELMONITORHWLAYER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef PIXELMONITORHWLAYER_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define PIXELMONITORHWLAYER_VERSION_CODE PACKAGE_VERSION_CODE(PIXELMONITORHWLAYER_VERSION_MAJOR, PIXELMONITORHWLAYER_VERSION_MINOR, PIXELMONITORHWLAYER_VERSION_PATCH)
#ifndef PIXELMONITORHWLAYER_PREVIOUS_VERSIONS
#define PIXELMONITORHWLAYER_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PIXELMONITORHWLAYER_VERSION_MAJOR, PIXELMONITORHWLAYER_VERSION_MINOR, PIXELMONITORHWLAYER_VERSION_PATCH)
#else
#define PIXELMONITORHWLAYER_FULL_VERSION_LIST PIXELMONITORHWLAYER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PIXELMONITORHWLAYER_VERSION_MAJOR, PIXELMONITORHWLAYER_VERSION_MINOR, PIXELMONITORHWLAYER_VERSION_PATCH)
#endif

namespace pixelmonitorhwlayer {

const std::string package = "pixelmonitorhwlayer";
const std::string versions = PIXELMONITORHWLAYER_FULL_VERSION_LIST;
const std::string description = "CMS PIXELMONITOR helper software.";
const std::string authors = "Jeroen Hegeman, Weinan Si";
const std::string summary = "Part of the CMS TCDS software, modified for PixelMonitor";
const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
config::PackageInfo getPackageInfo();
void checkPackageDependencies();
std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace pixelmonitorhwlayer

#endif
