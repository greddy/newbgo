#ifndef _PixelMonitor_utilslayer_InfoSpaceItem_h_
#define _PixelMonitor_utilslayer_InfoSpaceItem_h_

#include <string>

namespace PixelMonitor {
namespace utilslayer {

class InfoSpaceItem {

  public:
    enum ItemType {
        STRING,
        TIMEVAL,
        UINT32,
        UINT64,
        BOOL,
        DOUBLE,
        FLOAT,
        STRINGVEC,
        UINT32VEC
    };
    enum UpdateType {
        HW32,
        HW64,
        HWBOOL,
        PROCESS,
        TRACKER,
        CONFIGURATION,
        NOUPDATE
    };

    InfoSpaceItem(std::string const &name,
                  ItemType const type,
                  std::string const &format,
                  UpdateType const updateType,
                  bool const isValid);

    std::string name() const;
    ItemType type() const;
    std::string format() const;
    UpdateType updateType() const;
    bool isValid() const;
    void setValid();
    void setInvalid();

    friend bool operator==(InfoSpaceItem const &item, std::string const &name);
    friend bool operator==(std::string const &name, InfoSpaceItem const &item);

  private:
    std::string name_;
    ItemType type_;
    std::string format_;
    UpdateType updateType_;
    bool isValid_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_InfoSpaceItem_h_
