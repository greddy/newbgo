#ifndef _PixelMonitor_utilslayer_Utils_h_
#define _PixelMonitor_utilslayer_Utils_h_

#include <cctype>
#include <map>
#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

#include "toolbox/TimeVal.h"

#include "PixelMonitor/utilslayer/ConfigurationInfoSpaceHandler.h"
#include "PixelMonitor/hwlayer/HwDeviceTCA.h"

namespace PixelMonitor {
namespace hwlayer {
class HwDeviceTCA;
}
}

namespace PixelMonitor {
namespace utilslayer {
class ConfigurationInfoSpaceHandler;
}
}

namespace PixelMonitor {
namespace utilslayer {

// This expands environment variables in path names.
std::string expandPathName(std::string const &pathNameIn);

// Some misc. string methods.
struct convertUp {
    char operator()(char c) {
        return ::toupper((unsigned char)c);
    }
};

struct convertDown {
    char operator()(char c) {
        return ::tolower((unsigned char)c);
    }
};

std::string escapeAsJSONString(std::string const &stringIn);
std::string jsquote(std::string const &stringIn);
std::string trimString(std::string const &stringToTrim,
                       std::string const &whitespace = " \t\r\n");

std::string capitalizeString(std::string const &stringIn);

// A method to nicely write time values.
std::string formatTimestamp(toolbox::TimeVal const timestamp);

// A method to nicely write time differences.
std::string formatDeltaTString(toolbox::TimeVal const timeBegin,
                               toolbox::TimeVal const timeEnd);

// A little helper to (zlib) compress a string.
std::string compressString(std::string const stringIn);

unsigned int countTrailingZeros(uint32_t const val);

void HwDeviceConnectImpl(PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler const &cfgInfoSpace,
                         PixelMonitor::hwlayer::HwDeviceTCA &hw);

// execute system command and get stdout output
std::string exec(const char *cmd);
} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_Utils_h_
