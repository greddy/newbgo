#ifndef _PixelMonitor_utilslayer_AppHist_h_
#define _PixelMonitor_utilslayer_AppHist_h_

#include <deque>
#include <string>

#include "PixelMonitor/utilslayer/AppHistItem.h"

namespace PixelMonitor {
namespace utilslayer {

class AppHist {

  public:
    AppHist();
    ~AppHist();

    void addItem(std::string const &msg);

    std::string getJSONString() const;

  private:
    static unsigned const kMaxHistSize = 1000;
    std::deque<PixelMonitor::utilslayer::AppHistItem> history_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_AppHistItem_h_
