#ifndef _PixelMonitor_utilslayer_WebTableMultiCol_h
#define _PixelMonitor_utilslayer_WebTableMultiCol_h

#include <cstddef>
#include <string>
#include <vector>

#include "PixelMonitor/utilslayer/WebObject.h"

namespace PixelMonitor {
namespace utilslayer {

class Monitor;

bool sortStringPairVector(const std::pair<std::string, std::string> &p1,
                          const std::pair<std::string, std::string> &p2);

class WebTableMultiCol : public WebObject {

  public:
    WebTableMultiCol(std::string const &name,
                     std::string const &description,
                     Monitor const &monitor,
                     std::vector<std::string> const &itemSetNameVec,
                     std::string const &tabName,
                     size_t const colSpan);

    std::string getHTMLString() const;
    std::string getHTMLthead() const;
    std::string getHTMLtbody() const;
    bool checked;

  private:
    bool checkItemSets() const;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_WebTableMultiCol_h
