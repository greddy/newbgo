#ifndef _PixelMonitor_utilslayer_InfoSpaceUpdaterPlain_h_
#define _PixelMonitor_utilslayer_InfoSpaceUpdaterPlain_h_

#include "PixelMonitor/utilslayer/InfoSpaceUpdater.h"

namespace PixelMonitor {
namespace utilslayer {

class InfoSpaceUpdaterPlain : public PixelMonitor::utilslayer::InfoSpaceUpdater {

  public:
    InfoSpaceUpdaterPlain();
    virtual ~InfoSpaceUpdaterPlain();
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_InfoSpaceUpdaterPlain_h_
