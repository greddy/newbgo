#ifndef _PixelMonitor_utilslayer_ConfigurationInfoSpaceHandler_h_
#define _PixelMonitor_utilslayer_ConfigurationInfoSpaceHandler_h_

#include <string>

// OBSOLETE OBSOLETE OBSOLETE
/* #include "xdata/ActionListener.h" */
// OBSOLETE OBSOLETE OBSOLETE end

#include "PixelMonitor/utilslayer/InfoSpaceHandler.h"

namespace xdaq {
class Application;
}

// OBSOLETE OBSOLETE OBSOLETE
/* namespace xdata { */
/*   class Event; */
/* } */
// OBSOLETE OBSOLETE OBSOLETE end

namespace PixelMonitor {
namespace utilslayer {

class Monitor;
class WebServer;

/**
     * Abstract base class handling the XDAQ application's
     * configuration items.
     *
     * @note
     * The items in this InfoSpace are not updated by an
     * InfoSpaceUpdater, but by the usual XDAQ configuration methods.
     */
class ConfigurationInfoSpaceHandler : public PixelMonitor::utilslayer::InfoSpaceHandler
                                      // OBSOLETE OBSOLETE OBSOLETE
                                      //      public xdata::ActionListener
                                      // OBSOLETE OBSOLETE OBSOLETE end
                                      {

  public:
    virtual ~ConfigurationInfoSpaceHandler();

  protected:
    ConfigurationInfoSpaceHandler(xdaq::Application *const xdaqApp,
                                  std::string const &name = "PixelMonitor-application-config");

    // OBSOLETE OBSOLETE OBSOLETE
    /* virtual void actionPerformed(xdata::Event& event); */
    // OBSOLETE OBSOLETE OBSOLETE end

    /**
       * Register itemsets in this InfoSpace with a monitor
       * object. Obviously needs to be implemented by the derived
       * class itself.
       */
    virtual void registerItemSetsWithMonitor(Monitor &monitor);

    /**
       * Register itemsets in this InfoSpace with a webserver
       * object. Obviously needs to be implemented by the derived
       * class itself.
       */
    virtual void registerItemSetsWithWebServer(WebServer &webServer,
                                               Monitor &monitor);
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_ConfigurationInfoSpaceHandler_h_
