#ifndef _PixelMonitor_utilslayer_InfoSpaceUpdater_h_
#define _PixelMonitor_utilslayer_InfoSpaceUpdater_h_

namespace PixelMonitor {
namespace utilslayer {

class InfoSpaceHandler;
class InfoSpaceItem;

/**
     * Abstract base class specifying the basic behaviour for
     * InfoSpace updaters.
     */
class InfoSpaceUpdater {

  public:
    virtual ~InfoSpaceUpdater();

    void updateInfoSpace(InfoSpaceHandler *const infoSpaceHandler);

  protected:
    /**
       * @note
       * Protected constructor since this is an abstract base class.
       */
    InfoSpaceUpdater();

    virtual void updateInfoSpaceImpl(InfoSpaceHandler *const infoSpaceHandler);

    virtual bool updateInfoSpaceItem(InfoSpaceItem &item,
                                     InfoSpaceHandler *const infoSpaceHandler);
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_InfoSpaceUpdater_h_
