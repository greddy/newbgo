#ifndef _PixelMonitor_utilslayer_Lock_h_
#define _PixelMonitor_utilslayer_Lock_h_

#include "toolbox/BSem.h"

namespace PixelMonitor {
namespace utilslayer {

class Lock {

  public:
    Lock(toolbox::BSem::State state = toolbox::BSem::EMPTY, bool recursive = true);
    ~Lock();

    void lock();
    void unlock();

  private:
    toolbox::BSem semaphore_;

    // Prevent copying.
    Lock(Lock const &);
    Lock &operator=(Lock const &);
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_Lock_h_
