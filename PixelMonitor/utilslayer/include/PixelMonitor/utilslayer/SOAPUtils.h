#ifndef _PixelMonitor_utilslayer_SOAPUtils_h
#define _PixelMonitor_utilslayer_SOAPUtils_h

#include <stdint.h>
#include <string>
#include <vector>

#include "xoap/MessageReference.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPName.h"

namespace PixelMonitor {
namespace utilslayer {

namespace soap {

// NOTE: The 'xdaq' namespace is determined by the fact that
// this is the namespace used by RunControl.

// Enum definitions for SOAP fault-code types. Defined in SOAP v1.2-speak:
// - SOAPFaultSender for v1.1 'Client' and v1.2 'Sender',
// - SOAPFaultReceiver for v1.1 'Server' and v1.2 'Receiver'.
enum SOAPFaultCodeType {
    SOAPFaultCodeSender,
    SOAPFaultCodeReceiver
};

xoap::MessageReference makeParameterGetSOAPCmd(std::string const &className,
                                               std::string const &parName,
                                               std::string const &parType);

xoap::MessageReference makeSOAPFaultReply(xoap::MessageReference const &msg,
                                          SOAPFaultCodeType const faultCode,
                                          std::string const &faultString,
                                          std::string const &faultDetail = "");
xoap::MessageReference makeCommandSOAPReply(xoap::MessageReference const &msg,
                                            std::string const &fsmState = "");
xoap::MessageReference makeCommandSOAPReply(xoap::MessageReference const &msg,
                                            std::string const &replyMsg,
                                            std::string const &fsmState = "");

xoap::MessageReference makeSOAPFaultReply(std::string const &soapProtocolVersion,
                                          SOAPFaultCodeType const faultCode,
                                          std::string const &faultString,
                                          std::string const &faultDetail = "");
xoap::MessageReference makeCommandSOAPReply(std::string const &soapProtocolVersion,
                                            std::string const &command,
                                            std::string const &fsmState);
xoap::MessageReference makeCommandSOAPReply(std::string const &soapProtocolVersion,
                                            std::string const &command,
                                            std::string const &replyMsg,
                                            std::string const &fsmState);

std::string extractSOAPCommandName(xoap::MessageReference const &msg);
std::string extractSOAPCommandRequestorId(xoap::MessageReference const &msg);
std::string extractSOAPCommandRCMSURL(xoap::MessageReference const &msg);

bool hasFault(xoap::MessageReference const &msg);
bool hasFaultDetail(xoap::MessageReference const &msg);
std::string extractFaultString(xoap::MessageReference const &msg);
std::string extractFaultDetail(xoap::MessageReference const &msg);

bool hasSOAPCommandParameter(xoap::MessageReference const &msg,
                             std::string const &parameterName);
bool extractSOAPCommandParameterBool(xoap::MessageReference const &msg,
                                     std::string const &parameterName);
std::string extractSOAPCommandParameterString(xoap::MessageReference const &msg,
                                              std::string const &parameterName);
uint32_t extractSOAPCommandParameterUnsignedInteger(xoap::MessageReference const &msg,
                                                    std::string const &parameterName);

xoap::MessageReference makePSXDPGetSOAPCmd(std::string const &dpName);

std::string extractPSXDPGetReply(xoap::MessageReference &msg,
                                 std::string const &dpName);

xoap::SOAPElement extractSOAPCommandParameterElement(xoap::MessageReference const &msg,
                                                     std::string const &parameterName);
void addSOAPReplyParameter(xoap::MessageReference const &msg,
                           std::string const &parameterName,
                           bool const val);
void addSOAPReplyParameter(xoap::MessageReference const &msg,
                           std::string const &parameterName,
                           std::string const &val);
void addSOAPReplyParameter(xoap::MessageReference const &msg,
                           std::string const &parameterName,
                           uint32_t const val);

/**
       * Extract all child nodes of the body element of a message. Two
       * methods:
       * - extractBodyNode() for when only a single node is allowed,
       * - extractBodyNodes() for when more than one node is allowed.
       */
xoap::SOAPElement extractBodyNode(xoap::MessageReference const &msg);
std::vector<xoap::SOAPElement> extractBodyNodes(xoap::MessageReference const &msg);

/**
       * Starting from a given node, extract its child node with the
       * specified name. Only one such child node is allowed.
       */
xoap::SOAPElement extractChildNode(xoap::SOAPElement &node,
                                   xoap::SOAPName &childName);

} // namespace soap

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_SOAPUtils_h
