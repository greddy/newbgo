#ifndef _PixelMonitor_utilslayer_WebTable_h
#define _PixelMonitor_utilslayer_WebTable_h

#include <cstddef>
#include <string>

#include "PixelMonitor/utilslayer/WebObject.h"

namespace PixelMonitor {
namespace utilslayer {

class Monitor;

class WebTable : public WebObject {

  public:
    WebTable(std::string const &name,
             std::string const &description,
             Monitor const &monitor,
             std::string const &itemSetName,
             std::string const &tabName,
             size_t const colSpan);

    std::string getHTMLString() const;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_WebTable_h
