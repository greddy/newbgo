#ifndef _PixelMonitor_utilslayer_WebServer_h
#define _PixelMonitor_utilslayer_WebServer_h

#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"

#include "PixelMonitor/utilslayer/Layout.h"
#include "PixelMonitor/utilslayer/TCDSXDAQObject.h"
#include "PixelMonitor/utilslayer/Uncopyable.h"
#include "PixelMonitor/utilslayer/WebObject.h"
#include "PixelMonitor/utilslayer/WebTab.h"

namespace log4cplus {
class Logger;
}

namespace xgi {
class Input;
class Output;
}

namespace PixelMonitor {
namespace utilslayer {

class Monitor;
class XDAQAppBase;

class WebServer : public xgi::framework::UIManager,
                  public TCDSXDAQObject,
                  private Uncopyable

                  /**
     * Webserver class: allows access to all monitorable content in
     * the TCDS XDAQ applications.
     *
     * The HTML produced by this class is based on HTML5 BoilerPlate
     * (http://html5boilerplate.com/), via a bit of playing and
     * creative copy-paste from initializr
     * (http://www.initializr.com/).
     */
                  {

  public:
    WebServer(XDAQAppBase *const xdaqApp);
    ~WebServer();

    void registerTab(std::string const &name,
                     std::string const &description,
                     size_t const numColumns);
    template <class O, class I>
    void registerWebObject(std::string const &name,
                           std::string const &description,
                           Monitor const &monitor,
                           I const &itemSetName,
                           std::string const &tabName,
                           size_t const colSpan = 1);
    void registerTable(std::string const &name,
                       std::string const &description,
                       Monitor const &monitor,
                       std::string const &itemSetName,
                       std::string const &tabName,
                       size_t const colSpan = 1);
    void registerMultiColTable(std::string const &name,
                               std::string const &description,
                               Monitor const &monitor,
                               std::vector<std::string> const &itemSetNameVec,
                               std::string const &tabName,
                               size_t const colSpan = 1);
    void registerSpacer(std::string const &name,
                        std::string const &description,
                        Monitor const &monitor,
                        std::string const &itemSetName,
                        std::string const &tabName,
                        size_t const colSpan = 1);

    void jsonUpdate(xgi::Input *const in, xgi::Output *const out) throw(xgi::exception::Exception);
    void monitoringWebPage(xgi::Input *const in, xgi::Output *const out) throw(xgi::exception::Exception);

  private:
    /**
       * Returns the class name of the owner XDAQ application (without
       * namespaces).
       */
    std::string getApplicationName() const;

    std::string getServiceName() const;

    WebTab &getTab(std::string const &tabName) const;
    bool tabExists(std::string const &tabName) const;

    void printTabs(xgi::Output *const out) const;

    void jsonUpdateCore(xgi::Input *const in, xgi::Output *const out) const;
    void monitoringWebPageCore(xgi::Input *const in, xgi::Output *const out) const;

    log4cplus::Logger &logger_;
    std::vector<WebTab *> tabs_;
    PixelMonitor::utilslayer::Layout layout_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#include "PixelMonitor/utilslayer/WebServer.hxx"

#endif // _PixelMonitor_utilslayer_WebServer_h
