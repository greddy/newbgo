#ifndef _PixelMonitor_utilslayer_AppHistItem_h_
#define _PixelMonitor_utilslayer_AppHistItem_h_

#include <string>

#include "toolbox/TimeVal.h"

namespace PixelMonitor {
namespace utilslayer {

class AppHistItem {

  public:
    AppHistItem(std::string const &msg);
    ~AppHistItem();
    void increaseCount();

    toolbox::TimeVal timestamp() const;
    std::string message() const;
    unsigned int count() const;

  private:
    toolbox::TimeVal const timestamp_;
    std::string const msg_;
    unsigned int count_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_AppHistItem_h_
