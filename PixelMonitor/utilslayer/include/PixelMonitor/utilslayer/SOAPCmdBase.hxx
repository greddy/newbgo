#include "log4cplus/loggingmacros.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/Method.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/utilslayer/SOAPUtils.h"

template <class T>
PixelMonitor::utilslayer::SOAPCmdBase<T>::SOAPCmdBase(T& controller,
                                                      std::string const& commandName,
                                                      bool const requiresHwLease) :
  controller_(controller),
  commandName_(commandName),
  requiresHwLease_(requiresHwLease)
{
  // Bind this as SOAP command.
  xoap::deferredbind(&(this->controller_), this, &SOAPCmdBase<T>::execute,
                     commandName, XDAQ_NS_URI);
}

template <class T>
PixelMonitor::utilslayer::SOAPCmdBase<T>::~SOAPCmdBase()
{
}

template <class T>
std::string
PixelMonitor::utilslayer::SOAPCmdBase<T>::commandName() const
{
  return commandName_;
}

template <class T>
xoap::MessageReference
PixelMonitor::utilslayer::SOAPCmdBase<T>::execute(xoap::MessageReference msg)
{
  xoap::MessageReference reply;
  bool executeCommand = false;

  std::string requestorId = "";
  if (requiresHwLease_)
    {
      // First check if the request comes from the hardware owner or
      // not.
      requestorId = PixelMonitor::utilslayer::soap::extractSOAPCommandRequestorId(msg);
      if (requestorId.empty())
        {
          // No requestor ID -> send error back.
          std::string const commandName =
            PixelMonitor::utilslayer::soap::extractSOAPCommandName(msg);
          std::string msgBase =
            toolbox::toString("SOAP command '%s' not allowed "
                              "without requestor ID.",
                              commandName.c_str());
          LOG4CPLUS_ERROR(controller_.getApplicationLogger(), msgBase);
          XCEPT_DECLARE(PixelMonitor::exception::FSMTransitionProblem, top, msgBase);
          controller_.notifyQualified("error", top);
          PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode =
            PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
          std::string faultString = msgBase;
          reply = PixelMonitor::utilslayer::soap::makeSOAPFaultReply(msg, faultCode, faultString);

          // Keep track of history.
          std::string const histMsg =
            toolbox::toString("Ignored '%s' SOAP command without requestor ID",
                              commandName_.c_str());
          controller_.appStateInfoSpace_.addHistoryItem(histMsg);
        }
      else if (!controller_.isHwLeased())
        {
          // Hardware is not owned by anyone -> send error back.
          std::string const commandName =
            PixelMonitor::utilslayer::soap::extractSOAPCommandName(msg);
          std::string msgBase =
            toolbox::toString("SOAP command '%s' not allowed "
                              "without valid hardware lease.",
                              commandName.c_str());
          LOG4CPLUS_ERROR(controller_.getApplicationLogger(), msgBase);
          XCEPT_DECLARE(PixelMonitor::exception::FSMTransitionProblem, top, msgBase);
          controller_.notifyQualified("error", top);
          PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode =
            PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
          std::string faultString = msgBase;
          reply = PixelMonitor::utilslayer::soap::makeSOAPFaultReply(msg, faultCode, faultString);

          // Keep track of history.
          std::string const histMsg =
            toolbox::toString("Ignored '%s' SOAP command from '%s'",
                              commandName_.c_str(),
                              requestorId.c_str());
          controller_.appStateInfoSpace_.addHistoryItem(histMsg);
        }
      else
        {
          std::string const leaseOwnerId = controller_.getHwLeaseOwnerId();
          if (requestorId != leaseOwnerId)
            {
              // No match -> send error back.
              std::string const commandName =
                PixelMonitor::utilslayer::soap::extractSOAPCommandName(msg);
              std::string msgBase =
                toolbox::toString("SOAP command '%s' not allowed: "
                                  "hardware lease owned by session '%s'.",
                                  commandName.c_str(),
                                  leaseOwnerId.c_str());
              LOG4CPLUS_ERROR(controller_.getApplicationLogger(), msgBase);
              XCEPT_DECLARE(PixelMonitor::exception::FSMTransitionProblem, top, msgBase);
              controller_.notifyQualified("error", top);
              PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode =
                PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
              std::string faultString = msgBase;
              reply = PixelMonitor::utilslayer::soap::makeSOAPFaultReply(msg, faultCode, faultString);

              // Keep track of history.
              std::string const histMsg =
                toolbox::toString("Ignored '%s' SOAP command from '%s' since hardware is owned by '%s'",
                                  commandName_.c_str(),
                                  requestorId.c_str(),
                                  leaseOwnerId.c_str());
              controller_.appStateInfoSpace_.addHistoryItem(histMsg);
            }
          else
            {
              executeCommand = true;
            }
        }
    }
  else
    {
      // No valid hardware lease required.
      executeCommand = true;
    }

  // The command has been accepted. Renew the hardware lease and
  // execute the command.
  if (executeCommand)
    {
      if (requiresHwLease_)
        {
          controller_.renewHwLease();
        }

      try
        {
          // Keep track of history.
          std::string const histMsg =
            toolbox::toString("Executing '%s' SOAP command",
                              commandName_.c_str());
          controller_.appStateInfoSpace_.addHistoryItem(histMsg);
          // Execute command.
          reply = executeImpl(msg);
        }
      catch (PixelMonitor::exception::Exception& err)
        {
          PixelMonitor::utilslayer::soap::SOAPFaultCodeType const faultCode =
            PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
          std::string const commandName =
            PixelMonitor::utilslayer::soap::extractSOAPCommandName(msg);
          std::string const msgBase =
            toolbox::toString("Problem executing SOAP command '%s'.", commandName.c_str());
          std::string const faultString = msgBase;
          std::string const faultDetail = err.message().c_str();
          reply = PixelMonitor::utilslayer::soap::makeSOAPFaultReply(msg,
                                                                     faultCode,
                                                                     faultString,
                                                                     faultDetail);

          // Keep track of history.
          std::string histMsg = "";
          if (!requestorId.empty())
            {
              histMsg = toolbox::toString("Failed to execute '%s' SOAP command from '%s': '%s'",
                                          commandName_.c_str(),
                                          requestorId.c_str(),
                                          faultDetail.c_str());
            }
          else
            {
              histMsg = toolbox::toString("Failed to execute '%s' SOAP command: '%s'",
                                          commandName_.c_str(),
                                          faultDetail.c_str());
            }
          controller_.appStateInfoSpace_.addHistoryItem(histMsg);
        }
    }

  return reply;
}
