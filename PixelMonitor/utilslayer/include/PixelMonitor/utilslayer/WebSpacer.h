#ifndef _PixelMonitor_utilslayer_WebSpacer_h
#define _PixelMonitor_utilslayer_WebSpacer_h

#include <cstddef>
#include <string>

#include "PixelMonitor/utilslayer/WebObject.h"

namespace PixelMonitor {
namespace utilslayer {

class Monitor;

class WebSpacer : public WebObject {

  public:
    WebSpacer(std::string const &name,
              std::string const &description,
              Monitor const &monitor,
              std::string const &itemSetName,
              std::string const &tabName,
              size_t const colSpan);

    virtual std::string getHTMLString() const;
    virtual std::string getJSONString() const;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_WebSpacer_h
