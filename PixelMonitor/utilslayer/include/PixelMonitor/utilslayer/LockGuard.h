#ifndef _PixelMonitor_utilslayer_LockGuard_h_
#define _PixelMonitor_utilslayer_LockGuard_h_

namespace PixelMonitor {
namespace utilslayer {

template <class L>
class LockGuard {

  public:
    LockGuard(L &lock);
    ~LockGuard();

  private:
    L &lock_;

    // Prevent copying.
    LockGuard(LockGuard const &);
    LockGuard &operator=(LockGuard const &);
};

} // namespace utilslayer
} // namespace PixelMonitor

template <class L>
PixelMonitor::utilslayer::LockGuard<L>::LockGuard(L &lock)
    : lock_(lock) {
          lock_.lock();
}

template <class L>
PixelMonitor::utilslayer::LockGuard<L>::~LockGuard() {
    lock_.unlock();
}

#endif // _PixelMonitor_utilslayer_LockGuard_h_
