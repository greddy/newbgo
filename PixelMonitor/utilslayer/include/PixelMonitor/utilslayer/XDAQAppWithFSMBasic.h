#ifndef _PixelMonitor_utilslayer_XDAQAppWithFSMBasic_h_
#define _PixelMonitor_utilslayer_XDAQAppWithFSMBasic_h_

#include <memory>

#include "PixelMonitor/utilslayer/XDAQAppWithFSMBase.h"

namespace PixelMonitor {
namespace hwlayer {
class HwDeviceTCA;
}
}

namespace xdaq {
class ApplicationStub;
}

namespace PixelMonitor {
namespace utilslayer {

class XDAQAppWithFSMBasic : public XDAQAppWithFSMBase {

  public:
    virtual ~XDAQAppWithFSMBasic();

  protected:
    /**
       * Abstract class, so protected constructor.
       */
    XDAQAppWithFSMBasic(xdaq::ApplicationStub *const stub,
                        std::unique_ptr<PixelMonitor::hwlayer::HwDeviceTCA> hw);
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_XDAQAppWithFSMBasic_h_
