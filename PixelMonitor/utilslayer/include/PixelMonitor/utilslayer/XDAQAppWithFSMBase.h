#ifndef _PixelMonitor_utilslayer_XDAQAppWithFSMBase_h_
#define _PixelMonitor_utilslayer_XDAQAppWithFSMBase_h_

#include <memory>

#include "toolbox/Event.h"
#include "xoap/MessageReference.h"

#include "PixelMonitor/utilslayer/FSM.h"
#include "PixelMonitor/utilslayer/XDAQAppBase.h"

namespace PixelMonitor {
namespace hwlayer {
class HwDeviceTCA;
}
}

namespace xdaq {
class ApplicationStub;
}

namespace PixelMonitor {
namespace utilslayer {

class XDAQAppWithFSMBase : public XDAQAppBase {

    /**
       * The FSM is a friend so it has access to the
       * appStateInfoSpace_;
       */
    friend class FSM;

  public:
    virtual ~XDAQAppWithFSMBase();

    /**
       * These are the usual state transitions.
       */
    void coldResetAction(toolbox::Event::Reference event);
    void configureAction(toolbox::Event::Reference event);
    void enableAction(toolbox::Event::Reference event);
    void failAction(toolbox::Event::Reference event);
    void haltAction(toolbox::Event::Reference event);
    void pauseAction(toolbox::Event::Reference event);
    void resumeAction(toolbox::Event::Reference event);
    void stopAction(toolbox::Event::Reference event);

    /**
       * These two are special TTC-action state transitions from
       * Paused via an intermediate action state back to Paused.
       */
    void ttcHardResetAction(toolbox::Event::Reference event);
    void ttcResyncAction(toolbox::Event::Reference event);

    void zeroAction(toolbox::Event::Reference event);

    xoap::MessageReference changeState(xoap::MessageReference msg);

  protected:
    /**
       * Abstract class, so protected constructor.
       */
    XDAQAppWithFSMBase(xdaq::ApplicationStub *const stub,
                       std::unique_ptr<PixelMonitor::hwlayer::HwDeviceTCA> hw);

    virtual void coldResetActionImpl(toolbox::Event::Reference event);
    virtual void configureActionImpl(toolbox::Event::Reference event);
    virtual void enableActionImpl(toolbox::Event::Reference event);
    virtual void failActionImpl(toolbox::Event::Reference event);
    virtual void haltActionImpl(toolbox::Event::Reference event);
    virtual void pauseActionImpl(toolbox::Event::Reference event);
    virtual void resumeActionImpl(toolbox::Event::Reference event);
    virtual void stopActionImpl(toolbox::Event::Reference event);

    virtual void ttcHardResetActionImpl(toolbox::Event::Reference event);
    virtual void ttcResyncActionImpl(toolbox::Event::Reference event);

    virtual void zeroActionImpl(toolbox::Event::Reference event);

    virtual xoap::MessageReference changeStateImpl(xoap::MessageReference msg);

    /**
       * The hardware-level equivalents of the state transitions.
       */
    void hwColdReset();
    virtual void hwColdResetImpl();
    void hwConfigure();
    virtual void hwConfigureImpl();
    // BUG BUG BUG
    // Missing a few here.
    // BUG BUG BUG end

    /**
       * Some configuration helpers. These methods are called just
       * before and just after applying a configuration to the
       * hardware.
       */
    void hwCfgInitialize();
    void hwCfgFinalize();
    virtual void hwCfgInitializeImpl();
    virtual void hwCfgFinalizeImpl();

  private:
    FSM fsm_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_XDAQAppWithFSMBase_h_
