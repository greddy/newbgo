#ifndef _PixelMonitor_utilslayer_TCDSXDAQObject_h_
#define _PixelMonitor_utilslayer_TCDSXDAQObject_h_

namespace PixelMonitor {
namespace utilslayer {

class XDAQAppBase;

class TCDSXDAQObject {

  public:
    virtual ~TCDSXDAQObject();
    XDAQAppBase *getOwnerApplication() const;

  protected:
    TCDSXDAQObject(XDAQAppBase *const owner);

  private:
    XDAQAppBase *const ownerP_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_TCDSXDAQObject_h_
