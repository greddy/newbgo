#ifndef _PixelMonitor_utilslayer_MonitorItem_h_
#define _PixelMonitor_utilslayer_MonitorItem_h_

#include <string>

namespace PixelMonitor {
namespace utilslayer {

class InfoSpaceHandler;

class MonitorItem {

  public:
    MonitorItem(std::string const &name,
                std::string const &description,
                InfoSpaceHandler *const infoSpaceHandler,
                std::string const &docString = "");

    std::string getName() const;
    std::string getDescription() const;
    InfoSpaceHandler *getInfoSpaceHandler() const;
    std::string getDocString() const;

  private:
    std::string name_;
    std::string description_;
    InfoSpaceHandler *infoSpaceHandlerP_;
    std::string docString_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_MonitorItem_h_
