#ifndef _PixelMonitor_utilslayer_WebTableAppHist_h
#define _PixelMonitor_utilslayer_WebTableAppHist_h

#include <cstddef>
#include <string>

#include "PixelMonitor/utilslayer/WebObject.h"

namespace PixelMonitor {
namespace utilslayer {
class Monitor;
}
}

namespace PixelMonitor {
namespace utilslayer {

class WebTableAppHist : public PixelMonitor::utilslayer::WebObject {

  public:
    WebTableAppHist(std::string const &name,
                    std::string const &description,
                    PixelMonitor::utilslayer::Monitor const &monitor,
                    std::string const &itemSetName,
                    std::string const &tabName,
                    size_t const colSpan);

    std::string getHTMLString() const;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_WebTableAppHist_h
