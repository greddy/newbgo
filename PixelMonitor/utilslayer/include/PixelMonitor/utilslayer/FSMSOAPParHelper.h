#ifndef _PixelMonitor_utilslayer_FSMSOAPParHelper_h_
#define _PixelMonitor_utilslayer_FSMSOAPParHelper_h_

#include <string>

namespace PixelMonitor {
namespace utilslayer {

class FSMSOAPParHelper {

  public:
    FSMSOAPParHelper(std::string const &commandName,
                     std::string const &parameterName,
                     bool const isRequired = true /* , */
                     /* std::string const& defaultValue="" */);
    ~FSMSOAPParHelper();

    std::string commandName() const;
    std::string parameterName() const;
    bool isRequired() const;
    /* std::string defaultValue() const; */

  private:
    std::string commandName_;
    std::string parameterName_;
    bool isRequired_;
    /* std::string defaultValue_; */
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_FSMSOAPParHelper_h_
