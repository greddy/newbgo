#ifndef _PixelMonitor_utilslayer_XDAQAppBase_h_
#define _PixelMonitor_utilslayer_XDAQAppBase_h_

#include <memory>
#include <string>

#include "toolbox/task/TimerListener.h"
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xoap/MessageReference.h"

// #include "PixelMonitor/hwlayer/DeviceBase.h"
#include "PixelMonitor/hwlayer/HwDeviceTCA.h"
#include "PixelMonitor/hwlayer/RegisterInfo.h"
#include "PixelMonitor/utilslayer/ApplicationStateInfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/FSMSOAPParHelper.h"
#include "PixelMonitor/utilslayer/InfoSpaceUpdaterPlain.h"
#include "PixelMonitor/utilslayer/Lock.h"
#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/WebServer.h"

namespace log4cplus {
class Logger;
}

namespace toolbox {
namespace task {
class Timer;
class TimerEvent;
}
}

namespace xcept {
class Exception;
}

namespace xdaq {
class ApplicationDescriptor;
class ApplicationStub;
}

namespace xdata {
class Event;
}

namespace PixelMonitor {
namespace utilslayer {

class ConfigurationInfoSpaceHandler;

class XDAQAppBase : public toolbox::task::TimerListener,
                    public xdaq::Application,
                    public xdata::ActionListener
                    /**
     * Abstract base class for all TCDS top-level XDAQ applications
     * (or 'Controllers').
     */
                    {
    // The Monitor class is a friend so it has access to the
    // monitoringLock_.
    friend class Monitor;
    friend class WebServer;

  public:
    virtual ~XDAQAppBase();

    virtual void actionPerformed(xdata::Event &event);

    std::string getFullURL();

    bool isHwLeased() const;
    std::string getHwLeaseOwnerId() const;
    void assignHwLease(std::string const &leaseOwnerId);
    void renewHwLease();
    void revokeHwLease();

    /**
       * Connect to hardware/release hardware.
       */
    void hwConnect();
    void hwRelease();

    /**
       * Setting parameters straight from SOAP messages is not
       * allowed. This method is only implemented to send a SOAP fault
       * back in case someone tries it anyway.
       */
    xoap::MessageReference parameterSet(xoap::MessageReference msg);

    std::string readHardwareConfiguration();
    std::string readHardwareState();

    // Utility method to determine the application icon file path
    // for an application specified by the ApplicationDescriptor (or
    // the current application).
    std::string buildIconPathName(xdaq::ApplicationDescriptor const *const app = 0);

    // Access to inside information.
    // wsi: add write access to applicationStateInfoSpaceHandler to write app history msg
    ConfigurationInfoSpaceHandler const &getConfigurationInfoSpaceHandler() const;
    ApplicationStateInfoSpaceHandler &getApplicationStateInfoSpaceHandler();

  protected:
    XDAQAppBase(xdaq::ApplicationStub *const stub,
                std::unique_ptr<PixelMonitor::hwlayer::HwDeviceTCA> hw);

    // Some alarm handling helpers.
    void raiseAlarm(std::string const &name,
                    std::string const &severity,
                    xcept::Exception &err);
    void revokeAlarm(std::string const &name);

    // Several methods for SOAP messaging back and forth.
    xoap::MessageReference executeSOAPCommand(xoap::MessageReference &cmd,
                                              xdaq::ApplicationDescriptor &dest);
    xoap::MessageReference sendSOAP(xoap::MessageReference &msg,
                                    xdaq::ApplicationDescriptor &dest);
    xoap::MessageReference postSOAP(xoap::MessageReference &msg,
                                    xdaq::ApplicationDescriptor &destination);

    virtual void setupInfoSpaces();

    virtual void hwConnectImpl() = 0;
    virtual void hwReleaseImpl() = 0;

    /**
       * Access the hardware pointer as HwDeviceTCA&.
       */
    virtual PixelMonitor::hwlayer::HwDeviceTCA &getHw() const;

    InfoSpaceUpdaterPlain appStateInfoSpaceUpdater_;
    ApplicationStateInfoSpaceHandler appStateInfoSpace_;
    /**
       * @note
       *
       * The following two variables should be instantiated by each
       * and every inheriting class in the constructor.
       * So even though they are pointers, we should be able to simply
       * use them everywhere outside the constructor.
       */
    std::unique_ptr<ConfigurationInfoSpaceHandler> cfgInfoSpaceP_;
    std::unique_ptr<PixelMonitor::hwlayer::HwDeviceTCA> hwP_;

    log4cplus::Logger &logger_;
    Monitor monitor_;
    WebServer webServer_;

    // A string identifying the RunControl session owning the
    // hardware associated with this control application.
    std::string hwLeaseOwnerId_;

    // A lock to allow scheduling between the monitoring updates
    // (which potentially access the hardware) and other actions on
    // the hardware.
    // NOTE: This is extremely coarse locking.
    Lock monitoringLock_;

    // TODO TODO TODO
    // Maybe all the hardware-level state transition methods should
    // be moved here too.
    virtual PixelMonitor::hwlayer::HwDeviceTCA::RegContentsVec hwReadHardwareConfiguration() const;
    // TODO TODO TODO end

    virtual bool isRegisterAllowed(PixelMonitor::hwlayer::RegisterInfo const &regInfo) const;

    // Several methods involved in loading parameters from an FSM
    // SOAP command message into the ConfigurationInfoSpace.
    std::vector<FSMSOAPParHelper> expectedFSMSoapPars(std::string const &commandName) const;
    virtual std::vector<FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const &commandName) const;
    void loadSOAPCommandParameters(xoap::MessageReference const &msg);
    virtual void loadSOAPCommandParametersImpl(xoap::MessageReference const &msg);
    void loadSOAPCommandParameter(xoap::MessageReference const &msg,
                                  FSMSOAPParHelper const &param);
    virtual void loadSOAPCommandParameterImpl(xoap::MessageReference const &msg,
                                              FSMSOAPParHelper const &param);

  private:
    // This method renews the hardware lease from a SOAP command.
    xoap::MessageReference RenewHwLease(xoap::MessageReference msg);

    virtual std::string readHardwareConfigurationImpl();
    virtual std::string readHardwareStateImpl();

    // This method is used to auto-expire the hardware lease after a
    // given time.
    void timeExpired(toolbox::task::TimerEvent &event);

    /**
       * A filtering method to select which part of the hardware
       * configuration is exposed to the outside world via
       * hwReadHardwareConfiguration().
       */
    PixelMonitor::hwlayer::RegisterInfo::RegInfoVec filterRegInfo(PixelMonitor::hwlayer::RegisterInfo::RegInfoVec const &regInfosIn) const;

    // The auto-expiry timer for the hardware lease.
    std::string timerName_;
    std::string const timerJobName_;
    toolbox::task::Timer *hwLeaseExpiryTimerP_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_XDAQAppBase_h_
