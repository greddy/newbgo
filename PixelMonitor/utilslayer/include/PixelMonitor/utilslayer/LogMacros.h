#ifndef _PixelMonitor_utilslayer_LogMacros_h_
#define _PixelMonitor_utilslayer_LogMacros_h_

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

/** @file
 * Just a few macros to make life (and logging) easier.
 */

#define DEBUG(MSG) LOG4CPLUS_DEBUG(logger_, MSG)
#define INFO(MSG) LOG4CPLUS_INFO(logger_, MSG)
#define WARN(MSG) LOG4CPLUS_WARN(logger_, MSG)
#define ERROR(MSG) LOG4CPLUS_ERROR(logger_, MSG)
#define FATAL(MSG) LOG4CPLUS_FATAL(logger_, MSG)

#endif
