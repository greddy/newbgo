#ifndef _PixelMonitor_utilslayer_version_h_
#define _PixelMonitor_utilslyaer_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define PIXELMONITORUTILSLAYER_VERSION_MAJOR 2
#define PIXELMONITORUTILSLAYER_VERSION_MINOR 5
#define PIXELMONITORUTILSLAYER_VERSION_PATCH 3

// If any previous versions available:
#define PIXELMONITORUTILSLAYER_PREVIOUS_VERSIONS "2.5.2"
// else:
// #undef PIXELMONITORUTILSLAYER_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define PIXELMONITORUTILSLAYER_VERSION_CODE PACKAGE_VERSION_CODE(PIXELMONITORUTILSLAYER_VERSION_MAJOR, PIXELMONITORUTILSLAYER_VERSION_MINOR, PIXELMONITORUTILSLAYER_VERSION_PATCH)
#ifndef PIXELMONITORUTILSLAYER_PREVIOUS_VERSIONS
#define PIXELMONITORUTILSLAYER_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PIXELMONITORUTILSLAYER_VERSION_MAJOR, PIXELMONITORUTILSLAYER_VERSION_MINOR, PIXELMONITORUTILSLAYER_VERSION_PATCH)
#else
#define PIXELMONITORUTILSLAYER_FULL_VERSION_LIST PIXELMONITORUTILSLAYER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PIXELMONITORUTILSLAYER_VERSION_MAJOR, PIXELMONITORUTILSLAYER_VERSION_MINOR, PIXELMONITORUTILSLAYER_VERSION_PATCH)
#endif

namespace pixelmonitorutilslayer {

const std::string package = "pixelmonitorutilslayer";
const std::string versions = PIXELMONITORUTILSLAYER_FULL_VERSION_LIST;
const std::string description = "CMS PIXELMONITOR helper software.";
const std::string authors = "Jeroen Hegeman, Weinan Si";
const std::string summary = "CMS TCDS helper software, modified for PixelMonitor";
const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
config::PackageInfo getPackageInfo();
void checkPackageDependencies();
std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace pixelmonitorutilslayer

#endif
