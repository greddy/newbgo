#ifndef _PixelMonitor_utils_Uncopyable_h
#define _PixelMonitor_utils_Uncopyable_h

namespace PixelMonitor {
namespace utilslayer {

class Uncopyable
    /**
     * Just our own simple mix-in class to prevent copying.
     */
    {

  protected:
    Uncopyable() {};
    ~Uncopyable() {};

  private:
    Uncopyable(Uncopyable const &);
    Uncopyable &operator=(Uncopyable const &);
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utils_Uncopyable_h
