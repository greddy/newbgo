#include "PixelMonitor/utilslayer/SOAPUtils.h"

#include <string>

template <class T>
PixelMonitor::utilslayer::SOAPCmdDumpHardwareState<T>::SOAPCmdDumpHardwareState(T& controller) :
  SOAPCmdBase<T>(controller, "DumpHardwareState", false)
{
}

template <class T>
xoap::MessageReference
PixelMonitor::utilslayer::SOAPCmdDumpHardwareState<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Do what we were asked to do.
  std::string res = this->controller_.readHardwareState();

  // Send reply.
  xoap::MessageReference reply = PixelMonitor::utilslayer::soap::makeCommandSOAPReply(msg);
  PixelMonitor::utilslayer::soap::addSOAPReplyParameter(reply, "registerDump", res);
  return reply;
}
