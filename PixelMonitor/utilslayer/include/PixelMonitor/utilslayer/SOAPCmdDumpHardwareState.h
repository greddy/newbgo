#ifndef _PixelMonitor_utilslayer_SOAPCmdDumpHardwareState_h_
#define _PixelMonitor_utilslayer_SOAPCmdDumpHardwareState_h_

#include "xoap/MessageReference.h"

#include "PixelMonitor/utilslayer/SOAPCmdBase.h"

namespace PixelMonitor {
namespace utilslayer {

class XDAQAppBase;

template <class T>
class SOAPCmdDumpHardwareState : public SOAPCmdBase<T> {

  public:
    SOAPCmdDumpHardwareState(T &controller);

    /**
         * Reads back the hardware configuration. Bound to the SOAP
         * command DumpHardwareState.
         * - SOAP command parameters:
         *   None.
         * - SOAP command return value:
         *   A big string containing a full register dump.
         */
    virtual xoap::MessageReference executeImpl(xoap::MessageReference const &msg);
};

} // namespace utilslayer
} // namespace PixelMonitor

#include "PixelMonitor/utilslayer/SOAPCmdDumpHardwareState.hxx"

#endif // _PixelMonitor_utilslayer_SOAPCmdDumpHardwareState_h_
