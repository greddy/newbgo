#ifndef _PixelMonitor_utilslayer_CurlGetter_h_
#define _PixelMonitor_utilslayer_CurlGetter_h_

#include <stddef.h>
#include <string>
#include <curl/curl.h>

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLURL.hpp"

namespace PixelMonitor {
namespace utilslayer {

class CurlGetter {

  public:
    CurlGetter();
    ~CurlGetter();

    std::string buildFullURL(std::string const &scheme,
                             std::string const &address,
                             std::string const &path,
                             std::string const &parameters = "",
                             std::string const &query = "",
                             std::string const &fragment = "") const;
    std::string get(std::string const &scheme,
                    std::string const &address,
                    std::string const &path,
                    std::string const &parameters = "",
                    std::string const &query = "",
                    std::string const &fragment = "");

  private:
    static size_t staticWriteCallback(char *buffer, size_t size, size_t nItems, void *getter);

    void performGetRequest(XERCES_CPP_NAMESPACE::XMLURL const &xmlUrl);
    size_t receive(char *buffer, size_t size, size_t nItems);

    CURL *curl_;
    std::string receiveBuffer_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_CurlGetter_h_
