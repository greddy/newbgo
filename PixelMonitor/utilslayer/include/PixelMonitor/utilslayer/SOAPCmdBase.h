#ifndef _PixelMonitor_utilslayer_SOAPCmdBase_h_
#define _PixelMonitor_utilslayer_SOAPCmdBase_h_

#include <string>

#include "toolbox/lang/Class.h"
#include "xoap/MessageReference.h"

namespace PixelMonitor {
namespace utilslayer {

template <class T>
class SOAPCmdBase : public toolbox::lang::Class {

  public:
    virtual ~SOAPCmdBase();

    std::string commandName() const;
    xoap::MessageReference execute(xoap::MessageReference msg);

    virtual xoap::MessageReference executeImpl(xoap::MessageReference const &msg) = 0;

  protected:
    SOAPCmdBase(T &controller,
                std::string const &commandName,
                bool const requiresHwLease = true);

    T &controller_;
    std::string const commandName_;
    bool const requiresHwLease_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#include "PixelMonitor/utilslayer/SOAPCmdBase.hxx"

#endif // _PixelMonitor_utilslayer_SOAPCmdBase_h_
