#ifndef _PixelMonitor_utilslayer_Monitor_h_
#define _PixelMonitor_utilslayer_Monitor_h_

#include <string>
#include <tr1/unordered_map>
#include <utility>
#include <vector>

#include "toolbox/exception/Listener.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/TimeVal.h"

#include "PixelMonitor/utilslayer/MonitorItem.h"
#include "PixelMonitor/utilslayer/TCDSXDAQObject.h"
#include "PixelMonitor/utilslayer/Uncopyable.h"

namespace log4cplus {
class Logger;
}

namespace sentinel {
namespace utils {
class Alarm;
}
}

namespace toolbox {
namespace task {
class Timer;
class TimerEvent;
}
}

namespace xcept {
class Exception;
}

namespace xdata {
class Event;
class InfoSpace;
}

namespace PixelMonitor {
namespace utilslayer {

class InfoSpaceHandler;
class XDAQAppBase;

class Monitor : public toolbox::task::TimerListener,
                public toolbox::exception::Listener,
                public TCDSXDAQObject,
                private Uncopyable
                /**
     * Driving class behind all monitoring going on in the TCDS XDAQ
     * framework. Access from 'outside' is via the WebServer
     * class. This class is uncopyable since it holds all kinds of
     * pointers to InfoSpaces.
     */
                {

  public:
    Monitor(XDAQAppBase *const xdaqApp, std::string const &name = "");
    ~Monitor();

    // OBSOLETE OBSOLETE OBSOLETE
    /* void actionPerformed(xdata::Event& event); */
    // OBSOLETE OBSOLETE OBSOLETE

    void startMonitoring();
    /* void stopMonitoring(); */

    typedef std::vector<std::pair<std::string, std::string> > StringPairVector;
    StringPairVector getFormattedItemSet(std::string const &itemSetName) const;
    StringPairVector getItemSetDocStrings(std::string const &itemSetName) const;

    /**
       * Add the item with itemName to the itemset with itemSetName.
       */
    void addItem(std::string const &itemSetName,
                 std::string const &itemName,
                 std::string const &itemDesc,
                 InfoSpaceHandler *const infoSpaceHandler,
                 std::string const &docString = "");

    /**
         Create a new itemset with the name given.
       */
    void newItemSet(std::string const &name);

    // The toolbox::exception::Listener callback.
    virtual void onException(xcept::Exception &err);

  protected:
    log4cplus::Logger &logger_;

    void addInfoSpace(InfoSpaceHandler *const infoSpace);

    void timeExpired(toolbox::task::TimerEvent &event);

  private:
    // This is the name of any monitoring alarms raised.
    std::string const alarmName_;

    std::tr1::unordered_map<std::string, InfoSpaceHandler *> infoSpaceMap_;
    typedef std::tr1::unordered_map<std::string,
                                    std::vector<std::pair<std::string, MonitorItem> > > MonitorItemMap;
    MonitorItemMap itemSets_;
    toolbox::task::Timer *timerP_;

    // The start time of the monitor. Just for a simplistic estimate
    // of the application uptime.
    toolbox::TimeVal timeStart_;

    /**
       * The main update method: crawls over all registered InfoSpaces
       * and updates all of them.
       */
    void updateAllInfoSpaces();

    void handleException(xcept::Exception &err);

    xdata::InfoSpace *getMonitoringInfoSpace() const;
    sentinel::utils::Alarm *getMonitoringAlarm() const;
    void raiseMonitoringProblem(std::string const &problemDesc) const;
    void revokeMonitoringProblem() const;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_Monitor_h_
