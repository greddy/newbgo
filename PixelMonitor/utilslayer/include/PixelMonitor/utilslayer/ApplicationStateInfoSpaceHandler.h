#ifndef ApplicationStateInfoSpaceHandler_h_
#define ApplicationStateInfoSpaceHandler_h_

#include <map>
#include <stddef.h>
#include <string>

#include "PixelMonitor/utilslayer/AppHist.h"
#include "PixelMonitor/utilslayer/InfoSpaceHandler.h"

namespace xdaq {
class Application;
}

namespace PixelMonitor {
namespace utilslayer {

class InfoSpaceUpdater;
class Monitor;
class WebServer;

class ApplicationStateInfoSpaceHandler : public InfoSpaceHandler
                                         /**
     * This class holds the basic information about the top-level
     * state of the whole application. For example:
     * - The current state of the software state machine (if present).
     * - The state of the monitoring (i.e., running or
         stopped-because-of-a-problem).
     * NOTE: The variables in this InfoSpace are all a little
     * different from 'normal' InfoSpace items in that they are not
     * updated by an updater, but directly (i.e., from some other
     * place in the code).
     */
                                         {

  public:
    ApplicationStateInfoSpaceHandler(xdaq::Application *const xdaqApp,
                                     PixelMonitor::utilslayer::InfoSpaceUpdater *updater);
    virtual ~ApplicationStateInfoSpaceHandler();

    virtual std::string formatItem(PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec::const_iterator const &item) const;

    void addProblem(std::string const &keyword,
                    std::string const &problemDescription);
    void removeProblem(std::string const &keyword);

    void addHistoryItem(std::string const &desc);

    void setFSMState(std::string const &stateDescription,
                     std::string const &problemDescription = kNoProblemString);
    void addMonitoringProblem(std::string const &problemDescription);
    void removeMonitoringProblem();

  private:
    void setApplicationState(std::string const &stateDescription);
    void setProblemDescription(std::string const &problemDescription);
    void updateStatusDescription();

    virtual void registerItemSetsWithMonitor(Monitor &monitor);
    virtual void registerItemSetsWithWebServer(WebServer &webServer,
                                               Monitor &monitor);

    static std::string const kNoProblemString;

    std::map<std::string, std::string> problems_;
    PixelMonitor::utilslayer::AppHist history_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // ApplicationStateInfoSpaceHandler_h_
