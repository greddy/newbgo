#ifndef _PixelMonitor_utilslayer_XDAQObject_h_
#define _PixelMonitor_utilslayer_XDAQObject_h_

namespace xdaq {
class Application;
}

namespace PixelMonitor {
namespace utilslayer {

class XDAQObject {

  public:
    virtual ~XDAQObject();
    xdaq::Application &getOwnerApplication() const;

  protected:
    XDAQObject(xdaq::Application *const owner);

  private:
    xdaq::Application *const ownerP_;
};

} // namespace utilslayer
} // namespace PixelMonitor

#endif // _PixelMonitor_utilslayer_XDAQObject_h_
