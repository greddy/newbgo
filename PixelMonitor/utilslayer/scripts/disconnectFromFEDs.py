#!/usr/bin/env python

import datetime
import sys
import urllib2
import httplib
import argparse
import urlparse
from xml.dom import minidom

# Fixed constants.
URI_NS_XSD = "http://www.w3.org/2001/XMLSchema"
URI_NS_XSI = "http://www.w3.org/2001/XMLSchema-instance"
SOAP_PREFIX_XDAQ = "xdaq"
URI_NS_XDAQ = "urn:xdaq-soap:3.0"

###############################################################################

def build_xdaq_soap_command_message(requestor,
                                    command_name,
                                    parameters=None):
    if parameters is None:
        parameters = {}

    uri_ns_soap_envelope = "http://www.w3.org/2003/05/soap-envelope"
    soap_envelope_prefix = "env"

    # Create the main DOM document
    doc = minidom.Document()

    # Create and attach the SOAP envelope
    envelope = doc.createElement("{0:s}:Envelope".format(soap_envelope_prefix))
    envelope.setAttribute("xmlns:{0:s}".format(soap_envelope_prefix),
                          uri_ns_soap_envelope)
    envelope.setAttribute("xmlns:xsd", URI_NS_XSD)
    envelope.setAttribute("xmlns:xsi", URI_NS_XSI)
    doc.appendChild(envelope)

    # Create the header and attach it to the envelope
    header = doc.createElement("{0:s}:Header".format(soap_envelope_prefix))
    envelope.appendChild(header)

    # Create the SOAP body and attach it to the envelope
    body = doc.createElement("{0:s}:Body".format(soap_envelope_prefix))
    envelope.appendChild(body)

    # Create and attach body element
    cmd_element = doc.createElement("{0:s}:{1:s}".format(SOAP_PREFIX_XDAQ,
                                                         command_name))
    cmd_element.setAttribute("xmlns:{0:s}".format(SOAP_PREFIX_XDAQ),
                             URI_NS_XDAQ)
    body.appendChild(cmd_element)

    # Attach actionRequesterId
    cmd_element.setAttribute("{0:s}:actionRequestorId".format(SOAP_PREFIX_XDAQ),
                             requestor)

    # If any parameters were given, attach those to the command node.
    if len(parameters):
        parameter_names = parameters.keys()
        parameter_names.sort()
        for par_name in parameter_names:
            (par_type, par_val) = parameters[par_name]
            param_element = doc.createElement("{0:s}:{1:s}".format(SOAP_PREFIX_XDAQ,
                                                                   par_name))
            param_element.setAttribute("xsi:type", "xsd:{0:s}".format(par_type))
            value_element = doc.createTextNode(str(par_val))
            param_element.appendChild(value_element)
            cmd_element.appendChild(param_element)

    # End of build_xdaq_soap_commmand_message()
    return doc


###############################################################################

def send_xdaq_soap_message(host, port, lid, msg, verbose=False):
    
    if verbose:
        print "Sending SOAP message:"
        print msg.toprettyxml()

    soap_message_text = msg.toxml()
    headers = {
        "Content-Location" : "urn:xdaq-application:lid={0:d}".format(lid),
        "Content-type" : "application/soap+xml",
        "Content-length" : len(soap_message_text)
    }
    url = "http://{0:s}:{1:d}".format(host, port)
    response_tmp = send_message(url, soap_message_text, headers, verbose)

    response = minidom.parseString(response_tmp)
    if verbose:
        print "Received SOAP response:"
        print response.toprettyxml()
        print '-'*79
        print

    # End of send_xdaq_soap_message()
    return response

###############################################################################

def send_message(url, content=None, headers={}, verbose=False):

    req = urllib2.Request(url, content, headers)
    response = None

    try:
        reply = urllib2.urlopen(req)
        response = reply.read()
    except httplib.BadStatusLine, err:
        msg = "It looks like the applicatioin crashed before responding."
        print "URL to be sent: ", url
        print >> sys.stderr, msg
        sys.exit(1)
    except urllib2.HTTPError, err:
        msg = "Problem sending message, " \
              "the server could not fullfill the request. " \
              "Error code: {0:d}"
        print >> sys.stderr, msg.format(err.code)
        sys.exit(1)
    except urllib2.URLError, err:
        msg = "Problem sending message, " \
              "failed to reach the server: '{0:s}'."
        print >> sys.stderr, msg.format(str(err.reason))
        sys.exit(1)

    # End of send_message()
    return response

###############################################################################

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("urls",
                        help="The file contains URLs of FEDMonitors to connect",
                        type=str)
    args = parser.parse_args()
    urlLine = open(args.urls, 'r').readline().strip('\n')
    targetList = urlLine.split(', ')

    timestamp_begin = datetime.datetime.utcnow()
    print "Begin: ", timestamp_begin

    command_name = "Halt"
    parameters = {}

    for target in targetList:
        # target e.g. http://srv-c2d11-08-01.cms:1977/urn:xdaq-application:lid=1450
        target = target.strip()
        host_name = target.split('/')[2].split(':')[0] # srv-c2d11-08-01.cms
        port_number = int(target.split('/')[2].split(':')[1]) # 1977
        lid_number = int(target.split('=')[1]) # 1450

        soap_msg = build_xdaq_soap_command_message("FEDRack",
                                                   command_name,
                                                   parameters)
        soap_reply = send_xdaq_soap_message(host_name,
                                            port_number,
                                            lid_number,
                                            soap_msg,
                                            verbose=True)
    timestamp_end = datetime.datetime.utcnow()
    print "End: ", timestamp_end
    print "Take us: ", timestamp_end - timestamp_begin

    # All done
    sys.exit(0)
