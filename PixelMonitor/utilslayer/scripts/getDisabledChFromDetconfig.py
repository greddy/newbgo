#!/usr/bin/env python
import os
import sys
import collections
from subprocess import PIPE, Popen

'''
    input FED#
    printout enable status of channels, '1' is Disabled
    CH48                  <--                   Ch01
    000000000000000000000000111111111111111111111111
'''
##############################################################################

def errMsg(msg):
    ''' color console output message '''
    # White foreground, red background
    pre = "\033[41;37mERROR\033[0m"
    # Red foreground
    cMsg = "\033[31m" + msg + "\033[0m"
    return pre + ": " + cMsg

##############################################################################

def showDict(d):
    for k in d.keys():
        print k, ': ', d[k]

##############################################################################

def getDefaultFromAlias(aliases_txt):
    d = {}
    try:
        with open(aliases_txt, 'r') as f:
            for line in f:
                line = line.strip()
                if len(line.split()) != 4: continue
                line = line.split()
                if line[2] == "Default":
                    if line[0] in d.keys():
                        raise RuntimeError("Same default was set twice!")
                    d[line[0]] = line[1]
    except RuntimeError, e:
        print errMsg("In {0}:".format(aliases_txt))
        sys.exit(errMsg(str(e)))
    except:
        raise
        sys.exit(errMsg("Something wrong with {0}!".format(aliases_txt)))
    return d

##############################################################################

def getFed2ChRoc(fed, translation_dat):
    '''input: fedid
       return a dictionary whose
           key: ch
           value: [ROCs]'''
    d = collections.defaultdict(list)
    try:
        with open(translation_dat, 'r') as f:
            for line in f:
                if line.startswith('#'): continue
                line = line.strip().split()
                fed_ = int(line[8])
                if fed_ != fed: continue
                ch  = int(line[9])
                if ch < 1 or ch > 48:
                    raise RuntimeError("channel number must between 1 and 48")
                d[ch].append(line[0])
        if len(d.keys()) == 0:
            raise RuntimeError("FED# not existed in nametranslation file!")
    except:
        raise
        sys.exit(errMsg("Something wrong with {0}!".format(translation_dat)))
    return d

##############################################################################

def getEnabledCh4Fed(fed, translation_dat, detectconfig_dat):
    '''input: fed#
       return: [enabled channel]'''
    l = [0]*48
    chRocDict = getFed2ChRoc(fed, translation_dat)
    try:
        d = collections.defaultdict(bool)
        with open(detectconfig_dat, 'r') as f:
            for line in f:
                if line.startswith("Rocs"): continue
                line = line.strip().split()
                d[line[0]] = True if (len(line) == 1) else False
        for k in sorted(chRocDict.keys()):
            enable = 1
            for roc in chRocDict[k]:
                if not d[roc]:
                    enable = 0
                    break
            l[k-1] = enable
    except:
        raise
        sys.exit(errMsg("Something wrong with {0}!".format(detectconfig_dat)))
    return l

##############################################################################

if __name__ == "__main__":
    try:
        envar = 'PIXELCONFIGURATIONBASE'
        pixelConfigBase = os.environ[envar]
    except KeyError:
        sys.exit(errMsg("Environment variable {0} is not set!!".format(envar)))
    
    defaultNum = getDefaultFromAlias(os.path.join(pixelConfigBase,"aliases.txt"))
    detconfigFn = os.path.join(pixelConfigBase, 'detconfig', defaultNum['detconfig'], "detectconfig.dat")
    translationFn = os.path.join(pixelConfigBase, 'nametranslation', defaultNum['nametranslation'], "translation.dat")

    enabledCh4Fed = getEnabledCh4Fed(int(sys.argv[1]), translationFn, detconfigFn)
    res = [str(1-x) for x in reversed(enabledCh4Fed)]
    print ''.join(res)

    sys.exit(0)
