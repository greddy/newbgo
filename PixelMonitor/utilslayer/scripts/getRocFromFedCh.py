#!/usr/bin/env python
import os
import sys
'''
    input: fed#, ch#
    printout ROCname
'''

##############################################################################

def errMsg(msg):
    ''' color console output message '''
    # White foreground, red background
    pre = "\033[41;37mERROR\033[0m"
    # Red foreground
    cMsg = "\033[31m" + msg + "\033[0m"
    return pre + ": " + cMsg

##############################################################################

def getDefaultFromAlias(aliases_txt):
    d = {}
    try:
        with open(aliases_txt, 'r') as f:
            for line in f:
                line = line.strip()
                if len(line.split()) != 4: continue
                line = line.split()
                if line[2] == "Default":
                    if line[0] in d.keys():
                        raise RuntimeError("Same default was set twice!")
                    d[line[0]] = line[1]
    except RuntimeError, e:
        print errMsg("In {0}:".format(aliases_txt))
        sys.exit(errMsg(str(e)))
    except:
        raise
        sys.exit(errMsg("Something wrong with {0}!".format(aliases_txt)))
    return d

##############################################################################

def getRocFromFedCh(fed, ch, translation_dat):
    '''input: fed#, ch#, nametranslation
       return ROC name'''
    if ch < 1 or ch > 48:
        raise RuntimeError("channel number must between 1 and 48")
    l = []
    res = ""
    try:
        with open(translation_dat, 'r') as f:
            for line in f:
                if line.startswith('#'): continue
                line = line.strip().split()
                fed_ = int(line[8])
                ch_  = int(line[9])
                if fed_ != fed or ch_ != ch: continue
                l.append(line[0])
        if len(l) == 0:
           res = "Not connected with module" 
        else:
           mod = set([x.rsplit('_',1)[0] for x in l])
           if len(mod) != 1:
               raise RuntimeError("FED{0} ch{1} corresponds to ROCs of more than 1 module!".format(fed,ch))
           chs = [int(x.rsplit('_',1)[1].lstrip('ROC')) for x in l]
           chs.sort()
           if chs[0] + len(chs) -1 != chs[-1]:
               raise RuntimeError("ROC number not successive! smething wong with {0}".format(translation_dat))
           else:
               res = "{0} ROC{1}-{2}".format(list(mod)[0],chs[0],chs[-1])
    except RuntimeError, e:
        sys.exit(errMsg(str(e)))
    except:
        raise
        sys.exit(errMsg("Something wrong with {0}!".format(translation_dat)))
    return res

##############################################################################

if __name__ == "__main__":
    try:
        envar = 'PIXELCONFIGURATIONBASE'
        pixelConfigBase = os.environ[envar]
    except KeyError:
        sys.exit(errMsg("Environment variable {0} is not set!!".format(envar)))
    
    defaultNum = getDefaultFromAlias(os.path.join(pixelConfigBase,"aliases.txt"))
    translationFn = os.path.join(pixelConfigBase, 'nametranslation', defaultNum['nametranslation'], "translation.dat")

    print getRocFromFedCh(int(sys.argv[1]), int(sys.argv[2]), translationFn)
    sys.exit(0) 
    try:
        sys.stdout.close()
    except:
        pass
