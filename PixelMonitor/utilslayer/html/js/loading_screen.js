// NOTE: The following is executed immediately at load time. This is
// intentional, since the loading screen should be the first thing in
// the <body>, and rendered immediately.

// First check if this is a TCDS application or not. That way we do
// not affect 'core' XDAQ applications.
var appName = jQuery("body").attr("data-app-name");
if ((typeof appName !== typeof undefined) &&
    (appName.startsWith("PixelMonitor::")))
{
    var iconName = "/PixelMonitor/utilslayer/images/PixelMonitor_generic_icon.png";
    if (typeof appName !== typeof undefined)
    {
        var tmp0 = appName.split("::")[1];
        var tmp1 = appName.split("::")[2];
        iconName = "/PixelMonitor/" + tmp0 + "/images/" + tmp1.toLowerCase() + "_icon.png";
    }

    var service = "PIXEL MONITOR SERVICE";//"some TCDS service";
    var serviceName = jQuery("body").attr("data-app-service");
    if (typeof serviceName !== typeof undefined)
    {
        service = serviceName;
    }

    // Yes, a little bit of hacking this is, yes.
    // The 'XXXController' applications all have icons with holes and
    // a transparent background. These come out better on a
    // light/white background. The other applications have a
    // transparent background as well, but don't have holes. So these
    // come out better on a dark(er) background.
    var styleStr = "";
    if (appName.toLowerCase().indexOf("controller") > -1)
    {
        styleStr = " style=\"background-color: white; border: 1pt solid white;\"";
    }
    var templateStr = "<div class='pg-loading-inner'>\n  <div class='pg-loading-center-outer'>\n    <div class='pg-loading-center-middle'>\n      <h1 class='pg-loading-logo-header'>\n        <img class='pg-loading-logo'" + styleStr + "></img>\n      </h1>\n      <div class='pg-loading-html'>\n      </div>\n    </div>\n  </div>\n</div>";

    window.loading_screen = pleaseWait({
        template: templateStr,
        logo: iconName,
        backgroundColor: "#ffffff",//"#f46d3b",
        loadingHtml: "<div><p>Loading " + service + "...</p></div>"
    });
}
