//-----------------------------------------------------------------------------
//
// NOTE: This has been written to work with the XDAQ12-style HyperDAQ
// and doT.js.
//
//-----------------------------------------------------------------------------

// NOTE: All global variables are stuffed into the global 'tcds'
// container.
var tcds = {};
tcds.baseUrl = undefined;
tcds.updateUrl = undefined;
tcds.updateInterval = 1000;
tcds.updateFailCount = 0;
tcds.maxUpdateFailCount = 10;
tcds.data = undefined;
tcds.grids = {};
tcds.l1ahistos = undefined;
tcds.initialised = false;
tcds.templateCache = {};

var pixelDaqTimer = undefined;
monitorHostId = {};

//-----------------------------------------------------------------------------

// HyperDAQ javascript 'callback' method.
function xdaqWindowPreLoad()
{
    if (isTCDSApplication())
    {
        // Perform some setup operations.
        doSetup();

        // Figure out the update URL.
        var pathName = window.location.pathname;
        if (pathName[0] == "/")
        {
            pathName = pathName.substr(1);
        }
        if (pathName[-1] == "/")
        {
            pathName = pathName.substr(0, -1);
        }
        var pathArray = pathName.split("/");
        var newPathName = pathArray[0];
        var tmpUrl = window.location.protocol + "//" + window.location.host + "/" + newPathName;
        var updateUrl = tmpUrl + "/update";
        tcds.baseUrl = tmpUrl;
        tcds.updateUrl = updateUrl;
    }
};

//-----------------------------------------------------------------------------

// HyperDAQ javascript 'callback' method.
function xdaqWindowPostLoad()
{
    if (isTCDSApplication())
    {
        // NOTE: At some point we may want to clean up this
        // button-business a bit.

        // Add the 'Modify TTCSpy configuration' button if we're
        // dealing with a PIController.
        var tmp = jQuery(".tcds-tab-name:contains('TTCSpy')");
        if (tmp.length)
        {
            var hook = tmp.parent();
            fixupTTCSpyConfigButton(hook);
        }

        // Add the 'Modify random-trigger rate' button if we're
        // dealing with a CPMController.
        tmp = jQuery("p:contains('Random-trigger configuration')");
        if (tmp.length)
        {
            var hook = tmp.last();
            fixupRandomRateConfigButton(hook);
        }

        // Add configure button for fedmonitor
        tmp = jQuery(".tcds-tab-name:contains('FED STATUS')");
        if (tmp.length)
        {
            var hook = tmp.parent();
            fixupMonitorConfigButton(hook);
            fixupMonitorHaltButton(hook);
            //fixupPlayButton(hook);
        }
        
        // Add configure button for rack
        tmp = jQuery(".tcds-tab-name:contains('FED-MONITOR-LIST')");
        if (tmp.length)
        {
            var hook = tmp.parent();
            fixupRackConfigButton(hook);
            fixupRackHaltButton(hook);
            fixupPlayButton(hook);
            hook.append("<div id=\"crateButtons\"></div>");
        }

        tmp = jQuery(".tcds-tab-name:contains('CHANNEL-MASK-STATUS')");
        if (tmp.length)
        {
            var hook = tmp.parent();
            $("body").append("<div id=\"dialog_maskch_tbm\" title=\"TBM Masked Channel List\"   class=\"dialog_monitor\"></div>");
            $("body").append("<div id=\"dialog_maskch_auto\" title=\"Auto Masked Channel List\" class=\"dialog_monitor\"></div>");
            $("body").append("<div id=\"dialog_maskch_disable\" title=\"Disabled Channel List\"class=\"dialog_monitor\"></div>");
            fixupRackMasklistButton(hook);
        }
        // Add the 'Dump system state' button if we're
        // dealing with a TCDSCentral.
        // tmp = jQuery("h1:contains('TCDSCentral')");
        // if (tmp.length)
        // {
        //     var hook = tmp;
        //     fixupSystemDumpButton(hook);
        // }

        // Make sure that data gets updated when switching tabs (since
        // normally only visible data, and not the data in the
        // background tabs) gets updated regularly from the AJAX
        // requests.
        jQuery(".xdaq-tab-nav").click(function() {
            applyData();
        });

        startUpdate();
    }
}

//-----------------------------------------------------------------------------

function isTCDSApplication()
{
    // Here we can use a feature of the XDAQ HyperDAQ framework and
    // check for the 'data-app-name' attribute of the HTML body.

    var res = false;
    var appName = jQuery("body").attr("data-app-name");
    if (typeof appName !== typeof undefined)
    {
        res = appName.startsWith("PixelMonitor::");
    }
    return res;
}

//-----------------------------------------------------------------------------

function finishLoadingScreen()
{
    var loadingScreen = window.loading_screen;
    if ((typeof loadingScreen !== typeof undefined) &&
        (!loadingScreen.finishing))
    {
        loadingScreen.finish();
    }
}

//-----------------------------------------------------------------------------

function doSetup()
{
    // Insert a <div> that we can use as scratch pad.
    jQuery("<div id=\"tcds-log-wrapper\"></div>").insertBefore("#xdaq-main");
    jQuery("#tcds-log-wrapper").append("<div id=\"tcds-log\"></div>");
    hideLog();

    // Add an onClick() handler for the scratch pad. When it is
    // clicked, the AJAX updates are started (if they are stopped).
    jQuery("#tcds-log-wrapper").click(function() {
        startUpdate();
    });
}

//-----------------------------------------------------------------------------

function startUpdate()
{
    updateLoop();
}

//-----------------------------------------------------------------------------
// WSI: wrap window.setTimeout as a  away to pause/resume AJAX update
//
function Timer(callback, delay) {
    var timerId, start, remaining = delay;

    this.pause = function() {
        window.clearTimeout(timerId);
        remaining -= new Date() - start;
    };

    this.resume = function() {
        start = new Date();
        window.clearTimeout(timerId);
        timerId = window.setTimeout(callback, remaining);
    };

    this.resume();
}

//-----------------------------------------------------------------------------

function updateLoop()
{
    pixelDaqTimer = new Timer(function(){
        var data;
        jQuery.ajax({
            url : tcds.updateUrl,
            dataType : "json",
            data : data,
            timeout : 2000,
            success : function(data, textStatus, jqXHR) {
                processAJAXSuccess(data, textStatus, jqXHR);
                tcds.updateFailCount = 0;
                updateLoop();
            },
            error : function(jqXHR, textStatus, errorThrown)
            {
                tcds.updateFailCount += 1;
                if (tcds.updateFailCount > tcds.maxUpdateFailCount)
                {
                    processAJAXError(jqXHR, textStatus, errorThrown);
                }
                else
                {
                    updateLoop();
                }
            }
        });
    }, tcds.updateInterval);
};

//-----------------------------------------------------------------------------

function processAJAXSuccess(data, textStatus, jqXHR)
{
    hideLog();

    // Check if we really received something. In case something went
    // really bad, we will receive an empty string.
    if (data === "")
    {
        showError("Received an empty JSON update. " +
                  "<br>" +
                  "Something must have gone horribly wrong " +
                  "on the application side. " +
                  "<br>" +
                  "Please have a look at the application log " +
                  "for more information.");
        return;
    }

    //----------

    try
    {
        // Process and apply the data to the DOM.
        // tcds.dataPrev = tcds.data;
        tcds.data = data;
        applyData();

        //----------

        // Replace the HyperDAQ framework page title with our own one
        // (which is of course better).
        const title = jQuery("#tcds-application-title").text();
        const pxSubTitle = jQuery("#pixelmonitor-application-subtitle").text();
        const subTitle = jQuery("#tcds-application-subtitle").text();
        var ourTitle = title;
        if (pxSubTitle != "undefined" && pxSubTitle != "") { ourTitle = ourTitle + "-" + pxSubTitle; }
        if (subTitle != "undefined" && subTitle != "") { ourTitle = ourTitle + '-' + subTitle; }
        document.title = ourTitle;

        //----------
        const windowurl = document.URL.split("=")[0];
        if ($('.firstColumn').length && $(".tcds-item-table-title:contains('FEDMonitor-table')").length){
            $('.firstColumn').each(function(){
                //loop all object
                var originalFirstColumn = $(this).text();
                var mId = originalFirstColumn.split("-")[1];
                $(this).html('<a href="' + windowurl + '=' + mId + '" target="_blank">' + originalFirstColumn + '</a>');
                var tts = data[originalFirstColumn]["TTS State"];
                if      (tts === "RDY") { $(this).removeClass('xdaq-orange xdaq-yellow xdaq-red'); $(this).addClass('xdaq-green'); }
                else if (tts === "WRN") { $(this).removeClass('xdaq-green xdaq-yellow xdaq-red'); $(this).addClass('xdaq-orange'); }
                else if (tts === "OOS") { $(this).removeClass('xdaq-green xdaq-orange xdaq-red'); $(this).addClass('xdaq-yellow'); }
                else if (tts === "BSY") { $(this).removeClass('xdaq-green xdaq-orange xdaq-yellow'); $(this).addClass('xdaq-red'); }
            });
        }
        
        if($('.firstColumn').length && $(".tcds-item-table-title:contains('FED Rack table')").length){
            var obj = document.getElementById('rackTable');
            sorttable.makeSortable(obj);
            $('.firstColumn').each(function(){
                    var tts = $(this).closest('tr').children('td[title="TTS"]').text();
                    var ste = $(this).closest('tr').children('td[title="state"]').text();
                    if (ste === "Configured") {
                        if      (tts === "RDY") { $(this).removeClass('xdaq-orange xdaq-yellow xdaq-red'); $(this).addClass('xdaq-green'); }
                        else if (tts === "WRN") { $(this).removeClass('xdaq-green xdaq-yellow xdaq-red'); $(this).addClass('xdaq-orange'); }
                        else if (tts === "OOS") { $(this).removeClass('xdaq-green xdaq-orange xdaq-red'); $(this).addClass('xdaq-yellow'); }
                        else if (tts === "BSY") { $(this).removeClass('xdaq-green xdaq-orange xdaq-yellow'); $(this).addClass('xdaq-red'); }
                    }
                    else {
                        $(this).removeClass('xdaq-orange xdaq-yellow xdaq-red xdaq-green');
                    }
            });
        }
        if ($('.firstColumn').length && $(".tcds-tab-name:contains('FED STATUS')").length){
            //var obj = document.getElementById('fedTable');
            //sorttable.makeSortable(obj);
            $('.firstColumn').each(function(){
                //loop all object
                var maskCode = parseInt($(this).closest('tr').children('.me').text()) * 4 +
                               parseInt($(this).closest('tr').children('.mt').text()) * 2 +
                               parseInt($(this).closest('tr').children('.mc').text()) * 1;
                // disabled in detectconfig.dat
                if (maskCode>=4) {$(this).closest('tr').removeClass(); $(this).closest('tr').addClass('xdaq-color-lightgrey');}
                // enabled, but get TBMmasked
                else if (maskCode>1 && maskCode<4) {$(this).closest('tr').removeClass(); $(this).closest('tr').addClass('xdaq-lightgrey');}
                // enabled, not yet TBMmasked, but get auto-masked
                else if (maskCode===1) {$(this).closest('tr').removeClass(); $(this).closest('tr').addClass('xdaq-color-orange');}
                else {$(this).closest('tr').removeClass();}
            });
        }
        if ($('.firstColumn').length && $(".tcds-item-table-title:contains('FED channel masking status table')").length){
            $('.firstColumn').each(function(){
                $(this).closest('tr').children('td').each(function(){
                    var maskbit = parseInt($(this).children(".ms").text(),2);
                    if (maskbit>=4)
                    {$(this).removeClass('xdaq-red xdaq-yellow xdaq-green'); $(this).addClass('xdaq-darkgrey');}
                    else if (maskbit<4 && maskbit>1)
                    {$(this).removeClass('xdaq-darkgrey xdaq-yellow xdaq-green'); $(this).addClass('xdaq-red');}
                    else if (maskbit===1)
                    {$(this).removeClass('xdaq-darkgrey xdaq-red xdaq-green'); $(this).addClass('xdaq-yellow');}
                    else if (maskbit===0)
                    {$(this).removeClass('xdaq-darkgrey xdaq-red xdaq-green'); $(this).addClass('xdaq-green');}
                    //$(this).children(".ms").html("");
                });
            });
        }
        tmp = jQuery(".tcds-tab-name:contains('FED-MONITOR-LIST')");
        if (tmp.length)
        {
            var objects = tcds.data["fs-itemset"]["flashlist information"];
            $.each(objects, function(key, value) {
                var host = objects[key]["url"].split("=")[0];
                var url  = objects[key]["url"];
                if (!monitorHostId[host]) {monitorHostId[host]=[];}
                if (jQuery.inArray(url, monitorHostId[host]) === -1)
                {
                    monitorHostId[host].push(url);
                }
            });

            var soapCmdFromOutside = tcds.data["Application configuration"]["SOAP Command From Supervisor"];
            if (soapCmdFromOutside==="Enable" && $('#button_rack_configure').is(':enabled')) {$('#button_rack_configure').click();}
            if (soapCmdFromOutside==="Disable" && $('#button_rack_halt').is(':enabled')) {$('#button_rack_halt').click();}
            fixupCrateControlForRack();
        }

        // Add mask list button for mask bit table
        tmp = jQuery(".tcds-tab-name:contains('CHANNEL-MASK-STATUS')");
        if (tmp.length)
        {
            stuffMaskInfo();
        }

        // A special case: we always expect an entry called 'Application
        // State' - 'Problem description'. If that does not exist, log an
        // error. If it does exist and is not "-", announce that there has
        // apparently been some problem in the XDAQ application.
        var tmp0 = "Application state";
        var tmp1 = "Problem description";
        // Verify that this element exists in the JSON data we received in
        // response to our AJAX call.
        var tmpContents = "-";
        try
        {
            tmpContents = data[tmp0][tmp1];
        }
        catch (err)
        {
            showError("Expected (but could not find) an item called " +
                      "'" + tmp0 + "' - '" + tmp1 + "'" +
                      " in the JSON update data.");
        }
        if (tmpContents != "-")
        {
            showError("This application requires attention.");
        }

        //----------

        // BUG BUG BUG
        // This should be somewhere else, really.

        // Manipulation of the TTCSpy-in-the-PI configuration button and
        // the LPM/CPM random-rate button.
        tmp0 = "Application state";
        tmp1 = "Application FSM state";
        var fsmState = tcds.data[tmp0][tmp1];
        if ((fsmState == "Halted") ||
            (fsmState == "Configuring") ||
            (fsmState == "Zeroing"))
        {
            // In these states we are not (yet fully) connected to the
            // hardware.
            jQuery("#button_configure_ttcspy").prop("disabled", true);
            jQuery("#button_configure_randomrate").prop("disabled", true);
            jQuery("#button_monitor_configure").prop("disabled", false);
            jQuery("#button_monitor_halt").prop("disabled", true);
        }
        else
        {
            // In anything but the above we are connected to the hardware.
            jQuery("#button_configure_ttcspy").prop("disabled", false);
            // Argh! Only enable the button if random-triggers are
            // actually enabled.
            var button = jQuery("#button_configure_randomrate");
            if (button.length)
            {
                var tmp = tcds.data["itemset-inputs"]["Random-trigger generator"];
                var randomsEnabled = (tmp == "enabled");
                button.prop("disabled", !randomsEnabled);
            }
            jQuery("#button_monitor_configure").prop("disabled",true);
            jQuery("#button_monitor_halt").prop("disabled",false);

        }
        // BUG BUG BUG end

    }
    catch(err)
    {
        showError("Caught an exception: " + err);
    }

    //----------

    finishLoadingScreen();
}

//-----------------------------------------------------------------------------

function processAJAXError(jqXHR, textStatus, errorThrown)
{
    var reasonString = "";
    var baseString = "";

    // Let's at least catch the usual problems. If nothing known
    // matches show a blanket fail message.
    if (textStatus == "parsererror")
    {
        baseString = "Something went wrong with the AJAX update.";
        reasonString = "An error occurred parsing the received JSON data ('" + errorThrown.message + "').";
    }
    else if (textStatus == "error")
    {
        if (errorThrown == "Bad Request")
        {
            reasonString = "Cannot connect to the XDAQ application ('Bad Request').";
        }
        else if ((jqXHR.status == 404) || (errorThrown == "Not Found"))
        {
            reasonString = "Cannot find the remote location ('Not Found').";
        }
        else
        {
            reasonString = "Lost connection to the XDAQ application ('Connection Failed').";
        }
    }
    else if (textStatus == "timeout")
    {
        reasonString = "Lost connection to the XDAQ application (connection timed out).";
    }
    else
    {
        baseString = "Something went wrong with the AJAX update.";
        reasonString = "No clue what though.";
    }
    var msg = "";
    if (baseString.length > 0)
    {
        msg = baseString;
    }
    if (reasonString.length > 0)
    {
        if (msg.length > 0)
        {
            msg += "<br>";
        }
        msg += reasonString;
    }
    if (msg.length > 0)
    {
        msg += "<br>";
    }
    msg += "Updating stopped... Click this box to restart.";
    showError(msg);
    console.error("Error while obtaining/parsing JSON data: " + msg);

    //----------

    finishLoadingScreen();
}

//-----------------------------------------------------------------------------

function applyData()
{
    var data = tcds.data;

    //----------

    var scripts = jQuery("script[type='text/x-dot-template']");
    jQuery(scripts).each(function() {
        if (jQuery(this).parent().is(":visible") || !tcds.initialised)
        {
            var templateStr = jQuery(this).text();
            var template = tcds.templateCache[templateStr];
            if (template == undefined)
            {
                template = doT.template(templateStr);
                tcds.templateCache[templateStr] = template;
            }
            // NOTE: The following is an attempt to handle both real
            // HTML and strings with HTML-like characters.
            var tmp = template(data);

            var html = hackIntoHTML(tmp);
            var target = jQuery(this).siblings(".target")[0];
            if (jQuery(target).html() != html)
            {
                jQuery(target).html(html);
            }
        }
    });

    //----------

    // Fill the application state history table.
    var placeholderName = "#application-status-history-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be some sort of TCDS application.
        var data = tcds.data["itemset-application-status-history"]["Application status history"];
        var columns = [
            {id: "timestamp", name: "Timestamp", field: "Timestamp", width: 200},
            {id: "msg", name: "Message", field: "Message", width: 400}
        ];
        updateGrid("apphist-grid", placeholder, data, columns, true);
    }

    //----------

    // Fill the TTCSpy log table.
    var placeholderName = "#ttcspylog-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a PIController.
        var spyData = tcds.data["itemset-ttcspylog"]["TTCSpy log contents"];
        var columns = [
            {id: "entry", name: "Entry #", field: "Entry"},
            {id: "delta", name: "Delta-t (BX)", field: "Delta"},
            {id: "bx", name: "BX #", field: "BX"},
            {id: "type", name: "Type", field: "Type"},
            {id: "flags", name: "Flags", field: "Flags"},
            {id: "data", name: "Data", field: "Data"},
            {id: "address", name: "B-command address", field: "Address"}
        ];
        updateGrid("ttcspylog-grid", placeholder, spyData, columns, false);
    }

    //----------

    // Fill the TTS log table.

    function ttsLogFormatter(row, cell, value, columnDef, dataContext)
    {
        return "<span tts_val='" + value + "'>" + value + "</span>";
    }

    var placeholderName = "#ttslog-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a PIController.
        var logData = tcds.data["itemset-ttslog"]["TTS log contents"];
        var columns = [
            {id: "entry", name: "Entry #", field: "Entry"},
            {id: "orbit", name: "Orbit #", field: "Orbit"},
            {id: "bx", name: "BX #", field: "BX"},
            {id: "in_fed1", name: "FED1", field: "FED1", formatter: ttsLogFormatter},
            {id: "in_fed2", name: "FED2", field: "FED2", formatter: ttsLogFormatter},
            {id: "in_fed3", name: "FED3", field: "FED3", formatter: ttsLogFormatter},
            {id: "in_fed4", name: "FED4", field: "FED4", formatter: ttsLogFormatter},
            {id: "in_fed5", name: "FED5", field: "FED5", formatter: ttsLogFormatter},
            {id: "in_fed6", name: "FED6", field: "FED6", formatter: ttsLogFormatter},
            {id: "in_fed7", name: "FED7", field: "FED7", formatter: ttsLogFormatter},
            {id: "in_fed8", name: "FED8", field: "FED8", formatter: ttsLogFormatter},
            {id: "in_fed9", name: "FED9", field: "FED9", formatter: ttsLogFormatter},
            {id: "in_fed10", name: "FED10", field: "FED10", formatter: ttsLogFormatter},
            {id: "in_rj45", name: "RJ45", field: "RJ45", formatter: ttsLogFormatter},
            {id: "out_or", name: "OR output", field: "OR", formatter: ttsLogFormatter},
            {id: "out_force", name: "'Forced' output", field: "FORCE", formatter: ttsLogFormatter},
            {id: "out_pi", name: "PI output", field: "OUT", formatter: ttsLogFormatter}
        ];
        logData.getItemMetadata = function(row) {
            var outputState = this[row]['OUT'];
            if (outputState != "ready")
            {
                return {cssClasses: 'tts_log_row_bad'}
            }
            else
            {
                return {cssClasses: 'tts_log_row_good'}
            }
        };
        updateGrid("ttslog-grid", placeholder, logData, columns, false);
    }

    //----------

    // Fill the recent-nibbles history table.
    var placeholderName = "#nibblehistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a CPMController.
        var historyData = tcds.data["itemset-cpm-nibble-history"]["Recent nibbles"];
        var columns = [
            {id: "runnum", name: "Run number", field: "RunNumber"},
            {id: "sectionnum", name: "Section number", field: "SectionNumber"},
            {id: "nibblenum", name: "Nibble number", field: "NibbleNumber"},
            {id: "runactive", name: "Run active", field: "RunActive"},
            {id: "numorbits", name: "# orbits", field: "NumOrbits"},
            {id: "triggerrate", name: "Trigger rate (Hz)", field: "TriggerRate"},
            {id: "suppressedtriggerrate", name: "Suppressed-trigger rate (Hz)", field: "SuppressedTriggerRate"},
            {id: "deadtime", name: "Deadtime (% of time)", field: "Deadtime"},
            {id: "deadtimebeamactive", name: "Deadtime with beam active (% of time)", field: "DeadtimeBeamActive"}
        ];
        updateGrid("nibblehistory-grid", placeholder, historyData, columns, true);
    }

    //----------

    // Fill the recent-sections history table.
    var placeholderName = "#sectionhistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a CPMController.
        var historyData = tcds.data["itemset-cpm-section-history"]["Recent sections"];
        var columns = [
            {id: "runnum", name: "Run number", field: "RunNumber"},
            {id: "sectionnum", name: "Section number", field: "SectionNumber"},
            {id: "numnibbles", name: "# nibbles", field: "NumNibbles"},
            {id: "triggerrate", name: "Trigger rate (Hz)", field: "TriggerRate"},
            {id: "suppressedtriggerrate", name: "Suppressed-trigger rate (Hz)", field: "SuppressedTriggerRate"},
            {id: "deadtime", name: "Deadtime (% of time)", field: "Deadtime"},
            {id: "deadtimebeamactive", name: "Deadtime with beam active (% of time)", field: "DeadtimeBeamActive"}
        ];
        updateGrid("sectionhistory-grid", placeholder, historyData, columns, true);
    }

    //----------

    // Fill the recent-BRILDAQ history table.
    var placeholderName = "#brildaqhistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be a BRILDAQChecker.
        var historyData = tcds.data["itemset-brildaq-history"]["Recent TCDS BRILDAQ data"];
        var columns = [
            // {id: "formatversion", name: "Data format version", field: "FormatVersion"},
            {id: "timestamp", name: "BST timestamp", field: "BSTTimestamp"},
            {id: "fillnum", name: "Fill number", field: "FillNumber"},
            {id: "runnum", name: "Run number", field: "RunNumber"},
            {id: "sectionnum", name: "Section number", field: "SectionNumber"},
            {id: "nibblenum", name: "Nibble number", field: "NibbleNumber"},
            {id: "runactive", name: "Run active", field: "CMSRunActive"},
            {id: "deadtime", name: "Deadtime (% of time)", field: "Deadtime"},
            {id: "deadtimebeamactive", name: "Deadtime with beam-active (% of time)", field: "DeadtimeBeamActive"}
        ];
        updateGrid("brildaqhistory-grid", placeholder, historyData, columns, true);
    }

    //----------
    // Fill the APVE pipeline history table.
    var placeholderName = "#apvesimhistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be an APVEController.
        var historyData = tcds.data["itemset-simhist"]["Simulated APV pipeline history"];
        var columns = [
            {id: "eventnumber", name: "Event number", field: "EventNumber"},
            {id: "address", name: "Pipeline address", field: "PipelineAddress"},
            {id: "graycode", name: "Gray code", field: "PipelineAddressGrayCode"},
            {id: "deltaaddress", name: "Address differences", field: "PipelineAddressDelta"}
        ];
        updateGrid("apvesimhistory-grid", placeholder, historyData, columns, false);
    }

    //----------

    // Fill the APVE status history table.
    var placeholderName = "#apvestatushistory-placeholder";
    var placeholder = jQuery(placeholderName);
    if (placeholder.length)
    {
        // If the placeholder exists, this must be an APVEController.
        var historyData = tcds.data["itemset-statushist"]["TTS and APVE status history"];
        var columns = [
            {id: "orbitnumber", name: "Orbit number", field: "OrbitNumber"},
            {id: "bxnumber", name: "BX number", field: "BXNumber"},
            {id: "ttsstate", name: "Output TTS state", field: "TTSState"},
            {id: "fmmttsstate", name: "FMM TTS", field: "FMMTTSState"},
            {id: "apvttsstate", name: "APV TTS", field: "APVTTSState"},
            {id: "apvoosreason", name: "APV 'Out-of-sync' reason", field: "APVOOSReason"},
            {id: "apverrorreason", name: "APV 'Error' reason", field: "APVErrorReason"},
            {id: "flags", name: "Flags", field: "Flags"}
        ];
        updateGrid("apvestatushistory-grid", placeholder, historyData, columns, false);
    }

    //----------

    placeholderName = "#l1ahistos-placeholder";
    placeholder = jQuery(placeholderName);
    if (placeholder.length && placeholder.is(":visible"))
    {
        // First extract the data from the JSON update and remodel it
        // a little.
        const histoDataTmp = tcds.data["itemset-l1a-histos"];
        var histoData = [];
        const labels = Object.keys(histoDataTmp);
        for (var i = 0; i != labels.length; ++i)
        {
            var label = labels[i];
            var dataTmp = histoDataTmp[label];
            //if ((dataTmp != "-") && (Math.max.apply(Math, dataTmp) != 0))
            if (dataTmp != "-")
            {
                histoData.push({label: label,
                                data: dataTmp});
            }
        }

        if (!histoData.length)
        {
            placeholder.text("-");
            delete tcds.l1ahistos;
            tcds.l1ahistos = undefined;
        }
        else
        {
            if (tcds.l1ahistos === undefined)
            {
                jQuery(placeholderName).html(jQuery("<div></div>")
                                             .attr("id", "chart-container")
                                             .attr("class", "chart-container"));
                tcds.l1ahistos = new L1AHistos("#chart-container");
                tcds.l1ahistos.init();
                tcds.l1ahistos.setData(histoData);
                tcds.l1ahistos.plot();
            }
            else
            {
                tcds.l1ahistos.setData(histoData);
                tcds.l1ahistos.update();
            }
        }
    }

    //----------

    tcds.initialised = true;
}

//-----------------------------------------------------------------------------

function updateGrid(id, container, gridData, columns, reverse)
{
    if (jQuery(container).length && jQuery(container).is(":visible"))
    {
        if (gridData == "-")
        {
            jQuery(container).text("-");
            delete tcds.grids[id];
            tcds.grids[id] = undefined;
        }
        else
        {
            if (reverse)
            {
                gridData.reverse();
            }

            var grid = tcds.grids[id];
            if (grid === undefined)
            {
                var options = {
                    enableCellNavigation: true,
                    enableColumnReorder: false,
                    forceFitColumns: true,
                    fullWidthRows: true,
                    multiSelect: true
                };
                jQuery(container).html(jQuery("<div></div>").attr("id", id).attr("class", "grid"));
                grid = new Slick.Grid("#" + id, gridData, columns, options);
                grid.setSelectionModel(new Slick.RowSelectionModel());
                grid.registerPlugin(new Slick.AutoTooltips({enableForHeaderCells: true}));
                grid.render();
                tcds.grids[id] = grid;

                if (!reverse)
                {
                    // NOTE: This is not particularly pretty, no...
                    jQuery(container).prepend("<input type=\"checkbox\" id=\"test_button\" style=\"display: inline-block;\"/><label for=\"test_button\" style=\"display: inline-block;\">Follow tail of table</label>");
                }
            }
            else
            {
                grid.setData(gridData);
                grid.invalidate();
                grid.resizeCanvas();
                grid.render();
            }

            // Scroll to bottom of requested.
            var button = jQuery("#test_button");
            if (button.is(":checked"))
            {
                grid.scrollRowIntoView(grid.getDataLength());
            }
        }
    }
}

//-----------------------------------------------------------------------------

function hideLog()
{
    jQuery("#tcds-log-wrapper").hide();
}

//-----------------------------------------------------------------------------

function showLog()
{
    jQuery("#tcds-log-wrapper").show();
}

//-----------------------------------------------------------------------------

function showError(htmlText)
{
    jQuery("#tcds-log").html(htmlText);
    showLog();
    // Mark the page title with a warning.
    var tmp = document.title;
    var ind = tmp.indexOf("(");
    if (ind >= 0)
    {
        tmp = tmp.substring(0, ind);
    }
    tmp = tmp + " (!)";
    document.title = tmp;
}

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------

function configureMonitor(toURL)
{
    var cmdName = "Configure";
    parameters = [
    {
        "name": "hardwareConfigurationString",
        "type": "string",
        "value": ""
    }];

    var soapMsg = buildSOAPCommand(cmdName, parameters);

    jQuery.ajax({
        type: "post",
        url: toURL,
        contentType: "text/xml",
        dataType: "xml",
        data: soapMsg,
        processData: false,
        success: function(data, textStatus, req) {
            var fault = jQuery(data).find("Fault");
            if (fault.size())
            {
                var msg = "Something went wrong";
                var tmpFaultString = fault.find("faultstring");
                var tmpDetail = fault.find("detail");
                if (!tmpFaultString.size() && !tmpDetail)
                {
                    msg = msg + ", but it is not clear exactly what."
                }
                else
                {
                    if (tmpFaultString.size())
                    {
                        msg = msg + ": " + tmpFaultString.text();
                    }
                    if (tmpDetail.size())
                    {
                        if (tmpFaultString.size())
                        {
                            msg = msg + ": ";
                        }
                        msg += tmpDetail.text();
                    }
                }
                alert(msg);
            }
        },
        error: function(data, status, req) {
            alert(req.responseText + " " + status);
        }
    });
}

//-----------------------------------------------------------------------------

function sendSoapSwitch(cmd, urls)
{
    var cmdName = cmd;
    var monitorURLs = urls;
    //console.log(monitorURLs)

    parameters = [
    {
        "name" : "targetAppURLs",
        "type" : "string",
        "value" : monitorURLs.join(", ")
    }
    ];
    var soapMsg = buildSOAPCommand(cmdName, parameters);
    jQuery.ajax({
        type: "post",
        url: tcds.baseUrl,
        contentType: "text/xml",
        dataType: "xml",
        data: soapMsg,
        processData: false,
        success: function(data, textStatus, req) {
            var fault = jQuery(data).find("Fault");
            if (fault.size())
            {
                var msg = "Something went wrong";
                var tmpFaultString = fault.find("faultstring");
                var tmpDetail = fault.find("detail");
                if (!tmpFaultString.size() && !tmpDetail)
                {
                    msg = msg + ", but it is not clear exactly what."
                }
                else
                {
                    if (tmpFaultString.size())
                    {
                        msg = msg + ": " + tmpFaultString.text();
                    }
                    if (tmpDetail.size())
                    {
                        if (tmpFaultString.size())
                        {
                            msg = msg + ": ";
                        }
                        msg += tmpDetail.text();
                    }
                }
                alert(msg);
            }
        },
        error: function(data, status, req) {
            alert(req.responseText + " " + status);
        }
    });
}

//-----------------------------------------------------------------------------

function haltMonitor(toURL)
{
    var cmdName = "Halt";
    parameters = [];

    var soapMsg = buildSOAPCommand(cmdName, parameters);

    jQuery.ajax({
        type: "post",
        url: toURL,
        contentType: "text/xml",
        dataType: "xml",
        data: soapMsg,
        processData: false,
        success: function(data, textStatus, req) {
            var fault = jQuery(data).find("Fault");
            if (fault.size())
            {
                var msg = "Something went wrong";
                var tmpFaultString = fault.find("faultstring");
                var tmpDetail = fault.find("detail");
                if (!tmpFaultString.size() && !tmpDetail)
                {
                    msg = msg + ", but it is not clear exactly what."
                }
                else
                {
                    if (tmpFaultString.size())
                    {
                        msg = msg + ": " + tmpFaultString.text();
                    }
                    if (tmpDetail.size())
                    {
                        if (tmpFaultString.size())
                        {
                            msg = msg + ": ";
                        }
                        msg += tmpDetail.text();
                    }
                }
                alert(msg);
            }
        },
        error: function(data, status, req) {
            alert(req.responseText + " " + status);
        }
    });
}

//-----------------------------------------------------------------------------

function buildSOAPCommand(cmdName, params)
{
    var soapMsg = "<?xml version=\"1.0\" ?>" +
        "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"" +
        "              xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"" +
        "              xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
        "<env:Header/>" +
        "<env:Body>" +
        "<xdaq:" + cmdName + " xdaq:actionRequestorId=\"FEDMonitor\" xmlns:xdaq=\"urn:xdaq-soap:3.0\">";

    for (var i = 0; i < params.length; ++i)
    {
        var par = params[i];
        soapMsg += "<xdaq:" + par.name + " xsi:type=\"xsd:" + par.type + "\">" +
            par.value +
            "</xdaq:" + par.name + ">";
    }

    soapMsg += "</xdaq:" + cmdName + ">" +
        "</env:Body>" +
        "</env:Envelope>";

    soapMsg = jQuery.trim(soapMsg);
    //console.log(soapMsg);
    return soapMsg;
}

//-----------------------------------------------------------------------------

function fixupMonitorConfigButton(hook)
{
    //hook.append("<br />");
    hook.append("<button id=\"button_monitor_configure\" class=\"button_monitor\">WIRE-ME</button>");
    $("#button_monitor_configure").button().on("click", function() {
            configureMonitor(tcds.baseurl);
    });
}

//-----------------------------------------------------------------------------

function fixupMonitorHaltButton(hook)
{
    hook.append("<button id=\"button_monitor_halt\" class=\"button_monitor\">UNWIRE-ME</button>");
    $("#button_monitor_halt").button().on("click", function() {
            haltMonitor(tcds.baseUrl);
    });
}

//-----------------------------------------------------------------------------

function fixupRackConfigButton(hook)
{
    hook.append("<button id=\"button_rack_configure\" class=\"button_monitor\">WIRE ALL</button>");
    $("#button_rack_configure").button().click(function() {
            var i = 0;
            // put keys of monitorHostId into an array
            var monitorHostIdKey = [];
            $.each(monitorHostId, function(key, value){
                monitorHostIdKey.push(key);        
            });
            // set 1 sec interval between each soap message sending
            function myLoop() {
                setTimeout(function() {
                    sendSoapSwitch("EnableAllMonitors", monitorHostId[monitorHostIdKey[i]]);
                    i++;
                    if ( i< monitorHostIdKey.length ) {
                        myLoop();
                    }
                }, 500)
            }
            myLoop();

            return true;
    });
}

//-----------------------------------------------------------------------------

function fixupRackHaltButton(hook)
{
    hook.append("<button id=\"button_rack_halt\" class=\"button_monitor\">UNWIRE ALL</button>");
    $("#button_rack_halt").button().click(function() {
            var i = 0;
            // put keys of monitorHostId into an array
            var monitorHostIdKey = [];
            $.each(monitorHostId, function(key, value){
                monitorHostIdKey.push(key);        
            });
            // set 1 sec delay interval in sending soap messages to each crate
            function myLoop() {
                setTimeout(function() {
                    sendSoapSwitch("DisableAllMonitors", monitorHostId[monitorHostIdKey[i]]);
                    i++;
                    if ( i< monitorHostIdKey.length ) {
                        myLoop();
                    }
                }, 1000)
            }
            myLoop();

            return true;
    });
}

function fixupRackMasklistButton(hook)
{
    hook.append("<button id=\"button_rack_masklist_tbm\" class=\"button_monitor\">Show TBM Masked Channels</button>");
    hook.append("<button id=\"button_rack_masklist_auto\" class=\"button_monitor\">Show Auto Masked Channels</button>");
    hook.append("<button id=\"button_rack_masklist_disable\" class=\"button_monitor\">Show Disabled Channels</button>");

    var dialog_tbm = $("#dialog_maskch_tbm").dialog({
        autoOpen: false,
        autoResize: true,
        height: "auto",
        width: "40%",
        modal: true,
        resizable: true
    });

    $("#button_rack_masklist_tbm").button().on('click', function() {
        dialog_tbm.dialog("open");        
    });
    
    var dialog_auto = $("#dialog_maskch_auto").dialog({
        autoOpen: false,
        autoResize: true,
        height: "auto",
        width: "40%",
        modal: true,
        resizable: true
    });

    $("#button_rack_masklist_auto").button().on('click', function() {
        dialog_auto.dialog("open");        
    });

    var dialog_disable = $("#dialog_maskch_disable").dialog({
        autoOpen: false,
        autoResize: true,
        height: "auto",
        width: "40%",
        modal: true,
        resizable: true
    });

    $("#button_rack_masklist_disable").button().on('click', function() {
        dialog_disable.dialog("open");        
    });

}

function stuffMaskInfo()
{
    tbmMasked_ = {"fpix":{"1":[], "2":[], "3":[]},
                  "bpix":{"1":[], "2":[], "3":[], "4":[]} };
    disabled_ = {"fpix":{"1":[], "2":[], "3":[]},
                 "bpix":{"1":[], "2":[], "3":[], "4":[]} };
    autoMasked_ = {"fpix":{"1":[], "2":[], "3":[]},
                   "bpix":{"1":[], "2":[], "3":[], "4":[]} };

    $("#rackTable_mask").find("td.xdaq-tooltip").each(function(){
        if ($(this).hasClass("xdaq-red")) {
            var roctmp = $(this).children('.xdaq-tooltip-msg').text();
            //console.log(roctmp);
            var tmp = roctmp.split('Pix')[0];
            if (tmp.charAt(tmp.length-1)==='F') {
                var dsk = roctmp.split('_D')[1].charAt(0);
                tbmMasked_['fpix'][dsk].push(roctmp);
            }
            else if (tmp.charAt(tmp.length-1)==='B') {
                var lyr = roctmp.split('LYR')[1].charAt(0);
                tbmMasked_['bpix'][lyr].push(roctmp);
            }
        }
        else if ($(this).hasClass("xdaq-darkgrey")) {
            var roctmp = $(this).children('.xdaq-tooltip-msg').text();
            var tmp = roctmp.split('Pix')[0];
            if (tmp.charAt(tmp.length-1)==='F') {
                var dsk = roctmp.split('_D')[1].charAt(0);
                disabled_['fpix'][dsk].push(roctmp);
            }
            else if (tmp.charAt(tmp.length-1)==='B') {
                var lyr = roctmp.split('LYR')[1].charAt(0);
                disabled_['bpix'][lyr].push(roctmp);
            }
        }
        else if ($(this).hasClass("xdaq-yellow")) {
            var roctmp = $(this).children('.xdaq-tooltip-msg').text();
            var tmp = roctmp.split('Pix')[0];
            if (tmp.charAt(tmp.length-1)==='F') {
                var dsk = roctmp.split('_D')[1].charAt(0);
                autoMasked_['fpix'][dsk].push(roctmp);
            }
            else if (tmp.charAt(tmp.length-1)==='B') {
                var lyr = roctmp.split('LYR')[1].charAt(0);
                autoMasked_['bpix'][lyr].push(roctmp);
            }
        }
    });

    var maskedDialogHtml = "";
    $.each(tbmMasked_, function(pix, indexRoc){
        maskedDialogHtml += "<p><b>"+pix+":</b></p>";
        var delim = (pix==="fpix") ? "Disk" : "Layer";
        $.each(indexRoc, function(index, rocs){
                maskedDialogHtml += "<p><i>--"+delim+index+" "+rocs.length+"ch in total</i></p>";
                $.each(rocs, function(i, r){
                    maskedDialogHtml += r+"<br>";    
                });
        });
    });
    $("#dialog_maskch_tbm").html('');
    $("#dialog_maskch_tbm").append(maskedDialogHtml);

    maskedDialogHtml = "";
    $.each(autoMasked_, function(pix, indexRoc){
        maskedDialogHtml += "<p><b>"+pix+":</b></p>";
        var delim = (pix==="fpix") ? "Disk" : "Layer";
        $.each(indexRoc, function(index, rocs){
                maskedDialogHtml += "<p><i>--"+delim+index+" "+rocs.length+"ch in total</i></p>";
                $.each(rocs, function(i, r){
                    maskedDialogHtml += r+"<br>";    
                });
        });
    });
    $("#dialog_maskch_auto").html('');
    $("#dialog_maskch_auto").append(maskedDialogHtml);

    maskedDialogHtml = "";
    $.each(disabled_, function(pix, indexRoc){
        maskedDialogHtml += "<p><b>"+pix+":</b></p>";
        var delim = (pix==="fpix") ? "Disk" : "Layer";
        $.each(indexRoc, function(index, rocs){
                maskedDialogHtml += "<p><i>--"+delim+index+" "+rocs.length+"ch in total</i></p>";
                $.each(rocs, function(i, r){
                    maskedDialogHtml += r+"<br>";    
                });
        });
    });
    $("#dialog_maskch_disable").html('');
    $("#dialog_maskch_disable").append(maskedDialogHtml);

}

function fixupPlayButton(hook)
{
    hook.append("<button id=\"button_play_on\" class=\"button_monitor\">FREEZE table</button>");
    hook.append("<button id=\"button_play_off\" class=\"button_monitor\" disabled=true>WAKE table</button>");
    $('#button_play_on').button().click(function() {
            $('#button_play_on').button("disable");
            $('#button_play_off').button("enable");
            pixelDaqTimer.pause();
    });
    $('#button_play_off').button().click(function() {
            $('#button_play_on').button("enable");
            $('#button_play_off').button("disable");
            pixelDaqTimer.resume();
    });
}

function fixupCrateControlForRack()
{
    var buttonContent ="";
    for(var host in monitorHostId)
    {
        var strippedHost = host.split(":")[1].substr(2).split(".")[0];
        buttonContent += "<br />";
        buttonContent += "<span style=\"margin-right: 15px;\"><strong>HOST</strong>: "+strippedHost+".cms</span>";
        buttonContent += "<button id=\"config-"+strippedHost+"\" class=\"button_monitor\">Wire crate</button>";
        buttonContent += "<button id=\"halt-"+strippedHost+"\" class=\"button_monitor\">Unwire crate</button>";

    }
    $("#crateButtons").html(buttonContent);
    jQuery.each(monitorHostId, function(host, urls)
    {
        var strippedHost = host.split(":")[1].substr(2).split(".")[0];
        $('#config-'+strippedHost).button().click(function() {
                sendSoapSwitch("EnableAllMonitors", urls);
                return true;
        });
        $('#halt-'+strippedHost).button().click(function() {
                sendSoapSwitch("DisableAllMonitors", urls);
                return true;
        });
        var hostKey = host.split("/urn")[0];
        if (tcds.data["Application configuration"][hostKey]==="Configured") {
            $('#config-'+strippedHost).button("disable");
            $('#halt-'+strippedHost).button("enable");
        } else if (tcds.data["Application configuration"][hostKey]==="Halted")
        {
            $('#config-'+strippedHost).button("enable");
            $('#halt-'+strippedHost).button("disable");
        } else
        {
            $('#config-'+strippedHost).button("disable");
            $('#halt-'+strippedHost).button("disable");
        }
    });
    var monitorHostStateValue = [];
    for (var host in monitorHostId)
    {
        var hostKey = host.split("/urn")[0];
        monitorHostStateValue.push(tcds.data["Application configuration"][hostKey]);
    }
    var crateOn = $.grep(monitorHostStateValue, function(i){return i==="Configured";});
    if (!crateOn.length) {
        $('#button_rack_configure').button("enable");
        $('#button_rack_halt').button("disable");
        //console.log("All off");
    } else if (monitorHostStateValue.length===crateOn.length) {
        $('#button_rack_configure').button("disable");
        $('#button_rack_halt').button("enable");
        //console.log("All on");
    } else {
        $('#button_rack_configure').button("disable");
        $('#button_rack_halt').button("disable");
        //console.log("Some on, some off");
    }
}

//-----------------------------------------------------------------------------

// A string cleaner copy-pasted from here:
//   http://phpjs.org/functions/htmlspecialchars

function htmlspecialchars(string, quote_style, charset, double_encode) {
  //       discuss at: http://phpjs.org/functions/htmlspecialchars/
  //      original by: Mirek Slugen
  //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //      bugfixed by: Nathan
  //      bugfixed by: Arno
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //         input by: Ratheous
  //         input by: Mailfaker (http://www.weedem.fr/)
  //         input by: felix
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  //             note: charset argument not supported
  //        example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
  //        returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
  //        example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
  //        returns 2: 'ab"c&#039;d'
  //        example 3: htmlspecialchars('my "&entity;" is still here', null, null, false);
  //        returns 3: 'my &quot;&entity;&quot; is still here'

  var optTemp = 0,
    i = 0,
    noquotes = false;
  if (typeof quote_style === 'undefined' || quote_style === null) {
    quote_style = 2;
  }
  string = string.toString();
  if (double_encode !== false) { // Put this first to avoid double-encoding
    string = string.replace(/&/g, '&amp;');
  }
  string = string.replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');

  var OPTS = {
    'ENT_NOQUOTES': 0,
    'ENT_HTML_QUOTE_SINGLE': 1,
    'ENT_HTML_QUOTE_DOUBLE': 2,
    'ENT_COMPAT': 2,
    'ENT_QUOTES': 3,
    'ENT_IGNORE': 4
  };
  if (quote_style === 0) {
    noquotes = true;
  }
  if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
    quote_style = [].concat(quote_style);
    for (i = 0; i < quote_style.length; i++) {
      // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
      if (OPTS[quote_style[i]] === 0) {
        noquotes = true;
      } else if (OPTS[quote_style[i]]) {
        optTemp = optTemp | OPTS[quote_style[i]];
      }
    }
    quote_style = optTemp;
  }
  if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
    string = string.replace(/'/g, '&#039;');
  }
  if (!noquotes) {
    string = string.replace(/"/g, '&quot;');
  }

  return string;
}

//-----------------------------------------------------------------------------

// Check if the browser considers this proper HTML. Overly strict, but
// okay. For details, see:
//   http://stackoverflow.com/questions/10026626/check-if-html-snippet-is-valid-with-javascript
function isProperHTML(html)
{
  var doc = document.createElement("div");
  doc.innerHTML = html;
  return (doc.innerHTML === html);
}

//-----------------------------------------------------------------------------

function hackIntoHTML(text)
{
    var res = jQuery("<div></div>").html(text).html();
    return res;
}

//-----------------------------------------------------------------------------

// From php.js: http://phpjs.org/functions/htmlspecialchars
function htmlspecialchars(string, quote_style, charset, double_encode) {
  //       discuss at: http://phpjs.org/functions/htmlspecialchars/
  //      original by: Mirek Slugen
  //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //      bugfixed by: Nathan
  //      bugfixed by: Arno
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //         input by: Ratheous
  //         input by: Mailfaker (http://www.weedem.fr/)
  //         input by: felix
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  //             note: charset argument not supported
  //        example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
  //        returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
  //        example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
  //        returns 2: 'ab"c&#039;d'
  //        example 3: htmlspecialchars('my "&entity;" is still here', null, null, false);
  //        returns 3: 'my &quot;&entity;&quot; is still here'

  var optTemp = 0,
    i = 0,
    noquotes = false;
  if (typeof quote_style === 'undefined' || quote_style === null) {
    quote_style = 2;
  }
  string = string.toString();
  if (double_encode !== false) { // Put this first to avoid double-encoding
    string = string.replace(/&/g, '&amp;');
  }
  string = string.replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');

  var OPTS = {
    'ENT_NOQUOTES': 0,
    'ENT_HTML_QUOTE_SINGLE': 1,
    'ENT_HTML_QUOTE_DOUBLE': 2,
    'ENT_COMPAT': 2,
    'ENT_QUOTES': 3,
    'ENT_IGNORE': 4
  };
  if (quote_style === 0) {
    noquotes = true;
  }
  if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
    quote_style = [].concat(quote_style);
    for (i = 0; i < quote_style.length; i++) {
      // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
      if (OPTS[quote_style[i]] === 0) {
        noquotes = true;
      } else if (OPTS[quote_style[i]]) {
        optTemp = optTemp | OPTS[quote_style[i]];
      }
    }
    quote_style = optTemp;
  }
  if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
    string = string.replace(/'/g, '&#039;');
  }
  if (!noquotes) {
    string = string.replace(/"/g, '&quot;');
  }

  return string;
}

//-----------------------------------------------------------------------------

// Some debug helper methods (copy-pasted from the internet somewhere).

function syntaxHighlight(json)
{
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

function prettyPrint(obj)
{
    return JSON.stringify(obj);
}

function getRandomInt(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

//-----------------------------------------------------------------------------
