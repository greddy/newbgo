#include "PixelMonitor/utilslayer/InfoSpaceItem.h"

// BUG BUG BUG
#include <iostream>
// BUG BUG BUG end

PixelMonitor::utilslayer::InfoSpaceItem::InfoSpaceItem(std::string const &name,
                                                       ItemType const type,
                                                       std::string const &format,
                                                       UpdateType const updateType,
                                                       bool const isValid)
    : name_(name),
      type_(type),
      format_(format),
      updateType_(updateType),
      isValid_(isValid) {
}

std::string
PixelMonitor::utilslayer::InfoSpaceItem::name() const {
    return name_;
}

PixelMonitor::utilslayer::InfoSpaceItem::ItemType
PixelMonitor::utilslayer::InfoSpaceItem::type() const {
    return type_;
}

std::string
PixelMonitor::utilslayer::InfoSpaceItem::format() const {
    return format_;
}

PixelMonitor::utilslayer::InfoSpaceItem::UpdateType
PixelMonitor::utilslayer::InfoSpaceItem::updateType() const {
    return updateType_;
}

bool
PixelMonitor::utilslayer::InfoSpaceItem::isValid() const {
    return isValid_;
}

void
PixelMonitor::utilslayer::InfoSpaceItem::setValid() {
    isValid_ = true;
}

void
PixelMonitor::utilslayer::InfoSpaceItem::setInvalid() {
    isValid_ = false;
}

bool
PixelMonitor::utilslayer::
operator==(PixelMonitor::utilslayer::InfoSpaceItem const &item, std::string const &name) {
    return (item.name_ == name);
}

bool
PixelMonitor::utilslayer::
operator==(std::string const &name, PixelMonitor::utilslayer::InfoSpaceItem const &item) {
    return (item == name);
}
