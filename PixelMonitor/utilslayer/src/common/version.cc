#include "PixelMonitor/utilslayer/version.h"

#include "config/version.h"
#include "sentinel/utils/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
// #include "xdaq2rc/version.h"
#include "xdata/version.h"
#include "xgi/version.h"
#include "xoap/version.h"

#include "PixelMonitor/exception/version.h"
#include "PixelMonitor/hwlayer/version.h"

GETPACKAGEINFO(pixelmonitorutilslayer)

void
pixelmonitorutilslayer::checkPackageDependencies() {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(sentinelutils);
    CHECKDEPENDENCY(tcdsexception);
    CHECKDEPENDENCY(pixelmonitorhwlayer);
    CHECKDEPENDENCY(toolbox);
    CHECKDEPENDENCY(xcept);
    CHECKDEPENDENCY(xdaq);
    // CHECKDEPENDENCY(xdaq2rc);
    CHECKDEPENDENCY(xdata);
    CHECKDEPENDENCY(xgi);
    CHECKDEPENDENCY(xoap);
}

std::set<std::string, std::less<std::string> >
pixelmonitorutilslayer::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, sentinelutils);
    ADDDEPENDENCY(dependencies, tcdsexception);
    ADDDEPENDENCY(dependencies, pixelmonitorhwlayer);
    ADDDEPENDENCY(dependencies, toolbox);
    ADDDEPENDENCY(dependencies, xcept);
    ADDDEPENDENCY(dependencies, xdaq);
    // ADDDEPENDENCY(dependencies, xdaq2rc);
    ADDDEPENDENCY(dependencies, xdata);
    ADDDEPENDENCY(dependencies, xgi);
    ADDDEPENDENCY(dependencies, xoap);

    return dependencies;
}
