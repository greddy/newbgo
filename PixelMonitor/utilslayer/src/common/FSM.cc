#include "PixelMonitor/utilslayer/FSM.h"

#include <cassert>
#include <map>
#include <stdint.h>
#include <vector>

#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/fsm/exception/Exception.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/InvalidInputEvent.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xoap/MessageFactory.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/hwlayer/RegDumpConfigurationProcessor.h"
#include "PixelMonitor/utilslayer/ConfigurationInfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/LogMacros.h"
#include "PixelMonitor/utilslayer/SOAPUtils.h"
#include "PixelMonitor/utilslayer/XDAQAppWithFSMBase.h"

PixelMonitor::utilslayer::FSM::FSM(XDAQAppWithFSMBase *const xdaqApp)
    : fsmP_(0),
      xdaqAppP_(xdaqApp),
      logger_(xdaqAppP_->getApplicationLogger())
      // rcmsNotifier_(xdaqAppP_)
{
    // Find our connection to RCMS.
    // rcmsNotifier_.findRcmsStateListener();

    // Create the underlying Finite State Machine itself.
    std::string const commandLoopName =
        toolbox::toString("FSMCommandLoop_lid%d",
                          xdaqAppP_->getApplicationDescriptor()->getLocalId());
    fsmP_ = new toolbox::fsm::AsynchronousFiniteStateMachine(commandLoopName);

    // Setup the FSM with states and transitions.
    // NOTE: There is also the hard-coded 'F'/'Failed' state.
    fsmP_->addState('q', "ColdResetting", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('z', "Zeroing", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('c', "Configuring", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('C', "Configured", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('e', "Enabling", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('E', "Enabled", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('h', "Halting", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('H', "Halted", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('p', "Pausing", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('P', "Paused", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('r', "Resuming", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('s', "Stopping", this, &FSM::stateChangedWithNotification);

    // NOTE: We ran out of appropriate letters, so let's just use
    // numbers for some of the intermediate states.
    fsmP_->addState('0', "TTCResyncing", this, &FSM::stateChangedWithNotification);
    fsmP_->addState('1', "TTCHardResetting", this, &FSM::stateChangedWithNotification);

    // ColdReset: H -> H.
    fsmP_->addStateTransition('H', 'q', "ColdReset");
    fsmP_->addStateTransition('q', 'H', "ColdResettingDone", xdaqAppP_, &XDAQAppWithFSMBase::coldResetAction);

    // Configure: H -> C.
    fsmP_->addStateTransition('H', 'c', "Configure");
    fsmP_->addStateTransition('c', 'z', "ConfiguringDone", xdaqAppP_, &XDAQAppWithFSMBase::configureAction);
    fsmP_->addStateTransition('z', 'C', "ZeroingDone", xdaqAppP_, &XDAQAppWithFSMBase::zeroAction);

    // Reconfigure: C -> C.
    fsmP_->addStateTransition('C', 'c', "Reconfigure");

    // Configured to Configured: nothing happens c->C, C -> C.
    fsmP_->addStateTransition('c', 'C', "ConfigureConfigured");
    fsmP_->addStateTransition('C', 'C', "ConfiguredConfigured");

    // Enable: C -> E.
    fsmP_->addStateTransition('C', 'e', "Enable");
    fsmP_->addStateTransition('e', 'E', "EnablingDone", xdaqAppP_, &XDAQAppWithFSMBase::enableAction);

    // Pause: E -> P.
    fsmP_->addStateTransition('E', 'p', "Pause");
    fsmP_->addStateTransition('p', 'P', "PausingDone", xdaqAppP_, &XDAQAppWithFSMBase::pauseAction);

    // Resume: P -> E.
    fsmP_->addStateTransition('P', 'r', "Resume");
    fsmP_->addStateTransition('r', 'E', "ResumingDone", xdaqAppP_, &XDAQAppWithFSMBase::resumeAction);

    // Stop: E/P -> S.
    fsmP_->addStateTransition('E', 's', "Stop");
    fsmP_->addStateTransition('P', 's', "Stop");
    fsmP_->addStateTransition('s', 'z', "StoppingDone", xdaqAppP_, &XDAQAppWithFSMBase::stopAction);

    // Halt: C/E/F/H/P -> H.
    fsmP_->addStateTransition('C', 'h', "Halt");
    fsmP_->addStateTransition('E', 'h', "Halt");
    fsmP_->addStateTransition('F', 'h', "Halt");
    fsmP_->addStateTransition('H', 'h', "Halt");
    fsmP_->addStateTransition('P', 'h', "Halt");
    fsmP_->addStateTransition('h', 'H', "HaltingDone", xdaqAppP_, &XDAQAppWithFSMBase::haltAction);

    // Halted to Halted: nothing happens H -> H.
    fsmP_->addStateTransition('H', 'H', "HaltedHalted");

    // The following are two special transitions from Paused to Paused:
    // - TTCResync:    Paused -> TTCResyncing -> Paused.
    // - TTCHardReset: Paused -> TTCHardReseting -> Paused.
    fsmP_->addStateTransition('P', '0', "TTCResync");
    fsmP_->addStateTransition('0', 'P', "TTCResyncingDone", xdaqAppP_, &XDAQAppWithFSMBase::ttcResyncAction);
    fsmP_->addStateTransition('P', '1', "TTCHardReset");
    fsmP_->addStateTransition('1', 'P', "TTCHardResettingDone", xdaqAppP_, &XDAQAppWithFSMBase::ttcHardResetAction);

    // The following are special, for the special case of failure.
    // NOTE: This automatically creates an 'X' -> 'F' transition for all
    // states 'X'.
    fsmP_->setFailedStateTransitionAction(xdaqAppP_, &XDAQAppWithFSMBase::failAction);
    fsmP_->setFailedStateTransitionChanged(this, &FSM::stateChangedToFailedWithNotification);
    fsmP_->setStateName('F', "Failed");
    fsmP_->setInvalidInputStateTransitionAction(this, &FSM::invalidStateTransitionAction);

    // Start out with the FSM in its initial state: Halted.
    fsmP_->setInitialState('H');
    reset("Startup.");
}

PixelMonitor::utilslayer::FSM::~FSM() {
    if (fsmP_ != 0) {
        delete fsmP_;
        fsmP_ = 0;
    }
}

xoap::MessageReference
PixelMonitor::utilslayer::FSM::changeState(xoap::MessageReference msg) {
    std::string commandName = "undefined";
    std::string requestorId = "undefined";
    try {
        commandName = PixelMonitor::utilslayer::soap::extractSOAPCommandName(msg);
        requestorId = PixelMonitor::utilslayer::soap::extractSOAPCommandRequestorId(msg);
    }
    catch (PixelMonitor::exception::Exception &err) {
        // Somehow we don't understand the SOAP message. Log an error
        // and flag that we are having trouble.
        std::string const histMsg =
            toolbox::toString("Ignoring ununderstood SOAP command '%s' requested by '%s'.",
                              commandName.c_str(),
                              requestorId.c_str());
        xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
        std::string msgBase = "FSM SOAP command not understood";
        ERROR(toolbox::toString("%s: '%s'.",
                                msgBase.c_str(),
                                xcept::stdformat_exception_history(err).c_str()));
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::FSMTransitionProblem, top,
                             toolbox::toString("%s.", msgBase.c_str()), err);
        xdaqAppP_->notifyQualified("error", top);
        std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
        PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeSender;
        std::string faultString = toolbox::toString("%s.", msgBase.c_str());
        std::string faultDetail = toolbox::toString("%s: '%s'.",
                                                    msgBase.c_str(),
                                                    err.message().c_str());
        xoap::MessageReference reply =
            PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                               faultCode,
                                                               faultString,
                                                               faultDetail);
        return reply;
    }

    INFO(toolbox::toString("Received FSM transition command '%s' from '%s'.",
                           commandName.c_str(), requestorId.c_str()));

    //----------

    // Figure out if the current FSM command is allowed, based on our
    // (lack of) hardware lease.

    // First question: Is there a lease owner currently?
    if (xdaqAppP_->isHwLeased()) {
        // Second question: Who is the current lease owner?
        std::string const leaseOwnerId = xdaqAppP_->getHwLeaseOwnerId();
        // Third question: Does the current request come from our
        // hardware owner?
        if (requestorId != leaseOwnerId) {
            std::string const histMsg =
                toolbox::toString("Ignoring state transition command '%s' requested by '%s': "
                                  "the current hardware lease owner is '%s'.",
                                  commandName.c_str(),
                                  requestorId.c_str(),
                                  leaseOwnerId.c_str());
            xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
            // No match -> send error back.
            std::string msgBase =
                toolbox::toString("FSM command not allowed: "
                                  "hardware lease owned by session '%s'.",
                                  leaseOwnerId.c_str());
            ERROR(msgBase);
            XCEPT_DECLARE(PixelMonitor::exception::FSMTransitionProblem, top, msgBase);
            xdaqAppP_->notifyQualified("error", top);
            std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
            PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
            std::string faultString = msgBase;
            xoap::MessageReference reply =
                PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                                   faultCode,
                                                                   faultString);
            return reply;
        }
    } else {
        // No lease holder. In this case we can either 'ColdReset',
        // 'Configure' (the usual path), or 'Halt' (to recover from a
        // lost RunControl session). Any other request will result in an
        // error.
        if ((commandName != "ColdReset") &&
            (commandName != "Configure") &&
            (commandName != "Halt")) {
            std::string const histMsg =
                toolbox::toString("Ignoring state transition command '%s' requested by '%s': "
                                  "there is currently no hardware lease owner.",
                                  commandName.c_str(),
                                  requestorId.c_str());
            xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
            std::string msgBase =
                toolbox::toString("Without hardware lease holder "
                                  "only 'ColdReset', Configure', and 'Halt' "
                                  "FSM commands are allowed, not '%s'.",
                                  commandName.c_str());
            ERROR(msgBase);
            XCEPT_DECLARE(PixelMonitor::exception::FSMTransitionProblem, top, msgBase);
            xdaqAppP_->notifyQualified("error", top);
            std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
            PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
            std::string faultString = msgBase;
            xoap::MessageReference reply =
                PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                                   faultCode,
                                                                   faultString);
            return reply;
        }
    }

    //----------

    // Once we get here, let's see if the requested state transition is
    // a valid one for the current state.
    std::map<std::string, toolbox::fsm::State> allowedTransitions =
        fsmP_->getTransitions(fsmP_->getCurrentState());
    if (allowedTransitions.find(commandName) == allowedTransitions.end()) {
        std::string const histMsg =
            toolbox::toString("Ignoring invalid state transition '%s' from state '%s', "
                              "requested by '%s'.",
                              commandName.c_str(),
                              getCurrentStateName().c_str(),
                              requestorId.c_str());
        xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
        std::string msgBase =
            toolbox::toString("Invalid FSM state transition requested: "
                              "'%s' not allowed from '%s'.",
                              commandName.c_str(),
                              fsmP_->getStateName(fsmP_->getCurrentState()).c_str());
        ERROR(msgBase);
        XCEPT_DECLARE(PixelMonitor::exception::FSMTransitionProblem, top, msgBase);
        xdaqAppP_->notifyQualified("error", top);
        std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
        PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
        std::string faultString = msgBase;
        xoap::MessageReference reply =
            PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                               faultCode,
                                                               faultString);
        return reply;
    }

    //----------

    try {
        xdaqAppP_->loadSOAPCommandParameters(msg);
    }
    catch (xcept::Exception &err) {
        std::string const histMsg =
            toolbox::toString("Ignoring SOAP command '%s' requested by '%s': "
                              "failed to import the command parameters.",
                              commandName.c_str(),
                              requestorId.c_str());
        xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
        std::string msgBase =
            toolbox::toString("Problem importing the '%s' command parameters",
                              commandName.c_str());
        ERROR(toolbox::toString("%s: '%s'.",
                                msgBase.c_str(),
                                xcept::stdformat_exception_history(err).c_str()));
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::FSMTransitionProblem, top,
                             toolbox::toString("%s.", msgBase.c_str()), err);
        xdaqAppP_->notifyQualified("error", top);
        std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
        PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeSender;
        std::string faultString = toolbox::toString("%s.", msgBase.c_str());
        std::string faultDetail = toolbox::toString("%s.",
                                                    err.message().c_str());
        xoap::MessageReference reply =
            PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                               faultCode,
                                                               faultString,
                                                               faultDetail);
        return reply;
    }

    //----------

    // If we get here, all is good. Take the lease and then perform the
    // state transition.

    // But first mark the transition in the application history.
    std::string histMsg = toolbox::toString("Received '%s' SOAP command from '%s'",
                                            commandName.c_str(),
                                            requestorId.c_str());
    xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);

    xdaqAppP_->assignHwLease(requestorId);
    try {
        toolbox::Event::Reference event(new toolbox::Event(commandName, this));
        fsmP_->fireEvent(event);
    }
    catch (toolbox::fsm::exception::Exception &err) {
        std::string const histMsg =
            toolbox::toString("Ignoring SOAP command '%s' requested by '%s': "
                              "failed to execute the corresponding FSM transition.",
                              commandName.c_str(),
                              requestorId.c_str());
        xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
        std::string msgBase =
            toolbox::toString("Problem executing the FSM '%s' command",
                              commandName.c_str());
        ERROR(toolbox::toString("%s: %s.",
                                msgBase.c_str(),
                                xcept::stdformat_exception(err).c_str()));
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::FSMTransitionProblem, top,
                             toolbox::toString("%s.", msgBase.c_str()), err);
        xdaqAppP_->notifyQualified("error", top);
        std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
        PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeSender;
        std::string faultString = toolbox::toString("Failed to fire %s event.",
                                                    commandName.c_str());
        std::string faultDetail = toolbox::toString("%s: %s.",
                                                    msgBase.c_str(),
                                                    err.message().c_str());
        std::string faultActor = xdaqAppP_->getFullURL();
        xoap::MessageReference reply =
            PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                               faultCode,
                                                               faultString,
                                                               faultDetail);
        return reply;
    }

    //----------

    // Once we get here, the state transition has been triggered. Notify
    // the requestor that so far everything is fine. Any command 'X'
    // replies with 'XResponse'. Upon arrival in the next state (e.g.,
    // 'Configuring' for 'Configure') an asynchronous notification is
    // sent to RunControl with the name of the new state.
    std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
    try {
        xoap::MessageReference reply =
            PixelMonitor::utilslayer::soap::makeCommandSOAPReply(soapProtocolVersion,
                                                                 commandName,
                                                                 fsmP_->getStateName(fsmP_->getCurrentState()));
        return reply;
    }
    catch (xcept::Exception &err) {
        std::string msgBase =
            toolbox::toString("Failed to create FSM SOAP reply for command '%s'",
                              commandName.c_str());
        ERROR(toolbox::toString("%s: %s.",
                                msgBase.c_str(),
                                xcept::stdformat_exception(err).c_str()));
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top,
                             toolbox::toString("%s.", msgBase.c_str()), err);
        xdaqAppP_->notifyQualified("error", top);
        XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
    }

    //----------

    // Should only get here in case the above SOAP reply fails. In that
    // case: return an empty message.
    return xoap::createMessage();
}

void
PixelMonitor::utilslayer::FSM::stateChangedWithNotification(toolbox::fsm::FiniteStateMachine &fsm) {
    // INFO("State has changed. Current state is now '"
    //      << fsm.getStateName(fsm.getCurrentState())
    //      << "'. Notifying RCMS.");
    INFO("RunControl session in charge: '"
         << xdaqAppP_->appStateInfoSpace_.getString("hwLeaseOwnerId")
         << "'.");
    // wsi: runNumber is not meaningful for monitor
    // INFO("Run number: "
    //      << xdaqAppP_->cfgInfoSpaceP_->getUInt32("runNumber")
    //      << ".");

    std::string const stateName = getCurrentStateName();
    uint32_t const runNumber = xdaqAppP_->cfgInfoSpaceP_->getUInt32("runNumber");

    // Mark the transition in the application history.
    std::string histMsg = toolbox::toString("FSM state changed to '%s'",
                                            stateName.c_str());
    if (stateName == "Enabled") {
        histMsg.append(toolbox::toString(" (run #%d)", runNumber));
    }
    xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);

    xdaqAppP_->appStateInfoSpace_.setFSMState(stateName);
    // notifyRCMS(getCurrentStateName(), "Normal state change.");

    // If this is one of the intermediate '*ing' states: forward to the
    // final state.
    std::string stateNameEnding = stateName.substr(stateName.size() - 3);
    if (stateNameEnding == "ing") {
        std::string commandName = stateName + "Done";
        DEBUG("'"
              << stateName
              << "' is an intermediate state --> forwarding to '"
              << commandName
              << "'");

        try {
            toolbox::Event::Reference event(new toolbox::Event(commandName, this));
            fsmP_->fireEvent(event);
        }
        catch (toolbox::fsm::exception::Exception &err) {
            std::string msgBase =
                toolbox::toString("Problem executing the FSM '%s' command",
                                  commandName.c_str());
            ERROR(toolbox::toString("%s: %s.",
                                    msgBase.c_str(),
                                    xcept::stdformat_exception(err).c_str()));
            XCEPT_DECLARE_NESTED(PixelMonitor::exception::FSMTransitionProblem, top,
                                 toolbox::toString("%s.", msgBase.c_str()), err);
            xdaqAppP_->notifyQualified("error", top);
            XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
        }
    } else {
        DEBUG("'" << stateName << "' is not an intermediate state");
    }
}

void
PixelMonitor::utilslayer::FSM::stateChangedToFailedWithNotification(toolbox::fsm::FiniteStateMachine &fsm) {
    // Mark the transition in the application history.
    std::string const msg = toolbox::toString("FSM state changed to '%s'",
                                              getCurrentStateName().c_str());
    xdaqAppP_->appStateInfoSpace_.addHistoryItem(msg);

    ERROR("State has changed to 'Failed.'");

    // NOTE: Don't notify RCMS. This has already happened from
    // gotoFailed(), where more detailed failure information is
    // available.
}

std::string
PixelMonitor::utilslayer::FSM::getCurrentStateName() const {
    toolbox::fsm::State currentState = fsmP_->getCurrentState();
    std::string stateName = fsmP_->getStateName(currentState);
    return stateName;
}

void
PixelMonitor::utilslayer::FSM::gotoFailed(std::string const &reason) {
    xdaqAppP_->appStateInfoSpace_.setFSMState("Failed", reason);
    // ERROR("Going to 'Failed' state. Reason: '" << reason << "'. Notifying RCMS.");

    // Notify RCMS from here, since here we still have the detailed
    // failure information.
    std::string const className =
        xdaqAppP_->getApplicationDescriptor()->getClassName();
    std::string const serviceName =
        xdaqAppP_->getApplicationDescriptor()->getAttribute("service");
    // std::string const rcmsMsg =
    //   toolbox::toString("%s for service '%s' failed: '%s'. More detail at: %s.",
    //                     className.c_str(),
    //                     serviceName.c_str(),
    //                     reason.c_str(),
    //                     xdaqAppP_->getFullURL().c_str());
    // notifyRCMS(fsmP_->getStateName('F'), rcmsMsg);

    // Raise an exception so the FSM indeed goes to 'Failed.'
    XCEPT_RAISE(toolbox::fsm::exception::Exception, reason);
}

void
PixelMonitor::utilslayer::FSM::gotoFailed(xcept::Exception &err) {
    std::string reason = err.message();
    gotoFailed(reason);
}

void
PixelMonitor::utilslayer::FSM::invalidStateTransitionAction(toolbox::Event::Reference event) {
    // ASSERT ASSERT ASSERT
    assert(typeid(*event) == typeid(toolbox::fsm::InvalidInputEvent));
    // ASSERT ASSERT ASSERT end

    toolbox::fsm::InvalidInputEvent &invalidInputEvent =
        dynamic_cast<toolbox::fsm::InvalidInputEvent &>(*event);
    std::string const fromState = fsmP_->getStateName(invalidInputEvent.getFromState());
    std::string const toState = invalidInputEvent.getInput();
    std::string const histMsg =
        toolbox::toString("Ignoring invalid state transition '%s' from state '%s'.",
                          toState.c_str(),
                          fromState.c_str());
    xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
    std::string const msg =
        toolbox::toString("An invalid state transition has been invoked: "
                          "transition '%s' from state '%s'. I'm not deaf, "
                          "I'm just ignoring you.",
                          toState.c_str(), fromState.c_str());
    ERROR(msg);
}

// void
// PixelMonitor::utilslayer::FSM::notifyRCMS(std::string const& stateName, std::string const& msg)
// {
//   // Notify RCMS of a state change.
//   // NOTE: Should only be used for state _changes_.

//   try
//     {
//       rcmsNotifier_.stateChanged(stateName, msg);
//     }
//   catch(xcept::Exception& err)
//     {
//       ERROR("Failed to notify RCMS of state change: "
//             << xcept::stdformat_exception_history(err));
//       XCEPT_DECLARE_NESTED(PixelMonitor::exception::RCMSNotificationError, top,
//                            "Failed to notify RCMS of state change.", err);
//       xdaqAppP_->notifyQualified("error", top);
//     }
// }

void
PixelMonitor::utilslayer::FSM::reset(std::string const &msg) {
    fsmP_->reset();
    std::string stateName = fsmP_->getStateName(fsmP_->getCurrentState());
    xdaqAppP_->appStateInfoSpace_.setFSMState(stateName);
    // notifyRCMS(getCurrentStateName(), msg);
}
