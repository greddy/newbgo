#include "PixelMonitor/utilslayer/WebTableMultiCol.h"

#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>
#include <iterator>

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::utilslayer::WebTableMultiCol::WebTableMultiCol(std::string const &name,
                                                             std::string const &description,
                                                             Monitor const &monitor,
                                                             std::vector<std::string> const &itemSetNameVec,
                                                             std::string const &tabName,
                                                             size_t const colSpan)
    : WebObject(name, description, monitor, itemSetNameVec, tabName, colSpan),
      checked(false) {
    checked = checkItemSets();
    if (!checked) {
        std::string const msg = "None uniform itemSet found, cannot be put \
                             into a multi-column table!";
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    }
}

std::string
PixelMonitor::utilslayer::WebTableMultiCol::getHTMLString() const {
    // NOTE: This has been written to work with the new XDAQ12-style
    // HyperDAQ tabs and doT.js.

    std::stringstream res;

    res << "<div class=\"tcds-item-table-wrapper\">"
        << "\n";

    res << "<p class=\"tcds-item-table-title\">"
        << getName()
        << "</p>";
    res << "\n";

    std::string const desc = getDescription();
    if (!desc.empty()) {
        res << "<p class=\"tcds-item-table-description\">"
            << desc
            << "</p>";
        res << "\n";
    }

    res << "<table class=\"tcds-item-table xdaq-table pixelmonitor-table-compact\" \
          cellspacing=\"0\">"
        << "\n";

    //----------------------------------------------------------------------------
    res << getHTMLthead();
    res << getHTMLtbody();
    //----------------------------------------------------------------------------

    res << "</table>";
    res << "\n";

    res << "</div>";
    res << "\n";

    return res.str();
}

std::string
PixelMonitor::utilslayer::WebTableMultiCol::getHTMLthead() const {
    std::stringstream res;
    res << "<thead>\n";
    res << "  <tr>\n";

    Monitor::StringPairVector firstItem = monitor_.getItemSetDocStrings(itemSetNameVec_[0]);
    std::sort(firstItem.begin(), firstItem.end(), sortStringPairVector);
    res << "    <th><div>"
        << " Name "
        << "</div></th>";
    Monitor::StringPairVector::const_iterator fiIter;
    for (fiIter = firstItem.begin();
         fiIter != firstItem.end();
         ++fiIter) {
        res << "    <th";
        if (!fiIter->second.empty()) {
            res << " class=\"xdaq-tooltip\"";
        }
        res << "><div>" << fiIter->first << "</div>";
        if (!fiIter->second.empty()) {
            res << "<span class=\"xdaq-tooltip-msg\">" << fiIter->second << "</span>";
        }
        res << "</th>\n";
    }

    res << "  </tr>\n";
    res << "</thead>\n";

    return res.str();
}

std::string
PixelMonitor::utilslayer::WebTableMultiCol::getHTMLtbody() const {
    std::stringstream res;
    res << "<tbody>\n";

    std::vector<std::string>::const_iterator isNameiter;
    for (isNameiter = itemSetNameVec_.begin();
         isNameiter != itemSetNameVec_.end();
         ++isNameiter) {
        Monitor::StringPairVector items = monitor_.getItemSetDocStrings(*isNameiter);
        std::sort(items.begin(), items.end(), sortStringPairVector); // need to have the same sort method as thead.
        res << "  <tr>\n";
        res << "    <td><div class=\"firstColumn\">" << *isNameiter << "</div></td>\n"; // This is the first column
        Monitor::StringPairVector::const_iterator iter;
        for (iter = items.begin();
             iter != items.end();
             ++iter) {
            std::string const tmp = "['" + PixelMonitor::utilslayer::jsquote(*isNameiter) + "']['" + PixelMonitor::utilslayer::jsquote(iter->first) + "']";
            res << "    <td class=\"xdaq-tooltip\">"
                << "<span class=\"xdaq-tooltip-msg\">" << iter->first << "</span>"
                << "<script type=\"text/x-dot-template\">"
                << "{{=it" << tmp << "}}"
                << "</script>"
                << "<div class=\"target\"></div>"
                << "</td>\n";
        }
        res << "  </tr>\n";
    }

    res << "</tbody>\n";
    return res.str();
}

bool
PixelMonitor::utilslayer::WebTableMultiCol::checkItemSets() const {
    Monitor::StringPairVector firstItem = monitor_.getItemSetDocStrings(itemSetNameVec_[0]);
    std::vector<std::string> firstItemSets;
    Monitor::StringPairVector::const_iterator fiIter;
    for (fiIter = firstItem.begin();
         fiIter != firstItem.end();
         ++fiIter) {
        firstItemSets.push_back(fiIter->first);
    }
    std::sort(firstItemSets.begin(), firstItemSets.end());

    std::vector<std::string>::const_iterator isNameiter;
    for (isNameiter = itemSetNameVec_.begin();
         isNameiter != itemSetNameVec_.end();
         ++isNameiter) {
        Monitor::StringPairVector items = monitor_.getItemSetDocStrings(*isNameiter);
        std::vector<std::string> itemSets;
        Monitor::StringPairVector::iterator iter;
        for (iter = items.begin();
             iter != items.end();
             ++iter) {
            itemSets.push_back(iter->first);
        }
        std::sort(itemSets.begin(), itemSets.end());
        std::vector<std::string> v_diff;
        std::set_difference(firstItemSets.begin(), firstItemSets.end(),
                            itemSets.begin(), itemSets.end(),
                            std::back_inserter(v_diff));
        if (!v_diff.empty()) {
            return false;
        }
    }
    return true;
}

bool
PixelMonitor::utilslayer::sortStringPairVector(const std::pair<std::string, std::string> &p1,
                                               const std::pair<std::string, std::string> &p2) {
    return p1.first < p2.first;
}
