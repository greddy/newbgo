#include "PixelMonitor/utilslayer/WebTableNoLabels.h"

#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::utilslayer::WebTableNoLabels::WebTableNoLabels(std::string const &name,
                                                             std::string const &description,
                                                             Monitor const &monitor,
                                                             std::string const &itemSetName,
                                                             std::string const &tabName,
                                                             size_t const colSpan)
    : WebObject(name, description, monitor, itemSetName, tabName, colSpan) {
}

std::string
PixelMonitor::utilslayer::WebTableNoLabels::getHTMLString() const {
    // NOTE: This has been written to work with the new XDAQ12-style
    // HyperDAQ tabs and doT.js.

    std::stringstream res;

    res << "<div class=\"tcds-item-table-wrapper\">"
        << "\n";

    res << "<p class=\"tcds-item-table-title\">"
        << getName()
        << "</p>";
    res << "\n";

    std::string const desc = getDescription();
    if (!desc.empty()) {
        res << "<p class=\"tcds-item-table-description\">"
            << desc
            << "</p>";
        res << "\n";
    }

    // res << "<table class=\"tcds-item-table xdaq-table\">"
    //     << "\n";

    // res << "<tbody>";
    // res << "\n";

    // And now the actual table contents.
    // NOTE: The binding is specific to doT.js.
    Monitor::StringPairVector items = monitor_.getFormattedItemSet(itemSetName_);
    // Monitor::StringPairVector::const_iterator iter;
    // for (iter = items.begin(); iter != items.end(); ++iter)
    //   {
    //     std::string const tmp = "['" + itemSetName_ + "']['" + iter->first + "']";
    //     res << "<tr>"
    //         << "<td>"
    //         << "<script type=\"text/x-dot-template\">"
    //         << "{{=it" << tmp << "}}"
    //         << "</script>"
    //         // << "<div class=\"target\"></div>"
    //         << "<textarea readonly class=\"target\"></textarea>"
    //         << "</td>"
    //         << "</tr>"
    //         << "\n";
    //   }

    std::string const tmp = "['" + itemSetName_ + "']['" + items.at(0).first + "']";
    res << "<script type=\"text/x-dot-template\">"
        << "{{=it" << tmp << "}}"
        << "</script>"
        << "\n";
    res << "<textarea readonly class=\"target\"></textarea>"
        << "\n";

    // res << "</tbody>";
    // res << "\n";

    // res << "</table>";
    // res << "\n";

    res << "</div>";
    res << "\n";

    return res.str();
}
