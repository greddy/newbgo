#include "PixelMonitor/utilslayer/SOAPUtils.h"

#include <cassert>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/NamespaceURI.h"
#include "xdata/Boolean.h"
#include "xdata/soap/Serializer.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xoap/filter/MessageFilter.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPConstants.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPFault.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPPart.h"

#include "PixelMonitor/exception/Exception.h"

xoap::MessageReference
PixelMonitor::utilslayer::soap::makeParameterGetSOAPCmd(std::string const &className,
                                                        std::string const &parName,
                                                        std::string const &parType) {
    std::string const applicationNameSpace =
        toolbox::toString("urn:xdaq-application:%s", className.c_str());
    xoap::MessageReference msg = xoap::createMessage();
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration("xsi",
                                     "http://www.w3.org/2001/XMLSchema-instance");
    envelope.addNamespaceDeclaration("xsd",
                                     "http://www.w3.org/2001/XMLSchema");
    envelope.addNamespaceDeclaration("soapenc",
                                     "http://schemas.xmlsoap.org/soap/encoding/");
    xoap::SOAPBody body = envelope.getBody();
    xoap::SOAPName commandName = envelope.createName("ParameterGet",
                                                     "xdaq",
                                                     XDAQ_NS_URI);
    xoap::SOAPElement commandElement = body.addBodyElement(commandName);
    xoap::SOAPName propertiesName = envelope.createName("properties",
                                                        "p",
                                                        applicationNameSpace);
    xoap::SOAPElement propertiesElement = commandElement.addChildElement(propertiesName);
    xoap::SOAPName propertiesTypeName = envelope.createName("type",
                                                            "xsi",
                                                            "http://www.w3.org/2001/XMLSchema-instance");
    propertiesElement.addAttribute(propertiesTypeName, "soapenc:Struct");

    xoap::SOAPName propertyName = envelope.createName(parName, "p", applicationNameSpace);
    xoap::SOAPElement propertyElement = propertiesElement.addChildElement(propertyName);
    xoap::SOAPName propertyTypeName = envelope.createName("type",
                                                          "xsi",
                                                          "http://www.w3.org/2001/XMLSchema-instance");
    propertyElement.addAttribute(propertyTypeName, parType);
    return msg;
}

xoap::MessageReference
PixelMonitor::utilslayer::soap::makeSOAPFaultReply(xoap::MessageReference const &msg,
                                                   SOAPFaultCodeType const faultCode,
                                                   std::string const &faultString,
                                                   std::string const &faultDetail) {
    std::string const soapProtocolVersion =
        msg->getImplementationFactory()->getProtocolVersion();
    return makeSOAPFaultReply(soapProtocolVersion, faultCode, faultString, faultDetail);
}

xoap::MessageReference
PixelMonitor::utilslayer::soap::makeCommandSOAPReply(xoap::MessageReference const &msg,
                                                     std::string const &fsmState) {
    std::string const soapProtocolVersion =
        msg->getImplementationFactory()->getProtocolVersion();
    std::string const command = extractSOAPCommandName(msg);
    return makeCommandSOAPReply(soapProtocolVersion, command, fsmState);
}

xoap::MessageReference
PixelMonitor::utilslayer::soap::makeCommandSOAPReply(xoap::MessageReference const &msg,
                                                     std::string const &replyMsg,
                                                     std::string const &fsmState) {
    std::string const soapProtocolVersion =
        msg->getImplementationFactory()->getProtocolVersion();
    std::string const command = extractSOAPCommandName(msg);
    return makeCommandSOAPReply(soapProtocolVersion, command, replyMsg, fsmState);
}

xoap::MessageReference
PixelMonitor::utilslayer::soap::makeSOAPFaultReply(std::string const &soapProtocolVersion,
                                                   SOAPFaultCodeType const faultCode,
                                                   std::string const &faultString,
                                                   std::string const &faultDetail) {
    xoap::MessageFactory *messageFactory = xoap::MessageFactory::getInstance(soapProtocolVersion);
    xoap::MessageReference reply = messageFactory->createMessage();
    xoap::SOAPBody body = reply->getSOAPPart().getEnvelope().getBody();
    xoap::SOAPFault fault = body.addFault();
    if (messageFactory->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL) {
        switch (faultCode) {
        case SOAPFaultCodeSender:
            fault.setFaultCode("Client");
            break;
        case SOAPFaultCodeReceiver:
            fault.setFaultCode("Server");
            break;
        default:
            // ASSERT ASSERT ASSERT
            // Should never get here.
            assert(false);
            // ASSERT ASSERT ASSERT end
            break;
        }
        fault.setFaultString(faultString);
    } else if (messageFactory->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_2_PROTOCOL) {
        std::string faultCodeString = "";
        switch (faultCode) {
        case SOAPFaultCodeSender:
            faultCodeString = "Sender";
            break;
        case SOAPFaultCodeReceiver:
            faultCodeString = "Receiver";
            break;
        default:
            // ASSERT ASSERT ASSERT
            // Should never get here.
            assert(false);
            // ASSERT ASSERT ASSERT end
            break;
        }
        xoap::SOAPName faultCodeQName(faultCodeString,
                                      messageFactory->getEnvelopePrefix(),
                                      xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
        fault.setFaultCode(faultCodeQName);
        fault.addFaultReasonText(faultString, std::locale("en_US"));
    } else {
        // ASSERT ASSERT ASSERT
        // Should never get here.
        assert(false);
        // ASSERT ASSERT ASSERT end
    }

    if (faultDetail != "") {
        xoap::SOAPElement detail = fault.addDetail();
        detail.addTextNode(faultDetail);
    }

    return reply;
}

xoap::MessageReference
PixelMonitor::utilslayer::soap::makeCommandSOAPReply(std::string const &soapProtocolVersion,
                                                     std::string const &command,
                                                     std::string const &fsmState) {
    xoap::MessageFactory *messageFactory = xoap::MessageFactory::getInstance(soapProtocolVersion);
    xoap::MessageReference reply = messageFactory->createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPBody body = envelope.getBody();
    std::string replyString = command + "Response";
    xoap::SOAPName replyName = envelope.createName(replyString, "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement replyElement = body.addBodyElement(replyName);

    if (fsmState != "") {
        xoap::SOAPName elementName = envelope.createName("state", "xdaq", XDAQ_NS_URI);
        xoap::SOAPElement stateElement = replyElement.addChildElement(elementName);
        xoap::SOAPName attributeName = envelope.createName("stateName", "xdaq", XDAQ_NS_URI);
        stateElement.addAttribute(attributeName, fsmState);
    }

    return reply;
}

xoap::MessageReference
PixelMonitor::utilslayer::soap::makeCommandSOAPReply(std::string const &soapProtocolVersion,
                                                     std::string const &command,
                                                     std::string const &replyMsg,
                                                     std::string const &fsmState) {

    xoap::MessageReference reply =
        PixelMonitor::utilslayer::soap::makeCommandSOAPReply(soapProtocolVersion, command, fsmState);

    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName elementName = envelope.createName("contents", "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement replyElement = extractBodyNode(reply);
    xoap::SOAPElement msgElement = replyElement.addChildElement(elementName);
    msgElement.addTextNode(replyMsg);

    return reply;
}

std::string
PixelMonitor::utilslayer::soap::extractSOAPCommandName(xoap::MessageReference const &msg) {
    // The body should contain a single node with the name of the
    // command to execute.
    xoap::SOAPElement commandNode = extractBodyNode(msg);
    xoap::SOAPName commandSOAPName = commandNode.getElementName();
    std::string const commandName = commandSOAPName.getLocalName();

    return commandName;
}

std::string
PixelMonitor::utilslayer::soap::extractSOAPCommandRequestorId(xoap::MessageReference const &msg) {
    // The body should contain a single node with the name of the
    // command to execute. The originator should identify itself using
    // the 'xdaq:actionRequestorId' parameter.
    xoap::SOAPElement commandNode = extractBodyNode(msg);
    xoap::SOAPName requestorIdSOAPName("actionRequestorId", "xdaq", "");
    std::string requestorId = commandNode.getAttributeValue(requestorIdSOAPName);
    if (requestorId == "") {
        XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem,
                    "Expected an (non-empty) attribute 'xdaq:actionRequestorId' "
                    "in SOAP command message but could not find it.");
    }

    return requestorId;
}

/*
std::string
PixelMonitor::utilslayer::soap::extractSOAPCommandRCMSURL(xoap::MessageReference const& msg)
{
  // The body should contain a single node with the name of the
  // command to execute. The originator can include a new value of the
  // URL of where to send RCMS state change notifications using the
  // 'xdaq:rcmsURL' parameter.
  xoap::SOAPElement commandNode = extractBodyNode(msg);
  xoap::SOAPName rcmsURLSOAPName("rcmsURL", "xdaq", "");
  std::string rcmsURL = commandNode.getAttributeValue(rcmsURLSOAPName);
  if (rcmsURL == "")
    {
      XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem,
                  "Expected an (non-empty) attribute 'xdaq:rcmsURL' "
                  "in SOAP command message but could not find it.");
    }
  return rcmsURL;
}
*/
bool
PixelMonitor::utilslayer::soap::hasFault(xoap::MessageReference const &msg) {
    return msg->getSOAPPart().getEnvelope().getBody().hasFault();
}

bool
PixelMonitor::utilslayer::soap::hasFaultDetail(xoap::MessageReference const &msg) {
    bool res = false;
    if (PixelMonitor::utilslayer::soap::hasFault(msg)) {
        res = msg->getSOAPPart().getEnvelope().getBody().getFault().hasDetail();
    }
    return res;
}

std::string
PixelMonitor::utilslayer::soap::extractFaultString(xoap::MessageReference const &msg) {
    std::string res("");
    if (PixelMonitor::utilslayer::soap::hasFault(msg)) {
        xoap::SOAPFault fault = msg->getSOAPPart().getEnvelope().getBody().getFault();
        res = fault.getFaultString();
    }
    return res;
}

std::string
PixelMonitor::utilslayer::soap::extractFaultDetail(xoap::MessageReference const &msg) {
    std::string res("");
    if (PixelMonitor::utilslayer::soap::hasFaultDetail(msg)) {
        xoap::SOAPFault fault = msg->getSOAPPart().getEnvelope().getBody().getFault();
        res = fault.getDetail().getTextContent();
    }
    return res;
}

bool
PixelMonitor::utilslayer::soap::hasSOAPCommandParameter(xoap::MessageReference const &msg,
                                                        std::string const &parameterName) {
    bool res = false;
    try {
        extractSOAPCommandParameterElement(msg, parameterName);
        res = true;
    }
    catch (PixelMonitor::exception::SOAPFormatProblem) {
        // Nothing to be done.
    }
    return res;
}

bool
PixelMonitor::utilslayer::soap::extractSOAPCommandParameterBool(xoap::MessageReference const &msg,
                                                                std::string const &parameterName) {
    xdata::soap::Serializer serializer;
    xdata::Boolean tmpVal;
    xoap::SOAPElement element = extractSOAPCommandParameterElement(msg, parameterName);
    try {
        serializer.import(&tmpVal, element.getDOM());
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
                              parameterName.c_str(),
                              err.message().c_str());
        XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem, msg);
    }
    return bool(tmpVal);
}

std::string
PixelMonitor::utilslayer::soap::extractSOAPCommandParameterString(xoap::MessageReference const &msg,
                                                                  std::string const &parameterName) {
    xdata::soap::Serializer serializer;
    xdata::String tmpVal;
    xoap::SOAPElement element = extractSOAPCommandParameterElement(msg, parameterName);
    try {
        serializer.import(&tmpVal, element.getDOM());
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
                              parameterName.c_str(),
                              err.message().c_str());
        XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem, msg);
    }
    return std::string(tmpVal);
}

uint32_t
PixelMonitor::utilslayer::soap::extractSOAPCommandParameterUnsignedInteger(xoap::MessageReference const &msg,
                                                                           std::string const &parameterName) {
    xdata::soap::Serializer serializer;
    xdata::UnsignedInteger32 tmpVal;
    xoap::SOAPElement element = extractSOAPCommandParameterElement(msg, parameterName);
    try {
        serializer.import(&tmpVal, element.getDOM());
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
                              parameterName.c_str(),
                              err.message().c_str());
        XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem, msg);
    }
    return uint32_t(tmpVal);
}

xoap::MessageReference
PixelMonitor::utilslayer::soap::makePSXDPGetSOAPCmd(std::string const &dpName) {
    std::string const PSX_NS_URI = "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd";

    xoap::MessageReference msg = xoap::createMessage();
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration("xsi",
                                     "http://www.w3.org/2001/XMLSchema-instance");
    envelope.addNamespaceDeclaration("xsd",
                                     "http://www.w3.org/2001/XMLSchema");
    envelope.addNamespaceDeclaration("soapenc",
                                     "http://schemas.xmlsoap.org/soap/encoding/");
    xoap::SOAPBody body = envelope.getBody();
    xoap::SOAPName commandName = envelope.createName("dpGet",
                                                     "psx",
                                                     PSX_NS_URI);
    xoap::SOAPElement commandElement = body.addBodyElement(commandName);

    // The main datapoint.
    xoap::SOAPName dataPointName = envelope.createName("dp",
                                                       "psx",
                                                       PSX_NS_URI);
    xoap::SOAPElement dataPointElement = commandElement.addChildElement(dataPointName);
    xoap::SOAPName nameAttribute = envelope.createName("name",
                                                       "",
                                                       "");
    dataPointElement.addAttribute(nameAttribute, dpName + ":_online.._value");

    // The (first two) user-bits encoding the DIP quality.
    // - User-bit 1.
    xoap::SOAPName dataPointUserBit1Name = envelope.createName("dp",
                                                               "psx",
                                                               PSX_NS_URI);
    xoap::SOAPElement dataPointUserBit1Element = commandElement.addChildElement(dataPointUserBit1Name);
    xoap::SOAPName nameUserBit1Attribute = envelope.createName("name",
                                                               "",
                                                               "");
    dataPointUserBit1Element.addAttribute(nameUserBit1Attribute, dpName + ":_online.._userbit1");
    // - User-bit 2.
    xoap::SOAPName dataPointUserBit2Name = envelope.createName("dp",
                                                               "psx",
                                                               PSX_NS_URI);
    xoap::SOAPElement dataPointUserBit2Element = commandElement.addChildElement(dataPointUserBit2Name);
    xoap::SOAPName nameUserBit2Attribute = envelope.createName("name",
                                                               "",
                                                               "");
    dataPointUserBit2Element.addAttribute(nameUserBit2Attribute, dpName + ":_online.._userbit2");

    // The 'invalid' bit from PVSS.
    xoap::SOAPName dataPointInvalidFlagName = envelope.createName("dp",
                                                                  "psx",
                                                                  PSX_NS_URI);
    xoap::SOAPElement dataPointInvalidFlagElement = commandElement.addChildElement(dataPointInvalidFlagName);
    xoap::SOAPName nameInvalidFlagAttribute = envelope.createName("name",
                                                                  "",
                                                                  "");
    dataPointInvalidFlagElement.addAttribute(nameInvalidFlagAttribute, dpName + ":_online.._invalid");

    return msg;
}

std::string
PixelMonitor::utilslayer::soap::extractPSXDPGetReply(xoap::MessageReference &msg,
                                                     std::string const &dpName) {
    // BUG BUG BUG
    msg->writeTo(std::cout);
    // BUG BUG BUG end

    xoap::filter::MessageFilter filter("//psx:dp[@name='" + dpName + "']");
    if (!filter.match(msg)) {
        std::string const err =
            toolbox::toString("Could not find parameter '%s' in PSX dpGet SOAP reply.",
                              dpName.c_str());
        XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem, err);
    }
    std::list<xoap::SOAPElement> elements = filter.extract(msg);
    if (elements.size() == 0) {
        std::string const err =
            toolbox::toString("Could not find parameter '%s' in PSX dpGet SOAP reply.",
                              dpName.c_str());
        XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem, err);
    } else if (elements.size() > 1) {
        std::string const err =
            toolbox::toString("Found %d replies (instead of 1) for parameter '%s' in PSX dpGet SOAP reply.",
                              elements.size(),
                              dpName.c_str());
        XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem, err);
    }

    std::string const dpVal = elements.front().getTextContent();
    return dpVal;
}

xoap::SOAPElement
PixelMonitor::utilslayer::soap::extractSOAPCommandParameterElement(xoap::MessageReference const &msg,
                                                                   std::string const &parameterName) {
    // The body should contain a single node with the name of the
    // command to execute, with (sometimes optional) parameters.
    xoap::SOAPElement commandNode = extractBodyNode(msg);
    xoap::SOAPName soapName(parameterName, "xdaq", "");
    xoap::SOAPElement element = extractChildNode(commandNode, soapName);
    return element;
}

void
PixelMonitor::utilslayer::soap::addSOAPReplyParameter(xoap::MessageReference const &msg,
                                                      std::string const &parameterName,
                                                      bool const val) {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPName elementName = envelope.createName(parameterName,
                                                     "xdaq",
                                                     XDAQ_NS_URI);
    xoap::SOAPElement replyNode = extractBodyNode(msg);
    xoap::SOAPElement element = replyNode.addChildElement(elementName);

    xdata::soap::Serializer serializer;
    xdata::Boolean tmpVal(val);
    try {
        serializer.exportAll(&tmpVal, dynamic_cast<DOMElement *>(element.getDOM()), true);
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to export SOAP command parameter 'xdaq:%s': '%s'.",
                              parameterName.c_str(),
                              err.message().c_str());
        XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg);
    }
}

void
PixelMonitor::utilslayer::soap::addSOAPReplyParameter(xoap::MessageReference const &msg,
                                                      std::string const &parameterName,
                                                      std::string const &val) {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPName elementName = envelope.createName(parameterName,
                                                     "xdaq",
                                                     XDAQ_NS_URI);
    xoap::SOAPElement replyNode = extractBodyNode(msg);
    xoap::SOAPElement element = replyNode.addChildElement(elementName);

    xdata::soap::Serializer serializer;
    xdata::String tmpVal(val);
    try {
        serializer.exportAll(&tmpVal, dynamic_cast<DOMElement *>(element.getDOM()), true);
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to export SOAP command parameter 'xdaq:%s': '%s'.",
                              parameterName.c_str(),
                              err.message().c_str());
        XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg);
    }
}

void
PixelMonitor::utilslayer::soap::addSOAPReplyParameter(xoap::MessageReference const &msg,
                                                      std::string const &parameterName,
                                                      uint32_t const val) {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPName elementName = envelope.createName(parameterName,
                                                     "xdaq",
                                                     XDAQ_NS_URI);
    xoap::SOAPElement replyNode = extractBodyNode(msg);
    xoap::SOAPElement element = replyNode.addChildElement(elementName);

    xdata::soap::Serializer serializer;
    xdata::UnsignedInteger32 tmpVal(val);
    try {
        serializer.exportAll(&tmpVal, dynamic_cast<DOMElement *>(element.getDOM()), true);
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to export SOAP command parameter 'xdaq:%s': '%s'.",
                              parameterName.c_str(),
                              err.message().c_str());
        XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg);
    }
}

xoap::SOAPElement
PixelMonitor::utilslayer::soap::extractBodyNode(xoap::MessageReference const &msg) {
    std::vector<xoap::SOAPElement> bodyList = extractBodyNodes(msg);
    if (bodyList.size() != 1) {
        XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem,
                    toolbox::toString("Expected exactly one element "
                                      "in the body of the SOAP command message "
                                      "but found %d.",
                                      bodyList.size()));
    }
    return bodyList.at(0);
}

std::vector<xoap::SOAPElement>
PixelMonitor::utilslayer::soap::extractBodyNodes(xoap::MessageReference const &msg) {
    std::vector<xoap::SOAPElement> bodyList =
        msg->getSOAPPart().getEnvelope().getBody().getChildElements();
    std::vector<xoap::SOAPElement> res;
    // Remove 'empty' text nodes (e.g., line-breaks, etc.).
    for (std::vector<xoap::SOAPElement>::iterator i = bodyList.begin();
         i != bodyList.end();
         ++i) {
        if (i->getElementName().getQualifiedName() != "#text") {
            res.push_back(*i);
        }
    }
    return res;
}

xoap::SOAPElement
PixelMonitor::utilslayer::soap::extractChildNode(xoap::SOAPElement &node,
                                                 xoap::SOAPName &childName) {
    std::vector<xoap::SOAPElement> children = node.getChildElements(childName);
    if (children.size() != 1) {
        XCEPT_RAISE(PixelMonitor::exception::SOAPFormatProblem,
                    toolbox::toString("Expected exactly one child element "
                                      "with name '%s' "
                                      "but found %d.",
                                      childName.getQualifiedName().c_str(),
                                      children.size()));
    }
    return children.at(0);
}
