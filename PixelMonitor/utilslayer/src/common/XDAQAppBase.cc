#include "PixelMonitor/utilslayer/XDAQAppBase.h"

#include <algorithm>
#include <cassert>
#include <cerrno>
#include <cstring>
#include <ios>
#include <list>
#include <set>
#include <stdint.h>
#include <vector>

#include "log4cplus/logger.h"

#include "sentinel/utils/Alarm.h"
#include "toolbox/string.h"
#include "toolbox/task/exception/Exception.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq2rc/ClassnameAndInstance.h"
#include "xdata/Bag.h"
#include "xdata/InfoSpace.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/soap/Serializer.h"
#include "xdata/String.h"
#include "xoap/MessageFactory.h"
#include "xoap/Method.h"
#include "xoap/SOAPElement.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/hwlayer/ConfigurationProcessor.h"
#include "PixelMonitor/hwlayer/HwDeviceTCA.h"
#include "PixelMonitor/hwlayer/RegDumpConfigurationProcessor.h"
#include "PixelMonitor/utilslayer/ConfigurationInfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/Lock.h"
#include "PixelMonitor/utilslayer/LockGuard.h"
#include "PixelMonitor/utilslayer/LogMacros.h"
#include "PixelMonitor/utilslayer/SOAPUtils.h"
#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::utilslayer::XDAQAppBase::XDAQAppBase(xdaq::ApplicationStub *const stub,
                                                   std::unique_ptr<PixelMonitor::hwlayer::HwDeviceTCA> hw)
    : xdaq::Application(stub),
      appStateInfoSpace_(this, &appStateInfoSpaceUpdater_),
      cfgInfoSpaceP_(0),
      hwP_(hw),
      logger_(this->getApplicationLogger()),
      monitor_(this),
      webServer_(this),
      hwLeaseOwnerId_(""),
      monitoringLock_(toolbox::BSem::FULL, false),
      timerName_(""),
      timerJobName_("HwLeaseExpiryTimerJob"),
      hwLeaseExpiryTimerP_(0) {
    std::string const iconPath = buildIconPathName();
#ifdef XDAQ13
    getApplicationDescriptor()->setAttribute("icon", iconPath);
#else
    editApplicationDescriptor()->setAttribute("icon", iconPath);
#endif

    // Binding of the ParameterSet SOAP method. The TCDS control
    // applications do not allow direct setting of parameters.
    xoap::bind<XDAQAppBase>(this, &XDAQAppBase::parameterSet, "ParameterSet", XDAQ_NS_URI);

    // Bind the hardware lease renewal.
    xoap::bind<XDAQAppBase>(this, &XDAQAppBase::RenewHwLease, "RenewHardwareLease", XDAQ_NS_URI);

    // Create the auto-expiry timer for the hardware lease.
    timerName_ = toolbox::toString("HwLeaseExpiryTimer_lid%d",
                                   getApplicationDescriptor()->getLocalId());
    hwLeaseExpiryTimerP_ = toolbox::task::getTimerFactory()->createTimer(timerName_);

    // Register to be notified when the XDAQ framework loads the
    // configuration values from the XML file.
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

    appStateInfoSpace_.addHistoryItem("Application started");

    revokeHwLease();
}

PixelMonitor::utilslayer::XDAQAppBase::~XDAQAppBase() {
    revokeHwLease();
    if (hwLeaseExpiryTimerP_) {
        toolbox::task::getTimerFactory()->createTimer(timerName_);
        hwLeaseExpiryTimerP_ = 0;
    }
}

void
PixelMonitor::utilslayer::XDAQAppBase::raiseAlarm(std::string const &name,
                                                  std::string const &severity,
                                                  xcept::Exception &err) {
    // Raise the specified alarm (if it's not already raised).
    std::string const alarmsInfoSpaceName = "urn:xdaq-sentinel:alarms";

    xdata::InfoSpace *alarmInfoSpace =
        xdata::getInfoSpaceFactory()->get(alarmsInfoSpaceName);

    if (!alarmInfoSpace->hasItem(name)) {
        sentinel::utils::Alarm *alarm = new sentinel::utils::Alarm(severity, err, this);
        alarmInfoSpace->fireItemAvailable(name, alarm);

        //----------

        // Mark the raising of the alarm in the history record.
        std::string const histMsg = "Raising '" + name + "' alarm";
        appStateInfoSpace_.addHistoryItem(histMsg);
    }

    //----------

    // Mark the problem in the application state as well.
    appStateInfoSpace_.addProblem(name, err.what());
}

void
PixelMonitor::utilslayer::XDAQAppBase::revokeAlarm(std::string const &name) {
    // Revoke the specified alarm (if it exists).
    std::string const alarmsInfoSpaceName = "urn:xdaq-sentinel:alarms";

    xdata::InfoSpace *alarmInfoSpace = xdata::getInfoSpaceFactory()->get(alarmsInfoSpaceName);

    if (alarmInfoSpace->hasItem(name)) {
        sentinel::utils::Alarm *const alarm =
            dynamic_cast<sentinel::utils::Alarm *>(alarmInfoSpace->find(name));
        alarmInfoSpace->fireItemRevoked(name, this);
        delete alarm;

        //----------

        // Mark the revoking of the alarm in the history record.
        std::string const histMsg = "Revoking '" + name + "' alarm";
        appStateInfoSpace_.addHistoryItem(histMsg);
    }

    //----------

    // Remove the problem from the  application state as well.
    appStateInfoSpace_.removeProblem(name);
}

xoap::MessageReference
PixelMonitor::utilslayer::XDAQAppBase::executeSOAPCommand(xoap::MessageReference &cmd,
                                                          xdaq::ApplicationDescriptor &dest) {
    try {
        xoap::MessageReference reply = sendSOAP(cmd, dest);
        if (PixelMonitor::utilslayer::soap::hasFault(reply)) {
            std::string msg("");
            std::string const faultString =
                PixelMonitor::utilslayer::soap::extractFaultString(reply);
            if (PixelMonitor::utilslayer::soap::hasFaultDetail(reply)) {
                std::string const faultDetail =
                    PixelMonitor::utilslayer::soap::extractFaultDetail(reply);
                msg =
                    toolbox::toString("Received a SOAP fault as reply: '%s: %s'.",
                                      faultString.c_str(),
                                      faultDetail.c_str());
            } else {
                msg =
                    toolbox::toString("Received a SOAP fault as reply: '%s'.",
                                      faultString.c_str());
            }
            XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg);
        } else {
            return reply;
        }
    }
    catch (xcept::Exception &err) {
        std::string const msg = toolbox::toString("Problem executing SOAP command: '%s'.",
                                                  err.what());
        XCEPT_RETHROW(PixelMonitor::exception::RuntimeProblem,
                      msg,
                      err);
    }
}

xoap::MessageReference
PixelMonitor::utilslayer::XDAQAppBase::sendSOAP(xoap::MessageReference &msg,
                                                xdaq::ApplicationDescriptor &dest) {
    xoap::MessageReference const reply = postSOAP(msg, dest);
    return reply;
}

xoap::MessageReference
PixelMonitor::utilslayer::XDAQAppBase::postSOAP(xoap::MessageReference &msg,
                                                xdaq::ApplicationDescriptor &destination) {
    xoap::MessageReference const tmp =
#ifdef XDAQ13
        const_cast<PixelMonitor::utilslayer::XDAQAppBase *>(this)->getApplicationContext()->postSOAP(msg,
                                                                                                     *const_cast<PixelMonitor::utilslayer::XDAQAppBase *>(this)->getApplicationDescriptor(),
                                                                                                     const_cast<xdaq::ApplicationDescriptor &>(destination));
#else
        getApplicationContext()->postSOAP(msg,
                                          *getApplicationDescriptor(),
                                          destination);
#endif

    // NOTE: The following is necessary to make things work with replies
    // from XDAQ applications in the same executive as the receiving
    // application.
    // Details: https://svnweb.cern.ch/trac/cmsos/ticket/3255
    xoap::MessageReference const reply = xoap::createMessage(tmp);
    return reply;
}

void
PixelMonitor::utilslayer::XDAQAppBase::setupInfoSpaces() {
    if (cfgInfoSpaceP_.get() != 0) {
        cfgInfoSpaceP_->readInfoSpace();
    }
}

void
PixelMonitor::utilslayer::XDAQAppBase::actionPerformed(xdata::Event &event) {
    // This is called after all default configuration values have been
    // loaded (from the XDAQ configuration file).
    if (event.type() == "urn:xdaq-event:setDefaultValues") {
        // Make sure the configuration settings are up-to-date.
        cfgInfoSpaceP_->readInfoSpace();

        // Fake an update in order to trigger the necessary update
        // callbacks. Among other things, these callbacks trigger XMAS
        // flashlist updates, etc.
        cfgInfoSpaceP_->writeInfoSpace();

        setupInfoSpaces();
        monitor_.startMonitoring();
    }
}

std::string
PixelMonitor::utilslayer::XDAQAppBase::getFullURL() {
    std::string url = getApplicationDescriptor()->getContextDescriptor()->getURL();
    std::string urn = getApplicationDescriptor()->getURN();
    std::string fullURL = toolbox::toString("%s/%s", url.c_str(), urn.c_str());
    return fullURL;
}

bool
PixelMonitor::utilslayer::XDAQAppBase::isHwLeased() const {
    return (hwLeaseOwnerId_ != "");
}

std::string
PixelMonitor::utilslayer::XDAQAppBase::getHwLeaseOwnerId() const {
    return hwLeaseOwnerId_;
}

void
PixelMonitor::utilslayer::XDAQAppBase::assignHwLease(std::string const &leaseOwnerId) {
    // First figure out how long the lease is supposed to last.
    toolbox::TimeInterval leaseDuration = toolbox::TimeVal(10.);
    std::string const tmp = cfgInfoSpaceP_->getString("hardwareLeaseDuration");
    try {
        leaseDuration.fromString(tmp);
    }
    catch (toolbox::exception::Exception &err) {
        std::string const state = "Failed to interpret .";
        std::string const msg =
            toolbox::toString("Failed to interpret '%s' as hardware lease duration. "
                              "Using 10 seconds instead.",
                              tmp.c_str());
        ERROR(msg);
        notifyQualified("error", err);
    }
    ///toolbox::TimeVal endOfLeaseTime =
    ///  toolbox::TimeVal::gettimeofday() + leaseDuration;

    // Check if we are assigning or renewing the hardware lease.
    bool const wasAlreadyLeased = isHwLeased();

    hwLeaseOwnerId_ = leaseOwnerId;
    appStateInfoSpace_.setString("hwLeaseOwnerId", hwLeaseOwnerId_, true);

    // Start the auto-expiry timer.
    /** COMMENT OUT FOR NO HWLEASE REQUIRED
  try
    {
      // NOTE: This should not be necessary, but it does not hurt
      // either.
      hwLeaseExpiryTimerP_->remove(timerJobName_);
    }
  catch (toolbox::task::exception::Exception err)
    {
      // No problem.
    }
  hwLeaseExpiryTimerP_->schedule(this, endOfLeaseTime, 0, timerJobName_);
  **/

    std::string msgBase = "";
    if (wasAlreadyLeased) {
        msgBase = "Renewing hardware lease";
    } else {
        msgBase = "Assigning hardware lease";
    }
    std::string const histMsg =
        toolbox::toString("%s to '%s'", msgBase.c_str(), leaseOwnerId.c_str());
    appStateInfoSpace_.addHistoryItem(histMsg);
}

void
PixelMonitor::utilslayer::XDAQAppBase::renewHwLease() {
    assignHwLease(hwLeaseOwnerId_);
}

void
PixelMonitor::utilslayer::XDAQAppBase::revokeHwLease() {
    // Keep track of what happened.
    std::string const leaseOwnerId = appStateInfoSpace_.getString("hwLeaseOwnerId");
    if (leaseOwnerId != "") {
        std::string const histMsg = toolbox::toString("Releasing hardware lease of '%s'",
                                                      leaseOwnerId.c_str());
        appStateInfoSpace_.addHistoryItem(histMsg);
    }

    if (hwLeaseExpiryTimerP_) {
        try {
            hwLeaseExpiryTimerP_->remove(timerJobName_);
        }
        catch (toolbox::task::exception::Exception err) {
            // This should not happen here, but okay...
        }
    }

    hwLeaseOwnerId_ = "";
    appStateInfoSpace_.setString("hwLeaseOwnerId", hwLeaseOwnerId_, true);
}

void
PixelMonitor::utilslayer::XDAQAppBase::hwConnect() {
    if (!getHw().isHwConnected()) {
        hwConnectImpl();
    }
}

void
PixelMonitor::utilslayer::XDAQAppBase::hwRelease() {
    hwReleaseImpl();
}

std::string
PixelMonitor::utilslayer::XDAQAppBase::readHardwareConfiguration() {
    return readHardwareConfigurationImpl();
}

std::string
PixelMonitor::utilslayer::XDAQAppBase::readHardwareState() {
    return readHardwareStateImpl();
}

std::string
PixelMonitor::utilslayer::XDAQAppBase::readHardwareConfigurationImpl() {
    // Read the register contents from the hardware.
    PixelMonitor::hwlayer::HwDeviceTCA::RegContentsVec hwContents;
    hwContents = hwReadHardwareConfiguration();

    PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec hwContentsVec;
    for (PixelMonitor::hwlayer::HwDeviceTCA::RegContentsVec::const_iterator i = hwContents.begin();
         i != hwContents.end();
         ++i) {
        hwContentsVec.push_back(std::make_pair(i->first.name(), i->second));
    }

    // Now turn the register contents we found into something that we
    // can stuff into a SOAP message.
    PixelMonitor::hwlayer::RegDumpConfigurationProcessor cfgProcessor;
    std::string const res = cfgProcessor.compose(hwContentsVec);

    return res;
}

std::string
PixelMonitor::utilslayer::XDAQAppBase::readHardwareStateImpl() {
    // NOTE: Things like hwConnect(), hwRelease(), etc., are protected
    // by a lock in the FSM state transitions. In this case we have to
    // take care of locking ourselves. This ensures that nothing fiddles
    // with the hardware while we're dumping the state.
    LockGuard<Lock> guardedLock(monitoringLock_);

    bool const hwWasConnected = getHw().isHwConnected();

    if (!hwWasConnected) {
        hwConnect();
    }

    PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec hwContentsVec = getHw().dumpRegisterContents();

    if (!hwWasConnected) {
        hwRelease();
    }

    // Now turn the register contents we found into something that we
    // can stuff into a SOAP message.
    PixelMonitor::hwlayer::RegDumpConfigurationProcessor cfgProcessor;
    std::string const res = cfgProcessor.compose(hwContentsVec);
    return res;
}

std::string
PixelMonitor::utilslayer::XDAQAppBase::buildIconPathName(xdaq::ApplicationDescriptor const *const app) {
    // The XDAQ application icon is set based on the (XML) class name.
    // NOTE: This assumes that all TCDS packages are structured the same
    // way using the following application class naming:
    // PixelMonitor::packageName::applicationName. (And the same thing with
    // single colons in XML.)
    xdaq::ApplicationDescriptor const *tmp = app;
    if (tmp == 0) {
        tmp = getApplicationDescriptor();
    }
#ifdef XDAQ13
    std::string const className = const_cast<xdaq::ApplicationDescriptor *>(tmp)->getClassName();
#else
    std::string const className = tmp->getClassName();
#endif
    std::list<std::string> const classNamePiecesTmp =
        toolbox::parseTokenList(className, ":");
    std::vector<std::string> const classNamePieces =
        std::vector<std::string>(classNamePiecesTmp.begin(), classNamePiecesTmp.end());
    std::string iconFileName = "/PixelMonitor/utilslayer/images/PixelMonitor_generic_icon.png";
    if (classNamePieces.size() > 2) {
        size_t const index = classNamePieces.size() - 2;
        std::string const packageName = toolbox::tolower(classNamePieces.at(index));
        std::string const applicationName = toolbox::tolower(classNamePieces.back());
        iconFileName = "/PixelMonitor/" + packageName + "/images/" + applicationName + "_icon.png";
    }
    return iconFileName;
}

PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler const &
PixelMonitor::utilslayer::XDAQAppBase::getConfigurationInfoSpaceHandler() const {
    return static_cast<PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler &>(*cfgInfoSpaceP_.get());
}

PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler &
PixelMonitor::utilslayer::XDAQAppBase::getApplicationStateInfoSpaceHandler() {
    return appStateInfoSpace_;
}

xoap::MessageReference
PixelMonitor::utilslayer::XDAQAppBase::parameterSet(xoap::MessageReference msg) {
    // It is not allowed to set parameters straight from SOAP: log and
    // send error back.
    std::string errMsg = "Someone is trying to set an InfoSpace parameter via SOAP. "
                         "This is not allowed, and will be ignored.";
    appStateInfoSpace_.addHistoryItem(errMsg);
    ERROR(errMsg);
    XCEPT_DECLARE(PixelMonitor::exception::SOAPCommandProblem, top, errMsg);
    notifyQualified("error", top);

    std::string soapErrMsg = "Direct setting of InfoSpace parameters via SOAP is not allowed.";
    std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
    PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
    std::string faultString = soapErrMsg;
    xoap::MessageReference reply =
        PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                           faultCode,
                                                           faultString);
    return reply;
}

PixelMonitor::hwlayer::HwDeviceTCA &
PixelMonitor::utilslayer::XDAQAppBase::getHw() const {
    return static_cast<PixelMonitor::hwlayer::HwDeviceTCA &>(*hwP_.get());
}

xoap::MessageReference
PixelMonitor::utilslayer::XDAQAppBase::RenewHwLease(xoap::MessageReference msg) {
    // First question: Is there a lease owner currently?
    if (!isHwLeased()) {
        // No lease owner, so nothing to renew. Send error back.
        std::string msgBase = "There is no hardware lease to renew.";
        ERROR(msgBase);
        XCEPT_DECLARE(PixelMonitor::exception::SOAPCommandProblem, top, msgBase);
        notifyQualified("error", top);
        std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
        PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
        std::string faultString = msgBase;
        xoap::MessageReference reply =
            PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                               faultCode,
                                                               faultString);
        return reply;
    } else {
        // Second question: Who is the current lease owner?
        std::string leaseOwnerId = getHwLeaseOwnerId();

        // Third question: Does the current request come from our
        // hardware owner?
        std::string commandName = "undefined";
        std::string requestorId = "undefined";
        try {
            commandName = PixelMonitor::utilslayer::soap::extractSOAPCommandName(msg);
            requestorId = PixelMonitor::utilslayer::soap::extractSOAPCommandRequestorId(msg);
        }
        catch (PixelMonitor::exception::Exception &err) {
            // Somehow we don't understand the SOAP message. Log an
            // error and flag that we are having trouble.
            std::string msgBase = "SOAP command not understood";
            ERROR(toolbox::toString("%s: '%s'.",
                                    msgBase.c_str(),
                                    xcept::stdformat_exception_history(err).c_str()));
            XCEPT_DECLARE_NESTED(PixelMonitor::exception::HwLeaseRenewalProblem, top,
                                 toolbox::toString("%s.", msgBase.c_str()), err);
            notifyQualified("error", top);
            std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
            PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeSender;
            std::string faultString = msgBase;
            std::string faultDetail = toolbox::toString("%s: '%s'.",
                                                        msgBase.c_str(),
                                                        err.message().c_str());
            xoap::MessageReference reply =
                PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                                   faultCode,
                                                                   faultString,
                                                                   faultDetail);
            return reply;
        }

        if (requestorId != leaseOwnerId) {
            // No match -> send error back.
            std::string msgBase =
                toolbox::toString("Hardware lease renewal not allowed: "
                                  "hardware lease owned by session '%s'.",
                                  leaseOwnerId.c_str());
            ERROR(msgBase);
            XCEPT_DECLARE(PixelMonitor::exception::SOAPCommandProblem, top, msgBase);
            notifyQualified("error", top);
            std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
            PixelMonitor::utilslayer::soap::SOAPFaultCodeType faultCode = PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver;
            std::string faultString = msgBase;
            xoap::MessageReference reply =
                PixelMonitor::utilslayer::soap::makeSOAPFaultReply(soapProtocolVersion,
                                                                   faultCode,
                                                                   faultString);
            return reply;
        } else {
            // Ok, everything checks out. Do what we were asked to do.
            assignHwLease(leaseOwnerId);

            //----------

            // Reply that all is well.
            std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
            try {
                xoap::MessageReference reply =
                    PixelMonitor::utilslayer::soap::makeCommandSOAPReply(soapProtocolVersion,
                                                                         commandName,
                                                                         "");
                return reply;
            }
            catch (xcept::Exception &err) {
                std::string msgBase =
                    toolbox::toString("Failed to create SOAP reply for command '%s'",
                                      commandName.c_str());
                ERROR(toolbox::toString("%s: %s.",
                                        msgBase.c_str(),
                                        xcept::stdformat_exception(err).c_str()));
                XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top,
                                     toolbox::toString("%s.", msgBase.c_str()), err);
                notifyQualified("error", top);
                XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
            }
        }
    }

    //----------

    // Should only get here in case the above SOAP reply fails. In that
    // case: return an empty message.
    return xoap::createMessage();
}

void
PixelMonitor::utilslayer::XDAQAppBase::timeExpired(toolbox::task::TimerEvent &event) {
    INFO("The time has come: expiring hardware lease.");
    appStateInfoSpace_.addHistoryItem("Expiring hardware lease");
    revokeHwLease();
}

PixelMonitor::hwlayer::HwDeviceTCA::RegContentsVec
PixelMonitor::utilslayer::XDAQAppBase::hwReadHardwareConfiguration() const {
    PixelMonitor::hwlayer::RegisterInfo::RegInfoVec regInfos = hwP_->getRegisterInfos();
    PixelMonitor::hwlayer::RegisterInfo::RegInfoVec regInfosFiltered = filterRegInfo(regInfos);
    return hwP_->readHardwareConfiguration(regInfosFiltered);
}

PixelMonitor::hwlayer::RegisterInfo::RegInfoVec
PixelMonitor::utilslayer::XDAQAppBase::filterRegInfo(PixelMonitor::hwlayer::RegisterInfo::RegInfoVec const &regInfosIn) const {
    // Filter the full hardware contents to remove 'the things that
    // should not be.'
    PixelMonitor::hwlayer::RegisterInfo::RegInfoVec regInfosOut;

    for (PixelMonitor::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo = regInfosIn.begin();
         regInfo != regInfosIn.end();
         ++regInfo) {
        // Only registers that can be read _and_ written are useful in a
        // configuration dump.
        if (regInfo->isReadable() && regInfo->isWritable()) {
            // Not all registers are allowed to be used as configuration
            // settings. Let's filter out the disallowed ones.
            if (isRegisterAllowed(*regInfo)) {
                regInfosOut.push_back(*regInfo);
            }
        }
    }

    return regInfosOut;
}

bool
PixelMonitor::utilslayer::XDAQAppBase::isRegisterAllowed(PixelMonitor::hwlayer::RegisterInfo const &regInfo) const {
    return true;
}

std::vector<PixelMonitor::utilslayer::FSMSOAPParHelper>
PixelMonitor::utilslayer::XDAQAppBase::expectedFSMSoapPars(std::string const &commandName) const {
    return expectedFSMSoapParsImpl(commandName);
}

std::vector<PixelMonitor::utilslayer::FSMSOAPParHelper>
PixelMonitor::utilslayer::XDAQAppBase::expectedFSMSoapParsImpl(std::string const &commandName) const {
    // Define what we expect in terms of parameters for each SOAP FSM
    // command.
    std::vector<FSMSOAPParHelper> params;
    if ((commandName == "Configure") ||
        (commandName == "Reconfigure")) {
        params.push_back(FSMSOAPParHelper(commandName, "hardwareConfigurationString", true));
    } else if (commandName == "Enable") {
        params.push_back(FSMSOAPParHelper(commandName, "runNumber", true));
    }
    return params;
}

void
PixelMonitor::utilslayer::XDAQAppBase::loadSOAPCommandParameters(xoap::MessageReference const &msg) {
    loadSOAPCommandParametersImpl(msg);
}

void
PixelMonitor::utilslayer::XDAQAppBase::loadSOAPCommandParametersImpl(xoap::MessageReference const &msg) {
    std::string const commandName = PixelMonitor::utilslayer::soap::extractSOAPCommandName(msg);

    //----------

    // Figure out what to expect in terms of parameters for this FSM
    // SOAP command.
    std::vector<FSMSOAPParHelper> paramsExpected = expectedFSMSoapPars(commandName);
    std::vector<std::string> paramNamesExpected;
    for (std::vector<FSMSOAPParHelper>::const_iterator param = paramsExpected.begin();
         param != paramsExpected.end();
         ++param) {
        paramNamesExpected.push_back(param->parameterName());
    }

    //----------

    // Figure out which parameter we received.
    xoap::SOAPElement commandNode = PixelMonitor::utilslayer::soap::extractBodyNode(msg);
    std::vector<xoap::SOAPElement> paramsFound = commandNode.getChildElements();

    //----------

    // Check if all required parameters are present.
    for (std::vector<FSMSOAPParHelper>::const_iterator param = paramsExpected.begin();
         param != paramsExpected.end();
         ++param) {
        if (param->isRequired()) {
            std::string const parName = param->parameterName();
            if (!PixelMonitor::utilslayer::soap::hasSOAPCommandParameter(msg, parName)) {
                // Missing a required parameter.
                std::string const msg =
                    toolbox::toString("Missing a SOAP command parameter "
                                      "that is required for '%s': '%s.'",
                                      commandName.c_str(),
                                      parName.c_str());
                ERROR(msg);
                XCEPT_DECLARE(PixelMonitor::exception::SOAPCommandProblem, err, msg);
                notifyQualified("error", err);
                XCEPT_RAISE(PixelMonitor::exception::SOAPCommandProblem, msg);
            }
        }
    }

    //----------

    // Check for unexpected/unallowed parameters.
    for (std::vector<xoap::SOAPElement>::iterator param = paramsFound.begin();
         param != paramsFound.end();
         ++param) {
        std::string const parName = param->getElementName().getLocalName();
        std::vector<std::string>::iterator i =
            std::find(paramNamesExpected.begin(), paramNamesExpected.end(), parName);
        if (i == paramNamesExpected.end()) {
            // Found a parameter that we did not expect.
            std::string const msg =
                toolbox::toString("Found a SOAP command parameter "
                                  "that is not accepted for '%s': '%s.'",
                                  commandName.c_str(),
                                  parName.c_str());
            ERROR(msg);
            XCEPT_DECLARE(PixelMonitor::exception::SOAPCommandProblem, err, msg);
            notifyQualified("error", err);
            XCEPT_RAISE(PixelMonitor::exception::SOAPCommandProblem, msg);
        }
    }

    //----------

    // Loop through all expected parameters and (try to) load them.
    for (std::vector<FSMSOAPParHelper>::const_iterator param = paramsExpected.begin();
         param != paramsExpected.end();
         ++param) {
        loadSOAPCommandParameter(msg, *param);
    }
}

void
PixelMonitor::utilslayer::XDAQAppBase::loadSOAPCommandParameter(xoap::MessageReference const &msg,
                                                                FSMSOAPParHelper const &param) {
    loadSOAPCommandParameterImpl(msg, param);

    // Any command can take (optionally) an xdaq::rcmsURL
    // parameter. This is used for asynchronous reporting of FSM state
    // changes.
    /*
  std::string rcmsURL = "";
  bool urlFound = false;
  try
    {
      rcmsURL = PixelMonitor::utilslayer::soap::extractSOAPCommandRCMSURL(msg);
      urlFound = true;
    }
  catch(PixelMonitor::exception::Exception& err)
    {
      // This is okay. If no RCMS url is specified, we continue
      // without.
    }
  if (urlFound)
    {
      INFO(toolbox::toString("Updating RCMS state notification listener URL to '%s'.",
                             rcmsURL.c_str()));
      xdata::Serializable* tmp = getApplicationInfoSpace()->find("rcmsStateListener");
      xdata::Bag<xdaq2rc::ClassnameAndInstance>* bag = dynamic_cast<xdata::Bag<xdaq2rc::ClassnameAndInstance>*>(tmp);
      bag->getField("url")->setValue(xdata::String(rcmsURL));
      getApplicationInfoSpace()->fireItemValueChanged("rcmsStateListener");
    }
  */
}

void
PixelMonitor::utilslayer::XDAQAppBase::loadSOAPCommandParameterImpl(xoap::MessageReference const &msg,
                                                                    FSMSOAPParHelper const &param) {
    std::string const commandName = param.commandName();
    std::string const parName = param.parameterName();

    if ((commandName == "Configure") ||
        (commandName == "Reconfigure")) {
        if (parName == "hardwareConfigurationString") {
            // Import 'xdaq:hardwareConfigurationString' (required).
            std::string hwCfgString =
                PixelMonitor::utilslayer::soap::extractSOAPCommandParameterString(msg, parName);
            // NOTE: explicitly mark empty strings, simply for clarity.
            if (PixelMonitor::utilslayer::trimString(hwCfgString, " \t\n").empty()) {
                hwCfgString =
                    toolbox::toString("%c Empty string received. "
                                      "This message was inserted on the receiving side for clarity.",
                                      PixelMonitor::hwlayer::RegDumpConfigurationProcessor::kCommentChar);
            }
            cfgInfoSpaceP_->setString("hardwareConfigurationStringReceived",
                                      hwCfgString,
                                      true);
        }
    } else if (commandName == "Enable") {
        if (parName == "runNumber") {
            // Import 'xdaq:runNumber' (required).
            uint32_t const runNumber =
                PixelMonitor::utilslayer::soap::extractSOAPCommandParameterUnsignedInteger(msg, parName);
            cfgInfoSpaceP_->setUInt32(parName, runNumber, true);
        }
    }
}
