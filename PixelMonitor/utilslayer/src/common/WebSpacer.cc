#include "PixelMonitor/utilslayer/WebSpacer.h"

#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::utilslayer::WebSpacer::WebSpacer(std::string const &name,
                                               std::string const &description,
                                               Monitor const &monitor,
                                               std::string const &itemSetName,
                                               std::string const &tabName,
                                               size_t const colSpan)
    : WebObject(name, description, monitor, itemSetName, tabName, colSpan) {
}

std::string
PixelMonitor::utilslayer::WebSpacer::getHTMLString() const {
    std::stringstream res;

    res << "<div class=\"tcds-item-table-wrapper\">";
    res << "</div>";
    res << "\n";

    return res.str();
}

std::string
PixelMonitor::utilslayer::WebSpacer::getJSONString() const {
    return "";
}
