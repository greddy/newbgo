#include "PixelMonitor/utilslayer/CurlGetter.h"

#include <memory>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xoap/domutils.h"

#include "PixelMonitor/exception/Exception.h"

size_t
PixelMonitor::utilslayer::CurlGetter::staticWriteCallback(char *buffer, size_t size, size_t nItems, void *getter) {
    return static_cast<PixelMonitor::utilslayer::CurlGetter *>(getter)->receive(buffer, size, nItems);
}

PixelMonitor::utilslayer::CurlGetter::CurlGetter() {
    curl_ = curl_easy_init();
}

PixelMonitor::utilslayer::CurlGetter::~CurlGetter() {
    curl_easy_cleanup(curl_);
}

std::string
PixelMonitor::utilslayer::CurlGetter::buildFullURL(std::string const &scheme,
                                                   std::string const &address,
                                                   std::string const &path,
                                                   std::string const &parameters,
                                                   std::string const &query,
                                                   std::string const &fragment) const {
    std::string url = scheme + "://" + address + "/" + path;
    if (!parameters.empty()) {
        url += ";" + parameters;
    }
    if (!query.empty()) {
        url += "?" + query;
    }
    if (!fragment.empty()) {
        url += "#" + fragment;
    }

    return url;
}

std::string
PixelMonitor::utilslayer::CurlGetter::get(std::string const &scheme,
                                          std::string const &address,
                                          std::string const &path,
                                          std::string const &parameters,
                                          std::string const &query,
                                          std::string const &fragment) {
    std::string const url = buildFullURL(scheme, address, path, parameters, query, fragment);

    // See if the URL is usable.
    XERCES_CPP_NAMESPACE::XMLURL xmlUrl;
    try {
        xmlUrl = url.c_str();
    }
    catch (XERCES_CPP_NAMESPACE::MalformedURLException const &err) {
        std::string const msgBase = "Stumbled across a malformed URL";
        std::string const msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), url.c_str());
        XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg.c_str());
    }

    // Let's get the result.
    receiveBuffer_.clear();
    try {
        performGetRequest(xmlUrl);
    }
    catch (xcept::Exception const &err) {
        std::string const msgBase = toolbox::toString("Failed to GET '%s'", url.c_str());
        std::string const msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
        XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg.c_str());
    }

    std::string const res = receiveBuffer_;
    return res;
}

void
PixelMonitor::utilslayer::CurlGetter::performGetRequest(XERCES_CPP_NAMESPACE::XMLURL const &xmlUrl) {
    curl_easy_reset(curl_);

    // Protect against a CURL bug.
    // http://stackoverflow.com/questions/9191668/error-longjmp-causes-uninitialized-stack-frame
    curl_easy_setopt(curl_, CURLOPT_NOSIGNAL, 1);

    // Specify which URL to get.
    std::string const url = xoap::XMLCh2String(xmlUrl.getURLText());
    curl_easy_setopt(curl_, CURLOPT_URL, url.c_str());

    // Specify the HTTP method to use: GET.
    curl_easy_setopt(curl_, CURLOPT_HTTPGET, 1);

    // Specify a user-agent, just for completeness.
    curl_easy_setopt(curl_, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    // Specify timeout values: 2 seconds.
    // long timeout = 2;
    // wsi: Change timeout setting to 60s. https://svnweb.cern.ch/trac/cmsos/ticket/4650#comment:9
    long timeout = 60;
    curl_easy_setopt(curl_, CURLOPT_TIMEOUT, timeout);
    curl_easy_setopt(curl_, CURLOPT_CONNECTTIMEOUT, timeout);

    // Specify the callback method for writing received data.
    curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, PixelMonitor::utilslayer::CurlGetter::staticWriteCallback);

    // Specify the data pointer to pass to the write callback.
    curl_easy_setopt(curl_, CURLOPT_WRITEDATA, static_cast<void *>(this));

    // Try to prevent infinite redirect loops.
    curl_easy_setopt(curl_, CURLOPT_MAXREDIRS, 10);

    // Clear our receiving buffer.
    receiveBuffer_.clear();

    // Engage!
    CURLcode status = curl_easy_perform(curl_);

    // See what happened.
    if (status != CURLE_OK) {
        XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, curl_easy_strerror(status));
    }
}

size_t
PixelMonitor::utilslayer::CurlGetter::receive(char *buffer, size_t size, size_t nItems) {
    size_t realSize = size * nItems;
    receiveBuffer_.append(static_cast<char const *>(buffer), realSize);
    return realSize;
}
