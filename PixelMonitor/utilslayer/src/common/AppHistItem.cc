#include "PixelMonitor/utilslayer/AppHistItem.h"

PixelMonitor::utilslayer::AppHistItem::AppHistItem(std::string const &msg)
    : timestamp_(toolbox::TimeVal::gettimeofday()),
      msg_(msg),
      count_(1) {
}

PixelMonitor::utilslayer::AppHistItem::~AppHistItem() {
}

void
PixelMonitor::utilslayer::AppHistItem::increaseCount() {
    ++count_;
}

toolbox::TimeVal
PixelMonitor::utilslayer::AppHistItem::timestamp() const {
    return timestamp_;
}

std::string
PixelMonitor::utilslayer::AppHistItem::message() const {
    return msg_;
}

unsigned int
PixelMonitor::utilslayer::AppHistItem::count() const {
    return count_;
}
