#include "PixelMonitor/utilslayer/XDAQObject.h"

#include "xdaq/Application.h"

PixelMonitor::utilslayer::XDAQObject::XDAQObject(xdaq::Application *const owner)
    : ownerP_(owner) {
}

PixelMonitor::utilslayer::XDAQObject::~XDAQObject() {
}

xdaq::Application &
PixelMonitor::utilslayer::XDAQObject::getOwnerApplication() const {
    return *ownerP_;
}
