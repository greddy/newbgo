#include "PixelMonitor/utilslayer/WebTableAppHist.h"

#include <cassert>
#include <sstream>
#include <string>
#include <vector>

#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::utilslayer::WebTableAppHist::WebTableAppHist(std::string const &name,
                                                           std::string const &description,
                                                           PixelMonitor::utilslayer::Monitor const &monitor,
                                                           std::string const &itemSetName,
                                                           std::string const &tabName,
                                                           size_t const colSpan)
    : PixelMonitor::utilslayer::WebObject(name, description, monitor, itemSetName, tabName, colSpan) {
}

std::string
PixelMonitor::utilslayer::WebTableAppHist::getHTMLString() const {
    // NOTE: This has been written to work with the new XDAQ12-style
    // HyperDAQ tabs and doT.js.

    std::stringstream res;

    res << "<div class=\"tcds-item-table-wrapper\">"
        << "\n";

    res << "<p class=\"tcds-item-table-title\">"
        << getName()
        << "</p>";
    res << "\n";

    res << "<p class=\"tcds-item-table-description\">"
        << getDescription()
        << "</p>";
    res << "\n";

    // NOTE: This one is kinda special. Mainly due to the fact that this
    // table can become quite long. Therefore this is handled
    // completely in JavaScript (with the help of a cool, virtual
    // scrolling grid plugin).
    res << "<div id=\"application-status-history-placeholder\" style=\"font-size: 12px; font-family: \'Open Sans\';\"></div>"
        << "\n";

    res << "</div>"
        << "\n";

    return res.str();
}
