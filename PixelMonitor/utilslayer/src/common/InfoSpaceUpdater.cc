#include "PixelMonitor/utilslayer/InfoSpaceUpdater.h"

#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/utilslayer/InfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/InfoSpaceItem.h"

PixelMonitor::utilslayer::InfoSpaceUpdater::InfoSpaceUpdater() {
}

PixelMonitor::utilslayer::InfoSpaceUpdater::~InfoSpaceUpdater() {
}

void
PixelMonitor::utilslayer::InfoSpaceUpdater::updateInfoSpace(InfoSpaceHandler *const infoSpaceHandler) {
    try {
        updateInfoSpaceImpl(infoSpaceHandler);
    }
    catch (...) {
        infoSpaceHandler->setInvalid();
        throw;
    }
}

void
PixelMonitor::utilslayer::InfoSpaceUpdater::updateInfoSpaceImpl(InfoSpaceHandler *const infoSpaceHandler) {
    InfoSpaceHandler::ItemVec &items = infoSpaceHandler->getItems();
    InfoSpaceHandler::ItemVec::iterator iter;

    for (iter = items.begin(); iter != items.end(); ++iter) {
        try {
            updateInfoSpaceItem(*iter, infoSpaceHandler);
        }
        catch (...) {
            iter->setInvalid();
            throw;
        }
    }

    // Now sync everything from the cache to the InfoSpace itself.
    infoSpaceHandler->writeInfoSpace();
}

bool
PixelMonitor::utilslayer::InfoSpaceUpdater::updateInfoSpaceItem(InfoSpaceItem &item,
                                                                InfoSpaceHandler *const infoSpaceHandler) {
    // Nothing is implemented here. It's simply a catch-all which raises
    // an exception to show that something is missing.
    PixelMonitor::utilslayer::InfoSpaceItem::UpdateType updateType = item.updateType();
    if (updateType != PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE) {
        std::string msg =
            toolbox::toString("Updating not implemented for item with name '%s'.",
                              item.name().c_str());
        item.setInvalid();
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
        return false;
    }
    return true;
}
