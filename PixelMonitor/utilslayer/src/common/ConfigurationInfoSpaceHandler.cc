#include "PixelMonitor/utilslayer/ConfigurationInfoSpaceHandler.h"

#include "toolbox/string.h"
#include "xdaq/Application.h"
// OBSOLETE OBSOLETE OBSOLETE
//#include "xdata/Event.h"
// OBSOLETE OBSOLETE OBSOLETE end

#include "PixelMonitor/hwlayer/RegDumpConfigurationProcessor.h"
#include "PixelMonitor/utilslayer/InfoSpaceItem.h"
#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/WebServer.h"
#include "PixelMonitor/utilslayer/WebTableNoLabels.h"

PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application *const xdaqApp,
                                                                                       std::string const &name)
    : PixelMonitor::utilslayer::InfoSpaceHandler(xdaqApp, name, 0, xdaqApp->getApplicationInfoSpace()) {
    // Create all InfoSpace variables.

    // The monitoring update interval. Stored as string, so we can have
    // nice formats like described here:
    //   http://www.w3schools.com/schema/schema_dtypes_date.asp.
    createString("monitoringInterval", "PT1S", "", InfoSpaceItem::NOUPDATE, true);

    // The hardware lease expiration period. Same format as above.
    createString("hardwareLeaseDuration", "PT10M", "", InfoSpaceItem::NOUPDATE, true);

    // This is the path to the text file containing the default hardware
    // configuration.
    // NOTE: This needs to be set on a per-application basis in order to
    // point to the right file for the hardware in question.
    createString("defaultHwConfigurationFilePath",
                 "unset",
                 "",
                 InfoSpaceItem::NOUPDATE,
                 true);

    //----------

    // The CMS DAQ run number.
    createUInt32("runNumber", 0, "", InfoSpaceItem::NOUPDATE, true);

    // The various versions of the hardware configuration string.
    std::string const tmp = toolbox::toString("%c Hardware configuration string not yet set.",
                                              PixelMonitor::hwlayer::RegDumpConfigurationProcessor::kCommentChar);

    // The default hardwareConfigurationString (read from file).
    createString("hardwareConfigurationStringDefault", tmp, "", InfoSpaceItem::NOUPDATE, true);

    // The hardwareConfigurationString we received by SOAP.
    createString("hardwareConfigurationStringReceived", tmp, "", InfoSpaceItem::NOUPDATE, true);

    // The hardwareConfigurationString applied to the hardware (i.e.,
    // after merging with the default settings).
    createString("hardwareConfigurationStringApplied", tmp, "", InfoSpaceItem::NOUPDATE, true);

    //----------

    // OBSOLETE OBSOLETE OBSOLETE
    // // Register to be notified when the XDAQ framework loads the
    // // configuration values from the XML file.
    // xdaqApp->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    // OBSOLETE OBSOLETE OBSOLETE end
}

PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler::~ConfigurationInfoSpaceHandler() {
}

// OBSOLETE OBSOLETE OBSOLETE
// void
// PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler::actionPerformed(xdata::Event& event)
// {
//   // This is called after all default configuration values have been
//   // loaded (from the XDAQ configuration file). This means the
//   // underlying InfoSpace has been modified -> resync our
//   // InfoSpaceHandler buffer to the underlying InfoSpace.
//   if (event.type() == "urn:xdaq-event:setDefaultValues")
//     {
//       readInfoSpace();
//     }
// }
// OBSOLETE OBSOLETE OBSOLETE end

void
PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(Monitor &monitor) {
    // Application configuration items.
    monitor.newItemSet("Application configuration");
    //monitor.addItem("Application configuration",
    //                "runNumber",
    //                "Run number",
    //                this);
    monitor.addItem("Application configuration",
                    "monitoringInterval",
                    "Monitoring update interval",
                    this);
    //monitor.addItem("Application configuration",
    //                "hardwareLeaseDuration",
    //                "Hardware lease duration",
    //                this);
    //monitor.addItem("Application configuration",
    //                "defaultHwConfigurationFilePath",
    //                "Path to default hardware configuration file",
    //                this);

    // Hardware configuration items.
    //monitor.newItemSet("itemset-hardware-configuration-default");
    //monitor.addItem("itemset-hardware-configuration-default",
    //                "hardwareConfigurationStringDefault",
    //                "Configuration string (default)",
    //                this);
    //monitor.newItemSet("itemset-hardware-configuration-received");
    //monitor.addItem("itemset-hardware-configuration-received",
    //                "hardwareConfigurationStringReceived",
    //                "Configuration string (received)",
    //                this);
    //monitor.newItemSet("itemset-hardware-configuration-applied");
    //monitor.addItem("itemset-hardware-configuration-applied",
    //                "hardwareConfigurationStringApplied",
    //                "Configuration string (applied)",
    //                this);
}

void
PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(WebServer &webServer,
                                                                                       Monitor &monitor) {
    webServer.registerTab("Configuration",
                          "Configuration parameters",
                          3);
    webServer.registerTable("Application configuration",
                            "Application configuration parameters",
                            monitor,
                            "Application configuration",
                            "Configuration",
                            3);
    //webServer.registerWebObject<PixelMonitor::utilslayer::WebTableNoLabels, std::string>("Hardware configuration (default)",
    //                                                                        "Default hardware configuration parameters read from file",
    //                                                                        monitor,
    //                                                                        "itemset-hardware-configuration-default",
    //                                                                        "Configuration");
    //webServer.registerWebObject<PixelMonitor::utilslayer::WebTableNoLabels, std::string>("Hardware configuration (received)",
    //                                                                        "Hardware configuration parameters received by SOAP",
    //                                                                        monitor,
    //                                                                        "itemset-hardware-configuration-received",
    //                                                                        "Configuration");
    //webServer.registerWebObject<PixelMonitor::utilslayer::WebTableNoLabels, std::string>("Hardware configuration (applied)",
    //                                                                        "Final configuration parameters applied to the hardware",
    //                                                                        monitor,
    //                                                                        "itemset-hardware-configuration-applied",
    //                                                                        "Configuration");
}
