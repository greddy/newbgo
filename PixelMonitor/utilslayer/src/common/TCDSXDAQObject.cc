#include "PixelMonitor/utilslayer/TCDSXDAQObject.h"

#include "PixelMonitor/utilslayer/XDAQAppBase.h"

PixelMonitor::utilslayer::TCDSXDAQObject::TCDSXDAQObject(XDAQAppBase *const owner)
    : ownerP_(owner) {
}

PixelMonitor::utilslayer::TCDSXDAQObject::~TCDSXDAQObject() {
}

PixelMonitor::utilslayer::XDAQAppBase *
PixelMonitor::utilslayer::TCDSXDAQObject::getOwnerApplication() const {
    return ownerP_;
}
