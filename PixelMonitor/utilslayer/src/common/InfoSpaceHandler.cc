#include "PixelMonitor/utilslayer/InfoSpaceHandler.h"

#include <algorithm>
#include <functional>
#include <iomanip>
#include <ios>
#include <utility>

#include "log4cplus/logger.h"

#include "toolbox/net/URN.h"
#include "toolbox/string.h"
#include "toolbox/TimeInterval.h"
#include "xcept/Exception.h"
#include "xdata/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdaq/Application.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/utilslayer/InfoSpaceUpdater.h"
#include "PixelMonitor/utilslayer/LogMacros.h"
#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::utilslayer::InfoSpaceHandler::InfoSpaceHandler(xdaq::Application *const xdaqApp,
                                                             std::string const &name,
                                                             InfoSpaceUpdater *const updater,
                                                             xdata::InfoSpace *const mirrorInfoSpace)
    : XDAQObject(xdaqApp),
      logger_(xdaqApp->getApplicationLogger()),
      kInvalidItemString_("-"),
      infoSpaceP_(0),
      isInfoSpaceOwner_(true),
      name_(name),
      updaterP_(updater),
      mirrorInfoSpaceP_(mirrorInfoSpace) {
    std::string const infoSpaceName =
        toolbox::toString("%s:lid=%s",
                          name.c_str(),
                          xdaqApp->getApplicationDescriptor()->getAttribute("id").c_str());
    try {
        toolbox::net::URN const urn = xdaqApp->createQualifiedInfoSpace(infoSpaceName);
        infoSpaceP_ = xdata::getInfoSpaceFactory()->get(urn.toString());
        //infoSpaceP_ = xdata::getInfoSpaceFactory()->create(infoSpaceName);
        isInfoSpaceOwner_ = true;
    }
    catch (xdata::exception::Exception &err) {
        std::string msg = "Could not create InfoSpace '" + infoSpaceName + "'.";
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        xdaqApp->notifyQualified("error", top);
        throw top;
    }
}

PixelMonitor::utilslayer::InfoSpaceHandler::~InfoSpaceHandler() {
    if (isInfoSpaceOwner_) {
        if (infoSpaceP_ != 0) {
            xdata::getInfoSpaceFactory()->destroy(infoSpaceP_->name());
            infoSpaceP_ = 0;
        }
    }
}

bool
PixelMonitor::utilslayer::InfoSpaceHandler::exists(std::string const &itemName) const {
    ItemVec::const_iterator item = std::find(items_.begin(), items_.end(), itemName);
    return (item != items_.end());
}

std::string
PixelMonitor::utilslayer::InfoSpaceHandler::name() const {
    return name_;
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::createBool(std::string const &name,
                                                       bool const value,
                                                       std::string const &format,
                                                       InfoSpaceItem::UpdateType const updateType,
                                                       bool const isValid) {
    if (exists(name)) {
        throwItemExists(name);
    } else {
        items_.push_back(InfoSpaceItem(name, InfoSpaceItem::BOOL, format, updateType, isValid));
        BoolInfoSpacePair item(value);
        boolMap_.insert(std::make_pair<std::string, BoolInfoSpacePair>(name, item));
        fireItemAvailable(name, item.ptr().get());
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::createDouble(std::string const &name,
                                                         double const value,
                                                         std::string const &format,
                                                         InfoSpaceItem::UpdateType const updateType,
                                                         bool const isValid) {
    if (exists(name)) {
        throwItemExists(name);
    } else {
        items_.push_back(InfoSpaceItem(name, InfoSpaceItem::DOUBLE, format, updateType, isValid));
        DoubleInfoSpacePair item(value);
        doubleMap_.insert(std::make_pair<std::string, DoubleInfoSpacePair>(name, item));
        fireItemAvailable(name, item.ptr().get());
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::createFloat(std::string const &name,
                                                        float const value,
                                                        std::string const &format,
                                                        InfoSpaceItem::UpdateType const updateType,
                                                        bool const isValid) {
    if (exists(name)) {
        throwItemExists(name);
    } else {
        items_.push_back(InfoSpaceItem(name, InfoSpaceItem::FLOAT, format, updateType, isValid));
        FloatInfoSpacePair item(value);
        floatMap_.insert(std::make_pair<std::string, FloatInfoSpacePair>(name, item));
        fireItemAvailable(name, item.ptr().get());
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::createString(std::string const &name,
                                                         std::string const &value,
                                                         std::string const &format,
                                                         InfoSpaceItem::UpdateType const updateType,
                                                         bool const isValid) {
    if (exists(name)) {
        throwItemExists(name);
    } else {
        items_.push_back(InfoSpaceItem(name, InfoSpaceItem::STRING, format, updateType, isValid));
        StringInfoSpacePair item(value);
        stringMap_.insert(std::make_pair<std::string, StringInfoSpacePair>(name, item));
        fireItemAvailable(name, item.ptr().get());
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::createTimeVal(std::string const &name,
                                                          toolbox::TimeVal const &value,
                                                          std::string const &format,
                                                          InfoSpaceItem::UpdateType const updateType,
                                                          bool const isValid) {
    if (exists(name)) {
        throwItemExists(name);
    } else {
        items_.push_back(InfoSpaceItem(name, InfoSpaceItem::TIMEVAL, format, updateType, isValid));
        TimeValInfoSpacePair item(value);
        timevalMap_.insert(std::make_pair<std::string, TimeValInfoSpacePair>(name, item));
        fireItemAvailable(name, item.ptr().get());
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::createUInt32(std::string const &name,
                                                         uint32_t const value,
                                                         std::string const &format,
                                                         InfoSpaceItem::UpdateType const updateType,
                                                         bool const isValid) {
    if (exists(name)) {
        throwItemExists(name);
    } else {
        items_.push_back(InfoSpaceItem(name, InfoSpaceItem::UINT32, format, updateType, isValid));
        UInt32InfoSpacePair item(value);
        uint32Map_.insert(std::make_pair<std::string, UInt32InfoSpacePair>(name, item));
        fireItemAvailable(name, item.ptr().get());
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::createUInt64(std::string const &name,
                                                         uint64_t const value,
                                                         std::string const &format,
                                                         InfoSpaceItem::UpdateType const updateType,
                                                         bool const isValid) {
    if (exists(name)) {
        throwItemExists(name);
    } else {
        items_.push_back(InfoSpaceItem(name, InfoSpaceItem::UINT64, format, updateType, isValid));
        UInt64InfoSpacePair item(value);
        uint64Map_.insert(std::make_pair<std::string, UInt64InfoSpacePair>(name, item));
        fireItemAvailable(name, item.ptr().get());
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::createStringVec(std::string const &name,
                                                            std::vector<std::string> const value,
                                                            std::string const &format,
                                                            InfoSpaceItem::UpdateType const updateType,
                                                            bool const isValid) {
    if (exists(name)) {
        throwItemExists(name);
    } else {
        items_.push_back(InfoSpaceItem(name, InfoSpaceItem::STRINGVEC, format, updateType, isValid));
        StringVecInfoSpacePair item(value);
        stringVecMap_.insert(std::make_pair<std::string, StringVecInfoSpacePair>(name, item));
        fireItemAvailable(name, item.ptr().get());
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::createUInt32Vec(std::string const &name,
                                                            std::vector<uint32_t> const value,
                                                            std::string const &format,
                                                            InfoSpaceItem::UpdateType const updateType,
                                                            bool const isValid) {
    if (exists(name)) {
        throwItemExists(name);
    } else {
        items_.push_back(InfoSpaceItem(name, InfoSpaceItem::UINT32VEC, format, updateType, isValid));
        UInt32VecInfoSpacePair item(value);
        uint32VecMap_.insert(std::make_pair<std::string, UInt32VecInfoSpacePair>(name, item));
        fireItemAvailable(name, item.ptr().get());
    }
}

std::string
PixelMonitor::utilslayer::InfoSpaceHandler::getFormatted(std::string const &name) const {
    if (!exists(name)) {
        std::string msg = "Trying to read a non-existent InfoSpace item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        ItemVec::const_iterator item = std::find(items_.begin(), items_.end(), name);
        return formatItem(item);
    }
}

PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec &
PixelMonitor::utilslayer::InfoSpaceHandler::getItems() {
    return items_;
}

bool
PixelMonitor::utilslayer::InfoSpaceHandler::getBool(std::string const &name) const {
    BoolMap::const_iterator iter = boolMap_.find(name);
    if (iter == boolMap_.end()) {
        std::string msg = "Trying to read a non-existent InfoSpace boolean item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        bool res = (*iter).second.value();
        unlock();
        return res;
    }
}

double
PixelMonitor::utilslayer::InfoSpaceHandler::getDouble(std::string const &name) const {
    DoubleMap::const_iterator iter = doubleMap_.find(name);
    if (iter == doubleMap_.end()) {
        std::string msg = "Trying to read a non-existent InfoSpace double item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        double res = (*iter).second.value();
        unlock();
        return res;
    }
}

float
PixelMonitor::utilslayer::InfoSpaceHandler::getFloat(std::string const &name) const {
    FloatMap::const_iterator iter = floatMap_.find(name);
    if (iter == floatMap_.end()) {
        std::string msg = "Trying to read a non-existent InfoSpace float item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        float res = (*iter).second.value();
        unlock();
        return res;
    }
}

std::string
PixelMonitor::utilslayer::InfoSpaceHandler::getString(std::string const &name) const {
    StringMap::const_iterator iter = stringMap_.find(name);
    if (iter == stringMap_.end()) {
        std::string msg = "Trying to read a non-existent InfoSpace string item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        std::string res = (*iter).second.value();
        unlock();
        return res;
    }
}

toolbox::TimeVal
PixelMonitor::utilslayer::InfoSpaceHandler::getTimeVal(std::string const &name) const {
    TimeValMap::const_iterator iter = timevalMap_.find(name);
    if (iter == timevalMap_.end()) {
        std::string msg = "Trying to read a non-existent InfoSpace TimeVal item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        toolbox::TimeVal res = (*iter).second.value();
        unlock();
        return res;
    }
}

uint32_t
PixelMonitor::utilslayer::InfoSpaceHandler::getUInt32(std::string const &name) const {
    UInt32Map::const_iterator iter = uint32Map_.find(name);
    if (iter == uint32Map_.end()) {
        std::string msg = "Trying to read a non-existent InfoSpace uint32 item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        uint32_t res = (*iter).second.value();
        unlock();
        return res;
    }
}

uint64_t
PixelMonitor::utilslayer::InfoSpaceHandler::getUInt64(std::string const &name) const {
    UInt64Map::const_iterator iter = uint64Map_.find(name);
    if (iter == uint64Map_.end()) {
        std::string msg = "Trying to read a non-existent InfoSpace uint64 item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        uint64_t res = (*iter).second.value();
        unlock();
        return res;
    }
}

std::vector<std::string>
PixelMonitor::utilslayer::InfoSpaceHandler::getStringVec(std::string const &name) const {
    StringVecMap::const_iterator iter = stringVecMap_.find(name);
    if (iter == stringVecMap_.end()) {
        std::string msg = "Trying to read a non-existent InfoSpace string-vector item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        std::vector<std::string> res = (*iter).second.value();
        unlock();
        return res;
    }
}

std::vector<uint32_t>
PixelMonitor::utilslayer::InfoSpaceHandler::getUInt32Vec(std::string const &name) const {
    UInt32VecMap::const_iterator iter = uint32VecMap_.find(name);
    if (iter == uint32VecMap_.end()) {
        std::string msg = "Trying to read a non-existent InfoSpace uint32-vector item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        std::vector<uint32_t> res = (*iter).second.value();
        unlock();
        return res;
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setBool(std::string const &name,
                                                    bool const newVal,
                                                    bool const push) {
    BoolMap::iterator iter = boolMap_.find(name);
    if (iter == boolMap_.end()) {
        std::string msg = "Trying to set a non-existent InfoSpace boolean item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        (*iter).second.set(newVal, push);
        if (push) {
            fireItemValueChanged(name);
        }
        unlock();
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setDouble(std::string const &name,
                                                      double const newVal,
                                                      bool const push) {
    DoubleMap::iterator iter = doubleMap_.find(name);
    if (iter == doubleMap_.end()) {
        std::string msg = "Trying to set a non-existent InfoSpace double item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        (*iter).second.set(newVal, push);
        if (push) {
            fireItemValueChanged(name);
        }
        unlock();
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setFloat(std::string const &name,
                                                     float const newVal,
                                                     bool const push) {
    FloatMap::iterator iter = floatMap_.find(name);
    if (iter == floatMap_.end()) {
        std::string msg = "Trying to set a non-existent InfoSpace float item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        (*iter).second.set(newVal, push);
        if (push) {
            fireItemValueChanged(name);
        }
        unlock();
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setString(std::string const &name,
                                                      std::string const &newVal,
                                                      bool const push) {
    StringMap::iterator iter = stringMap_.find(name);
    if (iter == stringMap_.end()) {
        std::string msg = "Trying to set a non-existent InfoSpace string item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        (*iter).second.set(newVal, push);
        if (push) {
            fireItemValueChanged(name);
        }
        unlock();
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setTimeVal(std::string const &name,
                                                       toolbox::TimeVal const &newVal,
                                                       bool const push) {
    TimeValMap::iterator iter = timevalMap_.find(name);
    if (iter == timevalMap_.end()) {
        std::string msg = "Trying to set a non-existent InfoSpace TimeVal item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        (*iter).second.set(newVal, push);
        if (push) {
            fireItemValueChanged(name);
        }
        unlock();
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setUInt32(std::string const &name,
                                                      uint32_t const newVal,
                                                      bool const push) {
    UInt32Map::iterator iter = uint32Map_.find(name);
    if (iter == uint32Map_.end()) {
        std::string msg = "Trying to set a non-existent InfoSpace uint32 item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        (*iter).second.set(newVal, push);
        if (push) {
            fireItemValueChanged(name);
        }
        unlock();
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setUInt64(std::string const &name,
                                                      uint64_t const newVal,
                                                      bool const push) {
    UInt64Map::iterator iter = uint64Map_.find(name);
    if (iter == uint64Map_.end()) {
        std::string msg = "Trying to set a non-existent InfoSpace uin64 item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        (*iter).second.set(newVal, push);
        if (push) {
            fireItemValueChanged(name);
        }
        unlock();
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setStringVec(std::string const &name,
                                                         std::vector<std::string> const newVal,
                                                         bool const push) {
    StringVecMap::iterator iter = stringVecMap_.find(name);
    if (iter == stringVecMap_.end()) {
        std::string msg = "Trying to set a non-existent InfoSpace string-vector item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        (*iter).second.set(newVal, push);
        if (push) {
            fireItemValueChanged(name);
        }
        unlock();
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setUInt32Vec(std::string const &name,
                                                         std::vector<uint32_t> const newVal,
                                                         bool const push) {
    UInt32VecMap::iterator iter = uint32VecMap_.find(name);
    if (iter == uint32VecMap_.end()) {
        std::string msg = "Trying to set a non-existent InfoSpace uint32-vector item '" +
                          name +
                          "'.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    } else {
        lock();
        (*iter).second.set(newVal, push);
        if (push) {
            fireItemValueChanged(name);
        }
        unlock();
    }
}

std::string
PixelMonitor::utilslayer::InfoSpaceHandler::formatItem(ItemVec::const_iterator const &item) const {
    std::string res = kInvalidItemString_;
    if (item->isValid()) {
        bool badFormat = false;
        std::stringstream tmp;
        std::string name = item->name();
        InfoSpaceItem::ItemType type = item->type();
        std::string fmt = item->format();
        if (type == InfoSpaceItem::BOOL) {
            bool value = getBool(name);
            if ((fmt == "") || (fmt == "yes/no")) {
                if (value) {
                    tmp << "yes";
                } else {
                    tmp << "no";
                }
            } else if (fmt == "enabled/disabled") {
                if (value) {
                    tmp << "enabled";
                } else {
                    tmp << "disabled";
                }
            } else if (fmt == "true/false") {
                // NOTE: Note the difference with 'false/true'.
                if (value) {
                    tmp << "true";
                } else {
                    tmp << "false";
                }
            } else if (fmt == "false/true") {
                // NOTE: Note the difference with 'true/false'.
                if (value) {
                    tmp << "false";
                } else {
                    tmp << "true";
                }
            } else if (fmt == "good/bad") {
                if (value) {
                    tmp << "good";
                } else {
                    tmp << "bad";
                }
            } else {
                ERROR("Format '" << fmt << "' for item '" << name << "' is not supported.");
                badFormat = true;
            }
        } else if ((type == InfoSpaceItem::FLOAT) ||
                   (type == InfoSpaceItem::DOUBLE)) {
            // NOTE: For formatting and viewing the double-vs-float
            // precision does not matter. Let's simply use float for
            // both.
            float value = 0;
            if (type == InfoSpaceItem::FLOAT) {
                value = getFloat(name);
            } else {
                value = getDouble(name);
            }
            if (fmt == "") {
                tmp << value;
            } else if (fmt == "percentage") {
                tmp << std::fixed << std::setprecision(2) << value;
            } else if ((fmt == "rate") || (fmt == "freq")) {
                if (value == 0.) {
                    tmp << std::fixed << std::setprecision(0) << value;
                } else if (value <= 1.) {
                    tmp << std::fixed << std::setprecision(1) << value;
                } else {
                    tmp << std::fixed << std::setprecision(0) << value;
                }
            }
            // OBSOLETE OBSOLETE OBSOLETE
            // The InfoSpaceHandler now stores 'native' TimeVals.
            else if (fmt == "time_val") {
                tmp << PixelMonitor::utilslayer::formatTimestamp(value);
            }
            // OBSOLETE OBSOLETE OBSOLETE end
            else if (fmt == "time_interval") {
                tmp << toolbox::TimeInterval(value).toString();
            } else if (toolbox::startsWith(fmt, "%")) {
                tmp << toolbox::toString(fmt.c_str(), value);
            } else {
                ERROR("Format '" << fmt << "' for item '" << name << "' is not supported.");
                badFormat = true;
            }
        } else if (type == InfoSpaceItem::STRING) {
            std::string value = getString(name);
            if (fmt == "") {
                tmp << value;
            } else {
                ERROR("Format '" << fmt << "' for item '" << name << "' is not supported.");
                badFormat = true;
            }
        } else if (type == InfoSpaceItem::TIMEVAL) {
            toolbox::TimeVal value = getTimeVal(name);
            if (fmt == "") {
                tmp << PixelMonitor::utilslayer::formatTimestamp(value);
            } else if (fmt == "time_val_no_zero") {
                if (double(value) != 0.) {
                    tmp << PixelMonitor::utilslayer::formatTimestamp(value);
                } else {
                    // NOTE: Let's not simply show '-'. That is already
                    // used for invalid/unupdated items. Show 'n/a'
                    // instead.
                    tmp << "n/a";
                }
            } else {
                ERROR("Format '" << fmt << "' for item '" << name << "' is not supported.");
                badFormat = true;
            }
        } else if (type == InfoSpaceItem::UINT32) {
            uint32_t value = getUInt32(name);
            if (fmt == "") {
                tmp << std::dec << value;
            } else if ((fmt == "x") || (fmt == "hex")) {
                tmp << "0x" << std::hex << value;
            } else if (fmt == "x2") {
                tmp << "0x" << std::setfill('0') << std::setw(2) << std::hex << value;
            } else if (fmt == "x4") {
                tmp << "0x" << std::setfill('0') << std::setw(4) << std::hex << value;
            } else if (fmt == "x8") {
                tmp << "0x" << std::setfill('0') << std::setw(8) << std::hex << value;
            }
            // else if (fmt == "tts_state")
            //   {
            //     tmp << TTSStateToString(value);
            //   }
            // else if (fmt == "tts_trigger")
            //   {
            //     tmp << TTSTriggerToString(value);
            //   }
            else if (fmt == "time_val") {
                tmp << PixelMonitor::utilslayer::formatTimestamp(value);
            } else if (fmt == "good_if_zero") {
                if (value == 0) {
                    tmp << "good";
                } else {
                    tmp << "bad";
                }
            } else if (fmt == "good_if_one") {
                if (value == 1) {
                    tmp << "good";
                } else {
                    tmp << "bad";
                }
            } else {
                ERROR("Format '" << fmt << "' for item '" << name << "' is not supported.");
                badFormat = true;
            }
        } else if (type == InfoSpaceItem::UINT64) {
            uint64_t value = getUInt64(name);
            if (fmt == "") {
                tmp << std::dec << value;
            } else if ((fmt == "x") || (fmt == "hex")) {
                tmp << "0x" << std::hex << value;
            } else if (fmt == "x2") {
                tmp << "0x" << std::setfill('0') << std::setw(2) << std::hex << value;
            } else if (fmt == "x4") {
                tmp << "0x" << std::setfill('0') << std::setw(4) << std::hex << value;
            } else if (fmt == "x8") {
                tmp << "0x" << std::setfill('0') << std::setw(8) << std::hex << value;
            } else {
                ERROR("Format '" << fmt << "' for item '" << name << "' is not supported.");
                badFormat = true;
            }
        } else if (type == InfoSpaceItem::UINT32VEC) {
            std::vector<uint32_t> value = getUInt32Vec(name);
            if (fmt == "") {
                tmp << std::dec;
                for (std::vector<uint32_t>::const_iterator it = value.begin();
                     it != value.end();
                     ++it) {
                    if (!tmp.str().empty()) {
                        tmp << ", ";
                    }
                    tmp << *it;
                }
            } else {
                ERROR("Format '" << fmt << "' for item '" << name << "' is not supported.");
                badFormat = true;
            }
        } else {
            badFormat = true;
        }

        if (badFormat) {
            std::stringstream msgTmp;
            msgTmp << "Formatting of InfoSpaceItem type "
                   << type
                   << " (for item '"
                   << name
                   << "') has not been implemented";
            std::string msg = msgTmp.str();
            FATAL(msg);
            XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
        }

        res = tmp.str();
    }

    // NOTE: The assumption is that this method only formats JSON
    // strings. (I.e., nothing that needs to be interpreted as a proper
    // JavaScript object.
    res = escapeAsJSONString(res);

    return res;
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::throwItemExists(std::string const &name) {
    std::string msg = "An element with the name '" +
                      name +
                      "' already exists in the InfoSpace.";
    FATAL(msg);
    XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::fireItemAvailable(std::string const &name,
                                                              xdata::Serializable *serializable) {
    lock();
    infoSpaceP_->fireItemAvailable(name, serializable);
    if (mirrorInfoSpaceP_) {
        mirrorInfoSpaceP_->fireItemAvailable(name, serializable);
    }
    unlock();
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::fireItemValueChanged(std::string const &name) {
    // BUG BUG BUG
    // For the moment this is commented out in order to avoid breaking
    // the xmas monitoring with 'pushed' single-item updates. This
    // should be fixed 'the right way' by removing the pushed updating
    // itself.
    // BUG BUG BUG end

    // // NOTE: The xmas::probe only responds to
    // // urn:xdata-event:ItemGroupChangedEvent, not to a single-item
    // // ItemValueChangedEvent. So for the xmas monitoring to work, one
    // // _has to fire_ the ItemGroupChangedEvent.
    // std::list<std::string> tmp;
    // tmp.push_back(name);
    // fireItemGroupChanged(tmp);
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::fireItemGroupChanged(std::list<std::string> &names) {
    lock();
    infoSpaceP_->fireItemGroupChanged(names, 0);
    if (mirrorInfoSpaceP_) {
        mirrorInfoSpaceP_->fireItemGroupChanged(names, 0);
    }
    unlock();
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::update() {
    DEBUG("Updating InfoSpace " << name());
    if (!updaterP_) {
        DEBUG("No updater registered for InfoSpace '"
              << name()
              << "'. Not updating anything.");
    } else {
        DEBUG("Calling registered updater for InfoSpace '"
              << name()
              << "'.");
        updaterP_->updateInfoSpace(this);
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setValid() {
    std::for_each(items_.begin(), items_.end(), std::mem_fun_ref(&PixelMonitor::utilslayer::InfoSpaceItem::setValid));
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::setInvalid() {
    std::for_each(items_.begin(), items_.end(), std::mem_fun_ref(&PixelMonitor::utilslayer::InfoSpaceItem::setInvalid));
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::registerItemSets(Monitor &monitor,
                                                             WebServer &webServer) {
    registerItemSetsWithMonitor(monitor);
    registerItemSetsWithWebServer(webServer, monitor);
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::lock() const {
    infoSpaceP_->lock();
    if (mirrorInfoSpaceP_) {
        mirrorInfoSpaceP_->lock();
    }
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::unlock() const {
    if (mirrorInfoSpaceP_) {
        mirrorInfoSpaceP_->unlock();
    }
    infoSpaceP_->unlock();
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::readInfoSpace() {
    lock();

    // Sync all booleans.
    for (BoolMap::iterator iter = boolMap_.begin();
         iter != boolMap_.end(); ++iter) {
        iter->second.readFromInfoSpace();
    }

    // Sync all doubles.
    for (DoubleMap::iterator iter = doubleMap_.begin();
         iter != doubleMap_.end(); ++iter) {
        iter->second.readFromInfoSpace();
    }

    // Sync all floats.
    for (FloatMap::iterator iter = floatMap_.begin();
         iter != floatMap_.end(); ++iter) {
        iter->second.readFromInfoSpace();
    }

    // Sync all strings.
    for (StringMap::iterator iter = stringMap_.begin();
         iter != stringMap_.end(); ++iter) {
        iter->second.readFromInfoSpace();
    }

    // Sync all TimeVals.
    for (TimeValMap::iterator iter = timevalMap_.begin();
         iter != timevalMap_.end(); ++iter) {
        iter->second.readFromInfoSpace();
    }

    // Sync all unsigned 32-bit integers.
    for (UInt32Map::iterator iter = uint32Map_.begin();
         iter != uint32Map_.end(); ++iter) {
        iter->second.readFromInfoSpace();
    }

    // Sync all unsigned 64-bit integers.
    for (UInt64Map::iterator iter = uint64Map_.begin();
         iter != uint64Map_.end(); ++iter) {
        iter->second.readFromInfoSpace();
    }

    // Sync all string vectors.
    for (StringVecMap::iterator iter = stringVecMap_.begin();
         iter != stringVecMap_.end(); ++iter) {
        iter->second.readFromInfoSpace();
    }

    // Sync all unsigned 32-bit integer vectors.
    for (UInt32VecMap::iterator iter = uint32VecMap_.begin();
         iter != uint32VecMap_.end(); ++iter) {
        iter->second.readFromInfoSpace();
    }

    unlock();
}

void
PixelMonitor::utilslayer::InfoSpaceHandler::writeInfoSpace() {
    lock();

    // Sync all booleans.
    for (BoolMap::iterator iter = boolMap_.begin();
         iter != boolMap_.end(); ++iter) {
        iter->second.writeToInfoSpace();
    }

    // Sync all doubles.
    for (DoubleMap::iterator iter = doubleMap_.begin();
         iter != doubleMap_.end(); ++iter) {
        iter->second.writeToInfoSpace();
    }

    // Sync all floats.
    for (FloatMap::iterator iter = floatMap_.begin();
         iter != floatMap_.end(); ++iter) {
        iter->second.writeToInfoSpace();
    }

    // Sync all strings.
    for (StringMap::iterator iter = stringMap_.begin();
         iter != stringMap_.end(); ++iter) {
        iter->second.writeToInfoSpace();
    }

    // Sync all TimeVals.
    for (TimeValMap::iterator iter = timevalMap_.begin();
         iter != timevalMap_.end(); ++iter) {
        iter->second.writeToInfoSpace();
    }

    // Sync all 32-bit integers.
    for (UInt32Map::iterator iter = uint32Map_.begin();
         iter != uint32Map_.end(); ++iter) {
        iter->second.writeToInfoSpace();
    }

    // Sync all 64-bit integers.
    for (UInt64Map::iterator iter = uint64Map_.begin();
         iter != uint64Map_.end(); ++iter) {
        iter->second.writeToInfoSpace();
    }

    // Sync all string vectors.
    for (StringVecMap::iterator iter = stringVecMap_.begin();
         iter != stringVecMap_.end(); ++iter) {
        iter->second.writeToInfoSpace();
    }

    // Sync all 32-bit integer vectors.
    for (UInt32VecMap::iterator iter = uint32VecMap_.begin();
         iter != uint32VecMap_.end(); ++iter) {
        iter->second.writeToInfoSpace();
    }

    // Let XMAS know everything changed.
    ItemVec itemVec = getItems();
    std::list<std::string> itemNames;
    for (ItemVec::const_iterator i = itemVec.begin();
         i != itemVec.end();
         ++i) {
        itemNames.push_back(i->name());
    }
    fireItemGroupChanged(itemNames);

    unlock();
}
