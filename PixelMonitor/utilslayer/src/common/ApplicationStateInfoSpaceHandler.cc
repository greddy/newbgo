#include "PixelMonitor/utilslayer/ApplicationStateInfoSpaceHandler.h"

#include "xdaq/Application.h"

#include "toolbox/string.h"

#include "PixelMonitor/utilslayer/InfoSpaceUpdater.h"
#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"
#include "PixelMonitor/utilslayer/WebServer.h"
#include "PixelMonitor/utilslayer/WebTableAppHist.h"

std::string const PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::kNoProblemString = "-";

PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::ApplicationStateInfoSpaceHandler(xdaq::Application *const xdaqApp,
                                                                                             PixelMonitor::utilslayer::InfoSpaceUpdater *updater)
    : PixelMonitor::utilslayer::InfoSpaceHandler(xdaqApp, "PixelMonitor-application-state", updater, xdaqApp->getApplicationInfoSpace()) {
    // NOTE: In order for RunControl to work, each application needs to
    // have a 'stateName' string in the default application
    // InfoSpace. Therefore this InfoSpaceHandler uses the built-in
    // 'mirroring' capabilities of the InfoSpaceHandler to copy its
    // contents into the default application InfoSpace.

    // These are _real_ application state variables.
    createString("stateName", "uninitialized", "", InfoSpaceItem::NOUPDATE, true);
    createString("applicationState", "uninitialized", "", InfoSpaceItem::NOUPDATE, true);
    createString("problemDescription", "uninitialized", "", InfoSpaceItem::NOUPDATE, true);

    // A string identifying the RunControl session owning the hardware
    // lease.
    createString("hwLeaseOwnerId", "", "", InfoSpaceItem::NOUPDATE, true);

    // Some time-keeping variables (for monitoring only, really).
    createDouble("upTime", 0., "time_interval", InfoSpaceItem::NOUPDATE, true);
    createTimeVal("latestMonitoringUpdate", 0., "", InfoSpaceItem::NOUPDATE, true);
    createDouble("latestMonitoringDuration", 0., "%.3f", InfoSpaceItem::NOUPDATE, true);

    // Keep track of what happened.
    createString("history", "uninitialized", "", InfoSpaceItem::NOUPDATE, true);
}

PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::~ApplicationStateInfoSpaceHandler() {
}

std::string
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::formatItem(PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec::const_iterator const &item) const {
    std::string res = PixelMonitor::utilslayer::escapeAsJSONString(PixelMonitor::utilslayer::InfoSpaceHandler::kInvalidItemString_);
    if (item->isValid()) {
        std::string name = item->name();
        if (name == "history") {
            // Special in the sense that this is something that needs to
            // be interpreted as a proper JavaScript object, so let's
            // not add any more double quotes.
            res = getString(name);
        } else {
            // For everything else simply call the generic formatter.
            res = InfoSpaceHandler::formatItem(item);
        }
    }
    return res;
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::addProblem(std::string const &keyword,
                                                                       std::string const &problemDescription) {
    lock();
    problems_[keyword] = problemDescription;
    updateStatusDescription();
    unlock();
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::removeProblem(std::string const &keyword) {
    lock();
    problems_.erase(keyword);
    updateStatusDescription();
    unlock();
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::addHistoryItem(std::string const &desc) {
    // Keep track of the underlying history items first.
    history_.addItem(desc);

    // Update the history string used for the table.
    std::string histString = history_.getJSONString();
    setString("history", histString, true);
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::setFSMState(std::string const &stateDescription,
                                                                        std::string const &problemDescription) {
    std::string const keyword = "FSM transition problem";
    lock();
    setString("stateName", stateDescription, true);
    //setString("stateName", "n/a", true);
    if (problemDescription != kNoProblemString) {
        addProblem(keyword, problemDescription);
    } else {
        removeProblem(keyword);
    }
    unlock();
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::addMonitoringProblem(std::string const &problemDescription) {
    std::string const keyword = "Monitoring problem";
    addProblem(keyword, problemDescription);
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::removeMonitoringProblem() {
    std::string const keyword = "Monitoring problem";
    removeProblem(keyword);
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::setApplicationState(std::string const &stateDescription) {
    setString("applicationState", stateDescription, true);
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::setProblemDescription(std::string const &problemDescription) {
    setString("problemDescription", problemDescription, true);
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::updateStatusDescription() {
    if (problems_.size() == 0) {
        setApplicationState("All OK");
        setProblemDescription(kNoProblemString);
    } else {
        std::string tmp = "";
        std::string problemList = "";
        for (std::map<std::string, std::string>::const_iterator i = problems_.begin();
             i != problems_.end();
             ++i) {
            if (!tmp.empty()) {
                tmp += ", ";
                problemList += "\n";
            }
            tmp += i->first;
            problemList += i->first;
            problemList += ":\n  ";
            problemList += i->second;
        }

        std::string problemSummary;
        if (problems_.size() == 1) {
            problemSummary =
                toolbox::toString("Detected a problem: %s", tmp.c_str());
        } else {
            problemSummary =
                toolbox::toString("Detected %d problems: %s.", problems_.size(), tmp.c_str());
        }
        setApplicationState(problemSummary);
        setProblemDescription(problemList);
    }
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::registerItemSetsWithMonitor(Monitor &monitor) {
    std::string itemSetName = "Application state";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "stateName",
                    "Application FSM state",
                    this);
    monitor.addItem(itemSetName,
                    "applicationState",
                    "Application status",
                    this);
    monitor.addItem(itemSetName,
                    "problemDescription",
                    "Problem description",
                    this);
    //monitor.addItem(itemSetName,
    //                "hwLeaseOwnerId",
    //                "RunControl session in charge",
    //                this);
    monitor.addItem(itemSetName,
                    "upTime",
                    "Uptime",
                    this);
    monitor.addItem(itemSetName,
                    "latestMonitoringUpdate",
                    "Latest monitoring update time",
                    this);
    monitor.addItem(itemSetName,
                    "latestMonitoringDuration",
                    "Latest monitoring update duration (s)",
                    this);

    itemSetName = "itemset-application-status-history";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "history",
                    "Application status history",
                    this);
}

void
PixelMonitor::utilslayer::ApplicationStateInfoSpaceHandler::registerItemSetsWithWebServer(WebServer &webServer,
                                                                                          Monitor &monitor) {
    std::string const tabName = "Application status";
    webServer.registerTab(tabName,
                          "Application information",
                          1);
    webServer.registerTable("Application state",
                            "Info on the state of the XDAQ application",
                            monitor,
                            "Application state",
                            tabName);
    webServer.registerWebObject<WebTableAppHist, std::string>("History",
                                                              "Application status and command history",
                                                              monitor,
                                                              "itemset-application-status-history",
                                                              tabName);
}
