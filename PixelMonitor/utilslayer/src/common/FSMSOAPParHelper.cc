#include "PixelMonitor/utilslayer/FSMSOAPParHelper.h"

PixelMonitor::utilslayer::FSMSOAPParHelper::FSMSOAPParHelper(std::string const &commandName,
                                                             std::string const &parameterName,
                                                             bool const isRequired // ,
                                                                                   // std::string const& defaultValue
                                                             )
    : commandName_(commandName),
      parameterName_(parameterName),
      isRequired_(isRequired) // ,
                              // defaultValue_(defaultValue)
{
}

PixelMonitor::utilslayer::FSMSOAPParHelper::~FSMSOAPParHelper() {
}

std::string
PixelMonitor::utilslayer::FSMSOAPParHelper::commandName() const {
    return commandName_;
}

std::string
PixelMonitor::utilslayer::FSMSOAPParHelper::parameterName() const {
    return parameterName_;
}

bool
PixelMonitor::utilslayer::FSMSOAPParHelper::isRequired() const {
    return isRequired_;
}

// std::string
// PixelMonitor::utilslayer::FSMSOAPParHelper::defaultValue() const
// {
//   return defaultValue_;
// }
