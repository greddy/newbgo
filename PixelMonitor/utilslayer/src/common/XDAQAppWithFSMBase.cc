#include "PixelMonitor/utilslayer/XDAQAppWithFSMBase.h"

#include <algorithm>
#include <fstream>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>

#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"

#include "PixelMonitor/hwlayer/ConfigurationProcessor.h"
#include "PixelMonitor/hwlayer/RegDumpConfigurationProcessor.h"
#include "PixelMonitor/hwlayer/RegisterInfo.h"
#include "PixelMonitor/hwlayer/HwDeviceTCA.h"
#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/utilslayer/ConfigurationInfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/Lock.h"
#include "PixelMonitor/utilslayer/LockGuard.h"
#include "PixelMonitor/utilslayer/LogMacros.h"
#include "PixelMonitor/utilslayer/SOAPUtils.h"
#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::utilslayer::XDAQAppWithFSMBase::XDAQAppWithFSMBase(xdaq::ApplicationStub *const stub,
                                                                 std::unique_ptr<PixelMonitor::hwlayer::HwDeviceTCA> hw)
    : XDAQAppBase(stub, hw),
      fsm_(this) {
}

PixelMonitor::utilslayer::XDAQAppWithFSMBase::~XDAQAppWithFSMBase() {
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::coldResetAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::coldResetAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        coldResetActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'ColdReset' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::coldResetAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::configureAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::configureAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        configureActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'Configure' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    cfgInfoSpaceP_->writeInfoSpace();

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::configureAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::enableAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::enableAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        enableActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'Enable' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::enableAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::failAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::failAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        failActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'Fail' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::failAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::haltAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::haltAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        haltActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'Halt' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    // If we got here, all is well. Hardware has been released. Revoke
    // the hardware lease as well now.
    revokeHwLease();

    // Reset the run number as well.
    cfgInfoSpaceP_->setUInt32("runNumber", 0, true);
    cfgInfoSpaceP_->writeInfoSpace();

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::haltAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::pauseAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::pauseAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        pauseActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'Pause' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::pauseAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::resumeAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::resumeAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        resumeActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'Resume' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::resumeAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::stopAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::stopAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        stopActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'Stop' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::stopAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::ttcHardResetAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::ttcHardResetAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        ttcHardResetActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'TTCHardReset' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::ttcHardResetAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::ttcResyncAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::ttcResyncAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        ttcResyncActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'TTCResync' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::ttcResyncAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::zeroAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMBase::zeroAction()");
    LockGuard<Lock> guardedLock(monitoringLock_);
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try {
        zeroActionImpl(event);
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "State transition 'Zero' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMBase::zeroAction() took "
         << PixelMonitor::utilslayer::formatDeltaTString(timeBegin, timeEnd)
         << ".");
}

xoap::MessageReference
PixelMonitor::utilslayer::XDAQAppWithFSMBase::changeState(xoap::MessageReference msg) {
    return changeStateImpl(msg);
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::coldResetActionImpl(toolbox::Event::Reference event) {
    try {
        hwColdReset();
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "Could not ColdReset the hardware";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::HardwareProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::configureActionImpl(toolbox::Event::Reference event) {
    try {
        hwConnect();
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "Could not connect to the hardware";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::HardwareProblem, top, msg, err);
        notifyQualified("fatal", top);
        fsm_.gotoFailed(top);
    }

    try {
        //hwConfigure();
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "Could not configure the hardware";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::HardwareProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::enableActionImpl(toolbox::Event::Reference event) {
    // try
    //   {
    //     hwP_->hwEnable();
    //   }
    // catch (PixelMonitor::exception::Exception& err)
    //   {
    //     std::string msgBase = "Could not enable the hardware";
    //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
    //     ERROR(msg);
    //     XCEPT_DECLARE_NESTED(PixelMonitor::exception::HardwareProblem, top, msg, err);
    //     notifyQualified("fatal", top);
    //     fsm_.gotoFailed(top);
    //   }
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::failActionImpl(toolbox::Event::Reference event) {
    // BUG BUG BUG
    // Figure out if we really want to do this.
    // if (hwP_ != 0)
    //   {
    //     // BUG BUG BUG
    //     // This is a mess (and may need a lock, actually).
    //     hwP_->hwRelease();
    //     // BUG BUG BUG end
    //   }
    // BUG BUG BUG end
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::haltActionImpl(toolbox::Event::Reference event) {
    // try
    //   {
    //     hwP_->hwHalt();
    //   }
    // catch (PixelMonitor::exception::Exception& err)
    //   {
    //     std::string msgBase = "Could not halt the hardware";
    //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
    //     ERROR(msg);
    //     XCEPT_DECLARE_NESTED(PixelMonitor::exception::HardwareProblem, top, msg, err);
    //     notifyQualified("fatal", top);
    //     fsm_.gotoFailed(top);
    //   }

    try {
        hwRelease();
    }
    catch (PixelMonitor::exception::Exception &err) {
        std::string msgBase = "Could not release the hardware";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(PixelMonitor::exception::HardwareProblem, top, msg, err);
        notifyQualified("fatal", top);
        fsm_.gotoFailed(top);
    }
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::pauseActionImpl(toolbox::Event::Reference event) {
    // try
    //   {
    //     hwP_->hwPause();
    //   }
    // catch (PixelMonitor::exception::Exception& err)
    //   {
    //     std::string msgBase = "Could not pause the hardware";
    //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
    //     ERROR(msg);
    //     XCEPT_DECLARE_NESTED(PixelMonitor::exception::HardwareProblem, top, msg, err);
    //     notifyQualified("fatal", top);
    //     fsm_.gotoFailed(top);
    //   }
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::resumeActionImpl(toolbox::Event::Reference event) {
    // try
    //   {
    //     hwP_->hwResume();
    //   }
    // catch (PixelMonitor::exception::Exception& err)
    //   {
    //     std::string msgBase = "Could not resume the hardware";
    //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
    //     ERROR(msg);
    //     XCEPT_DECLARE_NESTED(PixelMonitor::exception::HardwareProblem, top, msg, err);
    //     notifyQualified("fatal", top);
    //     fsm_.gotoFailed(top);
    //   }
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::stopActionImpl(toolbox::Event::Reference event) {
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::ttcHardResetActionImpl(toolbox::Event::Reference event) {
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::ttcResyncActionImpl(toolbox::Event::Reference event) {
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::zeroActionImpl(toolbox::Event::Reference event) {
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::hwColdReset() {
    hwColdResetImpl();
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::hwColdResetImpl() {
    // The default does nothing.
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::hwConfigure() {
    hwConfigureImpl();
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::hwConfigureImpl() {
    // Extract the hardware configuration string and parse it into a
    // register-value list.
    std::string const configurationString =
        cfgInfoSpaceP_->getString("hardwareConfigurationStringReceived");
    PixelMonitor::hwlayer::RegDumpConfigurationProcessor cfgProcessor;
    PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec const cfgInfoUser =
        cfgProcessor.parse(configurationString);

    // Load and parse the default configuration file.
    std::string const tmp = cfgInfoSpaceP_->getString("defaultHwConfigurationFilePath");
    std::string const defaultConfigurationFileName = PixelMonitor::utilslayer::expandPathName(tmp);
    std::ifstream inputFile(defaultConfigurationFileName.c_str());
    std::string defaultConfigurationString = "";
    if (!inputFile.is_open() || !inputFile.good()) {
        std::string const msg =
            toolbox::toString("Failed to read default hardware configuration from file '%s'.",
                              defaultConfigurationFileName.c_str());
        XCEPT_RAISE(PixelMonitor::exception::ConfigurationProblem, msg);
    } else {
        inputFile.seekg(0, std::ios::end);
        defaultConfigurationString.resize(inputFile.tellg());
        inputFile.seekg(0, std::ios::beg);
        inputFile.read(&defaultConfigurationString[0], defaultConfigurationString.size());
    }
    inputFile.close();
    cfgInfoSpaceP_->setString("hardwareConfigurationStringDefault", defaultConfigurationString);
    PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec const cfgInfoDefault =
        cfgProcessor.parse(defaultConfigurationString);

    // Merge the default and the user configurations.
    PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec cfgInfo = cfgInfoUser;
    std::vector<std::string> regNamesInUserConfig;
    for (PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = cfgInfoUser.begin();
         i != cfgInfoUser.end();
         ++i) {
        regNamesInUserConfig.push_back(i->first);
    }
    for (PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = cfgInfoDefault.begin();
         i != cfgInfoDefault.end();
         ++i) {
        if (std::find(regNamesInUserConfig.begin(),
                      regNamesInUserConfig.end(),
                      i->first) == regNamesInUserConfig.end()) {
            cfgInfo.push_back(*i);
        }
    }

    // Be explicit about empty 'applied' hardware configuration strings.
    std::string appliedCfgStr = cfgProcessor.compose(cfgInfo);
    if (appliedCfgStr.empty()) {
        appliedCfgStr = "# Empty.";
    }
    cfgInfoSpaceP_->setString("hardwareConfigurationStringApplied", appliedCfgStr);

    //----------

    // Make sure that we're not accessing any registers that we are not
    // allowed to access.

    // Start by building a list of registers that exist in the hardware
    // (or at least in the address table).
    std::vector<std::string> regNamesVec = hwP_->getRegisterNames();
    std::set<std::string> regNamesSet(regNamesVec.begin(), regNamesVec.end());

    // Build a list of registers mentioned in the configuration.
    std::vector<std::string> cfgRegNamesVec;
    cfgRegNamesVec.reserve(cfgInfo.size());
    for (PixelMonitor::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = cfgInfo.begin();
         i != cfgInfo.end();
         ++i) {
        cfgRegNamesVec.push_back(i->first);
    }
    std::set<std::string> const cfgRegNamesSet(cfgRegNamesVec.begin(), cfgRegNamesVec.end());
    PixelMonitor::hwlayer::RegisterInfo::RegInfoVec regInfos = hwP_->getRegisterInfos();

    for (std::set<std::string>::const_iterator i = cfgRegNamesSet.begin();
         i != cfgRegNamesSet.end();
         ++i) {
        // Check if the register that is specified in the configuration
        // actually exists.
        if ((regNamesSet.erase(*i)) < 1) {
            // Find out if this register came from the user-config or
            // from the default config.
            bool const isRegFromUserConfig = (std::find(regNamesInUserConfig.begin(),
                                                        regNamesInUserConfig.end(),
                                                        *i) != regNamesInUserConfig.end());
            std::string tmp = "which came from the default configuration";
            if (isRegFromUserConfig) {
                tmp = "which came from the user-specified configuration";
            }
            std::string const msg =
                toolbox::toString("Register '%s' (%s) does not exist.",
                                  i->c_str(),
                                  tmp.c_str());
            XCEPT_RAISE(PixelMonitor::exception::ValueError, msg);
        }

        // Check if the register that is specified in the configuration
        // is allowed to be configured.
        PixelMonitor::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo;
        regInfo = std::find_if(regInfos.begin(),
                               regInfos.end(),
                               PixelMonitor::hwlayer::RegisterInfo::RegInfoNameMatches(*i));
        // This should never happen, really.
        if (regInfo == regInfos.end()) {
            std::string const msg =
                toolbox::toString("Register '%s' (%s) does not exist "
                                  "(but somehow passed the check on register existence?).",
                                  i->c_str(),
                                  tmp.c_str());
            XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
        }
        if (!isRegisterAllowed(*regInfo)) {
            // Find out if this register came from the user-config or
            // from the default config.
            bool const isRegFromUserConfig = (std::find(regNamesInUserConfig.begin(),
                                                        regNamesInUserConfig.end(),
                                                        *i) != regNamesInUserConfig.end());
            std::string tmp = "which came from the default configuration";
            if (isRegFromUserConfig) {
                tmp = "which came from the user-specified configuration";
            }
            std::string const msg =
                toolbox::toString("Register '%s' (%s) is not allowed to be configured.",
                                  i->c_str(),
                                  tmp.c_str());
            XCEPT_RAISE(PixelMonitor::exception::ValueError, msg);
        }
    }

    //----------

    // Initialize (or reset) the hardware (or at least the functional
    // firmware block) we're connected to.
    hwCfgInitialize();

    // Do the actual configuration.
    // hwP_->writeHardwareConfiguration(cfgInfo);

    // Finalize the configuration.
    hwCfgFinalize();
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::hwCfgInitialize() {
    hwCfgInitializeImpl();
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::hwCfgInitializeImpl() {
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::hwCfgFinalize() {
    hwCfgFinalizeImpl();
}

void
PixelMonitor::utilslayer::XDAQAppWithFSMBase::hwCfgFinalizeImpl() {
}

// This simply forwards the message to the FSM object, since it is
// technically not possible to bind directly to anything but an
// xdaq::Application.
xoap::MessageReference
PixelMonitor::utilslayer::XDAQAppWithFSMBase::changeStateImpl(xoap::MessageReference msg) {
    return fsm_.changeState(msg);
}
