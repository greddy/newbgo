#include "PixelMonitor/utilslayer/WebServer.h"

#include <algorithm>
#include <fstream>
#include <set>
#include <sstream>

#include "log4cplus/logger.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xdaq/Application.h"
#include "xgi/Output.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/utilslayer/LogMacros.h"
#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"
#include "PixelMonitor/utilslayer/WebSpacer.h"
#include "PixelMonitor/utilslayer/WebTable.h"
#include "PixelMonitor/utilslayer/WebTableMultiCol.h"
#include "PixelMonitor/utilslayer/XDAQAppBase.h"
#include "PixelMonitor/utilslayer/XGIMethod.h"

PixelMonitor::utilslayer::WebServer::WebServer(XDAQAppBase *const xdaqApp)
    : xgi::framework::UIManager(xdaqApp),
      TCDSXDAQObject(xdaqApp),
      logger_(xdaqApp->getApplicationLogger()) {
    // Direct binding for the JSON access to the InfoSpace contents.
    PixelMonitor::utilslayer::helper0::deferredbind(xdaqApp, this, &WebServer::jsonUpdate, "update");

    // Binding for the HTML pages, via the XGI framework so we benefit
    // from the HyperDAQ header and footer.
    PixelMonitor::utilslayer::helper1::deferredbind(xdaqApp, this, &WebServer::monitoringWebPage, "Default");
    PixelMonitor::utilslayer::helper1::deferredbind(xdaqApp, this, &WebServer::monitoringWebPage, "monitoring");

    setLayout(&layout_);
}

PixelMonitor::utilslayer::WebServer::~WebServer() {
}

std::string
PixelMonitor::utilslayer::WebServer::getApplicationName() const {
    std::string xmlClassName = getOwnerApplication()->getApplicationDescriptor()->getClassName();
    size_t lastColon = xmlClassName.find_last_of(":");
    std::string appName = xmlClassName;
    if (lastColon != std::string::npos) {
        appName = xmlClassName.substr(lastColon + 1);
    }
    return appName;
}

std::string
PixelMonitor::utilslayer::WebServer::getServiceName() const {
    std::string const res = getOwnerApplication()->getApplicationDescriptor()->getAttribute("service");
    return res;
}

PixelMonitor::utilslayer::WebTab &
PixelMonitor::utilslayer::WebServer::getTab(std::string const &tabName) const {
    std::vector<WebTab *>::const_iterator tab =
        std::find_if(tabs_.begin(), tabs_.end(), WebTab::FindByName(tabName));
    if (tab == tabs_.end()) {
        std::string msg = "No tab with name '" +
                          tabName +
                          "' exists in the webserver.";
        ERROR(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    }
    return *(*tab);
}

bool
PixelMonitor::utilslayer::WebServer::tabExists(std::string const &tabName) const {
    std::vector<WebTab *>::const_iterator tab =
        std::find_if(tabs_.begin(), tabs_.end(), WebTab::FindByName(tabName));
    return (tab != tabs_.end());
}

void
PixelMonitor::utilslayer::WebServer::registerTab(std::string const &name,
                                                 std::string const &description,
                                                 size_t const numColumns) {
    // NOTE: It's not a problem if the requested tab already exists, of
    // course. Less work.
    if (!tabExists(name)) {
        WebTab *tab = new WebTab(name, description, numColumns);
        tabs_.push_back(tab);
    }
}

void
PixelMonitor::utilslayer::WebServer::registerTable(std::string const &name,
                                                   std::string const &description,
                                                   Monitor const &monitor,
                                                   std::string const &itemSetName,
                                                   std::string const &tabName,
                                                   size_t const colSpan) {
    registerWebObject<WebTable, std::string>(name, description, monitor, itemSetName, tabName, colSpan);
}

void
PixelMonitor::utilslayer::WebServer::registerMultiColTable(std::string const &name,
                                                           std::string const &description,
                                                           Monitor const &monitor,
                                                           std::vector<std::string> const &itemSetNameVec,
                                                           std::string const &tabName,
                                                           size_t const colSpan) {
    registerWebObject<WebTableMultiCol, std::vector<std::string> >(name, description, monitor, itemSetNameVec, tabName, colSpan);
}

void
PixelMonitor::utilslayer::WebServer::registerSpacer(std::string const &name,
                                                    std::string const &description,
                                                    Monitor const &monitor,
                                                    std::string const &itemSetName,
                                                    std::string const &tabName,
                                                    size_t const colSpan) {
    registerWebObject<WebSpacer, std::string>(name, description, monitor, itemSetName, tabName, colSpan);
}

void
PixelMonitor::utilslayer::WebServer::jsonUpdate(xgi::Input *const in, xgi::Output *const out)  {
    try {
        jsonUpdateCore(in, out);
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to serve the JSON update. "
                              "Caught an exception: '%s'.",
                              err.what());
        ERROR(msg);
        XCEPT_DECLARE(PixelMonitor::exception::RuntimeProblem, top, msg);
        getOwnerApplication()->notifyQualified("error", top);
    }
}

void
PixelMonitor::utilslayer::WebServer::jsonUpdateCore(xgi::Input *const in, xgi::Output *const out) const {
    // Prepare the actual JSON contents.
    std::vector<WebTab *>::const_iterator iter;
    std::stringstream tmp("");
    for (iter = tabs_.begin(); iter != tabs_.end(); ++iter) {
        std::string const jsonTmp = (*iter)->getJSONString();
        if (!jsonTmp.empty()) {
            if (!tmp.str().empty()) {
                tmp << ",\n";
            }
            tmp << jsonTmp;
        }
    }

    std::string jsonContents = "{\n" + tmp.str() + "\n}";

    // Stuff everything into the output.
    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << jsonContents;
}

void
PixelMonitor::utilslayer::WebServer::monitoringWebPage(xgi::Input *const in, xgi::Output *const out)  {
    xgi::Output tmpOut;
    tmpOut.str("");
    tmpOut.setHTTPResponseHeader(out->getHTTPResponseHeader());
    try {
        monitoringWebPageCore(in, &tmpOut);
        *out << tmpOut.str();
        out->setHTTPResponseHeader(tmpOut.getHTTPResponseHeader());
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to serve the monitoring web page. "
                              "Caught an exception: '%s'.",
                              err.what());
        *out << msg;
        ERROR(msg);
        XCEPT_DECLARE(PixelMonitor::exception::RuntimeProblem, top, msg);
        getOwnerApplication()->notifyQualified("error", top);
    }
}

void
PixelMonitor::utilslayer::WebServer::monitoringWebPageCore(xgi::Input *const in, xgi::Output *const out) const {
    // NOTE: Since we are using the deferred binding for HTTP calls, the
    // XDAQ framework will take care of the HTML skeleton. We only have
    // to fill in the meat.

    // NOSCRIPT handling. Not very nice, but descriptive it is.
    *out << "<noscript>\n"
         << "<div style=\"position: fixed; top: 0px; left: 0px; z-index: 3000;"
         << "height: 100%; width: 100%; background-color: #FFFFFF\">"
         << "<p style=\"margin-left: 10px\">"
         << "Sorry. The TCDS control applications just don't work without JavaScript, "
         << "and JavaScript seems to be disabled."
         << "</p>"
         << "</div>\n"
         << "</noscript>\n";

    // And then this is the real meat.
    printTabs(out);
}

void
PixelMonitor::utilslayer::WebServer::printTabs(xgi::Output *out) const {
    // NOTE: This has been written to work with the new XDAQ12-style
    // HyperDAQ tabs and doT.js.

    // Start with an overall application title.
    // NOTE: Mix in a bit of templating to show the FSM state name in
    // the title.
    // NOTE: Only show the state name if the state name string is
    // defined and not set to 'n/a'.
    std::string const stateNameSrc =
        "[\"Application state\"][\"Application FSM state\"]";
    std::string const hwCode =
        "[\"Application configuration\"][\"uTCA HW connected\"]";

    *out << "<div class=\"tcds-application-title-wrapper\">"
         << "\n";
    std::string tmpServiceName = getServiceName();
    if (!tmpServiceName.empty()) {
        tmpServiceName = " '" + tmpServiceName + "'";
    }
    *out << "<h1 id=\"tcds-application-title\">" << getApplicationName() << tmpServiceName << "</h1>"
         << "\n";
    *out << "<script type=\"text/x-dot-template\">"
         << "<span style=\"display: none;\" id=\"pixelmonitor-application-subtitle\">"
         << "{{=it" << hwCode << "}}</span>"
         << "\n"
         << "{{? it" << stateNameSrc << " != 'n/a'}}"
         << "<h2 id=\"tcds-application-subtitle\">"
         << "({{=it" << stateNameSrc << "}})</h2>"
         << "\n"
         << "{{?}}"
         << "</script>"
         << "\n"
         << "<div class=\"target\"></div>";
    *out << "</div>"
         << "\n";

    // Now plug in the main tab wrapper.
    *out << "<div class=\"xdaq-tab-wrapper\">";
    *out << "\n";

    for (std::vector<WebTab *>::const_iterator tab = tabs_.begin();
         tab != tabs_.end();
         ++tab) {
        *out << "<div class=\"xdaq-tab\" "
             << "title=\"" << (*tab)->getName() << "\">"
             << "\n"
             << (*tab)->getHTMLString()
             << "\n"
             << "</div>"
             << "\n";
    }

    *out << "</div>";
    *out << "\n";
}
