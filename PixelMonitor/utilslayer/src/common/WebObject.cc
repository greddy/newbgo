#include "PixelMonitor/utilslayer/WebObject.h"

#include <sstream>

#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"

std::string const PixelMonitor::utilslayer::WebObject::kStringInvalidItem("-");
std::string const PixelMonitor::utilslayer::WebObject::kStringUnknownItem("?");

PixelMonitor::utilslayer::WebObject::WebObject(std::string const &name,
                                               std::string const &description,
                                               Monitor const &monitor,
                                               std::string const &itemSetName,
                                               std::string const &tabName,
                                               size_t const colSpan)
    : name_(name),
      description_(description),
      monitor_(monitor),
      itemSetName_(itemSetName),
      tabName_(tabName),
      colSpan_(colSpan) {
}

PixelMonitor::utilslayer::WebObject::WebObject(std::string const &name,
                                               std::string const &description,
                                               Monitor const &monitor,
                                               std::vector<std::string> const &itemSetNameVec,
                                               std::string const &tabName,
                                               size_t const colSpan)
    : name_(name),
      description_(description),
      monitor_(monitor),
      itemSetNameVec_(itemSetNameVec),
      tabName_(tabName),
      colSpan_(colSpan) {
}

PixelMonitor::utilslayer::WebObject::~WebObject() {
}

std::string
PixelMonitor::utilslayer::WebObject::getName() const {
    return name_;
}

std::string
PixelMonitor::utilslayer::WebObject::getDescription() const {
    return description_;
}

std::string
PixelMonitor::utilslayer::WebObject::getItemSetName() const {
    if (!itemSetName_.empty()) {
        return itemSetName_;
    } else {
        std::string const res = "Not available, try ::getItemSetNameVec()";
        return res;
    }
}

//std::vector<std:string>
//PixelMonitor::utilslayer::WebObject::getItemSetNameVec() const
//{
//  std::vector<std::string> res = itemSetNameVec_;
//  if (!res.empty())
//  {
//    return res;
//  }
//  else
//  {
//    std::cout<<"Not available, try ::getItemSetName()"<<std::endl;
//    return res;
//  }
//}

std::string
PixelMonitor::utilslayer::WebObject::getTabName() const {
    return tabName_;
}

size_t
PixelMonitor::utilslayer::WebObject::getColSpan() const {
    return colSpan_;
}

std::string
PixelMonitor::utilslayer::WebObject::getJSONStringFromItemSetName(std::string const &isName) const {
    Monitor::StringPairVector items = monitor_.getFormattedItemSet(isName);
    Monitor::StringPairVector::const_iterator iter;

    std::stringstream tmp("");
    for (iter = items.begin(); iter != items.end(); ++iter) {
        std::string name = escapeAsJSONString(iter->first);
        std::string value = iter->second;
        if (!tmp.str().empty()) {
            tmp << ",\n";
        }
        tmp << name << ": " << value;
    }

    std::string res =
        escapeAsJSONString(isName) +
        ": {\n" +
        tmp.str() +
        "\n}";

    return res;
}

std::string
PixelMonitor::utilslayer::WebObject::getJSONString() const {
    if (!itemSetName_.empty()) {
        return getJSONStringFromItemSetName(itemSetName_);
    }
    if (!itemSetNameVec_.empty()) {
        std::stringstream tmp("");
        std::vector<std::string>::const_iterator iter;
        for (iter = itemSetNameVec_.begin();
             iter != itemSetNameVec_.end();
             ++iter) {
            if (!tmp.str().empty()) {
                tmp << ",\n";
            }
            tmp << getJSONStringFromItemSetName(*iter);
        }
        std::string res = "\n" + tmp.str() + "\n";
        return res;
    }
    return "None.";
}
