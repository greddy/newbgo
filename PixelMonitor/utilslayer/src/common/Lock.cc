#include "PixelMonitor/utilslayer/Lock.h"

PixelMonitor::utilslayer::Lock::Lock(toolbox::BSem::State state, bool recursive)
    : semaphore_(state, recursive) {
}

PixelMonitor::utilslayer::Lock::~Lock() {
    unlock();
}

void
PixelMonitor::utilslayer::Lock::lock() {
    semaphore_.take();
}

void
PixelMonitor::utilslayer::Lock::unlock() {
    semaphore_.give();
}
