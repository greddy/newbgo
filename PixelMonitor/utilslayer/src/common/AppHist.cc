#include "PixelMonitor/utilslayer/AppHist.h"

#include <sstream>

#include "toolbox/string.h"

#include "PixelMonitor/utilslayer/Utils.h"

PixelMonitor::utilslayer::AppHist::AppHist() {
}

PixelMonitor::utilslayer::AppHist::~AppHist() {
}

void
PixelMonitor::utilslayer::AppHist::addItem(std::string const &msg) {
    // Keep track of the underlying history items first.
    if ((history_.size() > 0) && (history_.back().message() == msg)) {
        history_.back().increaseCount();
    } else {
        history_.push_back(AppHistItem(msg));
    }

    // Make sure things don't grow out of hand.
    while (history_.size() > kMaxHistSize) {
        history_.pop_front();
    }
}

std::string
PixelMonitor::utilslayer::AppHist::getJSONString() const {
    std::stringstream res;

    res << "[";
    size_t index = 0;
    for (std::deque<PixelMonitor::utilslayer::AppHistItem>::const_iterator it = history_.begin();
         it != history_.end();
         ++it) {
        res << "{";

        // The timestamp.
        res << "\"Timestamp\": \"" << PixelMonitor::utilslayer::formatTimestamp(it->timestamp()) << "\"";
        res << ", ";

        // The message itself.
        std::string msg = it->message();
        if (it->count() > 1) {
            unsigned int repeatCount = it->count() - 1;
            if (repeatCount == 1) {
                msg.append(" (repeated 1 time)");
            } else {
                msg.append(toolbox::toString(" (repeated %d times)", repeatCount));
            }
        }
        res << "\"Message\": \"" << msg << "\"";

        res << "}";

        if (index != (history_.size() - 1)) {
            res << ", ";
        }
        ++index;
    }
    res << "]";

    return res.str();
}
