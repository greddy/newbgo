#include "PixelMonitor/utilslayer/Utils.h"

#include <algorithm>
#include <cassert>
#include <cctype>
#include <iomanip>
#include <sstream>
#include <zconf.h>
#include <zlib.h>
#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>

#include "toolbox/exception/Exception.h"
#include "toolbox/Runtime.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "PixelMonitor/exception/Exception.h"

std::string
PixelMonitor::utilslayer::expandPathName(std::string const &pathNameIn) {
    std::vector<std::string> expandedPaths;
    try {
        expandedPaths = toolbox::getRuntime()->expandPathName(pathNameIn);
    }
    catch (toolbox::exception::Exception &err) {
        std::string const msgBase = "Cannot expand filename";
        XCEPT_RAISE(PixelMonitor::exception::ConfigurationProblem,
                    toolbox::toString("%s %s.", msgBase.c_str(), pathNameIn.c_str()));
    }

    if (expandedPaths.size() != 1) {
        XCEPT_RAISE(PixelMonitor::exception::ConfigurationProblem,
                    toolbox::toString("Expanding %s leads to ambiguities.",
                                      pathNameIn.c_str()));
    }

    return expandedPaths.at(0);
}

/**
 * Escape a string such that it's safe to embed as string in JSON
 * format. Takes care of slashes, backslashes, double quotes, and
 * special characters like tabs and line feeds.
 */
std::string
PixelMonitor::utilslayer::escapeAsJSONString(std::string const &stringIn) {
    return "\"" + toolbox::jsonquote(stringIn) + "\"";
}

std::string
PixelMonitor::utilslayer::jsquote(std::string const &stringIn) {
    // NOTE: This is basically the same method as toolbox::jsonquote(),
    // but with the single quote added to the list of characters to be
    // escaped.
    std::string result;
    size_t len = stringIn.length();
    for (size_t i = 0; i < len; i++) {
        char c = stringIn[i];
        switch (c) {
        case '\'':
            result += "\\\'";
            break;
        case '\"':
            result += "\\\"";
            break;
        case '\\':
            result += "\\\\";
            break;
        case '\b':
            result += "\\b";
            break;
        case '\f':
            result += "\\f";
            break;
        case '\n':
            result += "\\n";
            break;
        case '\r':
            result += "\\r";
            break;
        case '\t':
            result += "\\t";
            break;
        default:
            if (c > 0 && c <= 0x1F) // is a control character
            {
                std::ostringstream oss;
                oss << "\\u" << std::hex << std::uppercase << std::setfill('0') << std::setw(4) << static_cast<int>(c);
                result += oss.str();
            } else {
                result += c;
            }
            break;
        }
    }
    return result;
}

std::string
PixelMonitor::utilslayer::trimString(std::string const &stringToTrim,
                                     std::string const &whitespace) {
    size_t const strBegin = stringToTrim.find_first_not_of(whitespace);
    if (strBegin == std::string::npos) {
        // No content at all in this string.
        return "";
    }

    size_t const strEnd = stringToTrim.find_last_not_of(whitespace);
    size_t const strRange = strEnd - strBegin + 1;

    return stringToTrim.substr(strBegin, strRange);
}

std::string
PixelMonitor::utilslayer::capitalizeString(std::string const &stringIn) {
    std::string res = stringIn;
    if (!stringIn.empty()) {
        std::transform(res.begin(), res.end(), res.begin(), convertDown());
        res[0] = convertUp()(res[0]);
    }
    return res;
}

std::string
PixelMonitor::utilslayer::formatTimestamp(toolbox::TimeVal const timestamp) {
    return toolbox::TimeVal(timestamp).toString("%Y-%m-%d %H:%M:%S UTC",
                                                toolbox::TimeVal::gmt);
}

std::string
PixelMonitor::utilslayer::formatDeltaTString(toolbox::TimeVal const timeBegin,
                                             toolbox::TimeVal const timeEnd) {
    std::stringstream result;
    toolbox::TimeVal deltaT = timeEnd - timeBegin;
    if (deltaT.sec() != 0) {
        result << deltaT.sec() << " second";
        if (deltaT.sec() > 1) {
            result << "s";
        }
    }
    if (deltaT.millisec() != 0) {
        if (result.str().size() != 0) {
            result << " and ";
        }
        result << deltaT.millisec() << " millisecond";
        if (deltaT.millisec() > 1) {
            result << "s";
        }
    }
    if (result.str().size() == 0) {
        result << "negligible time";
    }
    return result.str();
}

std::string
PixelMonitor::utilslayer::compressString(std::string const stringIn) {
    size_t srcSize = stringIn.size();
    Bytef const *src = reinterpret_cast<Bytef const *>(stringIn.c_str());
    // NOTE: The magic number below is a rounded-up result of reading
    // the zlib docs (for compress()).
    size_t dstSize = srcSize + (.1 * srcSize) + 16;
    Bytef *dst = reinterpret_cast<Bytef *>(new char[dstSize]);

    z_stream zStream;
    // Not pretty, but necessary here.
    zStream.next_in = const_cast<Bytef *>(src);
    zStream.avail_in = srcSize;
    zStream.next_out = dst;
    zStream.avail_out = dstSize;
    zStream.zalloc = Z_NULL;
    zStream.zfree = Z_NULL;
    zStream.opaque = Z_NULL;

    // NOTE: The '8' below is the value of DEF_MEM_LEVEL, the default
    // memory-usage level. This is defined in zutil.h, which is not
    // installed (for whatever reason) by the zlib-devel RPM.
    // NOTE: The '+ 16' changes the behaviour from zlib to gzip format.
    int res = deflateInit2(&zStream,
                           Z_DEFAULT_COMPRESSION,
                           Z_DEFLATED,
                           MAX_WBITS + 16,
                           8,
                           Z_DEFAULT_STRATEGY);

    if (res != Z_OK) {
        std::string const msg = "Failed to initialize zlib.";
        XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg.c_str());
    } else {
        res = deflate(&zStream, Z_FINISH);
        if ((res != Z_STREAM_END) && (res != Z_OK)) {
            deflateEnd(&zStream);
            std::string const msg = "Failed to compress string using zlib "
                                    "(looks like a buffer size problem).";
            XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg.c_str());
        }
        dstSize = zStream.total_out;
        res = deflateEnd(&zStream);
        if (res != Z_OK) {
            std::string const msg = "Failed to compress string using zlib.";
            XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg.c_str());
        }
    }

    // Turn the result back into an std::string.
    std::string stringOut(reinterpret_cast<char *>(dst), dstSize);
    delete[] dst;
    return stringOut;
}

unsigned int
PixelMonitor::utilslayer::countTrailingZeros(uint32_t const val) {
    unsigned int numZeros = 0;
    uint32_t tmp = val;
    while ((tmp & 0x1) == 0) {
        numZeros += 1;
        tmp >>= 1;
    }
    return numZeros;
}

void
PixelMonitor::utilslayer::HwDeviceConnectImpl(PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler const &cfgInfoSpace,
                                              PixelMonitor::hwlayer::HwDeviceTCA &hw) {
    //std::string const boardId = cfgInfoSpace.getString("connectionName");
    //std::string const uri = cfgInfoSpace.getString("connectionURI");
    //std::string const addressTable = cfgInfoSpace.getString("addressTable");
    //hw.hwConnect(boardId, uri, addressTable);
    std::string const boardId = cfgInfoSpace.getString("connectionName");
    std::string const connectionsFile = cfgInfoSpace.getString("connectionsFileName");
    hw.hwConnect(connectionsFile, boardId);
}

std::string
PixelMonitor::utilslayer::exec(const char *cmd) {
    char buffer[128];
    std::string result = "";
    FILE *pipe = popen(cmd, "r");
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
        XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, "popen() failed!");
    }
    try {
        while (!feof(pipe)) {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    }
    catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}
