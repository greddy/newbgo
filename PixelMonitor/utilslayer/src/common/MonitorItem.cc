#include "PixelMonitor/utilslayer/MonitorItem.h"

PixelMonitor::utilslayer::MonitorItem::MonitorItem(std::string const &name,
                                                   std::string const &description,
                                                   InfoSpaceHandler *const infoSpaceHandler,
                                                   std::string const &docString)
    : name_(name),
      description_(description),
      infoSpaceHandlerP_(infoSpaceHandler),
      docString_(docString) {
}

std::string
PixelMonitor::utilslayer::MonitorItem::getName() const {
    return name_;
}

std::string
PixelMonitor::utilslayer::MonitorItem::getDescription() const {
    return description_;
}

PixelMonitor::utilslayer::InfoSpaceHandler *
PixelMonitor::utilslayer::MonitorItem::getInfoSpaceHandler() const {
    return infoSpaceHandlerP_;
}

std::string
PixelMonitor::utilslayer::MonitorItem::getDocString() const {
    return docString_;
}
