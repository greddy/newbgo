#include "PixelMonitor/utilslayer/Monitor.h"

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <time.h>
#include <typeinfo>

#include "log4cplus/logger.h"

#include "sentinel/utils/Alarm.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/string.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"
#include "xdata/Event.h"
#include "xdata/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/InfoSpaceFactory.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/utilslayer/ConfigurationInfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/InfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/Lock.h"
#include "PixelMonitor/utilslayer/LockGuard.h"
#include "PixelMonitor/utilslayer/LogMacros.h"
#include "PixelMonitor/utilslayer/XDAQAppBase.h"

PixelMonitor::utilslayer::Monitor::Monitor(XDAQAppBase *const xdaqApp, std::string const &name)
    : TCDSXDAQObject(xdaqApp),
      logger_(xdaqApp->getApplicationLogger()),
      alarmName_(toolbox::toString("MonitoringAlarm_lid%d",
                                   getOwnerApplication()->getApplicationDescriptor()->getLocalId())),
      timerP_(0),
      timeStart_(toolbox::TimeVal::gettimeofday()) {
    std::string timerName;
    if (name.empty()) {
        timerName = toolbox::toString("MonitoringTimer_lid%d",
                                      getOwnerApplication()->getApplicationDescriptor()->getLocalId());
    } else {
        timerName = toolbox::toString("%s_lid%d",
                                      name.c_str(),
                                      getOwnerApplication()->getApplicationDescriptor()->getLocalId());
    }
    timerP_ = toolbox::task::getTimerFactory()->createTimer(timerName);
    timerP_->addExceptionListener(this);
}

PixelMonitor::utilslayer::Monitor::~Monitor() {
}

void
PixelMonitor::utilslayer::Monitor::startMonitoring() {
    std::string const tmp = getOwnerApplication()->cfgInfoSpaceP_->getString("monitoringInterval");
    toolbox::TimeInterval monitoringInterval;
    try {
        monitoringInterval.fromString(tmp);
    }
    catch (toolbox::exception::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to start monitoring thread. "
                              "Failed to interpret '%s' as update interval.",
                              tmp.c_str());
        raiseMonitoringProblem(msg);
        return;
    }

    // Check for zero-intervals. These imply no monitoring updates at
    // all.
    if (monitoringInterval != toolbox::TimeInterval(0)) {
        // Just add a little bit of a random delay, so not all
        // monitoring applications run in-sync and create bandwidth
        // issues.
        // NOTE: This is especially important for the connections to the
        // hardware. At the hardware level we're often dealing with a
        // single, physical bus (e.g., the I2C bus on the uTCA carrier
        // boards) that just cannot be accessed by more than a single
        // entity at any point in time.
        float const delayRnd = (1. * rand() / RAND_MAX) * double(monitoringInterval);
        float const delaySec = std::floor(delayRnd);
        float const delayUsec = (delayRnd - delaySec) * 1000000;
        toolbox::TimeInterval const delay(delaySec, delayUsec);
        toolbox::TimeVal start = toolbox::TimeVal::gettimeofday() + delay;

        try {
            // This schedules periodic calls to
            // PixelMonitor::utilslayer::Monitor::timeExpired().
            // NOTE: The way the XDAQ toolbox::task::Timer handles this,
            // it always waits for the callback to finish before
            // rescheduling. This is the right thing to do for periodic
            // regreshes, in case the update takes longer than the
            // update interval.
            timerP_->scheduleAtFixedRate(start, this, monitoringInterval, 0, "");
        }
        catch (xcept::Exception &err) {
            std::string const msg =
                toolbox::toString("Failed to activate the monitoring timer: '%s'.",
                                  err.what());
            raiseMonitoringProblem(msg);
        }
    }
}

PixelMonitor::utilslayer::Monitor::StringPairVector
PixelMonitor::utilslayer::Monitor::getFormattedItemSet(std::string const &itemSetName) const {
    PixelMonitor::utilslayer::Monitor::StringPairVector res;

    // Get the requested ItemSet and complain if it does not exist.
    MonitorItemMap::const_iterator itemSet = itemSets_.find(itemSetName);
    if (itemSet == itemSets_.end()) {
        std::string msg = "No ItemSet with name '" +
                          itemSetName +
                          "' exists in the monitor.";
        ERROR(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    }

    std::vector<std::pair<std::string, MonitorItem> > items = itemSet->second;
    std::vector<std::pair<std::string, MonitorItem> >::const_iterator iter;
    for (iter = items.begin(); iter != items.end(); ++iter) {
        MonitorItem const item = iter->second;
        std::string const desc = item.getDescription();
        std::string value = "";
        try {
            value = item.getInfoSpaceHandler()->getFormatted(item.getName());
            //std::cout<<desc<<" : "<<value<<std::endl;
        }
        catch (PixelMonitor::exception::Exception &err) {
            // BUG BUG BUG
            // This should really disappear once we have a
            // 'InfoSpaceChecker' implemented.
            value = "\"!!! monitoring error !!!\"";
            // BUG BUG BUG end
            std::string const msg =
                toolbox::toString("Failed to format monitoring item '%s': %s.",
                                  item.getName().c_str(),
                                  err.what());
            raiseMonitoringProblem(msg);
        }
        res.push_back(std::make_pair<std::string, std::string>(desc, value));
    }

    return res;
}

PixelMonitor::utilslayer::Monitor::StringPairVector
PixelMonitor::utilslayer::Monitor::getItemSetDocStrings(std::string const &itemSetName) const {
    PixelMonitor::utilslayer::Monitor::StringPairVector res;

    // Get the requested ItemSet and complain if it does not exist.
    MonitorItemMap::const_iterator itemSet = itemSets_.find(itemSetName);
    if (itemSet == itemSets_.end()) {
        std::string msg = "No ItemSet with name '" +
                          itemSetName +
                          "' exists in the monitor.";
        ERROR(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    }

    std::vector<std::pair<std::string, MonitorItem> > items = itemSet->second;
    std::vector<std::pair<std::string, MonitorItem> >::const_iterator iter;
    for (iter = items.begin(); iter != items.end(); ++iter) {
        MonitorItem const item = iter->second;
        std::string const desc = item.getDescription();
        std::string const docString = item.getDocString();
        res.push_back(std::make_pair<std::string, std::string>(desc, docString));
    }

    return res;
}

void
PixelMonitor::utilslayer::Monitor::onException(xcept::Exception &err) {
    handleException(err);
}

void
PixelMonitor::utilslayer::Monitor::handleException(xcept::Exception &err) {
    // TODO TODO TODO
    // Should have this do the same thing with the alarm etc. as
    // 'normal' exceptions do.
    // TODO TODO TODO end
}

void
PixelMonitor::utilslayer::Monitor::addInfoSpace(InfoSpaceHandler *const infoSpace) {
    std::string name = infoSpace->name();
    std::tr1::unordered_map<std::string, InfoSpaceHandler *>::iterator iter =
        infoSpaceMap_.find(name);
    if (iter == infoSpaceMap_.end()) {
        DEBUG("Adding InfoSpace '"
              << name
              << "' to monitoring list.");
        infoSpaceMap_.insert(std::make_pair<std::string, InfoSpaceHandler *>(name, infoSpace));
    }
}

void
PixelMonitor::utilslayer::Monitor::addItem(std::string const &itemSetName,
                                           std::string const &itemName,
                                           std::string const &itemDesc,
                                           InfoSpaceHandler *const infoSpaceHandler,
                                           std::string const &docString) {
    // Find the ItemSet into which this item should go.
    typedef std::tr1::unordered_map<std::string, std::vector<std::pair<std::string, MonitorItem> > >::iterator SetListIter;
    SetListIter set = itemSets_.find(itemSetName);
    if (set == itemSets_.end()) {
        std::string msg = "ItemSet with name '" +
                          itemSetName +
                          "' does not exist in the monitor.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    }

    MonitorItem item(itemName, itemDesc, infoSpaceHandler, docString);
    (*set).second.push_back(std::make_pair<std::string, MonitorItem>(itemName, item));

    addInfoSpace(infoSpaceHandler);
}

void
PixelMonitor::utilslayer::Monitor::newItemSet(std::string const &itemSetName) {
    if (itemSets_.find(itemSetName) != itemSets_.end()) {
        std::string msg = "ItemSet with name '" +
                          itemSetName +
                          "' already exists in the monitor.";
        FATAL(msg);
        XCEPT_RAISE(PixelMonitor::exception::SoftwareProblem, msg);
    }

    std::vector<std::pair<std::string, MonitorItem> > emptySet;
    itemSets_.insert(std::make_pair<std::string, std::vector<std::pair<std::string, MonitorItem> > >(itemSetName, emptySet));
}

void
PixelMonitor::utilslayer::Monitor::timeExpired(toolbox::task::TimerEvent &event) {
    updateAllInfoSpaces();
}

void
PixelMonitor::utilslayer::Monitor::updateAllInfoSpaces() {
    // Keep track of how long this takes.
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    // NOTE: This is extremely coarse locking. Any monitoring update
    // blocks anything else that depends on this lock.
    LockGuard<Lock> guardedLock(getOwnerApplication()->monitoringLock_);

    bool success = true;
    std::string msg = "";
    std::string const msgBase = "Problem updating monitoring information";

    std::tr1::unordered_map<std::string, InfoSpaceHandler *>::iterator iter;
    for (iter = infoSpaceMap_.begin(); iter != infoSpaceMap_.end(); ++iter) {
        try {
            iter->second->update();
        }
        catch (xcept::Exception &err) {
            // Log the problem.
            std::string const msgTmp =
                toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
            ERROR(msgTmp);
            if (!msg.size()) {
                msg = msgBase + ": ";
            } else {
                msg += "\n";
            }
            msg += std::string("'") + err.what() + "'";
            success = false;
        }
        catch (std::exception const &err) {
            // Log the problem.
            std::string const msgTmp =
                toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
            ERROR(msgTmp);
            if (!msg.size()) {
                msg = msgBase + ": ";
            } else {
                msg += "\n";
            }
            msg += std::string("'") + err.what() + "'";
            success = false;
        }
    }

    if (success) {
        revokeMonitoringProblem();
    } else {
        std::string const tmp =
            toolbox::toString("Failed to update monitoring information: '%s'", msg.c_str());
        raiseMonitoringProblem(tmp);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();

    // Update the uptime estimate and the timestamp of the latest
    // monitoring update.
    toolbox::TimeVal timeNow(toolbox::TimeVal::gettimeofday());
    toolbox::TimeInterval upTime = timeNow - timeStart_;
    getOwnerApplication()->appStateInfoSpace_.setDouble("upTime",
                                                        double(upTime),
                                                        true);
    getOwnerApplication()->appStateInfoSpace_.setTimeVal("latestMonitoringUpdate",
                                                         double(timeNow),
                                                         true);
    getOwnerApplication()->appStateInfoSpace_.setDouble("latestMonitoringDuration",
                                                        double(timeEnd - timeBegin),
                                                        true);
}

xdata::InfoSpace *
PixelMonitor::utilslayer::Monitor::getMonitoringInfoSpace() const {
    // Get the sentinel alarms InfoSpace.
    // NOTE: If for some reason we can't find this, there is nothing
    // left to do but log an error and notify the user.
    xdata::InfoSpace *is = 0;
    try {
        is = xdata::getInfoSpaceFactory()->get("urn:xdaq-sentinel:alarms");
    }
    catch (xdata::exception::Exception &err) {
        std::string const msg =
            toolbox::toString("Could not find the sentinel alarms InfoSpace: '%s'.",
                              err.what());
        ERROR(msg);
        getOwnerApplication()->notifyQualified("error", err);
    }
    return is;
}

sentinel::utils::Alarm *
PixelMonitor::utilslayer::Monitor::getMonitoringAlarm() const {
    sentinel::utils::Alarm *alarm = 0;
    xdata::InfoSpace *is = getMonitoringInfoSpace();
    if (is != 0) {
        try {
            alarm = dynamic_cast<sentinel::utils::Alarm *>(is->find(alarmName_));
        }
        catch (xdata::exception::Exception &err) {
            // Nothing to be done. Just means that no alarm has been
            // raised so far.
        }
    }

    return alarm;
}

void
PixelMonitor::utilslayer::Monitor::raiseMonitoringProblem(std::string const &problemDesc) const {
    XCEPT_DECLARE(PixelMonitor::exception::MonitoringFailureAlarm, alarmException, problemDesc);
    getOwnerApplication()->raiseAlarm(alarmName_, "error", alarmException);
    getOwnerApplication()->appStateInfoSpace_.addMonitoringProblem(problemDesc);

    // Log the problem.
    ERROR(problemDesc);

    // Send a notification.
    XCEPT_DECLARE(PixelMonitor::exception::MonitoringProblem, err, problemDesc);
    getOwnerApplication()->notifyQualified("error", err);
}

void
PixelMonitor::utilslayer::Monitor::revokeMonitoringProblem() const {
    //getOwnerApplication()->revokeAlarm(alarmName_);
    getOwnerApplication()->appStateInfoSpace_.removeMonitoringProblem();
}
