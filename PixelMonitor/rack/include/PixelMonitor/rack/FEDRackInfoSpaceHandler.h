#ifndef _PixelMonitor_rack_FEDRackInfoSpaceHandler_h_
#define _PixelMonitor_rack_FEDRackInfoSpaceHandler_h_

#include <string>

#include "PixelMonitor/utilslayer/InfoSpaceHandler.h"

namespace xdaq {
class Application;
}

namespace PixelMonitor {
namespace utilslayer {
class InfoSpaceHandler;
class Monitor;
class WebServer;
}

namespace rack {
class FEDRackInfoSpaceHandler : public PixelMonitor::utilslayer::InfoSpaceHandler {
  public:
    FEDRackInfoSpaceHandler(xdaq::Application *xdaqApp,
                            PixelMonitor::utilslayer::InfoSpaceUpdater *updater);
    virtual ~FEDRackInfoSpaceHandler();

  private:
    virtual void registerItemSetsWithMonitor(PixelMonitor::utilslayer::Monitor &monitor);
    virtual void registerItemSetsWithWebServer(PixelMonitor::utilslayer::WebServer &webServer,
                                               PixelMonitor::utilslayer::Monitor &monitor);
    virtual std::string formatItem(PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec::const_iterator const &item) const;
};

} // namespace rack
} // namespace PixelMonitor

#endif // _PixelMonitor_rack_FEDRackInfoSpaceHandler_h_
