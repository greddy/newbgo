#ifndef _PixelMonitor_rack_WebTableMaskInfo_h
#define _PixelMonitor_rack_WebTableMaskInfo_h

#include <cstddef>
#include <string>

#include "PixelMonitor/utilslayer/WebObject.h"

namespace PixelMonitor {
namespace utilslayer {
class Monitor;
}

namespace rack {

class WebTableMaskInfo : public PixelMonitor::utilslayer::WebObject {
  public:
    WebTableMaskInfo(std::string const &name,
                     std::string const &description,
                     PixelMonitor::utilslayer::Monitor const &monitor,
                     std::string const &itemSetName,
                     std::string const &tabName,
                     size_t const colSpan);

    std::string getHTMLString() const;
};

} // namespace rack
} // namespace PixelMonitor

#endif // _PixelMonitor_rack_WebTableMaskInfo_h
