#ifndef _PixelMonitor_rack_FEDRackConfigurationInfoSpaceHandler_h
#define _PixelMonitor_rack_FEDRackConfigurationInfoSpaceHandler_h

#include <vector>

#include "xdaq/ContextDescriptor.h"
#include "PixelMonitor/utilslayer/ConfigurationInfoSpaceHandler.h"

namespace xdaq {
class Application;
}

namespace PixelMonitor {
namespace utilslayer {
class Monitor;
class WebServer;
}

namespace rack {
class FEDRackConfigurationInfoSpaceHandler : public PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler {
  public:
    FEDRackConfigurationInfoSpaceHandler(xdaq::Application *const xdaqApp);

  private:
    virtual void registerItemSetsWithMonitor(PixelMonitor::utilslayer::Monitor &monitor);
    std::vector<xdaq::ContextDescriptor *> contextVec_;
    //virtual void registerItemSetsWithWebServer(PixelMonitor::utilslayer::WebServer& webServer,
    //                                           PixelMonitor::utilslayer::Monitor& monitor);
};
} // namespace rack
} // namespace PixelMonitor

#endif //_PixelMonitor_rack_FEDRackConfigurationInfoSpaceHandler_h
