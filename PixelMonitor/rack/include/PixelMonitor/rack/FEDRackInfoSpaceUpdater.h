#ifndef _PixelMonitor_rack_FEDRackInfoSpaceUpdater_h
#define _PixelMonitor_rack_FEDRackInfoSpaceUpdater_h

#include <string>
#include <map>

#include "PixelMonitor/utilslayer/InfoSpaceUpdater.h"

namespace PixelMonitor {
namespace utilslayer {
class InfoSpaceHandler;
class InfoSpaceItem;
}

namespace rack {
class FEDRack;
class FEDRackInfoSpaceUpdater : public PixelMonitor::utilslayer::InfoSpaceUpdater {
  public:
    FEDRackInfoSpaceUpdater(PixelMonitor::rack::FEDRack &xdaqApp);
    virtual ~FEDRackInfoSpaceUpdater();

    virtual bool updateInfoSpaceItem(PixelMonitor::utilslayer::InfoSpaceItem &item,
                                     PixelMonitor::utilslayer::InfoSpaceHandler *const infoSpaceHandler);

  private:
    std::string getFlInfo() const;
    std::string getMaskInfo();
    PixelMonitor::rack::FEDRack &xdaqApp_;
    std::map<std::pair<int, int>, std::string> fedCh2Roc_;
};
} // namespace rack
} // namespace PixelMonitor

#endif // _PixelMonitor_rack_FEDRackInfoSpaceUpdater_h
