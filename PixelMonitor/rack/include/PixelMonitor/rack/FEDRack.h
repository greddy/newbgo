#ifndef _PixelMonitor_rack_FEDRack_h_
#define _PixelMonitor_rack_FEDRack_h_

#include <string>
#include <vector>
#include "toolbox/task/WorkLoop.h"
#include "xoap/exception/Exception.h"
#include "xoap/MessageReference.h"

#include "PixelMonitor/utilslayer/XDAQAppBase.h"

namespace xdaq {
class ApplicationStub;
}

namespace xgi {
class Input;
class Output;
}

namespace PixelMonitor {
namespace rack {
class FEDRackInfoSpaceHandler;
class FEDRackInfoSpaceUpdater;

class FEDRack : public PixelMonitor::utilslayer::XDAQAppBase {
  public:
    XDAQ_INSTANTIATOR();

    FEDRack(xdaq::ApplicationStub *const stub);
    virtual ~FEDRack();

  protected:
    virtual void setupInfoSpaces();
    xoap::MessageReference enableAll(xoap::MessageReference msg) throw(xoap::exception::Exception);
    xoap::MessageReference disableAll(xoap::MessageReference msg) throw(xoap::exception::Exception);

    virtual void hwConnectImpl();
    virtual void hwReleaseImpl();
    virtual void hwConfigureImpl();

  private:
    bool enableAllCore(toolbox::task::WorkLoop *wl);
    bool disableAllCore(toolbox::task::WorkLoop *wl);
    std::unique_ptr<PixelMonitor::rack::FEDRackInfoSpaceHandler> rackHandlerP_;
    std::unique_ptr<PixelMonitor::rack::FEDRackInfoSpaceUpdater> rackUpdaterP_;

    std::string targetURLs_;
};

} // namespace rack
} // namespace PixelMonitor

#endif //_PixelMonitor_rack_FEDRack_h_
