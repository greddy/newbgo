#ifndef _PixelMonitor_rack_Utils_h
#define _PixelMonitor_rack_Utils_h

#include <string>
#include "xdata/Table.h"

namespace xdaq {
class ApplicationDescriptor;
}

namespace PixelMonitor {
namespace rack {

// Helper to sort XDAQ applications by host, port, localId
class compareApplications {
  public:
    bool operator()(xdaq::ApplicationDescriptor const *const &lhs,
                    xdaq::ApplicationDescriptor const *const &rhs) const;
};

xdata::Table getFlashList(std::string const &lasURL,
                          std::string const &lasServiceName,
                          std::string const &flashListName);
//xdata::Table getFlashList(std::string const& url);

} //namespace rack
} //namespace PixelMonitor

#endif //_PixelMonitor_rack_Utils_h
