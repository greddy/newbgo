#include "PixelMonitor/rack/FEDRackInfoSpaceHandler.h"

#include "PixelMonitor/rack/WebTableFlInfo.h"
#include "PixelMonitor/rack/WebTableMaskInfo.h"
#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"
#include "PixelMonitor/utilslayer/WebServer.h"

PixelMonitor::rack::FEDRackInfoSpaceHandler::FEDRackInfoSpaceHandler(xdaq::Application *xdaqApp,
                                                                     PixelMonitor::utilslayer::InfoSpaceUpdater *updater)
    : InfoSpaceHandler(xdaqApp, "pixel-rack-monitor", updater) {
    // hold flashlist information
    createString("fl_info", "", "json-object");
    createString("mask_info", "", "json-object");
}

PixelMonitor::rack::FEDRackInfoSpaceHandler::~FEDRackInfoSpaceHandler() {
}

void
PixelMonitor::rack::FEDRackInfoSpaceHandler::registerItemSetsWithMonitor(PixelMonitor::utilslayer::Monitor &monitor) {
    std::string const itemSetName_0 = "fs-itemset";
    monitor.newItemSet(itemSetName_0);
    monitor.addItem(itemSetName_0,
                    "fl_info",
                    "flashlist information",
                    this);
    std::string const itemSetName_1 = "ms-itemset";
    monitor.newItemSet(itemSetName_1);
    monitor.addItem(itemSetName_1,
                    "mask_info",
                    "channel mask information",
                    this);
}

void
PixelMonitor::rack::FEDRackInfoSpaceHandler::registerItemSetsWithWebServer(PixelMonitor::utilslayer::WebServer &webServer,
                                                                           PixelMonitor::utilslayer::Monitor &monitor) {
    std::string const tabName_0 = "FED-MONITOR-LIST";
    std::string const tabDesc_0 = "<ul> \
                                      <li><span class=\"xdaq-green\">FEDMonitor</span>  RDY</li> \
                                      <li><span class=\"xdaq-orange\">FEDMonitor</span>  WRN</li> \
                                      <li><span class=\"xdaq-yellow\">FEDMonitor</span>  OOS</li> \
                                      <li><span class=\"xdaq-red\">FEDMonitor</span>  BSY</li> \
                                   </ul>";
    webServer.registerTab(tabName_0,
                          tabDesc_0,
                          1);
    webServer.registerWebObject<WebTableFlInfo>("FED Rack table",
                                                "",
                                                monitor,
                                                "fs-itemset",
                                                tabName_0);

    std::string const tabName_1 = "CHANNEL-MASK-STATUS";
    std::string const tabDesc_1 = "<ul> \
                                      <li><span class=\"xdaq-green\">Mask</span> Enabled, not masked</li> \
                                      <li><span class=\"xdaq-yellow\">Mask</span> Enabled, auto-masked</li> \
                                      <li><span class=\"xdaq-red\">Mask</span> Enabled, tbm-masked</li> \
                                      <li><span class=\"xdaq-darkgrey\">Mask</span>  Disabled</li> \
                                   </ul>";
    webServer.registerTab(tabName_1,
                          tabDesc_1,
                          1);
    webServer.registerWebObject<WebTableMaskInfo>("FED channel masking status table",
                                                  "",
                                                  monitor,
                                                  "ms-itemset",
                                                  tabName_1);
}

std::string
PixelMonitor::rack::FEDRackInfoSpaceHandler::formatItem(PixelMonitor::utilslayer::InfoSpaceHandler::ItemVec::const_iterator const &item) const {
    std::string res = PixelMonitor::utilslayer::escapeAsJSONString(kInvalidItemString_);
    if (item->isValid()) {
        std::string name = item->name();
        if (name == "fl_info" || name == "mask_info") {
            res = getString(name);
        } else {
            res = InfoSpaceHandler::formatItem(item);
        }
    }
    return res;
}
