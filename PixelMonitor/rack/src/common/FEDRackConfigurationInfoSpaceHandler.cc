#include "PixelMonitor/rack/FEDRackConfigurationInfoSpaceHandler.h"

#include <string>
#include <vector>
#include "xdaq/Application.h"
#include "xdaq/ContextDescriptor.h"
#include "xdaq/ApplicationContext.h"

#include "PixelMonitor/utilslayer/InfoSpaceItem.h"
#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/WebServer.h"

PixelMonitor::rack::FEDRackConfigurationInfoSpaceHandler::FEDRackConfigurationInfoSpaceHandler(xdaq::Application *const xdaqApp)
    : PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler(xdaqApp) {
    createString("lasURL",
                 "unknown",
                 "",
                 PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE,
                 true);
    createString("lasServiceName",
                 "xmaslas2g",
                 "",
                 PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE,
                 true);
    createString("flashlistName",
                 "FEDMonitor",
                 "",
                 PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE,
                 true);
    createString("SOAPCommandFromSupervisor",
                 "n/a",
                 "",
                 PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE,
                 true);
    contextVec_ =
        xdaqApp->getApplicationContext()->getContextTable()->getContextDescriptors();
    std::vector<xdaq::ContextDescriptor *>::const_iterator contextVecIter;
    for (contextVecIter = contextVec_.begin();
         contextVecIter != contextVec_.end();
         ++contextVecIter) {
        //std::cout<<(*contextVecIter)->getURL()<<std::endl;
        std::string url = (*contextVecIter)->getURL();
        if (url[url.size() - 1] == '/') {
            url = url.substr(0, url.size() - 1);
        }
        createString(url,
                     "Halted",
                     "",
                     PixelMonitor::utilslayer::InfoSpaceItem::NOUPDATE,
                     true);
    }
}

void
PixelMonitor::rack::FEDRackConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(PixelMonitor::utilslayer::Monitor &monitor) {
    PixelMonitor::utilslayer::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(monitor);
    std::string itemSetName = "Application configuration"; //"FEDRack-configIs-itemset";
    //monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "lasURL",
                    "LAS URL",
                    this);
    monitor.addItem(itemSetName,
                    "lasServiceName",
                    "LAS service name",
                    this);
    monitor.addItem(itemSetName,
                    "flashlistName",
                    "Flashlist Name",
                    this);
    monitor.addItem(itemSetName,
                    "SOAPCommandFromSupervisor",
                    "SOAP Command From Supervisor",
                    this);
    std::vector<xdaq::ContextDescriptor *>::const_iterator contextVecIter;
    for (contextVecIter = contextVec_.begin();
         contextVecIter != contextVec_.end();
         ++contextVecIter) {
        std::string url = (*contextVecIter)->getURL();
        if (url[url.size() - 1] == '/') {
            url = url.substr(0, url.size() - 1);
        }
        monitor.addItem(itemSetName,
                        url,
                        url,
                        this);
    }
}

/*--
void
PixelMonitor::rack::FEDRackConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(PixelMonitor::utilslayer::WebServer& webServer,
                                                                                        PixelMonitor::utilslayer::Monitor& monitor)
{
    std::string const tabName = "Configuration";
    webServer.registerTab(tabName,
                          "Configuration parameters",
                          2);
    webServer.registerTable("Application configuration",
                            "Application configuration parameters",
                            monitor,
                            "FEDRack-configIs-itemset",
                            tabName);
}
--*/
