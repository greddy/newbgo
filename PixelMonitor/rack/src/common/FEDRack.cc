#include "PixelMonitor/rack/FEDRack.h"

#include <sstream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <stdio.h>
#include <unistd.h>
#include <fstream>
#include <iostream>

#include "xgi/Method.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"
#include "toolbox/string.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xcept/Exception.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/Method.h"
#include "xoap/MessageFactory.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/hwlayer/HwDeviceTCA.h"
#include "PixelMonitor/rack/FEDRackInfoSpaceHandler.h"
#include "PixelMonitor/rack/FEDRackInfoSpaceUpdater.h"
#include "PixelMonitor/rack/FEDRackConfigurationInfoSpaceHandler.h"
#include "PixelMonitor/utilslayer/LogMacros.h"
#include "PixelMonitor/utilslayer/SOAPUtils.h"

XDAQ_INSTANTIATOR_IMPL(PixelMonitor::rack::FEDRack);

PixelMonitor::rack::FEDRack::FEDRack(xdaq::ApplicationStub *const stub) try
    : PixelMonitor::utilslayer::XDAQAppBase(stub, std::unique_ptr<PixelMonitor::hwlayer::HwDeviceTCA>(0)) {
    cfgInfoSpaceP_ =
        std::unique_ptr<FEDRackConfigurationInfoSpaceHandler>(new FEDRackConfigurationInfoSpaceHandler(this));
    xoap::bind<FEDRack>(this, &FEDRack::enableAll, "EnableAllMonitors", XDAQ_NS_URI);
    xoap::bind<FEDRack>(this, &FEDRack::disableAll, "DisableAllMonitors", XDAQ_NS_URI);
}
catch (PixelMonitor::exception::Exception const &err) {
    std::string msgBase = "Something went wrong instantiating the FEDRack";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
}

PixelMonitor::rack::FEDRack::~FEDRack() {
}

void
PixelMonitor::rack::FEDRack::setupInfoSpaces() {
    cfgInfoSpaceP_->readInfoSpace();

    rackUpdaterP_ = std::unique_ptr<FEDRackInfoSpaceUpdater>(new FEDRackInfoSpaceUpdater(*this));
    rackHandlerP_ = std::unique_ptr<FEDRackInfoSpaceHandler>(new FEDRackInfoSpaceHandler(this,
                                                                                       rackUpdaterP_.get()));

    // No FSM, no state either
    appStateInfoSpace_.setString("stateName", "n/a");
    // similar for hardware lease.
    appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

    cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
    appStateInfoSpace_.registerItemSets(monitor_, webServer_);
    rackHandlerP_->registerItemSets(monitor_, webServer_);
}

xoap::MessageReference
PixelMonitor::rack::FEDRack::enableAll(xoap::MessageReference msg)  {
    xoap::MessageReference reply;

    std::string requestorId = PixelMonitor::utilslayer::soap::extractSOAPCommandRequestorId(msg);
    std::string commandName = PixelMonitor::utilslayer::soap::extractSOAPCommandName(msg);
    if (commandName != "EnableAllMonitors") {
        std::string const histMsg = "Cannot understand commandName, ignoring...";
        this->appStateInfoSpace_.addHistoryItem(histMsg);
        ERROR(histMsg);
    }
    //cfgInfoSpaceP_->setString("SOAPCommandFromSupervisor", "Enable", true);
    //cfgInfoSpaceP_->readInfoSpace();
    bool good = true;
    targetURLs_ = PixelMonitor::utilslayer::soap::extractSOAPCommandParameterString(msg, "targetAppURLs");

    // creating filename...
    std::stringstream ss(targetURLs_);
    std::string tok;
    std::vector<std::string> connectionList;
    while (std::getline(ss, tok, ',')) {
        connectionList.push_back(tok.substr(tok.find("lid=") + 4));
    }
    std::stringstream urlfn;
    urlfn << "/tmp/wsi/FEDRack_connectionURL_";
    for (std::vector<std::string>::iterator fedIdIter = connectionList.begin();
         fedIdIter != connectionList.end();
         ++fedIdIter) {
        urlfn << *fedIdIter;
        if (fedIdIter != connectionList.end() - 1) {
            urlfn << "-";
        }
    }
    urlfn << ".txt";

    // store the list in a tmp file for cmd, due to c_str() size limit
    std::ofstream targetsTmpFile;
    targetsTmpFile.open(urlfn.str().c_str());
    targetsTmpFile << targetURLs_;
    targetsTmpFile.close();

    std::string const hostName = targetURLs_.substr(0, targetURLs_.find("/urn"));
    cfgInfoSpaceP_->setString(hostName, "Configured", true);
    cfgInfoSpaceP_->readInfoSpace();

    toolbox::task::ActionSignature *as;
    as = toolbox::task::bind(this, &PixelMonitor::rack::FEDRack::enableAllCore, "enableAllCore");
    toolbox::task::WorkLoop *wl =
        toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:myapplication-connect", "waiting");
    if (!wl->isActive()) {
        wl->activate();
    }
    wl->submit(as);

    if (good) {
        std::string const histMsg = "Received 'EnableAllMonitors' command.";
        reply = PixelMonitor::utilslayer::soap::makeCommandSOAPReply(msg);
        this->appStateInfoSpace_.addHistoryItem(histMsg);
        INFO(histMsg);
    } else {
        std::string const histMsg = "Fail to ENABLE FEDMonitors...";
        reply = PixelMonitor::utilslayer::soap::makeSOAPFaultReply(msg,
                                                                   PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver,
                                                                   histMsg);
        this->appStateInfoSpace_.addHistoryItem(histMsg);
        ERROR(histMsg);
    }
    return reply;
}

bool
PixelMonitor::rack::FEDRack::enableAllCore(toolbox::task::WorkLoop *wl) {
    // Call connectWithFed.py script to configure them
    std::string const targetURLs = "\"" + targetURLs_ + "\"";
    std::stringstream ss(targetURLs_);
    std::string tok;
    std::vector<std::string> connectionList;
    while (std::getline(ss, tok, ',')) {
        connectionList.push_back(tok.substr(tok.find("lid=") + 4));
    }

    std::stringstream fn;
    fn << "/tmp/wsi/FEDRack_connect_";
    std::stringstream urlfn;
    urlfn << "/tmp/wsi/FEDRack_connectionURL_";

    std::string const msg = "Starting to connect with all FEDs...";
    this->appStateInfoSpace_.addHistoryItem(msg);
    INFO(msg);
    for (std::vector<std::string>::iterator fedIdIter = connectionList.begin();
         fedIdIter != connectionList.end();
         ++fedIdIter) {
        std::string const tmp = "To be connected: FED" + *fedIdIter;
        this->appStateInfoSpace_.addHistoryItem(tmp);
        INFO(tmp);
        fn << *fedIdIter;
        urlfn << *fedIdIter;
        if (fedIdIter != connectionList.end() - 1) {
            fn << "-";
            urlfn << "-";
        }
    }

    fn << ".log";
    urlfn << ".txt";
    std::string const cmd = toolbox::toString("/cmsnfshome0/nfshome0/wsi/TriDAS/pixel/PixelMonitor/utilslayer/scripts/connectWithFEDs.py %s > %s &",
                                              urlfn.str().c_str(), fn.str().c_str());

    bool good = true;
    std::stringstream output;
    std::string err = "";
    FILE *input = 0;
    char buff[512];
    if (!(input = popen(cmd.c_str(), "r"))) {
        good = false;
        err = toolbox::toString("Failed to spawn a shell to execute command '%s'.", cmd.c_str());
    }

    if (good) {
        while (fgets(buff, sizeof(buff), input) != 0) {
            output << buff;
        }
        int const status = pclose(input);
        good = (status == 0);
        err = toolbox::toString("Failed to execute command '%s'.", cmd.c_str());
    }
    if (good) {
        std::string const msg = "Done with connection.";
        this->appStateInfoSpace_.addHistoryItem(msg);
        INFO(msg);
    } else {
        this->appStateInfoSpace_.addHistoryItem("Failed with connection.");
        std::string const msg = toolbox::toString("Failed with connection: %s",
                                                  err.c_str());
        ERROR(msg);
    }
    return false;
}

xoap::MessageReference
PixelMonitor::rack::FEDRack::disableAll(xoap::MessageReference msg)  {
    xoap::MessageReference reply;

    std::string requestorId = PixelMonitor::utilslayer::soap::extractSOAPCommandRequestorId(msg);
    std::string commandName = PixelMonitor::utilslayer::soap::extractSOAPCommandName(msg);
    if (commandName != "DisableAllMonitors") {
        std::string const histMsg = "Cannot understand commandName, ignoring...";
        this->appStateInfoSpace_.addHistoryItem(histMsg);
        ERROR(histMsg);
    }
    //cfgInfoSpaceP_->setString("SOAPCommandFromSupervisor", "Disable", true);
    //cfgInfoSpaceP_->readInfoSpace();
    bool good = true;
    targetURLs_ = PixelMonitor::utilslayer::soap::extractSOAPCommandParameterString(msg, "targetAppURLs");

    // creating filename...
    std::stringstream ss(targetURLs_);
    std::string tok;
    std::vector<std::string> connectionList;
    while (std::getline(ss, tok, ',')) {
        connectionList.push_back(tok.substr(tok.find("lid=") + 4));
    }
    std::stringstream urlfn;
    urlfn << "/tmp/wsi/FEDRack_disconnectionURL_";
    for (std::vector<std::string>::iterator fedIdIter = connectionList.begin();
         fedIdIter != connectionList.end();
         ++fedIdIter) {
        urlfn << *fedIdIter;
        if (fedIdIter != connectionList.end() - 1) {
            urlfn << "-";
        }
    }
    urlfn << ".txt";

    // store the list in a tmp file for cmd, due to c_str() size limit
    std::ofstream targetsTmpFile;
    targetsTmpFile.open(urlfn.str().c_str());
    targetsTmpFile << targetURLs_;
    targetsTmpFile.close();

    std::string const hostName = targetURLs_.substr(0, targetURLs_.find("/urn"));
    cfgInfoSpaceP_->setString(hostName, "Halted", true);
    cfgInfoSpaceP_->readInfoSpace();

    toolbox::task::ActionSignature *as;
    as = toolbox::task::bind(this, &PixelMonitor::rack::FEDRack::disableAllCore, "disableAllCore");
    toolbox::task::WorkLoop *wl =
        toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:myapplication-disconnect", "waiting");
    if (!wl->isActive()) {
        wl->activate();
    }
    wl->submit(as);

    if (good) {
        std::string const histMsg = "Received 'DisableAllMonitors' command.";
        reply = PixelMonitor::utilslayer::soap::makeCommandSOAPReply(msg);
        this->appStateInfoSpace_.addHistoryItem(histMsg);
        INFO(histMsg);
    } else {
        std::string const histMsg = "Fail to DISABLE FEDMonitors...";
        reply = PixelMonitor::utilslayer::soap::makeSOAPFaultReply(msg,
                                                                   PixelMonitor::utilslayer::soap::SOAPFaultCodeReceiver,
                                                                   histMsg);
        this->appStateInfoSpace_.addHistoryItem(histMsg);
        ERROR(histMsg);
    }
    return reply;
}

bool
PixelMonitor::rack::FEDRack::disableAllCore(toolbox::task::WorkLoop *wl) {
    // Call disconnectWithFed.py script to configure them
    std::string const targetURLs = "\"" + targetURLs_ + "\"";
    std::stringstream ss(targetURLs_);
    std::string tok;
    std::vector<std::string> connectionList;
    while (std::getline(ss, tok, ',')) {
        connectionList.push_back(tok.substr(tok.find("lid=") + 4));
    }

    std::stringstream fn;
    fn << "/tmp/wsi/FEDRack_disconnect_";
    std::stringstream urlfn;
    urlfn << "/tmp/wsi/FEDRack_disconnectionURL_";

    std::string const msg = "Starting to disconnect with all FEDs...";
    this->appStateInfoSpace_.addHistoryItem(msg);
    INFO(msg);
    for (std::vector<std::string>::iterator fedIdIter = connectionList.begin();
         fedIdIter != connectionList.end();
         ++fedIdIter) {
        std::string const tmp = "To be disconnected: FED" + *fedIdIter;
        this->appStateInfoSpace_.addHistoryItem(tmp);
        INFO(tmp);
        fn << *fedIdIter;
        urlfn << *fedIdIter;
        if (fedIdIter != connectionList.end() - 1) {
            fn << "-";
            urlfn << "-";
        }
    }

    fn << ".log";
    urlfn << ".txt";
    std::string const cmd = toolbox::toString("/cmsnfshome0/nfshome0/wsi/TriDAS/pixel/PixelMonitor/utilslayer/scripts/disconnectFromFEDs.py %s > %s &",
                                              urlfn.str().c_str(), fn.str().c_str());

    bool good = true;
    std::stringstream output;
    std::string err = "";
    FILE *input = 0;
    char buff[512];
    if (!(input = popen(cmd.c_str(), "r"))) {
        good = false;
        err = toolbox::toString("Failed to spawn a shell to execute command '%s'.", cmd.c_str());
        ERROR(err);
    }

    if (good) {
        while (fgets(buff, sizeof(buff), input) != 0) {
            output << buff;
        }
        int const status = pclose(input);
        good = (status == 0);
        err = toolbox::toString("Failed to execute command '%s'.", cmd.c_str());
    }
    if (good) {
        std::string const msg = "Done with disconnection.";
        this->appStateInfoSpace_.addHistoryItem(msg);
        INFO(msg);
    } else {
        this->appStateInfoSpace_.addHistoryItem("Failed with disconnection.");
        std::string const msg = toolbox::toString("Failed with connection: %s",
                                                  err.c_str());
        ERROR(msg);
    }
    return false;
}

void PixelMonitor::rack::FEDRack::hwConnectImpl() {}
void PixelMonitor::rack::FEDRack::hwReleaseImpl() {}
void PixelMonitor::rack::FEDRack::hwConfigureImpl() {}
