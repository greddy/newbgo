#include "PixelMonitor/rack/Utils.h"

#include <string>

#include "toolbox/net/URL.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdaq/ApplicationDescriptor.h"

#include "PixelMonitor/exception/Exception.h"
#include "PixelMonitor/utilslayer/CurlGetter.h"

bool
PixelMonitor::rack::compareApplications::
operator()(xdaq::ApplicationDescriptor const *const &lhs,
           xdaq::ApplicationDescriptor const *const &rhs) const {
#ifdef XDAQ13
    toolbox::net::URL const lhu = toolbox::net::URL(const_cast<xdaq::ApplicationDescriptor *const>(lhs)->getContextDescriptor()->getURL());
    toolbox::net::URL const rhu = toolbox::net::URL(const_cast<xdaq::ApplicationDescriptor *const>(rhs)->getContextDescriptor()->getURL());
#else
    toolbox::net::URL const lhu = toolbox::net::URL(lhs->getContextDescriptor()->getURL());
    toolbox::net::URL const rhu = toolbox::net::URL(rhs->getContextDescriptor()->getURL());
#endif
    unsigned int const lhp = lhu.getPort();
    unsigned int const rhp = rhu.getPort();
    if (lhp != rhp) {
        return (lhp < rhp);
    } else {
        std::string const lhh = lhu.getHost();
        std::string const rhh = rhu.getHost();
        if (lhh != rhh) {
            return (lhh < rhh);
        } else {
#ifdef XDAQ13
            if (const_cast<xdaq::ApplicationDescriptor *const>(lhs)->getLocalId() !=
                const_cast<xdaq::ApplicationDescriptor *const>(rhs)->getLocalId())
#else
            if (lhs->getLocalId() != rhs->getLocalId())
#endif
            {
#ifdef XDAQ13
                return (const_cast<xdaq::ApplicationDescriptor *const>(lhs)->getLocalId() <
                        const_cast<xdaq::ApplicationDescriptor *const>(rhs)->getLocalId());
#else
                return lhs->getLocalId() < rhs->getLocalId();
#endif
            } else {
                return (lhs < rhs);
            } // same localId
        }     // same port
    }         // same host
}

xdata::Table
PixelMonitor::rack::getFlashList(std::string const &lasURL,
                                 std::string const &lasServiceName,
                                 std::string const &flashListName) {
    std::string const lasAddress =
        "urn:xdaq-application:service=" + lasServiceName + "/retrieveCollection";
    // The data we get will have (E)XDR format
    std::string lasQuery =
        "fmt=exdr&flash=urn:xdaq-flashlist:" + flashListName;

    PixelMonitor::utilslayer::CurlGetter getter;
    std::string rawTableData =
        getter.get("http", lasURL, lasAddress, "", lasQuery, "");

    std::string const tmp = toolbox::tolower(rawTableData);
    std::string failMsg = "failed to find flashlist";
    if (tmp.find(failMsg) != std::string::npos) {
        std::string const fullURL =
            getter.buildFullURL("http", lasURL, lasAddress, "", lasQuery, "");
        std::string const msg =
            toolbox::toString("Failed to find flashlist '%s' on LAS '%s' (full query URL: '%s').",
                              flashListName.c_str(),
                              lasURL.c_str(),
                              fullURL.c_str());
        XCEPT_RAISE(PixelMonitor::exception::RuntimeProblem, msg);
    }

    xdata::exdr::FixedSizeInputStreamBuffer
    inBuffer(static_cast<char *>(&rawTableData[0]), rawTableData.size());

    xdata::Table table;
    xdata::exdr::Serializer serializer;
    try {
        serializer.import(&table, &inBuffer);
    }
    catch (xdata::exception::Exception &err) {
        std::string const msgBase = "Failed to deserialize incoming flashlist table";
        std::string const msg =
            toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        XCEPT_RETHROW(PixelMonitor::exception::RuntimeProblem, msg, err);
    }

    return table;
}

/** FOR TEST
xdata::Table
PixelMonitor::rack::getFlashList(std::string const& url)
{
    PixelMonitor::utilslayer::CurlGetter getter;
    std::string const addr = "urn:xdaq-application:service=fedmonitor/retrieveCollection";
    std::string const query = "fmt=exdr&flash=urn:xdaq-flashlist:FEDMonitor";
    std::string rawTableData = getter.get("http", url, addr, "", query,"");
    xdata::exdr::FixedSizeInputStreamBuffer
        inBuffer(static_cast<char*>(&rawTableData[0]), rawTableData.size());
    xdata::Table table;
    xdata::exdr::Serializer serializer;
    try
    {
        serializer.import(&table, &inBuffer);
    }
    catch (xdata::exception::Exception& err)
    {
        std::string const msgBase = "Failed to deserialize incoming flashlist table";
        std::string const msg =
            toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        XCEPT_RETHROW(PixelMonitor::exception::RuntimeProblem, msg, err);
    }

    return table;
}
**/
