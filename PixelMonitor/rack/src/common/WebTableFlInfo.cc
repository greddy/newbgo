#include "PixelMonitor/rack/WebTableFlInfo.h"

#include <sstream>
#include <string>
#include <vector>

#include "PixelMonitor/utilslayer/Monitor.h"
#include "PixelMonitor/utilslayer/Utils.h"
#include "PixelMonitor/utilslayer/WebObject.h"

PixelMonitor::rack::WebTableFlInfo::WebTableFlInfo(std::string const &name,
                                                   std::string const &description,
                                                   PixelMonitor::utilslayer::Monitor const &monitor,
                                                   std::string const &itemSetName,
                                                   std::string const &tabName,
                                                   size_t const colSpan)
    : PixelMonitor::utilslayer::WebObject(name, description, monitor, itemSetName, tabName, colSpan) {
}

std::string
PixelMonitor::rack::WebTableFlInfo::getHTMLString() const {
    std::stringstream res;
    res << "<div class=\"tcds-item-table-wrapper\">"
        << "\n";

    res << "<p class=\"tcds-item-table-title\">"
        << getName()
        << "</p>";
    res << "\n";

    res << "<p class=\"tcds-item-table-description\">"
        << getDescription()
        << "</p>";
    res << "\n";

    // And now the actual table contents.
    PixelMonitor::utilslayer::Monitor::StringPairVector items =
        monitor_.getFormattedItemSet(itemSetName_);
    // ASSERT ASSERT ASSERT
    assert(items.size() == 1);
    // ASSERT ASSERT ASSERT end

    std::string const itemName = items.at(0).first;
    std::string const tmp = "[\"" + itemSetName_ + "\"][\"" + itemName + "\"]";
    std::string const invalidVal = PixelMonitor::utilslayer::WebObject::kStringInvalidItem;

    res << "<script type=\"text/x-dot-template\">";
    // Case 0: invalid item ('-').
    res << "{{? it" << tmp << " == '" << invalidVal << "'}}"
        << "<span>" << invalidVal << "</span>"
        << "\n";
    // Case 1: a (hopefully non-empty) array.
    res << "{{??}}"
        << "\n"
        << "<table id=\"rackTable\" class=\"xdaq-table tcds-item-table sortable pixelmonitor-table-compact\">"
        << "\n"
        << "<thead>"
        << "<tr>"
        << "<th>Board</th>"
        << "<th>FED</th>"
        << "<th>URL</th>"
        << "<th>EnableState</th>"
        << "<th>FW Ver</th>"
        << "<th>TTS</th>"
        << "<th>L1A</th>"
        //<< "<th>Masked Ch</th>"
        << "<th>PLL200MHz</th>"
        << "<th>PLL200MHz_idelay</th>"
        << "<th>PLL400MHz</th>"
        << "<th>InRDY</th>"
        << "<th>InBSY</th>"
        << "<th>InOOS</th>"
        << "<th>InWRN</th>"
        << "<th>ToRDY</th>"
        << "<th>ToBSY</th>"
        << "<th>ToOOS</th>"
        << "<th>ToWRN</th>"
        << "<th>MACAddress</th>"
        << "<th>Latest update (Local)</th>"
        << "</tr>"
        << "</thead>"
        << "\n"
        << "<tbody>"
        << "\n"
        << "{{~it" << tmp << " :value:index}}"
        << "\n"
        << "<tr>"
        << "\n"
        << "<td class=\"firstColumn\">{{=value[\"BoardCode\"]}}</td>"
        << "<td>{{=value[\"FEDid\"]}}</td>"
        << "<td><a href=\"{{=value[\"url\"]}}\" target=\"_blank\">ClickMe</a></td>"
        << "<td title=\"state\" class=\"emph-{{=value[\"stateName\"].toLowerCase()}}\">{{=value[\"stateName\"]}}</td>"
        << "<td>{{=value[\"FwVer\"]}}</td>"
        << "<td title=\"TTS\">{{=value[\"TTS State\"]}}</td>"
        << "<td>{{=value[\"L1A\"]}}</td>"
        //<< "<td>{{=value[\"maskedCh\"]}}/48</td>"
        << "<td>{{=value[\"pll_200MHz\"]}}</td>"
        << "<td>{{=value[\"pll_200MHz_idelay\"]}}</td>"
        << "<td>{{=value[\"pll_400MHz\"]}}</td>"
        << "<td>{{=value[\"inRDY\"]}}</td>"
        << "<td>{{=value[\"inBSY\"]}}</td>"
        << "<td>{{=value[\"inOOS\"]}}</td>"
        << "<td>{{=value[\"inWRN\"]}}</td>"
        << "<td>{{=value[\"toRDY\"]}}</td>"
        << "<td>{{=value[\"toBSY\"]}}</td>"
        << "<td>{{=value[\"toOOS\"]}}</td>"
        << "<td>{{=value[\"toWRN\"]}}</td>"
        << "<td>{{=value[\"MACAddress\"]}}</td>"
        << "<td>{{=value[\"timestamp\"]}}</td>"
        << "\n"
        << "</tr>"
        << "\n"
        << "{{~}}"
        << "\n"
        << "</tbody>"
        << "\n"
        << "</table>"
        << "\n";
    res << "{{?}}"
        << "\n";
    res << "</script>"
        << "\n"
        << "<div class=\"target\"></div>"
        << "\n";

    return res.str();
}
