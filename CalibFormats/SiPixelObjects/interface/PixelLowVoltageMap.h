#ifndef PixelLowVoltageMap_h
#define PixelLowVoltageMap_h
/**
*   \file CalibFormats/SiPixelObjects/interface/PixelLowVoltageMap.h
*   \brief This class implements..
*
*   A longer explanation will be placed here later
*/

#include <vector>
#include <set>
#include <map>
#include <utility>
#include <string>
#include "CalibFormats/SiPixelObjects/interface/PixelConfigBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelModuleName.h"
#include "CalibFormats/SiPixelObjects/interface/PixelHdwAddress.h"
#include "CalibFormats/SiPixelObjects/interface/PixelNameTranslation.h"
#include "CalibFormats/SiPixelObjects/interface/PixelROCStatus.h"

namespace pos {
/*!  \ingroup ConfigurationObjects "Configuration Objects"
*    
*  @{
*
*  \class PixelLowVoltageMap PixelLowVoltageMap.h
*  \brief This is the documentation about PixelLowVoltageMap...
*
*   A longer explanation will be placed here later
*/
class PixelLowVoltageMap : public PixelConfigBase {

  public:
    PixelLowVoltageMap(std::vector<std::vector<std::string> > &tableMat);
    PixelLowVoltageMap(std::string filename);

    void writeASCII(std::string dir = "") const;
    virtual void writeXMLHeader(int version, std::string path, std::ofstream *out) const;
    virtual void writeXML(std::ofstream *out) const;
    virtual void writeXMLTrailer(std::ofstream *out) const;

    std::string dpName(const PixelModuleName &module) const;
    std::string dpNameIana(const PixelModuleName &module) const;
    std::string dpNameIdigi(const PixelModuleName &module) const;
    std::string dpNameHV(const PixelModuleName &module) const;
    std::vector<PixelModuleName> modules(const std::string &dpNmae) const;

    std::set<unsigned int> getFEDs(PixelNameTranslation *translation) const;
    std::map<unsigned int, std::set<unsigned int> > getFEDsAndChannels(PixelNameTranslation *translation) const;

  private:
    //ugly... FIXME
    //Ok I will try
    std::map<PixelModuleName, std::pair<std::string, std::vector<std::string> > > dpNameMap_;
    //    std::map<PixelModuleName, std::pair<std::string, std::pair<std::string, std::string> > > dpNameMap_;
    //                                    base                    Iana          Idigi
    std::map<std::string, std::vector<PixelModuleName> > dpNameModuleMap_;
    //        dpNameBase     modules
};
}
/* @} */
#endif
