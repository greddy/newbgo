#ifndef PixelPowerMap_h
#define PixelPowerMap_h
/**
* \file CalibFormats/SiPixelObjects/interface/PixelPowerMap.h
* \brief This class provides the mapping between portcards and the modules controlled by the card
*
*   A longer explanation will be placed here later
*
*/

#include <string>
#include <vector>
#include <map>
#include <set>
#include "CalibFormats/SiPixelObjects/interface/PixelConfigBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelModuleName.h"
namespace pos {
/*!  \ingroup ConfigurationObjects "Configuration Objects"
*    
*  @{
*
*  \class PixelPortCardConfig PixelPortCardConfig.h
*  \brief This is the documentation about PixelNameTranslation...
*
*  This class provides the mapping between portcards and the modules controlled by the card
*   
*/
class PixelPowerMap : public PixelConfigBase {
  public:
    PixelPowerMap(std::string filename);

    PixelPowerMap(std::vector<std::vector<std::string> > &tableMat);

    virtual ~PixelPowerMap();

    // pair of DCDC converter and power group relevant for module
    const std::pair<std::string, std::string> dcdcAndPowergroup(const PixelModuleName &aModule) const;

    // set of all modules attached to a dcdc converter in a powergroup
    std::set<PixelModuleName> modules(std::string dcdcName, std::string powergroupName) const;
    std::set<PixelModuleName> modulesPG(std::string powergroupName) const;
    int numModules(std::string dcdcName, std::string powergroupName) {
        return modules(dcdcName, powergroupName).size();
    }
    int numModulesPG(std::string powergroupName) {
        return modulesPG(powergroupName).size();
    }

    virtual void writeASCII(std::string dir) const;
    virtual void writeXMLHeader(int version, std::string path, std::ofstream *out) const;
    virtual void writeXML(std::ofstream *out) const;
    virtual void writeXMLTrailer(std::ofstream *out) const;

  private:
    // pixel module, dcdc converter, power group
    std::map<PixelModuleName, std::pair<std::string, std::string> > map_;
};
}
/* @} */
#endif
