#ifndef PixelDCDCConverterConfig_h
#define PixelDCDCConverterConfig_h
/**
* \file CalibFormats/SiPixelObjects/interface/PixelDCDCConverterConfig.h
* \brief This class provides the mapping between portcards and the modules controlled by the card
*
*   A longer explanation will be placed here later
*
*/

#include <string>
#include <vector>
#include <map>
#include <set>
#include <bitset>
#include "CalibFormats/SiPixelObjects/interface/PixelConfigBase.h"
namespace pos {
/*!  \ingroup ConfigurationObjects "Configuration Objects"
*    
*  @{
*
*  \class PixelDCDCConverterConfig PixelDCDCConverterConfig.h
*  \brief This is the documentation about PixelDCDCConverterConfig...
*
*  This class provides the configuration of DCDC converters
*   
*/
class PixelDCDCConverterConfig : public PixelConfigBase {
  public:
    PixelDCDCConverterConfig(std::string filename);

    PixelDCDCConverterConfig(std::vector<std::vector<std::string> > &tableMat);

    virtual ~PixelDCDCConverterConfig();

    // pair of PIA channel and bit of DCDC converter for power good bit
    const std::pair<int, std::bitset<8> > getPgChannelAndBit(const std::string &dcdcname) const;
    // pair of PIA channel and bit of DCDC converter for enable bit
    const std::pair<int, std::bitset<8> > getEnChannelAndBit(const std::string &dcdcname) const;
    // returns the default status after reset
    const bool getDefault(const std::string &dcdcname) const;
    // returns the CCU configuration this DCDC converter belongs to
    const std::string getConfiguration(const std::string &dcdcname) const;

    //returns a logical or of all EN bits of dcdc converters in the set
    const std::bitset<8> getAllEnBits(const std::string &configuration, int &piachannel) const;

    //returns a logical or of all PG bits of dcdc converters in the set
    const std::bitset<8> getAllPgBits(const std::string &configuration, int &piachannel) const;

    virtual void writeASCII(std::string dir) const;
    virtual void writeXMLHeader(int version, std::string path, std::ofstream *out) const;
    virtual void writeXML(std::ofstream *out) const;
    virtual void writeXMLTrailer(std::ofstream *out) const;

    void test(PixelDCDCConverterConfig &classundertest) const;

  private:
    //                               portcardname, AOH #
    std::map<std::string, std::pair<int, std::bitset<8> > > mapEN_;
    std::map<std::string, std::pair<int, std::bitset<8> > > mapPG_;
    std::map<std::string, bool> mapDefault_;
    std::map<std::string, std::string> mapConfiguration_;
    std::map<std::pair<std::string, int>, std::set<std::string> > mapEnDCDCOnPiaCh_;
    std::map<std::pair<std::string, int>, std::set<std::string> > mapPgDCDCOnPiaCh_;
};
}
/* @} */
#endif
