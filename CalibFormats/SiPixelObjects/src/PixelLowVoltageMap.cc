//
// Implementation of the detector configuration
//
//
//
//

#include "CalibFormats/SiPixelObjects/interface/PixelLowVoltageMap.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <ios>
#include <assert.h>
#include <stdio.h>
#include <stdexcept>

using namespace std;
using namespace pos;

PixelLowVoltageMap::PixelLowVoltageMap(std::vector<std::vector<std::string> > &tableMat)
    : PixelConfigBase("", "", "") {
    std::string mthn = "[PixelLowVoltageMap::PixelLowVoltageMap()] ";
    std::map<std::string, int> colM;
    std::vector<std::string> colNames;

    colNames.push_back("MODULE_NAME");
    colNames.push_back("DATAPOINT");
    colNames.push_back("LV_DIGITAL");
    colNames.push_back("LV_ANALOG");
    colNames.push_back("HV_CHANNEL");

    bool HVchannelFlag = true;
    for (unsigned int c = 0; c < tableMat[0].size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    } //end for
    for (unsigned int n = 0; n < colNames.size(); n++) {
        if (colM.find(colNames[n]) == colM.end()) {
            if (n < 5) {
                std::cerr << mthn << "Couldn't find in the database the column with name " << colNames[n] << std::endl;
                assert(0);
            } else {
                HVchannelFlag = false;
            }
        }
    }

    std::string modulename;
    std::string dpNameBase;
    std::string ianaChannel;
    std::string idigiChannel;
    std::string HVChannel;
    for (unsigned int r = 1; r < tableMat.size(); r++) { //Goes to every row of the Matrix
        modulename = tableMat[r][colM["MODULE_NAME"]];
        dpNameBase = tableMat[r][colM["DATAPOINT"]];
        ianaChannel = tableMat[r][colM["LV_ANALOG"]];
        idigiChannel = tableMat[r][colM["LV_DIGITAL"]];
        if (HVchannelFlag)
            HVChannel = tableMat[r][colM["HV_CHANNEL"]];
        PixelModuleName module(modulename);
        std::vector<std::string> channels;
        channels.push_back(ianaChannel);
        channels.push_back(idigiChannel);
        if (HVchannelFlag)
            channels.push_back(HVChannel);
        pair<string, vector<string> > dpName(dpNameBase, channels);
        dpNameMap_[module] = dpName;
    }
} //end constructor

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

PixelLowVoltageMap::PixelLowVoltageMap(std::string filename)
    : PixelConfigBase("", "", "") {

    std::string mthn = "[PixelLowVoltageMap::PixelLowVoltageMap()]\t\t\t    ";

    if (filename[filename.size() - 1] == 't') {

        std::ifstream in(filename.c_str());

        if (!in.good()) {
            std::cout << __LINE__ << "]\t" << mthn << "Could not open: " << filename << std::endl;
            throw std::runtime_error("Failed to open file " + filename);
        } else {
            std::cout << __LINE__ << "]\t" << mthn << "Opened: " << filename << std::endl;
        }

        if (in.eof()) {
            std::cout << __LINE__ << "]\t" << mthn << "eof before reading anything!" << std::endl;
            throw std::runtime_error("Failure when reading file; file seems to be empty: " + filename);
        }

        dpNameMap_.clear();
        dpNameModuleMap_.clear();

        std::string modulename;
        std::string dpNameBase;
        std::string ianaChannel;
        std::string idigiChannel;
        std::string HVChannel = "";

        std::string line;
        while (std::getline(in, line)) {
            std::istringstream line_as_stream(line);
            std::string temp;
            std::vector<std::string> linevector;
            while (line_as_stream >> temp) {
                linevector.push_back(temp);
            }
            if (linevector.size() >= 4) {
                modulename = linevector.at(0);
                dpNameBase = linevector.at(1);
                ianaChannel = linevector.at(2);
                idigiChannel = linevector.at(3);
            }
            if (linevector.size() == 5) {
                HVChannel = linevector.at(4);
            }
            //cout << __LINE__ << "]\t" << mthn << "Read modulename: " << modulename << endl;
            PixelModuleName module(modulename);
            std::vector<std::string> channels;
            channels.push_back(ianaChannel);
            channels.push_back(idigiChannel);
            channels.push_back(HVChannel);
            pair<string, vector<string> > dpName(dpNameBase, channels);
            dpNameMap_[module] = dpName;
            if (dpNameModuleMap_.find(dpNameBase) == dpNameModuleMap_.end()) {
                std::vector<PixelModuleName> moduleVec;
                dpNameModuleMap_[dpNameBase] = moduleVec;
            }
        }
    } else {
        assert(0);
    }
}

std::string PixelLowVoltageMap::dpName(const PixelModuleName &module) const {

    std::string mthn = "[PixelLowVoltageMap::dpName()]\t\t\t    ";
    std::map<PixelModuleName, std::pair<std::string, std::vector<std::string> > >::const_iterator i =
        dpNameMap_.find(module);

    if (i == dpNameMap_.end()) {
        cout << __LINE__ << "]\t" << mthn << "Could not find module: " << module.modulename() << endl;
        return "";
    }

    return i->second.first;
}

std::string PixelLowVoltageMap::dpNameIana(const PixelModuleName &module) const {

    std::string mthn = "[PixelLowVoltageMap::dpNameIana()]\t\t\t    ";
    std::map<PixelModuleName, std::pair<std::string, std::vector<std::string> > >::const_iterator i =
        dpNameMap_.find(module);

    if (i == dpNameMap_.end()) {
        cout << __LINE__ << "]\t" << mthn << "Could not find module: " << module.modulename() << endl;
    }

    return i->second.first + "/" + i->second.second.at(0);
}

std::string PixelLowVoltageMap::dpNameIdigi(const PixelModuleName &module) const {

    std::string mthn = "[PixelLowVoltageMap::dpNameIdigi()]\t\t\t    ";
    std::map<PixelModuleName, std::pair<std::string, std::vector<std::string> > >::const_iterator i =
        dpNameMap_.find(module);

    if (i == dpNameMap_.end()) {
        cout << __LINE__ << "]\t" << mthn << "Could not find module: " << module.modulename() << endl;
    }

    return i->second.first + "/" + i->second.second.at(1);
}

std::string PixelLowVoltageMap::dpNameHV(const PixelModuleName &module) const {

    std::string mthn = "[PixelLowVoltageMap::dpNameHV()]\t\t\t    ";
    std::map<PixelModuleName, pair<string, vector<string> > >::const_iterator i =
        dpNameMap_.find(module);

    if (i == dpNameMap_.end()) {
        cout << __LINE__ << "]\t" << mthn << "Could not find module: " << module.modulename() << endl;
    }

    return i->second.first + "/" + i->second.second.at(2);
}

std::vector<PixelModuleName> PixelLowVoltageMap::modules(const std::string &dpName) const {

    std::string mthn = "[PixelLowVoltageMap::modules()]\t\t\t    ";

    std::map<std::string, std::vector<PixelModuleName> >::const_iterator i =
        dpNameModuleMap_.find(dpName);

    if (i == dpNameModuleMap_.end()) {
        cout << __LINE__ << "]\t" << mthn << "Could not find data point: " << dpName << endl;
        std::vector<PixelModuleName> emptyVec;
        return emptyVec;
    }

    return i->second;
}

void PixelLowVoltageMap::writeASCII(std::string dir) const {

    std::string mthn = "[PixelLowVoltageMap::writeASCII()]\t\t\t    ";
    if (dir != "")
        dir += "/";
    std::string filename = dir + "lowvoltagemap.dat";

    std::ofstream out(filename.c_str(), std::ios_base::out);
    if (!out) {
        std::cout << __LINE__ << "]\t" << mthn << "Could not open file " << filename << " for write" << std::endl;
        exit(1);
    }
    std::map<PixelModuleName, pair<string, vector<string> > >::const_iterator imodule =
        dpNameMap_.begin();

    for (; imodule != dpNameMap_.end(); ++imodule) {
        out << imodule->first
            << " " << imodule->second.first
            << " " << imodule->second.second.at(0)
            << " " << imodule->second.second.at(1)
            << " " << imodule->second.second.at(2)
            << endl;
    }

    out.close();
}

//=============================================================================================
void PixelLowVoltageMap::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[[PixelLowVoltageMap::writeXMLHeader()]\t\t\t    ";
    std::string mthn = s.str();
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_LowVoltageMap_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_LOW_VOLTAGE_MAP</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel Low Voltage Map</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel LowVoltageMap</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel Low Voltage Map Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}

//=============================================================================================
void PixelLowVoltageMap::writeXML(std::ofstream *outstream) const {
    std::string mthn = "[PixelLowVoltageMap::writeXML()]\t\t\t    ";

    std::map<PixelModuleName, pair<string, vector<string> > >::const_iterator imodule = dpNameMap_.begin();

    for (; imodule != dpNameMap_.end(); ++imodule) {
        *outstream << "        <DATA>" << std::endl;
        *outstream << "            <MODULE_NAME>" << imodule->first << "</MODULE_NAME>" << std::endl;
        *outstream << "            <DATAPOINT>" << imodule->second.first << "</DATAPOINT>" << std::endl;
        *outstream << "            <LV_DIGITAL>" << imodule->second.second.at(0) << "</LV_DIGITAL>" << std::endl;
        *outstream << "            <LV_ANALOG>" << imodule->second.second.at(1) << "</LV_ANALOG>" << std::endl;
        *outstream << "            <HV_CHANNEL>" << imodule->second.second.at(2) << "</HV_CHANNEL>" << std::endl;

        *outstream << "        </DATA>" << std::endl;
    }
}

//=============================================================================================
void PixelLowVoltageMap::writeXMLTrailer(std::ofstream *outstream) const {
    std::string mthn = "[PixelLowVoltageMap::writeXMLTrailer()]\t\t\t    ";

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT>" << std::endl;

    outstream->close();
}
