//
// This class stores the information about a FEC.
// This include the number, crate, and base address
//
//

#include "CalibFormats/SiPixelObjects/interface/PixelFECConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"
#include "CalibFormats/SiPixelObjects/interface/Utility.h"
#include <fstream>
#include <sstream>
#include <map>
#include <assert.h>
#include <stdexcept>

using namespace pos;
using namespace std;

PixelFECConfig::PixelFECConfig(std::vector<std::vector<std::string> > &tableMat)
    : PixelConfigBase(" ", " ", " ") {
    std::map<std::string, int> colM;
    std::vector<std::string> colNames;

    colNames.push_back("PXLFEC_NUMBER");
    colNames.push_back("CRATE_NUMBER");
    colNames.push_back("VME_BASE_ADDRS");
    colNames.push_back("TYPE");
    colNames.push_back("URI");

    for (unsigned int c = 0; c < tableMat[0].size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    }

    for (unsigned int n = 0; n < colNames.size(); n++) {
        if (colM.find(colNames[n]) == colM.end()) {
            std::cerr << "[PixelFECConfig::PixelFECConfig()]\tCouldn't find in the database the column with name " << colNames[n] << std::endl;
            assert(0);
        }
    }

    fecconfig_.clear();
    for (unsigned int r = 1; r < tableMat.size(); r++) { //Goes to every row of the Matrix
        unsigned int fecnumber;
        unsigned int crate;
        unsigned int vme_base_address;
        std::string type;
        std::string uri;

        fecnumber = atoi(tableMat[r][colM["PXLFEC_NUMBER"]].c_str());
        crate = atoi(tableMat[r][colM["CRATE_NUMBER"]].c_str());
        string hexVMEAddr = tableMat[r][colM["VME_BASE_ADDRS"]];
        sscanf(hexVMEAddr.c_str(), "%x", &vme_base_address);
        type = tableMat[r][colM["TYPE"]].c_str();
        uri = tableMat[r][colM["URI"]].c_str();

        PixelFECParameters tmp;

        tmp.setFECParameters(fecnumber, crate, vme_base_address);
        tmp.setType(type);
        tmp.setURI(uri);

        fecconfig_.push_back(tmp);
    }
} // end contructor

//****************************************************************************************

PixelFECConfig::PixelFECConfig(std::string filename)
    : PixelConfigBase(" ", " ", " ") {

    std::string mthn = "[PixelFECConfig::PixelFECConfig()]\t\t\t    ";
    std::ifstream in(filename.c_str());

    if (!in.good()) {
        std::cout << __LINE__ << "]\t" << mthn << "Could not open: " << filename.c_str() << std::endl;
        throw std::runtime_error("Failed to open file " + filename);
    } else {
        std::cout << __LINE__ << "]\t" << mthn << "Opened: " << filename.c_str() << std::endl;
    }

    std::string line;

    while (getline(in, line)) {
        if (line[0] == '#' || line.find_first_not_of(" \t") == std::string::npos)
            continue;
        std::vector<std::string> tokens = tokenize(line, true);
        if (tokens.size() == 0)
            continue;                                     // a comment line
        assert(tokens.size() >= 3 && tokens.size() <= 5); // 3 to be backward compatible with VME-only POS, 5 with VME-or-uTCA POS

        const unsigned fednumber = strtoul(tokens[0].c_str(), 0, 10);
        const unsigned crate = strtoul(tokens[1].c_str(), 0, 10);
        const unsigned vme_base_address = strtoul(tokens[2].c_str(), 0, 16);

        PixelFECParameters tmp;
        tmp.setFECParameters(fednumber, crate, vme_base_address);

        if (tokens.size() == 3) {
            tmp.setType("VME");
        } else if (tokens.size() == 4) {
            assert(tokens[3] == "VME");
            tmp.setType("VME");
        } else {
            tmp.setType(tokens[3]);
            tmp.setURI(tokens[4]);
        }

        assert(tmp.getType() == "VME" || tmp.getType() == "GLIB" || tmp.getType() == "CTA");

        fecconfig_.push_back(tmp);
    }

    in.close();
}

//std::ostream& operator<<(std::ostream& s, const PixelFECConfig& table){

//for (unsigned int i=0;i<table.translationtable_.size();i++){
//	s << table.translationtable_[i]<<std::endl;
//   }
// return s;

//}

unsigned int PixelFECConfig::getNFECBoards() const {
    return fecconfig_.size();
}

unsigned int PixelFECConfig::getFECNumber(unsigned int i) const {

    assert(i < fecconfig_.size());
    return fecconfig_[i].getFECNumber();
}

unsigned int PixelFECConfig::getCrate(unsigned int i) const {

    assert(i < fecconfig_.size());
    return fecconfig_[i].getCrate();
}

unsigned int PixelFECConfig::getVMEBaseAddress(unsigned int i) const {

    assert(i < fecconfig_.size());
    return fecconfig_[i].getVMEBaseAddress();
}

unsigned int PixelFECConfig::crateFromFECNumber(unsigned int fecnumber) const {

    std::string mthn = "[PixelFECConfig::crateFromFECNumber()]\t\t\t    ";
    for (unsigned int i = 0; i < fecconfig_.size(); i++) {
        if (fecconfig_[i].getFECNumber() == fecnumber)
            return fecconfig_[i].getCrate();
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FEC number: " << fecnumber << std::endl;

    assert(0);

    return 0;
}

std::string PixelFECConfig::typeFromFECNumber(unsigned int fecnumber) const {

    std::string mthn = "[PixelFECConfig::typeFromFECNumber()]\t\t\t    ";
    for (unsigned int i = 0; i < fecconfig_.size(); i++) {
        if (fecconfig_[i].getFECNumber() == fecnumber)
            return fecconfig_[i].getType();
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FEC number: " << fecnumber << std::endl;

    assert(0);

    return 0;
}

std::string PixelFECConfig::URIFromFECNumber(unsigned int fecnumber) const {

    std::string mthn = "[PixelFECConfig::URIFromFECNumber()]\t\t\t    ";
    for (unsigned int i = 0; i < fecconfig_.size(); i++) {
        if (fecconfig_[i].getFECNumber() == fecnumber)
            return fecconfig_[i].getURI();
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FEC number: " << fecnumber << std::endl;

    assert(0);

    return 0;
}

unsigned int PixelFECConfig::VMEBaseAddressFromFECNumber(unsigned int fecnumber) const {

    std::string mthn = "[PixelFECConfig::VMEBaseAddressFromFECNumber()]\t\t    ";
    for (unsigned int i = 0; i < fecconfig_.size(); i++) {
        if (fecconfig_[i].getFECNumber() == fecnumber)
            return fecconfig_[i].getVMEBaseAddress();
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FEC number: " << fecnumber << std::endl;

    assert(0);

    return 0;
}

//=============================================================================================
void PixelFECConfig::writeASCII(std::string dir) const {

    if (dir != "")
        dir += "/";
    std::string filename = dir + "fecconfig.dat";
    std::ofstream out(filename.c_str());

    std::vector<PixelFECParameters>::const_iterator i = fecconfig_.begin();

    out << "#FEC number     crate     vme base address     type    URI" << endl;
    for (; i != fecconfig_.end(); ++i) {
        out << i->getFECNumber() << "               "
            << i->getCrate() << "         "
            << "0x" << hex << i->getVMEBaseAddress() << dec << "     "
            << i->getType() << "     "
            << i->getURI() << endl;
    }
    out.close();
}

//=============================================================================================
void PixelFECConfig::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[[PixelFECConfig::writeXMLHeader()]\t\t\t    ";
    std::string mthn = s.str();
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_FEC_Config_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_PXLFEC_CONFIG</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel FEC Configuration</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel FEC Configuration</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel FEC Configuration Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}

//=============================================================================================
void PixelFECConfig::writeXML(std::ofstream *outstream) const {
    std::string mthn = "[PixelFECConfig::writeXML()]\t\t\t    ";

    std::vector<PixelFECParameters>::const_iterator i = fecconfig_.begin();

    for (; i != fecconfig_.end(); ++i) {
        *outstream << "        <DATA>" << std::endl;
        *outstream << "            <PXLFEC_NUMBER>" << i->getFECNumber() << "</PXLFEC_NUMBER>" << std::endl;
        *outstream << "            <CRATE_NUMBER>" << i->getCrate() << "</CRATE_NUMBER>" << std::endl;
        *outstream << "            <VME_BASE_ADDRS>0x" << hex << i->getVMEBaseAddress() << dec << "</VME_BASE_ADDRS>" << std::endl;
        *outstream << "            <TYPE>" << i->getType() << "</TYPE>" << std::endl;
        *outstream << "            <URI>" << i->getURI() << "</URI>" << std::endl;
        *outstream << "        </DATA>" << std::endl;
    }
}

//=============================================================================================
void PixelFECConfig::writeXMLTrailer(std::ofstream *outstream) const {
    std::string mthn = "[PixelFECConfig::writeXMLTrailer()]\t\t\t    ";

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT> " << std::endl;

    outstream->close();
}
