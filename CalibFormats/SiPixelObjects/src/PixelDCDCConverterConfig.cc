//
// This class provides the mapping between
// modules , the dcdc converters, and power groups
//
//
//

#include "CalibFormats/SiPixelObjects/interface/PixelDCDCConverterConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"

#include <sstream>
#include <cassert>
#include <stdexcept>

using namespace pos;
using namespace std;

PixelDCDCConverterConfig::PixelDCDCConverterConfig(std::vector<std::vector<std::string> > &tableMat)
    : PixelConfigBase(" ", " ", " ") {

    std::string mthn = "[PixelDCDCConverterConfig::PixelDCDCConverterConfig()]\t\t\t    ";
    std::vector<std::string> ins = tableMat[0];
    std::map<std::string, int> colM;
    std::vector<std::string> colNames;

    colNames.push_back("DCDC_CONVERTER");
    colNames.push_back("DEFAULT_VALUE");
    colNames.push_back("CONFIGURATION");
    colNames.push_back("PGPIA_ADDR");
    colNames.push_back("PG_BIT");
    colNames.push_back("ENPIA_ADDR");
    colNames.push_back("EN_BIT");

    for (unsigned int c = 0; c < ins.size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    } //end for

    for (unsigned int n = 0; n < colNames.size(); n++) {
        if (colM.find(colNames[n]) == colM.end()) {
            std::cerr << __LINE__ << "]\t" << mthn
                      << "Couldn't find in the database the column with name " << colNames[n] << std::endl;
            assert(0);
        }
    }

    std::string dcdcname;
    bool defaultstate;
    std::string configuration;
    int pgpiach;
    std::bitset<8> pgbit;
    int enpiach;
    std::bitset<8> enbit;

    for (unsigned int r = 1; r < tableMat.size(); r++) { //Goes to every row of the Matrix
        dcdcname = tableMat[r][colM["DCDC_CONVERTER"]];
        defaultstate = tableMat[r][colM["DEFAULT_VALUE"]] == "1";
        configuration = tableMat[r][colM["CONFIGURATION"]];
        pgpiach = atoi(tableMat[r][colM["PGPIA_ADDR"]].c_str());
        pgbit = std::bitset<8>(tableMat[r][colM["PG_BIT"]]);
        enpiach = atoi(tableMat[r][colM["ENPIA_ADDR"]].c_str());
        enbit = std::bitset<8>(tableMat[r][colM["EN_BIT"]]);

        std::pair<int, bitset<8> > pg(pgpiach, pgbit);
        std::pair<int, bitset<8> > en(enpiach, enbit);
        mapPG_[dcdcname] = pg;
        mapEN_[dcdcname] = en;
        mapDefault_[dcdcname] = defaultstate;
        mapConfiguration_[dcdcname] = configuration;

        std::pair<std::string, int> pgpiachannel(configuration, pgpiach);
        std::pair<std::string, int> enpiachannel(configuration, enpiach);

        if (mapEnDCDCOnPiaCh_.find(enpiachannel) == mapEnDCDCOnPiaCh_.end()) {
            mapEnDCDCOnPiaCh_[enpiachannel] = std::set<std::string>();
        }
        mapEnDCDCOnPiaCh_[enpiachannel].insert(dcdcname);

        if (mapPgDCDCOnPiaCh_.find(pgpiachannel) == mapPgDCDCOnPiaCh_.end())
            mapPgDCDCOnPiaCh_[pgpiachannel] = std::set<std::string>();
        mapPgDCDCOnPiaCh_[pgpiachannel].insert(dcdcname);
    } //end for r
} //end constructor
//*****************************************************************************

PixelDCDCConverterConfig::PixelDCDCConverterConfig(std::string filename)
    : PixelConfigBase(" ", " ", " ") {

    std::string mthn = "[PixelDCDCConverterConfig::PixelDCDCConverterConfig()]\t\t\t    ";
    std::ifstream in(filename.c_str());

    if (!in.good()) {
        std::cout << __LINE__ << "]\t" << mthn << "Could not open: " << filename << std::endl;
        throw std::runtime_error("Failed to open file " + filename);
    } else {
        std::cout << __LINE__ << "]\t" << mthn << "Reading from: " << filename << std::endl;
    }

    std::string dummy;

    //this is for the comment line in the beginning of each ASCII file
    in >> dummy;
    in >> dummy;
    in >> dummy;
    in >> dummy;
    in >> dummy;
    in >> dummy;
    in >> dummy;
    in >> dummy;

    do {

        std::string dcdcname;
        std::string defaultstatestring;
        std::string configuration;
        bool defaultstate;
        int pgpiach;
        std::bitset<8> pgbit;
        int enpiach;
        std::bitset<8> enbit;

        in >> dcdcname;
        in >> defaultstatestring;
        defaultstate = defaultstatestring == "1";
        in >> configuration;
        in >> std::hex >> pgpiach;
        in >> pgbit;
        in >> std::hex >> enpiach;
        in >> enbit;

        if (!in.eof()) {
            std::pair<int, bitset<8> > pg(pgpiach, pgbit);
            std::pair<int, bitset<8> > en(enpiach, enbit);
            mapPG_[dcdcname] = pg;
            mapEN_[dcdcname] = en;
            mapDefault_[dcdcname] = defaultstate;
            mapConfiguration_[dcdcname] = configuration;

            std::pair<std::string, int> pgpiachannel(configuration, pgpiach);
            std::pair<std::string, int> enpiachannel(configuration, enpiach);

            if (mapEnDCDCOnPiaCh_.find(enpiachannel) == mapEnDCDCOnPiaCh_.end())
                mapEnDCDCOnPiaCh_[enpiachannel] = std::set<std::string>();
            mapEnDCDCOnPiaCh_[enpiachannel].insert(dcdcname);

            if (mapPgDCDCOnPiaCh_.find(pgpiachannel) == mapPgDCDCOnPiaCh_.end())
                mapPgDCDCOnPiaCh_[pgpiachannel] = std::set<std::string>();
            mapPgDCDCOnPiaCh_[pgpiachannel].insert(dcdcname);
        }

    } while (!in.eof());

    //test section:
    //test(*this);
}

PixelDCDCConverterConfig::~PixelDCDCConverterConfig() {}

void PixelDCDCConverterConfig::writeASCII(std::string dir) const {

    std::string mthn = "[PixelDCDCConverterConfig::writeASCII()]\t\t\t\t    ";
    if (dir != "")
        dir += "/";
    string filename = dir + "dcdcconfig.dat";

    ofstream out(filename.c_str());
    if (!out.good()) {
        cout << __LINE__ << "]\t" << mthn << "Could not open file: " << filename << endl;
        assert(0);
    }

    out << "# DCDCname    Default    Configuration    PGPIAaddress    PGbit    ENPIAaddress    ENbit" << endl;
    std::map<std::string, std::pair<int, std::bitset<8> > >::const_iterator iPG = mapPG_.begin();
    for (; iPG != mapPG_.end(); ++iPG) {
        std::map<std::string, std::pair<int, std::bitset<8> > >::const_iterator iEN = mapEN_.find(iPG->first);
        assert(iEN != mapEN_.end());
        std::map<std::string, bool>::const_iterator iDef = mapDefault_.find(iPG->first);
        assert(iDef != mapDefault_.end());
        std::map<std::string, std::string>::const_iterator iCon = mapConfiguration_.find(iPG->first);
        assert(iCon != mapConfiguration_.end());
        out << iPG->first << "       "
            << iDef->second << "       "
            << iCon->second << "       "
            << iPG->second.first << "       "
            << iPG->second.second << "       "
            << iEN->second.first << "       "
            << iEN->second.second << endl;
    }
    out.close();
}

//=============================================================================================
void PixelDCDCConverterConfig::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::string mthn = "[PixelDCDCConverterConfig::writeXMLHeader()]\t\t\t    ";
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_DCDC_CONF_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << __LINE__ << "]\t" << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_DCDC_CONF</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel DCDC Configuration</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel DCDC Configuration</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel DCDC Configuration Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}

//=============================================================================================
void PixelDCDCConverterConfig::writeXML(std::ofstream *outstream) const {
    std::string mthn = "[PixelDCDCConverterConfig::writeXML()]\t\t\t    ";

    std::map<std::string, std::pair<int, std::bitset<8> > >::const_iterator iPG = mapPG_.begin();
    for (; iPG != mapPG_.end(); ++iPG) {
        std::map<std::string, std::pair<int, std::bitset<8> > >::const_iterator iEN = mapEN_.find(iPG->first);
        assert(iEN != mapEN_.end());
        std::map<std::string, bool>::const_iterator iDef = mapDefault_.find(iPG->first);
        assert(iDef != mapDefault_.end());
        std::map<std::string, std::string>::const_iterator iCon = mapConfiguration_.find(iPG->first);
        assert(iCon != mapConfiguration_.end());
        *outstream << "        <DATA>" << std::endl;
        *outstream << "            <DCDC_CONVERTER>" << iPG->first << "</DCDC_CONVERTER>" << std::endl;
        *outstream << "            <DEFAULT_VALUE>" << iDef->second << "</DEFAULT_VALUE>" << std::endl;
        *outstream << "            <CONFIGURATION>" << iCon->second << "</CONFIGURATION>" << std::endl;
        *outstream << "            <PGPIA_ADDR>" << iPG->second.first << "</PGPIA_ADDR>" << std::endl;
        *outstream << "            <PG_BIT>" << iPG->second.second << "</PG_BIT>" << std::endl;
        *outstream << "            <ENPIA_ADDR>" << iEN->second.first << "</ENPIA_ADDR>" << std::endl;
        *outstream << "            <EN_BIT>" << iEN->second.second << "</EN_BIT>" << std::endl;
        *outstream << "        </DATA>" << std::endl;
    }
}

//=============================================================================================
void PixelDCDCConverterConfig::writeXMLTrailer(std::ofstream *outstream) const {
    std::string mthn = "[PixelDCDCConverterConfig::writeXMLTrailer()]\t\t\t    ";

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT> " << std::endl;

    outstream->close();
}

const std::pair<int, std::bitset<8> > PixelDCDCConverterConfig::getPgChannelAndBit(const std::string &dcdcname) const {
    std::map<std::string, std::pair<int, std::bitset<8> > >::const_iterator iPG = mapPG_.find(dcdcname);
    assert(iPG != mapPG_.end());
    return (iPG->second);
}

const std::pair<int, std::bitset<8> > PixelDCDCConverterConfig::getEnChannelAndBit(const std::string &dcdcname) const {
    std::map<std::string, std::pair<int, std::bitset<8> > >::const_iterator iEN = mapEN_.find(dcdcname);
    assert(iEN != mapEN_.end());
    return (iEN->second);
}

const bool PixelDCDCConverterConfig::getDefault(const std::string &dcdcname) const {
    std::map<std::string, bool>::const_iterator iDef = mapDefault_.find(dcdcname);
    assert(iDef != mapDefault_.end());
    return (iDef->second);
}

const std::string PixelDCDCConverterConfig::getConfiguration(const std::string &dcdcname) const {
    std::map<std::string, std::string>::const_iterator iCon = mapConfiguration_.find(dcdcname);
    assert(iCon != mapConfiguration_.end());
    return (iCon->second);
}

//returns a logical or of all EN bits of dcdc converters in the set
const std::bitset<8> PixelDCDCConverterConfig::getAllEnBits(const std::string &configuration, int &piachannel) const {
    std::pair<std::string, int> key(configuration, piachannel);
    std::set<std::string> dcdcnames = mapEnDCDCOnPiaCh_.find(key)->second;
    std::set<std::string>::const_iterator idcdc = dcdcnames.begin();
    std::bitset<8> result;
    for (; idcdc != dcdcnames.end(); idcdc++) {
        result |= mapEN_.find(*idcdc)->second.second;
    }
    return (result);
}

//returns a logical or of all PG bits of dcdc converters in the set
const std::bitset<8> PixelDCDCConverterConfig::getAllPgBits(const std::string &configuration, int &piachannel) const {
    std::pair<std::string, int> key(configuration, piachannel);
    std::set<std::string> dcdcnames = mapPgDCDCOnPiaCh_.find(key)->second;
    std::set<std::string>::const_iterator idcdc = dcdcnames.begin();
    std::bitset<8> result;
    for (; idcdc != dcdcnames.end(); idcdc++) {
        result |= mapPG_.find(*idcdc)->second.second;
    }
    return (result);
}

void PixelDCDCConverterConfig::test(PixelDCDCConverterConfig &classundertest) const {
    std::set<std::string> dcdcnames;
    dcdcnames.insert("BPix_DCDC1");
    dcdcnames.insert("BPix_DCDC2");
    dcdcnames.insert("BPix_DCDC3");
    dcdcnames.insert("BPix_DCDC4");
    dcdcnames.insert("BPix_DCDC5");
    dcdcnames.insert("BPix_DCDC6");
    dcdcnames.insert("BPix_DCDC7");
    dcdcnames.insert("BPix_DCDC8");
    dcdcnames.insert("BPix_DCDC9");
    dcdcnames.insert("BPix_DCDC10");
    dcdcnames.insert("BPix_DCDC11");
    dcdcnames.insert("BPix_DCDC12");
    dcdcnames.insert("BPix_DCDC13");
    dcdcnames.insert("FPix_DCDC1");
    dcdcnames.insert("FPix_DCDC2");
    dcdcnames.insert("FPix_DCDC3");
    dcdcnames.insert("FPix_DCDC4");
    dcdcnames.insert("FPix_DCDC5");
    dcdcnames.insert("FPix_DCDC6");

    std::set<std::string>::const_iterator dcdcname = dcdcnames.begin();
    for (; dcdcname != dcdcnames.end(); dcdcname++) {
        std::pair<int, std::bitset<8> > pgchannelandbit = classundertest.getPgChannelAndBit(*dcdcname);
        std::pair<int, std::bitset<8> > enchannelandbit = classundertest.getEnChannelAndBit(*dcdcname);
        bool def = classundertest.getDefault(*dcdcname);
        std::string configuration = classundertest.getConfiguration(*dcdcname);
        std::cout << *dcdcname << " "
                  << def << " "
                  << configuration << " "
                  << pgchannelandbit.first << " "
                  << pgchannelandbit.second << " "
                  << enchannelandbit.first << " "
                  << enchannelandbit.second << " " << std::endl;
    }

    int ch30 = 0x30;
    int ch31 = 0x31;
    int ch32 = 0x32;
    int ch33 = 0x33;

    std::bitset<8> allEnBits30 = classundertest.getAllEnBits("BPix", ch30);
    std::bitset<8> allEnBits31 = classundertest.getAllEnBits("BPix", ch31);
    std::bitset<8> allEnBits32 = classundertest.getAllEnBits("BPix", ch32);
    std::bitset<8> allEnBits33 = classundertest.getAllEnBits("BPix", ch33);

    std::bitset<8> allPgBits30 = classundertest.getAllPgBits("BPix", ch30);
    std::bitset<8> allPgBits31 = classundertest.getAllPgBits("BPix", ch31);
    std::bitset<8> allPgBits32 = classundertest.getAllPgBits("BPix", ch32);
    std::bitset<8> allPgBits33 = classundertest.getAllPgBits("BPix", ch33);

    std::cout << "BPix" << std::endl;
    std::cout << "ch 0x30: allEnable:    " << allEnBits30 << std::endl;
    std::cout << "         allPowerGood: " << allPgBits30 << std::endl;
    std::cout << "ch 0x31: allEnable:    " << allEnBits31 << std::endl;
    std::cout << "         allPowerGood: " << allPgBits31 << std::endl;
    std::cout << "ch 0x32: allEnable:    " << allEnBits32 << std::endl;
    std::cout << "         allPowerGood: " << allPgBits32 << std::endl;
    std::cout << "ch 0x33: allEnable:    " << allEnBits33 << std::endl;
    std::cout << "         allPowerGood: " << allPgBits33 << std::endl;

    std::bitset<8> allEnBits31F = classundertest.getAllEnBits("FPix", ch31);
    std::bitset<8> allEnBits32F = classundertest.getAllEnBits("FPix", ch32);
    std::bitset<8> allEnBits33F = classundertest.getAllEnBits("FPix", ch33);

    std::bitset<8> allPgBits31F = classundertest.getAllPgBits("FPix", ch31);
    std::bitset<8> allPgBits32F = classundertest.getAllPgBits("FPix", ch32);
    std::bitset<8> allPgBits33F = classundertest.getAllPgBits("FPix", ch33);

    std::cout << "FPix" << std::endl;
    std::cout << "ch 0x31: allEnable:    " << allEnBits31F << std::endl;
    std::cout << "         allPowerGood: " << allPgBits31F << std::endl;
    std::cout << "ch 0x32: allEnable:    " << allEnBits32F << std::endl;
    std::cout << "         allPowerGood: " << allPgBits32F << std::endl;
    std::cout << "ch 0x33: allEnable:    " << allEnBits33F << std::endl;
    std::cout << "         allPowerGood: " << allPgBits33F << std::endl;

    classundertest.writeASCII("/afs/cern.ch/user/v/vormwald/scratch1/TriDAS/pixel");
    std::cout << "File written to /afs/cern.ch/user/v/vormwald/scratch1/TriDAS/pixel" << std::endl;
}
