//
// This class stores the information about a TKFEC.
// This include the number, crate, and base address
//
//

#include "CalibFormats/SiPixelObjects/interface/PixelTKFECConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"
#include <fstream>
#include <sstream>
#include <map>
#include <assert.h>
#include <stdexcept>

using namespace pos;
using namespace std;

PixelTKFECConfig::PixelTKFECConfig(std::vector<std::vector<std::string> > &tableMat)
    : PixelConfigBase(" ", " ", " ") {
    std::map<std::string, int> colM;
    std::vector<std::string> colNames;

    colNames.push_back("TKFEC_NAME");
    colNames.push_back("CRATE_NUMBER");
    colNames.push_back("TYPE");
    colNames.push_back("URI");

    for (unsigned int c = 0; c < tableMat[0].size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    }
    for (unsigned int n = 0; n < colNames.size(); n++) {
        if (colM.find(colNames[n]) == colM.end()) {
            std::cerr << "[PixelTKFECConfig::PixelTKFECConfig()]\tCouldn't find in the database the column with name " << colNames[n] << std::endl;
            assert(0);
        }
    }

    for (unsigned int r = 1; r < tableMat.size(); r++) { //Goes to every row of the Matrix
        std::string TKFECID = tableMat[r][colM["TKFEC_NAME"]].c_str();
        unsigned int crate = atoi(tableMat[r][colM["CRATE_NUMBER"]].c_str());
        std::string type = tableMat[r][colM["TYPE"]].c_str();
        unsigned int address = 0;
        std::string uri = tableMat[r][colM["URI"]].c_str();

        PixelTKFECParameters tmp;
        tmp.setTKFECParameters(TKFECID, crate, type, address, uri);

        TKFECconfig_.push_back(tmp);
    }
} // end contructor

//****************************************************************************************

PixelTKFECConfig::PixelTKFECConfig(std::string filename)
    : PixelConfigBase(" ", " ", " ") {

    std::string mthn = "]\t[PixelTKFECConfig::PixelTKFECConfig()]\t\t\t    ";
    std::ifstream in(filename.c_str());

    if (!in.good()) {
        std::cout << __LINE__ << mthn << "Could not open: " << filename << std::endl;
        throw std::runtime_error("Failed to open file " + filename);
    } else {
        std::cout << __LINE__ << mthn << "Opened: " << filename << std::endl;
    }

    std::string dummy;

    getline(in, dummy); // skip the column headings

    do {

        std::string TKFECID;
        unsigned int crate;
        std::string type;
        unsigned int address;
        std::string uri;

        in >> TKFECID;
        in >> crate;
        in >> type;

        if (type == "VME" || type == "PCI") {
            in >> std::hex >> address >> std::dec;
        } else if (type == "GLIB" || type == "CTA") {
            in >> uri;
            address = 0;
        }

        if (!in.eof()) {
            //std::cout << TKFECID <<" "<< crate << " "
            //      << std::hex << vme_base_address<<std::dec<<std::endl;

            PixelTKFECParameters tmp;

            tmp.setTKFECParameters(TKFECID, crate, type, address, uri);

            TKFECconfig_.push_back(tmp);
        }

    } while (!in.eof());
    in.close();
}

PixelTKFECConfig::~PixelTKFECConfig() {}

void PixelTKFECConfig::writeASCII(std::string dir) const {

    if (dir != "")
        dir += "/";
    string filename = dir + "tkfecconfig.dat";

    ofstream out(filename.c_str());
    if (!out.good()) {
        cout << "Could not open file:" << filename << endl;
        assert(0);
    }

    out << "#TKFEC ID     crate     VME/PCI    slot/address" << endl;
    for (unsigned int i = 0; i < TKFECconfig_.size(); i++) {
        out << TKFECconfig_[i].getTKFECID() << "          "
            << TKFECconfig_[i].getCrate() << "          ";
        const std::string &type = TKFECconfig_[i].getType();
        if (type == "GLIB" || type == "CTA") {
            out << type << " " << TKFECconfig_[i].getURI() << endl;
        } else {
            if (TKFECconfig_[i].getType() == "PCI") {
                out << "PCI       ";
            } else {
                out << "          ";
            }
            out << "0x" << hex << TKFECconfig_[i].getAddress() << dec << endl;
        }
    }
    out.close();
}

//std::ostream& operator<<(std::ostream& s, const PixelTKFECConfig& table){

//for (unsigned int i=0;i<table.translationtable_.size();i++){
//	s << table.translationtable_[i]<<std::endl;
//   }
// return s;

//}

unsigned int PixelTKFECConfig::getNTKFECBoards() const {
    return TKFECconfig_.size();
}

std::string PixelTKFECConfig::getTKFECID(unsigned int i) const {

    assert(i < TKFECconfig_.size());
    return TKFECconfig_[i].getTKFECID();
}

unsigned int PixelTKFECConfig::getCrate(unsigned int i) const {

    assert(i < TKFECconfig_.size());
    return TKFECconfig_[i].getCrate();
}

std::string PixelTKFECConfig::getType(unsigned int i) const {

    assert(i < TKFECconfig_.size());
    return TKFECconfig_[i].getType();
}

unsigned int PixelTKFECConfig::getAddress(unsigned int i) const {

    assert(i < TKFECconfig_.size());
    return TKFECconfig_[i].getAddress();
}

std::string PixelTKFECConfig::getURI(unsigned int i) const {

    assert(i < TKFECconfig_.size());
    return TKFECconfig_[i].getURI();
}

unsigned int PixelTKFECConfig::crateFromTKFECID(std::string TKFECID) const {

    for (unsigned int i = 0; i < TKFECconfig_.size(); i++) {
        if (TKFECconfig_[i].getTKFECID() == TKFECID)
            return TKFECconfig_[i].getCrate();
    }

    std::cout << "Could not find TKFEC ID:" << TKFECID << std::endl;

    assert(0);

    return 0;
}

std::string PixelTKFECConfig::typeFromTKFECID(std::string TKFECID) const {

    for (unsigned int i = 0; i < TKFECconfig_.size(); i++) {
        if (TKFECconfig_[i].getTKFECID() == TKFECID)
            return TKFECconfig_[i].getType();
    }

    std::cout << "Could not find TKFEC ID:" << TKFECID << std::endl;

    assert(0);

    return 0;
}

unsigned int PixelTKFECConfig::addressFromTKFECID(std::string TKFECID) const {

    for (unsigned int i = 0; i < TKFECconfig_.size(); i++) {
        if (TKFECconfig_[i].getTKFECID() == TKFECID)
            return TKFECconfig_[i].getAddress();
    }

    std::cout << "Could not find TKFEC ID:" << TKFECID << std::endl;

    assert(0);

    return 0;
}

std::string PixelTKFECConfig::URIFromTKFECID(std::string TKFECID) const {

    for (unsigned int i = 0; i < TKFECconfig_.size(); i++) {
        if (TKFECconfig_[i].getTKFECID() == TKFECID)
            return TKFECconfig_[i].getURI();
    }

    std::cout << "Could not find TKFEC ID:" << TKFECID << std::endl;

    assert(0);

    return 0;
}

//=============================================================================================
void PixelTKFECConfig::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[[PixelTKFECConfig::writeXMLHeader()]\t\t\t    ";
    std::string mthn = s.str();
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_TkfecConfig_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_TKFEC_CONFIG</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel Tracker FEC Config</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel TrkFEC Config</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel Tracker FEC Config Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}

//=============================================================================================
void PixelTKFECConfig::writeXML(std::ofstream *outstream) const {
    std::string mthn = "[PixelTKFECConfig::writeXML()]\t\t\t    ";

    for (unsigned int i = 0; i < TKFECconfig_.size(); i++) {
        *outstream << "  <DATA>" << std::endl;
        *outstream << "   <TKFEC_NAME>" << TKFECconfig_[i].getTKFECID() << "</TKFEC_NAME>" << std::endl;
        *outstream << "   <CRATE_NUMBER>" << TKFECconfig_[i].getCrate() << "</CRATE_NUMBER>" << std::endl;
        *outstream << "   <TYPE>" << TKFECconfig_[i].getType() << "</TYPE>" << std::endl;
        *outstream << "   <URI>" << TKFECconfig_[i].getURI() << "</URI>" << std::endl;
        *outstream << "  </DATA>" << std::endl;
    }
}

//=============================================================================================
void PixelTKFECConfig::writeXMLTrailer(std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[PixelTKFECConfig::writeXMLTrailer()]\t\t\t    ";
    std::string mthn = s.str();

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT>" << std::endl;

    outstream->close();
}
