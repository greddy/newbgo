//
// This class provide a base class for the
// pixel ROC DAC data for the pixel FEC configuration
//
//
//
//

#include "CalibFormats/SiPixelObjects/interface/PixelDACSettings.h"
#include "CalibFormats/SiPixelObjects/interface/PixelROCDACSettings.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"
#include <fstream>
#include <iostream>
#include <ios>
#include <assert.h>
#include <stdexcept>
#include <map>
#include <sstream>
#include <sys/time.h>
#include <cstdlib>

using namespace pos;

namespace {
  const bool readTemperatures = false;
  //const bool readTemperatures = true;
  //const int temperatureReg = 0x9; // hardwire to fixed reference voltage 0x8 + 0x1
  const int temperatureReg = 0x1; // hardwire to the usefull range, change to range 1, Marco's request, 25/10/11
  const bool dump=false;
}

PixelDACSettings::PixelDACSettings(std::string filename)
    : PixelConfigBase("", "", "") {

    std::string mthn = "[PixelDACSettings::PixelDACSettings()]\t\t\t    ";

    if (filename[filename.size() - 1] == 't') {

        std::ifstream in(filename.c_str());

        if (!in.good()) {
            std::cout << __LINE__ << "]\t" << mthn << "Could not open: " << filename << std::endl;
            throw std::runtime_error("Failed to open file " + filename);
        } else {
            // std::cout << "Opened:"<<filename<<std::endl;
        }

        dacsettings_.clear();

        std::string tag;

        in >> tag;

        while (!in.eof()) {

            PixelROCName rocid(in);

            //    std::cout << "[PixelDACSettings::PixelDACSettings()] DAC setting ROC id:"<<rocid<<std::endl;

            PixelROCDACSettings tmp;

            tmp.read(in, rocid);

            //    std::cout << "[PixelDACSettings::PixelDACSettings()] DACSetting: " << std::endl << tmp << std::endl ;
            dacsettings_.push_back(tmp);

            in >> tag;

            assert(dacsettings_.size() < 100);
        }

        in.close();

    } else {

        std::ifstream in(filename.c_str(), std::ios::binary);

        if (!in.good()) {
            std::cout << __LINE__ << "]\t" << mthn << "Could not open: " << filename << std::endl;
            assert(0);
        } else {
            std::cout << __LINE__ << "]\t" << mthn << "Opened: " << filename << std::endl;
        }

        char nchar;

        in.read(&nchar, 1);

        std::string s1;

        //wrote these lines of code without ref. needs to be fixed
        for (int i = 0; i < nchar; i++) {
            char c;
            in >> c;
            s1.push_back(c);
        }

        //std::cout << __LINE__ << "]\t" << mthn << "READ ROC name: " << s1 << std::endl;

        dacsettings_.clear();

        while (!in.eof()) {

            //std::cout << __LINE__ << "]\t" << mthn << "read s1   : " << s1    << std::endl;

            PixelROCName rocid(s1);

            //std::cout << __LINE__ << "]\t" << mthn << "read rocid: " << rocid << std::endl;

            PixelROCDACSettings tmp;

            tmp.readBinary(in, rocid);

            dacsettings_.push_back(tmp);

            in.read(&nchar, 1);

            s1.clear();

            if (in.eof())
                continue;

            //wrote these lines of code without ref. needs to be fixed
            for (int i = 0; i < nchar; i++) {
                char c;
                in >> c;
                s1.push_back(c);
            }
        }

        in.close();
    }

    //std::cout << __LINE__ << "]\t" << mthn << "Read dac settings for "<<dacsettings_.size()<<" ROCs"<<std::endl;
}
// modified by MR on 10-01-2008 14:48:19
PixelDACSettings::PixelDACSettings(PixelROCDACSettings &rocname)
    : PixelConfigBase("", "", "") {
          dacsettings_.push_back(rocname);
}

// modified by MR on 24-01-2008 14:27:35a
void PixelDACSettings::addROC(PixelROCDACSettings &rocname) {
    dacsettings_.push_back(rocname);
}

PixelDACSettings::PixelDACSettings(std::vector<std::vector<std::string> > &tableMat)
    : PixelConfigBase("", "", "") {
    std::vector<std::string> ins = tableMat[0];
    std::string mthn("[PixelDACSettings::PixelDACSettings()] ");
    std::string dacName;
    std::istringstream dbin;
    std::map<std::string, int> colM;
    std::vector<std::string> colNames;
    std::map<std::string, std::string> nameTranslation;

    colNames.push_back("ROC_NAME");
    colNames.push_back("VDD");
    colNames.push_back("VANA");
    colNames.push_back("VSH");
    colNames.push_back("VCOMP");
    colNames.push_back("VWLLPR");
    colNames.push_back("VWLLSH");
    colNames.push_back("VHLDDEL");
    colNames.push_back("VTRIM");
    colNames.push_back("VCTHR");
    colNames.push_back("VIBIAS_BUS");
    colNames.push_back("PHOFFSET");
    colNames.push_back("VCOMP_ADC");
    colNames.push_back("PHSCALE");
    colNames.push_back("VICOLOR");
    colNames.push_back("VCAL");
    colNames.push_back("CALDEL");
    colNames.push_back("TEMPRANGE");
    colNames.push_back("WBC");
    colNames.push_back("CHIPCONTREG");
    colNames.push_back("READBACK");

    nameTranslation["VDD"] = k_DACName_Vdd;
    nameTranslation["VANA"] = k_DACName_Vana;
    nameTranslation["VSH"] = k_DACName_Vsf;
    nameTranslation["VCOMP"] = k_DACName_Vcomp;
    nameTranslation["VWLLPR"] = k_DACName_VwllPr;
    nameTranslation["VWLLSH"] = k_DACName_VwllSh;
    nameTranslation["VHLDDEL"] = k_DACName_VHldDel;
    nameTranslation["VTRIM"] = k_DACName_Vtrim;
    nameTranslation["VCTHR"] = k_DACName_VcThr;
    nameTranslation["VIBIAS_BUS"] = k_DACName_VIbias_bus;
    nameTranslation["PHOFFSET"] = k_DACName_PHOffset;
    nameTranslation["VCOMP_ADC"] = k_DACName_Vcomp_ADC;
    nameTranslation["PHSCALE"] = k_DACName_PHScale;
    nameTranslation["VICOLOR"] = k_DACName_VIColOr;
    nameTranslation["VCAL"] = k_DACName_Vcal;
    nameTranslation["CALDEL"] = k_DACName_CalDel;
    nameTranslation["TEMPRANGE"] = k_DACName_TempRange;
    nameTranslation["WBC"] = k_DACName_WBC;
    nameTranslation["CHIPCONTREG"] = k_DACName_ChipContReg;
    nameTranslation["READBACK"] = k_DACName_Readback;

    for (unsigned int c = 0; c < ins.size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    } //end for
    for (unsigned int n = 0; n < colNames.size(); n++) {
        if (colM.find(colNames[n]) == colM.end()) {
            std::cerr << "[PixelDACSettings::PixelDACSettings()]\tCouldn't find in the database the column with name " << colNames[n] << std::endl;
            assert(0);
        }
    }

    dacsettings_.clear();
    for (unsigned int r = 1; r < tableMat.size(); r++) { //Goes to every row of the Matrix
        PixelROCName rocid(tableMat[r][colM["ROC_NAME"]]);
        PixelROCDACSettings tmp(rocid);
        std::ostringstream dacs("");
        for (unsigned int n = 1; n < colNames.size(); n++) {
            dacs << nameTranslation[colNames[n]] << ": " << atoi(tableMat[r][colM[colNames[n]]].c_str()) << std::endl;
        }
        dbin.str(dacs.str());
        tmp.read(dbin, rocid);
        dacsettings_.push_back(tmp);
    }
}

PixelROCDACSettings PixelDACSettings::getDACSettings(int ROCId) const {
    return dacsettings_[ROCId];
}

PixelROCDACSettings *PixelDACSettings::getDACSettings(PixelROCName name) {

    for (unsigned int i = 0; i < dacsettings_.size(); i++) {
        if (dacsettings_[i].getROCName() == name)
            return &(dacsettings_[i]);
    }

    return 0;
}

void PixelDACSettings::writeBinary(std::string filename) const {

    std::ofstream out(filename.c_str(), std::ios::binary);

    for (unsigned int i = 0; i < dacsettings_.size(); i++) {
        dacsettings_[i].writeBinary(out);
    }
}

void PixelDACSettings::writeASCII(std::string dir) const {

    std::string mthn = "[PixelDACSettings::writeASCII()]\t\t\t    ";
    PixelModuleName module(dacsettings_[0].getROCName().rocname());

    std::string filename = dir + "/ROC_DAC_module_" + module.modulename() + ".dat";
    std::cout << __LINE__ << "]\t" << mthn << "Writing to file " << filename << std::endl;
    std::ofstream out(filename.c_str());

    for (unsigned int i = 0; i < dacsettings_.size(); i++) {
        dacsettings_[i].writeASCII(out);
    }
}

//=============================================================================================
void PixelDACSettings::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[[PixelDACSettings::writeXMLHeader()]\t\t\t    ";
    std::string mthn = s.str();
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_ROC_DAC_Settings_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_ROC_DAC_SETTINGS</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel ROC DAC Settings</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel ROC DAC Settings</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel ROC DAC Settings Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}

//=============================================================================================
void PixelDACSettings::writeXML(std::ofstream *outstream) const {
    std::string mthn = "[PixelDACSettings::writeXML()]\t\t\t    ";

    for (unsigned int i = 0; i < dacsettings_.size(); i++) {
        dacsettings_[i].writeXML(outstream);
    }
}

//=============================================================================================
void PixelDACSettings::writeXMLTrailer(std::ofstream *outstream) const {
    std::string mthn = "[PixelDACSettings::writeXMLTrailer()]\t\t\t    ";

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT>" << std::endl;

    outstream->close();
}

/* O B S O L E T E -----

//=============================================================================================
void PixelDACSettings::writeXML(pos::PixelConfigKey key, int version, std::string path) const {
  std::string mthn = "[PixelDACSettings::writeXML()]\t\t\t    " ;
  std::stringstream fullPath ;

  PixelModuleName module(dacsettings_[0].getROCName().rocname());
  fullPath << path << "/dacsettings_" << module.modulename() << ".xml" ;
  std::cout << mthn << "Writing to: |" << fullPath.str()  << "|" << std::endl ;
  

  std::ofstream outstream(fullPath.str().c_str()) ;
  
  out << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>"          << std::endl ;
  out << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>"           << std::endl ;
  out << " <HEADER>"          << std::endl ;
  out << "  <TYPE>"          << std::endl ;
  out << "   <EXTENSION_TABLE_NAME>ROC_DAC_SETTINGS_COL</EXTENSION_TABLE_NAME>"   << std::endl ;
  out << "   <NAME>ROC DAC Settings Col</NAME>"   << std::endl ;
  out << "  </TYPE>"          << std::endl ;
  out << "  <RUN>"          << std::endl ;
  out << "   <RUN_TYPE>test</RUN_TYPE>"           << std::endl ;
  out << "   <RUN_NUMBER>1</RUN_NUMBER>"          << std::endl ;
  out << "   <RUN_BEGIN_TIMESTAMP>" << PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl ;
  out << "   <COMMENT_DESCRIPTION>Test of DAC Settings xml</COMMENT_DESCRIPTION>"  << std::endl ;
  out << "   <LOCATION>CERN TAC</LOCATION>"          << std::endl ;
  out << "   <CREATED_BY_USER>Dario Menasce</CREATED_BY_USER>"            << std::endl ;
  out << "  </RUN>"          << std::endl ;
  out << " </HEADER>"          << std::endl ;
  out << ""          << std::endl ;
  out << " <DATA_SET>"                                                              << std::endl ;
  out << "  <VERSION>" << version << "</VERSION>"                                   << std::endl ;
  out << " "                                                                        << std::endl ;
  out << "  <PART>"                                                                 << std::endl ;
  out << "   <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>"                               << std::endl ;
  out << "   <KIND_OF_PART>Detector ROOT</KIND_OF_PART>"                            << std::endl ;
  out << "  </PART>"                                                                << std::endl ;
  out << " "                                                                        << std::endl ;

  for(unsigned int i=0;i<dacsettings_.size();i++){
//    dacsettings_[i].writeXML(out, key, version, path);
  }

  out << " </DATA_SET>"                                                                          << std::endl ;
  out << "</ROOT>"                                                                               << std::endl ;

  out.close() ;
  std::cout << mthn << "Data written"                                                            << std::endl ;
}
*/

//=============================================================================================
void PixelDACSettings::setROCDisable(PixelFECConfigInterface *pixelFEC,
                                     PixelNameTranslation *trans, PixelDetectorConfig *detconfig) const {

    const bool bufferData = true;
    std::vector<unsigned int> dacs;
    for (unsigned int i = 0; i < dacsettings_.size(); i++) { // loop over ROCs

        bool disableRoc = rocIsDisabled(detconfig, dacsettings_[i].getROCName());
        dacsettings_[i].getDACs(dacs);
        PixelHdwAddress theROC = *(trans->getHdwAddress(dacsettings_[i].getROCName()));
        int controlreg = dacsettings_[i].getControlRegister();
        //std::cout<<"ROC= "<<dacsettings_[i].getROCName()<<" disabled "<<disableRoc<<" ctrlreg "<<controlreg<<std::endl;
        pixelFEC->progdac(theROC.mfec(), // Disable ALL ROCs
                          theROC.mfecchannel(),
                          theROC.hubaddress(),
                          theROC.portaddress(),
                          theROC.rocid(),
                          0xfd,
                          controlreg | 0x2, //=010 in binary. should disable the chip
                          bufferData);
    }

    if (bufferData) { // Send data to the FEC
        pixelFEC->qbufsend();
    }
}

//Execute this from DAC-B DDR Segment segment
void PixelDACSettings::setROCEnable(PixelFECConfigInterface *pixelFEC,
                                    PixelNameTranslation *trans, PixelDetectorConfig *detconfig,
                                    pos::PixelPortcardMap *portcardmap) const {

    const bool bufferData = true;
    const bool onlySEND = false;

    std::vector<unsigned int> dacs;
    unsigned int mfec, mfecchannel, hubaddress, aoh_ch;

    //they are same for all ROCs in the loop below, need these values for onlySEND command
    if (dacsettings_.size()) {
        PixelHdwAddress theroc = *(trans->getHdwAddress(dacsettings_[0].getROCName()));
        mfec = theroc.mfec();
        mfecchannel = theroc.mfecchannel();
        hubaddress = theroc.hubaddress();
        PixelChannel theChannel = trans->getChannelFromHdwAddress(theroc);
        std::pair<std::string, int> portCardAndAOH = portcardmap->PortCardAndAOH(theChannel);
        aoh_ch = portCardAndAOH.second;
    }

    if (!onlySEND) {
        for (unsigned int i = 0; i < dacsettings_.size(); i++) { // loop over ROCs

            bool disableRoc = rocIsDisabled(detconfig, dacsettings_[i].getROCName());
            if (disableRoc)
                continue; // skip disabled rocs

            dacsettings_[i].getDACs(dacs);
            PixelHdwAddress theROC = *(trans->getHdwAddress(dacsettings_[i].getROCName()));
            int controlreg = dacsettings_[i].getControlRegister();
            //std::cout<<"ROC= "<<dacsettings_[i].getROCName()<<" enabled "<<disableRoc<<" ctrlreg "<<controlreg<<std::endl;
            pixelFEC->progdac(theROC.mfec(), // Enable ROCs
                              theROC.mfecchannel(),
                              theROC.hubaddress(),
                              theROC.portaddress(),
                              theROC.rocid(),
                              0xfd,
                              controlreg, // Enable
                              bufferData);
        }
    }

    std::string offsetNode = pixelFEC->getDDRSegmentName(aoh_ch, "ROC_B");
    std::string maskBitNode = pixelFEC->getDDRMaskName(mfec, mfecchannel, aoh_ch, "ROC_B");

    if (onlySEND) { //Only Send DDR Data per module, not being used
        pixelFEC->confDataSend(mfec, mfecchannel, hubaddress, offsetNode, maskBitNode);
    } else { //Only Load Data to DDR, (send option is disabled)
        if (bufferData) {
            pixelFEC->ddrload(mfec, mfecchannel, hubaddress, offsetNode);
        }
    }
}
void PixelDACSettings::generateConfiguration(PixelFECConfigInterface *pixelFEC,
                                             PixelNameTranslation *trans,
                                             PixelDetectorConfig *detconfig,
                                             pos::PixelPortcardMap *portcardmap,
                                             bool HVon,
                                             std::string segment) const {

    bool bufferData = true;
    const bool onlySEND = false;
    bool doReadbackDAC = true; //turn this off if regular readback programming not needed
    int random = std::rand() % 5;

    std::vector<unsigned int> dacs;
    unsigned int mfec, mfecchannel, hubaddress, aoh_ch;

    //they are same for all ROCs in the loop below, need these values for onlySEND command
    if (dacsettings_.size()) {
        PixelHdwAddress theroc = *(trans->getHdwAddress(dacsettings_[0].getROCName()));
        mfec = theroc.mfec();
        mfecchannel = theroc.mfecchannel();
        hubaddress = theroc.hubaddress();
        PixelChannel theChannel = trans->getChannelFromHdwAddress(theroc);
        std::pair<std::string, int> portCardAndAOH = portcardmap->PortCardAndAOH(theChannel);
        aoh_ch = portCardAndAOH.second;
    }

    if (!onlySEND) {

        for (unsigned int i = 0; i < dacsettings_.size(); i++) { // loop over ROCs

            bool disableRoc = rocIsDisabled(detconfig, dacsettings_[i].getROCName());

            dacsettings_[i].getDACs(dacs);

            PixelHdwAddress theROC = *(trans->getHdwAddress(dacsettings_[i].getROCName()));

            // Set initially to low power mode
            int controlreg = dacsettings_[i].getControlRegister();
            if(dump) std::cout<<"ROC= "<<dacsettings_[i].getROCName()<<" HV? "<<HVon<<" disabled "<<disableRoc<<" ctrlreg "<<controlreg<<std::endl;
            pixelFEC->progdac(theROC.mfec(), // Disable ALL ROCs
                              theROC.mfecchannel(),
                              theROC.hubaddress(),
                              theROC.portaddress(),
                              theROC.rocid(),
                              0xfd,
                              //controlreg,
                              controlreg | 0x2, //=010 in binary. should disable the chip
                              bufferData);

            pixelFEC->progdac(theROC.mfec(), // Set vcolor to 0
                              theROC.mfecchannel(),
                              theROC.hubaddress(),
                              theROC.portaddress(),
                              theROC.rocid(),
                              21,
                              0,
                              bufferData);

            pixelFEC->progdac(theROC.mfec(), // Set all Vcth to 30
                              theROC.mfecchannel(),
                              theROC.hubaddress(),
                              theROC.portaddress(),
                              theROC.rocid(),
                              11,
                              30,
                              bufferData);

            if (!HVon || disableRoc) {
                dacs[11] = 30; //set Vcthr DAC to 30 (Vcthr is DAC 12=11+1)
                               //dacs[21]=0;  // vcolor=0
                               //std::cout<<" setting VcThr to "<<dacs[11]<<std::endl; //for debugging
            }

            pixelFEC->setAllDAC(theROC, dacs, bufferData);

            // start with no pixels on for calibration
            pixelFEC->clrcal(theROC.mfec(),
                             theROC.mfecchannel(),
                             theROC.hubaddress(),
                             theROC.portaddress(),
                             theROC.rocid(),
                             bufferData);

            if (!HVon || disableRoc) { //HV off
                //All ROCs are already disabled
                // disable all the double columns
                for (int dcol = 0; dcol < 26; dcol++) {
                    pixelFEC->dcolenable(theROC.mfec(),
                                         theROC.mfecchannel(),
                                         theROC.hubaddress(),
                                         theROC.portaddress(),
                                         theROC.rocid(),
                                         dcol,
                                         0,
                                         bufferData);
                }
            } else { // HV ON
                //std::cout << "[PixelDACSettings::generateConfiguration] HV on, ROC control reg to be set to: " <<  (controlreg) <<std::endl;
                int controlreg = dacsettings_[i].getControlRegister();
                pixelFEC->progdac(theROC.mfec(),
                                  theROC.mfecchannel(),
                                  theROC.hubaddress(),
                                  theROC.portaddress(),
                                  theROC.rocid(),
                                  0xfd,
                                  controlreg, // enable ROCs
                                  //controlreg | 0x2, //=010 in binary. should disable the chip
                                  bufferData);

                // enable all the double columns
                for (int dcol = 0; dcol < 26; dcol++) {
                    pixelFEC->dcolenable(theROC.mfec(),
                                         theROC.mfecchannel(),
                                         theROC.hubaddress(),
                                         theROC.portaddress(),
                                         theROC.rocid(),
                                         dcol,
                                         1,
                                         bufferData);
                }
            } //HV off

            if (doReadbackDAC) {
                int readbacks[5] = { 8, 9, 10, 11, 12 };
                int cyclic = random + i;
                if (cyclic > 4 && cyclic <= 9)
                    cyclic -= 5;
                else if (cyclic > 9 && cyclic <= 14)
                    cyclic -= 10;
                else if (cyclic > 14)
                    cyclic -= 15;
                if(dump) std::cout << pos::PixelTimeFormatter::getTime() << " readbacksetting " << readbacks[cyclic] << " " << dacsettings_[i].getROCName() << std::endl;
                pixelFEC->progdac(theROC.mfec(), // Set all readback register here
                                  theROC.mfecchannel(),
                                  theROC.hubaddress(),
                                  theROC.portaddress(),
                                  theROC.rocid(),
                                  k_DACAddress_Readback,
                                  readbacks[cyclic],
                                  bufferData);
            } else {
	      //std::cout<<" not RANDOM READBACK <========================================"<<std::endl;
	      pixelFEC->progdac(theROC.mfec(), // Set all readback register here
				theROC.mfecchannel(),
				theROC.hubaddress(),
				theROC.portaddress(),
				theROC.rocid(),
				k_DACAddress_Readback,
				0,
				bufferData);
	      //std::cout<<" vcal "<<k_DACAddress_Vcal<<" "<<dacs[k_DACAddress_Vcal-1]<<std::endl;
	      pixelFEC->progdac(theROC.mfec(), // Set the vcal register 
	      			theROC.mfecchannel(),
	      			theROC.hubaddress(),
	      			theROC.portaddress(),
	      			theROC.rocid(),
	      			k_DACAddress_Vcal,
	      			dacs[k_DACAddress_Vcal-1],
	      			bufferData);
	    }

#if 0
        // Now program (again) the temperature register to make sure it is the last one
        // and appears in the LastDAC
        if(readTemperatures) {
          assert(0);
          //     std::cout<<"ROC="<<dacsettings_[i].getROCName()<<" ; VcThr set to "<<dacs[11]
          //       << " ROC control reg to be set to: " <<  dacs[28] <<" LastDAC=Temp"<<std::endl;
          if( (theROC.mfec()==1) && (theROC.mfecchannel()==1) &&  (theROC.hubaddress()==0) &&
          (theROC.portaddress()==0) &&  (theROC.rocid()) )
          std::cout<<"ROC="<<dacsettings_[i].getROCName()<< " ROC control reg to be set to: "
          <<  dacs[28] <<" LastDAC=Temp "<<temperatureReg<<std::endl;
          //int temperatureReg = dacs[26];  // overwrite with the number from DB
          pixelFEC->progdac(theROC.mfec(),
                            theROC.mfecchannel(),
                            theROC.hubaddress(),
                            theROC.portaddress(),
                            theROC.rocid(),
                            0x1B,
                            temperatureReg,bufferData);
        } else {
          //      std::cout<<"ROC="<<dacsettings_[i].getROCName()<<" ; VcThr set to "<<dacs[11]
          //	       << " ROC control reg to be set to: " <<  dacs[28] <<" LastDAC=Vcal"<<std::endl;
          if( (theROC.mfec()==1) && (theROC.mfecchannel()==1) &&  (theROC.hubaddress()==0) &&
          (theROC.portaddress()==0) &&  (theROC.rocid()) )
          std::cout<<"ROC="<<dacsettings_[i].getROCName()
          << " ROC control reg to be set to: " <<  dacs[28] <<" LastDAC=Vcal"<<std::endl;
          // VCAL
          pixelFEC->progdac(theROC.mfec(),
                            theROC.mfecchannel(),
                            theROC.hubaddress(),
                            theROC.portaddress(),
                            theROC.rocid(),
                            0x19,
                            200,
                            bufferData);
        }
#endif

        } // end ROC loop
    }

    std::string offsetNode = pixelFEC->getDDRSegmentName(aoh_ch, segment);
    std::string maskBitNode = pixelFEC->getDDRMaskName(mfec, mfecchannel, aoh_ch, segment);

    if (onlySEND) { //Only Send DDR Data per module, not being used
        pixelFEC->confDataSend(mfec, mfecchannel, hubaddress, offsetNode, maskBitNode);
    } else { //Only Load Data to DDR, (send option is disabled)
        if (bufferData) {
            pixelFEC->ddrload(mfec, mfecchannel, hubaddress, offsetNode);
        }
    }
}

void PixelDACSettings::loadDDRSetDAC(PixelFECConfigInterface *pixelFEC, PixelNameTranslation *trans,
                                     PixelDetectorConfig *detconfig, pos::PixelPortcardMap *portcardmap,
                                     unsigned int dacAddress, unsigned int dacValue, std::string segment) const {
    bool bufferData = true;

    std::vector<unsigned int> dacs;
    unsigned int mfec, mfecchannel, hubaddress, aoh_ch;

    if (dacsettings_.size()) {
        PixelHdwAddress theroc = *(trans->getHdwAddress(dacsettings_[0].getROCName()));
        mfec = theroc.mfec();
        mfecchannel = theroc.mfecchannel();
        hubaddress = theroc.hubaddress();
        PixelChannel theChannel = trans->getChannelFromHdwAddress(theroc);
        std::pair<std::string, int> portCardAndAOH = portcardmap->PortCardAndAOH(theChannel);
        aoh_ch = portCardAndAOH.second;
    }

    for (unsigned int i = 0; i < dacsettings_.size(); ++i) {
        PixelHdwAddress theROC = *(trans->getHdwAddress(dacsettings_[i].getROCName()));
        dacsettings_[i].getDACs(dacs);

        //pixelFEC->setAllDAC(theROC,dacs,bufferData);
        pixelFEC->progdac(theROC.mfec(),
                          theROC.mfecchannel(),
                          theROC.hubaddress(),
                          theROC.portaddress(),
                          theROC.rocid(),
                          dacAddress,
                          dacValue,
                          bufferData);
    }

    std::string offsetNode = pixelFEC->getDDRSegmentName(aoh_ch, segment);
    std::string maskBitNode = pixelFEC->getDDRMaskName(mfec, mfecchannel, aoh_ch, segment);

    pixelFEC->ddrload(mfec, mfecchannel, hubaddress, offsetNode);
}

void PixelDACSettings::setVcthrDisable(PixelFECConfigInterface *pixelFEC, PixelNameTranslation *trans) const {
    //the point here is to set Vcthr to 0
    //then disable the ROCs

    //note -- no need to look at the detconfig here, because we're going to disable everything no matter what
    bool bufferData = false;

    std::vector<unsigned int> dacs;

    int hubaddress, mfec, mfecchannel;
    for (unsigned int i = 0; i < dacsettings_.size(); i++) { //loop over the ROCs

        dacsettings_[i].getDACs(dacs);
        int controlreg = dacsettings_[i].getControlRegister();

        PixelHdwAddress theROC = *(trans->getHdwAddress(dacsettings_[i].getROCName()));

        //since these values are called for each module, they are same for all ROCs in the loop
        mfec = theROC.mfec();
        mfecchannel = theROC.mfecchannel();
        hubaddress = theROC.hubaddress();
        //std::cout<<"disabling ROC="<<dacsettings_[i].getROCName()<<std::endl;
        pixelFEC->progdac(theROC.mfec(),
                          theROC.mfecchannel(),
                          theROC.hubaddress(),
                          theROC.portaddress(),
                          theROC.rocid(),
                          12, //12 == Vcthr
                          0,  //set Vcthr to 0
                          bufferData);

        //this should disable the roc
        pixelFEC->progdac(theROC.mfec(),
                          theROC.mfecchannel(),
                          theROC.hubaddress(),
                          theROC.portaddress(),
                          theROC.rocid(),
                          0xfd,
                          controlreg | 0x2,
                          bufferData);

        // Now program (again) the temperature register to make sure it is the last one
        // and appears in the LastDAC
        // if(readTemperatures) {
        //   assert(0);
        //   //int temperatureReg = dacs[26];  // value from DB
        //   pixelFEC->progdac(theROC.mfec(),
        // 			theROC.mfecchannel(),
        // 			theROC.hubaddress(),
        // 			theROC.portaddress(),
        // 			theROC.rocid(),
        // 			0x1B,
        // 			temperatureReg,
        // 			bufferData);
        // } else {
        // // VCAL
        //   pixelFEC->progdac(theROC.mfec(),
        // 			theROC.mfecchannel(),
        // 			theROC.hubaddress(),
        // 			theROC.portaddress(),
        // 			theROC.rocid(),
        // 			0x19,
        // 			200,
        // 			bufferData);
        // }
    }

    std::string offsetNode = pixelFEC->getDDRSegmentName(0, "User");                  //var=0, no need portchannel for User segment
    std::string maskBitNode = pixelFEC->getDDRMaskName(mfec, mfecchannel, 0, "User"); //var=0, no need portchannel for User segment

    if (bufferData) { // Send data to the FEC
        pixelFEC->qbufsend(mfec, mfecchannel, hubaddress, offsetNode, maskBitNode);
        //pixelFEC->qbufsend();
    }
}

void PixelDACSettings::setVcthrEnable(PixelFECConfigInterface *pixelFEC, PixelNameTranslation *trans, PixelDetectorConfig *detconfig) const {
    //the point here is to set Vcthr to the nominal values
    //then enable the ROCs

    bool bufferData = false;

    std::vector<unsigned int> dacs;
    int hubaddress, mfec, mfecchannel;

    for (unsigned int i = 0; i < dacsettings_.size(); i++) { //loop over the ROCs

        bool disableRoc = rocIsDisabled(detconfig, dacsettings_[i].getROCName()); //don't enable ROCs that are disabled in the detconfig

        //std::cout<<"ROC="<<dacsettings_[i].getROCName()<<" ; VcThr set to "<<dacs[11]
        //	     << " ; ROC control reg to be set to: " <<  controlreg <<std::endl;

        if (!disableRoc) { // Disable

            dacsettings_[i].getDACs(dacs);
            int controlreg = dacsettings_[i].getControlRegister();

            PixelHdwAddress theROC = *(trans->getHdwAddress(dacsettings_[i].getROCName()));

            //since these values are called for each module, they are same for all ROCs in the loop
            mfec = theROC.mfec();
            mfecchannel = theROC.mfecchannel();
            hubaddress = theROC.hubaddress();
            pixelFEC->progdac(theROC.mfec(),
                              theROC.mfecchannel(),
                              theROC.hubaddress(),
                              theROC.portaddress(),
                              theROC.rocid(),
                              12, //12 == Vcthr
                              dacs[11],
                              bufferData);

            //enable the roc (assuming controlreg was set for the roc to be enabled)

            pixelFEC->progdac(theROC.mfec(),
                              theROC.mfecchannel(),
                              theROC.hubaddress(),
                              theROC.portaddress(),
                              theROC.rocid(),
                              0xfd,
                              controlreg,
                              bufferData);

            // Now program (again) the temperature register to make sure it is the last one
            // and appears in the LastDAC
            // if(readTemperatures) {
            // assert(0);
            // 	//int temperatureReg = dacs[26];  // value from DB
            // 	pixelFEC->progdac(theROC.mfec(),
            // 			  theROC.mfecchannel(),
            // 			  theROC.hubaddress(),
            // 			  theROC.portaddress(),
            // 			  theROC.rocid(),
            // 			  0x1B,
            // 			  temperatureReg,
            // 			  bufferData);
            // } else {
            // 	// VCAL
            // 	pixelFEC->progdac(theROC.mfec(),
            // 			  theROC.mfecchannel(),
            // 			  theROC.hubaddress(),
            // 			  theROC.portaddress(),
            // 			  theROC.rocid(),
            // 			  0x19,
            // 			  200,
            // 			  bufferData);
            // }

        } // end disable

    } // loop over ROCs

    std::string offsetNode = pixelFEC->getDDRSegmentName(0, "User");                  //var=0, no need portchannel for User segment
    std::string maskBitNode = pixelFEC->getDDRMaskName(mfec, mfecchannel, 0, "User"); //var=0, no need portchannel for User segment

    if (bufferData) { // Send data to the FEC
        pixelFEC->qbufsend(mfec, mfecchannel, hubaddress, offsetNode, maskBitNode);
        //pixelFEC->qbufsend();
    }
}

bool PixelDACSettings::rocIsDisabled(const PixelDetectorConfig *detconfig, const PixelROCName rocname) const {

    const std::map<PixelROCName, PixelROCStatus> &roclist = detconfig->getROCsList();
    const std::map<PixelROCName, PixelROCStatus>::const_iterator iroc = roclist.find(rocname);
    assert(iroc != roclist.end()); // the roc name should always be found
    PixelROCStatus thisROCstatus = iroc->second;

    return thisROCstatus.get(PixelROCStatus::noAnalogSignal);
}

std::ostream &operator<<(std::ostream &s, const PixelDACSettings &dacs) {

    s << dacs.getDACSettings(0) << std::endl;

    return s;
}
