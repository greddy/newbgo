// Read the pixelFED setup parameters from an ASCII file
// Will Johns & Danek Kotlinski 04/06.

#include <iostream>

#include "CalibFormats/SiPixelObjects/interface/PixelFEDCard.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"

#include <cassert>
#include <cstdarg>
#include <cstring>
#include <sstream>
#include <map>
#include <stdexcept>

using namespace std;
using namespace pos;

namespace {
  const int NumberOfOOSForAutomask = 63; // usually 30
}

PixelFEDCard::PixelFEDCard()
    : PixelConfigBase(" ", " ", " ") {
          clear();
}

PixelFEDCard::PixelFEDCard(vector<vector<string> > &tableMat)
    : PixelConfigBase(" ", " ", " ") {
    std::string mthn = "[PixelFEDCard::PixelFEDCard()]\t\t    ";
    vector<string> ins = tableMat[0];
    map<string, int> colM;
    vector<string> colNames;
    const bool localDEBUG = true;

    clear();

    colNames.push_back("FED_NUMBER");
    colNames.push_back("FED_CONFIG_FILE");
    colNames.push_back("FED_CONFIG_CLOB");

    for (unsigned int c = 0; c < ins.size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    } //end for
    for (unsigned int n = 0; n < colNames.size(); n++) {
        if (colM.find(colNames[n]) == colM.end()) {
            std::cerr << __LINE__ << "]\t[PixelFEDCard::PixelFEDCard]\tCouldn't find in the database the column with name " << colNames[n] << std::endl;
            assert(0);
        }
    }

    std::stringstream fedCardClob;
    fedCardClob << tableMat[1][colM["FED_CONFIG_CLOB"]];

    std::vector<std::string> fedCardLines;
    std::string line;
    while (getline(fedCardClob, line)) {
        fedCardLines.push_back(line);
    }

    std::map<std::string, std::string> params;

    for (unsigned int i = 0; i < fedCardLines.size(); ++i) {
        std::vector<std::string> vars = split(fedCardLines[i], ':');
        params[trim(vars[0])] = trim(vars[1]);
    }

    if (params["Type"] == "VMEPiggy") {
        std::cout << "Why are you still using VME?" << std::endl;
        assert(0);
    } else if (params["Type"] == "CTA") {
        type = CTA;

        cntrl_utca = hexConvert(params["Control bits"]);
        if (localDEBUG)
            printf("Control bits: 0x%llx\n", (unsigned long long)cntrl_utca);

        cntrl_utca_override = atoi(params["Control bits override"].c_str());
        if (localDEBUG)
            printf("Control bits override: %d\n", cntrl_utca_override);

        TransScopeCh = atoi(params["Transparent+scope channel"].c_str());
        if (localDEBUG)
            printf("Transparent+scope channel: %u\n", TransScopeCh);

        PACKET_NB = atoi(params["PACKET_NB"].c_str());
        if (localDEBUG)
            printf("PACKET_NB: %x\n", PACKET_NB);

        which_FMC = atoi(params["Which FMC (lower = 0)"].c_str());
        if (localDEBUG)
            printf("Which FMC (lower = 0): %d\n", which_FMC);

        swap_Fitel_order = atoi(params["Fitel channel order swapped"].c_str());
        if (localDEBUG)
            printf("Fitel channel order swapped: %d\n", swap_Fitel_order);

        acq_mode = atoi(params["Acquisition mode (1=TBM fifo, 2=Slink fifo, 4=FEROL)"].c_str());
        if (localDEBUG)
            printf("Acquisition mode (1=TBM fifo, 2=Slink fifo, 4=FEROL): %d\n", acq_mode);

        calib_mode = atoi(params["Calibration mode"].c_str());
        if (localDEBUG)
            printf("Calibration mode: %d\n", calib_mode);

        calib_mode_num_events = atoi(params["Calibration mode num events"].c_str());
        if (localDEBUG)
            printf("Calibration mode num events:%d\n", calib_mode_num_events);

        data_type = atoi(params["Data type (0=real data, 1=constants, 2=pattern)"].c_str());
        if (localDEBUG)
            printf("Data type (0=real data, 1=constants, 2=pattern):%d\n", data_type);

        tbm_trailer_mask = hexConvert(params["TBM trailer mask"]);
        if (localDEBUG)
            printf("TBM trailer mask: 0x%x\n", tbm_trailer_mask);

        tbm_trailer_mask_2 = hexConvert(params["TBM trailer mask 2"]);
        if (localDEBUG)
            printf("TBM trailer mask 2: 0x%x\n", tbm_trailer_mask_2);

        private_event_number = atoi(params["Private event number"].c_str());
        if (localDEBUG)
            printf("Private event number:%x\n", private_event_number);

        timeout_checking_enabled = atoi(params["Timeout checking enabled"].c_str());
        if (localDEBUG)
            printf("Timeout checking enabled: %d\n", timeout_checking_enabled);

        timeout_counter_start = atoi(params["Timeout counter start"].c_str());
        if (localDEBUG)
            printf("Timeout counter start: %d\n", timeout_counter_start);

        timeout_number_oos_threshold = atoi(params["Timeout number OOS threshold"].c_str());
        if (localDEBUG)
            printf("Timeout number OOS threshold: %d\n", timeout_number_oos_threshold);

        event_count_checking_enabled = atoi(params["Event count checking enabled"].c_str());
        if (localDEBUG)
            printf("Event count checking enabled: %d\n", event_count_checking_enabled);

        event_count_num_err_oos = atoi(params["Number event count errors before OOS"].c_str());
        if (localDEBUG)
            printf("Number event count errors before OOS: %d\n", event_count_num_err_oos);

        frontend_disable_backend = atoi(params["Frontend disable backend"].c_str());
        if (localDEBUG)
            printf("Frontend disable backend: %d\n", frontend_disable_backend);

        if (params.find("Number of emulated hits per ROC per channel") != params.end()) {
            number_emulated_hits = atoi(params["Number of emulated hits per ROC per channel"].c_str());
            if (localDEBUG)
                printf("Number of emulated hits per ROC per channel: %d\n", number_emulated_hits);
        }

        if (params.find("fifo_config.overflow_value") != params.end()) {
            fifo_config_overflow_value = hexConvert(params["fifo_config.overflow_value"].c_str());
            if (localDEBUG)
                printf("fifo_config.overflow_value: 0x%x\n", fifo_config_overflow_value);
        }

        if (params.find("THRESH_tbm_fifo_prog_full_TTS") != params.end()) {
            THRESH_tbm_fifo_prog_full_TTS = atoi(params["THRESH_tbm_fifo_prog_full_TTS"].c_str());
            if (localDEBUG)
                printf("fifo_config.overflow_value: %d\n", THRESH_tbm_fifo_prog_full_TTS);
        }
        if (params.find("THRESH_tbm_fifo_prog_full_DECOD") != params.end()) {
            THRESH_tbm_fifo_prog_full_DECOD = atoi(params["THRESH_tbm_fifo_prog_full_DECOD"].c_str());
            if (localDEBUG)
                printf("THRESH_tbm_fifo_prog_full_DECOD: %d\n", THRESH_tbm_fifo_prog_full_DECOD);
        }
    }

    FEDBASE_0 = hexConvert(params["FED Base address"]);
    fedNumber = hexConvert(params["FEDID Number"]);

    // Number of ROCs
    int ijx = 0;
    int nrocsmax = type == CTA ? 48 : 36;
    for (int i = 0; i < nrocsmax; i++) {
        stringstream ss;
        ijx = i + 1;
        ss << "Number of ROCs Chnl " << ijx;
        NRocs[i] = atoi(params[ss.str()].c_str());
        if (localDEBUG)
            printf("Number of ROCs per Chnl %d:%d \n", ijx, NRocs[i]);
    }

    Ccntrl_original = Ccntrl;
    modeRegister_original = modeRegister;

    cntrl_utca_original = cntrl_utca;

    Ncntrl_original = Ncntrl;
    NCcntrl_original = NCcntrl;
    SCcntrl_original = SCcntrl;
    Scntrl_original = Scntrl;

    Nbaseln_original = Nbaseln;
    NCbaseln_original = NCbaseln;
    SCbaseln_original = SCbaseln;
    Sbaseln_original = Sbaseln;

    return;
}

int checked_fscanf(FILE *f, const char *fmt, ...) {
    int count = 0;
    const char *tmp = fmt;
    while (*tmp) {
        if (*tmp == '%')
            ++count;
        ++tmp;
    }
    tmp = fmt;
    while ((tmp = strstr(tmp, "%%"))) {
        count -= 2;
        ++tmp;
    }
    //printf("count for [%s]: %i\n", fmt, count);

    va_list args;
    va_start(args, fmt);
    int ret = vfscanf(f, fmt, args);
    va_end(args);

    if (ret != count)
        fprintf(stderr, "PixelFEDCard: problem with scanf with fmt [%s] count %i ret %i\n", fmt, count, ret);

    return ret;
}

// Read the configuration parameters from file
PixelFEDCard::PixelFEDCard(string fileName)
    : PixelConfigBase(" ", " ", " ") {

    std::string mthn = "]\t[PixelFEDCard::PixelFEDCard()]\t\t\t\t    ";
    const bool localDEBUG = true;
    //const bool localDEBUG = false;

    // Added by Dario (March 26th, 2008): insure variables are all cleared before read-in
    clear();

    //  cout << __LINE__ << "]\t" << mthn <<" Get setup parameters from file "<<fileName<<endl;
    FILE *infile = fopen((fileName.c_str()), "r");
    if (infile == NULL)
        throw std::runtime_error("Failed to open FED Card parameter file: " + fileName);

    size_t linelen = 0;
    char *line = 0;
    getline(&line, &linelen, infile);
    if (localDEBUG)
        printf("first line: %s\n", line);

    if (strcmp(line, "Type: VMEPiggy\n") == 0) {
        type = VMEPiggy;

        //Bits (1st 4) used to set the channel you want to read in spy fifo2
        checked_fscanf(infile, "N  Scope channel(0-8):%x\n", &N_ScopeCh);
        checked_fscanf(infile, "NC Scope channel(0-8):%x\n", &NC_ScopeCh);
        checked_fscanf(infile, "SC Scope channel(0-8):%x\n", &SC_ScopeCh);
        checked_fscanf(infile, "S  Scope channel(0-8):%x\n", &S_ScopeCh);
        if (localDEBUG) {
            printf("N  Scope channel(0-8):%x\n", N_ScopeCh);
            printf("NC Scope channel(0-8):%x\n", NC_ScopeCh);
            printf("SC Scope channel(0-8):%x\n", SC_ScopeCh);
            printf("S  Scope channel(0-8):%x\n", S_ScopeCh);
        }

        getline(&line, &linelen, infile); // for the FED Base address line next with sscanf
    } else if (strcmp(line, "Type: CTA\n") == 0) {
        type = CTA;

        checked_fscanf(infile, "Control bits: %llx\n", (unsigned long long *)&cntrl_utca);
        if (localDEBUG)
            printf("Control bits: 0x%llx\n", (unsigned long long)cntrl_utca);

        checked_fscanf(infile, "Control bits override: %d\n", &cntrl_utca_override);
        if (localDEBUG)
            printf("Control bits override: %d\n", cntrl_utca_override);

        checked_fscanf(infile, "Transparent+scope channel: %u\n", &TransScopeCh);
        if (localDEBUG)
            printf("Transparent+scope channel: %u\n", TransScopeCh);

        checked_fscanf(infile, "PACKET_NB: %x\n", &PACKET_NB);
        if (localDEBUG)
            printf("PACKET_NB: %x\n", PACKET_NB);

        checked_fscanf(infile, "Which FMC (lower = 0): %d\n", &which_FMC);
        if (localDEBUG)
            printf("Which FMC (lower = 0): %d\n", which_FMC);

        checked_fscanf(infile, "Fitel channel order swapped: %d\n", &swap_Fitel_order);
        if (localDEBUG)
            printf("Fitel channel order swapped: %d\n", swap_Fitel_order);

        checked_fscanf(infile, "Acquisition mode (1=TBM fifo, 2=Slink fifo, 4=FEROL): %d\n", &acq_mode);
        if (localDEBUG)
            printf("Acquisition mode (1=TBM fifo, 2=Slink fifo, 4=FEROL): %d\n", acq_mode);

        checked_fscanf(infile, "Calibration mode: %d\n", &calib_mode);
        if (localDEBUG)
            printf("Calibration mode: %d\n", calib_mode);

        checked_fscanf(infile, "Calibration mode num events: %d\n", &calib_mode_num_events);
        if (localDEBUG)
            printf("Calibration mode num events:%d\n", calib_mode_num_events);

        checked_fscanf(infile, "Data type (0=real data, 1=constants, 2=pattern): %d\n", &data_type);
        if (localDEBUG)
            printf("Data type (0=real data, 1=constants, 2=pattern):%d\n", data_type);

        checked_fscanf(infile, "TBM trailer mask: %x\n", &tbm_trailer_mask);
        if (localDEBUG)
            printf("TBM trailer mask: %x\n", tbm_trailer_mask);

        checked_fscanf(infile, "TBM trailer mask 2: %x\n", &tbm_trailer_mask_2);
        if (localDEBUG)
            printf("TBM trailer mask 2: %x\n", tbm_trailer_mask_2);

        checked_fscanf(infile, "Private event number: %d\n", &private_event_number);
        if (localDEBUG)
            printf("Private event number:%d\n", private_event_number);

        checked_fscanf(infile, "Timeout checking enabled: %d\n", &timeout_checking_enabled);
        if (localDEBUG)
            printf("Timeout checking enabled: %d\n", timeout_checking_enabled);

        checked_fscanf(infile, "Timeout counter start: %d\n", &timeout_counter_start);
        if (localDEBUG)
            printf("Timeout counter start: %d\n", timeout_counter_start);

        checked_fscanf(infile, "Timeout number OOS threshold: %d\n", &timeout_number_oos_threshold);
        if (localDEBUG)
            printf("Timeout number OOS threshold: %d\n", timeout_number_oos_threshold);

        checked_fscanf(infile, "Event count checking enabled: %d\n", &event_count_checking_enabled);
        if (localDEBUG)
            printf("Event count checking enabled: %d\n", event_count_checking_enabled);

        checked_fscanf(infile, "Number event count errors before OOS: %d\n", &event_count_num_err_oos);
        if (localDEBUG)
            printf("Number event count errors before OOS: %d\n", event_count_num_err_oos);

        checked_fscanf(infile, "Frontend disable backend: %d\n", &frontend_disable_backend);
        if (localDEBUG)
            printf("Frontend disable backend: %d\n", frontend_disable_backend);

        checked_fscanf(infile, "Number of emulated hits per ROC per channel: %d\n", &number_emulated_hits);
        if (localDEBUG)
            printf("Number of emulated hits per ROC per channel: %d\n", number_emulated_hits);

        checked_fscanf(infile, "fifo_config.overflow_value: 0x%x\n", &fifo_config_overflow_value);
        if (localDEBUG)
            printf("fifo_config.overflow_value: 0x%x\n", fifo_config_overflow_value);

        checked_fscanf(infile, "THRESH_tbm_fifo_prog_full_TTS: %d\n", &THRESH_tbm_fifo_prog_full_TTS);
        if (localDEBUG)
            printf("THRESH_tbm_fifo_prog_full_TTS: %d\n", THRESH_tbm_fifo_prog_full_TTS);

        checked_fscanf(infile, "THRESH_tbm_fifo_prog_full_DECOD: %d\n", &THRESH_tbm_fifo_prog_full_DECOD);
        if (localDEBUG)
            printf("THRESH_tbm_fifo_prog_full_DECOD: %d\n", THRESH_tbm_fifo_prog_full_DECOD);

        getline(&line, &linelen, infile); // for the FED Base address line next with sscanf
    } else {
        // VME files don't have to start with Type: line and won't have extra lines for new params -- jump straight to the sscanf.
    }

    //Fed Base Address = unique key for non-VME
    sscanf(line, "FED Base address                         :%lx\n",
           &FEDBASE_0);
    free(line);

    checked_fscanf(infile, "FEDID Number                             :%lx\n",
                   &fedNumber);

    // Number of ROCs
    int ijx = 0;
    int nrocsmax = type == CTA ? 48 : 36;
    for (int i = 0; i < nrocsmax; i++) {
        ijx = i + 1;
        checked_fscanf(infile, "Number of ROCs Chnl %d:%d \n", &ijx, &NRocs[i]);
        if (localDEBUG)
            printf("Number of ROCs per Chnl %d:%d \n", ijx, NRocs[i]);
    }

    fclose(infile);

    Ccntrl_original = Ccntrl;
    modeRegister_original = modeRegister;

    cntrl_utca_original = cntrl_utca;

    Ncntrl_original = Ncntrl;
    NCcntrl_original = NCcntrl;
    SCcntrl_original = SCcntrl;
    Scntrl_original = Scntrl;

    Nbaseln_original = Nbaseln;
    NCbaseln_original = NCbaseln;
    SCbaseln_original = SCbaseln;
    Sbaseln_original = Sbaseln;

    return;
}

//==================================================================================
// Added by Dario (March 26th 2008)
void PixelFEDCard::clear(void) {
    type = 0;
    cntrl_utca = cntrl_utca_original = 0;
    TransScopeCh = 0;
    PACKET_NB = 0;
    which_FMC = 0;
    swap_Fitel_order = 0;
    timeout_checking_enabled = 0;
    timeout_counter_start = 0;
    timeout_number_oos_threshold = 0;
    frontend_disable_backend = 0;
    number_emulated_hits = 0;
    fifo_config_overflow_value = 0x82208; // 0x82208=520 hits, 307500=300 hits A/B chans; 262400= 256 hits (A/B) 922500 = 900 channel A/B
    //THRESH_tbm_fifo_prog_full_TTS = 5000; // ~60% full for a 8192 big fifo
    //THRESH_tbm_fifo_prog_full_DECOD = 7500; // ~90% full for a 8192 big fifo
    THRESH_tbm_fifo_prog_full_TTS = 3800;   // previous default value
    THRESH_tbm_fifo_prog_full_DECOD = 3800; // previous default value
    acq_mode = 0;
    calib_mode = 0;
    calib_mode_num_events = 0;
    data_type = 0;
    tbm_trailer_mask = 0;
    tbm_trailer_mask_2 = 0;
    private_event_number = 0;
    event_count_checking_enabled = 0;
    event_count_num_err_oos = 0;
    FEDBASE_0 = 0;
    fedNumber = 999;
    for (int i = 0; i < 48; i++)
        NRocs[i] = 0;
    for (int i = 0; i < 36; i++) {
        offs_dac[i] = 0;
        BlackHi[i] = 0;
        BlackLo[i] = 0;
        Ublack[i] = 0;
        DelayCh[i] = 0;
        TBM_L0[i] = 0;
        TBM_L1[i] = 0;
        TBM_L2[i] = 0;
        TBM_L3[i] = 0;
        TBM_L4[i] = 0;
        TRL_L0[i] = 0;
        TRL_L1[i] = 0;
        TRL_L2[i] = 0;
        TRL_L3[i] = 0;
        TRL_L4[i] = 0;
    }
    for (int i = 0; i < 3; i++) {
        opt_cap[i] = 0;
        opt_inadj[i] = 0;
        opt_ouadj[i] = 0;
    }
    clkphs1_9 = 0;
    clkphs10_18 = 0;
    clkphs19_27 = 0;
    clkphs28_36 = 0;

    for (int i = 0; i < 36; i++) {
        for (int j = 0; j < 26; j++) {
            ROC_L0[i][j] = 0;
            ROC_L1[i][j] = 0;
            ROC_L2[i][j] = 0;
            ROC_L3[i][j] = 0;
            ROC_L4[i][j] = 0;
        }
    }
    Ncntrl = 0;
    NCcntrl = 0;
    SCcntrl = 0;
    Scntrl = 0;
    CoarseDel = 0;
    ClkDes2 = 0;
    FineDes2Del = 0;
    FineDes1Del = 0;
    Ccntrl = 0;
    modeRegister = 0;
    Nadcg = 0;
    NCadcg = 0;
    SCadcg = 0;
    Sadcg = 0;
    Nbaseln = 0;
    NCbaseln = 0;
    SCbaseln = 0;
    Sbaseln = 0;
    N_TBMmask = 0;
    NC_TBMmask = 0;
    SC_TBMmask = 0;
    S_TBMmask = 0;
    N_Pword = 0;
    NC_Pword = 0;
    SC_Pword = 0;
    S_Pword = 0;
    N_ScopeCh = 0;
    NC_ScopeCh = 0;
    SC_ScopeCh = 0;
    S_ScopeCh = 0;
    SpecialDac = 0;
    Ooslvl = 0;
    Errlvl = 0;
    Nfifo1Bzlvl = 0;
    NCfifo1Bzlvl = 0;
    SCfifo1Bzlvl = 0;
    Sfifo1Bzlvl = 0;
    fifo3Wrnlvl = 0;

    BusyHoldMin = 0;
    BusyWhenBehind = 0;
    FeatureRegister = 0;
    FIFO2Limit = 0;
    LastDacOff = 0;
    SimHitsPerRoc = 0;
    TimeoutOROOSLimit = 0;
    TriggerHoldoff = 0;

    SPARE1 = 0;
    SPARE2 = 0;
    SPARE3 = 0;
    SPARE4 = 0;
    SPARE5 = 0;
    SPARE6 = 0;
    SPARE7 = 0;
    SPARE8 = 0;
    SPARE9 = 0;
    SPARE10 = 0;
}
//==================================================================================

void PixelFEDCard::writeASCII(std::string dir) const {

    std::string mthn = "[PixelFEDCard::writeASCII()]\t\t\t\t    ";

    ostringstream s1;
    s1 << fedNumber;
    std::string fedNum = s1.str();

    if (dir != "")
        dir += "/";

    std::string filename = dir + "params_fed_" + fedNum + ".dat";

    FILE *outfile = fopen((filename.c_str()), "w");
    if (outfile == NULL) {
        cout << __LINE__ << "]\t" << mthn << "Could not open file: " << filename << " for writing" << endl;
        return;
    }

    if (type == VMEPiggy) {
        fprintf(outfile, "Type: VMEPiggy\n");

        //Bits (1st 4) used to set the channel you want to read in spy fifo2
        fprintf(outfile, "N  Scope channel(0-8):%x\n", N_ScopeCh);
        fprintf(outfile, "NC Scope channel(0-8):%x\n", NC_ScopeCh);
        fprintf(outfile, "SC Scope channel(0-8):%x\n", SC_ScopeCh);
        fprintf(outfile, "S  Scope channel(0-8):%x\n", S_ScopeCh);
    } else if (type == CTA) {
        fprintf(outfile, "Type: CTA\n");
        fprintf(outfile, "Control bits: 0x%llx\n", (unsigned long long)cntrl_utca);
        fprintf(outfile, "Control bits override: %d\n", cntrl_utca_override);
        fprintf(outfile, "Transparent+scope channel: %u\n", TransScopeCh);
        fprintf(outfile, "PACKET_NB: %x\n", PACKET_NB);
        fprintf(outfile, "Which FMC (lower = 0): %d\n", which_FMC);
        fprintf(outfile, "Fitel channel order swapped: %d\n", swap_Fitel_order);
        fprintf(outfile, "Acquisition mode (1=TBM fifo, 2=Slink fifo, 4=FEROL): %d\n", acq_mode);
        fprintf(outfile, "Calibration mode: %d\n", calib_mode);
        fprintf(outfile, "Calibration mode num events: %d\n", calib_mode_num_events);
        fprintf(outfile, "Data type (0=real data, 1=constants, 2=pattern): %d\n", data_type);
        fprintf(outfile, "TBM trailer mask: 0x%x\n", tbm_trailer_mask);
        fprintf(outfile, "TBM trailer mask 2: 0x%x\n", tbm_trailer_mask_2);
        fprintf(outfile, "Private event number: %d\n", private_event_number);
        fprintf(outfile, "Timeout checking enabled: %d\n", timeout_checking_enabled);
        fprintf(outfile, "Timeout counter start: %d\n", timeout_counter_start);
        fprintf(outfile, "Timeout number OOS threshold: %d\n", timeout_number_oos_threshold);
        fprintf(outfile, "Event count checking enabled: %d\n", event_count_checking_enabled);
        fprintf(outfile, "Number event count errors before OOS: %d\n", event_count_num_err_oos);
        fprintf(outfile, "Frontend disable backend: %d\n", frontend_disable_backend);
        fprintf(outfile, "Number of emulated hits per ROC per channel: %d\n", number_emulated_hits);
        // fprintf(outfile, "fifo_config.overflow_value: 0x%x\n", &fifo_config_overflow_value);
        // fprintf(outfile, "THRESH_tbm_fifo_prog_full_TTS: %d\n", &THRESH_tbm_fifo_prog_full_TTS);
        // fprintf(outfile, "THRESH_tbm_fifo_prog_full_DECOD: %d\n", &THRESH_tbm_fifo_prog_full_DECOD);
        fprintf(outfile, "fifo_config.overflow_value: 0x%p\n", &fifo_config_overflow_value);
        fprintf(outfile, "THRESH_tbm_fifo_prog_full_TTS: %p\n", &THRESH_tbm_fifo_prog_full_TTS);
        fprintf(outfile, "THRESH_tbm_fifo_prog_full_DECOD: %p\n", &THRESH_tbm_fifo_prog_full_DECOD);
    } else
        fprintf(outfile, "Type: VME\n");

    //Fed Base Address
    fprintf(outfile, "FED Base address                         :0x%lx\n",
            FEDBASE_0);
    fprintf(outfile, "FEDID Number                             :0x%lx\n",
            fedNumber);

    // Number of ROCs
    int ijx = 0;
    int nrocsmax = type == CTA ? 48 : 36;
    for (int i = 0; i < nrocsmax; i++) {
        ijx = i + 1;
        fprintf(outfile, "Number of ROCs Chnl %d:%d\n", ijx, NRocs[i]);
    }

    //Settable optical input parameters
    fprintf(outfile, "Optical reciever 1  Capacitor Adjust(0-3):%d\n", opt_cap[0]);
    fprintf(outfile, "Optical reciever 2  Capacitor Adjust(0-3):%d\n", opt_cap[1]);
    fprintf(outfile, "Optical reciever 3  Capacitor Adjust(0-3):%d\n", opt_cap[2]);
    fprintf(outfile, "Optical reciever 1  Input Offset (0-15)  :%d\n", opt_inadj[0]);
    fprintf(outfile, "Optical reciever 2  Input Offset (0-15)  :%d\n", opt_inadj[1]);
    fprintf(outfile, "Optical reciever 3  Input Offset (0-15)  :%d\n", opt_inadj[2]);
    fprintf(outfile, "Optical reciever 1 Output Offset (0-3)   :%d\n", opt_ouadj[0]);
    fprintf(outfile, "Optical reciever 2 Output Offset (0-3)   :%d\n", opt_ouadj[1]);
    fprintf(outfile, "Optical reciever 3 Output Offset (0-3)   :%d\n", opt_ouadj[2]);

    //input offset dac
    for (int i = 0; i < 36; i++) {
        fprintf(outfile, "Offset DAC channel %d:%d\n", i + 1, offs_dac[i]);
    }

    //clock phases
    fprintf(outfile, "Clock Phase Bits ch   1-9:0x%x\n", clkphs1_9);
    fprintf(outfile, "Clock Phase Bits ch 10-18:0x%x\n", clkphs10_18);
    fprintf(outfile, "Clock Phase Bits ch 19-27:0x%x\n", clkphs19_27);
    fprintf(outfile, "Clock Phase Bits ch 28-36:0x%x\n", clkphs28_36);

    //Blacks
    for (int i = 0; i < 36; i++) {
        fprintf(outfile, "Black HiThold ch %d:%d \n", i + 1, BlackHi[i]);
        fprintf(outfile, "Black LoThold ch %d:%d \n", i + 1, BlackLo[i]);
        fprintf(outfile, "ULblack Thold ch %d:%d \n", i + 1, Ublack[i]);
    }

    //Channel delays
    for (int i = 0; i < 36; i++) {
        fprintf(outfile, "Delay channel %d(0-15):%d\n", i + 1, DelayCh[i]);
    }

    //Signal levels
    for (int i = 0; i < 36; i++) {
        fprintf(outfile, "TBM level 0 Channel  %d:%d\n", i + 1, TBM_L0[i]);
        fprintf(outfile, "TBM level 1 Channel  %d:%d\n", i + 1, TBM_L1[i]);
        fprintf(outfile, "TBM level 2 Channel  %d:%d\n", i + 1, TBM_L2[i]);
        fprintf(outfile, "TBM level 3 Channel  %d:%d\n", i + 1, TBM_L3[i]);
        fprintf(outfile, "TBM level 4 Channel  %d:%d\n", i + 1, TBM_L4[i]);

        for (int j = 0; j < NRocs[i]; j++) {
            fprintf(outfile, "ROC%d level 0 Channel  %d :%d\n",
                    j, i + 1, ROC_L0[i][j]);
            fprintf(outfile, "ROC%d level 1 Channel  %d :%d\n",
                    j, i + 1, ROC_L1[i][j]);
            fprintf(outfile, "ROC%d level 2 Channel  %d :%d\n",
                    j, i + 1, ROC_L2[i][j]);
            fprintf(outfile, "ROC%d level 3 Channel  %d :%d\n",
                    j, i + 1, ROC_L3[i][j]);
            fprintf(outfile, "ROC%d level 4 Channel  %d :%d\n",
                    j, i + 1, ROC_L4[i][j]);
        }

        fprintf(outfile, "TRLR level 0 Channel %d:%d\n", i + 1, TRL_L0[i]);
        fprintf(outfile, "TRLR level 1 Channel %d:%d\n", i + 1, TRL_L1[i]);
        fprintf(outfile, "TRLR level 2 Channel %d:%d\n", i + 1, TRL_L2[i]);
        fprintf(outfile, "TRLR level 3 Channel %d:%d\n", i + 1, TRL_L3[i]);
        fprintf(outfile, "TRLR level 4 Channel %d:%d\n", i + 1, TRL_L4[i]);
    }

    //These bits turn off(1) and on(0) channels
    fprintf(outfile, "Channel Enbable bits chnls 1-9  (on = 0):0x%x\n",
            Ncntrl);
    fprintf(outfile, "Channel Enbable bits chnls 10-18(on = 0):0x%x\n",
            NCcntrl);
    fprintf(outfile, "Channel Enbable bits chnls 19-27(on = 0):0x%x\n",
            SCcntrl);
    fprintf(outfile, "Channel Enbable bits chnls 28-36(on = 0):0x%x\n",
            Scntrl);

    //These are delays to the TTCrx
    fprintf(outfile, "TTCrx Coarse Delay Register 2:%d\n", CoarseDel);
    fprintf(outfile, "TTCrc      ClkDes2 Register 3:0x%x\n", ClkDes2);
    fprintf(outfile, "TTCrc Fine Dlay ClkDes2 Reg 1:%d\n", FineDes2Del);

    // Control register
    fprintf(outfile, "Center Chip Control Reg:0x%x\n", Ccntrl);
    fprintf(outfile, "Initial Slink DAQ mode:%d\n", modeRegister);

    //These bits set ADC Gain/Range 1Vpp(0) and 2Vpp(1) for channels
    fprintf(outfile, "Channel ADC Gain bits chnls  1-12(1Vpp = 0):0x%x\n",
            Nadcg);
    fprintf(outfile, "Channel ADC Gain bits chnls 13-20(1Vpp = 0):0x%x\n",
            NCadcg);
    fprintf(outfile, "Channel ADC Gain bits chnls 21-28(1Vpp = 0):0x%x\n",
            SCadcg);
    fprintf(outfile, "Channel ADC Gain bits chnls 29-36(1Vpp = 0):0x%x\n",
            Sadcg);

    //These bits set Baseline adjustment value (common by FPGA)//can turn on by channel
    fprintf(outfile, "Channel Baseline Enbable chnls 1-9  (on = (0x1ff<<16)+):0x%x\n",
            Nbaseln);
    fprintf(outfile, "Channel Baseline Enbable chnls 10-18(on = (0x1ff<<16)+):0x%x\n",
            NCbaseln);
    fprintf(outfile, "Channel Baseline Enbable chnls 19-27(on = (0x1ff<<16)+):0x%x\n",
            SCbaseln);
    fprintf(outfile, "Channel Baseline Enbable chnls 28-36(on = (0x1ff<<16)+):0x%x\n",
            Sbaseln);

    //These bits set TBM trailer mask (common by FPGA)
    fprintf(outfile, "TBM trailer mask chnls 1-9  (0xff = all masked):0x%x\n",
            N_TBMmask);
    fprintf(outfile, "TBM trailer mask chnls 10-18(0xff = all masked):0x%x\n",
            NC_TBMmask);
    fprintf(outfile, "TBM trailer mask chnls 19-27(0xff = all masked):0x%x\n",
            SC_TBMmask);
    fprintf(outfile, "TBM trailer mask chnls 28-36(0xff = all masked):0x%x\n",
            S_TBMmask);

    //These bits set the Private fill/gap word value (common by FPGA)
    fprintf(outfile, "Private 8 bit word chnls 1-9  :0x%x\n",
            N_Pword);
    fprintf(outfile, "Private 8 bit word chnls 10-18:0x%x\n",
            NC_Pword);
    fprintf(outfile, "Private 8 bit word chnls 19-27:0x%x\n",
            SC_Pword);
    fprintf(outfile, "Private 8 bit word chnls 28-36:0x%x\n",
            S_Pword);

    //These bit sets the special dac mode for random triggers
    fprintf(outfile, "Special Random testDAC mode (on = 0x1, off=0x0):0x%x\n",
            SpecialDac);

    //These bits set the number of Out of consecutive out of sync events until a TTs OOs
    fprintf(outfile, "Number of Consecutive (max 1023) Out of Syncs till TTs OOS set:%d\n",
            Ooslvl);

    //These bits set the number of Empty events until a TTs Error
    fprintf(outfile, "Number of Consecutive (max 1023) Empty events till TTs ERR set:%d\n",
            Errlvl);

    //These bits set the Almost Full level in fifo-1, Almost full = TTs BUSY in fifo-1 N
    fprintf(outfile, "N Fifo-1 almost full level,sets TTs BUSY (max 1023):%d\n",
            Nfifo1Bzlvl);

    //These bits set the Almost Full level in fifo-1, Almost full = TTs BUSY in fifo-1 NC
    fprintf(outfile, "NC Fifo-1 almost full level,sets TTs BUSY (max 1023):%d\n",
            NCfifo1Bzlvl);

    //These bits set the Almost Full level in fifo-1, Almost full = TTs BUSY in fifo-1 SC
    fprintf(outfile, "SC Fifo-1 almost full level,sets TTs BUSY (max 1023):%d\n",
            SCfifo1Bzlvl);

    //These bits set the Almost Full level in fifo-1, Almost full = TTs BUSY in fifo-1 S
    fprintf(outfile, "S Fifo-1 almost full level,sets TTs BUSY (max 1023):%d\n",
            Sfifo1Bzlvl);

    //These bits set the Almost Full level in fifo-3, Almost full = TTs WARN in fifo-3
    fprintf(outfile, "Fifo-3 almost full level,sets TTs WARN (max 8191):%d\n",
            fifo3Wrnlvl);

    fprintf(outfile, "FED Master delay 0=0,1=32,2=48,3=64:%d\n",
            FedTTCDelay);

    fprintf(outfile, "TTCrx Register 0 fine delay ClkDes1:%d\n",
            FineDes1Del);

    int checkword = 20211;

    fprintf(outfile, "Params FED file check word:%d\n",
            checkword);

    //These bits set the hit limit in fifo-1 for an event
    fprintf(outfile, "N fifo-1 hit limit (max 1023 (hard) 900 (soft):%d\n",
            N_hitlimit); //ch 1-9

    fprintf(outfile, "NC fifo-1 hit limit (max 1023 (hard) 900 (soft):%d\n",
            NC_hitlimit); //ch 10-18

    fprintf(outfile, "SC fifo-1 hit limit (max 1023 (hard) 900 (soft):%d\n",
            SC_hitlimit); //ch 19-27

    fprintf(outfile, "S fifo-1 hit limit (max 1023 (hard) 900 (soft):%d\n",
            S_hitlimit); //ch 28-36

    // Testregs
    fprintf(outfile, "N  testreg:%x\n",
            N_testreg);

    fprintf(outfile, "NC testreg:%x\n",
            NC_testreg);

    fprintf(outfile, "SC testreg:%x\n",
            SC_testreg);

    fprintf(outfile, "S  testreg:%x\n",
            S_testreg);

    fprintf(outfile, "Set BUSYWHENBEHIND by this many triggers with timeouts:%d\n",
            BusyWhenBehind);

    fprintf(outfile, "D[0]=1 enable fed-stuck reset D[1]=1 disable ev# protect(dont):0x%x\n",
            FeatureRegister);

    fprintf(outfile, "Limit for fifo-2 almost full (point for the TTS flag):0x%x\n",
            FIFO2Limit);

    fprintf(outfile, "Limit for consecutive timeout OR OOSs:%d\n",
            TimeoutOROOSLimit);

    fprintf(outfile, "Turn off filling of lastdac fifos(exc 1st ROC):%d\n",
            LastDacOff);

    fprintf(outfile, "Number of simulated hits per ROC for internal generator:%d\n",
            SimHitsPerRoc);

    fprintf(outfile, "Miniumum hold time for busy (changing definition):%d\n",
            BusyHoldMin);

    fprintf(outfile, "Trigger Holdoff in units of 25us(0=none):%d\n",
            TriggerHoldoff);

    fprintf(outfile, "Spare fedcard input 1:%d\n", SPARE1);
    fprintf(outfile, "Spare fedcard input 2:%d\n", SPARE2);
    fprintf(outfile, "Spare fedcard input 3:%d\n", SPARE3);
    fprintf(outfile, "Spare fedcard input 4:%d\n", SPARE4);
    fprintf(outfile, "Spare fedcard input 5:%d\n", SPARE5);
    fprintf(outfile, "Spare fedcard input 6:%d\n", SPARE6);
    fprintf(outfile, "Spare fedcard input 7:%d\n", SPARE7);
    fprintf(outfile, "Spare fedcard input 8:%d\n", SPARE8);
    fprintf(outfile, "Spare fedcard input 9:%d\n", SPARE9);
    fprintf(outfile, "Spare fedcard input 10:%d\n", SPARE10);

    fclose(outfile);
}

//=============================================================================================
void PixelFEDCard::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[[PixelFEDCard::writeXMLHeader()]\t\t\t    ";
    std::string mthn = s.str();
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_FED_Card_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_FED_CARD</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel FED Card</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel FED Card</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel FED Card Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}
//=============================================================================================
void PixelFEDCard::writeXML(std::ofstream *outstream) const {
    std::string mthn = "[PixelFEDCard::writeXML()]\t\t\t    ";

    *outstream << "        <DATA>" << std::endl;
    *outstream << "            <FED_NUMBER>" << fedNumber << "</FED_NUMBER>" << std::endl;
    *outstream << "            <FED_CONFIG_FILE>params_fed_" << fedNumber << ".dat</FED_CONFIG_FILE>" << std::endl;
    *outstream << "        </DATA>" << std::endl;
}

//=============================================================================================
void PixelFEDCard::writeXMLTrailer(std::ofstream *outstream) const {
    std::string mthn = "[PixelFEDCard::writeXMLTrailer()]\t\t\t    ";

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT>" << std::endl;

    outstream->close();
}

//=============================================================================================
uint64_t PixelFEDCard::enabledChannels() {
    if (type == CTA)
        return ~cntrl_utca;

    uint64_t channels = 0;
    // return a 64-bit word with low 36 bits set if a channel is enabled
    // if bits are set in the control registers, transfer of data from
    // fifo1 to fifo 2 is not done, meaning the channel is disabled.
    channels = (Ncntrl & 0x1ffLL); // Add LL for SLC4, d.k. 12/07
    channels += (NCcntrl & 0x1ffLL) << 9;
    channels += (SCcntrl & 0x1ffLL) << 18;
    channels += (Scntrl & 0x1ffLL) << 27;
    return ~channels; //bitwise complement to get enabled channels
}

bool PixelFEDCard::useChannel(unsigned int iChannel) {
    assert(iChannel > 0 && iChannel < (type == CTA ? 49 : 37));
    return (enabledChannels() >> (iChannel - 1)) & 0x1LL;
}

void PixelFEDCard::setChannel(unsigned int iChannel, bool mode) {
    assert(iChannel > 0 && iChannel < (type == CTA ? 49 : 37));
    long long mask = enabledChannels();
    long long bit = 0x1LL << (iChannel - 1);
    if (mode) {
        mask = mask | bit;
    } else {
        bit = ~bit;
        mask = mask & bit;
    }
    mask = ~mask;

    if (type == CTA) {
        cntrl_utca = mask;
    } else {
        Ncntrl = (Ncntrl & 0xffff0000LL) | (mask & 0x1ffLL);
        mask = mask >> 9;
        NCcntrl = (NCcntrl & 0xffff0000LL) | (mask & 0x1ffLL);
        mask = mask >> 9;
        SCcntrl = (SCcntrl & 0xffff0000LL) | (mask & 0x1ffLL);
        mask = mask >> 9;
        Scntrl = (Scntrl & 0xffff0000LL) | (mask & 0x1ffLL);
    }
}

void PixelFEDCard::restoreBaselinAndChannelMasks() {
    cntrl_utca = cntrl_utca_original;

    Ncntrl = Ncntrl_original;
    NCcntrl = NCcntrl_original;
    SCcntrl = SCcntrl_original;
    Scntrl = Scntrl_original;

    Nbaseln = Nbaseln_original;
    NCbaseln = NCbaseln_original;
    SCbaseln = SCbaseln_original;
    Sbaseln = Sbaseln_original;
}

void PixelFEDCard::restoreControlAndModeRegister() {

    Ccntrl = Ccntrl_original;
    modeRegister = modeRegister_original;
}

std::vector<std::pair<std::string, uint32_t> > PixelFEDCard::paramSetup() {
    std::vector<std::pair<std::string, uint32_t> > cVecReg;

    const uint32_t tbm_mask_1((cntrl_utca_override ? cntrl_utca_original : cntrl_utca) & 0xFFFFFFFFULL);
    const uint32_t tbm_mask_2(((cntrl_utca_override ? cntrl_utca_original : cntrl_utca) >> 32) & 0xFFFF);

    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.DDR0_end_readout", 0));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.DDR1_end_readout", 0));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.acq_ctrl.acq_mode", acq_mode)); // 1: TBM fifo, 2: Slink FIFO, 4: FEROL
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.acq_ctrl.calib_mode", calib_mode));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.acq_ctrl.calib_mode_NEvents", calib_mode_num_events - 1)); // = nevents - 1
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.data_type", data_type));                                   // 0: real data, 1: constants after TBM fifo, 2: pattern before TBM fifo
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.fitel_i2c_cmd_reset", 1));                                 // fitel I2C bus reset & fifo TX & RX reset
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.PACKET_NB", PACKET_NB));                                   // the FW needs to be aware of the true 32 bit workd Block size for some reason! This is the Packet_nb_true in the python script?!
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.TBM_MASK_1", tbm_mask_1));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.TBM_MASK_2", tbm_mask_2));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tbm_trailer_status_mask", tbm_trailer_mask));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tbm_trailer_status_mask2", tbm_trailer_mask_2));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tbm_trailer_status_mask3", 0)); //KME BAD need in new fed card 1=error trailers (lots!) 0=none
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.slink_ctrl.privateEvtNb", private_event_number));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.slink_ctrl.slinkFormatH_source_id", fedNumber));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.slink_ctrl.slinkFormatH_FOV", 0xe));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.slink_ctrl.slinkFormatH_Evt_ty", 0));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.slink_ctrl.slinkFormatH_Evt_stat", 0xf));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tts.timeout_check_valid", timeout_checking_enabled));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tts.timeout_value", timeout_counter_start));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tts.timeout_nb_oos_thresh", timeout_number_oos_threshold));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tts.event_cnt_check_valid", event_count_checking_enabled));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tts.evt_err_nb_oos_thresh", event_count_num_err_oos));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tts.bc0_ec0_polar", 0));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.tts.force_RDY", 0));
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.phase_finding.disable", 0));
#ifdef JMT_FROMFIFO1
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.disable_BE", 1));
#else
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.disable_BE", frontend_disable_backend));
#endif
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.fifo_config.overflow_value", fifo_config_overflow_value)); // KMEBAD 0x82208=520 hits, 307500=300 hits A/B chans; 262400= 256 hits (A/B) 922500 = 900 channel A/B
    //cVecReg.push_back(std::make_pair("fe_ctrl_regs.fifo_config.overflow_value",0xfffff)); // KMEBAD 0x82208=520 hits, 307500=300 hits A/B chans; 262400= 256 hits (A/B) 922500 = 900 channel A/B
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.fifo_config.TBM_old_new", 0x0)); // 0x0 = PSI46dig, 0x1 = PROC600 // JMTBAD this needs to be a per channel thing if the bpix boards distribute the layer-1 occupancy
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.fifo_config.channel_of_interest", TransScopeCh));
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.decode_reg_reset", 1)); // init FE spy fifos etc JMTBAD take out if this doesn't work any more
    cVecReg.push_back(std::make_pair("REGMGR_DISPATCH", 1000));            // JMTBAD there were two separate WriteStackReg calls, take this out if it doesn't matter, the 1000 means sleep 1 ms
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.fitel_i2c_cmd_reset", 0));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.fitel_rx_i2c_req", 0));
    //    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.BX_cnt_offset", 1));  // KMEBAD hardwired (for miniDAQ); move to Fed card (1 for HC test stand)
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.BX_cnt_offset", 3));           // KMEBAD hardwired (2 for miniDAQ); 3 for global running (1 for HC test stand)
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.trailer_timeout_valid", 1));   // KMEBAD hardwired (to squash FED stuck in Busy); move to FED card
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.trailer_timeout_value", 255)); //KMEBAD hardwired, move to FED card 10 was hardwired in fw; 30 works (2 BmO, 4 BmI); try 40 after timing

    // added on 21/2/17 following the version sent by Hannsjoerg
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.TBM_H_ID_check_valid", 1));            // AKBAD hardwired, should be moved to FED card
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.cnt_evtResyncAheadBy1_reset_sel", 1)); //KMEBAD hardwired, move to FED card (1=only reset at configure time,0=also at re-sync)
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.cnt_TBM_H_ID_err_reset_sel", 1));      //KMEBAD hardwired, move to FED card (1=only reset at configure time,0=also at re-sync)
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.cnt_trailer_err_reset_sel", 1));       //KMEBAD hardwired, move to FED card (1=only reset at configure time,0=also at re-sync)
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.auto_mask_valid", 1));                 // auto mask of channel
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.OOS_nb_thresh", NumberOfOOSForAutomask)); // after 30 cumulative OOS (either from timeout or ENE). Just a custom number, do not look for a particular meaning. We have reduced it from 63 at begginning of Run3
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.resync_mode", 1));                     // private resync-safe state machine FW >= 14.14
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.delay_switch_timeoutVal", 75));        // delay before applying timeout change in resync
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.timeoutValueResync", 1));              // timeout value during resync
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.Enable_mask_ch_word", 1));             // 1=write masked channel words in FEROL

    // JMTBAD use block write for this one
    for (int i = 0; i < 48; ++i) {
        char buf[256];
        snprintf(buf, 256, "pixfed_ctrl_regs.tts.evt_timeout_value.tbm_ch_%02i", i);
        cVecReg.push_back(std::make_pair(std::string(buf), uint32_t(timeout_counter_start)));
    }
    return cVecReg;
}

std::vector<uint32_t> PixelFEDCard::setupModules() {
    std::vector<uint32_t> idel_individual_ctrl_default;
    idel_individual_ctrl_default.assign(24, 0);
    for (int i = 0; i < 24; ++i) {
        if ((NRocs[2 * i] == 2) && (NRocs[(2 * i) + 1] == 2)) {
            cout << " setup fiber " << (i + 1) << " as a layer 1 module " << endl;
            idel_individual_ctrl_default[i] = 0x80010000;
        } else if ((NRocs[2 * i] >= 4) && (NRocs[(2 * i) + 1] >= 4)) {
            cout << " setup fiber " << (i + 1) << " as a layer 2/3/4  module " << endl;
            idel_individual_ctrl_default[i] = 0x80000000;
        } else {
            cout << " setup fiber " << (i + 1) << " I do not know what to seup " << endl;
            idel_individual_ctrl_default[i] = 0x80000000;
        }
    }
    return idel_individual_ctrl_default;
}
