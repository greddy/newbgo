//
// This class stores the information about a FED.
// This include the number, crate, and base address
//
//

#include "CalibFormats/SiPixelObjects/interface/PixelFEDConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"
#include "CalibFormats/SiPixelObjects/interface/Utility.h"
#include <fstream>
#include <iostream>
#include <map>
#include <assert.h>
#include <stdexcept>

using namespace pos;
using namespace std;

PixelFEDConfig::PixelFEDConfig(std::vector<std::vector<std::string> > &tableMat)
    : PixelConfigBase(" ", " ", " ") {
    std::string mthn = "[PixelFEDConfig::PixelFEDConfig()]\t\t\t    ";

    std::vector<std::string> ins = tableMat[0];
    std::map<std::string, int> colM;
    std::vector<std::string> colNames;

    colNames.push_back("FED_NUMBER");
    colNames.push_back("CRATE_NUMBER");
    colNames.push_back("VME_BASE_ADDRS");
    colNames.push_back("TYPE");
    colNames.push_back("URI");

    for (unsigned int c = 0; c < tableMat[0].size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    } //end for

    for (unsigned int n = 0; n < colNames.size(); n++) {
        if (colM.find(colNames[n]) == colM.end()) {
            std::cerr << __LINE__ << "]\t" << mthn << "Couldn't find in the database the column with name " << colNames[n] << std::endl;
            assert(0);
        }
    }

    fedconfig_.clear();
    bool flag = false;
    for (unsigned int r = 1; r < tableMat.size(); r++) { //Goes to every row of the Matrix
        unsigned int fednum;
        fednum = atoi(tableMat[r][colM["FED_NUMBER"]].c_str());
        if (fedconfig_.empty()) {
            unsigned int crate = atoi(tableMat[r][colM["CRATE_NUMBER"]].c_str());
            unsigned int vme_base_address;
            std::string type;
            std::string uri;

            string hexVMEAddr = tableMat[r][colM["VME_BASE_ADDRS"]];
            sscanf(hexVMEAddr.c_str(), "%x", &vme_base_address);
            type = tableMat[r][colM["TYPE"]].c_str();
            uri = tableMat[r][colM["URI"]].c_str();

            PixelFEDParameters tmp;

            tmp.setFEDParameters(fednum, crate, vme_base_address);
            tmp.setType(type);
            tmp.setURI(uri);

            fedconfig_.push_back(tmp);
        } else {
            for (unsigned int y = 0; y < fedconfig_.size(); y++) {
                if (fedconfig_[y].getFEDNumber() == fednum) { // This is to check if there are Pixel Feds already in the vector because
                    flag = true;                              // in the view of the database that I'm reading there are many repeated entries (AS FAR AS THESE PARAMS ARE CONCERNED).
                    break;                                    // This ensure that there are no objects in the fedconfig vector with repeated values.
                } else
                    flag = false;
            }
            if (flag == false) {
                unsigned int crate = atoi(tableMat[r][colM["CRATE_NUMBER"]].c_str());
                unsigned int vme_base_address;
                std::string type;
                std::string uri;

                string hexVMEAddr = tableMat[r][colM["VME_BASE_ADDRS"]];
                sscanf(hexVMEAddr.c_str(), "%x", &vme_base_address);
                type = tableMat[r][colM["TYPE"]].c_str();
                uri = tableMat[r][colM["URI"]].c_str();

                PixelFEDParameters tmp;

                tmp.setFEDParameters(fednum, crate, vme_base_address);
                tmp.setType(type);
                tmp.setURI(uri);

                fedconfig_.push_back(tmp);
            }
        } //end else
    }     //end for r
} //end Constructor

//*****************************************************************************************************

PixelFEDConfig::PixelFEDConfig(std::string filename)
    : PixelConfigBase(" ", " ", " ") {

    std::string mthn = "[PixelFEDConfig::PixelFEDConfig()]\t\t\t    ";
    std::ifstream in(filename.c_str());

    if (!in.good()) {
        std::cout << __LINE__ << "]\t" << mthn << "Could not open: " << filename.c_str() << std::endl;
        throw std::runtime_error("Failed to open file " + filename);
    } else {
        std::cout << __LINE__ << "]\t" << mthn << "Opened: " << filename.c_str() << std::endl;
    }

    std::string line;

    while (getline(in, line)) {
        if (line[0] == '#' || line.find_first_not_of(" \t") == std::string::npos)
            continue;
        std::vector<std::string> tokens = tokenize(line, true);
        if (tokens.size() == 0)
            continue;                                     // a comment line
        assert(tokens.size() >= 3 && tokens.size() <= 5); // 3 to be backward compatible with VME-only POS, 5 with VME-or-uTCA POS

        const unsigned fednumber = strtoul(tokens[0].c_str(), 0, 10);
        const unsigned crate = strtoul(tokens[1].c_str(), 0, 10);
        const unsigned vme_base_address = strtoul(tokens[2].c_str(), 0, 16);

        PixelFEDParameters tmp;
        tmp.setFEDParameters(fednumber, crate, vme_base_address);

        if (tokens.size() == 3) {
            tmp.setType("VME");
        } else if (tokens.size() == 4) {
            assert(tokens[3] == "VME" || tokens[3] == "VMEPiggy");
            tmp.setType(tokens[3]);
        } else {
            tmp.setType(tokens[3]);
            tmp.setURI(tokens[4]);
        }

        assert(tmp.getType() == "VME" || tmp.getType() == "VMEPiggy" || tmp.getType() == "CTA");

        fedconfig_.push_back(tmp);
    }

    in.close();
}

//std::ostream& operator<<(std::ostream& s, const PixelFEDConfig& table){

//for (unsigned int i=0;i<table.translationtable_.size();i++){
//	s << table.translationtable_[i]<<std::endl;
//   }
// return s;

//}

PixelFEDConfig::~PixelFEDConfig() {}

void PixelFEDConfig::writeASCII(std::string dir) const {

    std::string mthn = "[PixelFEDConfig::writeASCII()]\t\t\t\t    ";
    if (dir != "")
        dir += "/";
    string filename = dir + "fedconfig.dat";

    ofstream out(filename.c_str());
    if (!out.good()) {
        cout << __LINE__ << "]\t" << mthn << "Could not open file: " << filename << endl;
        assert(0);
    }

    out << "#FED number     crate     vme base address     type    URI" << endl;
    for (unsigned int i = 0; i < fedconfig_.size(); i++) {
        out << fedconfig_[i].getFEDNumber() << "               "
            << fedconfig_[i].getCrate() << "         "
            << "0x" << hex << fedconfig_[i].getVMEBaseAddress() << dec << "    "
            << fedconfig_[i].getType() << "     "
            << fedconfig_[i].getURI() << endl;
    }
    out.close();
}

unsigned int PixelFEDConfig::getNFEDBoards() const {
    return fedconfig_.size();
}

unsigned int PixelFEDConfig::getFEDNumber(unsigned int i) const {

    assert(i < fedconfig_.size());
    return fedconfig_[i].getFEDNumber();
}

unsigned int PixelFEDConfig::getCrate(unsigned int i) const {

    assert(i < fedconfig_.size());
    return fedconfig_[i].getCrate();
}

unsigned int PixelFEDConfig::getVMEBaseAddress(unsigned int i) const {

    assert(i < fedconfig_.size());
    return fedconfig_[i].getVMEBaseAddress();
}

unsigned int PixelFEDConfig::crateFromFEDNumber(unsigned int fednumber) const {

    std::string mthn = "[PixelFEDConfig::crateFromFEDNumber()]\t\t\t    ";
    for (unsigned int i = 0; i < fedconfig_.size(); i++) {
        if (fedconfig_[i].getFEDNumber() == fednumber)
            return fedconfig_[i].getCrate();
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FED number: " << fednumber << std::endl;

    assert(0);

    return 0;
}

std::string PixelFEDConfig::typeFromFEDNumber(unsigned int fednumber) const {

    std::string mthn = "[PixelFEDConfig::typeFromFEDNumber()]\t\t\t    ";
    for (unsigned int i = 0; i < fedconfig_.size(); i++) {
        if (fedconfig_[i].getFEDNumber() == fednumber)
            return fedconfig_[i].getType();
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FED number: " << fednumber << std::endl;

    assert(0);

    return 0;
}

std::string PixelFEDConfig::URIFromFEDNumber(unsigned int fednumber) const {

    std::string mthn = "[PixelFEDConfig::URIFromFEDNumber()]\t\t\t    ";
    for (unsigned int i = 0; i < fedconfig_.size(); i++) {
        if (fedconfig_[i].getFEDNumber() == fednumber)
            return fedconfig_[i].getURI();
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FED number: " << fednumber << std::endl;

    assert(0);

    return 0;
}

unsigned int PixelFEDConfig::slotFromFEDNumber(unsigned int fednumber) const {

    std::string mthn = "[PixelFEDConfig::slotFromFEDNumber()]\t\t\t    ";
    for (unsigned int i = 0; i < fedconfig_.size(); i++) {
        if (fedconfig_[i].getFEDNumber() == fednumber) {
            std::string URI = fedconfig_[i].getURI();
            std::size_t found = URI.find(":50001");
            unsigned int slot;
            istringstream(URI.substr(found - 2, 2)) >> slot;
            return slot;
        }
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FED number: " << fednumber << std::endl;

    assert(0);

    return 0;
}

std::vector<unsigned int> PixelFEDConfig::getFEDsFromCrate(unsigned int crate) const {

    std::string mthn = "[PixelFEDConfig::getFEDsFromCrate()]\t    ";
    std::vector<unsigned int> FEDlist;
    for (unsigned int i = 0; i < fedconfig_.size(); i++) {
        if (fedconfig_[i].getCrate() == crate) {
            FEDlist.push_back(fedconfig_[i].getFEDNumber());
        }
    }

    if (FEDlist.empty()) {
        std::cout << __LINE__ << "]\t" << mthn << "Could not find FED crate: " << crate << std::endl;

        assert(0);

        return FEDlist;
    } else {
        return FEDlist;
    }
}

unsigned int PixelFEDConfig::VMEBaseAddressFromFEDNumber(unsigned int fednumber) const {

    std::string mthn = "[PixelFEDConfig::VMEBaseAddressFromFEDNumber()]\t\t    ";
    for (unsigned int i = 0; i < fedconfig_.size(); i++) {
        if (fedconfig_[i].getFEDNumber() == fednumber)
            return fedconfig_[i].getVMEBaseAddress();
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FED number: " << fednumber << std::endl;

    assert(0);

    return 0;
}

unsigned int PixelFEDConfig::FEDNumberFromCrateAndVMEBaseAddress(unsigned int crate, unsigned int vmebaseaddress) const {

    std::string mthn = "[PixelFEDConfig::FEDNumberFromCrateAndVMEBaseAddress()]\t    ";
    for (unsigned int i = 0; i < fedconfig_.size(); i++) {
        if (fedconfig_[i].getCrate() == crate &&
            fedconfig_[i].getVMEBaseAddress() == vmebaseaddress)
            return fedconfig_[i].getFEDNumber();
    }

    std::cout << __LINE__ << "]\t" << mthn << "Could not find FED crate and address: " << crate << ", " << vmebaseaddress << std::endl;

    assert(0);

    return 0;
}

//=============================================================================================
void PixelFEDConfig::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[[PixelFEDConfig::writeXMLHeader()]\t\t\t    ";
    std::string mthn = s.str();
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_FED_Config_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_FED_CONFIG</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel FED Configuration</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel FED Configuration</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel FED Configuration Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}

//=============================================================================================
void PixelFEDConfig::writeXML(std::ofstream *outstream) const {
    std::string mthn = "[PixelFEDConfig::writeXML()]\t\t\t    ";

    for (unsigned int i = 0; i < fedconfig_.size(); i++) {
        *outstream << "        <DATA>" << std::endl;
        *outstream << "            <FED_NUMBER>" << fedconfig_[i].getFEDNumber() << "</FED_NUMBER>" << std::endl;
        *outstream << "            <CRATE_NUMBER>" << fedconfig_[i].getCrate() << "</CRATE_NUMBER>" << std::endl;
        *outstream << "            <VME_BASE_ADDRS>"
                   << "0x" << hex << fedconfig_[i].getVMEBaseAddress() << dec << "</VME_BASE_ADDRS>" << std::endl;
        *outstream << "            <TYPE>" << fedconfig_[i].getType() << "</TYPE>" << std::endl;
        *outstream << "            <URI>" << fedconfig_[i].getURI() << "</URI>" << std::endl;
        *outstream << "        </DATA>" << std::endl;
    }
}

//=============================================================================================
void PixelFEDConfig::writeXMLTrailer(std::ofstream *outstream) const {
    std::string mthn = "[PixelFEDConfig::writeXMLTrailer()]\t\t\t    ";

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT> " << std::endl;

    outstream->close();
}
