//
// This class provide a base class for the
// pixel ROC DAC data for the pixel FEC configuration
//
// Adopt for the new delay in TBM10d. d.k. 4/21
//
//

#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTBMSettings.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <ios>
#include <assert.h>
#include <stdexcept>

using namespace pos;
using namespace std;
namespace {
  const bool PRINT = false;
}

PixelTBMSettings::PixelTBMSettings(std::vector<std::vector<std::string> > &tableMat)
    : PixelConfigBase("", "", "") {

    std::string mthn = "]\t[PixelTBMSettings::PixelTBMSettings()]\t\t\t    ";
    std::vector<std::string> ins = tableMat[0];
    std::map<std::string, int> colM;
    std::vector<std::string> colNames;

    colNames.push_back("TBM_NAME");
    colNames.push_back("ROC_NAME");
    colNames.push_back("TBMA_BASE0");
    colNames.push_back("TBMB_BASE0");
    colNames.push_back("TBMA_AUTO_RESET");
    colNames.push_back("TBMB_AUTO_RESET");
    colNames.push_back("TBMA_NOTOKEN_PASS");
    colNames.push_back("TBMB_NOTOKEN_PASS");
    colNames.push_back("TBMA_DISABLE_PKAM_COUNTER");
    colNames.push_back("TBMB_DISABLE_PKAM_COUNTER");
    colNames.push_back("TBMA_PKAM_COUNT");
    colNames.push_back("TBMB_PKAM_COUNT");
    colNames.push_back("TBM_PLL_DELAY");
    colNames.push_back("TBM_PLL_B_DELAY");
    colNames.push_back("TBMB_DELAY");
    colNames.push_back("TBMA_DELAY");

    for (unsigned int c = 0; c < ins.size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    } //end for
    for (unsigned int n = 0; n < colNames.size(); n++) {
        if (colM.find(colNames[n]) == colM.end()) {
            std::cerr << __LINE__ << mthn << "Couldn't find in the database the column with name " << colNames[n] << std::endl;
            assert(0);
        }
    }

    for (unsigned int r = 1; r < tableMat.size(); r++) {
        PixelROCName tmp(tableMat[r][colM["ROC_NAME"]]);
        rocid_ = tmp;

        TBMABase0_ = atoi(tableMat[r][colM["TBMA_BASE0"]].c_str());
        TBMBBase0_ = atoi(tableMat[r][colM["TBMB_BASE0"]].c_str());
        TBMAAutoReset_ = atoi(tableMat[r][colM["TBMA_AUTO_RESET"]].c_str());
        TBMBAutoReset_ = atoi(tableMat[r][colM["TBMB_AUTO_RESET"]].c_str());
        TBMANoTokenPass_ = atoi(tableMat[r][colM["TBMA_NOTOKEN_PASS"]].c_str());
        TBMBNoTokenPass_ = atoi(tableMat[r][colM["TBMB_NOTOKEN_PASS"]].c_str());
        TBMADisablePKAMCounter_ = atoi(tableMat[r][colM["TBMA_DISABLE_PKAM_COUNTER"]].c_str());
        TBMBDisablePKAMCounter_ = atoi(tableMat[r][colM["TBMB_DISABLE_PKAM_COUNTER"]].c_str());
        TBMAPKAMCount_ = atoi(tableMat[r][colM["TBMA_PKAM_COUNT"]].c_str());
        TBMBPKAMCount_ = atoi(tableMat[r][colM["TBMB_PKAM_COUNT"]].c_str());
        TBMPLLDelay_ = atoi(tableMat[r][colM["TBM_PLL_DELAY"]].c_str());
        TBMPLLBDelay_ = atoi(tableMat[r][colM["TBM_PLL_B_DELAY"]].c_str());
        TBMADelay_ = atoi(tableMat[r][colM["TBMA_DELAY"]].c_str());
        TBMBDelay_ = atoi(tableMat[r][colM["TBMB_DELAY"]].c_str());
    }
} //end contructor

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

PixelTBMSettings::PixelTBMSettings(std::string filename)
    : PixelConfigBase("", "", "") {

    std::string mthn = "]\t[PixelTBMSettings::PixelTBMSettings()]\t\t\t    ";
    if (filename[filename.size() - 1] == 't') {

        std::ifstream in(filename.c_str());

        if (!in.good()) {
            std::cout << __LINE__ << mthn << "Could not open:" << filename << std::endl;
            throw std::runtime_error("Failed to open file " + filename);
        } else {
            // std::cout << "Opened:"<<filename<<std::endl;
        }

        PixelROCName tmp(in);
        rocid_ = tmp;

        std::map<std::string, unsigned int> parameters;
        while (!in.eof()) {
            std::string tag;
            unsigned int tmpint;
            in >> tag;
            in >> tmpint;
            parameters[tag] = tmpint;
        }

        in.close();

        std::map<std::string, unsigned int>::const_iterator position;

        position = parameters.find("TBMABase0:");
        assert(position != parameters.end());
        TBMABase0_ = position->second;

        position = parameters.find("TBMBBase0:");
        assert(position != parameters.end());
        TBMBBase0_ = position->second;

        position = parameters.find("TBMAAutoReset:");
        assert(position != parameters.end());
        TBMAAutoReset_ = position->second;

        position = parameters.find("TBMBAutoReset:");
        assert(position != parameters.end());
        TBMBAutoReset_ = position->second;

        position = parameters.find("TBMANoTokenPass:");
        assert(position != parameters.end());
        TBMANoTokenPass_ = position->second;

        position = parameters.find("TBMBNoTokenPass:");
        assert(position != parameters.end());
        TBMBNoTokenPass_ = position->second;

        position = parameters.find("TBMADisablePKAMCounter:");
        assert(position != parameters.end());
        TBMADisablePKAMCounter_ = position->second;

        position = parameters.find("TBMBDisablePKAMCounter:");
        assert(position != parameters.end());
        TBMBDisablePKAMCounter_ = position->second;

        position = parameters.find("TBMAPKAMCount:");
        assert(position != parameters.end());
        TBMAPKAMCount_ = position->second;

        position = parameters.find("TBMBPKAMCount:");
        assert(position != parameters.end());
        TBMBPKAMCount_ = position->second;

        position = parameters.find("TBMADelay:");
        assert(position != parameters.end());
        TBMADelay_ = position->second;

        position = parameters.find("TBMBDelay:");
        assert(position != parameters.end());
        TBMBDelay_ = position->second;

        position = parameters.find("TBMAAutoResetCount:");
        if (position != parameters.end())
            TBMAAutoResetCount_ = position->second;
        else
            TBMAAutoResetCount_ = 5;

        position = parameters.find("TBMBAutoResetCount:");
        if (position != parameters.end())
            TBMBAutoResetCount_ = position->second;
        else
            TBMBAutoResetCount_ = 5;

        position = parameters.find("TBMPLLDelay:");
        assert(position != parameters.end());
        TBMPLLDelay_ = position->second;

        position = parameters.find("TBMPLLBDelay:");
        //assert(position != parameters.end());
        if( position != parameters.end() ) 
	  {TBMPLLBDelay_ = position->second;}
	else {TBMPLLBDelay_ = 0;} // if it is not defined 

    } else {

        std::ifstream in(filename.c_str(), std::ios::binary);

        if (!in.good()) {
            std::cout << __LINE__ << mthn << "Could not open:" << filename << std::endl;
            assert(0);
        } else {
            std::cout << __LINE__ << mthn << "Opened:" << filename << std::endl;
        }

        char nchar;
        std::string s1;

        in.read(&nchar, 1);

        s1.clear();

        //wrote these lines of code without ref. needs to be fixed
        for (int i = 0; i < nchar; i++) {
            char c;
            in >> c;
            s1.push_back(c);
        }

        PixelROCName tmp(s1);

        rocid_ = tmp;

        in >> TBMABase0_;
        in >> TBMBBase0_;
        in >> TBMAAutoReset_;
        in >> TBMBAutoReset_;
        in >> TBMANoTokenPass_;
        in >> TBMBNoTokenPass_;
        in >> TBMADisablePKAMCounter_;
        in >> TBMBDisablePKAMCounter_;
        in >> TBMAPKAMCount_;
        in >> TBMBPKAMCount_;
        in >> TBMPLLDelay_;
        in >> TBMPLLBDelay_;
        in >> TBMADelay_;
        in >> TBMBDelay_;

        in.close();
    }
}

void PixelTBMSettings::setTBMGenericValue(std::string what, int value) {
    if (what == "TBMABase0") {
        TBMABase0_ = (unsigned char)value;
    } else if (what == "TBMBBase0") {
        TBMBBase0_ = (unsigned char)value;
    } else if (what == "TBMAAutoReset") {
        TBMAAutoReset_ = (unsigned char)value;
    } else if (what == "TBMAAutoResetCount") {
        TBMAAutoResetCount_ = (unsigned char)value;
    } else if (what == "TBMBAutoReset") {
        TBMBAutoReset_ = (unsigned char)value;
    } else if (what == "TBMBAutoResetCount") {
        TBMBAutoResetCount_ = (unsigned char)value;
    } else if (what == "TBMANoTokenPass") {
        TBMANoTokenPass_ = (unsigned char)value;
    } else if (what == "TBMBNoTokenPass") {
        TBMBNoTokenPass_ = (unsigned char)value;
    } else if (what == "TBMADisablePKAMCounter") {
        TBMADisablePKAMCounter_ = (unsigned char)value;
    } else if (what == "TBMBDisablePKAMCounter") {
        TBMBDisablePKAMCounter_ = (unsigned char)value;
    } else if (what == "TBMAPKAMCount") {
        TBMAPKAMCount_ = (unsigned char)value;
    } else if (what == "TBMBPKAMCount") {
        TBMBPKAMCount_ = (unsigned char)value;
    } else if (what == "TBMPLLDelay") {
        TBMPLLDelay_ = (unsigned char)value;
    } else if (what == "TBMPLLBDelay") {
        TBMPLLBDelay_ = (unsigned char)value;
    } else if (what == "TBMADelay") {
        TBMADelay_ = (unsigned char)value;
    } else if (what == "TBMBDelay") {
        TBMBDelay_ = (unsigned char)value;
    } else {
        std::cout << __LINE__ << "]\t[PixelTBMSettings::setTBMGenericValue()]\t\tFATAL: invalid key/value pair: " << what << "/" << value << std::endl;
        assert(0);
    }
}

void PixelTBMSettings::writeBinary(std::string filename) const {

    std::ofstream out(filename.c_str(), std::ios::binary);

    out << (char)rocid_.rocname().size();
    out.write(rocid_.rocname().c_str(), rocid_.rocname().size());

    out << TBMABase0_;
    out << TBMBBase0_;
    out << TBMAAutoReset_;
    out << TBMAAutoResetCount_;
    out << TBMBAutoReset_;
    out << TBMBAutoResetCount_;
    out << TBMANoTokenPass_;
    out << TBMBNoTokenPass_;
    out << TBMADisablePKAMCounter_;
    out << TBMBDisablePKAMCounter_;
    out << TBMAPKAMCount_;
    out << TBMBPKAMCount_;
    out << TBMPLLDelay_;
    out << TBMPLLBDelay_;
    out << TBMADelay_;
    out << TBMBDelay_;
}

void PixelTBMSettings::writeASCII(std::string dir) const {

    PixelModuleName module(rocid_.rocname());

    if (dir != "")
        dir += "/";
    std::string filename = dir + "TBM_module_" + module.modulename() + ".dat";

    std::ofstream out(filename.c_str());

    out << rocid_.rocname() << std::endl;

    out << "TBMABase0: " << (int)TBMABase0_ << std::endl;
    out << "TBMBBase0: " << (int)TBMBBase0_ << std::endl;
    out << "TBMAAutoReset: " << (int)TBMAAutoReset_ << std::endl;
    out << "TBMAAutoResetCount: " << (int)TBMAAutoResetCount_ << std::endl;
    out << "TBMBAutoReset: " << (int)TBMBAutoReset_ << std::endl;
    out << "TBMBAutoResetCount: " << (int)TBMBAutoResetCount_ << std::endl;
    out << "TBMANoTokenPass: " << (int)TBMANoTokenPass_ << std::endl;
    out << "TBMBNoTokenPass: " << (int)TBMBNoTokenPass_ << std::endl;
    out << "TBMADisablePKAMCounter: " << (int)TBMADisablePKAMCounter_ << std::endl;
    out << "TBMBDisablePKAMCounter: " << (int)TBMBDisablePKAMCounter_ << std::endl;
    out << "TBMAPKAMCount: " << (int)TBMAPKAMCount_ << std::endl;
    out << "TBMBPKAMCount: " << (int)TBMBPKAMCount_ << std::endl;
    out << "TBMPLLDelay: " << (int)TBMPLLDelay_ << std::endl;
    out << "TBMPLLBDelay: " << (int)TBMPLLBDelay_ << std::endl;
    out << "TBMADelay: " << (int)TBMADelay_ << std::endl;
    out << "TBMBDelay: " << (int)TBMBDelay_ << std::endl;
}

//Modified: This now will only Load Configuration data to DDR, will NOT send
void PixelTBMSettings::generateConfiguration(PixelFECConfigInterface *pixelFEC,
                                             PixelNameTranslation *trans, pos::PixelPortcardMap *portcardmap,
                                             bool physics, bool doResets) const {

    //always buffer otherwise it will overwrite DDR config data
    const bool bufferData = true;
    //local flag, not same as in PixFECSupervisor, keep it false to not send data
    const bool onlySEND = false;

    PixelHdwAddress theROC = *(trans->getHdwAddress(rocid_));
    int mfec = theROC.mfec();
    int mfecchannel = theROC.mfecchannel();
    int tbmchannel = 14;
    int tbmchannelB = 15;
    int hubaddress = theROC.hubaddress();

    PixelChannel theChannel = trans->getChannelFromHdwAddress(theROC);
    std::pair<std::string, int> portCardAndAOH = portcardmap->PortCardAndAOH(theChannel);
    unsigned int aoh_ch = portCardAndAOH.second;

    //Moved this part to PixelFECSupervisor, since this is not Loadable in DDR
    /*
    if (doResets) {
      pixelFEC->injectrsttbm(mfec, 1);
      pixelFEC->injectrstroc(mfec,1);
    }
    pixelFEC->enablecallatency(mfec,0);
    pixelFEC->disableexttrigger(mfec,0);
    pixelFEC->injecttrigger(mfec,0);
    pixelFEC->callatencycount(mfec,79);
    */

    //Check with Benedikt, since doResets=false those tbmcmd are not Loaded in DDR
    if (!onlySEND) {
        unsigned int base0_A = TBMABase0_, base0_B = TBMBBase0_;
        if (!TBMAAutoReset_)
            base0_A |= 0x80;
        if (!TBMBAutoReset_)
            base0_B |= 0x80;
        if (TBMANoTokenPass_)
            base0_A |= 0x40;
        if (TBMBNoTokenPass_)
            base0_B |= 0x40;
        if (TBMADisablePKAMCounter_)
            base0_A |= 0x1;
        if (TBMBDisablePKAMCounter_)
            base0_B |= 0x1;

        if (PRINT)
            cout << " setup TBMs " << endl;
        if (doResets)
            pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannel, hubaddress, 4, 2, 0x14, 0, bufferData);
        if (PRINT)
            cout << " TBM RESET TBM A: 2 0x14" << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannel, hubaddress, 4, 0, base0_A, 0, bufferData);
        if (PRINT)
            cout << " TBM A Reg: 0 " << hex << base0_A << dec << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannel, hubaddress, 4, 1, 0xC0, 0, bufferData);
        // setting the mode, we should always stay in the CAL = 0xC0 mode since the EventNumberClear Mode = 0x80 does not work correctly (Not True!)
        if (PRINT)
            cout << " TBM A Reg: 1 0xC0" << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannel, hubaddress, 4, 4, TBMAPKAMCount_, 0, bufferData);
        if (PRINT)
            cout << " TBM A Reg: 4 " << hex << int(TBMAPKAMCount_) << dec << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannel, hubaddress, 4, 5, TBMADelay_, 0, bufferData);
        if (PRINT)
            cout << " TBM A Reg: 5 " << hex << int(TBMADelay_) << dec << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannel, hubaddress, 4, 6, TBMAAutoResetCount_, 0, bufferData);
        if (PRINT)
            cout << " TBM A Reg: 6 " << hex << int(TBMAAutoResetCount_) << dec << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannel, hubaddress, 4, 7, TBMPLLDelay_, 0, bufferData);
        if (PRINT)
            cout << " TBM A Reg: 7 " << hex << int(TBMPLLDelay_) << dec << endl;

        if (doResets)
            pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannelB, hubaddress, 4, 2, 0x14, 0, bufferData);
        if (PRINT)
            cout << " TBM RESET TBM B: 2 0x14" << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannelB, hubaddress, 4, 0, base0_B, 0, bufferData);
        if (PRINT)
            cout << " TBM B Reg: 0 " << hex << base0_B << dec << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannelB, hubaddress, 4, 1, 0xC0, 0, bufferData);
        // setting the mode, we should always stay in the CAL = 0xC0 mode since the EventNumberClear Mode = 0x80 does not work correctly (Not True!)
        if (PRINT)
            cout << " TBM B Reg: 1 0xC0" << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannelB, hubaddress, 4, 4, TBMBPKAMCount_, 0, bufferData);
        if (PRINT)
            cout << " TBM B Reg: 4 " << hex << int(TBMBPKAMCount_) << dec << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannelB, hubaddress, 4, 5, TBMBDelay_, 0, bufferData);
        if (PRINT)
            cout << " TBM B Reg: 5 " << hex << int(TBMBDelay_) << dec << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannelB, hubaddress, 4, 6, TBMBAutoResetCount_, 0, bufferData);
        if (PRINT)
            cout << " TBM B Reg: 6 " << hex << int(TBMBAutoResetCount_) << dec << endl;
        pixelFEC->tbmcmd(mfec, mfecchannel, tbmchannelB, hubaddress, 4, 7, TBMPLLBDelay_, 0, bufferData);
        if (PRINT)
            cout << " TBM B Reg: 7 " << hex << int(TBMPLLBDelay_) << dec << endl;
    }

    std::string offsetNode = pixelFEC->getDDRSegmentName(aoh_ch, "TBM");
    std::string maskBitNode = pixelFEC->getDDRMaskName(mfec, mfecchannel, aoh_ch, "TBM");

    if (onlySEND) { //Only SEND DDR data per module call, this is not being used
        pixelFEC->confDataSend(mfec, mfecchannel, hubaddress, offsetNode, maskBitNode);
    } else { // Only Load Data to DDR, (send has been disabled)
        if (bufferData)
            pixelFEC->ddrload(mfec, mfecchannel, hubaddress, offsetNode);
    }
}

namespace pos{
    std::ostream &operator<<(std::ostream &s, const PixelTBMSettings &tbm) {

        s << "Module: " << tbm.rocid_.rocname() << std::endl;
        s << "TBMABase0: " << tbm.TBMABase0_ << std::endl;
        s << "TBMBBase0: " << tbm.TBMBBase0_ << std::endl;
        s << "TBMAAutoReset: " << tbm.TBMAAutoReset_ << std::endl;
        s << "TBMBAutoReset: " << tbm.TBMBAutoReset_ << std::endl;
        s << "TBMAAutoResetCount: " << tbm.TBMAAutoResetCount_ << std::endl;
        s << "TBMBAutoResetCount: " << tbm.TBMBAutoResetCount_ << std::endl;
        s << "TBMANoTokenPass: " << tbm.TBMANoTokenPass_ << std::endl;
        s << "TBMBNoTokenPass: " << tbm.TBMBNoTokenPass_ << std::endl;
        s << "TBMADisablePKAMCounter: " << tbm.TBMADisablePKAMCounter_ << std::endl;
        s << "TBMBDisablePKAMCounter: " << tbm.TBMBDisablePKAMCounter_ << std::endl;
        s << "TBMAPKAMCount: " << tbm.TBMAPKAMCount_ << std::endl;
        s << "TBMBPKAMCount: " << tbm.TBMBPKAMCount_ << std::endl;
        s << "TBMPLLDelay: " << tbm.TBMPLLDelay_ << std::endl;
        s << "TBMPLLBDelay: " << tbm.TBMPLLBDelay_ << std::endl;
        s << "TBMADelay: " << tbm.TBMADelay_ << std::endl;
        s << "TBMBDelay: " << tbm.TBMBDelay_ << std::endl;
        return s;
    }
}
//=============================================================================================
void PixelTBMSettings::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[[PixelTBMSettings::writeXMLHeader()]\t\t\t    ";
    std::string mthn = s.str();
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_TBM_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_TBM</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel TBM</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel TBM</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel TBM Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}

//=============================================================================================
void PixelTBMSettings::writeXML(std::ofstream *outstream) const {
    std::string mthn = "]\t[PixelTBMSettings::writeXML()]\t\t\t    ";

    PixelModuleName module(rocid_.rocname());

    *outstream << "        <DATA>" << std::endl;
    *outstream << "            <TBM_NAME>" << module.modulename() << "</TBM_NAME>" << std::endl;
    *outstream << "            <ROC_NAME>" << rocid_.rocname() << "</ROC_NAME>" << std::endl;
    *outstream << "            <TBMA_BASE0>" << (int)TBMABase0_ << "</TBMA_BASE0>" << std::endl;
    *outstream << "            <TBMB_BASE0>" << (int)TBMBBase0_ << "</TBMB_BASE0>" << std::endl;
    *outstream << "            <TBMA_AUTO_RESET>" << (int)TBMAAutoReset_ << "</TBMA_AUTO_RESET>" << std::endl;
    *outstream << "            <TBMB_AUTO_RESET>" << (int)TBMBAutoReset_ << "</TBMB_AUTO_RESET>" << std::endl;
    *outstream << "            <TBMA_NOTOKEN_PASS>" << (int)TBMANoTokenPass_ << "</TBMA_NOTOKEN_PASS>" << std::endl;
    *outstream << "            <TBMB_NOTOKEN_PASS>" << (int)TBMBNoTokenPass_ << "</TBMB_NOTOKEN_PASS>" << std::endl;
    *outstream << "            <TBMA_DISABLE_PKAM_COUNTER>" << (int)TBMADisablePKAMCounter_ << "</TBMA_DISABLE_PKAM_COUNTER>" << std::endl;
    *outstream << "            <TBMB_DISABLE_PKAM_COUNTER>" << (int)TBMBDisablePKAMCounter_ << "</TBMB_DISABLE_PKAM_COUNTER>" << std::endl;
    *outstream << "            <TBMA_PKAM_COUNT>" << (int)TBMAPKAMCount_ << "</TBMA_PKAM_COUNT>" << std::endl;
    *outstream << "            <TBMB_PKAM_COUNT>" << (int)TBMBPKAMCount_ << "</TBMB_PKAM_COUNT>" << std::endl;
    *outstream << "            <TBM_PLL_DELAY>" << (int)TBMPLLDelay_ << "</TBM_PLL_DELAY>" << std::endl;
    *outstream << "            <TBM_PLL_B_DELAY>" << (int)TBMPLLDelay_ << "</TBM_PLL_B_DELAY>" << std::endl;
    *outstream << "            <TBMA_DELAY>" << (int)TBMADelay_ << "</TBMA_DELAY>" << std::endl;
    *outstream << "            <TBMB_DELAY>" << (int)TBMBDelay_ << "</TBMB_DELAY>" << std::endl;
    *outstream << "        </DATA>" << std::endl;
}

//=============================================================================================
void PixelTBMSettings::writeXMLTrailer(std::ofstream *outstream) const {
    std::string mthn = "]\t[PixelTBMSettings::writeXMLTrailer()]\t\t\t    ";

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT> " << std::endl;

    outstream->close();
}

void PixelTBMSettings::getDACs(std::map<std::string, unsigned int> &dacs) const {
    dacs.clear();

    dacs[k_DACName_TBMADelay] = TBMADelay_;
    dacs[k_DACName_TBMBDelay] = TBMBDelay_;
    dacs[k_DACName_TBMPLL] = TBMPLLDelay_;
    unsigned int tmp1 = TBMABase0_, tmp2 = TBMBBase0_;
    if (!TBMAAutoReset_)
        tmp1 |= 0x80;
    if (!TBMBAutoReset_)
        tmp2 |= 0x80;
    if (TBMANoTokenPass_)
        tmp1 |= 0x40;
    if (TBMBNoTokenPass_)
        tmp2 |= 0x40;
    if (TBMADisablePKAMCounter_)
        tmp1 |= 0x1;
    if (TBMBDisablePKAMCounter_)
        tmp2 |= 0x1;
    dacs[k_DACName_TBMNoPass] = tmp1;
}
