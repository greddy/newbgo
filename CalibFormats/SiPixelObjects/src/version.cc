#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "pixel/CalibFormats/SiPixelObjects/version.h"

GETPACKAGEINFO(SiPixelObjects)

void SiPixelObjects::checkPackageDependencies() {

    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(xcept);
    CHECKDEPENDENCY(xdaq);
}

std::set<std::string, std::less<std::string> > SiPixelObjects::getPackageDependencies() {

    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, xcept);
    ADDDEPENDENCY(dependencies, xdaq);
    return dependencies;
}
