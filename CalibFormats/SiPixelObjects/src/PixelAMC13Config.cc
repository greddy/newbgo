#include <cassert>
#include <cstdlib>
#include <sstream>
#include <stdexcept>
#include "CalibFormats/SiPixelObjects/interface/PixelAMC13Config.h"
#include "CalibFormats/SiPixelObjects/interface/Utility.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"

using namespace std;

namespace pos {
PixelAMC13Config::PixelAMC13Config(std::vector<std::vector<std::string> > &tableMat)
    : PixelConfigBase(" ", " ", " ") {
    std::string mthn = "[PixelAMC13Config::PixelAMC13Config()]\t\t    ";
    vector<string> ins = tableMat[0];
    map<string, int> colM;
    vector<string> colNames;

    colNames.push_back("CRATE");
    colNames.push_back("URI_T1");
    colNames.push_back("URI_T2");
    colNames.push_back("SLOTMASK");
    colNames.push_back("CALBX");
    colNames.push_back("L1A_DELAY");
    colNames.push_back("NEWWAY");
    colNames.push_back("VERIFY_L1A");

    for (unsigned int c = 0; c < tableMat[0].size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    }

    for (unsigned int r = 1; r < tableMat.size(); r++) {
        unsigned int crate = atoi(tableMat[r][colM["CRATE"]].c_str());
        std::string uriT1 = tableMat[r][colM["URI_T1"]].c_str();
        std::string uriT2 = tableMat[r][colM["URI_T2"]].c_str();
        std::string slotMask = tableMat[r][colM["SLOTMASK"]].c_str();
        unsigned calBX = strtoul(tableMat[r][colM["CALBX"]].c_str(), 0, 10);
        unsigned l1ADelay = strtoul(tableMat[r][colM["L1A_DELAY"]].c_str(), 0, 10);
        std::string newWayStr = tableMat[r][colM["NEWWAY"]].c_str();
        assert(newWayStr.size() == 1);
        bool newWay = tolower(newWayStr[0]) == 'y';
        std::string verifyL1AStr = tableMat[r][colM["VERIFY_L1A"]].c_str();
        assert(verifyL1AStr.size() == 1);
        bool verifyL1A = tolower(verifyL1AStr[0]) == 'y';

        PixelAMC13Parameters p;
        p.setCrate(crate);
        p.setUriT1(uriT1);
        p.setUriT2(uriT2);
        p.setSlotMask(slotMask);
        p.setCalBX(calBX);
        p.setL1ADelay(l1ADelay);
        p.setNewWay(newWay);
        p.setVerifyL1A(verifyL1A);

        params[crate] = p;
    }
}

std::pair<bool, PixelAMC13Parameters> PixelAMC13Config::parse_line(const std::string &line) {
    std::pair<bool, PixelAMC13Parameters> ret;
    ret.first = false;
    PixelAMC13Parameters &p = ret.second;

    if (line[0] != '#' && line.find_first_not_of(" \t") != std::string::npos) {
        std::vector<std::string> tokens = tokenize(line, true);
        const size_t ntokens = tokens.size();
        if (ntokens) {
            if (ntokens != 8 && ntokens != 10)
                throw std::runtime_error("# tokens not equal to 8 or 10: line: " + line);
            else {
                p.setCrate(atoi(tokens[0].c_str()));
                p.setUriT1(tokens[1]);
                p.setUriT2(tokens[2]);
                p.setSlotMask(tokens[3]);
                p.setCalBX(strtoul(tokens[4].c_str(), 0, 10));
                p.setL1ADelay(strtoul(tokens[5].c_str(), 0, 10));
                p.setNewWay(tolower(tokens[6][0]) == 'y');
                p.setVerifyL1A(tolower(tokens[7][0]) == 'y');
                if (ntokens == 10) {
                    p.setAddressT1(tokens[8]);
                    p.setAddressT2(tokens[9]);
                }
                ret.first = true;
            }
        }
    }

    return ret;
}

PixelAMC13Config::PixelAMC13Config(std::string filename)
    : PixelConfigBase(" ", " ", " ") {
    std::string mthn = "]\t[PixelAMC13Config::PixelAMC13Config()]\t\t\t    ";
    std::ifstream in(filename.c_str());

    if (!in.good()) {
        std::cout << __LINE__ << mthn << "Could not open: " << filename << std::endl;
        throw std::runtime_error("Failed to open file " + filename);
    } else
        std::cout << __LINE__ << mthn << "Opened : " << filename << std::endl;

    std::string line;
    while (getline(in, line)) {
        std::pair<bool, PixelAMC13Parameters> r = parse_line(line);
        if (r.first)
            params[r.second.getCrate()] = r.second;
    }
}

std::string PixelAMC13Config::toASCII() const {
    std::ostringstream out;
    out << "# crate  uriT1                                                      uriT2                                                 slotMask                 calBX  L1ADelay  DoNothing  NewWay  VerifyL1A   [addressT1, addressT2 columns here if you want, otherwise assumed to use system amc13 etc]\n";
    char silly[2] = { 'n', 'y' };
    for (param_map::const_iterator it = params.begin(), ite = params.end(); it != ite; ++it) {
        PixelAMC13Parameters p = it->second;
        assert(p.getCrate() == it->first);
        out << p.getCrate()
            << " " << p.getUriT1()
            << " " << p.getUriT2()
            << " " << p.getSlotMask()
            << " " << p.getCalBX()
            << " " << p.getL1ADelay()
            << " " << silly[p.getNewWay()]
            << " " << silly[p.getVerifyL1A()]
            << " " << p.getAddressT1()
            << " " << p.getAddressT2()
            << "\n";
    }
    return out.str();
}

void PixelAMC13Config::writeASCII(std::string dir) const {
    if (dir != "")
        dir += "/";
    std::string filename = dir + "amc13.dat";
    std::ofstream out(filename.c_str());
    out << toASCII();
    out.close();
}

void PixelAMC13Config::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[[PixelAMC13Config::writeXMLHeader()]\t\t\t    ";
    std::string mthn = s.str();
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_AMC13_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_AMC13</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel AMC13 Settings</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel AMC13 Settings</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel AMC13 Settings Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}

void PixelAMC13Config::writeXML(std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[PixelAMC13Config::writeXML()]\t\t\t    ";
    std::string mthn = s.str();

    char silly[2] = { 'n', 'y' };
    for (param_map::const_iterator it = params.begin(), ite = params.end(); it != ite; ++it) {
        PixelAMC13Parameters p = it->second;
        assert(p.getCrate() == it->first);
        *outstream << "        <DATA>" << std::endl;
        *outstream << "            <CRATE>" << p.getCrate() << "</CRATE>" << std::endl;
        *outstream << "            <URI_T1>" << p.getUriT1() << "</URI_T1>" << std::endl;
        *outstream << "            <URI_T2>" << p.getUriT2() << "</URI_T2>" << std::endl;
        *outstream << "            <SLOTMASK>" << p.getSlotMask() << "</SLOTMASK>" << std::endl;
        *outstream << "            <CALBX>" << p.getCalBX() << "</CALBX>" << std::endl;
        *outstream << "            <L1A_DELAY>" << p.getL1ADelay() << "</L1A_DELAY>" << std::endl;
        *outstream << "            <NEWWAY>" << silly[p.getNewWay()] << "</NEWWAY>" << std::endl;
        *outstream << "            <VERIFY_L1A>" << silly[p.getVerifyL1A()] << "</VERIFY_L1A>" << std::endl;
        *outstream << "        </DATA>" << std::endl;
    }
}

void PixelAMC13Config::writeXMLTrailer(std::ofstream *outstream) const {
    std::stringstream s;
    s << __LINE__ << "]\t[PixelAMC13Config::writeXMLTrailer()]\t\t\t    ";
    std::string mthn = s.str();

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT> " << std::endl;

    outstream->close();
}
}
