//
// This class provides the mapping between
// modules , the dcdc converters, and power groups
//
//
//

#include "CalibFormats/SiPixelObjects/interface/PixelPowerMap.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"

#include <sstream>
#include <cassert>
#include <stdexcept>

using namespace pos;
using namespace std;

PixelPowerMap::PixelPowerMap(std::vector<std::vector<std::string> > &tableMat)
    : PixelConfigBase(" ", " ", " ") {
    std::string mthn = "[PixelPowerMap::PixelPowerMap()]\t\t\t    ";
    std::vector<std::string> ins = tableMat[0];
    std::map<std::string, int> colM;
    std::vector<std::string> colNames;

    colNames.push_back("MODULE");
    colNames.push_back("DCDC_CNVRTR");
    colNames.push_back("POWER_GRP");

    for (unsigned int c = 0; c < ins.size(); c++) {
        for (unsigned int n = 0; n < colNames.size(); n++) {
            if (tableMat[0][c] == colNames[n]) {
                colM[colNames[n]] = c;
                break;
            }
        }
    } //end for
    for (unsigned int n = 0; n < colNames.size(); n++) {
        if (colM.find(colNames[n]) == colM.end()) {
            std::cerr << __LINE__ << "]\t" << mthn << "Couldn't find in the database the column with name " << colNames[n] << std::endl;
            assert(0);
        }
    }

    std::string dcdcname;
    std::string modulename;
    std::string powergroupname;

    for (unsigned int r = 1; r < tableMat.size(); r++) { //Goes to every row of the Matrix
        modulename = tableMat[r][colM["MODULE"]];
        dcdcname = tableMat[r][colM["DCDC_CNVRTR"]];
        powergroupname = tableMat[r][colM["POWER_GRP"]];
        PixelModuleName module(modulename);
        if (module.modulename() != modulename) {
            std::cout << __LINE__ << "]\t" << mthn << "Modulename: " << modulename << std::endl;
            std::cout << __LINE__ << "]\t" << mthn << "Parsed to : " << module.modulename() << std::endl;
            assert(0);
        }

        std::pair<std::string, std::string> dcdcAndPowergroup(dcdcname, powergroupname);
        map_[module] = dcdcAndPowergroup;
    } //end for r
} //end constructor
//*****************************************************************************

PixelPowerMap::PixelPowerMap(std::string filename)
    : PixelConfigBase(" ", " ", " ") {

    std::string mthn = "[PixelPowerMap::PixelPowerMap()]\t\t\t    ";
    std::ifstream in(filename.c_str());

    if (!in.good()) {
        std::cout << __LINE__ << "]\t" << mthn << "Could not open: " << filename << std::endl;
        throw std::runtime_error("Failed to open file " + filename);
    } else {
        std::cout << __LINE__ << "]\t" << mthn << "Reading from: " << filename << std::endl;
    }

    std::string dummy;

    in >> dummy;
    in >> dummy;
    in >> dummy;
    in >> dummy;

    do {

        std::string dcdcname;
        std::string modulename;
        std::string powergroupname;

        in >> modulename >> dcdcname >> powergroupname;

        if (!in.eof()) {
            PixelModuleName module(modulename);
            if (module.modulename() != modulename) {
                std::cout << __LINE__ << "]\t" << mthn << "Modulename: " << modulename << std::endl;
                std::cout << __LINE__ << "]\t" << mthn << "Parsed to : " << module.modulename() << std::endl;
                assert(0);
            }

            std::pair<std::string, std::string> dcdcAndPowergroup(dcdcname, powergroupname);
            map_[module] = dcdcAndPowergroup;
        }

    } while (!in.eof());
}

PixelPowerMap::~PixelPowerMap() {}

void PixelPowerMap::writeASCII(std::string dir) const {

    std::string mthn = "[PixelPowerMap::writeASCII()]\t\t\t\t    ";
    if (dir != "")
        dir += "/";
    string filename = dir + "powermap.dat";

    ofstream out(filename.c_str());
    if (!out.good()) {
        cout << __LINE__ << "]\t" << mthn << "Could not open file: " << filename << endl;
        assert(0);
    }

    out << "# Module          DCDC converter                     Power group" << endl;
    std::map<PixelModuleName, std::pair<std::string, std::string> >::const_iterator i = map_.begin();
    for (; i != map_.end(); ++i) {
        out << i->first.modulename() << "       "
            << i->second.first << "       "
            << i->second.second << endl;
    }
    out.close();
}

// pair of DCDC converter and power group relevant for module
const std::pair<std::string, std::string> PixelPowerMap::dcdcAndPowergroup(const PixelModuleName &aModule) const {
    std::map<PixelModuleName, std::pair<std::string, std::string> >::const_iterator found = map_.find(aModule);
    if (found == map_.end()) {
        std::pair<std::string, std::string> returnThis("none", "none");
        return returnThis;
    } else {
        return found->second;
    }
}

// set of all modules attached to a dcdc converter in a powergroup
std::set<PixelModuleName> PixelPowerMap::modules(std::string dcdcName, std::string powergroupName) const {
    std::set<PixelModuleName> returnThis;

    // Loop over the entire map, searching for elements matching PixelModuleName.  Add matching elements to returnThis.
    for (std::map<PixelModuleName, std::pair<std::string, std::string> >::const_iterator map_itr = map_.begin(); map_itr != map_.end(); ++map_itr) {
        if (map_itr->second.first == dcdcName && map_itr->second.second == powergroupName) {
            returnThis.insert(map_itr->first);
        }
    }
    return returnThis;
}

// set of all modules attached to a dcdc converter in a powergroup
std::set<PixelModuleName> PixelPowerMap::modulesPG(std::string powergroupName) const {
    std::set<PixelModuleName> returnThis;

    // Loop over the entire map, searching for elements matching PixelModuleName.  Add matching elements to returnThis.
    for (std::map<PixelModuleName, std::pair<std::string, std::string> >::const_iterator map_itr = map_.begin(); map_itr != map_.end(); ++map_itr) {
        if (map_itr->second.second == powergroupName) {
            returnThis.insert(map_itr->first);
        }
    }
    return returnThis;
}

//=============================================================================================
void PixelPowerMap::writeXMLHeader(int version, std::string path, std::ofstream *outstream) const {
    std::string mthn = "[PixelPowerMap::writeXMLHeader()]\t\t\t    ";
    std::stringstream fullPath;
    fullPath << path << "/PH1_PXL_POWER_MAP_" << PixelTimeFormatter::getmSecTime() << ".xml";
    std::cout << __LINE__ << "]\t" << mthn << "Writing to: " << fullPath.str() << std::endl;

    outstream->open(fullPath.str().c_str());

    *outstream << "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>" << std::endl;
    *outstream << "<ROOT xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <HEADER>" << std::endl;
    *outstream << "    <HINTS mode='only-det-root'/>" << std::endl;
    *outstream << "        <TYPE>" << std::endl;
    *outstream << "            <EXTENSION_TABLE_NAME>PH1_PXL_POWER_MAP</EXTENSION_TABLE_NAME>" << std::endl;
    *outstream << "            <NAME>Ph1 Pixel Power Map</NAME>" << std::endl;
    *outstream << "        </TYPE>" << std::endl;
    *outstream << "        <RUN>" << std::endl;
    *outstream << "            <RUN_TYPE>Ph1 Pixel Power Map</RUN_TYPE>" << std::endl;
    *outstream << "            <RUN_NUMBER>1</RUN_NUMBER>" << std::endl;
    *outstream << "            <RUN_BEGIN_TIMESTAMP>" << pos::PixelTimeFormatter::getTime() << "</RUN_BEGIN_TIMESTAMP>" << std::endl;
    *outstream << "            <COMMENT_DESCRIPTION>Ph1 Pixel Power Map Test</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "            <LOCATION>CERN</LOCATION>" << std::endl;
    *outstream << "            <INITIATED_BY_USER>" << getAuthor() << "</INITIATED_BY_USER>" << std::endl;
    *outstream << "        </RUN>" << std::endl;
    *outstream << "    </HEADER>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "    <DATA_SET>" << std::endl;
    *outstream << "        <VERSION>" << version << "</VERSION>" << std::endl;
    *outstream << "        <COMMENT_DESCRIPTION>" << getComment() << "</COMMENT_DESCRIPTION>" << std::endl;
    *outstream << "" << std::endl;
    *outstream << "        <PART>" << std::endl;
    *outstream << "            <NAME_LABEL>CMS-PIXEL-ROOT</NAME_LABEL>" << std::endl;
    *outstream << "            <KIND_OF_PART>Detector ROOT</KIND_OF_PART>" << std::endl;
    *outstream << "        </PART>" << std::endl;
    *outstream << "" << std::endl;
}

//=============================================================================================
void PixelPowerMap::writeXML(std::ofstream *outstream) const {
    std::string mthn = "[PixelPowerMap::writeXML()]\t\t\t    ";

    std::map<PixelModuleName, std::pair<std::string, std::string> >::const_iterator i = map_.begin();
    for (; i != map_.end(); ++i) {
        *outstream << "        <DATA>" << std::endl;
        *outstream << "            <MODULE>" << i->first.modulename() << "</MODULE>" << std::endl;
        *outstream << "            <DCDC_CNVRTR>" << i->second.first << "</DCDC_CNVRTR>" << std::endl;
        *outstream << "            <POWER_GRP>" << i->second.second << "</POWER_GRP> " << std::endl;
        *outstream << "        </DATA>" << std::endl;
    }
}

//=============================================================================================
void PixelPowerMap::writeXMLTrailer(std::ofstream *outstream) const {
    std::string mthn = "[PixelPowerMap::writeXMLTrailer()]\t\t\t    ";

    *outstream << "    </DATA_SET>" << std::endl;
    *outstream << "</ROOT> " << std::endl;

    outstream->close();
}
