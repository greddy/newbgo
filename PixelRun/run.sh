#!/bin/sh
DTSTAMP=`awk "BEGIN{print strftime(\"%y%b%d_%H%M\");}" `
LOGFN=${POS_LOG_DIR}/log.$DTSTAMP
rm -f lastlog
ln -s $LOGFN lastlog
echo $LOGFN
echo

MY_PATH="`dirname \"$0\"`"              # relative
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"  # absolutized and normalized

gdb=0
gdbr=0
dcs=1
xml=0
hostn=$(hostname --long)
port=1973
b2inport=1914
ttcci=0
tcds=0
monitor=0

while test $# -gt 0
do
    case "$1" in
        --gdb) gdb=1
            ;;
        --gdbr) gdbr=1
            ;;
        --nodcs) dcs=0
            ;;
        --ttcci)
            shift
            ttcci=$1
            ;;
        --tcds) tcds=1
            ;;
        --xml)
            shift
            xml=$1
            ;;
        --hostn)
            shift
            hostn=$1
            ;;
        --port)
            shift
            port=$1
            ;;
        --b2inport)
            shift
            b2inport=$1
            ;;
        --*) echo "bad option $1"
            ;;
        *) echo "argument $1 not parsed"
            ;;
    esac
    shift
done

cmd="-z pixel -p $port -c "

TTCSupervisorApplicationName=PixelAMC13Controller
LTCSupervisorString=""
SimTTC=true
usettc=true
usetcds=false
if [ "$ttcci" != "0" ]; then
    TTCSupervisorApplicationName=ttc::TTCciControl
fi
if [ "$tcds" != "0" ]; then
    TTCSupervisorApplicationName=pixel::tcds::PixeliCISupervisor
    LTCSupervisorString='<LTCSupervisorApplicationName xsi:type="xsd:string">pixel::tcds::PixelPISupervisor</LTCSupervisorApplicationName>'
    usettc=false
    usetcds=true
fi

if [ "$xml" == "auto" ]; then
    # Generate auto.xml config file for the XDAQ
    # begin
    cat ${MY_PATH}/xml/auto/begin_.xml > auto.xml
    cat ${MY_PATH}/xml/auto/cont0_begin_so_.xml | sed \
        -e "s:__hostn__:${hostn}:g" \
        -e "s:__port__:${port}:g" \
        -e "s:__b2inport__:${b2inport}:g" \
        -e "s:__ENV_CMS_TK_ONLINE_ROOT__:${ENV_CMS_TK_ONLINE_ROOT}:g" \
        -e "s:__XDAQ_ROOT__:${XDAQ_ROOT}:g" \
        -e "s:__BUILD_HOME__:${BUILD_HOME}:g" \
        -e "s:__CACTUS_ROOT__:${CACTUS_ROOT}:g" \
        -e "s:__ROOTSYS__:${ROOTSYS}:g" \
        >> auto.xml
    cat ${MY_PATH}/xml/auto/cont0_main_.xml | sed \
        -e "s:__usetcds__:${usetcds}:g" \
        -e "s:__usettc__:${usettc}:g" \
        -e "s:__TTCSupervisorApplicationName__:${TTCSupervisorApplicationName}:g" \
        -e "s:__LTCSupervisorString__:${LTCSupervisorString}:g" \
        -e "s:__SimTTC__:${SimTTC}:g" \
        >> auto.xml
    
    if [ "$monitor" != "0" ] ; then
        cat ${MY_PATH}/xml/auto/cont0_monitor_.xml | sed \
            -e "s:__XDAQ_ROOT__:${XDAQ_ROOT}:g" \
            -e "s:__BUILD_HOME__:${BUILD_HOME}:g" \
            -e "s:__CACTUS_ROOT__:${CACTUS_ROOT}:g" \
            >> auto.xml
    fi
    if [ "$tcds" != "0" ]; then
        cat ${MY_PATH}/xml/auto/cont0_tcds_.xml >> auto.xml
    fi
    cat ${MY_PATH}/xml/auto/cont0_end_.xml >> auto.xml
    
    if [ "$ttcci" != "0" ]; then
        cat cont1_ttci_.xml | sed \
            -e "s:__XDAQ_ROOT__:${XDAQ_ROOT}:g" \
            -e "s:__ttcci__:${ttcci}:g" \
            >> auto.xml
    fi
    if [ "$tcds" != "0" ]; then
        cat ${MY_PATH}/xml/auto/cont2_tcds_.xml >> auto.xml
    fi
    # end
    cat ${MY_PATH}/xml/auto/end_.xml >> auto.xml
    
    cmd="$cmd $(pwd)/auto.xml"
elif [ "$xml" == "0" ]; then
    if [ "$dcs" == "1" ]; then
        cmd="$cmd ${BUILD_HOME}/pixel/XDAQConfiguration/XDAQ_ConfigurationPilotPix_AIO_TCDS_DCS.xml"
    else
        cmd="$cmd ${BUILD_HOME}/pixel/XDAQConfiguration/XDAQ_ConfigurationPilotPix_AIO_TCDS.xml"
    fi
else
    cmd="$cmd $xml"
fi

if [ "$gdb" == "1" ]; then
    cmd="gdb --args ${XDAQ_ROOT}/bin/xdaq.exe $cmd"
elif [ "$gdbr" == "1" ]; then
    cmd="gdb --ex run --args ${XDAQ_ROOT}/bin/xdaq.exe $cmd"
else
    cmd="${XDAQ_ROOT}/bin/xdaq.sh $cmd"
fi

cmd="$cmd | tee $LOGFN"
echo $cmd
eval $cmd 
