import sys, os
from pprint import pprint
from JMTTools import *
from JMTROOTTools import *
set_style()

dynrng = False #'nodynrng' not in sys.argv

run = run_from_argv()
run_dir = run_dir(run)
in_fn = glob(os.path.join(run_dir, 'total.root'))
if not in_fn:
    root_flist = glob(os.path.join(run_dir, 'PixelAlive_Fed_*_Run_%i.root' % run))
    if not root_flist:
        root_flist = glob(os.path.join(run_dir, 'PixelAlive_*.root'))
        if not root_flist:
            raise RuntimeError('need to run analysis first!')
    out_root = os.path.join(run_dir,'total.root')
    args = ' '.join(root_flist)
    cmd = 'hadd %s %s' %(out_root, args)
    os.system(cmd)
in_fn = glob(os.path.join(run_dir, 'total.root'))
in_fn = in_fn[0]
out_dir = os.path.join(run_dir, 'dump_pixelalive')
os.system('mkdir -p %s' % out_dir)

f = ROOT.TFile(in_fn)

dirs = ['BPix/BPix_%(hc)s/BPix_%(hc)s_SEC%(sec)i/BPix_%(hc)s_SEC%(sec)i_LYR%(lyr)i/BPix_%(hc)s_SEC%(sec)i_LYR%(lyr)i_LDR%(ldr)i%(let)s/BPix_%(hc)s_SEC%(sec)i_LYR%(lyr)i_LDR%(ldr)i%(let)s_MOD%(mod)i' % locals() for hc in ['BmI', 'BmO', 'BpI', 'BpO'] for sec in range(1,9) for lyr in range(1,5) for ldr in range(1,33) for let in ['H','F'] for mod in range(1,5) ]

#print dirs

by_ntrigs = []
first = True

c = ROOT.TCanvas('c', '', 1300, 1000)
c.Divide(4,4)
c.cd(0)
pdf_fn = os.path.join(out_dir, 'all.pdf')
c.Print(pdf_fn + '[')

num_dead = defaultdict(int)
eff_thresh = 100.

for d in dirs:
    if not f.Get(d):
        continue
    for ikey, key in enumerate(f.Get(d).GetListOfKeys()):
        obj = key.ReadObj()
        name = obj.GetName().replace(' (inv)', '')
        rest, roc = name.split('ROC')
        if 'ADC' in roc: continue
        iroc = int(roc)
        if int(roc) < 10:
            name = rest + 'ROC0' + roc
        mineff, maxeff = 100., 0.
        ntrigs = int(obj.Integral())
        by_ntrigs.append((ntrigs, name))
        for x in xrange(1, obj.GetNbinsX()+1):
            for y in xrange(1, obj.GetNbinsY()+1):
                eff = obj.GetBinContent(x,y)
                mineff = min(eff, mineff)
                maxeff = max(eff, maxeff)
                if obj.GetBinContent(x,y) < eff_thresh:
                    num_dead[name] += 1
        c.cd(iroc+1)
        if dynrng:
            if maxeff - mineff < 1:
                maxeff += 1
            obj.SetMinimum(mineff)
            obj.SetMaximum(maxeff)
        obj.Draw('colz')
    c.cd(0)
    c.SaveAs(os.path.join(out_dir, d.split('/')[-1]) + '.png')
    c.Print(pdf_fn)
c.Print(pdf_fn + ']')

by_ntrigs.sort(key=lambda x: x[1])
by_ntrigs.sort(key=lambda x: x[0], reverse=True)
pprint(by_ntrigs)

if 'html' in sys.argv:
    pngs = [f for f in os.listdir(out_dir) if os.path.isfile(os.path.join(out_dir,f)) and f.endswith('png')]
    pngs.sort()

    html_fn = os.path.join(out_dir, 'index.html')
    html = open(html_fn, 'wt')
    html.write('<html><body>\n')

    for item in pngs:
    	html.write('<br><h1>%s</h1>\n' %item)
    	html.write('<img src="%s">\n' %item)

    html.write('</body></html>\n')
    html.close()
    os.system('firefox %s' %html_fn)
else:
    os.system('evince %s' %pdf_fn)

print '# dead pixels (eff lower than %f):' % eff_thresh
for roc in sorted(num_dead.keys()):
    if num_dead[roc] and num_dead[roc]>167:
        print '%s %10i      %.2f%%' % (roc.ljust(35), num_dead[roc], 100.0*float(num_dead[roc])/float(4160))


if 'scp' in sys.argv:
    remote_dir = 'public_html/qwer/dump_pixelalive/%i' % run
    cmd = 'ssh jmt46@lnx201.lns.cornell.edu "mkdir -p %s"' % remote_dir
    print cmd
    os.system(cmd)
    cmd = 'scp -r %s/* jmt46@lnx201.lns.cornell.edu:%s' % (out_dir, remote_dir)
    print cmd
    os.system(cmd)
