import sys, os
from pprint import pprint
from JMTTools import *
from JMTROOTTools import *
set_style()

dynrng = False #'nodynrng' not in sys.argv

run = run_from_argv()
run_dir = run_dir(run)
in_fn = glob(os.path.join(run_dir, 'PixelAlive_BPix.root'))
f = ROOT.TFile(in_fn,"READ")

dirs = ['BPix/BPix_%(hc)s/BPix_%(hc)s_SEC%(sec)i/BPix_%(hc)s_SEC%(sec)i_LYR%(lyr)i/BPix_%(hc)s_SEC%(sec)i_LYR%(lyr)i_LDR%(ldr)i%(let)s/BPix_%(hc)s_SEC%(sec)i_LYR%(lyr)i_LDR%(ldr)i%(let)s_MOD%(mod)i' % locals() for hc in ['BmI', 'BmO', 'BpI', 'BpO'] for sec in range(1,9) for lyr in range(1,5) for ldr in range(1,33) for let in ['H','F'] for mod in range(1,5) ]

#print dirs

by_ntrigs = []
first = True


num_noisy = defaultdict(int)
eff_thresh = 0.

for d in dirs:
    if not f.Get(d):
        continue
    for ikey, key in enumerate(f.Get(d).GetListOfKeys()):
        obj = key.ReadObj()
        name = obj.GetName().replace(' (inv)', '')
        rest, roc = name.split('ROC')
        iroc = int(roc)
        if int(roc) < 10:
            name = rest + 'ROC0' + roc
        mineff, maxeff = 100., 0.
        ntrigs = int(obj.Integral())
        by_ntrigs.append((ntrigs, name))
        for x in xrange(1, obj.GetNbinsX()+1):
            for y in xrange(1, obj.GetNbinsY()+1):
                eff = obj.GetBinContent(x,y)
                mineff = min(eff, mineff)
                maxeff = max(eff, maxeff)
                if obj.GetBinContent(x,y) > eff_thresh:
                    num_noisy[name] += 1
        if dynrng:
            if maxeff - mineff < 1:
                maxeff += 1
            obj.SetMinimum(mineff)
            obj.SetMaximum(maxeff)

by_ntrigs.sort(key=lambda x: x[1])
by_ntrigs.sort(key=lambda x: x[0], reverse=True)
pprint(by_ntrigs)
