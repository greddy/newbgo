#ifndef _pixel_utils_InfoSpaceUpdaterPlain_h_
#define _pixel_utils_InfoSpaceUpdaterPlain_h_

#include "pixel/utils/InfoSpaceUpdater.h"

namespace xdaq {
class Application;
}

namespace pixel {
namespace utils {

class InfoSpaceUpdaterPlain : public pixel::utils::InfoSpaceUpdater {

  public:
    InfoSpaceUpdaterPlain(xdaq::Application &xdaqApp);
    virtual ~InfoSpaceUpdaterPlain();
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_InfoSpaceUpdaterPlain_h_
