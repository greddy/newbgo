#ifndef _pixel_utils_WebSpacer_h
#define _pixel_utils_WebSpacer_h

#include <cstddef>
#include <string>

#include "pixel/utils/WebObject.h"

namespace pixel {
namespace utils {

class Monitor;

class WebSpacer : public WebObject {

  public:
    WebSpacer(std::string const &name,
              std::string const &description,
              Monitor const &monitor,
              std::string const &itemSetName,
              std::string const &tabName,
              size_t const colSpan);

    virtual std::string getHTMLString() const;
    virtual std::string getJSONString() const;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_WebSpacer_h
