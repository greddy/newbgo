#ifndef _pixel_utils_Lock_h_
#define _pixel_utils_Lock_h_

#include "toolbox/BSem.h"

namespace pixel {
namespace utils {

class Lock {

  public:
    Lock(toolbox::BSem::State state = toolbox::BSem::EMPTY, bool recursive = true);
    ~Lock();

    void lock();
    void unlock();

  private:
    toolbox::BSem semaphore_;

    // Prevent copying.
    Lock(Lock const &);
    Lock &operator=(Lock const &);
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_Lock_h_
