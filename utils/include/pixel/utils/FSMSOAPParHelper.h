#ifndef _pixel_utils_FSMSOAPParHelper_h_
#define _pixel_utils_FSMSOAPParHelper_h_

#include <string>

namespace pixel {
namespace utils {

class FSMSOAPParHelper {

  public:
    FSMSOAPParHelper(std::string const &commandName,
                     std::string const &parameterName,
                     bool const isRequired = true /* , */
                     /* std::string const& defaultValue="" */);
    ~FSMSOAPParHelper();

    std::string commandName() const;
    std::string parameterName() const;
    bool isRequired() const;
    /* std::string defaultValue() const; */

  private:
    std::string commandName_;
    std::string parameterName_;
    bool isRequired_;
    /* std::string defaultValue_; */
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_FSMSOAPParHelper_h_
