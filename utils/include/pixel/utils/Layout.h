#ifndef _pixel_utils_Layout_h_
#define _pixel_utils_Layout_h_

#include <string>

#include "hyperdaq/framework/Layout.h"
#ifndef XDAQ_TARGET_XDAQ15
#include "xgi/exception/Exception.h"
#endif

/* namespace xcept { */
/*   class Exception; */
/* } */

namespace xgi {
class Input;
class Output;
}

namespace xgi {
namespace framework {
class UIManager;
}
}

namespace pixel {
namespace utils {

class UIManager;

class Layout : public hyperdaq::framework::Layout {

  public:
    Layout();
    virtual ~Layout();

#ifndef XDAQ_TARGET_XDAQ15
    // NOTE: This method needs the throw specifier, since the parent
    // class already has it.
    virtual void getHTMLHeader(xgi::framework::UIManager *manager,
                               xgi::Input *in,
                               xgi::Output *out);
#else
    virtual void getHTMLHeader(xgi::framework::UIManager *manager,
                               xgi::Input *in,
                               xgi::Output *out);
#endif

  private:
    // This helps in defining the exact Javascript script
    // includes. Guideline from
    // http://www.growingwiththeweb.com/2014/02/async-vs-defer-attributes.html:
    // Typically you want to use async where possible, then defer
    // then no attribute. Here are some general rules to follow:
    // - If the script is modular and does not rely on any scripts
    //   then use async.
    // - If the script relies upon or is relied upon by another
    //   script then use defer.
    // - If the script is small and is relied upon by an async
    //   script then use an inline script with no attributes placed
    //   above the async scripts.
    enum JS_SCRIPT_TYPE {
        JS_SCRIPT_TYPE_PLAIN,
        JS_SCRIPT_TYPE_ASYNC,
        JS_SCRIPT_TYPE_DEFER
    };

    std::string buildJSLink(std::string const &jsFile,
                            JS_SCRIPT_TYPE const type = JS_SCRIPT_TYPE_PLAIN,
                            std::string const &pars = "") const;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_Layout_h_
