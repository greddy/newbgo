#ifndef _pixel_utils_WebTableMultiCol_h
#define _pixel_utils_WebTableMultiCol_h

#include <cstddef>
#include <string>
#include <vector>

#include "pixel/utils/WebObject.h"

namespace pixel {
namespace utils {

class Monitor;

////bool sortStringPairVector(const std::pair<std::string, std::string> &p1,
                          ////const std::pair<std::string, std::string> &p2);

class WebTableMultiCol : public WebObject {

  public:
    WebTableMultiCol(std::string const &name,
                     std::string const &description,
                     Monitor const &monitor,
                     std::vector<std::string> const &itemSetNameVec,
                     std::string const &tabName,
                     std::string const &id="");

    std::string getHTMLString() const;
    std::string getHTMLthead() const;
    std::string getHTMLtbody() const;
    ////bool checked;

  private:
    std::string id_;
    ////bool checkItemSets() const;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_WebTableMultiCol_h
