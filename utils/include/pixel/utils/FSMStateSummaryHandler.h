#ifndef _pixel_FSMStateSummaryHandler_h_
#define _pixel_FSMStateSummaryHandler_h_


#include <string>

#include "pixel/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace pixel {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;

    class FSMStateSummaryHandler : public InfoSpaceHandler //This should inherit from the general InfoSpaceHandler
    {

    public:
      FSMStateSummaryHandler(xdaq::Application& xdaqApp,InfoSpaceUpdater* updater);
      virtual ~FSMStateSummaryHandler();

    protected:
      virtual void registerItemSetsWithMonitor(pixel::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
                                                 pixel::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  }
} // namespace pixel

#endif // _pixel_FSMStateSummaryHandler_h_
