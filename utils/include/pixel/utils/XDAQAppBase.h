#ifndef _pixel_utils_XDAQAppBase_h_
#define _pixel_utils_XDAQAppBase_h_

#include <memory>
#include <stdint.h>
#include <string>

#include "log4cplus/logger.h"
#include "toolbox/ActionListener.h"
#include "toolbox/task/TimerListener.h"
#include "xdaq/Application.h"
#include "xdaq/Event.h"
#include "xdata/ActionListener.h"
#ifndef XDAQ_TARGET_XDAQ15
#include "xoap/exception/Exception.h"
#endif
#include "xoap/MessageReference.h"

#include "pixel/hwlayer/DeviceBase.h"
#include "pixel/hwlayer/RegisterInfo.h"
#include "pixel/utils/ApplicationStateInfoSpaceHandler.h"
#include "pixel/utils/FSMSOAPParHelper.h"
#include "pixel/utils/InfoSpaceUpdaterPlain.h"
#include "pixel/utils/Lock.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/OwnerId.h"
#include "pixel/utils/WebServer.h"
#include "pixel/utils/SOAPER.h"

namespace log4cplus {
class Logger;
}

namespace toolbox {
class Event;
namespace task {
class Timer;
class TimerEvent;
}
}

namespace xcept {
class Exception;
}

namespace xdaq {
class ApplicationDescriptor;
class ApplicationStub;
}

namespace xdata {
class Event;
}

//namespace log4cplus{
//SharedAppenderPtr;
//}

/* namespace xgi { */
/*   class Input; */
/*   class Output; */
/* } */

namespace pixel {
namespace utils {

class ConfigurationInfoSpaceHandler;

class XDAQAppBase : public toolbox::task::TimerListener,
                    public toolbox::ActionListener,
                    public xdaq::Application,
                    public xdata::ActionListener,
                    public pixel::utils::SOAPER
                    /**
     * Abstract base class for all PIXEL top-level XDAQ applications
     * (or 'Controllers').
     */
                    {
    // The Monitor class is a friend so it has access to the
    // monitoringLock_.
    friend class Monitor;
    friend class WebServer;

  public:
    virtual ~XDAQAppBase();

    virtual void actionPerformed(xdata::Event &event);
    virtual void actionPerformed(toolbox::Event &event);

    std::string getFullURL();

    bool isHwLeased() const;
    bool wasHwLeased() const;
    pixel::utils::OwnerId getHwLeaseOwnerId() const;
    pixel::utils::OwnerId getExpiredHwLeaseOwnerId() const;
    void assignHwLease(pixel::utils::OwnerId const &leaseOwnerId);
    void renewHwLease();
    void revokeHwLease(bool const isExpiry = false);

    /**
       * Connect to hardware/release hardware.
       */
    void hwConnect();
    void hwRelease();

/**
       * Setting parameters straight from SOAP messages is not
       * allowed. This method is only implemented to send a SOAP fault
       * back in case someone tries it anyway.
       */
#ifndef XDAQ_TARGET_XDAQ15
    xoap::MessageReference parameterSet(xoap::MessageReference msg); // throw(xoap::exception::Exception);
#else
    xoap::MessageReference parameterSet(xoap::MessageReference msg);
#endif

    std::string readHardwareConfiguration();
    std::string readHardwareState();

    // Utility method to determine the application icon file path
    // for an application specified by the ApplicationDescriptor (or
    // the current application).
    std::string buildIconPathName(xdaq::ApplicationDescriptor const *const app = 0);

    // Access to inside information.
    virtual ConfigurationInfoSpaceHandler const &getConfigurationInfoSpaceHandler() const;
    // wsi: add write access to applicationStateInfoSpaceHandler to write app history msg
    ApplicationStateInfoSpaceHandler &getApplicationStateInfoSpaceHandler();

  protected:
    XDAQAppBase(xdaq::ApplicationStub *const stub,
                std::unique_ptr<pixel::hwlayer::DeviceBase> hw);

    // Some alarm handling helpers.
    void raiseAlarm(std::string const &baseName,
                    std::string const &severity,
                    xcept::Exception &err);
    void revokeAlarm(std::string const &baseName);
    std::string buildAlarmName(std::string const &baseName);

    // A helper to take action upon monitoring problems. Not all
    // applications may take these equally seriously, depending on
    // their core business (e.g., monitoring vs. control).
    virtual void handleMonitoringProblem(std::string const &problemDesc);

    /* // A little helper method for HTML action handling. */
    /* void redirect(xgi::Input* in, xgi::Output* out); */

    // Several methods for SOAP messaging back and forth.
    xoap::MessageReference executeSOAPCommand(xoap::MessageReference &cmd,
                                              xdaq::ApplicationDescriptor const &dest) const;
    xoap::MessageReference sendSOAP(xoap::MessageReference &msg,
                                    xdaq::ApplicationDescriptor const &dest) const;
    xoap::MessageReference postSOAP(xoap::MessageReference &msg,
                                    xdaq::ApplicationDescriptor const &destination) const;

    virtual void setupInfoSpaces();

    virtual void hwConnectImpl() = 0;
    virtual void hwReleaseImpl() = 0;

    /**
       * Access the hardware pointer as DeviceBase&.
       */
    virtual pixel::hwlayer::DeviceBase &getHw() const;

    InfoSpaceUpdaterPlain appStateInfoSpaceUpdater_;
    ApplicationStateInfoSpaceHandler appStateInfoSpace_;
    /**
       * @note
       *
       * The following two variables should be instantiated by each
       * and every inheriting class in the constructor.
       * So even though they are pointers, we should be able to simply
       * use them everywhere outside the constructor.
       */
    std::unique_ptr<ConfigurationInfoSpaceHandler> cfgInfoSpaceP_;
    std::unique_ptr<pixel::hwlayer::DeviceBase> hwP_;

    log4cplus::Logger &logger_;
    Monitor monitor_;
    WebServer webServer_;

    // A string identifying the RunControl session owning the
    // hardware associated with this control application.
    pixel::utils::OwnerId hwLeaseOwnerId_;
    // And the previous one, in case it expired...
    pixel::utils::OwnerId expiredHwLeaseOwnerId_;

    // A flag to show that the application is in 'maintenance mode.'
    bool maintenanceMode_;

    // A 'stashed' version of the true lease owner(s) for use in
    // maintenance mode.
    pixel::utils::OwnerId hwLeaseOwnerIdPrev_;
    pixel::utils::OwnerId expiredHwLeaseOwnerIdPrev_;
    // And a little helper to keep track of lease manipulations
    // during the time spent in maintenance mode. (Ugly...)
    bool shouldHaveALeaseOwner_;

    // A lock to allow scheduling between the monitoring updates
    // (which potentially access the hardware) and other actions on
    // the hardware.
    Lock monitoringLock_;

    // TODO TODO TODO
    // Maybe all the hardware-level state transition methods should
    // be moved here too.
    virtual pixel::hwlayer::DeviceBase::RegContentsVec hwReadHardwareConfiguration() const;
    // TODO TODO TODO end

    virtual bool isRegisterAllowed(pixel::hwlayer::RegisterInfo const &regInfo) const;

    // Several methods involved in loading parameters from an FSM
    // SOAP command message into the ConfigurationInfoSpace.
    std::vector<FSMSOAPParHelper> expectedFSMSoapPars(std::string const &commandName) const;
    virtual std::vector<FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const &commandName) const;
    void loadSOAPCommandParameters(xoap::MessageReference const &msg);
    virtual void loadSOAPCommandParametersImpl(xoap::MessageReference const &msg);
    void loadSOAPCommandParameter(xoap::MessageReference const &msg,
                                  FSMSOAPParHelper const &param);
    virtual void loadSOAPCommandParameterImpl(xoap::MessageReference const &msg,
                                              FSMSOAPParHelper const &param);
    log4cplus::SharedAppenderPtr appender;

  private:
// This method renews the hardware lease from a SOAP command.
#ifndef XDAQ_TARGET_XDAQ15
    xoap::MessageReference renewHwLease(xoap::MessageReference msg);
#else
    xoap::MessageReference renewHwLease(xoap::MessageReference msg);
#endif

// Several methods to handle entering and releasing of
// 'maintenance mode.'
#ifndef XDAQ_TARGET_XDAQ15
    xoap::MessageReference maintenanceModeOn(xoap::MessageReference msg);
    xoap::MessageReference maintenanceModeOff(xoap::MessageReference msg);
#else
    xoap::MessageReference maintenanceModeOn(xoap::MessageReference msg);
    xoap::MessageReference maintenanceModeOff(xoap::MessageReference msg);
#endif

    void switchMaintenanceMode(bool const on, pixel::utils::OwnerId const &maintainerId);
    void setMaintenanceMode(bool const on);

    virtual std::string readHardwareConfigurationImpl();
    virtual std::string readHardwareStateImpl();

    // This method is used to auto-expire the hardware lease after a
    // given time.
    void timeExpired(toolbox::task::TimerEvent &event);


    /**
       * A filtering method to select which part of the hardware
       * configuration is exposed to the outside world via
       * hwReadHardwareConfiguration().
       */
    pixel::hwlayer::RegisterInfo::RegInfoVec filterRegInfo(pixel::hwlayer::RegisterInfo::RegInfoVec const &regInfosIn) const;
    
    // The auto-expiry timer for the hardware lease.
    std::string timerName_;
    std::string const timerJobName_;
    toolbox::task::Timer *hwLeaseExpiryTimerP_;

    // The name of the XDAQ InfoSpace that alarms live in, and where
    // the Sentinel probe picks them up.
    std::string const sentinelInfoSpaceName_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_XDAQAppBase_h_
