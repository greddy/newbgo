#ifndef _pixel_utils_AppHist_h_
#define _pixel_utils_AppHist_h_

#include <deque>
#include <string>

#include "pixel/utils/AppHistItem.h"

namespace pixel {
namespace utils {

class AppHist {

  public:
    AppHist();
    ~AppHist();

    void addItem(std::string const &msg);

    std::string getJSONString() const;

  private:
    static unsigned const kMaxHistSize = 1000;
    std::deque<pixel::utils::AppHistItem> history_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_AppHistItem_h_
