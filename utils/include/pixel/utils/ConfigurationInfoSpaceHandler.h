#ifndef _pixel_utils_ConfigurationInfoSpaceHandler_h_
#define _pixel_utils_ConfigurationInfoSpaceHandler_h_

#include <string>

// OBSOLETE OBSOLETE OBSOLETE
/* #include "xdata/ActionListener.h" */
// OBSOLETE OBSOLETE OBSOLETE end

#include "pixel/utils/InfoSpaceHandler.h"

namespace xdaq {
class Application;
}

// OBSOLETE OBSOLETE OBSOLETE
/* namespace xdata { */
/*   class Event; */
/* } */
// OBSOLETE OBSOLETE OBSOLETE end

namespace pixel {
namespace utils {

class Monitor;
class WebServer;

/**
     * Abstract base class handling the XDAQ application's
     * configuration items.
     *
     * @note
     * The items in this InfoSpace are not updated by an
     * InfoSpaceUpdater, but by the usual XDAQ configuration methods.
     */
class ConfigurationInfoSpaceHandler : public pixel::utils::InfoSpaceHandler
                                      // OBSOLETE OBSOLETE OBSOLETE
                                      //      public xdata::ActionListener
                                      // OBSOLETE OBSOLETE OBSOLETE end
                                      {

  public:
    virtual ~ConfigurationInfoSpaceHandler();

  protected:
    ConfigurationInfoSpaceHandler(xdaq::Application &xdaqApp,
                                  std::string const &name = "pixel-application-config");

    // OBSOLETE OBSOLETE OBSOLETE
    /* virtual void actionPerformed(xdata::Event& event); */
    // OBSOLETE OBSOLETE OBSOLETE end

    /**
       * Register itemsets in this InfoSpace with a monitor
       * object. Obviously needs to be implemented by the derived
       * class itself.
       */
    virtual void registerItemSetsWithMonitor(Monitor &monitor);

    /**
       * Register itemsets in this InfoSpace with a webserver
       * object. Obviously needs to be implemented by the derived
       * class itself.
       */
    virtual void registerItemSetsWithWebServer(WebServer &webServer,
                                               Monitor &monitor,
                                               std::string const &forceTabName = "");
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_ConfigurationInfoSpaceHandler_h_
