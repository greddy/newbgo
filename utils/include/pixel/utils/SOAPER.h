/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2019, CERN.                                             *
 * All rights reserved.                                                  *
 * Authors: M. Alguacil, A. Datta and K. Padeken                         *
 *************************************************************************/

#ifndef _pixel_utils_SOAPER_h
#define _pixel_utils_SOAPER_h

#include <stdint.h>
#include <string>
#include <vector>
#include <map>
#include <deque>
#include <sys/time.h>
#include <boost/thread.hpp>

#include "xdaq/Application.h"

#include "xoap/MessageReference.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPName.h"
#include "xoap/MessageFactory.h"
#include "xoap/Method.h"

#include "xcept/tools.h"

#include "pixel/utils/OwnerId.h"
#include "pixel/utils/LogMacros.h"


typedef std::map<std::string, std::string> Attributes;
typedef std::map<std::string, std::vector<std::string> > Variables;
typedef std::vector< std::pair<std::string, std::string> > VariablesPairs;
typedef std::map<std::string, std::map<std::string, std::vector<std::string> > > VariablesWithType;


namespace pixel {
namespace utils {

// NOTE: The 'xdaq' namespace is determined by the fact that
// This is the namespace used by RunControl.

// Enum definitions for SOAP fault-code types. Defined in SOAP v1.2-speak:
// - SOAPFaultSender for v1.1 'Client' and v1.2 'Sender',
// - SOAPFaultReceiver for v1.1 'Server' and v1.2 'Receiver'.
enum SOAPFaultCodeType {
    SOAPFaultCodeSender,
    SOAPFaultCodeReceiver
};

class SOAPER{
    public:
        /** Parameterized Constructor
         *
         *  \param app - XDAQ application.
         *  \param history_size - message history size, i.e., the maximum number
         * of messages you want the SOAP message history to hold.
         */
        SOAPER(xdaq::Application *app, size_t const history_size=15);

        /** Copy Constructor
         *
         *  \param aSOAPER - SOAPER object.
         */
        SOAPER(const SOAPER &aSOAPER);

        /** Gets message history.
         *
         *  \return A queue with the message history.
         */
        inline std::deque<std::string> getHistory() const{
            return history_;
        }

        /** Gets last 'n' messages.
         *
         *  \param n - number of messages to get.
         *
         *  \return A queue with the last 'n' messages and the associated timestamp
         * if n is less than the size of message history. Otherwise, return
         * whole message history.
         */
        std::deque<std::string> getLastMessages(size_t const n) const;

        /** Gets last message.
         *
         *  \return A pair with the last message and the associated timestamp.
         */
        inline std::string getLastMessage() const{
            return history_.back();
        }

        /** Sets a new message.
         *
         *  Sets the new SOAP message by adding the timestamp with the following
         * format:
         *                    dd Mmm yyyy hh:mm:ss.uuu
         * where Mmm is the month (in letters), dd the day of the month,
         * yyyy the year, hh:mm:ss the time and uuu the microseconds.
         *
         *  \param msg - SOAP message.
         *  \param received - true if it is a received message, false if it is
         * a sent messaged
         *
         *  \return true if the message could be inserted, false in
         * another case.
         */
        virtual bool setMessage(xoap::MessageReference const &msg, bool received);

        /** Number of elements in the SOAP message history
         *
         * \return Number of elements in the history.
         */
        inline size_t size() const{
            return history_.size();
        }
        /** Maximum number of elements in the SOAP message history
         *
         * \return Maximum number of elements in the history.
         */
        inline size_t maxSize() const{
            return history_size_;
        }

        /** Extracts the timestamp associated with the SOAP message.
         *
         *  \param i - position of the SOAP message in the history.
         *
         *  \return The timestamp of the SOAP message in position i.
         */
       std::string getTimestamp(size_t const i=-1);

       /** Extracts the body content from the SOAP message.
         *
         *  \param i - position of the SOAP message in the history.
         *
         *  \return The body content from the SOAP message in position i.
         */
       std::string getBody(size_t const i=-1);

       /** Returns whether the SOAP message was received (true) or
         * sent (false).
         *
         * \param i - position of the SOAP message in the history.
         *
         * \return true if the message was received, false if it was sent
         *
         */
        bool received(size_t const i=-1);

        /** Returns a SOAP message with a clearer format.
         *
         *  \param msg - SOAP message.
         *
         *  \return A string with tabs and line breaks:
         * <env:Envelope ....>
         *      <env:Header/>
         *      <env:Body>
         *          <xdaq:PIAReset 'attribute 1' 'attribute 2' .... 'attribute n'>
         *              <'variable 1'></'end variable 1'>
         *              <'variable 2'></'end variable 2'>
         *              ................................
         *              <'variable m'></'end variable m'>
         *          </xdaq:PIAReset>
         *      </env:Body>
         * </env:Envelope>
         */
        std::string getFormattedSOAP(std::string const &msg);

        /** Writes the last 'n' messages to the given output stream or all
         * SOAP messages if 'n' is not in the range [0,m) where m is the
         * size of the history.
         *
         * \param ostream - the output stream to which the SOAP messages will
         * be written.
         * \param n - number of messages to write. By default, all messages.
         */
        // The 'n' parameter is defined with a value of -1, which because size_t
        // is an unsigned integral type, it is the largest possible representable
        // value for this type.
        void writeTo(std::ostream &stream, size_t const n=-1);

        /** Writes the last 'n' messages to the given file or all SOAP messages
         * if 'n' is not in the range [0,m) where m is the size of the history.
         *
         * \param filepath - a string indicating the path to the file where the
         * message history will be written.
         * \param n - number of messages to write. By default, all messages.
         */
        void writeTo(std::string const &filepath="output.xml", size_t const n=-1);


        std::string extractSOAPCommandName(xoap::MessageReference const &msg);
        std::string extractSOAPCommandAttribute(xoap::MessageReference const &msg, std::string attribute);
        std::string extractSOAPCommandRCMSURL(xoap::MessageReference const &msg);
        uint32_t extractSOAPCommandRCMSSessionId(xoap::MessageReference const &msg);
        std::string extractSOAPCommandRequestorId(xoap::MessageReference const &msg);
        pixel::utils::OwnerId extractSOAPCommandOwnerId(xoap::MessageReference const &msg);

        /** Extract all child nodes of the body element of a message.
         *
         *  - extractBodyNode() for when only a single node is allowed.
         *
         *  \param msg - SOAP message.
         *
         *  \return The child node of the body element of a message.
         */
        xoap::SOAPElement extractBodyNode(xoap::MessageReference const &msg);
        /** Extract all child nodes of the body element of a message.
         *
         *  - extractBodyNodes() for when more than one node is allowed.
         *
         *  \param msg - SOAP message.
         *
         *  \return All child nodes of the body element of a message.
         */
        std::vector<xoap::SOAPElement> extractBodyNodes(xoap::MessageReference const &msg);

        /** Starting from a given node, extract its child node with the
         *  specified name. Only one such child node is allowed.
         *
         */
        xoap::SOAPElement extractChildNode(xoap::SOAPElement &node,
                                           xoap::SOAPName &childName);

        bool hasFault(xoap::MessageReference const &msg) const;
        bool hasFaultDetail(xoap::MessageReference const &msg) const;
        std::string extractFaultString(xoap::MessageReference const &msg) const;
        std::string extractFaultDetail(xoap::MessageReference const &msg) const;

        bool hasSOAPCommandAttribute(xoap::MessageReference const &msg,
                             std::string const &attributeName);
        bool hasSOAPCommandParameter(xoap::MessageReference const &msg,
                                     std::string const &parameterName);
        bool extractSOAPCommandParameterBool(xoap::MessageReference const &msg,
                                             std::string const &parameterName);
        std::string extractSOAPCommandParameterString(xoap::MessageReference const &msg,
                                                      std::string const &parameterName);
        uint32_t extractSOAPCommandParameterUnsignedInteger(xoap::MessageReference const &msg,
                                                            std::string const &parameterName);

        xoap::MessageReference makePSXDPGetSOAPCmd(std::string const &dpName);

        std::string extractPSXDPGetReply(xoap::MessageReference &msg,
                                         std::string const &dpName);

        xoap::SOAPElement extractSOAPCommandParameterElement(xoap::MessageReference const &msg,
                                                             std::string const &parameterName);

        xoap::MessageReference makeParameterGetSOAPCmd(std::string const &className,
                                                       std::string const &parName,
                                                       std::string const &parType);

        // The following three methods prepare SOAP replies to SOAP
        // messages. The protocol version of the reply is based on the
        // protocol version of the original message.
        xoap::MessageReference makeSOAPFaultReply(xoap::MessageReference const &msg,
                                                  SOAPFaultCodeType const faultCode,
                                                  std::string const &faultString,
                                                  std::string const &faultDetail = "");
        xoap::MessageReference makeCommandSOAPReply(xoap::MessageReference const &msg,
                                                    std::string const &fsmState = "");
        xoap::MessageReference makeCommandSOAPReply(xoap::MessageReference const &msg,
                                                    std::string const &replyMsg,
                                                    std::string const &fsmState);

        // The following four methods prepare SOAP replies to SOAP
        // messages. These are used internally by the methods above.
        xoap::MessageReference makeSOAPFaultReply(std::string const &soapProtocolVersion,
                                                  SOAPFaultCodeType const faultCode,
                                                  std::string const &faultString,
                                                  std::string const &faultDetail);
        xoap::MessageReference makeCommandSOAPReply(std::string const &soapProtocolVersion,
                                                    std::string const &command,
                                                    std::string const &fsmState);
        xoap::MessageReference makeCommandSOAPReply(std::string const &soapProtocolVersion,
                                                    std::string const &command,
                                                    std::string const &replyMsg,
                                                    std::string const &fsmState);

        template <typename T>
        void addSOAPReplyParameter(xoap::MessageReference const &msg,
                                   std::string const &parameterName,
                                   T const &val);


        /** Extract the variable names and the associated values.
         *
         * \param msg - SOAP message.
         *
         * \return A map containing the variable names and the values associated with
         * each one.
         */
        Variables getSOAPCommandVariables(xoap::MessageReference const &msg);


        /** Extracts the attributes from a SOAP message.
         *
         *  Gets all values and checks if the parameters indicated in the second
         * argument are presents in the list of incoming parameters.
         *
         *  \param msg - SOAP message.
         *  \param parametersNames - a vector containing the mandatory attribute names.
         *
         *  \return A map containing the attribute names and the value associated with
         * each one.
         */
        Attributes getAllSOAPCommandAttributes(xoap::MessageReference const &msg,
                                               std::vector<std::string> const &parameterNames = std::vector<std::string>());

        /** Extracts the values of the indicated attributes from a SOAP message.
         *
         *  Gets the values associated with the names indicated in the second argument
         * from a SOAP message.
         *
         *  \param msg - SOAP message.
         *  \param parametersNames - a vector containing the mandatory attribute names.
         *  \param optional - boolean value indicating whether the specified attributes
         * are optional or not.
         *
         *  \return A map containing the attribute names and the value associated with
         * each one.
         */
        Attributes getSomeSOAPCommandAttributes(xoap::MessageReference const &msg,
                                                std::vector<std::string> const &parameterNames);

        /** Creates an attribute with the given name and value and adds it to
         * the SOAP message.
         *
         *  \param msg - SOAP message.
         *  \param attributeName - a string with the name of the attribute.
         *  \param attributeValue - a string giving the value of the attribute.
         */
        void addSOAPCommandAttribute(xoap::MessageReference const &msg,
                                     std::string const &attributeName,
                                     std::string const &attributeValue);
        /** Creates some attributes with the given names and values and adds them to
         * the SOAP message.
         *
         *  \param msg - SOAP message.
         *  \param attributes - a two-strings map giving the name and the associated
         * value of the attributes.
         */
        void addSOAPCommandAttributes(xoap::MessageReference const &msg,
                                      Attributes const &attributes);


        /** Creates a variable with the given name and value and adds it to
         * the SOAP message.
         *
         *  \param msg - SOAP message.
         *  \param variableName - a string with the name of the variable.
         *  \param variableValue - a string giving the value of the variable.
         *  \param type - a string with the data type of the variable.
         */
        void addSOAPCommandVariable(xoap::MessageReference const &msg,
                                    std::string const &variableName,
                                    std::string const &variableValue,
                                    std::string const &type);

        /** Creates some variables with the given names and values and adds them to
         * the SOAP message.
         *
         *  \param msg - SOAP message.
         *  \param vars - a map of strings giving the name and the associated values
         * of the variables:
         *                  map[name, vector[value]]
         *  \param alterVars
         */
        void addSOAPCommandVariables(xoap::MessageReference const &msg,
                                     Variables const &vars,
                                     std::vector<std::string> const &alterVars);
        /** Creates some variables with the given names and values and adds them to
         * the SOAP message.
         *
         *  \param msg - SOAP message.
         *  \param vars - a map of strings giving the name and the associated values
         * of the variables:
         *                  map[name, vector[value]]
         */
        void addSOAPCommandVariables(xoap::MessageReference const &msg,
                                     Variables const &vars);
        /** Creates some variables with the given names and values and adds them to
         * the SOAP message.
         *
         *  \param msg - SOAP message.
         *  \param vars - a map of strings giving the name and the associated values
         * and the types of the variables:
         *                  map[type, map[name, vector[value]]]
         */
        void addSOAPCommandVariables(xoap::MessageReference const &msg,
                                     VariablesWithType const &vars);
        /** Creates some variables with the given names and values and adds them to
         * the SOAP message.
         *
         *  \param msg - SOAP message.
         *  \param vars - a map of strings giving the name and the associated values
         * and the types of the variables:
         *                  map[type, map[name, vector[value]]]
         */
        void addSOAPCommandVariables(xoap::MessageReference const &msg,
                                     VariablesWithType const &vars,
                                     std::vector<std::string> const &alterVars);

        /** Creates a new SOAP message with the given command and attributes.
         *
         *  \param cmd - a string with the command.
         *  \param attributes - a two-strings map giving the name and the associated
         * value of the attributes.
         *
         *  \return The new SOAP message created with the given command and attributes.
         */
        xoap::MessageReference makeSOAPMessageReference(std::string const &cmd,
                                                        Attributes const &attributes = Attributes());
        /** Creates a new SOAP message with the given command, attributes and
         * variables.
         *
         *  \param cmd - a string with the command.
         *  \param attributes - a two-strings map giving the name and the associated
         * value of the attributes.
         *  \param vars - a map of strings giving the name and the associated values
         * and, optionally, the types of the variables:
         *                      map[name, vector[value]]
         * or
         *                  map[type, map[name, vector[value]]]
         *
         *  \return The new SOAP message created with the given command, attributes
         * and variables.
         */
        template <typename U>
        xoap::MessageReference makeSOAPMessageReference(std::string const &cmd,
                                                        Attributes const &attributes,
                                                        U const &vars,
                                                        std::vector<std::string> const &alterVars=std::vector<std::string>());
        /** Creates a new SOAP message with the command, the information
         * given in the file.
         *
         *  \param cmd - a string with the command.
         *  \param filepath - a string indicating the path to the file where the SOAP
         * message information is located.
         *
         *  \return The new SOAP message created with the command and the information
         * given in the file.
         */
        xoap::MessageReference makeSOAPMessageReference(std::string const &cmd,
                                                        std::string const &filepath);

        /** Creates a new SOAP message with the given command and attributes,
         * and sends a SOAP response message to the specified target.
         *
         *  \param d - a an ApplicationDescriptor to which the response will be sent.
         *  \param cmd - a string with the command.
         *  \param attributes - a two-strings map giving the name and the associated
         * value of the attributes.
         *
         *  \return The command name of the SOAP response message.
         */
        ////template <typename U = map< string, vector<string> > >
        std::string send(const xdaq::ApplicationDescriptor *d,
                         std::string const &cmd,
                         Attributes const &attrib = Attributes())
                         ////U vars = U() )
                         ;
        /** Creates a new SOAP message with the given command, attributes and variables,
         * and sends a SOAP response message to the specified target.
         *
         *  \param d - a an ApplicationDescriptor to which the response will be sent.
         *  \param cmd - a string with the command.
         *  \param attributes - a two-strings map giving the name and the associated
         * value of the attributes.
         *  \param vars - a map of strings giving the name and the associated values
         * and, optionally, the types of the variables:
         *                      map[name, vector[value]]
         * or
         *                  map[type, map[name, vector[value]]]
         *
         *  \return The command name of the SOAP response message.
         */
        template <typename U>
        std::string send(const xdaq::ApplicationDescriptor *d,
                         std::string const &cmd,
                         Attributes const &attrib,
                         U const &vars,
                         std::vector<std::string> const &alterVars=std::vector<std::string>());
        /** Sends a SOAP response message to the specified target.
         *
         *  \param d - a an ApplicationDescriptor to which the response will be sent.
         *  \param msg - a SOAP message.
         *
         *  \return The SOAP response message.
         */
        xoap::MessageReference sendWithSOAPReply(const xdaq::ApplicationDescriptor *d,
                                                 xoap::MessageReference msg)
                                                 ;
        /** Creates a new SOAP message with the given command and attributes,
         * and sends a SOAP response message to the specified target.
         *
         *  \param d - a an ApplicationDescriptor to which the response will be sent.
         *  \param cmd - a string with the command.
         *  \param attributes - a two-strings map giving the name and the associated
         * value of the attributes.
         *
         *  \return The SOAP response message.
         */
        ////template <typename U = map< string, vector<string> > >
        xoap::MessageReference sendWithSOAPReply(const xdaq::ApplicationDescriptor *d,
                                                 std::string const &cmd,
                                                 Attributes const &attrib = Attributes())
                                                 ;
        /** Creates a new SOAP message with the given command and attributes,
         * and sends a SOAP response message to the specified target.
         *
         *  \param d - a an ApplicationDescriptor to which the response will be sent.
         *  \param cmd - a string with the command.
         *  \param attributes - a two-strings map giving the name and the associated
         * value of the attributes.
         *  \param vars - a map of strings giving the name and the associated values
         * and, optionally, the types of the variables:
         *                      map[name, vector[value]]
         * or
         *                  map[type, map[name, vector[value]]]
         *
         *  \return The SOAP response message.
         */
        template <typename U>
        xoap::MessageReference sendWithSOAPReply(const xdaq::ApplicationDescriptor *d,
                                                 std::string const &cmd,
                                                 Attributes const &attrib,
                                                 U const &vars,
                                                 std::vector<std::string> const &alterVars=std::vector<std::string>());
        /** Sends a status notification with the specified message, accompanied by the second and
         * microseconds in which it is sent to the specified ApplicationDescriptor.
         *
         *  \param d - ApplicationDescriptor to which the status notification will be sent.
         *  \param msg - description of the status notificacion.
         *
         *  \return The command name of the SOAP response message.
         */
         std::string sendStatus(const xdaq::ApplicationDescriptor *d,
                                std::string const &msg);

        /** Sends a SOAP message and checks if reply ok.
         *
         *  \param s - iterator of a map with the ApplicationDescriptor, and its associated
         * representative integer, to which the message will be sent.
         *  \param message - description of the message.
         *  \param replynormal - expected reply.
         *  \param ps - place where the reply will be stored in the associated
         * future.
         *  \param parameters (optional) - a two-strings map giving the name and
         * the associated value of the attributes.
         */
        void sendSOAPParallel(std::map<xdata::UnsignedIntegerT, const xdaq::ApplicationDescriptor *>::const_iterator &s,
                              std::string const &message, std::string const &replynormal,
                              boost::promise<std::string> &ps,
                              Attributes const &parameters = Attributes());

        /** Sends a common message to the specified ApplicationDescriptors and waits for
         * replies from each one, checking if they are ok, to complete its work.
         *
         *  \param SuperV - a map with the ApplicationDescriptor, and its associated
         * representative integer, to which the message will be sent.
         *  \param message - description of the message.
         *  \param replynormal - expected reply.
         *  \param parameters (optional) - a two-strings map giving the name and
         * the associated value of the attributes.
         */
        void sendSOAPBlock(std::map<xdata::UnsignedIntegerT, const xdaq::ApplicationDescriptor *> const &SuperV,
                                  std::string const &message, std::string const &replynormal,
                                  Attributes const &parameters = Attributes());

    protected:
        xdaq::Application *app_; ///< XDAQ application

    private:
        size_t history_size_;             ///< Message history size
        std::deque<std::string> history_; ///< Message history (Timestamp + SOAP message)
        log4cplus::Logger &sv_logger_;    ///< Logging information

        /** Starts concurrent execution of sendSOAPParallel, which sends a SOAP
         * message and checks if reply ok.
         *
         *  \param s - iterator of a map with the ApplicationDescriptor, and its associated
         * representative integer, to which the message will be sent.
         *  \param message - description of the message.
         *  \param replynormal - expected reply.
         *  \param ps - place where the reply will be stored in the associated
         * future.
         *  \param parameters (optional) - a two-strings map giving the name and
         * the associated value of the attributes.
         *
         *  \return Pointer to the thread.
         */
        boost::thread* makeSOAPThread(std::map<xdata::UnsignedIntegerT, const xdaq::ApplicationDescriptor *>::const_iterator &s,
                                      std::string const &message, std::string const &replynormal,
                                      Attributes const &parameters = Attributes());

        /** Export the data type into an existing DOM document.
         *
         */
        void exportAll(bool const &val,
                       xoap::SOAPElement element);
        void exportAll(std::string const &val,
                       xoap::SOAPElement element);
        void exportAll(uint32_t const &val,
                       xoap::SOAPElement element);
};

} // namespace utils
} // namespace pixel



#endif // _pixel_utils_SOAPER_h
