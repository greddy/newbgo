#ifndef _pixel_utils_InfoSpaceHandlerLocker_h_
#define _pixel_utils_InfoSpaceHandlerLocker_h_

#include "xdata/InfoSpace.h"

#include "pixel/utils/InfoSpaceHandler.h"

namespace pixel {
namespace utils {

class InfoSpaceHandlerLocker {

  public:
    InfoSpaceHandlerLocker(pixel::utils::InfoSpaceHandler const *const infoSpaceHandler);
    ~InfoSpaceHandlerLocker();

  private:
    void lock() const;
    void unlock() const;

    pixel::utils::InfoSpaceHandler const *const infoSpaceHandler_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_InfoSpaceHandlerLocker_h_
