#ifndef _pixel_utils_Colors_h_
#define _pixel_utils_Colors_h_

/** 
 * Colors to differentiate messages.
 */

#define black 			"\033[22;30m"
#define red 			"\033[22;31m"
#define green 			"\033[22;32m"
#define brown 			"\033[22;33m"
#define blue 			"\033[22;34m"
#define magenta 		"\033[22;35m"
#define cyan 			"\033[22;36m"
#define gray 			"\033[22;37m"
#define dark_gray 		"\033[01;30m"
#define light_red 		"\033[01;31m"
#define light_green 	"\033[01;32m"
#define yellow 			"\033[01;33m"
#define light_blue 		"\033[01;34m"
#define light_magenta 	"\033[01;35m"
#define light_cyan 		"\033[01;36m"
#define white 			"\033[01;37m"

#define creset 			"\033[0m"

#endif
