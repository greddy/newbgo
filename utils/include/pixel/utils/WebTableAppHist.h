#ifndef _pixel_utils_WebTableAppHist_h
#define _pixel_utils_WebTableAppHist_h

#include <cstddef>
#include <string>

#include "pixel/utils/WebObject.h"

namespace pixel {
namespace utils {
class Monitor;
}
}

namespace pixel {
namespace utils {

class WebTableAppHist : public pixel::utils::WebObject {

  public:
    WebTableAppHist(std::string const &name,
                    std::string const &description,
                    pixel::utils::Monitor const &monitor,
                    std::string const &itemSetName,
                    std::string const &tabName,
                    size_t const colSpan);

    std::string getHTMLString() const;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_WebTableAppHist_h
