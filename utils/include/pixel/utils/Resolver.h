#ifndef _pixel_utils_Resolver_h_
#define _pixel_utils_Resolver_h_

#include <netdb.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <vector>

namespace pixel {
namespace utils {

class Resolver {

  public:
    Resolver();
    ~Resolver();

    std::vector<struct addrinfo> resolve(std::string const &node,
                                         unsigned int const portNumber);
    std::vector<struct addrinfo> resolve(std::string const &node,
                                         std::string const &service = "");

  private:
    void reset();

    struct addrinfo *nodeInfo_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_Utils_h_
