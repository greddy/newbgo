#ifndef _TIMER_H_
#define _TIMER_H_
#include <math.h>
#include <sys/time.h>
#include <iostream>
#include <string>


namespace log4cplus {
class Logger;
}

namespace pixel {
namespace utils {
  
class Timer {

  public:
    Timer();
    Timer(log4cplus::Logger *sv_logger_);
    virtual ~Timer();

    void start(const std::string msg = "");
    void stop(const std::string msg = "");
    unsigned int ntimes() const;
    double elapsedtime();
    double tottime() const;
    double avgtime() const;
    double rms() const;

    std::string printStats() const;

    bool started() {
        return startflag_;
    }
    void setVerbose(bool verbosity) {
        verbose_ = verbosity;
    }
    void setName(std::string name) {
        name_ = name;
    }
    std::string printTime(const std::string msg = "");

    void restart();
    void reset();

  private:
    log4cplus::Logger *sv_logger_;
    unsigned int ntimes_;
    double ttot_;
    double ttotsq_;

    timeval tstart_;

    bool startflag_;
    bool verbose_;
    std::string name_;
    void printTime(const timeval t, const std::string msg = "") const;
};

}//pixel namespace
}//utils namespace
#endif
