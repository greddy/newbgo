#ifndef _pixel_SOAPMsgHistoryHandler_h_
#define _pixel_SOAPMsgHistoryHandler_h_

#include <string>

#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/SOAPER.h"

namespace xdaq {
    class Application;
}

namespace pixel {
    namespace utils {
        class InfoSpaceUpdater;
        class Monitor;
        class WebServer;
    }
}

namespace pixel {
namespace utils {
    class SOAPMsgHistoryHandler : public pixel::utils::InfoSpaceHandler {
        public:
            SOAPMsgHistoryHandler(xdaq::Application& xdaqApp,
                                       pixel::utils::InfoSpaceUpdater* updater,
                                       pixel::utils::SOAPER* soaperTKFEC,
                                       std::string const&id);
            virtual ~SOAPMsgHistoryHandler();
            void displaySOAPMessage(pixel::utils::Monitor& monitor);

        protected:
            virtual void registerItemSetsWithMonitor(pixel::utils::Monitor& monitor);
            virtual void registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
                                                       pixel::utils::Monitor& monitor,
                                                       std::string const& forceTabName="");

        private:
            pixel::utils::SOAPER * soaper_;
            std::string id_;
            std::vector<std::string> itemSetNameVec;

            /** Extracts the content from the SOAP message body and makes the
              * appropriate changes to display it on the web page, adding color
              * to it as well.
              *
              *  \param i - position of the SOAP message in the history.
              *
              *  \return The body content from the SOAP message in position i
              *  HTML.
              */
           std::string syntaxHighlightBodyHTML(size_t const i=-1);
    };
} // namespace utils
} // namespace pixel

#endif // _pixel_TKFECSOAPMsgHistoryHandler_h_
