#ifndef _pixel_utils_Monitor_h_
#define _pixel_utils_Monitor_h_

#include <string>
#include <sys/types.h>
#include <tr1/unordered_map>
#include <utility>
#include <vector>

#include "toolbox/exception/Listener.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/TimeVal.h"

#include "pixel/utils/MonitorItem.h"
#include "pixel/utils/PIXELObject.h"
#include "pixel/utils/Uncopyable.h"

namespace log4cplus {
class Logger;
}

namespace toolbox {
namespace task {
class Timer;
class TimerEvent;
}
}

namespace xcept {
class Exception;
}

namespace pixel {
namespace utils {

class InfoSpaceHandler;
class XDAQAppBase;

class Monitor : public toolbox::task::TimerListener,
                public toolbox::exception::Listener,
                public PIXELObject,
                private Uncopyable
                /**
     * Driving class behind all monitoring going on in the PIXEL XDAQ
     * framework. Access from 'outside' is via the WebServer
     * class. This class is uncopyable since it holds all kinds of
     * pointers to InfoSpaces.
     */
                {

  public:
    Monitor(pixel::utils::XDAQAppBase &xdaqApp, std::string const &name = "");
    ~Monitor();

    void startMonitoring();
    /* void stopMonitoring(); */

    typedef std::vector<std::pair<std::string, std::string> > StringPairVector;
    StringPairVector getFormattedItemSet(std::string const &itemSetName) const;
    StringPairVector getItemSetDocStrings(std::string const &itemSetName) const;

    typedef std::vector<std::tuple<std::string, std::string, std::string> > StringTupleVector;
    StringTupleVector getNameFormattedItemSet(std::string const &itemSetName) const;

    /**
       * Add the item with itemName to the itemset with itemSetName.
       */
    void addItem(std::string const &itemSetName,
                 std::string const &itemName,
                 std::string const &itemDesc,
                 InfoSpaceHandler *const infoSpaceHandler,
                 std::string const &docString = "");

    /**
         Create a new itemset with the name given.
       */
    void newItemSet(std::string const &name);

    // The toolbox::exception::Listener callback.
    virtual void onException(xcept::Exception &err);

  protected:
    log4cplus::Logger &logger_;

    void addInfoSpace(InfoSpaceHandler *const infoSpace);

    void timeExpired(toolbox::task::TimerEvent &event);

  private:
    static const useconds_t kLoopRelaxTime = 100;

    // This is the name of any monitoring alarms raised.
    std::string const alarmName_;
    // And this flag is used to keep track of existing alarm
    // conditions. Just to ease the book keeping, really.
    // NOTE: This is mutable because it 'hides' an internal
    // implementation detail. The Monitor does not change by the
    // presence/absence of the alarm.
    mutable bool isInAlarm_;

    std::tr1::unordered_map<std::string, InfoSpaceHandler *> infoSpaceMap_;
    typedef std::tr1::unordered_map<std::string,
                                    std::vector<std::pair<std::string, MonitorItem> > > MonitorItemMap;
    MonitorItemMap itemSets_;
    toolbox::task::Timer *timerP_;

    // The start time of the monitor. Just for a simplistic estimate
    // of the application uptime.
    toolbox::TimeVal timeStart_;

    /**
       * The main update method: crawls over all registered InfoSpaces
       * and updates all of them.
       */
    void updateAllInfoSpaces();

    void handleException(xcept::Exception &err);

    void raiseMonitoringAlarm(std::string const &problemDesc) const;
    void revokeMonitoringAlarm() const;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_Monitor_h_
