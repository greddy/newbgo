#ifndef _pixel_utils_version_h_
#define _pixel_utils_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define PIXELUTILS_VERSION_MAJOR 3
#define PIXELUTILS_VERSION_MINOR 13
#define PIXELUTILS_VERSION_PATCH 1

// If any previous versions available:
// #define PIXELUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef PIXELUTILS_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define PIXELUTILS_VERSION_CODE PACKAGE_VERSION_CODE(PIXELUTILS_VERSION_MAJOR, PIXELUTILS_VERSION_MINOR, PIXELUTILS_VERSION_PATCH)
#ifndef PIXELUTILS_PREVIOUS_VERSIONS
#define PIXELUTILS_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PIXELUTILS_VERSION_MAJOR, PIXELUTILS_VERSION_MINOR, PIXELUTILS_VERSION_PATCH)
#else
#define PIXELUTILS_FULL_VERSION_LIST PIXELUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PIXELUTILS_VERSION_MAJOR, PIXELUTILS_VERSION_MINOR, PIXELUTILS_VERSION_PATCH)
#endif

namespace pixelutils {

const std::string project = "pixel";
const std::string package = "pixelutils";
const std::string versions = PIXELUTILS_FULL_VERSION_LIST;
const std::string description = "CMS PIXEL helper software.";
const std::string authors = "Pixel DAQ team";
const std::string summary = "CMS PIXEL helper software.";
const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/PixelOnlineSoftware";
config::PackageInfo getPackageInfo();
void checkPackageDependencies();
std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace pixelutils

#endif
