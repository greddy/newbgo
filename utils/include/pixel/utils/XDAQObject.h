#ifndef _pixel_utils_XDAQObject_h_
#define _pixel_utils_XDAQObject_h_

namespace xdaq {
class Application;
}

namespace pixel {
namespace utils {

class XDAQObject {

  public:
    virtual ~XDAQObject();
    xdaq::Application &getOwnerApplication() const;

  protected:
    XDAQObject(xdaq::Application &owner);

  private:
    xdaq::Application &owner_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_XDAQObject_h_
