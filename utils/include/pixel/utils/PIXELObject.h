#ifndef _pixel_utils_PIXELObject_h_
#define _pixel_utils_PIXELObject_h_

/* #include "pixel/utils/XDAQObject.h" */
/* #include "pixel/utils/XDAQAppBase.h" */

namespace pixel {
namespace utils {

class XDAQAppBase;

class PIXELObject // : public pixel::utils::XDAQObject
    {

  public:
    virtual ~PIXELObject();

  protected:
    PIXELObject(pixel::utils::XDAQAppBase &owner);

    pixel::utils::XDAQAppBase &getOwnerApplication() const;

  private:
    pixel::utils::XDAQAppBase &owner_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_PIXELObject_h_
