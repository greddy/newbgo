template <class O, class I>
void
pixel::utils::WebServer::registerWebObject(std::string const& name,
                                          std::string const& description,
                                          Monitor const& monitor,
                                          I const& itemSetName,
                                          std::string const& tabName,
                                          size_t const colSpan)
{
  WebTab& tab = getTab(tabName);

  // NOTE: This is a bit tricky, but from here the WebTab takes
  // ownership of the created object. So the auto_ptr makes sense.
  std::unique_ptr<O> object = std::unique_ptr<O>(new O(name, description, monitor, itemSetName, tabName, colSpan));
  tab.addWebObject(std::unique_ptr<WebObject>(std::move(object)));
}
template <class O, class I>
void
pixel::utils::WebServer::registerWebObject(std::string const& name,
                                          std::string const& description,
                                          Monitor const& monitor,
                                          I const& itemSetName,
                                          std::string const& tabName,
                                          std::string const &id)
{
  WebTab& tab = getTab(tabName);

  // NOTE: This is a bit tricky, but from here the WebTab takes
  // ownership of the created object. So the auto_ptr makes sense.
  std::unique_ptr<O> object = std::unique_ptr<O>(new O(name, description, monitor, itemSetName, tabName, id));
  tab.addWebObject(std::unique_ptr<WebObject>(std::move(object)));
}
