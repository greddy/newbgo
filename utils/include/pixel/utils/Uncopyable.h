#ifndef _pixel_utils_Uncopyable_h
#define _pixel_utils_Uncopyable_h

namespace pixel {
namespace utils {

class Uncopyable
    /**
     * Just our own simple mix-in class to prevent copying.
     */
    {

  protected:
    Uncopyable() {};
    ~Uncopyable() {};

  private:
    Uncopyable(Uncopyable const &);
    Uncopyable &operator=(Uncopyable const &);
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_Uncopyable_h
