#ifndef _pixel_LOGMsgHistoryHandler_h_
#define _pixel_LOGMsgHistoryHandler_h_

#include <string>

#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/LoggerHistory.h"

namespace xdaq {
    class Application;
}

namespace pixel {
    namespace utils {
        class InfoSpaceUpdater;
        class Monitor;
        class WebServer;
    }
}

namespace log4cplus {
    class LoggerHistory;
}

namespace pixel {
namespace utils {
    class LOGMsgHistoryHandler : public pixel::utils::InfoSpaceHandler {
        public:
            LOGMsgHistoryHandler(xdaq::Application& xdaqApp,
                                       pixel::utils::InfoSpaceUpdater* updater,
                                       log4cplus::LoggerHistory* loggerHistory,
                                       std::string const&id);
            virtual ~LOGMsgHistoryHandler();
            void displayLOGMessage(pixel::utils::Monitor& monitor);

        protected:
            virtual void registerItemSetsWithMonitor(pixel::utils::Monitor& monitor);
            virtual void registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
                                                       pixel::utils::Monitor& monitor,
                                                       std::string const& forceTabName="");

        private:
            log4cplus::LoggerHistory * loggerHistory_;
            std::string id_;
            std::vector<std::string> itemSetNameVec;
    };
} // namespace utils
} // namespace pixel

#endif // _pixel_LOGMsgHistoryHandler_h_
