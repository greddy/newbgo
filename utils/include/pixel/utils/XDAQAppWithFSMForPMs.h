#ifndef _pixel_utils_XDAQAppWithFSMForPMs_h_
#define _pixel_utils_XDAQAppWithFSMForPMs_h_

#include <memory>

#include "pixel/utils/XDAQAppWithFSMPixelBase.h"

namespace xdaq {
class ApplicationStub;
}

namespace pixel {
namespace utils {

class XDAQAppWithFSMForPMs : public XDAQAppWithFSMPixelBase {

    // NOTE: All PIXEL control applications (at least the ones that
    // have a state machine) internally contain the same FSM
    // (implemented in XDAQAppWithFSMBase). The difference between
    // classes like XDAQAppWithFSMAutomatic, XDAQAppWithFSMBasic,
    // etc. lies in the fact that different transitions are exposed
    // to the outside world (i.e., bound to SOAP commands).

  public:
    virtual ~XDAQAppWithFSMForPMs();

  protected:
    XDAQAppWithFSMForPMs(xdaq::ApplicationStub *const stub,
                         std::unique_ptr<pixel::hwlayer::DeviceBase> hw);
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_XDAQAppWithFSMForPMs_h_
