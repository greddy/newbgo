
#ifndef LOG4CPLUS_LoggerHistory_HEADER_
#define LOG4CPLUS_LoggerHistory_HEADER_

#include "pixel/utils/LOGMsgHistoryHandler.h"
#include "pixel/utils/Monitor.h"

#include <log4cplus/config.hxx>
#include <string>
#include <vector>
#include <deque>
#include <utility>
#include <sys/time.h>


#if defined (LOG4CPLUS_HAVE_PRAGMA_ONCE)
#pragma once
#endif

#include <log4cplus/appender.h>

namespace pixel{
    namespace utils{
        class LOGMsgHistoryHandler;
    }
}

namespace log4cplus {

    /**
     * Appends log events to a file.
     */
    class LOG4CPLUS_EXPORT LoggerHistory : public Appender {
    public:
      // Ctors
        LoggerHistory();
        LoggerHistory(const log4cplus::helpers::Properties&);
        LoggerHistory(size_t const history_size=100);
        LoggerHistory(LoggerHistory* aLogger);

      // Dtor
        virtual ~LoggerHistory();

      // Methods
        virtual void close();

        void registerInfoSpaceHandler(pixel::utils::LOGMsgHistoryHandler* _handler, pixel::utils::Monitor* _monitor);

        /** Gets message history.
         *
         *  \return A queue with the message history.
         */
        std::vector<std::string> get_history();

        /** Number of elements in the LOG message history
         *
         * \return Number of elements in the history.
         */
        inline size_t size() const{
            return history_.size();
        };
        /** Maximum number of elements in the SOAP message history
         *
         * \return Maximum number of elements in the history.
         */
        inline size_t maxSize() const{
            return history_size_;
        };

        /** Extracts the timestamp associated with the SOAP message.
        *
        *  \param i - position of the LOG message in the history.
        *
        *  \return The timestamp of the LOG message in position i.
        */
        std::string getTimestamp(size_t const i=-1);

        /** Extracts the message type.
        *
        *  \param i - position of the LOG message in the history.
        *
        *  \return The type of the log message in position i.
        */

        std::string getType(size_t const i=-1);

        /** Extracts the log message.
        *
        *  \param i - position of the log message in the history.
        *
        *  \return The timestamp of the log message in position i.
        *
        *  if not set or -1 the last message is returned
        */
        std::string getlog(size_t const i=-1);

    protected:
        virtual void append(const log4cplus::spi::InternalLoggingEvent& event);
        //virtual void doAppend(const log4cplus::spi::InternalLoggingEvent& event);
        LogLevelManager& llmCache;

    private:
      // Disallow copying of instances of this class
        LoggerHistory(const LoggerHistory&);
        LoggerHistory& operator=(const LoggerHistory&);
        size_t history_size_;             ///< Message history size
        std::unique_ptr<pixel::utils::LOGMsgHistoryHandler> handler;
        std::unique_ptr<pixel::utils::Monitor> monitor;
        std::deque<std::pair<std::string, std::string> > history_; ///< Message history
        log4cplus::tstring _name;
        std::unique_ptr<ErrorHandler> _eh;
        std::unique_ptr<Layout> _layout;
    };

} // end namespace log4cplus

#endif // LOG4CPLUS_LoggerHistory_HEADER_

