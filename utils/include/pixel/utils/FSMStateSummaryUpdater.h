#ifndef _pixel_FSMStateSummaryUpdater_h_
#define _pixel_FSMStateSummaryUpdater_h_

#include "pixel/utils/XDAQAppBase.h"
#include "pixel/utils/InfoSpaceUpdater.h"
#include <iostream>

namespace pixel {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceUpdater;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace pixel {

    class FSMStateSummaryUpdater : public pixel::utils::InfoSpaceUpdater {

    public:
      FSMStateSummaryUpdater(pixel::utils::XDAQAppBase& xdaqApp);

      virtual bool updateInfoSpaceItem(pixel::utils::InfoSpaceItem& item,
                                       pixel::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      pixel::utils::XDAQAppBase& xdaqApp_;

    };
}

#endif // _pixel_FSMStateSummaryUpdater_h_
