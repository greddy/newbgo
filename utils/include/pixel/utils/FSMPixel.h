#ifndef _pixel_utils_FSMPixel_h_
#define _pixel_utils_FSMPixel_h_

#include <map>
#include <string>

#include "log4cplus/logger.h"

#include "toolbox/Event.h"
#include "toolbox/lang/Class.h"
#include "xdaq2rc/RcmsStateNotifier.h"
#include "xoap/MessageReference.h"

#include "pixel/utils/SOAPER.h"

namespace toolbox {
namespace fsm {
class AsynchronousFiniteStateMachine;
class FiniteStateMachine;
}
}

namespace xcept {
class Exception;
}

namespace pixel {
namespace utils {

class XDAQAppWithFSMPixelBase;

/**
     * PIXEL software state machine implementation. The state machines
     * used for the partition manager controllers differ slightly from
     * the state machines used elsewhere. This class implements the
     * most extensive version of the two. In the the
     * non-partition-manager controllers the same FSM class is
     * instantiated but not all state transitions are exposed to the
     * outside world (i.e., are not bound to anything).
     */
class FSMPixel : public toolbox::lang::Class, public pixel::utils::SOAPER {

  public:
    FSMPixel(XDAQAppWithFSMPixelBase *const xdaqAppP);
    virtual ~FSMPixel();

    // SOAP-based access to state transitions.
    xoap::MessageReference changeState(xoap::MessageReference msg);

    // Direct access to state transitions.
    void coldReset();
    void configure();
    void reconfigure();
    void enable();
    void start();
    void pause();
    void resume();
    void stop();
    void halt();
    void fail();
    void fixSoftError();

    std::string getCurrentStateName() const;

    void gotoFailed(std::string const &reason = "No further information available");
    void gotoFailed(xcept::Exception &err);
    std::set<std::string> listOfAllStates();
    std::set<std::string> listOfAllowedStates();


    bool setMessage(xoap::MessageReference const &msg, bool received);

  protected:
    void stateChangedWithNotification(toolbox::fsm::FiniteStateMachine &fsm);
    void stateChangedToFailedWithNotification(toolbox::fsm::FiniteStateMachine &fsm);

  private:
    toolbox::fsm::AsynchronousFiniteStateMachine *fsmP_;
    XDAQAppWithFSMPixelBase *xdaqAppP_;
    log4cplus::Logger logger_;
    xdaq2rc::RcmsStateNotifier rcmsNotifier_;

    std::map<std::string, std::string> automaticTransitions_;

    void invalidStateTransitionAction(toolbox::Event::Reference event);
    void notifyRCMS(std::string const &stateName, std::string const &msg);
    void reset(std::string const &msg);

    void logStateChangeDetails() const;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_FSMPixel_h_
