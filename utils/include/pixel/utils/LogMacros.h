#ifndef _pixel_utils_LogMacros_h_
#define _pixel_utils_LogMacros_h_

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "Colors.h"

/** @file
 * Just a few macros to make life (and logging) easier.
 */

#define DEBUG(MSG) LOG4CPLUS_DEBUG(logger_, yellow << MSG << creset)
#define INFO(MSG) LOG4CPLUS_INFO(logger_, light_cyan << MSG << creset)
#define WARN(MSG) LOG4CPLUS_WARN(logger_, light_green << MSG << creset)
#define ERROR(MSG) LOG4CPLUS_ERROR(logger_, red << MSG << creset) 
#define FATAL(MSG) LOG4CPLUS_FATAL(logger_, light_red << MSG << creset)

#define LOGDEBUG(LOGGER, MSG) LOG4CPLUS_DEBUG(LOGGER, yellow << MSG << creset)
#define LOGINFO(LOGGER, MSG) LOG4CPLUS_INFO(LOGGER, light_cyan << MSG << creset)
#define LOGWARN(LOGGER, MSG) LOG4CPLUS_WARN(LOGGER, light_green << MSG << creset)
#define LOGERROR(LOGGER, MSG) LOG4CPLUS_ERROR(LOGGER, red << MSG << creset)
#define LOGFATAL(LOGGER, MSG) LOG4CPLUS_FATAL(LOGGER, light_red << MSG << creset)

#endif
