#ifndef _pixel_utils_InfoSpaceHandlerIF_h_
#define _pixel_utils_InfoSpaceHandlerIF_h_

#include <string>

namespace pixel {
namespace utils {

class Monitor;
class WebServer;

class InfoSpaceHandlerIF {

  public:
    virtual ~InfoSpaceHandlerIF() {};

    virtual void registerItemSets(Monitor &monitor,
                                  WebServer &webServer) = 0;

  protected:
    InfoSpaceHandlerIF() {};

    /**
       * Register itemsets in this InfoSpace with a monitor
       * object. Obviously needs to be implemented by the derived
       * class itself.
       */
    virtual void registerItemSetsWithMonitor(Monitor &monitor) = 0;

    /**
       * Register itemsets in this InfoSpace with a webserver
       * object. Obviously needs to be implemented by the derived
       * class itself.
       */
    /* virtual void registerItemSetsWithWebServer(WebServer& webServer, */
    /*                                            Monitor& monitor) = 0; */
    virtual void registerItemSetsWithWebServer(WebServer &webServer,
                                               Monitor &monitor,
                                               std::string const &forceTabName = "") = 0;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_InfoSpaceHandlerIF_h_
