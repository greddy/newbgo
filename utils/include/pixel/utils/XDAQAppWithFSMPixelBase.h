#ifndef _pixel_utils_XDAQAppWithFSMPixelBase_h_
#define _pixel_utils_XDAQAppWithFSMPixelBase_h_

#include <memory>
#include <string>

#include "xdata/UnsignedLong.h"
#include "xdata/String.h"
#include "xdata/Integer.h"
#include "xdata/Integer32.h"

#include "toolbox/Event.h"

#ifndef XDAQ_TARGET_XDAQ15
#include "xoap/exception/Exception.h"
#endif
#include "xoap/MessageReference.h"

#include "pixel/utils/FSMPixel.h"
#include "pixel/utils/XDAQAppBase.h"

namespace pixel {
namespace hwlayer {
class DeviceBase;
}
}

namespace xdaq {
class ApplicationStub;
}

namespace pixel {
namespace utils {

class XDAQAppWithFSMPixelBase : public XDAQAppBase {

    // NOTE: All PIXEL control applications (at least the ones that
    // have a state machine) internally contain the same FSM
    // (implemented in XDAQAppWithFSMPixelBase). The difference between
    // classes like XDAQAppWithFSMAutomatic, XDAQAppWithFSMBasic,
    // etc. lies in the fact that different transitions are exposed
    // to the outside world (i.e., bound to SOAP commands).

    /**
       * The FSM is a friend so it has access to the
       * appStateInfoSpace_;
       */
    friend class FSMPixel;

  public:
    virtual ~XDAQAppWithFSMPixelBase();

    /**
       * Access to the state-machine transitions for ourselves.
       */
    void coldReset();
    void configure();
    void enable();
    void start();
    void fail();
    void halt();
    void pause();
    void reconfigure();
    void resume();
    void stop();

    std::string getCurrentStateName() const;
    std::set<std::string> listOfAllStates();
    std::set<std::string> listOfAllowedStates();

    /**
       * These are the usual state transitions.
       */
    void coldResetAction(toolbox::Event::Reference event);
    void transitionHaltedToConfiguringAction(toolbox::Event::Reference event);
    void initializeAction(toolbox::Event::Reference event);
    void configureAction(toolbox::Event::Reference event);
    void startAction(toolbox::Event::Reference event);
    void failAction(toolbox::Event::Reference event);
    void transitionConfiguredToHaltingAction(toolbox::Event::Reference event);
    void transitionRunningOrPausedToHaltingAction(toolbox::Event::Reference event);
    void haltAction(toolbox::Event::Reference event);
    void pauseAction(toolbox::Event::Reference event);
    void resumeAction(toolbox::Event::Reference event);
    void stopAction(toolbox::Event::Reference event);
    void reconfigureAction(toolbox::Event::Reference event);

    /**
        * There are the Soft Error Recovery state transitions
        */
    void fixSoftErrorAction(toolbox::Event::Reference event);
    void resumeFromSoftErrorAction(toolbox::Event::Reference event);

    /**
       * These two are special TTC-action state transitions from
       * Paused via an intermediate action state back to Paused.
       */
    void ttcHardResetAction(toolbox::Event::Reference event);
    void ttcResyncAction(toolbox::Event::Reference event);

    /**
     * Calibration
     */
    virtual xoap::MessageReference beginCalibration(xoap::MessageReference msg);
    virtual xoap::MessageReference endCalibration(xoap::MessageReference msg);

#ifndef XDAQ_TARGET_XDAQ15
    xoap::MessageReference changeState(xoap::MessageReference msg);
#else
    xoap::MessageReference changeState(xoap::MessageReference msg);
#endif
#ifndef XDAQ_TARGET_XDAQ15
    xoap::MessageReference fsmStateRequest(xoap::MessageReference msg);
#else
    xoap::MessageReference fsmStateRequest(xoap::MessageReference msg);
#endif

  protected:
    /**
       * Abstract class, so protected constructor.
       */
    XDAQAppWithFSMPixelBase(xdaq::ApplicationStub *const stub,
                            std::unique_ptr<pixel::hwlayer::DeviceBase> hw);

    virtual void coldResetActionImpl(toolbox::Event::Reference event);
    virtual void transitionHaltedToConfiguringActionImpl(toolbox::Event::Reference event);
    virtual void initializeActionImpl(toolbox::Event::Reference event);
    virtual void configureActionImpl(toolbox::Event::Reference event);
    virtual void startActionImpl(toolbox::Event::Reference event);
    virtual void failActionImpl(toolbox::Event::Reference event);
    virtual void transitionConfiguredToHaltingActionImpl(toolbox::Event::Reference event);
    virtual void transitionRunningOrPausedToHaltingActionImpl(toolbox::Event::Reference event);
    virtual void haltActionImpl(toolbox::Event::Reference event);
    virtual void pauseActionImpl(toolbox::Event::Reference event);
    virtual void resumeActionImpl(toolbox::Event::Reference event);
    virtual void stopActionImpl(toolbox::Event::Reference event);
    virtual void reconfigureActionImpl(toolbox::Event::Reference event);

    virtual void fixSoftErrorActionImpl(toolbox::Event::Reference event);
    virtual void resumeFromSoftErrorActionImpl(toolbox::Event::Reference event);

    virtual void ttcHardResetActionImpl(toolbox::Event::Reference event);
    virtual void ttcResyncActionImpl(toolbox::Event::Reference event);

    virtual xoap::MessageReference changeStateImpl(xoap::MessageReference msg);
    void fixSoftError();

    /**
       * The hardware-level equivalents of the state transitions.
       */
    void hwColdReset();
    virtual void hwColdResetImpl();
    void hwConfigure();
    virtual void hwConfigureImpl();
    // BUG BUG BUG
    // Missing a few here.
    // BUG BUG BUG end

    /**
       * Some configuration helpers. These methods are called just
       * before and just after applying a configuration to the
       * hardware.
       */
    void hwCfgInitialize();
    void hwCfgFinalize();
    virtual void hwCfgInitializeImpl();
    virtual void hwCfgFinalizeImpl();

  private:
    FSMPixel fsm_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_XDAQAppWithFSMPixelBase_h_
