#ifndef _pixel_utils_WebServer_h
#define _pixel_utils_WebServer_h

#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#ifndef XDAQ_TARGET_XDAQ15
#include "xgi/exception/Exception.h"
#endif
#include "xgi/framework/UIManager.h"

#include "pixel/utils/Layout.h"
#include "pixel/utils/PIXELObject.h"
#include "pixel/utils/Uncopyable.h"
#include "pixel/utils/WebObject.h"
#include "pixel/utils/WebTab.h"

namespace log4cplus {
class Logger;
}

namespace xgi {
class Input;
class Output;
}

namespace pixel {
namespace utils {

class Monitor;
class XDAQAppBase;

class WebServer : public xgi::framework::UIManager,
                  public PIXELObject,
                  private Uncopyable

                  /**
     * Webserver class: allows access to all monitorable content in
     * the PIXEL XDAQ applications.
     *
     * The HTML produced by this class is based on HTML5 BoilerPlate
     * (http://html5boilerplate.com/), via a bit of playing and
     * creative copy-paste from initializr
     * (http://www.initializr.com/).
     */
                  {

  public:
    WebServer(pixel::utils::XDAQAppBase &xdaqApp);
    ~WebServer();

    void registerTab(std::string const &name,
                     std::string const &description,
                     size_t const numColumns);
    template <class O, class I = std::string>
    void registerWebObject(std::string const &name,
                           std::string const &description,
                           Monitor const &monitor,
                           I const &itemSetName,
                           std::string const &tabName,
                           size_t const colSpan = 1);
    template <class O, class I = std::string>
    void registerWebObject(std::string const &name,
                           std::string const &description,
                           Monitor const &monitor,
                           I const &itemSetName,
                           std::string const &tabName,
                           std::string const &id);
    void registerTable(std::string const &name,
                       std::string const &description,
                       Monitor const &monitor,
                       std::string const &itemSetName,
                       std::string const &tabName,
                       size_t const colSpan = 1);
    void registerMultiColTable(std::string const &name,
                               std::string const &description,
                               Monitor const &monitor,
                               std::vector<std::string> const &itemSetNameVec,
                               std::string const &tabName,
                               std::string const &id = "");
    void registerSpacer(std::string const &name,
                        std::string const &description,
                        Monitor const &monitor,
                        std::string const &itemSetName,
                        std::string const &tabName,
                        size_t const colSpan = 1);

#ifndef XDAQ_TARGET_XDAQ15
    void jsonUpdate(xgi::Input *const in, xgi::Output *const out);
    void monitoringWebPage(xgi::Input *const in, xgi::Output *const out);
#else
    void jsonUpdate(xgi::Input *const in, xgi::Output *const out);
    void monitoringWebPage(xgi::Input *const in, xgi::Output *const out);
#endif

  private:
    /**
       * Returns the class name of the owner XDAQ application (without
       * namespaces).
       */
    std::string getApplicationName() const;

    std::string getServiceName() const;

    WebTab &getTab(std::string const &tabName) const;
    bool tabExists(std::string const &tabName) const;

    void printTabs(xgi::Output *const out) const;

    void jsonUpdateCore(xgi::Input *const in, xgi::Output *const out) const;
    void monitoringWebPageCore(xgi::Input *const in, xgi::Output *const out) const;

    log4cplus::Logger &logger_;
    std::vector<WebTab *> tabs_;
    pixel::utils::Layout layout_;
};

} // namespace utils
} // namespace pixel

#include "pixel/utils/WebServer.hxx"

#endif // _pixel_utils_WebServer_h
