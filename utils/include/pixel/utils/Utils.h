#ifndef _pixel_utils_Utils_h_
#define _pixel_utils_Utils_h_

/** @file
 * This is just a bunch of helper routines that don't fit in their own
 * little boxes.
 */

#include <cctype>
#include <map>
#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

#include "toolbox/TimeVal.h"
#include "xdata/Table.h"

namespace pixel {
namespace utils {

class ConfigurationInfoSpaceHandler;

// Convert a string representing an integer number to an integer
// number.
long parseIntFromString(std::string const &numberAsString);

// This expands environment variables in path names.
std::string expandPathName(std::string const &pathNameIn);

// Check if a certain path exists.
// NOTE: Can be either a file or a directory.
bool pathExists(std::string const &pathName);

// Check if a certain directory exists.
bool dirExists(std::string const &dirName);

// Check if a certain path is a directory or not.
bool pathIsDir(std::string const &pathName);

// Create a directory (recursively).
void makeDir(std::string const &dirName);

// Rename a file/directory.
void rename(std::string const &from, std::string const &to);

// Some misc. string methods.
struct convertUp {
    char operator()(char c) {
        return ::toupper((unsigned char)c);
    }
};

struct convertDown {
    char operator()(char c) {
        return ::tolower((unsigned char)c);
    }
};

std::string escapeAsJSONString(std::string const &stringIn);
std::string trimString(std::string const &stringToTrim,
                       std::string const &whitespace = " \t\r\n");

std::string capitalizeString(std::string const &stringIn);

// A helper method to insert 'invisible spaces' into a FED vector
// (which are strings that tend to be incredibly long).
std::string chunkifyFEDVector(std::string const &fedVecIn);

// A method to get the current timestamp, nicely formatted.
std::string getTimestamp(bool const isoFormat = false,
                         bool const shortIsoFormat = false);

// A few methods to nicely write time values.
std::string formatTimestamp(toolbox::TimeVal const timestamp,
                            toolbox::TimeVal::TimeZone const tz = toolbox::TimeVal::gmt);
std::string formatTimestampISO(toolbox::TimeVal const timestamp,
                               bool const shortIsoFormat = false);
std::string formatTimestampISO(toolbox::TimeVal const timestamp,
                               toolbox::TimeVal::TimeZone const tz,
                               bool const shortIsoFormat = false);
std::string formatTimestampDate(toolbox::TimeVal const timestamp,
                                toolbox::TimeVal::TimeZone const tz = toolbox::TimeVal::gmt);
std::string formatTimestampTime(toolbox::TimeVal const timestamp,
                                toolbox::TimeVal::TimeZone const tz = toolbox::TimeVal::gmt);
// A method to nicely format time ranges.
std::string formatTimeRange(toolbox::TimeVal const timeBegin,
                            toolbox::TimeVal const timeEnd,
                            toolbox::TimeVal::TimeZone const tz = toolbox::TimeVal::gmt);

// A method to nicely write time differences.
std::string formatDeltaTString(toolbox::TimeVal const timeBegin,
                               toolbox::TimeVal const timeEnd,
                               bool const verbose = false);

// A little helper to (zlib) compress a string.
std::string compressString(std::string const stringIn);

unsigned int countTrailingZeros(uint32_t const val);

std::string formatLabel(std::string const &type,
                        unsigned int const lpmNumber,
                        unsigned int const number,
                        pixel::utils::ConfigurationInfoSpaceHandler const &cfgInfoSpace,
                        bool const labelForExternal);

// Extract a single flashlist from a LAS.
xdata::Table getFlashList(std::string const &lasURL,
                          std::string const &flashlistName);

//add some to_string functions. maybe template this?
std::string to_string(int value);

std::string to_string(unsigned int value);

std::string to_string(long unsigned int value);

std::string to_string(long int value);

std::string to_string(float value);

std::string to_string(double value);

std::string to_string(const char* value);

std::string hex_to_string(int value);

//static std::string to_string(int value) {
    //char buffer[50];
    //sprintf(buffer, "%d", value);
    //std::string str(buffer);
    //return str;
//};

//static std::string to_string(float value) {
    //char buffer[50];
    //sprintf(buffer, "%f", value);
    //std::string str(buffer);
    //return str;
//};

//static std::string hex_to_string(int value) {
    //char buffer[50];
    //sprintf(buffer, "%x", value);
    //std::string str(buffer);
    //return str;
//};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_Utils_h_
