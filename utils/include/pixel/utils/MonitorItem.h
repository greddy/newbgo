#ifndef _pixel_utils_MonitorItem_h_
#define _pixel_utils_MonitorItem_h_

#include <string>

namespace pixel {
namespace utils {

class InfoSpaceHandler;

class MonitorItem {

  public:
    MonitorItem(std::string const &name,
                std::string const &description,
                InfoSpaceHandler *const infoSpaceHandler,
                std::string const &docString = "");

    std::string getName() const;
    std::string getDescription() const;
    InfoSpaceHandler *getInfoSpaceHandler() const;
    std::string getDocString() const;

  private:
    std::string name_;
    std::string description_;
    InfoSpaceHandler *infoSpaceHandlerP_;
    std::string docString_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_MonitorItem_h_
