#ifndef _pixel_utils_XDAQAppWithFSMAutomatic_h_
#define _pixel_utils_XDAQAppWithFSMAutomatic_h_

#include <memory>
#include <string>

#include "xcept/Exception.h"

#include "pixel/utils/FSMDriver.h"
#include "pixel/utils/XDAQAppWithFSMPixelBase.h"

namespace pixel {
namespace hwlayer {
class DeviceBase;
}
}

namespace toolbox {
class Event;
}

namespace xdaq {
class ApplicationStub;
}

namespace pixel {
namespace utils {

class XDAQAppWithFSMAutomatic : public XDAQAppWithFSMPixelBase {

    friend class FSMDriver;

    // NOTE: All PIXEL control applications (at least the ones that
    // have a state machine) internally contain the same FSM
    // (implemented in XDAQAppWithFSMBase). The difference between
    // classes like XDAQAppWithFSMAutomatic, XDAQAppWithFSMBasic,
    // etc. lies in the fact that different transitions are exposed
    // to the outside world (i.e., bound to SOAP commands).

  public:
    virtual ~XDAQAppWithFSMAutomatic();

    virtual void actionPerformed(toolbox::Event &event);

  protected:
    /**
       * Abstract class, so protected constructor.
       */
    XDAQAppWithFSMAutomatic(xdaq::ApplicationStub *const stub,
                            std::unique_ptr<pixel::hwlayer::DeviceBase> hw);

    void raiseAlarm(std::string const &name,
                    std::string const &severity,
                    xcept::Exception &err);
    void revokeAlarm(std::string const &name);

    virtual void handleMonitoringProblem(std::string const &problemDesc);

  private:
    FSMDriver fsmDriver_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_XDAQAppWithFSMAutomatic_h_
