#ifndef _pixel_utils_HwInfoSpaceUpdaterBase_h_
#define _pixel_utils_HwInfoSpaceUpdaterBase_h_

#include "pixel/utils/InfoSpaceUpdater.h"
#include "pixel/utils/PIXELObject.h"

namespace pixel {
namespace hwlayer {
class DeviceBase;
}
}

namespace pixel {
namespace utils {

class InfoSpaceHandler;
class InfoSpaceItem;
class XDAQAppBase;

class HwInfoSpaceUpdaterBase : public pixel::utils::InfoSpaceUpdater,
                               public pixel::utils::PIXELObject {

  public:
    virtual ~HwInfoSpaceUpdaterBase();

  protected:
    using PIXELObject::getOwnerApplication;

    HwInfoSpaceUpdaterBase(pixel::utils::XDAQAppBase &xdaqApp,
                           pixel::hwlayer::DeviceBase const &hw);

    virtual void updateInfoSpaceImpl(InfoSpaceHandler *const infoSpaceHandler);

    virtual bool updateInfoSpaceItem(pixel::utils::InfoSpaceItem &item,
                                     pixel::utils::InfoSpaceHandler *const infoSpaceHandler);

    pixel::hwlayer::DeviceBase const &getHw() const;

  private:
    pixel::hwlayer::DeviceBase const &hw_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_HwInfoSpaceUpdaterBase_h_
