#ifndef _pixel_utils_WebTableNoLabels_h
#define _pixel_utils_WebTableNoLabels_h

#include <cstddef>
#include <string>

#include "pixel/utils/WebObject.h"

namespace pixel {
namespace utils {

class Monitor;

class WebTableNoLabels : public WebObject {

  public:
    WebTableNoLabels(std::string const &name,
                     std::string const &description,
                     Monitor const &monitor,
                     std::string const &itemSetName,
                     std::string const &tabName,
                     size_t const colSpan);

    std::string getHTMLString() const;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_WebTableNoLabels_h
