#ifndef _pixel_utils_MultiInfoSpaceHandler_h_
#define _pixel_utils_MultiInfoSpaceHandler_h_

#include <string>
#include <map>

#include "pixel/utils/InfoSpaceHandlerIF.h"
#include "pixel/utils/Uncopyable.h"
#include "pixel/utils/XDAQObject.h"

namespace log4cplus {
class Logger;
}

namespace xdaq {
class Application;
}

namespace pixel {
namespace utils {

class InfoSpaceHandler;
class InfoSpaceUpdater;
class Monitor;
class WebServer;

class MultiInfoSpaceHandler : public InfoSpaceHandlerIF,
                              public XDAQObject,
                              private Uncopyable {

  public:
    virtual ~MultiInfoSpaceHandler();

    virtual void registerItemSets(Monitor &monitor, WebServer &webServer);

    void writeInfoSpace(bool const force = false);

  protected:
    MultiInfoSpaceHandler(xdaq::Application &xdaqApp,
                          /* std::string const& name, */
                          InfoSpaceUpdater *const updater = 0);

    /* virtual void registerItemSetsWithMonitor(Monitor& monitor) = 0; */
    virtual void registerItemSetsWithMonitor(pixel::utils::Monitor &monitor);
    virtual void registerItemSetsWithWebServer(pixel::utils::WebServer &webServer,
                                               pixel::utils::Monitor &monitor,
                                               std::string const &forceTabName = "");

    log4cplus::Logger &logger_;

    // A map to keep track of the multitude of InfoSpaceHandlers.
    std::map<std::string, pixel::utils::InfoSpaceHandler *> infoSpaceHandlers_;

  private:
    /* std::string name_; */
    /* InfoSpaceUpdater* updaterP_; */
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_MultiInfoSpaceHandler_h_
