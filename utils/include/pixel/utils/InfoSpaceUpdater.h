#ifndef _pixel_utils_InfoSpaceUpdater_h_
#define _pixel_utils_InfoSpaceUpdater_h_

#include "pixel/utils/XDAQObject.h"

namespace xdaq {
class Application;
}

namespace pixel {
namespace utils {

class InfoSpaceHandler;
class InfoSpaceItem;

/**
     * Abstract base class specifying the basic behaviour for
     * InfoSpace updaters.
     */
class InfoSpaceUpdater : public XDAQObject {

  public:
    virtual ~InfoSpaceUpdater();

    void updateInfoSpace(InfoSpaceHandler *const infoSpaceHandler);

  protected:
    /**
       * @note
       * Protected constructor since this is an abstract base class.
       */
    InfoSpaceUpdater(xdaq::Application &xdaqApp);

    virtual void updateInfoSpaceImpl(InfoSpaceHandler *const infoSpaceHandler);

    virtual bool updateInfoSpaceItem(InfoSpaceItem &item,
                                     InfoSpaceHandler *const infoSpaceHandler);
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_InfoSpaceUpdater_h_
