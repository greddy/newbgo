#ifndef _pixel_utils_CurlGetter_h_
#define _pixel_utils_CurlGetter_h_

#include <stddef.h>
#include <string>
#include <curl/curl.h>

#include "toolbox/net/URL.h"

namespace pixel {
namespace utils {

class CurlGetter {

  public:
    CurlGetter();
    ~CurlGetter();

    static std::string buildFullURL(std::string const &scheme,
                                    std::string const &address,
                                    std::string const &path,
                                    std::string const &parameters,
                                    std::string const &query,
                                    std::string const &fragment);
    static std::string buildFullURL(std::string const &baseURL,
                                    std::string const &parameters,
                                    std::string const &query,
                                    std::string const &fragment);

    std::string get(std::string const &scheme,
                    std::string const &address,
                    std::string const &path,
                    std::string const &parameters,
                    std::string const &query,
                    std::string const &fragment);
    std::string get(std::string const &baseURL,
                    std::string const &parameters,
                    std::string const &query,
                    std::string const &fragment);
    std::string get(std::string const &url);

  private:
    static size_t staticWriteCallback(char *buffer, size_t size, size_t nItems, void *getter);

    void performGetRequest(toolbox::net::URL const &url);
    size_t receive(char *const buffer, size_t const size, size_t const nItems);

    CURL *curl_;
    std::string receiveBuffer_;
    std::string errorBuffer_;
};

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_CurlGetter_h_
