#ifndef _pixel_utils_OwnerId_h_
#define _pixel_utils_OwnerId_h_

#include <stdint.h>
#include <string>

namespace pixel {
namespace utils {

class OwnerId {
    /**
     * In principle, in CMS, each data-taking run is governed by a
     * single RCMS session. In order to avoid different runs stepping
     * on each other's feet, each PIXEL control application keeps track
     * of its owner. This owner is identified by a pair of values:
     *
     * - An unsigned integer representing the RCMS session ID number.
     * - A descriptive string, if wanted.
     *
     * The session ID can be set to zero, which means the top-level
     * owner is not an RCMS session (but, e.g., a script).
     *
     * NOTE: At least one of the two above parameters has to be
     * specified.
     */

  public:
    OwnerId(uint32_t const rcmsSessionId = 0,
            std::string const &sessionName = "");

    std::string asString() const;

    uint32_t rcmsSessionId() const;
    std::string const sessionName() const;

    bool isValid() const;

    void clear();

  private:
    uint32_t rcmsSessionId_;
    std::string sessionName_;
};

bool operator==(pixel::utils::OwnerId const &lhs, pixel::utils::OwnerId const &rhs);
bool operator!=(pixel::utils::OwnerId const &lhs, pixel::utils::OwnerId const &rhs);

} // namespace utils
} // namespace pixel

#endif // _pixel_utils_Utils_h_
