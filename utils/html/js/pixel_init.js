//-----------------------------------------------------------------------------

function getBaseURL()
{
    // Figure out the base URL of the application server.
    var pathName = window.location.pathname;
    if (pathName[0] == "/")
    {
        pathName = pathName.substr(1);
    }
    if (pathName[-1] == "/")
    {
        pathName = pathName.substr(0, -1);
    }
    var pathArray = pathName.split("/");
    var newPathName = pathArray[0];
    var baseUrl = window.location.protocol + "//" + window.location.host;
    return baseUrl;
}

//-----------------------------------------------------------------------------

function getApplicationURL()
{
    // Figure out the application URL.
    var pathName = window.location.pathname;
    if (pathName[0] == "/")
    {
        pathName = pathName.substr(1);
    }
    if (pathName[-1] == "/")
    {
        pathName = pathName.substr(0, -1);
    }
    var pathArray = pathName.split("/");
    var newPathName = pathArray[0];
    var appUrl = window.location.protocol + "//" + window.location.host + "/" + newPathName;
    return appUrl;
}

//-----------------------------------------------------------------------------

function getScript(scriptUrl, forceOrder=false)
{
    console.log("Going to load script " + scriptUrl);
    var elem = document.body.appendChild(document.createElement("script"));
    if (forceOrder)
    {
        elem.async = false;
    }
    elem.setAttribute("src", scriptUrl);
    console.log("Done loading script " + scriptUrl);
}

//-----------------------------------------------------------------------------

function getCSS(cssUrl)
{
    console.log("Going to load stylesheet " + cssUrl);
    var elem = document.body.appendChild(document.createElement("link"));
    elem.setAttribute("rel", "stylesheet");
    elem.setAttribute("href", cssUrl);
    console.log("Done loading stylesheet " + cssUrl);
}

//-----------------------------------------------------------------------------

function isPIXELApplication()
{
    // Here we can use a feature of the XDAQ HyperDAQ framework and
    // check for the 'data-app-name' attribute of the HTML body.

    var res = false;
    var appName = jQuery("body").attr("data-app-name");
    if (typeof appName !== typeof undefined)
    {
        res = appName.startsWith("pixel::");
    }
    return res;
}

//-----------------------------------------------------------------------------

function zeroFill(number, width)
{
    width -= number.toString().length;
    if (width > 0)
    {
        return new Array(width + (/\./.test( number ) ? 2 : 1)).join('0') + number;
    }
    return number + "";
}

//-----------------------------------------------------------------------------

function timestamp()
{
    return new Date();
}

//----------

function timestampStr(stamp=undefined)
{
    if (typeof stamp === 'undefined')
    {
        stamp = timestamp();
    }
    else if (typeof stamp == 'string')
    {
        stamp = new Date(stamp.replace(/-/g, '/'));
    }
    else
    {
        stamp = timestamp();
    }
    return zeroFill(stamp.getHours(), 2)
        + ":"
        + zeroFill(stamp.getMinutes(), 2)
        + ":"
        + zeroFill(stamp.getSeconds() ,2);
}

//-----------------------------------------------------------------------------

// NOTE: All global variables are stuffed into the global 'pixel'
// container.
var pixel = {};
pixel.baseUrl = undefined;
pixel.applicationUrl = undefined;
pixel.updateUrl = undefined;

// How long do we want to wait for an AJAX request before we call it
// timed out?
pixel.ajaxTimeout = 2000;

pixel.defaultUpdateInterval = 1000;
pixel.updateInterval = pixel.defaultUpdateInterval;
pixel.updateFailCount = 0;
pixel.maxUpdateFailCount = 6;

pixel.data = undefined;
pixel.grids = {};
pixel.l1ahistos = undefined;
pixel.initialised = false;
pixel.templateCache = {};

// Invalid/unavailable data items in the JSON updates will look like
// this:
pixel.invalidDataStr = "-";

pixel.errors = [];
pixel.logThing = undefined;
pixel.ajaxStatus = [];
pixel.ajaxStatusThing = undefined;

//----------

// Figure out some URLs.
var baseUrl = getBaseURL();
var applicationUrl = getApplicationURL();
var updateUrl = applicationUrl + "/update";

pixel.baseUrl = baseUrl;
pixel.applicationUrl = applicationUrl;
pixel.updateUrl = updateUrl;

//-----------------------------------------------------------------------------
