//-----------------------------------------------------------------------------
//
// NOTE: This has been written to work with the XDAQ12-style HyperDAQ
// and doT.js.
//
//-----------------------------------------------------------------------------

// HyperDAQ JS pre-load callback method.
$(document).on("xdaq-pre-load", function() {
  console.debug("xdaq-pre-load");
  if (isPIXELApplication())
  {
    showLoadingScreen();
    PNotify.styling.pixeltheme = {
      container: "pixeltheme",
      notice: "pixeltheme-notice",
      // notice_icon: "pixeltheme-icon-notice",
      info: "pixeltheme-info",
      // info_icon: "pixeltheme-icon-info",
      success: "pixeltheme-success",
      // success_icon: "pixeltheme-icon-success",
      error: "pixeltheme-error",
      // error_icon: "pixeltheme-icon-error"
    }

    //----------

  }
});

//-----------------------------------------------------------------------------

// HyperDAQ JS post-load callback method.
$(document).on("xdaq-post-load", function() {
  console.debug("xdaq-post-load");
  if (isPIXELApplication())
  {
    // NOTE: At some point we may want to clean up this
    // button-business a bit.

    // Add the 'Modify TTCSpy configuration' button if we're
    // dealing with a PIController.
    var tmp = jQuery(".pixel-tab-name:contains('TTCSpy')");
    if (tmp.length)
    {
      var hook = tmp.parent();
      fixupTTCSpyConfigButton(hook);
    }

    // Add the 'Modify random-trigger rate' button if we're
    // dealing with a CPMController.
    tmp = jQuery("p:contains('Random-trigger configuration')");
    if (tmp.length)
    {
      var hook = tmp.last();
      fixupRandomRateConfigButton(hook);
    }

    tmp = jQuery(".pixel-tab-name:contains('FSM')");
    if (tmp.length)
    {
      var hook = tmp.parent();
      fixupFSMButton(hook);
    }

    tmp = jQuery("p:contains('TK FEC Configurable Options')");
    if (tmp.length)
    {
      var hook = tmp.last();
      fixupTKFecConfigOptButton(hook);
    }

    tmp = jQuery(".pixel-tab-name:contains('LOG')");
    if (tmp.length && document.getElementById("log-tymsg1").innerText.length > 1)
    {
      var hook = tmp.parent();
      selectSubsetOfLOGMessagesByType(hook, "tkfec-log-msg-history");
      selectSubsetOfMessagesByTimestamp(hook, "tkfec-log-msg-history");
    }

    // Add the 'Dump system state' button if we're
    // dealing with a PIXELCentral.
    tmp = jQuery("h1:contains('PIXELCentral')");
    if (tmp.length)
    {
      var hook = tmp;
      fixupSystemDumpButton(hook);
    }

    // Make sure that data gets updated when switching tabs (since
    // normally only visible data, and not the data in the
    // background tabs) gets updated regularly from the AJAX
    // requests.
    jQuery(".xdaq-tab-nav").click(function() {
      applyData();
    });

    //----------

    startUpdate();

    finishLoadingScreen();
  }
});

//-----------------------------------------------------------------------------

function determineNextUpdateInterval()
{
  // This method implements a basic truncated binary exponential
  // backoff for the AJAX update interval.

  if (pixel.updateFailCount)
  {
    var N = (1 << pixel.updateFailCount) - 1;
    var rnd = Math.random();
    var factor = Math.round(rnd * N);
    pixel.updateInterval = (1 + factor ) * pixel.defaultUpdateInterval;

    console.debug("DEBUG JGH c = " + pixel.updateFailCount);
    console.debug("DEBUG JGH N = " + N);
    console.debug("DEBUG JGH rnd = " + rnd);
    console.debug("DEBUG JGH factor = " + factor);
    console.debug("DEBUG JGH res = " + pixel.updateInterval);
  }
  else
  {
    pixel.updateInterval = pixel.defaultUpdateInterval;
  }
}

//-----------------------------------------------------------------------------

function startUpdate()
{
  console.debug("Starting PIXEL AJAX update loop");
  updateLoop();
}

//-----------------------------------------------------------------------------

function updateLoop()
{
  console.debug("Iteration of updateLoop()");

  setTimeout(function() {
    determineNextUpdateInterval();
    var data;
    jQuery.ajax({
      url : pixel.updateUrl,
      dataType : "json",
      data : data,
      timeout : pixel.ajaxTimeout,
      success : function(data, textStatus, jqXHR) {
        clearErrors();
        clearAjaxStatus();

        pixel.updateFailCount = Math.max(0, pixel.updateFailCount - 1);
        addAjaxStatus("AJAX success at " + timestampStr());

        processAJAXSuccess(data, textStatus, jqXHR);

        // //----------
        // // A little hacky, but it can be useful. Enable
        // // the below to check for duplicate HTML IDs.
        // $('[id]').each(function() {
        //     var ids = $('[id="' + this.id + '"]');
        //     if ((ids.length > 1) && (ids[0] == this))
        //     {
        //         console.warn("Multiple IDs #" + this.id);
        //     }
        // });
        // //----------

        if (pixel.updateFailCount)
        {
          addError("Connection with the application is shaky."
          + " Information below may be stale."
          + " Updating throttled to level " + pixel.updateFailCount
          + " of " + pixel.maxUpdateFailCount + ".");
          if (pixel.updateFailCount == pixel.maxUpdateFailCount)
          {
            addError("(Level " + pixel.updateFailCount
            + " pretty much means 'connection lost',"
            + " with periodic retries.)");
          }
        }

        showErrors();
        showAjaxStatus();

        finishLoadingScreen();
        updateLoop();
      },
      error : function(jqXHR, textStatus, errorThrown)
      {
        clearErrors();
        clearAjaxStatus();

        pixel.updateFailCount = Math.min(pixel.maxUpdateFailCount, pixel.updateFailCount + 1);
        addAjaxStatus("AJAX failure at " + timestampStr());

        processAJAXError(jqXHR, textStatus, errorThrown);

        if (pixel.updateFailCount)
        {
          addError("Connection with the application is shaky."
          + " Information below may be stale."
          + " Updating throttled to level " + pixel.updateFailCount
          + " of " + pixel.maxUpdateFailCount + ".");
          if (pixel.updateFailCount == pixel.maxUpdateFailCount)
          {
            addError("(Level " + pixel.updateFailCount
            + " pretty much means 'connection lost',"
            + " with periodic retries.)");
          }
        }

        showErrors();
        showAjaxStatus();

        finishLoadingScreen();
        updateLoop();
      }
    });
  }, pixel.updateInterval);
};

//-----------------------------------------------------------------------------

function processAJAXSuccess(data, textStatus, jqXHR)
{
  console.debug("processAJAXSuccess()");
  // clearErrors();
  // clearAjaxStatus();

  //----------

  // Check if we really received something. In case something went
  // really bad, we will receive an empty string.
  if (jQuery.isEmptyObject(data))
  {
    addError("Received an empty JSON update. " +
    "<br>" +
    "Something must have gone horribly wrong " +
    "on the application side. " +
    "<br>" +
    "Please have a look at the application log " +
    "for more information.");
  }
  else
  {
    // This is a bit hacky, but it does save a lot of CPU cycles.
    // var tmp = data["Application state"]["Latest monitoring update time"]; //Emily commenting out
    // if ((pixel.update_timestamp !== undefined) &&
    // (pixel.update_timestamp == tmp))
    // {
    //   console.warn("Skipping an AJAX update"
    //   + " that delivered data we already had"
    //   + " (data from " + tmp + ").");
    //   addAjaxStatus("Update has timestamp " + timestampStr(tmp) + " -> ignoring old data.");
    //   return;
    // }
    // else
    // {
    //   addAjaxStatus("Update has timestamp " + timestampStr(tmp) + ".");
    // }
    // pixel.update_timestamp = tmp;

    //----------

    // And now we can get started actually doing something useful.
    try
    {
      // Process and apply the data to the DOM.
      // pixel.dataPrev = pixel.data;
      pixel.data = data;
      applyData();

      //----------

      // Replace the HyperDAQ framework page title with our own one
      // (which is of course better).
      const title = jQuery("#pixel-application-title").text();
      const subTitle = jQuery("#pixel-application-subtitle").text();
      const ourTitle = title + " " + subTitle;
      document.title = ourTitle;

      // A special case: we always expect an entry called 'Application
      // State' - 'Problem description'. If that does not exist, log an
      // error. If it does exist and is not "-", announce that there has
      // apparently been some problem in the XDAQ application.
      //Emily commented out
      // var tmp0 = "Application state";
      // var tmp1 = "Problem description";
      // var tmp2 = "Uptime";
      // Verify that this element exists in the JSON data we received in
      // response to our AJAX call.
      var tmpProbDesc = pixel.invalidDataStr;
      try
      {
        // tmpProbDesc = data[tmp0][tmp1]; //Emily commenting out
      }
      catch (err)
      {
        addError("Expected (but could not find) an item called " +
        // "'" + tmp0 + "' - '" + tmp1 + "'" +
        " in the JSON update data.");
      }
      if (tmpProbDesc != pixel.invalidDataStr)
      {
        addError("This application requires attention."
        + "<br>"
        + " Please see"
        + " 'Application status'"
        + " -> 'Application state'"
        + " -> 'Problem description'"
        + " for details.");
      }

      //----------

      // BUG BUG BUG
      // This should be somewhere else, really.

      // Manipulation of the TTCSpy-in-the-PI configuration button and
      // the LPM/CPM random-rate button.

      //Emily commenting out
      // tmp0 = "Application state";
      // tmp1 = "Application FSM state";
      // var fsmState = pixel.data[tmp0][tmp1];
      // if ((fsmState == "Halted") ||
      // (fsmState == "Configuring") ||
      // (fsmState == "Zeroing"))
      // {
      //   // In these states we are not (yet fully) connected to the
      //   // hardware.
      //   jQuery("#button_configure_ttcspy").prop("disabled", true);
      //   jQuery("#button_configure_randomrate").prop("disabled", true);
      // }
      // else
      // {
      //   // In anything but the above we are connected to the hardware.
      //   jQuery("#button_configure_ttcspy").prop("disabled", false);
      //   // Argh! Only enable the button if random-triggers are
      //   // actually enabled.
      //   var button = jQuery("#button_configure_randomrate");
      //   if (button.length)
      //   {
      //     var tmp = pixel.data["itemset-inputs"]["Random-trigger generator"];
      //     var randomsEnabled = (tmp == "enabled");
      //     button.prop("disabled", !randomsEnabled);
      //   }
      // }
      // // BUG BUG BUG end

    }
    catch(err)
    {
      addError("Caught an exception: " + err);
    }

  }

  // showErrors();
  // showAjaxStatus();

  //----------

  // finishLoadingScreen();
}

//-----------------------------------------------------------------------------

function processAJAXError(jqXHR, textStatus, errorThrown)
{
  console.debug("processAJAXError()"+ errorThrown.message);
  // clearErrors();
  // clearAjaxStatus();

  var reasonString = "";
  var baseString = "";

  // Let's at least catch the usual problems. If nothing known
  // matches show a blanket fail message.
  if (textStatus == "parsererror")
  {
    baseString = "Something went wrong with the AJAX update.";
    reasonString = "An error occurred parsing the received JSON data ('" + errorThrown.message + "').";
  }
  else if (textStatus == "error")
  {
    if (errorThrown == "Bad Request")
    {
      reasonString = "Cannot connect to the XDAQ application ('Bad Request').";
    }
    else if ((jqXHR.status == 404) || (errorThrown == "Not Found"))
    {
      reasonString = "Cannot find the remote location ('Not Found').";
    }
    else
    {
      reasonString = "Lost connection to the XDAQ application ('Connection Failed').";
    }
  }
  else if (textStatus == "timeout")
  {
    reasonString = "Lost connection to the XDAQ application (connection timed out).";
  }
  else
  {
    baseString = "Something went wrong with the AJAX update.";
    reasonString = "No clue what though.";
  }
  var msg = "";
  if (baseString.length > 0)
  {
    msg = baseString;
  }
  if (reasonString.length > 0)
  {
    if (msg.length > 0)
    {
      msg += "<br>";
    }
    msg += reasonString;
  }
  if (msg.length > 0)
  {
    msg += "<br>";
  }
  // msg += "Updating stopped... Click this box to restart.";
  // addError(msg);
  console.error("Error while obtaining/parsing JSON data: " + msg);

  // showErrors();
  // showAjaxStatus();

  //----------

  // finishLoadingScreen();
}

//-----------------------------------------------------------------------------

function applyData()
{
  var data = pixel.data;

  //----------

  // // This is a bit hacky, but it does save a lot of CPU cycles.
  // var tmp = data["Application state"]["Latest monitoring update time"];
  // if ((pixel.update_timestamp !== undefined) &&
  //     (pixel.update_timestamp == tmp))
  // {
  //     console.warn("Skipping an AJAX update"
  //                  + " that delivered data we already had"
  //                  + " (data from " + tmp + ").");
  //     addAjaxStatus("Update has timestamp " + timestampStr(tmp) + " -> ignoring old data.");
  //     return;
  // }
  // else
  // {
  //     addAjaxStatus("Update has timestamp " + timestampStr(tmp) + ".");
  // }
  // pixel.update_timestamp = tmp;

  // pixel.update_timestamp = data["Application state"]["Latest monitoring update time"]; //Emily commenting out

  var scripts;
  if (!pixel.initialised)
  {
    // The first time around, when things have not yet been
    // initialised, simply use all dot template scripts.
    scripts = jQuery("script[type='text/x-dot-template']");
  }
  else
  {
    // After initialisation, only use those dot template scripts
    // that do not have an invisible .xdaq-tab as parent.
    var tmp_scripts_0 = jQuery(".xdaq-tab").not(".xdaq-tab-hide").find("script[type='text/x-dot-template']");
    // NOTE: Don't forget to include those dot template scripts
    // that don't have an .xdaq-tab as parent at all. E.g., the
    // page subtitle...
    var tmp_scripts_1 = jQuery("script[type='text/x-dot-template']:not(.xdaq-tab script)");
    scripts = tmp_scripts_0;
    scripts = scripts.add(tmp_scripts_1);
  }
  jQuery(scripts).each(function() {
    var thisScript = jQuery(this);
    if (thisScript.parent().is(":visible") || !pixel.initialised)
    {
      var templateStr = thisScript.text();
      var template = pixel.templateCache[templateStr];
      if (template == undefined)
      {
        template = doT.template(templateStr);
        pixel.templateCache[templateStr] = template;
      }
      // NOTE: The following is an attempt to handle both real
      // HTML and strings with HTML-like characters.
      var tmp = template(data);

      var html = hackIntoHTML(tmp);
      var target = thisScript.siblings(".target")[0];
      if (jQuery(target).html() != html)
      {
        jQuery(target).html(html);
      }
    }
  });

  //----------

  // Fill the application state history table.
  var placeholderName = "#application-status-history-placeholder";
  var placeholder = jQuery(placeholderName);
  if (placeholder.length)
  {
    // If the placeholder exists, this must be some sort of PIXEL application.
    var data = pixel.data["itemset-application-status-history"]["Application status history"];
    var columns = [
      {id: "timestamp", name: "Timestamp", field: "Timestamp", width: 200},
      {id: "msg", name: "Message", field: "Message", width: 400}
    ];
    updateGrid("apphist-grid", placeholder, data, columns, true);
  }

  //----------

  // Fill the TTCSpy log table.
  var placeholderName = "#ttcspylog-placeholder";
  var placeholder = jQuery(placeholderName);
  if (placeholder.length)
  {
    // If the placeholder exists, this must be a PIController.
    var tmp = pixel.data["itemset-ttcspylog"]["TTCSpy log contents"];
    var spyData = tmp;
    var columns = [];
    if (tmp !== pixel.invalidDataStr)
    {
      var logColumns = tmp["columns"];
      var columnIds = Object.keys(logColumns);
      for (var id in logColumns)
      {
        columns.push({
          "id": id,
          "field": id,
          "name": logColumns[id]
          // "formatter": ttsLogFormatter
        });
      }
      spyData = tmp["data"];
      spyData.getItemMetadata = function(row) {
        var thisRowData = this[row];
        var res = {};

        //----------
        // CSS coloring of TTC spy entries based on their types.
        //----------

        // Mark errors in red.
        var flags = thisRowData['flags'];
        if (flags.indexOf("error") > -1)
        {
          res['cssClasses'] = 'ttc_spy_row_error';
        }
        else
        {
          // Mark everything else based on the entry type.
          var type = thisRowData['type'];
          if (type == "L1A")
          {
            res['cssClasses'] = 'ttc_spy_row_l1a';
          }
          else if (type == "Broadcast command")
          {
            res['cssClasses'] = 'ttc_spy_row_bcommand_broadcast';
          }
          else if (type == "Addressed command")
          {
            res['cssClasses'] = 'ttc_spy_row_bcommand_addressed';
          }
        }

        //----------

        return res;
      };
    }
    updateGrid("ttcspylog-grid", placeholder, spyData, columns, false);
  }

  //----------

  // Fill the TTS history table.

  function ttsLogFormatter(row, cell, value, columnDef, dataContext)
  {
    return "<span tts_val='" + value + "'>" + value + "</span>";
  }

  var placeholderName = "#ttslog-placeholder";
  var placeholder = jQuery(placeholderName);
  if (placeholder.length)
  {
    // If the placeholder exists, this must be a PIController.
    var tmp = pixel.data["itemset-ttslog"]["TTS log contents"];
    var logData = tmp;
    var columns = [];
    if (tmp !== pixel.invalidDataStr)
    {
      var logColumns = tmp["columns"];
      var columnIds = Object.keys(logColumns);
      for (var id in logColumns)
      {
        columns.push({
          "id": id,
          "field": id,
          "name": logColumns[id],
          "formatter": ttsLogFormatter
        });
      }
      logData = tmp["data"];
      logData.getItemMetadata = function(row) {
        var thisRowData = this[row];
        var res = {};

        //----------

        // NOTE: Each entry can be accompanied by a simple
        // text comment. Entries with orbit number 0 and BX
        // number 0 are used as 'pure comments.'

        // If this is a pure comment, put it widely across all
        // columns.
        if ((thisRowData.orbit == 0) && (thisRowData.bx == 0))
        {
          var options = {
            groupCssClass: "slick-group",
            groupTitleCssClass: "slick-group-title",
            totalsCssClass: "slick-group-totals",
            groupFocusable: true,
            totalsFocusable: false,
            toggleCssClass: "slick-group-toggle",
            toggleExpandedCssClass: "expanded",
            toggleCollapsedCssClass: "collapsed",
            enableExpandCollapse: true
            // groupFormatter: defaultGroupCellFormatter,
            // totalsFormatter: defaultTotalsCellFormatter
          };
          function defaultGroupCellFormatter(row, cell, value, columnDef, item) {
            if (!options.enableExpandCollapse) {
              return item.title;
            }

            var indentation = item.level * 15 + "px";

            return "<span class='" + options.toggleCssClass + " " +
            (item.collapsed ? options.toggleCollapsedCssClass : options.toggleExpandedCssClass) +
            "' style='margin-left:" + indentation +"'>" +
            "</span>" +
            "<span class='" + options.groupTitleCssClass + "' level='" + item.level + "'>" +
            thisRowData.comment +
            "</span>";
          }
          res = {
            "columns": {
              0: {
                colspan: "*",
                formatter: defaultGroupCellFormatter,
                editor: null
              }
            }
          }
        }
        else
        {
          //----------
          // CSS coloring of 'normal' TTS log entries.
          //----------
          // Let's mark entries with the output TTS state in
          // anything but READY.
          var outputState = thisRowData['tts_out'];
          if (outputState != "ready")
          {
            res['cssClasses'] = 'tts_log_row_not_ready';
          }
          else
          {
            res['cssClasses'] = 'tts_log_row_ready';
          }
        }

        //----------

        return res;
      };
    }
    updateGrid("ttslog-grid", placeholder, logData, columns, false);
  }

  //----------

  // Fill the recent-nibbles history table.
  var placeholderName = "#nibblehistory-placeholder";
  var placeholder = jQuery(placeholderName);
  if (placeholder.length)
  {
    // If the placeholder exists, this must be a CPMController.
    var historyData = pixel.data["itemset-cpm-nibble-history"]["Recent nibbles"];
    var columns = [
      {id: "runnum", name: "Run number", field: "RunNumber"},
      {id: "sectionnum", name: "Section number", field: "SectionNumber"},
      {id: "nibblenum", name: "Nibble number", field: "NibbleNumber"},
      {id: "runactive", name: "Run active", field: "RunActive"},
      {id: "numorbits", name: "# orbits", field: "NumOrbits"},
      {id: "triggerrate", name: "Trigger rate (Hz)", field: "TriggerRate"},
      {id: "suppressedtriggerrate", name: "Suppressed-trigger rate (Hz)", field: "SuppressedTriggerRate"},
      {id: "deadtime", name: "Deadtime (% of time)", field: "Deadtime"},
      {id: "deadtimebeamactive", name: "Deadtime with beam active (% of time)", field: "DeadtimeBeamActive"}
    ];
    updateGrid("nibblehistory-grid", placeholder, historyData, columns, true);
  }

  //----------

  // Fill the recent-sections history table.
  var placeholderName = "#sectionhistory-placeholder";
  var placeholder = jQuery(placeholderName);
  if (placeholder.length)
  {
    // If the placeholder exists, this must be a CPMController.
    var historyData = pixel.data["itemset-cpm-section-history"]["Recent sections"];
    var columns = [
      {id: "runnum", name: "Run number", field: "RunNumber"},
      {id: "sectionnum", name: "Section number", field: "SectionNumber"},
      {id: "numnibbles", name: "# nibbles", field: "NumNibbles"},
      {id: "triggerrate", name: "Trigger rate (Hz)", field: "TriggerRate"},
      {id: "suppressedtriggerrate", name: "Suppressed-trigger rate (Hz)", field: "SuppressedTriggerRate"},
      {id: "deadtime", name: "Deadtime (% of time)", field: "Deadtime"},
      {id: "deadtimebeamactive", name: "Deadtime with beam active (% of time)", field: "DeadtimeBeamActive"}
    ];
    updateGrid("sectionhistory-grid", placeholder, historyData, columns, true);
  }

  //----------

  // Fill the recent-BRILDAQ history table.
  var placeholderName = "#brildaqhistory-placeholder";
  var placeholder = jQuery(placeholderName);
  if (placeholder.length)
  {
    // If the placeholder exists, this must be a BRILDAQChecker.
    var historyData = pixel.data["itemset-brildaq-history"]["Recent PIXEL BRILDAQ data"];
    var columns = [
      // {id: "formatversion", name: "Data format version", field: "FormatVersion"},
      {id: "timestamp", name: "BST timestamp", field: "BSTTimestamp"},
      {id: "fillnum", name: "Fill number", field: "FillNumber"},
      {id: "runnum", name: "Run number", field: "RunNumber"},
      {id: "sectionnum", name: "Section number", field: "SectionNumber"},
      {id: "nibblenum", name: "Nibble number", field: "NibbleNumber"},
      {id: "runactive", name: "Run active", field: "CMSRunActive"},
      {id: "deadtime", name: "Deadtime (% of time)", field: "Deadtime"},
      {id: "deadtimebeamactive", name: "Deadtime with beam-active (% of time)", field: "DeadtimeBeamActive"}
    ];
    updateGrid("brildaqhistory-grid", placeholder, historyData, columns, true);
  }

  //----------
  // Fill the APVE pipeline history table.
  var placeholderName = "#apvesimhistory-placeholder";
  var placeholder = jQuery(placeholderName);
  if (placeholder.length)
  {
    // If the placeholder exists, this must be an APVEController.
    var historyData = pixel.data["itemset-simhist"]["Simulated APV pipeline history"];
    var columns = [
      {id: "eventnumber", name: "Event number", field: "EventNumber"},
      {id: "address", name: "Pipeline address", field: "PipelineAddress"},
      {id: "graycode", name: "Gray code", field: "PipelineAddressGrayCode"},
      {id: "deltaaddress", name: "Address difference", field: "PipelineAddressDelta"}
    ];
    updateGrid("apvesimhistory-grid", placeholder, historyData, columns, false);
  }

  //----------

  // Fill the APVE status history table.
  var placeholderName = "#apvestatushistory-placeholder";
  var placeholder = jQuery(placeholderName);
  if (placeholder.length)
  {
    // If the placeholder exists, this must be an APVEController.
    var historyData = pixel.data["itemset-statushist"]["TTS and APVE status history"];
    var columns = [
      {id: "orbitnumber", name: "Orbit number", field: "OrbitNumber"},
      {id: "bxnumber", name: "BX number", field: "BXNumber"},
      {id: "ttsstate", name: "Output TTS state", field: "TTSState"},
      {id: "fmmttsstate", name: "FMM TTS", field: "FMMTTSState"},
      {id: "apvttsstate", name: "APV TTS", field: "APVTTSState"},
      {id: "apvoosreason", name: "APV 'Out-of-sync' reason", field: "APVOOSReason"},
      {id: "apverrorreason", name: "APV 'Error' reason", field: "APVErrorReason"},
      {id: "flags", name: "Flags", field: "Flags"}
    ];
    updateGrid("apvestatushistory-grid", placeholder, historyData, columns, false);
  }

  //----------

  placeholderName = "#l1ahistos-placeholder";
  placeholder = jQuery(placeholderName);
  if (placeholder.length && placeholder.is(":visible"))
  {
    // First extract the data from the JSON update and remodel it
    // a little.
    const histoDataTmp = pixel.data["itemset-l1a-histos"];
    var histoData = [];
    const labels = Object.keys(histoDataTmp);
    for (var i = 0; i != labels.length; ++i)
    {
      var label = labels[i];
      var dataTmp = histoDataTmp[label];
      //if ((dataTmp != "-") && (Math.max.apply(Math, dataTmp) != 0))
      if (dataTmp != pixel.invalidDataStr)
      {
        histoData.push({label: label,
          data: dataTmp});
        }
      }

      if (!histoData.length)
      {
        placeholder.text(pixel.invalidDataStr);
        delete pixel.l1ahistos;
        pixel.l1ahistos = undefined;
      }
      else
      {
        if (pixel.l1ahistos === undefined)
        {
          jQuery(placeholderName).html(jQuery("<div></div>")
          .attr("id", "chart-container")
          .attr("class", "chart-container"));
          pixel.l1ahistos = new L1AHistos("#chart-container");
          pixel.l1ahistos.init();
          pixel.l1ahistos.setData(histoData);
          pixel.l1ahistos.plot();
        }
        else
        {
          pixel.l1ahistos.setData(histoData);
          pixel.l1ahistos.update();
        }
      }
    }

    //----------

    pixel.initialised = true;
  }

  //-----------------------------------------------------------------------------

  function updateGrid(id, container, gridData, columns, reverse)
  {
    if (jQuery(container).length && jQuery(container).is(":visible"))
    {
      if (gridData == pixel.invalidDataStr)
      {
        jQuery(container).text(pixel.invalidDataStr);
        delete pixel.grids[id];
        pixel.grids[id] = undefined;
      }
      else
      {
        if (reverse)
        {
          gridData.reverse();
        }

        var grid = pixel.grids[id];
        if (grid === undefined)
        {
          var options = {
            enableCellNavigation: true,
            enableColumnReorder: false,
            forceFitColumns: true,
            fullWidthRows: true,
            multiSelect: true
          };
          jQuery(container).html(jQuery("<div></div>").attr("id", id).attr("class", "grid"));
          grid = new Slick.Grid("#" + id, gridData, columns, options);
          grid.setSelectionModel(new Slick.RowSelectionModel());
          grid.registerPlugin(new Slick.AutoTooltips({enableForHeaderCells: true}));
          grid.render();
          pixel.grids[id] = grid;

          if (!reverse)
          {
            // NOTE: This is not particularly pretty, no...
            jQuery(container).prepend("<div>"
            + "<input type=\"checkbox\""
            + " id=\"tail_button_" + id + "\""
            + " style=\"display: inline-block;\"/>"
            + "<label for=\"tail_button_" + id + "\""
            + " style=\"display: inline-block;\">"
            + "Follow tail of table"
            + "</label>"
            + "</div>");
          }
          jQuery(container).prepend("<div>"
          + "<input type=\"checkbox\""
          + " id=\"freeze_button_" + id + "\""
          + " style=\"display: inline-block;\"/>"
          + "<label for=\"freeze_button_" + id + "\""
          + " style=\"display: inline-block;\">"
          + "Freeze table"
          + "</label>"
          + "</div>");
        }
        else
        {
          var button = jQuery("#freeze_button_" + id);
          if (!button.is(":checked"))
          {
            grid.setData(gridData);
            grid.invalidate();
            // grid.resizeCanvas();
            grid.render();
          }
        }

        // Scroll to bottom if requested.
        var button = jQuery("#tail_button_" + id);
        if (button.is(":checked"))
        {
          grid.scrollRowIntoView(grid.getDataLength());
        }
      }
    }
  }

  //-----------------------------------------------------------------------------

  function clearErrors(htmlText)
  {
    pixel.errors = [];
  }

  //-----------------------------------------------------------------------------

  function addError(htmlText)
  {
    pixel.errors.push(htmlText);
  }

  //-----------------------------------------------------------------------------

  function showErrors()
  {
    var numErrors = pixel.errors.length;
    if (numErrors > 0)
    {
      // Show the errors.
      var tmp = "";
      for (var i=0; i < numErrors; ++i)
      {
        if (i != 0)
        {
          tmp += "<hr>";
        }
        tmp += pixel.errors[i];
      }

      if (typeof pixel.logThing === 'undefined')
      {
        var stack_bar_top = {"dir1": "down",
        "dir2": "right",
        "push": "top",
        "spacing1": 0,
        "spacing2": 0};
        var opts = {
          type: "error",
          title: "",
          text: tmp,
          stack: stack_bar_top,
          addclass: "stack-bar-top bigandproud",
          cornerclass: "",
          width: "100%",
          hide: false,
          shadow: false,
          icon: false,
          styling: "pixeltheme",
          nonblock: {
            nonblock: true
          }
        };
        pixel.logThing = new PNotify(opts);
      }
      else
      {
        pixel.logThing.update(tmp);
      }

      // Mark the page title with a warning.
      var tmp = document.title;
      var ind = tmp.indexOf("(");
      if (ind >= 0)
      {
        tmp = tmp.substring(0, ind);
      }
      tmp = tmp + " (!)";
      document.title = tmp;
    }
    else
    {
      console.debug("No problem(s) to report");
      if (typeof pixel.logThing !== 'undefined')
      {
        pixel.logThing.remove();
        delete pixel.logThing;
        pixel.logThing = undefined;
      }
    }
  }

  //-----------------------------------------------------------------------------

  function clearAjaxStatus(htmlText)
  {
    pixel.ajaxStatus = [];
  }

  //-----------------------------------------------------------------------------

  function addAjaxStatus(htmlText)
  {
    pixel.ajaxStatus.push(htmlText);
  }

  //-----------------------------------------------------------------------------

  function showAjaxStatus()
  {
    var msgBase = pixel.ajaxStatus[0];
    var type = "notice";
    if (msgBase.indexOf("success") > -1)
    {
      type = "success";
    }
    else if (msgBase.indexOf("failure") > -1)
    {
      type = "error";
    }

    var num = pixel.ajaxStatus.length;
    if (num > 0)
    {
      var tmp = "";
      for (var i=0; i < num; ++i)
      {
        if (i != 0)
        {
          tmp += "<br>";
        }
        tmp += pixel.ajaxStatus[i];
      }
    }

    if (typeof pixel.ajaxStatusThing === 'undefined')
    {
      var stack_bottomright = {"dir1": "up",
      "dir2": "left",
      "firstpos1": 1,
      "firstpos2": 1};
      var opts = {
        type: type,
        text: tmp,
        stack: stack_bottomright,
        addclass: "stack-bottomright smallandtimid",
        cornerclass: "",
        width: "auto",
        hide: false,
        shadow: false,
        icon: false,
        styling: "pixeltheme"

      };
      pixel.ajaxStatusThing = new PNotify(opts);
      pixel.ajaxStatusThing.elem.attr("id", "ajax_status_thing");
    }
    else
    {
      pixel.ajaxStatusThing.update({
        type: type,
        text: tmp
      });
    }
  }

  //-----------------------------------------------------------------------------
  function fsmSendSOAP(buttonPressed)
  {
    var attributes = [
      {
        "name" : "xdaq:actionRequestorId",
        "value" : "dummy_session"
      },
      // {
      //   "name" : "GlobalKey", //Configure should never be sent from here
      //   "value" : 0
      // }
    ];

    var soapMsg = makeSOAPCommand(buttonPressed, attributes);

    jQuery.ajax({
      type: "post",
      url: pixel.applicationUrl,
      contentType: "text/xml",
      dataType: "xml",
      data: soapMsg,
      processData: false,
      success: function(data, textStatus, req) {
        var fault = jQuery(data).find("Fault");
        if (fault.length)
        {
          var msg = "Something went wrong";
          var tmpFaultString = fault.find("faultstring");
          var tmpDetail = fault.find("detail");
          if (!tmpFaultString.size() && !tmpDetail)
          {
            msg = msg + ", but it is not clear exactly what."
          }
          else
          {
            if (tmpFaultString.size())
            {
              msg = msg + ": " + tmpFaultString.text();
            }
            if (tmpDetail.size())
            {
              if (tmpFaultString.size())
              {
                msg = msg + ": ";
              }
              msg += tmpDetail.text();
            }
          }
          alert(msg);
        }
      },
      error: function(data, status, req) {
        alert(req.responseText + " " + status);
      }
    });

    setTimeout(function () {document.location.reload();}, 500);
  }

  function configureTTCSpy()
  {
    // Extract the parameters from the form.
    var loggingMode = jQuery("#loggingMode").val();
    var triggerTermCombinationOperator = jQuery("#triggerTermCombinationOperator").val();
    var l1a = jQuery("#l1a").is(":checked");
    var brcAll = jQuery("#brcAll").is(":checked");
    var addAll = jQuery("#addAll").is(":checked");
    var brcBC0 = jQuery("#brcBC0").is(":checked");
    var brcEC0 = jQuery("#brcEC0").is(":checked");
    var brcDDDDAll = jQuery("#brcDDDDAll").is(":checked");
    var brcTTAll = jQuery("#brcTTAll").is(":checked");
    var brcZeroData = jQuery("#brcZeroData").is(":checked");
    var adrZeroData = jQuery("#adrZeroData").is(":checked");
    var errCom = jQuery("#errCom").is(":checked");
    var brcDDDD = jQuery("#brcDDDD").val();
    var brcTT = jQuery("#brcTT").val();
    var brcVal0 = jQuery("#brcVal0").val();
    var brcVal1 = jQuery("#brcVal1").val();
    var brcVal2 = jQuery("#brcVal2").val();
    var brcVal3 = jQuery("#brcVal3").val();
    var brcVal4 = jQuery("#brcVal4").val();
    var brcVal5 = jQuery("#brcVal5").val();
    var parameters = [
      {
        "name" : "loggingMode",
        "type" : "string",
        "value" : loggingMode
      },
      {
        "name" : "triggerTermCombinationOperator",
        "type" : "string",
        "value" : triggerTermCombinationOperator
      },
      {
        "name" : "l1a",
        "type" : "boolean",
        "value" : l1a
      },
      {
        "name" : "brcAll",
        "type" : "boolean",
        "value" : brcAll
      },
      {
        "name" : "addAll",
        "type" : "boolean",
        "value" : addAll
      },
      {
        "name" : "brcBC0",
        "type" : "boolean",
        "value" : brcBC0
      },
      {
        "name" : "brcEC0",
        "type" : "boolean",
        "value" : brcEC0
      },
      {
        "name" : "brcDDDDAll",
        "type" : "boolean",
        "value" : brcDDDDAll
      },
      {
        "name" : "brcTTAll",
        "type" : "boolean",
        "value" : brcTTAll
      },
      {
        "name" : "brcZeroData",
        "type" : "boolean",
        "value" : brcZeroData
      },
      {
        "name" : "adrZeroData",
        "type" : "boolean",
        "value" : adrZeroData
      },
      {
        "name" : "errCom",
        "type" : "boolean",
        "value" : errCom
      },
      {
        "name" : "brcDDDD",
        "type" : "unsignedInt",
        "value" : brcDDDD
      },
      {
        "name" : "brcTT",
        "type" : "unsignedInt",
        "value" : brcTT
      },
      {
        "name" : "brcVal0",
        "type" : "unsignedInt",
        "value" : brcVal0
      },
      {
        "name" : "brcVal1",
        "type" : "unsignedInt",
        "value" : brcVal1
      },
      {
        "name" : "brcVal2",
        "type" : "unsignedInt",
        "value" : brcVal2
      },
      {
        "name" : "brcVal3",
        "type" : "unsignedInt",
        "value" : brcVal3
      },
      {
        "name" : "brcVal4",
        "type" : "unsignedInt",
        "value" : brcVal4
      },
      {
        "name" : "brcVal5",
        "type" : "unsignedInt",
        "value" : brcVal5
      }
    ];

    // Build the SOAP message and send it off.
    var soapMsg = buildSOAPCommand("ConfigureTTCSpy",
    parameters);

    jQuery.ajax({
      type: "post",
      url: pixel.applicationUrl,
      contentType: "text/xml",
      dataType: "xml",
      data: soapMsg,
      processData: false,
      success: function(data, textStatus, req) {
        var fault = jQuery(data).find("Fault");
        if (fault.length)
        {
          var msg = "Something went wrong";
          var tmpFaultString = fault.find("faultstring");
          var tmpDetail = fault.find("detail");
          if (!tmpFaultString.size() && !tmpDetail)
          {
            msg = msg + ", but it is not clear exactly what."
          }
          else
          {
            if (tmpFaultString.size())
            {
              msg = msg + ": " + tmpFaultString.text();
            }
            if (tmpDetail.size())
            {
              if (tmpFaultString.size())
              {
                msg = msg + ": ";
              }
              msg += tmpDetail.text();
            }
          }
          alert(msg);
        }
      },
      error: function(data, status, req) {
        alert(req.responseText + " " + status);
      }
    });
  }

  function configureOptionsTKFEC(){

    // Extract the parameters from the form.
    var doDCULoop = jQuery("#doDCULoop").is(":checked");
    var doCCUCheckLoop = jQuery("#doCCUCheckLoop").is(":checked");
    var workloopContinue = jQuery("#workloopContinue").is(":checked");
    var workloopContinueCCU = jQuery("#workloopContinueCCU").is(":checked");
    var workloopContinueRC = jQuery("#workloopContinueRC").is(":checked");
    var debugBool = jQuery("#debugBool").is(":checked");
    var skip_lv_check = jQuery("#skip_lv_check").is(":checked");
    var do_force_ccu_readout = jQuery("#do_force_ccu_readout").is(":checked");
    var resume_do_portcard_recovery = jQuery("#resume_do_portcard_recovery").is(":checked");
    var ser_do_portcard_recovery = jQuery("#ser_do_portcard_recovery").is(":checked");
    var ser_do_powercycle_dcdc = jQuery("#ser_do_powercycle_dcdc").is(":checked");
    var extraTimers = jQuery("#extraTimers").is(":checked");
    var readDCU_workloop_sleep = jQuery("#readDCU_workloop_sleep").val();
    var checkCCU_workloop_sleep = jQuery("#checkCCU_workloop_sleep").val();

    var attributes = [
      {
        "name" : "doDCULoop",
        "value" : doDCULoop
      },
      {
        "name" : "doCCUCheckLoop",
        "value" : doCCUCheckLoop
      },
      {
        "name" : "workloopContinue",
        "value" : workloopContinue
      },
      {
        "name" : "workloopContinueCCU",
        "value" : workloopContinueCCU
      },
      {
        "name" : "workloopContinueRC",
        "value" : workloopContinueRC
      },
      {
        "name" : "debugBool",
        "value" : debugBool
      },
      {
        "name" : "skip_lv_check",
        "value" : skip_lv_check
      },
      {
        "name" : "do_force_ccu_readout",
        "value" : do_force_ccu_readout
      },
      {
        "name" : "resume_do_portcard_recovery",
        "value" : resume_do_portcard_recovery
      },
      {
        "name" : "ser_do_portcard_recovery",
        "value" : ser_do_portcard_recovery
      },
      {
        "name" : "ser_do_powercycle_dcdc",
        "value" : ser_do_powercycle_dcdc
      },
      {
        "name" : "extraTimers",
        "value" : extraTimers
      },
      {
        "name" : "readDCU_workloop_sleep",
        "value" : readDCU_workloop_sleep
      },
      {
        "name" : "checkCCU_workloop_sleep",
        "value" : checkCCU_workloop_sleep
      }
    ];

    // Build the SOAP message and send it off.
    var soapMsg = makeSOAPCommand("ChangeTkFECConfig",attributes); //Will need to change this

    jQuery.ajax({
      type: "post",
      url: pixel.applicationUrl,
      contentType: "text/xml",
      dataType: "xml",
      data: soapMsg,
      processData: false,
      success: function(data, textStatus, req) {
        var fault = jQuery(data).find("Fault");
        if (fault.length) {
          var msg = "Something went wrong";
          var tmpFaultString = fault.find("faultstring");
          var tmpDetail = fault.find("detail");
          if (!tmpFaultString.size() && !tmpDetail) {
            msg = msg + ", but it is not clear exactly what."
          }
          else {
            if (tmpFaultString.size()) {
              msg = msg + ": " + tmpFaultString.text();
            }
            if (tmpDetail.size()) {
              if (tmpFaultString.size()) {
                msg = msg + ": ";
              }
              msg += tmpDetail.text();
            }
          }
          alert(msg);
        }
      },
      error: function(data, status, req) {
        alert(req.responseText + " " + status);
      }
    }); //end ajax part
  }


  //-----------------------------------------------------------------------------

  function configureRandomRate()
  {
    // Extract the parameters from the form.
    var frequency = jQuery("#frequency").val();
    var cmdName = "";
    var parameters = [];

    if (frequency != 0)
    {
      cmdName = "EnableRandomTriggers"
      parameters = [
        {
          "name" : "frequency",
          "type" : "unsignedInt",
          "value" : frequency
        },
      ];
    }
    else
    {
      cmdName = "DisableRandomTriggers"
      parameters = [];
    }

    // Build the SOAP message and send it off.
    var soapMsg = buildSOAPCommand(cmdName, parameters);

    jQuery.ajax({
      type: "post",
      url: pixel.applicationUrl,
      contentType: "text/xml",
      dataType: "xml",
      data: soapMsg,
      processData: false,
      success: function(data, textStatus, req) {
        var fault = jQuery(data).find("Fault");
        if (fault.length)
        {
          var msg = "Something went wrong";
          var tmpFaultString = fault.find("faultstring");
          var tmpDetail = fault.find("detail");
          if (!tmpFaultString.size() && !tmpDetail)
          {
            msg = msg + ", but it is not clear exactly what."
          }
          else
          {
            if (tmpFaultString.size())
            {
              msg = msg + ": " + tmpFaultString.text();
            }
            if (tmpDetail.size())
            {
              if (tmpFaultString.size())
              {
                msg = msg + ": ";
              }
              msg += tmpDetail.text();
            }
          }
          alert(msg);
        }
      },
      error: function(data, status, req) {
        alert(req.responseText + " " + status);
      }
    });
  }

  //-----------------------------------------------------------------------------

  function requestSystemDump()
  {
    // Extract the parameters from the form.
    var reason = jQuery("#reason").val();
    var cmdName = "DumpSystemState";
    var parameters = [
      {
        "name" : "reason",
        "type" : "string",
        "value" : reason
      }
    ]

    // Build the SOAP message and send it off.
    var soapMsg = buildSOAPCommand(cmdName, parameters);

    jQuery.ajax({
      type: "post",
      url: pixel.applicationUrl,
      contentType: "text/xml",
      dataType: "xml",
      data: soapMsg,
      processData: false,
      success: function(data, textStatus, req) {
        var fault = jQuery(data).find("Fault");
        if (fault.length)
        {
          var msg = "Something went wrong";
          var tmpFaultString = fault.find("faultstring");
          var tmpDetail = fault.find("detail");
          if (!tmpFaultString.size() && !tmpDetail)
          {
            msg = msg + ", but it is not clear exactly what."
          }
          else
          {
            if (tmpFaultString.size())
            {
              msg = msg + ": " + tmpFaultString.text();
            }
            if (tmpDetail.size())
            {
              if (tmpFaultString.size())
              {
                msg = msg + ": ";
              }
              msg += tmpDetail.text();
            }
          }
          alert(msg);
        }
      },
      error: function(data, status, req) {
        alert(req.responseText + " " + status);
      }
    });
  }

  //-----------------------------------------------------------------------------

  function buildSOAPCommand(cmdName, params)
  {
    var soapMsg = "<?xml version=\"1.0\" ?>" +
    "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"" +
    "              xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"" +
    "              xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
    "<env:Header/>" +
    "<env:Body>" +
    "<xdaq:" + cmdName + " xmlns:xdaq=\"urn:xdaq-soap:3.0\">";

    for (var i = 0; i < params.length; ++i)
    {
      var par = params[i];
      soapMsg += "<xdaq:" + par.name + " xsi:type=\"xsd:" + par.type + "\">" +
      par.value +
      "</xdaq:" + par.name + ">";
    }

    soapMsg += "</xdaq:" + cmdName + ">" +
    "</env:Body>" +
    "</env:Envelope>";

    soapMsg = jQuery.trim(soapMsg);
    return soapMsg;
  }

  function makeSOAPCommand(cmdName, attributes)
  {
    var soapMsg = "<?xml version=\"1.0\" ?>" +
    "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\"" +
    "              xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"" +
    "              xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
    "<env:Header/>" +
    "<env:Body>" +
    "<xdaq:" + cmdName;

    for (let attr of attributes)
        soapMsg += " " + attr.name + "=\"" + attr.value + "\"";

    soapMsg += " xmlns:xdaq=\"urn:xdaq-soap:3.0\"/>" +
    "</env:Body>" +
    "</env:Envelope>";

    soapMsg = jQuery.trim(soapMsg);
    return soapMsg;
  }

  //-----------------------------------------------------------------------------

  function fixupTTCSpyConfigButton(hook)
  {
    hook.append("<button id=\"button_configure_ttcspy\">Modify TTCSpy configuration</button>");

    jQuery("body").append("<div id=\"dialog_form_ttcspy\" title=\"Modify TTCSpy configuration\">" +
    "<form>" +

    "<fieldset>" +
    "<legend>Logging configuration:</legend>" +

    // Logging mode.
    "<label for=\"loggingMode\">Logging mode</label>" +
    "<select id=\"loggingMode\" name=\"loggingMode\">" +
    "<option value=\"LogOnly\">LogOnly</option>" +
    "<option value=\"LogAllExcept\">LogAllExcept</option>" +
    "</select>" +

    "</fieldset>" +

    "<fieldset>" +
    "<legend>Trigger configuration:</legend>" +

    // Trigger-term combination operator.
    "<label for=\"triggerTermCombinationOperator\">Logging logic operator</label>" +
    "<select id=\"triggerTermCombinationOperator\" name=\"triggerTermCombinationOperator\">" +
    "<option value=\"OR\">OR</option>" +
    "<option value=\"AND\">AND</option>" +
    "</select>" +

    // L1As.
    "<label for=\"bcrL1A\">Trigger on L1As</label>" +
    "<input type=\"checkbox\" id=\"l1a\" name=\"l1a\"/>" +

    // brcAll.
    "<label for=\"bcrAll\">Trigger on all broadcast B-commands</label>" +
    "<input type=\"checkbox\" id=\"brcAll\" name=\"brcAll\"/>" +

    // addAll.
    "<label for=\"bcrAll\">Trigger on all addressed B-commands</label>" +
    "<input type=\"checkbox\" id=\"addAll\" name=\"addAll\"/>" +

    // brcBC0.
    "<label for=\"bcrBC0\">Trigger on broadcast B-commands with the bunch-counter reset bit set</label>" +
    "<input type=\"checkbox\" id=\"brcBC0\" name=\"brcBC0\"/>" +

    // brcEC0.
    "<label for=\"bcrEC0\">Trigger on broadcast B-commands with the event-counter reset bit set</label>" +
    "<input type=\"checkbox\" id=\"brcEC0\" name=\"brcEC0\"/>" +

    // brcDDDDAll.
    "<label for=\"bcrDDDDAll\">Trigger on all broadcast B-commands with non-zero user-data bits</label>" +
    "<input type=\"checkbox\" id=\"brcDDDDAll\" name=\"brcDDDDAll\"/>" +

    // brcTTAll.
    "<label for=\"bcrTTAll\">Trigger on all broadcast B-commands with non-zero test-data bits</label>" +
    "<input type=\"checkbox\" id=\"brcTTAll\" name=\"brcTTAll\"/>" +

    // brcZeroData.
    "<label for=\"brcZeroData\">Trigger on all broadcast B-commands with zero data payload</label>" +
    "<input type=\"checkbox\" id=\"brcZeroData\" name=\"brcZeroData\"/>" +

    // adrZeroData.
    "<label for=\"adrZeroData\">Trigger on all addressed B-commands with zero data payload</label>" +
    "<input type=\"checkbox\" id=\"adrZeroData\" name=\"adrZeroData\"/>" +

    // errCom.
    "<label for=\"errComm\">Trigger on all communication errors</label>" +
    "<input type=\"checkbox\" id=\"errCom\" name=\"errCom\"/>" +

    // brcDDDD.
    "<label for=\"bcrDDDD\">Trigger on broadcast B-commands with this value in the user-data bits (if non-zero)</label>" +
    "<input type=\"text\" id=\"brcDDDD\" name=\"brcDDDD\"/>" +

    // brcTT.
    "<label for=\"bcrTT\">Trigger on broadcast B-commands with this value in the test-data bits (if non-zero)</label>" +
    "<input type=\"text\" id=\"brcTT\" name=\"brcTT\"/>" +

    // brcVal0.
    "<label for=\"bcrVal0\">Trigger on broadcast B-commands with this value in the (8-bit) data (1 of 6)</label>" +
    "<input type=\"text\" id=\"brcVal0\" name=\"brcVal0\"/>" +

    // brcVal1.
    "<label for=\"bcrVal1\">Trigger on broadcast B-commands with this value in the (8-bit) data (2 of 6)</label>" +
    "<input type=\"text\" id=\"brcVal1\" name=\"brcVal1\"/>" +

    // brcVal2.
    "<label for=\"bcrVal2\">Trigger on broadcast B-commands with this value in the (8-bit) data (3 of 6)</label>" +
    "<input type=\"text\" id=\"brcVal2\" name=\"brcVal2\"/>" +

    // brcVal3.
    "<label for=\"bcrVal3\">Trigger on broadcast B-commands with this value in the (8-bit) data (4 of 6)</label>" +
    "<input type=\"text\" id=\"brcVal3\" name=\"brcVal3\"/>" +

    // brcVal4.
    "<label for=\"bcrVal4\">Trigger on broadcast B-commands with this value in the (8-bit) data (5 of 6)</label>" +
    "<input type=\"text\" id=\"brcVal4\" name=\"brcVal4\"/>" +

    // brcVal5.
    "<label for=\"bcrVal5\">Trigger on broadcast B-commands with this value in the (8-bit) data (6 of 6)</label>" +
    "<input type=\"text\" id=\"brcVal5\" name=\"brcVal5\"/>" +

    "</fieldset>" +

    "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
    "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +

    "</form>" +
    "</div>");

    var dialog = jQuery("#dialog_form_ttcspy").dialog({
      autoOpen: false,
      autoResize: true,
      height: "auto",
      width: "40%",
      modal: true,
      resizable: false,
      buttons: {
        "Submit": function() {
          configureTTCSpy();
          dialog.dialog("close");
        },
        Cancel: function() {
          dialog.dialog("close");
        }
      },
      close: function() {
        form[0].reset();
        //allFields.removeClass("ui-state-error");
      }
    });

    var form = dialog.find("form").on("submit", function(event) {
      event.preventDefault();
      event.stopPropagation();
      configureTTCSpy();
      dialog.dialog("close");
    });

    jQuery("#button_configure_ttcspy").button().on("click", function() {
      // Fill in the current values, based on the data we have.
      const data = pixel.data;

      var loggingMode = data['itemset-ttcspylogging']['Logging mode'];

      var triggerTermCombinationOperator = data['itemset-ttcspytrigger']['Logging trigger-term combination operator'];
      var l1a = data['itemset-ttcspytrigger']['Trigger on L1As'];
      var brcAll = data['itemset-ttcspytrigger']['Trigger on all broadcast B-commands'];
      var addAll = data['itemset-ttcspytrigger']['Trigger on all addressed B-commands'];
      var brcBC0 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with the BC0 bit set'];
      var brcEC0 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with the EC0 bit set'];
      var brcDDDD = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the user-data bits (if non-zero)'];
      var brcTT = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the test-data bits (if non-zero)'];
      var brcDDDDAll = data['itemset-ttcspytrigger']['Trigger on all broadcast B-commands with non-zero user-data bits'];
      var brcTTAll = data['itemset-ttcspytrigger']['Trigger on all broadcast B-commands with non-zero test-data bits'];
      var brcZeroData = data['itemset-ttcspytrigger']['Trigger on all broadcast B-commands with zero data payload'];
      var adrZeroData = data['itemset-ttcspytrigger']['Trigger on all addressed B-commands with zero data payload'];
      var errCom = data['itemset-ttcspytrigger']['Trigger on all communication errors'];
      var brcVal0 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (1 of 6)'];
      var brcVal1 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (2 of 6)'];
      var brcVal2 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (3 of 6)'];
      var brcVal3 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (4 of 6)'];
      var brcVal4 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (5 of 6)'];
      var brcVal5 = data['itemset-ttcspytrigger']['Trigger on broadcast B-commands with this value in the (8-bit) data (6 of 6)'];
      jQuery("#loggingMode").val(loggingMode);
      jQuery("#triggerTermCombinationOperator").val(triggerTermCombinationOperator);
      jQuery("#l1a").prop("checked", l1a=="true");
      jQuery("#brcAll").prop("checked", brcAll=="true");
      jQuery("#addAll").prop("checked", addAll=="true");
      jQuery("#brcBC0").prop("checked", brcBC0=="true");
      jQuery("#brcEC0").prop("checked", brcEC0=="true");
      jQuery("#brcDDDD").val(brcDDDD);
      jQuery("#brcTT").val(brcTT);
      jQuery("#brcDDDDAll").prop("checked", brcDDDDAll=="true");
      jQuery("#brcTTAll").prop("checked", brcTTAll=="true");
      jQuery("#brcZeroData").prop("checked", brcZeroData=="true");
      jQuery("#adrZeroData").prop("checked", adrZeroData=="true");
      jQuery("#errCom").prop("checked", errCom=="true");
      jQuery("#brcVal0").val(brcVal0);
      jQuery("#brcVal1").val(brcVal1);
      jQuery("#brcVal2").val(brcVal2);
      jQuery("#brcVal3").val(brcVal3);
      jQuery("#brcVal4").val(brcVal4);
      jQuery("#brcVal5").val(brcVal5);

      // Open the dialog.
      dialog.dialog("open");
    });
  }


  function fixupTKFecConfigOptButton(hook)
  {
    hook.append("<br/>");
    hook.append("<button id=\"button_configOptions_tkfec\">Modify TK FEC configuration</button>");
    hook.append("<button id=\"button_startWork_tkfec\">Start workloops</button>");
    hook.append("<button id=\"button_stopWork_tkfec\">Stop workloops</button>");
    // setTimeout(function () {
    //   //Trying to gray out Configure options based on current state, this doesn't work
    //   var fsmState = document.querySelector("#itemset-application-state > div.target").textContent;
    //   console.log("STATEHERE: " + fsmState);
    //   jQuery("#button_configOptions_tkfec").prop("disabled", true);
    //   if (fsmState=="Initial" || fsmState=="Halted" || fsmState=="Configured") jQuery("#button_configOptions_tkfec").prop("disabled", false);
    // }, 1700);

    jQuery("body").append("<div id=\"dialog_form_tkfecConfig\" title=\"Modify TK FEC configuration\">" +
    "<form>" +


    "<fieldset>" +
    "<legend>Workloop configuration:</legend>" +


    // doDCULoop
    "<label for=\"doDCULoop\">Do DCU Loop?</label>" +
    "<input type=\"checkbox\" id=\"doDCULoop\" name=\"doDCULoop\"/>" +

    // readDCU_workloop_sleep
    "<label for=\"readDCU_workloop_sleep\">Read DCU workloop sleep (us)</label>" +
    "<input type=\"number\" min=\"0\" id=\"readDCU_workloop_sleep\" name=\"readDCU_workloop_sleep\"/>" +

    // doCCUCheckLoop
    "<label for=\"doCCUCheckLoop\">Do CCU Check loop?</label>" +
    "<input type=\"checkbox\" id=\"doCCUCheckLoop\" name=\"doCCUCheckLoop\"/>" +

    // checkCCU_workloop_sleep
    "<label for=\"checkCCU_workloop_sleep\">Check CCU workloop sleep</label>" +
    "<input type=\"number\" min=\"0\" id=\"checkCCU_workloop_sleep\" name=\"checkCCU_workloop_sleep\"/>" +

    // workloopContinue
    "<label for=\"workloopContinue\">Continue workloop?</label>" +
    "<input type=\"checkbox\" id=\"workloopContinue\" name=\"workloopContinue\"/>" +

    // workloopContinueCCU
    "<label for=\"workloopContinueCCU\">Continue CCU workloop?</label>" +
    "<input type=\"checkbox\" id=\"workloopContinueCCU\" name=\"workloopContinueCCU\"/>" +

    // workloopContinueRC
    "<label for=\"workloopContinueRC\">Continue RC workloop</label>" +
    "<input type=\"checkbox\" id=\"workloopContinueRC\" name=\"workloopContinueRC\"/>" +


    "<legend>Other configuration:</legend>" +
    // skip_lv_check
    "<label for=\"skip_lv_check\">Ignore detector response? (skip_lv_check)</label>" +
    "<input type=\"checkbox\" id=\"skip_lv_check\" name=\"skip_lv_check\"/>" +

    // extraTimers
    "<label for=\"extraTimers\">Extra timers?</label>" +
    "<input type=\"checkbox\" id=\"extraTimers\" name=\"extraTimers\"/>" +

    // debugBool
    "<label for=\"debugBool\">Debug? Mostly print statements</label>" +
    "<input type=\"checkbox\" id=\"debugBool\" name=\"debugBool\"/>" +

    // do_force_ccu_readout
    "<label for=\"do_force_ccu_readout\">Force CCU readout?</label>" +
    "<input type=\"checkbox\" id=\"do_force_ccu_readout\" name=\"do_force_ccu_readout\"/>" +

    // resume_do_portcard_recovery
    "<label for=\"resume_do_portcard_recovery\">Resume portcard recovery?</label>" +
    "<input type=\"checkbox\" id=\"resume_do_portcard_recovery\" name=\"resume_do_portcard_recovery\"/>" +

    // ser_do_portcard_recovery
    "<label for=\"ser_do_portcard_recovery\">SER Do portcard recovery?</label>" +
    "<input type=\"checkbox\" id=\"ser_do_portcard_recovery\" name=\"ser_do_portcard_recovery\"/>" +

    // ser_do_portcard_recovery
    "<label for=\"ser_do_powercycle_dcdc\">SER Powercycle DCDCs?</label>" +
    "<input type=\"checkbox\" id=\"ser_do_powercycle_dcdc\" name=\"ser_do_powercycle_dcdc\"/>" +

    "</fieldset>" +

    "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
    "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +

    "</form>" +
    "</div>");

    jQuery("body").append("<div id=\"dialog_startWork_tkfec\" title=\"Start workloops\">" +
    "<form>" + "<fieldset>" +
    "<p>Are you absolutely sure you want to <b>start the workloops</b>?</p>" +
    "</fieldset>" +
    "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
    "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +
    "</form>" + "</div>");

    jQuery("body").append("<div id=\"dialog_stopWork_tkfec\" title=\"Stop workloops\">" +
    "<form>" + "<fieldset>" +
    "<p>Are you absolutely sure you want to <b>stop the workloops</b>?</p>" +
    "</fieldset>" +
    "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
    "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +
    "</form>" + "</div>");

    var dialog = jQuery("#dialog_form_tkfecConfig").dialog({
      autoOpen: false,
      autoResize: true,
      height: "auto",
      width: "40%",
      modal: true,
      resizable: false,
      buttons: {
        "Submit": function() {
          configureOptionsTKFEC();
          dialog.dialog("close");
        },
        Cancel: function() {
          dialog.dialog("close");
        }
      },
      close: function() {
        form[0].reset();
        //allFields.removeClass("ui-state-error");
      }
    });

    var form = dialog.find("form").on("submit", function(event) {
      event.preventDefault();
      event.stopPropagation();
      configureOptionsTKFEC();
      dialog.dialog("close");
    });

    jQuery("#button_configOptions_tkfec").button().on("click", function() {
      dialog.dialog("open");

      // Fill in the current values, based on the data we have.
      const data = pixel.data;

      var doDCULoop = data['tkfec-config-bools']['DCU Loop'];
      var doCCUCheckLoop = data['tkfec-config-bools']['Check CCU Loop'];
      var workloopContinue = data['tkfec-config-bools']['Continue Workloop'];
      var workloopContinueCCU = data['tkfec-config-bools']['Continue CCU Workloop'];
      var workloopContinueRC = data['tkfec-config-bools']['Continue RC Workloop'];
      var debugBool = data['tkfec-config-bools']['Debugging bool'];
      var skip_lv_check = data['tkfec-config-bools']['Ignore Detector Response'];
      var do_force_ccu_readout = data['tkfec-config-bools']['Force CCU Readout'];
      var resume_do_portcard_recovery = data['tkfec-config-bools']['Resume Portcard Recovery'];
      var ser_do_portcard_recovery = data['tkfec-config-bools']['SER Do Portcard Recovery'];
      var ser_do_powercycle_dcdc = data['tkfec-config-bools']['SER Do Powercycle DCDC'];
      var extraTimers = data['tkfec-config-bools']['Extra timers'];
      var readDCU_workloop_sleep = data['tkfec-config-bools']['Read DCU Workloop sleep'];
      var checkCCU_workloop_sleep = data['tkfec-config-bools']['Check CCU Workloop sleep'];


      jQuery("#doDCULoop").prop("checked", doDCULoop=="true");
      jQuery("#doCCUCheckLoop").prop("checked", doCCUCheckLoop=="true");
      jQuery("#workloopContinue").prop("checked", workloopContinue=="true");
      jQuery("#workloopContinueCCU").prop("checked", workloopContinueCCU=="true");
      jQuery("#workloopContinueRC").prop("checked", workloopContinueRC=="true");
      jQuery("#debugBool").prop("checked", debugBool=="true");
      jQuery("#skip_lv_check").prop("checked", skip_lv_check=="true");
      jQuery("#do_force_ccu_readout").prop("checked", do_force_ccu_readout=="true");
      jQuery("#resume_do_portcard_recovery").prop("checked", resume_do_portcard_recovery=="true");
      jQuery("#ser_do_portcard_recovery").prop("checked", ser_do_portcard_recovery=="true");
      jQuery("#ser_do_powercycle_dcdc").prop("checked", ser_do_powercycle_dcdc=="true");
      jQuery("#extraTimers").prop("checked", extraTimers=="true");
      jQuery("#readDCU_workloop_sleep").val(readDCU_workloop_sleep);
      jQuery("#checkCCU_workloop_sleep").val(checkCCU_workloop_sleep);

      // Open the dialog.
      dialog.dialog("open");
    });

    var dialogStart = jQuery("#dialog_startWork_tkfec").dialog({
      autoOpen: false, autoResize: true, height: "auto", width: "40%",
      modal: true, resizable: false,
      buttons: {
        "Yes I'm sure": function() {
          fsmSendSOAP("StartWorkloops");
          dialogStart.dialog("close");
        },
        Cancel: function() {
          dialogStart.dialog("close");
        }
      },
      close: function() {
        form2[0].reset();
      }
    });

    var form2 = dialog.find("form2").on("Yes I'm sure", function(event) {
      event.preventDefault();
      event.stopPropagation();
      fsmSendSOAP("StartWorkloops");
      dialogStart.dialog("close");
    });

    jQuery("#button_startWork_tkfec").button().on("click", function() {
      dialogStart.dialog("open");
    });

    var dialogStop = jQuery("#dialog_stopWork_tkfec").dialog({
      autoOpen: false, autoResize: true, height: "auto", width: "40%",
      modal: true, resizable: false,
      buttons: {
        "Yes I'm sure": function() {
          fsmSendSOAP("StopWorkloops");
          dialogStop.dialog("close");
        },
        Cancel: function() {
          dialogStop.dialog("close");
        }
      },
      close: function() {
        form3[0].reset();
      }
    });

    var form3 = dialog.find("form3").on("Yes I'm sure", function(event) {
      event.preventDefault();
      event.stopPropagation();
      fsmSendSOAP("StopWorkloops");
      dialogStop.dialog("close");
    });

    jQuery("#button_stopWork_tkfec").button().on("click", function() {
      dialogStop.dialog("open");
    });


  }
  //-----------------------------------------------------------------------------

  function selectSubsetOfLOGMessagesByType(hook, id_logging_table) {
    hook.append("<br/>");
    hook.append("<button style=\"margin:5px;\" id=\"button_log_type\">Filter by <b>Types</b></button>");

    var msg_types = ["INFO", "WARN", "ERROR", "FATAL", "DEBUG"],
        checkboxes = "";

    for (let type of msg_types)
        checkboxes += "<input style=\"display: inline;\" type=\"checkbox\" id=\""+type+"\" name=\""+type+"\">" +
                      "<label style=\"display: inline;\" for=\""+type+"\">\t "+type+"</label><br>";
        //soapMsg += " " + attr.name + "=\"" + attr.value + "\"";

    jQuery("body").append("<div id=\"dialog_form_log_type\" title=\"Types\">" +
    "<form>" +

    "<fieldset>" +
    //"<legend>Workloop configuration:</legend>" +

    checkboxes +
    "<br><hr style=\"border-width:2\"><br>" +
    "<input style=\"display: inline;\" type=\"checkbox\" id=\"ALL\" name=\"ALL\">" +
    "<label style=\"display: inline;\" for=\"ALL\">\t ALL </label><br>" +

    "</fieldset>" +

    "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
    "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-100px\">" +

    "</form>" +
    "</div>");

    var dialog = jQuery("#dialog_form_log_type").dialog({
      autoOpen: false,
      autoResize: true,
      height: "auto",
      width: "auto",
      modal: true,
      resizable: false,
      buttons: {
        "Filter": function() {
          filterByMsgTypes(msg_types, id_logging_table);
          dialog.dialog("close");
        },
        Cancel: function() {
          dialog.dialog("close");
        }
      },
      close: function() {
        form[0].reset();
        //allFields.removeClass("ui-state-error");
      }
    });

    var form = dialog.find("form").on("filter", function(event) {
      event.preventDefault();
      event.stopPropagation();
      filterByMsgTypes(msg_types, id_logging_table);
      dialog.dialog("close");
    });

    jQuery("#button_log_type").button().on("click", function() {
      // Open the dialog.
      dialog.dialog("open");
    });
  }

  //-----------------------------------------------------------------------------

  // Display the log messages whose type has been marked.
  function filterByMsgTypes(msg_types, id_logging_table) {
    var table = document.getElementById(id_logging_table);

    if (jQuery("#ALL").is(":checked")) { // Display all messages
      for (let row of table.getElementsByTagName("tr"))
        row.style.display = "";
    }
    else { // Display the messages whose type has been marked.
      // Store the checked types.
      var types_to_display = [];
      for (let type of msg_types)
        if (jQuery("#"+type).is(":checked"))
          types_to_display.push(type);

      // Display messages whose type is in msg_types.
      for (let row of table.getElementsByTagName("tr")) {
        var col_msg_type = row.getElementsByTagName("td")[1];
        if (col_msg_type) {
          msg_type_without_spaces = col_msg_type.innerText.replace(/\s/g, '');

          //if (!msg_type_without_spaces && row.style.display == "none")
            //break;
          if (msg_type_without_spaces)
            row.style.display = types_to_display.includes(msg_type_without_spaces) ? "" : "none";
          else
            break;
        }
      }
    }
  }

  //-----------------------------------------------------------------------------
  // Look for log messages within a specific time period.
  function selectSubsetOfMessagesByTimestamp(hook, id_table) {
    ////hook.append("<br/>");
    hook.append("<button style=\"margin:5px;\" id=\"button_log_timestamp\">Search by <b>Time Period</b></button>");


    jQuery("body").append("<div id=\"dialog_form_log_timestamp\" title=\"Time Period\">" +
    "<form>" +

    "<fieldset>" +

    "<input style=\"display: inline;\"" +
      "onclick=\"document.getElementById('to-date').disabled=document.getElementById('to-time').disabled=this.checked;\"" +
      "type=\"checkbox\" id=\"LastLogMessages\">" +
    "<label style=\"display: inline;\" for=\"LastLogMessages\">\t Display the latest messages </label><br><br>" +

    "<label style=\"display: inline;\" for=\"from\">from </label>" +
    //"<input style=\"display: inline;\" type=\"text\" id=\"from\" name=\"from\" placeholder=\"11 Jun 2020 16:10:51.369\" disabled=\"disabled\"> " +
    "<input type=\"date\" id=\"from-date\" name=\"from-date\">" +
    "<input type=\"time\" id=\"from-time\" step=\"0.001\"></input>" +

    "<label style=\"display: inline;\" for=\"to\"> to </label>" +
    //"<input style=\"display: inline;\" type=\"text\" id=\"to\" name=\"to\" placeholder=\"11 Jun 2020 16:10:52.000\"><br><br>" +
    "<input type=\"date\" id=\"to-date\" name=\"from-date\">" +
    "<input type=\"time\" id=\"to-time\" step=\"0.001\"></input>" +

    "</fieldset>" +

    "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
    "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-100px\">" +

    "</form>" +
    "</div>");

    // By default
    var now = new Date(),
        dd = now.getDate(),
        mm = now.getMonth()+1, //January is 0!
        yyyy = now.getFullYear(),
        HH = now.getHours(),
        MM = now.getMinutes(),
        SS = now.getSeconds();
    if(dd<10) dd='0'+dd;
    if(mm<10) mm='0'+mm;
    if(HH<10) HH='0'+HH;
    if(MM<10) MM='0'+MM;
    if(SS<10) SS='0'+SS;

    today = yyyy+'-'+mm+'-'+dd;
    now = HH+':'+MM+':'+SS;
    document.getElementById("from-date").setAttribute("max", today);
    document.getElementById("from-date").setAttribute("value", today);
    document.getElementById("from-time").setAttribute("value", now);
    document.getElementById("to-date").setAttribute("max", today);
    document.getElementById("to-date").setAttribute("value", today);
    document.getElementById("to-time").setAttribute("value", now);

    var dialog = jQuery("#dialog_form_log_timestamp").dialog({
      autoOpen: false,
      autoResize: true,
      height: "auto",
      width: "auto",
      modal: true,
      resizable: false,
      buttons: {
        "Search": function() {
          searchByTimePeriod(id_table);
          dialog.dialog("close");
        },
        Cancel: function() {
          dialog.dialog("close");
        }
      },
      close: function() {
        form[0].reset();
        //allFields.removeClass("ui-state-error");
      }
    });

    var form = dialog.find("form").on("search", function(event) {
      event.preventDefault();
      event.stopPropagation();
      searchByTimePeriod(id_table);
      dialog.dialog("close");
    });

    jQuery("#button_log_timestamp").button().on("click", function() {
      document.getElementById('to-date').disabled=document.getElementById('to-time').disabled=false;
      // Open the dialog.
      dialog.dialog("open");
    });
  }

  function searchByTimePeriod(id_table) {
    const table = document.getElementById(id_table),
          start_datetime = new Date(document.getElementById("from-date").value + ' ' +
                                    document.getElementById("from-time").value);

    var rows = table.getElementsByTagName("tr"),
        i = 0;

    if (!document.getElementById("LastLogMessages").checked) {
      const end_datetime = new Date(document.getElementById("to-date").value + ' ' +
                                    document.getElementById("to-time").value);
      // Hide all the message before the first datetime in the interval
      for (let row of rows) {
        var timestamp = row.getElementsByTagName("td")[0];

        if (timestamp) {
          var textTS = timestamp.textContent || timestamp.innerText,
              datetime = new Date(textTS.substr(0, 21));
              datetime.setMilliseconds(textTS.substr(22));

          if (datetime > end_datetime)
            row.style.display = "none";
          else {
            row.style.display = "";
            break;
          }
        }
        ++i;
      }
    }

    // Display all the message before the last datetime in the interval
    for (let row of Array.from(rows).slice(i)) {
      var timestamp = row.getElementsByTagName("td")[0];

      if (timestamp) {
        var textTS = timestamp.textContent || timestamp.innerText,
            datetime = new Date(textTS.substr(0, 21));
            datetime.setMilliseconds(textTS.substr(22));

        if (start_datetime <= datetime)
          row.style.display = "";
        else {
          row.style.display = "none";
          break;
        }
      }

      ++i;
    };

    // Hide all other messages
    for (let row of Array.from(rows).slice(i+1)) {
      row.style.display = "none";
    };
  }

  //-----------------------------------------------------------------------------

  function fixupRandomRateConfigButton(hook)
  {
    hook.append("<br/>");
    hook.append("<button id=\"button_configure_randomrate\">Modify random-trigger rate</button>");

    jQuery("body").append("<div id=\"dialog_form_randomrate\" title=\"Modify random-trigger rate\">" +
    "<form>" +

    "<fieldset>" +

    "<label for=\"frequency\">Requested rate (Hz)</label>" +
    "<input type=\"number\" min=\"0\" id=\"frequency\" name=\"frequency\"/>" +

    "</fieldset>" +

    "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
    "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +

    "</form>" +
    "</div>");

    var dialog = jQuery("#dialog_form_randomrate").dialog({
      autoOpen: false,
      autoResize: true,
      height: "auto",
      width: "40%",
      modal: true,
      resizable: false,
      buttons: {
        "Submit": function() {
          configureRandomRate();
          dialog.dialog("close");
        },
        Cancel: function() {
          dialog.dialog("close");
        }
      },
      close: function() {
        form[0].reset();
        //allFields.removeClass("ui-state-error");
      }
    });

    var form = dialog.find("form").on("submit", function(event) {
      event.preventDefault();
      event.stopPropagation();
      configureRandomRate();
      dialog.dialog("close");
    });

    jQuery("#button_configure_randomrate").button().on("click", function() {
      // Fill in the current values.
      var frequency = pixel.data['itemset-random-trigger']['Requested random-trigger rate (Hz)'];
      jQuery("#frequency").val(frequency);

      // Open the dialog.
      dialog.dialog("open");
    });
  }


  function grabChosenGlobalKey() //This sends a SOAP message of "Configure" with the selected GlobalKey
{
    var configAliasKey = jQuery("#configAliasKey").val();

    attributes = [
      {
        "name" : "xdaq:actionRequestorId",
        "value" : "dummy_session"
      },
      {
        "name" : "GlobalKey",
        "value" : configAliasKey
      },
    ];


    var soapMsg = makeSOAPCommand("Configure", attributes);

    jQuery.ajax({
      type: "post",
      url: pixel.applicationUrl,
      contentType: "text/xml",
      dataType: "xml",
      data: soapMsg,
      processData: false,
      success: function(data, textStatus, req) {
        var fault = jQuery(data).find("Fault");
        if (fault.length)
        {
          var msg = "Something went wrong";
          var tmpFaultString = fault.find("faultstring");
          var tmpDetail = fault.find("detail");
          if (!tmpFaultString.size() && !tmpDetail)
          {
            msg = msg + ", but it is not clear exactly what."
          }
          else
          {
            if (tmpFaultString.size())
            {
              msg = msg + ": " + tmpFaultString.text();
            }
            if (tmpDetail.size())
            {
              if (tmpFaultString.size())
              {
                msg = msg + ": ";
              }
              msg += tmpDetail.text();
            }
          }
          alert(msg);
        }
      },
      error: function(data, status, req) {
        alert(req.responseText + " " + status);
      }
    });
    // document.location.reload();
  }

  function returnPrettyAlias(){
    var htmlDialog = "<div id=\"dialog_form_configurefsm\" title=\"Global Key Choice\">" +

        "<p>NOTE: Available configuration aliases: <br> </p>" +
        "<table style=\"width:100%\"" +

        "<tr> <th>Alias</th> <th>Key</th> </tr> " +
        "<tr> <td>ChannelMapTest</td> <td>0</td> </tr>" +
        "<tr> <td>POHBias</td> <td>1</td> </tr>" +
        "<tr> <td>Delay25</td> <td>2</td> </tr>" +
        "<tr> <td>CalDel</td> <td>3</td> </tr>" +
        "<tr> <td>VcThrCalDel</td> <td>4</td> </tr>" +
        "<tr> <td>PixelAlive</td> <td>5</td> </tr>" +
        "<tr> <td>PixelAliveMaskAllPixels</td> <td>6</td> </tr>" +
        "<tr> <td>PixelAliveDontMaskAllPixels</td> <td>7</td> </tr>" +
        "<tr> <td>HotPixels</td> <td>8</td> </tr>" +
        "<tr> <td>TBMDelayWithScores</td> <td>9</td> </tr>" +
        "<tr> <td>TBMDelayWithScores_hityes_or</td> <td>10</td> </tr>" +
        "<tr> <td>TBMDelayWithScores_nohit_or</td> <td>11</td> </tr>" +
        "<tr> <td>ROCDelayA</td> <td>12</td> </tr>" +
        "<tr> <td>ROCDelayB</td> <td>13</td> </tr>" +
        "<tr> <td>SCurve</td> <td>14</td> </tr>" +
        "<tr> <td>SCurve99By3</td> <td>15</td> </tr>" +
        "<tr> <td>SCurve_faster</td> <td>16</td> </tr>" +
        "<tr> <td>BB</td> <td>17</td> </tr>" +
        "<tr> <td>VcThrCalibration</td> <td>18</td> </tr>" +
        "<tr> <td>VcThrCalDelFIFO3</td> <td>19</td> </tr>" +
        "<tr> <td>CalDelCalibration</td> <td>20</td> </tr>" +
        "<tr> <td>PHOptimization</td> <td>21</td> </tr>" +
        "<tr> <td>GainCalibration</td> <td>22</td> </tr>" +
        "<tr> <td>TrimDefaultShort_JMT</td> <td>23</td> </tr>" +
        "<tr> <td>TrimVcThrShort_JMT</td> <td>24</td> </tr>" +
        "<tr> <td>TrimVtrimShort_JMT</td> <td>25</td> </tr>" +
        "<tr> <td>TrimDefault_JMT</td> <td>26</td> </tr>" +
        "<tr> <td>Readback</td> <td>27</td> </tr>" +
        "<tr> <td>Readback_ScanVana</td> <td>28</td> </tr>" +
        "<tr> <td>Readback_AlwaysAllRocs</td> <td>29</td> </tr>" +
        "<tr> <td>Readback_ScanVanaAlwaysAllRocs</td> <td>30</td> </tr>" +
        "<tr> <td>TBMDelay</td> <td>31</td> </tr>" +
        "<tr> <td>TBMDelayBPIX</td> <td>32</td> </tr>" +
        "<tr> <td>SCurve4ThrMin</td> <td>33</td> </tr>" +

        "</table>" +

        "<form>" + "<fieldset>" +

        "<label for=\"configAliasKey\">Input Global Key Alias (int)</label>" +
        "<input type=\"number\" min=\"0\" id=\"configAliasKey\" name=\"configAliasKey\"/>" +

        "</fieldset>" +

        "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
        "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +

        "</form>" +"</div>";
    return htmlDialog;
  }


  //-----------------------------------------------------------------------------
  function fixupFSMButton(hook)
  {
    var htmlDialog = returnPrettyAlias();
    //jQuery("body").append(htmlDialog);

    setTimeout(function () {
      //var fsmState = document.querySelector("#itemset-application-state > div.target").textContent;
      var clickableButtons = document.querySelector("#allowedStatesItemset > div.target").textContent;
      var allButtons = document.querySelector("#allStatesItemset > div.target").textContent;

      jQuery("#allStatesItemset_table").hide(); jQuery("#allStatesItemset").hide(); jQuery("#allStatesItemset_table > div").hide();
      jQuery("#allowedStatesItemset_table").hide(); jQuery("#allowedStatesItemset").hide(); jQuery("#allowedStatesItemset_table > div").hide();

      // Toggle buttons depending on allowed state transitions (ignoring the "Done" states)
      var eachButtonAll = allButtons.split(', ');
      var eachButtonAllowed = clickableButtons.split(', ');
      for (i = 0; i < eachButtonAll.length; i++) {
        if (eachButtonAll[i].includes("Done")) continue;
        else {
          for (j = 0; j < eachButtonAllowed.length; j++) {
            jQuery("#button_"+eachButtonAll[i]+"_fsm").prop("disabled", true);
            if (eachButtonAllowed[j]==eachButtonAll[i]) {
              jQuery("#button_"+eachButtonAll[i]+"_fsm").prop("disabled", false); break;
            }
          }
        }
      }

      var dialog = jQuery("#dialog_form_configurefsm").dialog({
        autoOpen: false, autoResize: true, height: "auto",
        width: "40%", modal: true, resizable: false,
        buttons: {
          "Configure": function() {
            grabChosenGlobalKey(); //Currently, this just receives a manually-input global key and runs that... maybe I can display the alias and key map?
            dialog.dialog("close");
            // fsmSendSOAP("Configure");
          },
          Cancel: function() {
            dialog.dialog("close");
          }
        },
        close: function() {
          form[0].reset();
          //allFields.removeClass("ui-state-error");
        }
      });

      var form = dialog.find("form").on("submit", function(event) {
        event.preventDefault();
        event.stopPropagation();
        grabChosenGlobalKey(); //Currently, this just receives a manually-input global key and runs that... maybe I can display the alias and key map?
        dialog.dialog("close");
      });

      //possible to make a general jQuery for all buttons on a page
      // $("button").click(function(){
      // action goes here!!
      //});
      // jQuery("button").button().on("click", function() {
      //     dialog.dialog("open");
      //     //Need to add the SOAPSending thing here
      // });
      jQuery("#button_Initialize_fsm").button().on("click", function() {
        fsmSendSOAP("Initialize");
      });
      jQuery("#button_Configure_fsm").button().on("click", function() {
        dialog.dialog("open");
        // fsmSendSOAP("Configure"); //Configure should never be sent through here
      });
      jQuery("#button_Reconfigure_fsm").button().on("click", function() {
        dialog.dialog("open");
        // fsmSendSOAP("Reconfigure");
      });
      jQuery("#button_ColdReset_fsm").button().on("click", function() {
        fsmSendSOAP("ColdReset");
      });
      jQuery("#button_Start_fsm").button().on("click", function() {
        fsmSendSOAP("Start");
      });
      jQuery("#button_Pause_fsm").button().on("click", function() {
        fsmSendSOAP("Pause");
      });
      jQuery("#button_Resume_fsm").button().on("click", function() {
        fsmSendSOAP("Resume");
      });
      jQuery("#button_Stop_fsm").button().on("click", function() {
        fsmSendSOAP("Stop");
      });
      jQuery("#button_Halt_fsm").button().on("click", function() {
        fsmSendSOAP("Halt");
      });
      jQuery("#button_FixSoftError_fsm").button().on("click", function() {
        fsmSendSOAP("FixSoftError");
      });
      jQuery("#button_ResumeSoftError_fsm").button().on("click", function() {
        fsmSendSOAP("ResumeSoftError");
      });
    }, 1700);
  }


  function fixupSystemDumpButton(hook)
  {
    hook.after("<br><button id=\"button_system_dump\">Dump system state</button>");

    jQuery("body").append("<div id=\"dialog_form_dumpreason\" title=\"Specify reason for system-state dump\">" +

    "<p>NOTE: A full system dump takes quite a long time, " +
    "creates lots of output, " +
    "and generates several very large emails. " +
    "Please only use when necessary.</p>" +

    "<form>" +

    "<fieldset>" +

    "<label for=\"reason\">Reason for dump:</label>" +
    "<input type=\"string\" id=\"reason\" name=\"reason\" size=\"90%\"/>" +

    "</fieldset>" +

    "<!-- Allow form submission with keyboard without duplicating the dialog button -->" +
    "<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">" +

    "</form>" +
    "</div>");

    var dialog = jQuery("#dialog_form_dumpreason").dialog({
      autoOpen: false,
      autoResize: true,
      height: "auto",
      width: "40%",
      modal: true,
      resizable: false,
      buttons: {
        "Submit": function() {
          requestSystemDump();
          dialog.dialog("close");
        },
        Cancel: function() {
          dialog.dialog("close");
        }
      },
      close: function() {
        form[0].reset();
        //allFields.removeClass("ui-state-error");
      }
    });

    var form = dialog.find("form").on("submit", function(event) {
      event.preventDefault();
      event.stopPropagation();
      requestSystemDump();
      dialog.dialog("close");
    });

    jQuery("#button_system_dump").button().on("click", function() {
      // // Fill in the current values.
      // var frequency = pixel.data['itemset-random-trigger']['Requested random-trigger rate (Hz)'];
      // jQuery("#frequency").val(frequency);

      // Open the dialog.
      dialog.dialog("open");
    });
  }

  //-----------------------------------------------------------------------------

  function L1AHistos(container)
  {
    const bxMin = 1;
    const bxMax = 3564;
    const maxNumBins = 100;
    // Source: http://c0bra.github.io/color-scheme-js.
    const colors = ["#ff0000", "#b30000", "#ffbfbf", "#ff8080", "#00cc00", "#008f00", "#bfffbf", "#80ff80", "#330099", "#24006b", "#d5bfff", "#aa80ff", "#ffcc00", "#b38f00", "#fff2bf", "#ffe680"];
    // const colors = ["#ae0404", "#ae2604", "#ae4b04", "#ae6d04", "#ae9204", "#a8ae04", "#84ae04", "#62ae04", "#3dae04", "#1bae04", "#04ae13", "#04ae35", "#04ae59", "#04ae7b", "#04aea0", "#049aae"]

    const options = {series: {stack: true},
    bars: {show: true,
      fill: 1,
      align: "center",
      barWidth: 1},
    xaxis: {minTickSize: 1,
      tickDecimals: 0,
      tickLength: 0,
      axisLabel: "BX"},
    yaxis: {axisLabel: "L1A count",
        axisLabelPadding: 35,
        min: 0},
    legend: {show: false},
    selection: {mode: "x"},
    grid: {borderColor: "#cbcbcb"}
    };

    this.data = undefined;
    this.mainPlot = undefined;
    this.container = container
    this.xMin = bxMin;
    this.xMax = bxMax;

    this.init = function()
    {
      const tmp = "<div id=\"chart-main\" class=\"chart-placeholder\"></div>" +
      "<div id=\"legend\" class=\"chart-legend\"></div>";
      jQuery(this.container).html(tmp);
    }

      this.setData = function(dataRaw) {
        this.data = [];
        for (var i = 0; i != dataRaw.length; ++i) {
          if (this.sum(dataRaw[i].data) > 0) {
            var tmp = [];
            for (var j = 0; j != dataRaw[i].data.length; ++j) {
              tmp.push([j+1, dataRaw[i].data[j]]);
            }
            this.data.push({label: dataRaw[i].label, data: tmp, color: i});
          }
        }
      }

      this.plot = function()
      {
        // Create the plot, and then update it with the data.
        this.createPlots();
        this.update();

        // Fix the vertical position of the XDAQ page footer.
        xdaqHeaderFooterStick();
      }

      this.update = function() {
        var tmp = this.getData(this.xMin, this.xMax);
        var plotData = tmp[0];
        var rebinFactor = tmp[1];

        pixel.l1ahistos.mainPlot.getXAxes()[0].options.min = undefined;
        pixel.l1ahistos.mainPlot.getXAxes()[0].options.max = undefined;
        this.mainPlot.getOptions().series.bars.barWidth = rebinFactor;
        this.mainPlot.setData(plotData);
        this.mainPlot.setupGrid();
        this.mainPlot.draw();

        // Fix up an 'unzoom' button if needed.
        if ((this.xMin != bxMin) || (this.xMax != bxMax)) {
          var button = jQuery("#unzoomButton");
          if (button.length == 0) {
            jQuery("<div id='unzoomButton' class='button' style='right: 20px; top: 20px'>unzoom</div>").appendTo("#chart-main").click(jQuery.proxy(function (event) {
              event.preventDefault();
              this.xMin = bxMin;
              this.xMax = bxMax;
              this.update();
            },
            this));
          }
        }
        else {
          var button = jQuery("#unzoomButton");
          if (button.length > 0) {
            button.remove();
          }
        }
      }

        this.getData = function(xMin, xMax) {
        // Create a sliced copy of the part of the data we want.
        var dataTmp = [];
        var tmpObj;
        var tmpDat;
        for (var i = 0; i != this.data.length; ++i) {
          tmpDat = this.data[i]["data"].slice(xMin - 1, xMax);
          tmpObj = jQuery.extend({}, this.data[i]);
          tmpObj.data = tmpDat;
          dataTmp.push(tmpObj);
        }

        // Determine if we need to rebin the data or not.
        const numBins = (xMax - xMin + 1);
        var rebinFactor;
        var dataCropped = [];
        if (numBins > maxNumBins) {
          const ratio = numBins / maxNumBins;
          rebinFactor = this.findDivisor(ratio, numBins);
          for (var i = 0; i != dataTmp.length; ++i) {
            tmpDat = this.rebinData(dataTmp[i]["data"], rebinFactor);
            tmpObj = jQuery.extend({}, dataTmp[i]);
            tmpObj.data = tmpDat;
            dataCropped.push(tmpObj);
          }
        }
        else
        {
          dataCropped = dataTmp;
          rebinFactor = 1;
        }
        return [dataCropped, rebinFactor];
      }

      this.rebinData = function(data, n) {
        // NOTE: The assumption is that n is a divisor of data.length.
        var dataRebinned = [];
        for (var i = 0; i < data.length; i += n)
        {
          var slice = data.slice(i, i + n);
          var tmp = slice.reduce(function(total, entry) {return total + entry[0];}, 0);
          var tmpX = tmp / slice.length;
          var tmpY = slice.reduce(function(total, entry) {return total + entry[1];}, 0);
          dataRebinned.push([tmpX, tmpY]);
        }
        return dataRebinned;
      }

      this.sum = function(values) {
        return values.reduce(function(a, b) {return a + b;})
      }

      this.findDivisor = function(val, n) {
        const divisors = this.getDivisors(n);
        var res;
        if (divisors.length > 2) {
          for (var i = 0; i < divisors.length; ++i) {
            if ((divisors[i + 1] >= val) && (divisors[i] < val)) {
              var tmpLo = Math.abs(val - divisors[i]);
              var tmpHi = Math.abs(val - divisors[i + 1]);
              if (tmpLo < tmpHi) { res = divisors[i]; }
              else { res = divisors[i + 1];}
              break;
            }
          }
        }
        else { res = 1; }
        return res;
      }

      this.getDivisors = function(n) {
        if (n < 1) {
          throw "ParameterError: expected N >= 1 but received N = " + n + ".";
        }
        var small = [];
        var large = [];
        const stop = Math.floor(Math.sqrt(n));
        for (var i = 1; i <= stop; ++i) {
          if ((n % i) == 0) {
            small.push(i);
            if ((i * i) != n) {
              large.push(n / i);
            }
          }
        }
        large.reverse();
        var res = small.concat(large);
        return res
      }

      this.createPlots = function() {
        // Create the main plot.
        this.mainPlot = jQuery.plot("#chart-main", [[]], jQuery.extend(true, {}, options, {
        legend: {show: true, container: "#legend"} }));

        function handleZoom(event, ranges){
          var from = Math.floor(ranges.xaxis.from);
          var to = Math.ceil(ranges.xaxis.to);

          // Clamp the overall range.
          if (from < bxMin) {
            from = bxMin;
          }
          if (to > bxMax) {
            to = bxMax;
          }

          // Clamp the zooming to prevent 'infinite' zoom.
          if ((to - from) < 1.) {
            to = from + 1;
          }

          // Store the zoom range for the next iteration.
          this.xMin = from;
          this.xMax = to;

          // Do the actual zooming.
          this.update();
          this.mainPlot.clearSelection();
        }

        // Now perform the required magic to bind the two plots together.
        jQuery("#chart-main").bind("plotselected", jQuery.proxy(handleZoom, this));
      }
    }

    //-----------------------------------------------------------------------------

    // A string cleaner copy-pasted from here:
    //   http://phpjs.org/functions/htmlspecialchars

    function htmlspecialchars(string, quote_style, charset, double_encode) {
      //       discuss at: http://phpjs.org/functions/htmlspecialchars/
      //      original by: Mirek Slugen
      //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      //      bugfixed by: Nathan
      //      bugfixed by: Arno
      //      bugfixed by: Brett Zamir (http://brett-zamir.me)
      //      bugfixed by: Brett Zamir (http://brett-zamir.me)
      //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      //         input by: Ratheous
      //         input by: Mailfaker (http://www.weedem.fr/)
      //         input by: felix
      // reimplemented by: Brett Zamir (http://brett-zamir.me)
      //             note: charset argument not supported
      //        example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
      //        returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
      //        example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
      //        returns 2: 'ab"c&#039;d'
      //        example 3: htmlspecialchars('my "&entity;" is still here', null, null, false);
      //        returns 3: 'my &quot;&entity;&quot; is still here'

      var optTemp = 0,
      i = 0,
      noquotes = false;
      if (typeof quote_style === 'undefined' || quote_style === null) {
        quote_style = 2;
      }
      string = string.toString();
      if (double_encode !== false) { // Put this first to avoid double-encoding
        string = string.replace(/&/g, '&amp;');
      }
      string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;');

      var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
      };
      if (quote_style === 0) {
        noquotes = true;
      }
      if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
        quote_style = [].concat(quote_style);
        for (i = 0; i < quote_style.length; i++) {
          // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
          if (OPTS[quote_style[i]] === 0) {
            noquotes = true;
          } else if (OPTS[quote_style[i]]) {
            optTemp = optTemp | OPTS[quote_style[i]];
          }
        }
        quote_style = optTemp;
      }
      if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/'/g, '&#039;');
      }
      if (!noquotes) {
        string = string.replace(/"/g, '&quot;');
      }

      return string;
    }

    //-----------------------------------------------------------------------------

    // Check if the browser considers this proper HTML. Overly strict, but
    // okay. For details, see:
    //   http://stackoverflow.com/questions/10026626/check-if-html-snippet-is-valid-with-javascript
    function isProperHTML(html)
    {
      var doc = document.createElement("div");
      doc.innerHTML = html;
      return (doc.innerHTML === html);
    }

    //-----------------------------------------------------------------------------

    function hackIntoHTML(text)
    {
      var res = jQuery("<div></div>").html(text).html();
      return res;
    }

    //-----------------------------------------------------------------------------

    // From php.js: http://phpjs.org/functions/htmlspecialchars
    function htmlspecialchars(string, quote_style, charset, double_encode) {
      //       discuss at: http://phpjs.org/functions/htmlspecialchars/
      //      original by: Mirek Slugen
      //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      //      bugfixed by: Nathan
      //      bugfixed by: Arno
      //      bugfixed by: Brett Zamir (http://brett-zamir.me)
      //      bugfixed by: Brett Zamir (http://brett-zamir.me)
      //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      //         input by: Ratheous
      //         input by: Mailfaker (http://www.weedem.fr/)
      //         input by: felix
      // reimplemented by: Brett Zamir (http://brett-zamir.me)
      //             note: charset argument not supported
      //        example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
      //        returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
      //        example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
      //        returns 2: 'ab"c&#039;d'
      //        example 3: htmlspecialchars('my "&entity;" is still here', null, null, false);
      //        returns 3: 'my &quot;&entity;&quot; is still here'

      var optTemp = 0, i = 0, noquotes = false;
      if (typeof quote_style === 'undefined' || quote_style === null) {
        quote_style = 2;
      }
      string = string.toString();
      if (double_encode !== false) { // Put this first to avoid double-encoding
        string = string.replace(/&/g, '&amp;');
      }
      string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;');

      var OPTS = {
        'ENT_NOQUOTES': 0,
        'ENT_HTML_QUOTE_SINGLE': 1,
        'ENT_HTML_QUOTE_DOUBLE': 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE': 4
      };
      if (quote_style === 0) {
        noquotes = true;
      }
      if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags
        quote_style = [].concat(quote_style);
        for (i = 0; i < quote_style.length; i++) {
          // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
          if (OPTS[quote_style[i]] === 0) {
            noquotes = true;
          } else if (OPTS[quote_style[i]]) {
            optTemp = optTemp | OPTS[quote_style[i]];
          }
        }
        quote_style = optTemp;
      }
      if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/'/g, '&#039;');
      }
      if (!noquotes) {
        string = string.replace(/"/g, '&quot;');
      }

      return string;
    }

    //-----------------------------------------------------------------------------

    // Some debug helper methods (copy-pasted from the internet
    // somewhere).

    function syntaxHighlight(json)
    {
      if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
      }
      json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
      return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
          if (/:$/.test(match)) {
            cls = 'key';
          } else {
            cls = 'string';
          }
        } else if (/true|false/.test(match)) {
          cls = 'boolean';
        } else if (/null/.test(match)) {
          cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
      });
    }

    function prettyPrint(obj)
    {
      return JSON.stringify(obj);
    }

    function getRandomInt(min, max)
    {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    //-----------------------------------------------------------------------------
