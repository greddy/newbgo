//-----------------------------------------------------------------------------

// Load some (more) stuff we need.

// NOTE: Only do this for PIXEL control applications. The check here is
// necessary because the call to
// xgi::framework::UIManager::setLayout() in the WebServer globally
// replaces the HyperDAQ layout for all applications in the XDAQ
// executive.

if (isPIXELApplication())
{
    // The splash screen.
    getScript(baseUrl + "/pixel/utils/html/please-wait/please-wait.min.js");
    getScript(baseUrl + "/pixel/utils/html/js/pixel_loading_screen.js");
    getCSS(baseUrl + "/pixel/utils/html/please-wait/please-wait.css");

    // JQuery friends.
    // NOTE: JQuery itself is already loaded by the XDAQ/HyperDAQ
    // framework JS.
    getScript(baseUrl + "/pixel/utils/html/jquery-ui/jquery-ui.min.js");
    getCSS(baseUrl + "/pixel/utils/html/jquery-ui/jquery-ui.css");

    // SlickGrid-related.
    getScript(baseUrl + "/pixel/utils/html/slickgrid/lib/jquery.event.drag-2.2.js");
    getScript(baseUrl + "/pixel/utils/html/slickgrid/slick.core.js");
    getScript(baseUrl + "/pixel/utils/html/slickgrid/slick.grid.js");
    getScript(baseUrl + "/pixel/utils/html/slickgrid/plugins/slick.rowselectionmodel.js");
    getScript(baseUrl + "/pixel/utils/html/slickgrid/plugins/slick.autotooltips.js");
    getCSS(baseUrl + "/pixel/utils/html/slickgrid/slick.grid.css");

    // PNotify.
    getScript(baseUrl + "/pixel/utils/html/pnotify/pnotify.custom.min.js");
    getCSS(baseUrl + "/pixel/utils/html/pnotify/pnotify.custom.min.css");

    // Flot.
    // NOTE: Loading order is important here.
    getScript(baseUrl + "/pixel/utils/html/flot/jquery.flot.js", true);
    getScript(baseUrl + "/pixel/utils/html/flot/jquery.flot.selection.js", true);
    getScript(baseUrl + "/pixel/utils/html/flot/jquery.flot.stack.js", true);
    getScript(baseUrl + "/pixel/utils/html/flot/jquery.flot.axislabels.js", true);

    // doT.
    getScript(baseUrl + "/pixel/utils/html/dot/doT.js");

    // PIXEL stuff.
    getCSS(baseUrl + "/pixel/utils/html/css/pixel.css");
}

//-----------------------------------------------------------------------------
