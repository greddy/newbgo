#include "pixel/utils/AppHistItem.h"

pixel::utils::AppHistItem::AppHistItem(std::string const &msg)
    : msg_(msg),
      count_(1) {
          timestamps_.push_back(toolbox::TimeVal::gettimeofday());
}

pixel::utils::AppHistItem::~AppHistItem() {
}

void
pixel::utils::AppHistItem::addOccurrence() {
    ++count_;
    timestamps_.push_back(toolbox::TimeVal::gettimeofday());
}

toolbox::TimeVal
pixel::utils::AppHistItem::timestamp() const {
    return timestamps_.front();
}

toolbox::TimeVal
pixel::utils::AppHistItem::timestampLastOccurrence() const {
    return timestamps_.back();
}

std::string
pixel::utils::AppHistItem::message() const {
    return msg_;
}

unsigned int
pixel::utils::AppHistItem::count() const {
    return count_;
}
