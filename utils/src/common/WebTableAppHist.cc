#include "pixel/utils/WebTableAppHist.h"

#include <cassert>
#include <sstream>
#include <string>
#include <vector>

#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"

pixel::utils::WebTableAppHist::WebTableAppHist(std::string const &name,
                                               std::string const &description,
                                               pixel::utils::Monitor const &monitor,
                                               std::string const &itemSetName,
                                               std::string const &tabName,
                                               size_t const colSpan)
    : pixel::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan) {
}

std::string
pixel::utils::WebTableAppHist::getHTMLString() const {
    // NOTE: This has been written to work with the new XDAQ12-style
    // HyperDAQ tabs and doT.js.

    std::stringstream res;

    res << "<div class=\"pixel-item-table-wrapper\">"
        << "\n";

    res << "<p class=\"pixel-item-table-title\">"
        << getName()
        << "</p>";
    res << "\n";

    res << "<p class=\"pixel-item-table-description\">"
        << getDescription()
        << "</p>";
    res << "\n";

    // NOTE: This one is kinda special. Mainly due to the fact that this
    // table can become quite long. Therefore this is handled
    // completely in JavaScript (with the help of a cool, virtual
    // scrolling grid plugin).
    res << "<div id=\"application-status-history-placeholder\"></div>"
        << "\n";

    res << "</div>"
        << "\n";

    return res.str();
}
