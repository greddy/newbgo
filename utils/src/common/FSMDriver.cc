#include "pixel/utils/FSMDriver.h"

#include <unistd.h>

#include "toolbox/string.h"
#include "toolbox/task/exception/Exception.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "xcept/Exception.h"

#include "pixel/exception/Exception.h"
#include "pixel/utils/XDAQAppWithFSMAutomatic.h"

pixel::utils::FSMDriver::FSMDriver(XDAQAppWithFSMAutomatic *const xdaqApp)
    : fsmDriverAlarmName_("FSMDriverAlarm"),
      xdaqAppP_(xdaqApp),
      timerP_(nullptr) {
          timerName_ = toolbox::toString("FSMDriverTimer_lid%d",
                                         xdaqAppP_->getApplicationDescriptor()->getLocalId());
}

pixel::utils::FSMDriver::~FSMDriver() {
    stop();
    try {
        toolbox::task::getTimerFactory()->removeTimer(timerName_);
    }
    catch (...) {
    }
    timerP_.reset();
}

void
pixel::utils::FSMDriver::start() {
    // If we get here and there is no timer (yet), we have to create it
    // first.
    if (!timerP_.get()) {
        try {
            timerP_ = std::unique_ptr<toolbox::task::Timer>(toolbox::task::getTimerFactory()->createTimer(timerName_));
            timerP_->addExceptionListener(this);
        }
        catch (toolbox::task::exception::Exception const &err) {
            // Raise the alarm.
            std::string const problemDesc =
                toolbox::toString("Failed to create FSMDriver timer: '%s'.", err.what());
            XCEPT_DECLARE(pixel::exception::FSMDriverFailureAlarm, alarmException, problemDesc);
            xdaqAppP_->raiseAlarm(fsmDriverAlarmName_, "error", alarmException);
        }
    }

    //----------

    toolbox::TimeInterval interval(kCheckInterval_, 0);
    toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
    try {
        timerP_->scheduleAtFixedRate(start, this, interval, 0, "");
    }
    catch (toolbox::task::exception::Exception const &err) {
        // Raise the alarm.
        std::string const problemDesc =
            toolbox::toString("Failed to start FSMDriver timer: '%s'.", err.what());
        XCEPT_DECLARE(pixel::exception::FSMDriverFailureAlarm, alarmException, problemDesc);
        xdaqAppP_->raiseAlarm(fsmDriverAlarmName_, "error", alarmException);
    }
}

void
pixel::utils::FSMDriver::stop() {
    if (timerP_.get() && timerP_->isActive()) {
        timerP_->stop();
    }
}

void
pixel::utils::FSMDriver::timeExpired(toolbox::task::TimerEvent &e) {
    std::string const stateName = xdaqAppP_->getCurrentStateName();
    // If the state name is 'Enabled', we're happy.
    if (stateName != "Enabled") {
        // If not, we want to take action. The action to be taken
        // depends on the current state.

        // NOTE: Just a little grace period here to make sure things
        // don't fly into an infinite loop.
        ::sleep(kGracePeriod_);

        if (stateName == "Failed") {
            xdaqAppP_->halt();
        } else if (stateName == "Halted") {
            xdaqAppP_->configure();
        } else if (stateName == "Configured") {
            xdaqAppP_->enable();
        }
    }
}

void
pixel::utils::FSMDriver::onException(xcept::Exception &err) {
    // Raise the alarm.
    std::string const problemDesc =
        toolbox::toString("Problem in FSMDriver: '%s'.", err.what());
    XCEPT_DECLARE(pixel::exception::FSMDriverFailureAlarm, alarmException, problemDesc);
    xdaqAppP_->raiseAlarm(fsmDriverAlarmName_, "error", alarmException);
}
