#include "pixel/utils/ApplicationStateInfoSpaceHandler.h"

#include <stdint.h>
#include <inttypes.h>

#include "xdaq/Application.h"
#include "xdaq/ApplicationContextImpl.h"

#include "toolbox/string.h"

#include "pixel/utils/InfoSpaceHandlerLocker.h"
#include "pixel/utils/InfoSpaceUpdater.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/OwnerId.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/WebServer.h"
#include "pixel/utils/WebTableAppHist.h"

std::string const pixel::utils::ApplicationStateInfoSpaceHandler::kAllOKString = "All OK";
std::string const pixel::utils::ApplicationStateInfoSpaceHandler::kNoProblemString = "-";

pixel::utils::ApplicationStateInfoSpaceHandler::ApplicationStateInfoSpaceHandler(xdaq::Application &xdaqApp,
                                                                                 pixel::utils::InfoSpaceUpdater *updater)
    : pixel::utils::InfoSpaceHandler(xdaqApp, "pixel-application-state", updater, xdaqApp.getApplicationInfoSpace()) {
    // NOTE: In order for RunControl to work, each application needs to
    // have a 'stateName' string in the default application
    // InfoSpace. Therefore this InfoSpaceHandler uses the built-in
    // 'mirroring' capabilities of the InfoSpaceHandler to copy its
    // contents into the default application InfoSpace.

    // These are _real_ application state variables.
    createString("stateName",
                 pixel::utils::InfoSpaceHandler::kUninitializedString,
                 "",
                 InfoSpaceItem::NOUPDATE,
                 true);
    createString("applicationState",
                 pixel::utils::InfoSpaceHandler::kUninitializedString,
                 "",
                 InfoSpaceItem::NOUPDATE,
                 true);
    createString("problemDescription",
                 pixel::utils::InfoSpaceHandler::kUninitializedString,
                 "",
                 InfoSpaceItem::NOUPDATE,
                 true);

    // The session ID of the RunControl session in charge of this
    // application. In the 'normal' case, in which there is a RunControl
    // session driving the current run, this also defines the hardware
    // lease owner. In 'special' cases where there is something else
    // driving the run, the rcmsSessionId should be set to zero, and the
    // hwLeaseOwnerId should be set to something adequately descriptive.
    createUInt32("rcmsSessionId", 0, "", InfoSpaceItem::NOUPDATE, true);

    // A string identifying the remote SOAP sender that owns the
    // hardware lease.
    // NOTE: This may be a RunControl session, or something else.
    createString("hwLeaseOwnerId", "", "", InfoSpaceItem::NOUPDATE, true);

    // A flag showing if we're in maintenance mode or in normal
    // operations mode.
    createBool("maintenanceMode", false, "", InfoSpaceItem::NOUPDATE, true);

    // Some time-keeping variables (for monitoring only, really).
    createDouble("upTime", 0., "time_interval", InfoSpaceItem::NOUPDATE, true);
    createTimeVal("latestMonitoringUpdate", 0., "", InfoSpaceItem::NOUPDATE, true);
    createDouble("latestMonitoringDuration", 0., "%.3f", InfoSpaceItem::NOUPDATE, true);

    // Keep track of what happened.
    createString("history",
                 pixel::utils::InfoSpaceHandler::kUninitializedString,
                 "",
                 InfoSpaceItem::NOUPDATE,
                 true);

    //----------

    // Update the session ID attached to the application descriptor.
    // NOTE: The main reason this is done here already is that (the
    // first time around) this also _creates_ the property (if it does
    // not exist yet).
    updateSessionIdInApplicationDescriptor();
}

pixel::utils::ApplicationStateInfoSpaceHandler::~ApplicationStateInfoSpaceHandler() {
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::initialize() {
    InfoSpaceHandlerLocker(this);
    // lock();
    problems_.clear();
    updateStatusDescription();
    // unlock();
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::update() {
    // First we do everything that we would normally do.
    pixel::utils::InfoSpaceHandler::update();

    // And then we do something ugly afterwards: propagate the RCMS
    // session ID to the application context.
    updateSessionIdInApplicationDescriptor();
}

std::string
pixel::utils::ApplicationStateInfoSpaceHandler::formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const &item) const {
    std::string res = pixel::utils::escapeAsJSONString(pixel::utils::InfoSpaceHandler::kInvalidItemString);
    if (item->isValid()) {
        std::string name = item->name();
        if (name == "history") {
            // Special in the sense that this is something that needs to
            // be interpreted as a proper JavaScript object, so let's
            // not add any more double quotes.
            res = getString(name);
        } else if (name == "maintenanceMode") {
            bool const value = getBool(name);
            if (value) {
                res = "Maintenance mode";
            } else {
                res = "Operations mode";
            }
            res = pixel::utils::escapeAsJSONString(res);
        } else {
            // For everything else simply call the generic formatter.
            res = InfoSpaceHandler::formatItem(item);
        }
    }
    return res;
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::addProblem(std::string const &keyword,
                                                           std::string const &problemDescription) {
    InfoSpaceHandlerLocker(this);
    // lock();
    problems_[keyword] = problemDescription;
    updateStatusDescription();
    // unlock();
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::removeProblem(std::string const &keyword) {
    InfoSpaceHandlerLocker(this);
    // lock();
    problems_.erase(keyword);
    updateStatusDescription();
    // unlock();
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::addHistoryItem(std::string const &desc) {
    InfoSpaceHandlerLocker(this);
    // lock();
    // Keep track of the underlying history items first.
    history_.addItem(desc);

    // Update the history string used for the table.
    std::string const histString = history_.getJSONString();
    setString("history", histString);
    writeInfoSpace();

    // unlock();
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::setFSMState(std::string const &stateDescription,
                                                            std::string const &problemDescription) {
    std::string const keyword = "FSM transition problem";
    InfoSpaceHandlerLocker(this);
    // lock();
    setString("stateName", stateDescription);
    if (problemDescription != kNoProblemString) {
        addProblem(keyword, problemDescription);
    } else {
        removeProblem(keyword);
    }
    // unlock();
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::setOwnerId(pixel::utils::OwnerId const &ownerId) {
    setUInt32("rcmsSessionId", ownerId.rcmsSessionId());
    setString("hwLeaseOwnerId", ownerId.sessionName());
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::addMonitoringProblem(std::string const &problemDescription) {
    std::string const keyword = "Monitoring problem";
    addProblem(keyword, problemDescription);
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::removeMonitoringProblem() {
    std::string const keyword = "Monitoring problem";
    removeProblem(keyword);
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::setApplicationState(std::string const &stateDescription) {
    setString("applicationState", stateDescription);
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::setProblemDescription(std::string const &problemDescription) {
    setString("problemDescription", problemDescription);
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::updateStatusDescription() {
    if (problems_.size() == 0) {
        setApplicationState(kAllOKString);
        setProblemDescription(kNoProblemString);
    } else {
        std::string tmp = "";
        std::string problemList = "";
        for (std::map<std::string, std::string>::const_iterator i = problems_.begin();
             i != problems_.end();
             ++i) {
            if (!tmp.empty()) {
                tmp += ", ";
                problemList += "\n";
            }
            tmp += i->first;
            problemList += i->first;
            problemList += ":\n  ";
            problemList += i->second;
        }
        std::string tmp0(problemList);
        std::replace(tmp0.begin(), tmp0.end(), '\n', ' ');

        std::string problemSummary;
        if (problems_.size() == 1) {
            problemSummary =
                toolbox::toString("Detected a problem: %s", tmp.c_str());
        } else {
            problemSummary =
                toolbox::toString("Detected %d problems: %s.", problems_.size(), tmp.c_str());
        }
        setApplicationState(problemSummary);
        setProblemDescription(problemList);
    }
    writeInfoSpace();
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::updateSessionIdInApplicationDescriptor() const {
    // NOTE: The reason this business is ugly is two-fold:
    // - The RCMS and XDAQ developers did not agree on the session ID
    //   type. In RCMS it is an unsigned integer, in XDAQ a string.
    // - XDAQ is not equipped to handle dynamic session IDs. The only
    //   normal way to assign a session ID is via the command line upon
    //   executive startup. The PIXEL control applications run as
    //   services and get dynamically assigned to RCMS sessions through
    //   SOAP command parameters.

    // NOTE: Because the RCMS session ID for PIXEL applications can be
    // assigned per application (as opposed to per executive), we cannot
    // use the built-in session ID in the application context.

    // NOTE: Because internally the PIXEL control software uses the
    // original RunControl convention of a numeric session ID, the 'no
    // session in control' case is covered by a zero value. For the
    // string representation this has to be translated to an empty
    // string.

    uint32_t const sessionUInt = getUInt32("rcmsSessionId");
    std::string const sessionStr = (sessionUInt == 0) ? "" : toolbox::toString("%" PRIu32, sessionUInt);
    getOwnerApplication().editApplicationDescriptor()->setAttribute("session", sessionStr);
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::registerItemSetsWithMonitor(Monitor &monitor) {
    std::string itemSetName = "Application state";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "stateName",
                    "Application FSM state",
                    this);
    monitor.addItem(itemSetName,
                    "applicationState",
                    "Application status",
                    this,
                    "The current status of the application. Should be 'All OK' if all is fine.");
    monitor.addItem(itemSetName,
                    "problemDescription",
                    "Problem description",
                    this,
                    "Description of any current problems experienced by the application, or '-' if no problems are observed.");

    monitor.addItem(itemSetName,
                    "rcmsSessionId",
                    "RCMS session identifier of the RunControl session in charge",
                    this,
                    "The identifier of the RunControl session controlling this application. Zero for non-RunControl sessions.");
    monitor.addItem(itemSetName,
                    "hwLeaseOwnerId",
                    "Identifier of RunControl or other session in charge",
                    this,
                    "The identifier of the RunControl or other control session controlling this application. Only SOAP commands from this source will be accepted.");

    monitor.addItem(itemSetName,
                    "maintenanceMode",
                    "Application mode",
                    this,
                    "In 'operations mode' the application is ready for data-taking operations. In 'maintenance mode' the application behaves differently. The latter mode is to be used only for tests and measurements on the PIXEL system.");

    monitor.addItem(itemSetName,
                    "upTime",
                    "Uptime",
                    this,
                    "The total time that has passed since the start of this application.");
    monitor.addItem(itemSetName,
                    "latestMonitoringUpdate",
                    "Latest monitoring update time",
                    this,
                    "The timestamp of the end of the most recent round of monitoring updates.");
    monitor.addItem(itemSetName,
                    "latestMonitoringDuration",
                    "Latest monitoring update duration (s)",
                    this,
                    "The time taken by the control application in the most recent round of monitoring updates. NOTE: This is unrelated to the update rate itself.");

    itemSetName = "itemset-application-status-history";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "history",
                    "Application status history",
                    this);
}

void
pixel::utils::ApplicationStateInfoSpaceHandler::registerItemSetsWithWebServer(WebServer &webServer,
                                                                              Monitor &monitor,
                                                                              std::string const &forceTabName) {
    std::string const tabName = forceTabName.empty() ? "Application status" : forceTabName;

    webServer.registerTab(tabName,
                          "Application information",
                          1);
    webServer.registerTable("Application state",
                            "Info on the state of the XDAQ application",
                            monitor,
                            "Application state",
                            tabName);
    webServer.registerWebObject<WebTableAppHist>("History",
                                                 "Application status and command history",
                                                 monitor,
                                                 "itemset-application-status-history",
                                                 tabName);
}
