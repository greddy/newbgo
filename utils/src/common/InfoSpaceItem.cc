#include "pixel/utils/InfoSpaceItem.h"

// BUG BUG BUG
#include <cassert>
#include <iostream>
// BUG BUG BUG end

pixel::utils::InfoSpaceItem::InfoSpaceItem(std::string const &name,
                                           ItemType const type,
                                           std::string const &format,
                                           UpdateType const updateType,
                                           bool const isValid)
    : name_(name),
      hwName_(name),
      type_(type),
      format_(format),
      updateType_(updateType),
      isValid_(isValid) {
          assert(!hwName_.empty());
}

pixel::utils::InfoSpaceItem::InfoSpaceItem(std::string const &name,
                                           std::string const &hwName,
                                           ItemType const type,
                                           std::string const &format,
                                           UpdateType const updateType,
                                           bool const isValid)
    : name_(name),
      hwName_(hwName),
      type_(type),
      format_(format),
      updateType_(updateType),
      isValid_(isValid) {
          assert(!hwName_.empty());
}

std::string
pixel::utils::InfoSpaceItem::name() const {
    return name_;
}

std::string
pixel::utils::InfoSpaceItem::hwName() const {
    return hwName_;
}

pixel::utils::InfoSpaceItem::ItemType
pixel::utils::InfoSpaceItem::type() const {
    return type_;
}

std::string
pixel::utils::InfoSpaceItem::format() const {
    return format_;
}

pixel::utils::InfoSpaceItem::UpdateType
pixel::utils::InfoSpaceItem::updateType() const {
    return updateType_;
}

bool
pixel::utils::InfoSpaceItem::isValid() const {
    return isValid_;
}

void
pixel::utils::InfoSpaceItem::setValid() {
    isValid_ = true;
}

void
pixel::utils::InfoSpaceItem::setInvalid() {
    isValid_ = false;
}

bool
pixel::utils::
operator==(pixel::utils::InfoSpaceItem const &item, std::string const &name) {
    return (item.name_ == name);
}

bool
pixel::utils::
operator==(std::string const &name, pixel::utils::InfoSpaceItem const &item) {
    return (item == name);
}
