#include "pixel/utils/XDAQAppWithFSMAutomatic.h"

#include "pixel/utils/LogMacros.h"

#include "toolbox/Event.h"
#include "xdaq/InstantiateApplicationEvent.h"

pixel::utils::XDAQAppWithFSMAutomatic::XDAQAppWithFSMAutomatic(xdaq::ApplicationStub *const stub,
                                                               std::unique_ptr<pixel::hwlayer::DeviceBase> hw)
    : XDAQAppWithFSMPixelBase(stub, std::move(hw)),
      fsmDriver_(this) {
    // This 'automatic' state machine is supposed to run
    // itself. Therefore none of the state transitions are exposed to
    // the outside world.
}

pixel::utils::XDAQAppWithFSMAutomatic::~XDAQAppWithFSMAutomatic() {
    fsmDriver_.stop();
}

void
pixel::utils::XDAQAppWithFSMAutomatic::actionPerformed(toolbox::Event &event) {
    // This is called after an application has been fully
    // instantiated. Here it is used to detect when our application is
    // fully up and running (according to XDAQ).

    if (event.type() == "urn:xdaq-event:InstantiateApplication") {
        xdaq::InstantiateApplicationEvent &de =
            dynamic_cast<xdaq::InstantiateApplicationEvent &>(event);
        if (de.getApplicationDescriptor() == getApplicationDescriptor()) {
            // First do what we would always do.
            pixel::utils::XDAQAppWithFSMPixelBase::actionPerformed(event);

            // Then fire up the 'automatic FSM driver'.
            fsmDriver_.start();
        }
    }
}

void
pixel::utils::XDAQAppWithFSMAutomatic::raiseAlarm(std::string const &name,
                                                  std::string const &severity,
                                                  xcept::Exception &err) {
    pixel::utils::XDAQAppWithFSMPixelBase::raiseAlarm(name, severity, err);
}

void
pixel::utils::XDAQAppWithFSMAutomatic::revokeAlarm(std::string const &name) {
    pixel::utils::XDAQAppWithFSMPixelBase::revokeAlarm(name);
}

void
pixel::utils::XDAQAppWithFSMAutomatic::handleMonitoringProblem(std::string const &problemDesc) {
    // In order for the automated FSM to be able to recover from a
    // monitoring problem (by reconnecting and reconfiguring the
    // hardware) we need to ensure that monitoring problems bring the
    // application to the Failed state. (But first do what we always do.)

    pixel::utils::XDAQAppWithFSMPixelBase::handleMonitoringProblem(problemDesc);

    fail();
}
