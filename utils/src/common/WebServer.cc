#include "pixel/utils/WebServer.h"

#include <algorithm>
#include <fstream>
#include <set>
#include <sstream>

#include "log4cplus/logger.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xdaq/Application.h"
#include "xgi/Output.h"

#include "pixel/exception/Exception.h"
#include "pixel/utils/LogMacros.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/WebSpacer.h"
#include "pixel/utils/WebTable.h"
#include "pixel/utils/WebTableMultiCol.h"
#include "pixel/utils/XDAQAppBase.h"
#include "pixel/utils/XGIMethod.h"

pixel::utils::WebServer::WebServer(pixel::utils::XDAQAppBase &xdaqApp)
    : xgi::framework::UIManager(&xdaqApp),
      PIXELObject(xdaqApp),
      logger_(xdaqApp.getApplicationLogger()) {
    // Direct binding for the JSON access to the InfoSpace contents.
    pixel::utils::helper0::deferredbind(&xdaqApp, this, &WebServer::jsonUpdate, "update");

    // Binding for the HTML pages, via the XGI framework so we benefit
    // from the HyperDAQ header and footer.
    // xgi::framework::deferredbind(xdaqApp, this, &WebServer::monitoringWebPage, "Default");
    // xgi::framework::deferredbind(xdaqApp, this, &WebServer::monitoringWebPage, "monitoring");
    pixel::utils::helper1::deferredbind(&xdaqApp, this, &WebServer::monitoringWebPage, "Default");
    pixel::utils::helper1::deferredbind(&xdaqApp, this, &WebServer::monitoringWebPage, "monitoring");

    // NOTE: This call replaces the default layout for the HyperDAQ
    // pages for _all_ applications in the XDAQ executive. Not just for
    // the PIXEL applications.
    setLayout(&layout_);
}

pixel::utils::WebServer::~WebServer() {
}

std::string
pixel::utils::WebServer::getApplicationName() const {
    std::string xmlClassName = getOwnerApplication().getApplicationDescriptor()->getClassName();
    size_t lastColon = xmlClassName.find_last_of(":");
    std::string appName = xmlClassName;
    if (lastColon != std::string::npos) {
        appName = xmlClassName.substr(lastColon + 1);
    }
    return appName;
}

std::string
pixel::utils::WebServer::getServiceName() const {
    std::string const res = getOwnerApplication().getApplicationDescriptor()->getAttribute("service");
    return res;
}

pixel::utils::WebTab &
pixel::utils::WebServer::getTab(std::string const &tabName) const {
    std::vector<WebTab *>::const_iterator tab =
        std::find_if(tabs_.begin(), tabs_.end(), WebTab::FindByName(tabName));
    if (tab == tabs_.end()) {
        std::string msg = "No tab with name '" +
                          tabName +
                          "' exists in the webserver.";
        ERROR(msg);
        XCEPT_RAISE(pixel::exception::SoftwareProblem, msg);
    }
    return *(*tab);
}

bool
pixel::utils::WebServer::tabExists(std::string const &tabName) const {
    std::vector<WebTab *>::const_iterator tab =
        std::find_if(tabs_.begin(), tabs_.end(), WebTab::FindByName(tabName));
    return (tab != tabs_.end());
}

void
pixel::utils::WebServer::registerTab(std::string const &name,
                                     std::string const &description,
                                     size_t const numColumns) {
    // NOTE: It's not a problem if the requested tab already exists, of
    // course. Less work.
    if (!tabExists(name)) {
        WebTab *tab = new WebTab(name, description, numColumns);
        tabs_.push_back(tab);
    }
}

void
pixel::utils::WebServer::registerTable(std::string const &name,
                                       std::string const &description,
                                       Monitor const &monitor,
                                       std::string const &itemSetName,
                                       std::string const &tabName,
                                       size_t const colSpan) {
    registerWebObject<WebTable>(name, description, monitor, itemSetName, tabName, colSpan);
}

void
pixel::utils::WebServer::registerMultiColTable(std::string const &name,
                                               std::string const &description,
                                               Monitor const &monitor,
                                               std::vector<std::string> const &itemSetNameVec,
                                               std::string const &tabName,
                                               std::string const &id) {
    registerWebObject<WebTableMultiCol, std::vector<std::string> >(name, description, monitor, itemSetNameVec, tabName, id);
}

void
pixel::utils::WebServer::registerSpacer(std::string const &name,
                                        std::string const &description,
                                        Monitor const &monitor,
                                        std::string const &itemSetName,
                                        std::string const &tabName,
                                        size_t const colSpan) {
    registerWebObject<WebSpacer>(name, description, monitor, itemSetName, tabName, colSpan);
}

void
#ifndef XDAQ_TARGET_XDAQ15
pixel::utils::WebServer::jsonUpdate(xgi::Input *const in, xgi::Output *const out)
#else
pixel::utils::WebServer::jsonUpdate(xgi::Input *const in, xgi::Output *const out)
#endif
{
    try {
        jsonUpdateCore(in, out);
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to serve the JSON update. "
                              "Caught an exception: '%s'.",
                              err.what());
        ERROR(msg);
        XCEPT_DECLARE(pixel::exception::RuntimeProblem, top, msg);
        getOwnerApplication().notifyQualified("error", top);
    }
}

void
pixel::utils::WebServer::jsonUpdateCore(xgi::Input *const in, xgi::Output *const out) const {
    // Prepare the actual JSON contents.
    std::vector<WebTab *>::const_iterator iter;
    std::stringstream tmp("");
    for (iter = tabs_.begin(); iter != tabs_.end(); ++iter) {
        std::string const jsonTmp = (*iter)->getJSONString();
        if (!jsonTmp.empty()) {
            if (!tmp.str().empty()) {
                tmp << ",\n";
            }
            tmp << jsonTmp;
        }
    }

    std::string jsonContents = "{\n" + tmp.str() + "\n}";

    // Stuff everything into the output.
    out->getHTTPResponseHeader().addHeader("Content-Type", "application/json");
    *out << jsonContents;
}

void
#ifndef XDAQ_TARGET_XDAQ15
pixel::utils::WebServer::monitoringWebPage(xgi::Input *const in, xgi::Output *const out)
#else
pixel::utils::WebServer::monitoringWebPage(xgi::Input *const in, xgi::Output *const out)
#endif
{
    xgi::Output tmpOut;
    tmpOut.str("");
    tmpOut.setHTTPResponseHeader(out->getHTTPResponseHeader());
    try {
        monitoringWebPageCore(in, &tmpOut);
        *out << tmpOut.str();
        out->setHTTPResponseHeader(tmpOut.getHTTPResponseHeader());
    }
    catch (xcept::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to serve the monitoring web page. "
                              "Caught an exception: '%s'.",
                              err.what());
        *out << msg;
        ERROR(msg);
        XCEPT_DECLARE(pixel::exception::RuntimeProblem, top, msg);
        getOwnerApplication().notifyQualified("error", top);
    }
}

void
pixel::utils::WebServer::monitoringWebPageCore(xgi::Input *const in, xgi::Output *const out) const {
    // NOTE: Since we are using the deferred binding for HTTP calls, the
    // XDAQ framework will take care of the HTML skeleton. We only have
    // to fill in the meat.

    // NOSCRIPT handling. Not very nice, but descriptive it is.
    *out << "<noscript>\n"
         << "<div style=\"position: fixed; top: 0px; left: 0px; z-index: 3000;"
         << "height: 100%; width: 100%; background-color: #FFFFFF\">"
         << "<p style=\"margin-left: 10px\">"
         << "Sorry. The PIXEL control applications just don't work without JavaScript, "
         << "and JavaScript seems to be disabled."
         << "</p>"
         << "</div>\n"
         << "</noscript>\n";

    // And then this is the real meat.
    printTabs(out);
}

void
pixel::utils::WebServer::printTabs(xgi::Output *out) const {
    // NOTE: This has been written to work with the new XDAQ12-style
    // HyperDAQ tabs and doT.js.

    // Start with an overall application title.
    // NOTE: Mix in a bit of templating to show the FSM state name in
    // the title.
    // NOTE: Only show the state name if the state name string is
    // defined and not set to 'n/a'.
    std::string const stateNameSrc =   "[\"Application state\"][\"Application FSM state\"]";
    *out << "<div class=\"pixel-application-title-wrapper\">"
         << "\n";
    std::string tmpServiceName = getServiceName();
    if (!tmpServiceName.empty()) {
        tmpServiceName = " '" + tmpServiceName + "'";
    }
    *out << "<h1 id=\"pixel-application-title\">" << getApplicationName() << tmpServiceName << "</h1>"
         << "\n";

         //Emily commented out
    // *out << "<script type=\"text/x-dot-template\">"
    //      << "{{? it" << stateNameSrc << " != 'n/a'}}"
    //      << "<h2 id=\"pixel-application-subtitle\">"
    //      << "({{=it" << stateNameSrc << "}})</h2>"
    //      << "\n"
    //      << "{{?}}"
    //      << "</script>"
    //      << "\n"
    //      << "<div class=\"target\"></div>";

    *out << "</div>"
         << "\n";

    // Now plug in the main tab wrapper.
    *out << "<div class=\"xdaq-tab-wrapper\">";
    *out << "\n";

    for (std::vector<WebTab *>::const_iterator tab = tabs_.begin();
         tab != tabs_.end();
         ++tab) {
        *out << "<div class=\"xdaq-tab\" "
             << "title=\"" << (*tab)->getName() << "\">"
             << "\n"
             << (*tab)->getHTMLString()
             << "\n"
             << "</div>"
             << "\n";
    }

    *out << "</div>";
    *out << "\n";
}
