#include "pixel/utils/XDAQAppWithFSMForPMs.h"

#include "xdaq/NamespaceURI.h"
#include "xoap/Method.h"

pixel::utils::XDAQAppWithFSMForPMs::XDAQAppWithFSMForPMs(xdaq::ApplicationStub *const stub,
                                                         std::unique_ptr<pixel::hwlayer::DeviceBase> hw)
    : XDAQAppWithFSMPixelBase(stub, std::move(hw)) {
    // These bindings expose the state machine to the outside world.
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "ColdReset", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "Configure", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "Enable", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "Halt", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "Pause", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "Reconfigure", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "Resume", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "Stop", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "TTCResync", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMForPMs>(this, &XDAQAppWithFSMForPMs::changeState, "TTCHardReset", XDAQ_NS_URI);
}

pixel::utils::XDAQAppWithFSMForPMs::~XDAQAppWithFSMForPMs() {
}
