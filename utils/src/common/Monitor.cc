#include "pixel/utils/Monitor.h"

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <memory>
#include <set>
#include <sstream>
#include <time.h>
#include <typeinfo>
#include <unistd.h>
#include <stdexcept>

#include "log4cplus/logger.h"

#include "toolbox/stacktrace.h"

#include "toolbox/exception/Exception.h"
#include "toolbox/string.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"

#include "pixel/exception/Exception.h"
#include "pixel/utils/ConfigurationInfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/Lock.h"
#include "pixel/utils/LockGuard.h"
#include "pixel/utils/LogMacros.h"
#include "pixel/utils/XDAQAppBase.h"

pixel::utils::Monitor::Monitor(pixel::utils::XDAQAppBase &xdaqApp, std::string const &name)
    : PIXELObject(xdaqApp),
      logger_(xdaqApp.getApplicationLogger()),
      alarmName_("MonitoringAlarm"),
      isInAlarm_(false),
      timerP_(0),
      timeStart_(toolbox::TimeVal::gettimeofday()) {
    std::string timerName;
    if (name.empty()) {
        timerName = toolbox::toString("MonitoringTimer_lid%d",
                                      getOwnerApplication().getApplicationDescriptor()->getLocalId());
    } else {
        timerName = toolbox::toString("%s_lid%d",
                                      name.c_str(),
                                      getOwnerApplication().getApplicationDescriptor()->getLocalId());
    }
    timerP_ = toolbox::task::getTimerFactory()->createTimer(timerName);
    timerP_->addExceptionListener(this);
}

pixel::utils::Monitor::~Monitor() {
}

void
pixel::utils::Monitor::startMonitoring() {
    std::string const tmp = getOwnerApplication().cfgInfoSpaceP_->getString("monitoringInterval");
    toolbox::TimeInterval monitoringInterval;
    try {
        monitoringInterval.fromString(tmp);
    }
    catch (toolbox::exception::Exception &err) {
        std::string const msg =
            toolbox::toString("Failed to start monitoring thread. "
                              "Failed to interpret '%s' as update interval.",
                              tmp.c_str());
        raiseMonitoringAlarm(msg);
        return;
    }

    // Check for zero-intervals. These imply no monitoring updates at
    // all.
    if (monitoringInterval != toolbox::TimeInterval(0)) {
        // Just add a little bit of a random delay, so not all
        // monitoring applications run in-sync and create bandwidth
        // issues.
        // NOTE: This is especially important for the connections to the
        // hardware. At the hardware level we're often dealing with a
        // single, physical bus (e.g., the I2C bus on the uTCA carrier
        // boards) that just cannot be accessed by more than a single
        // entity at any point in time.
        float const delayRnd = (1. * rand() / RAND_MAX) * double(monitoringInterval);
        float const delaySec = std::floor(delayRnd);
        float const delayUsec = (delayRnd - delaySec) * 1000000;
        toolbox::TimeInterval const delay(delaySec, delayUsec);
        toolbox::TimeVal start = toolbox::TimeVal::gettimeofday() + delay;

        try {
            // This schedules periodic calls to
            // pixel::utils::Monitor::timeExpired().
            // NOTE: The way the XDAQ toolbox::task::Timer handles this,
            // it always waits for the callback to finish before
            // rescheduling. This is the right thing to do for periodic
            // regreshes, in case the update takes longer than the
            // update interval.
            timerP_->scheduleAtFixedRate(start, this, monitoringInterval, 0, "");
        }
        catch (xcept::Exception &err) {
            std::string const msg =
                toolbox::toString("Failed to activate the monitoring timer: '%s'.",
                                  err.what());
            raiseMonitoringAlarm(msg);
        }
    }
}

pixel::utils::Monitor::StringPairVector
pixel::utils::Monitor::getFormattedItemSet(std::string const &itemSetName) const {
    pixel::utils::Monitor::StringPairVector res;

    // Get the requested ItemSet and complain if it does not exist.
    MonitorItemMap::const_iterator itemSet = itemSets_.find(itemSetName);
    if (itemSet == itemSets_.end()) {
        std::string msg = "No ItemSet with name '" +
                          itemSetName +
                          "' exists in the monitor.";
        ERROR(msg);
        XCEPT_RAISE(pixel::exception::SoftwareProblem, msg);
    }

    std::vector<std::pair<std::string, MonitorItem> > items = itemSet->second;
    std::vector<std::pair<std::string, MonitorItem> >::const_iterator iter;
    for (iter = items.begin(); iter != items.end(); ++iter) {
        MonitorItem const item = iter->second;
        std::string const desc = item.getDescription();
        std::string value = "";
        try {
            value = item.getInfoSpaceHandler()->getFormatted(item.getName());
        }
        catch (pixel::exception::Exception &err) {
            // BUG BUG BUG
            // This should really disappear once we have a
            // 'InfoSpaceChecker' implemented.
            value = "\"!!! monitoring error !!!\"";
            // BUG BUG BUG end
            std::string const msg =
                toolbox::toString("Failed to format monitoring item '%s': %s.",
                                  item.getName().c_str(),
                                  err.what());
            raiseMonitoringAlarm(msg);
        }
        res.push_back(std::make_pair(desc, value));
    }

    return res;
}

pixel::utils::Monitor::StringTupleVector
pixel::utils::Monitor::getNameFormattedItemSet(std::string const &itemSetName) const {
    pixel::utils::Monitor::StringTupleVector res;

    // Get the requested ItemSet and complain if it does not exist.
    MonitorItemMap::const_iterator itemSet = itemSets_.find(itemSetName);
    if (itemSet == itemSets_.end()) {
        std::string msg = "No ItemSet with name '" +
                          itemSetName +
                          "' exists in the monitor.";
        ERROR(msg);
        XCEPT_RAISE(pixel::exception::SoftwareProblem, msg);
    }

    std::vector<std::pair<std::string, MonitorItem> > items = itemSet->second;
    std::vector<std::pair<std::string, MonitorItem> >::const_iterator iter;
    for (iter = items.begin(); iter != items.end(); ++iter) {
        MonitorItem const item = iter->second;
        std::string const name = item.getName();
        std::string value = "";
        try {
            value = item.getInfoSpaceHandler()->getFormatted(name);
        }
        catch (pixel::exception::Exception &err) {
            // BUG BUG BUG
            // This should really disappear once we have a
            // 'InfoSpaceChecker' implemented.
            value = "\"!!! monitoring error !!!\"";
            // BUG BUG BUG end
            std::string const msg =
                toolbox::toString("Failed to format monitoring item '%s': %s.",
                                  name.c_str(),
                                  err.what());
            raiseMonitoringAlarm(msg);
        }
        res.push_back(std::make_tuple(name, item.getDescription(), value));
    }

    return res;
}

pixel::utils::Monitor::StringPairVector
pixel::utils::Monitor::getItemSetDocStrings(std::string const &itemSetName) const {
    pixel::utils::Monitor::StringPairVector res;

    // Get the requested ItemSet and complain if it does not exist.
    MonitorItemMap::const_iterator itemSet = itemSets_.find(itemSetName);
    if (itemSet == itemSets_.end()) {
        std::string msg = "No ItemSet with name '" +
                          itemSetName +
                          "' exists in the monitor.";
        ERROR(msg);
        XCEPT_RAISE(pixel::exception::SoftwareProblem, msg);
    }

    std::vector<std::pair<std::string, MonitorItem> > items = itemSet->second;
    std::vector<std::pair<std::string, MonitorItem> >::const_iterator iter;
    for (iter = items.begin(); iter != items.end(); ++iter) {
        MonitorItem const item = iter->second;
        std::string const desc = item.getDescription();
        std::string const docString = item.getDocString();
        res.push_back(std::make_pair(desc, docString));
    }

    return res;
}

void
pixel::utils::Monitor::onException(xcept::Exception &err) {
    handleException(err);
}

void
pixel::utils::Monitor::handleException(xcept::Exception &err) {
    // TODO TODO TODO
    // Should have this do the same thing with the alarm etc. as
    // 'normal' exceptions do.
    // TODO TODO TODO end
}

void
pixel::utils::Monitor::addInfoSpace(InfoSpaceHandler *const infoSpace) {
    std::string name = infoSpace->name();
    std::tr1::unordered_map<std::string, InfoSpaceHandler *>::iterator iter =
        infoSpaceMap_.find(name);
    if (iter == infoSpaceMap_.end()) {
        DEBUG("Adding InfoSpace '"
              << name
              << "' to monitoring list.");
        infoSpaceMap_.insert(std::make_pair(name, infoSpace));
    }
}

void
pixel::utils::Monitor::addItem(std::string const &itemSetName,
                               std::string const &itemName,
                               std::string const &itemDesc,
                               InfoSpaceHandler *const infoSpaceHandler,
                               std::string const &docString) {
    // Find the ItemSet into which this item should go.
    typedef std::tr1::unordered_map<std::string, std::vector<std::pair<std::string, MonitorItem> > >::iterator SetListIter;
    SetListIter set = itemSets_.find(itemSetName);
    if (set == itemSets_.end()) {
        std::string msg = "ItemSet with name '" +
                          itemSetName +
                          "' does not exist in the monitor.";
        FATAL(msg);
        XCEPT_RAISE(pixel::exception::SoftwareProblem, msg);
    }

    MonitorItem item(itemName, itemDesc, infoSpaceHandler, docString);
    (*set).second.push_back(std::make_pair(itemName, item));

    addInfoSpace(infoSpaceHandler);
}

void
pixel::utils::Monitor::newItemSet(std::string const &itemSetName) {
    // if (itemSetName == "itemset-app-config")
    //   {
    //     DEBUG("DEBUG JGH   ==================================================");
    //     std::stringstream tmpStream;
    //     toolbox::stacktrace(128, tmpStream);
    //     std::stringstream tmp;
    //     tmp << std::endl;
    //     std::list<std::string> tmpLines = toolbox::parseTokenList(tmpStream.str(), tmp.str());
    //     for (std::list<std::string>::const_iterator i = tmpLines.begin();
    //          i != tmpLines.end();
    //          ++i)
    //       {
    //         DEBUG(*i);
    //       }
    //     DEBUG("DEBUG JGH   ==================================================");
    //   }

    if (itemSets_.find(itemSetName) != itemSets_.end()) {
        std::string msg = "ItemSet with name '" +
                          itemSetName +
                          "' already exists in the monitor.";
        FATAL(msg);
        XCEPT_RAISE(pixel::exception::SoftwareProblem, msg);
    }

    std::vector<std::pair<std::string, MonitorItem> > emptySet;
    itemSets_.insert(std::make_pair(itemSetName, emptySet));
}

void
pixel::utils::Monitor::timeExpired(toolbox::task::TimerEvent &event) {
    updateAllInfoSpaces();
}

void
pixel::utils::Monitor::updateAllInfoSpaces() {
    // Keep track of how long this takes.
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    bool success = true;

    // NOTE: It is possible that this loop spans multiple instances of
    // the same type of InfoSpaceHandler. So it is not impossible to
    // encounter the same problem multiple times. Hence the fiddling
    // with the set.
    std::set<std::string> problems;
    std::tr1::unordered_map<std::string, InfoSpaceHandler *>::iterator iter;
    for (iter = infoSpaceMap_.begin(); iter != infoSpaceMap_.end(); ++iter) {
        // NOTE: One should be aware of the presence of this lock: any
        // monitoring update blocks anything else that depends on this
        // lock. It has been placed deliberately inside the loop over
        // InfoSpaces.
        std::unique_ptr<LockGuard<Lock> > guardedLock(new LockGuard<Lock>(getOwnerApplication().monitoringLock_));

        try {
            iter->second->update();
        }
        catch (xcept::Exception &err) {
            problems.insert(err.what());
            success = false;
        }
        catch (std::exception const &err) {
            problems.insert(err.what());
            success = false;
        }

        guardedLock.reset();
        ::usleep(kLoopRelaxTime);
    }

    if (success) {
        revokeMonitoringAlarm();
    } else {
        std::string const msgBase = "Problem updating monitoring information";
        std::string msg = msgBase + ": ";
        for (std::set<std::string>::const_iterator it = problems.begin();
             it != problems.end();
             ++it) {
            if (it != problems.begin()) {
                msg += ", ";
            }
            msg += "'" + *it + "'";
        }
        // Log the problem.
        ERROR(msg);
        raiseMonitoringAlarm(msg);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();

    // Update the uptime estimate and the timestamp of the latest
    // monitoring update.
    toolbox::TimeVal timeNow(toolbox::TimeVal::gettimeofday());
    toolbox::TimeInterval upTime = timeNow - timeStart_;
    getOwnerApplication().appStateInfoSpace_.setDouble("upTime",
                                                       double(upTime));
    getOwnerApplication().appStateInfoSpace_.setTimeVal("latestMonitoringUpdate",
                                                        double(timeNow));
    getOwnerApplication().appStateInfoSpace_.setDouble("latestMonitoringDuration",
                                                       double(timeEnd - timeBegin));
    getOwnerApplication().appStateInfoSpace_.writeInfoSpace();
}

void
pixel::utils::Monitor::raiseMonitoringAlarm(std::string const &problemDesc) const {
    // Set our internal flag.
    // NOTE: We don't care if this was already set or not. If we
    // encounter a new monitoring problem, we just overwrite the
    // existing one.
    isInAlarm_ = true;

    // Log the problem.
    ERROR("Raising monitoring alarm: '" << problemDesc << "'.");

    // Raise the alarm.
    XCEPT_DECLARE(pixel::exception::MonitoringFailureAlarm, alarmException, problemDesc);
    getOwnerApplication().raiseAlarm(alarmName_, "error", alarmException);
    getOwnerApplication().appStateInfoSpace_.addMonitoringProblem(problemDesc);

    // Let the application do whatever (else) it needs to do.
    getOwnerApplication().handleMonitoringProblem(problemDesc);
}

void
pixel::utils::Monitor::revokeMonitoringAlarm() const {
    // Only do something if we're actually in an alarm condition.
    if (isInAlarm_) {
        // Log the action.
        INFO("Revoking monitoring alarm.");

        // Revoke the alarm.
        getOwnerApplication().revokeAlarm(alarmName_);
        getOwnerApplication().appStateInfoSpace_.removeMonitoringProblem();

        // Update our internal flag.
        isInAlarm_ = false;
    }
}
