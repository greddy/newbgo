#include "pixel/utils/InfoSpaceUpdaterPlain.h"

#include "xdaq/Application.h"

#include "pixel/utils/InfoSpaceUpdater.h"

pixel::utils::InfoSpaceUpdaterPlain::InfoSpaceUpdaterPlain(xdaq::Application &xdaqApp)
    : pixel::utils::InfoSpaceUpdater(xdaqApp) {
}

pixel::utils::InfoSpaceUpdaterPlain::~InfoSpaceUpdaterPlain() {
}
