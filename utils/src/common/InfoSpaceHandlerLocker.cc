#include "pixel/utils/InfoSpaceHandlerLocker.h"

pixel::utils::InfoSpaceHandlerLocker::InfoSpaceHandlerLocker(pixel::utils::InfoSpaceHandler const *const infoSpaceHandler)
    : infoSpaceHandler_(infoSpaceHandler) {
          lock();
}

pixel::utils::InfoSpaceHandlerLocker::~InfoSpaceHandlerLocker() {
    unlock();
}

void
pixel::utils::InfoSpaceHandlerLocker::lock() const {
    infoSpaceHandler_->infoSpaceP_->lock();
    if (infoSpaceHandler_->mirrorInfoSpaceP_) {
        try {
            infoSpaceHandler_->mirrorInfoSpaceP_->lock();
        }
        catch (...) {
            infoSpaceHandler_->infoSpaceP_->unlock();
            throw;
        }
    }
}

void
pixel::utils::InfoSpaceHandlerLocker::unlock() const {
    try {
        infoSpaceHandler_->infoSpaceP_->unlock();
    }
    catch (...) {
        // Stumble on.
    }

    if (infoSpaceHandler_->mirrorInfoSpaceP_) {
        try {
            infoSpaceHandler_->mirrorInfoSpaceP_->unlock();
        }
        catch (...) {
            // Stumble on.
        }
    }
}
