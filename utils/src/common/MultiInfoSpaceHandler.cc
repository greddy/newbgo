#include "pixel/utils/MultiInfoSpaceHandler.h"

#include "log4cplus/logger.h"

#include "xdaq/Application.h"

#include "pixel/utils/Monitor.h"
#include "pixel/utils/LogMacros.h"
#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceUpdater.h"

pixel::utils::MultiInfoSpaceHandler::MultiInfoSpaceHandler(xdaq::Application &xdaqApp,
                                                           // std::string const& name,
                                                           InfoSpaceUpdater *const updater)
    : XDAQObject(xdaqApp),
      logger_(xdaqApp.getApplicationLogger())
      // name_(name),
      // updaterP_(updater)
{
}

pixel::utils::MultiInfoSpaceHandler::~MultiInfoSpaceHandler() {
    for (std::map<std::string, pixel::utils::InfoSpaceHandler *>::iterator
             i = infoSpaceHandlers_.begin();
         i != infoSpaceHandlers_.end();
         ++i) {
        delete i->second;
        infoSpaceHandlers_.erase(i);
    }
}

void
pixel::utils::MultiInfoSpaceHandler::registerItemSets(Monitor &monitor,
                                                      WebServer &webServer) {
    registerItemSetsWithMonitor(monitor);
    registerItemSetsWithWebServer(webServer, monitor);
}

void
pixel::utils::MultiInfoSpaceHandler::registerItemSetsWithMonitor(pixel::utils::Monitor &monitor) {
    for (std::map<std::string, pixel::utils::InfoSpaceHandler *>::iterator
             i = infoSpaceHandlers_.begin();
         i != infoSpaceHandlers_.end();
         ++i) {
        i->second->registerItemSetsWithMonitor(monitor);
    }
}

void
pixel::utils::MultiInfoSpaceHandler::registerItemSetsWithWebServer(pixel::utils::WebServer &webServer,
                                                                   pixel::utils::Monitor &monitor,
                                                                   std::string const &forceTabName) {
    for (std::map<std::string, pixel::utils::InfoSpaceHandler *>::iterator
             i = infoSpaceHandlers_.begin();
         i != infoSpaceHandlers_.end();
         ++i) {
        i->second->registerItemSetsWithWebServer(webServer, monitor, forceTabName);
    }
}

void
pixel::utils::MultiInfoSpaceHandler::writeInfoSpace(bool const force) {
    for (std::map<std::string, pixel::utils::InfoSpaceHandler *>::const_iterator
             it = infoSpaceHandlers_.begin();
         it != infoSpaceHandlers_.end();
         ++it) {
        it->second->writeInfoSpace(true);
    }
}
