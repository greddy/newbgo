#include "pixel/utils/XDAQObject.h"

pixel::utils::XDAQObject::XDAQObject(xdaq::Application &owner)
    : owner_(owner) {
}

pixel::utils::XDAQObject::~XDAQObject() {
}

xdaq::Application &
pixel::utils::XDAQObject::getOwnerApplication() const {
    return owner_;
}
