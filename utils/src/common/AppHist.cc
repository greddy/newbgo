#include "pixel/utils/AppHist.h"

#include <sstream>

#include "toolbox/string.h"

#include "pixel/utils/Utils.h"

pixel::utils::AppHist::AppHist() {
}

pixel::utils::AppHist::~AppHist() {
}

void
pixel::utils::AppHist::addItem(std::string const &msg) {
    // Keep track of the underlying history items first.
    if ((history_.size() > 0) && (history_.back().message() == msg)) {
        history_.back().addOccurrence();
    } else {
        history_.push_back(AppHistItem(msg));
    }

    // Make sure things don't grow out of hand.
    while (history_.size() > kMaxHistSize) {
        history_.pop_front();
    }
}

std::string
pixel::utils::AppHist::getJSONString() const {
    std::stringstream res;

    res << "[";
    size_t index = 0;
    for (std::deque<pixel::utils::AppHistItem>::const_iterator it = history_.begin();
         it != history_.end();
         ++it) {
        res << "{";

        // The time(stamp) info of the message.
        res << "\"Timestamp\": \"";
        if (it->count() <= 1) {
            // The timestamp.
            res << pixel::utils::formatTimestamp(it->timestamp());
        } else {
            // Add the timestamp of the last occurrence to show the time
            // span of the messages.
            res << pixel::utils::formatTimeRange(it->timestamp(),
                                                 it->timestampLastOccurrence());
        }
        res << "\""
            << ", ";

        // The message itself.
        std::string msg = it->message();
        if (it->count() > 1) {
            unsigned int repeatCount = it->count() - 1;
            if (repeatCount == 1) {
                msg.append(" (Same message repeated 1 more time.)");
            } else {
                msg.append(toolbox::toString(" (Same message repeated %d more times.)", repeatCount));
            }
        }
        res << "\"Message\": " << escapeAsJSONString(msg);

        res << "}";

        if (index != (history_.size() - 1)) {
            res << ", ";
        }
        ++index;
    }
    res << "]";

    return res.str();
}
