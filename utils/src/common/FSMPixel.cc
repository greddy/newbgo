#include "pixel/utils/FSMPixel.h"

#include <cassert>
#include <inttypes.h>
#include <stdint.h>
#include <vector>

#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"
#include "toolbox/fsm/exception/Exception.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/InvalidInputEvent.h"
#include "toolbox/string.h"
#include "toolbox/Event.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xdaq2rc/ClassnameAndInstance.h"
#include "xdata/Bag.h"
#include "xdata/Serializable.h"
#include "xdata/String.h"
#include "xoap/MessageFactory.h"

#include "pixel/exception/Exception.h"
#include "pixel/hwlayer/RegDumpConfigurationProcessor.h"
#include "pixel/utils/ConfigurationInfoSpaceHandler.h"
#include "pixel/utils/LogMacros.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/XDAQAppWithFSMPixelBase.h"

pixel::utils::FSMPixel::FSMPixel(XDAQAppWithFSMPixelBase *const xdaqApp)
    : pixel::utils::SOAPER(xdaqApp),
      fsmP_(0),
      xdaqAppP_(xdaqApp),
      logger_(xdaqAppP_->getApplicationLogger()),
      rcmsNotifier_(xdaqAppP_) {
    // Find our connection to RCMS.
    rcmsNotifier_.findRcmsStateListener();

    // Create the underlying Finite State Machine itself.
    std::string const commandLoopName =
        toolbox::toString("FSMCommandLoop_lid%d",
                          xdaqAppP_->getApplicationDescriptor()->getLocalId());
    fsmP_ = new toolbox::fsm::AsynchronousFiniteStateMachine(commandLoopName);

    // Setup the FSM with states and transitions.
    // NOTE: There is also the hard-coded 'F'/'Failed' state.
    fsmP_->addState('q', "ColdResetting", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('I', "Initial", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('c', "Configuring", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('C', "Configured", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('i', "Starting", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('R', "Running", this, &FSMPixel::stateChangedWithNotification);//Old 'S'
    fsmP_->addState('h', "Halting", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('H', "Halted", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('p', "Pausing", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('P', "Paused", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('r', "Resuming", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('T', "Stopping", this, &FSMPixel::stateChangedWithNotification);//Old 's'
    fsmP_->addState('z', "PostConfigureZeroing", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('y', "PreEnableZeroing", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('s', "FixingSoftError", this, &FSMPixel::stateChangedWithNotification);
    fsmP_->addState('S', "FixedSoftError", this, &FSMPixel::stateChangedWithNotification);

    // ColdReset: H -> H.
    fsmP_->addStateTransition('H', 'q', "ColdReset");
    fsmP_->addStateTransition('q', 'H', "ColdResettingDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::coldResetAction);

    // Initialize : I -> H
    fsmP_->addStateTransition('I', 'H', "Initialize", xdaqAppP_, &XDAQAppWithFSMPixelBase::initializeAction);

    // Configure: H -> C.
    fsmP_->addStateTransition('H', 'c', "Configure", xdaqAppP_, &XDAQAppWithFSMPixelBase::transitionHaltedToConfiguringAction);
    fsmP_->addStateTransition('c', 'C', "ConfiguringDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::configureAction);

    // Reconfigure: C -> C or P -> P.
    fsmP_->addStateTransition('C', 'C', "Reconfigure", xdaqAppP_, &XDAQAppWithFSMPixelBase::reconfigureAction);
    fsmP_->addStateTransition('P', 'P', "Reconfigure", xdaqAppP_, &XDAQAppWithFSMPixelBase::reconfigureAction);

    // Start: C -> R.
    //TODO need to be checked what EnableZeroing is
    //fsmP_->addStateTransition('y', 'e', "PreEnableZeroingDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::zeroAction);
    fsmP_->addStateTransition('C', 'i', "Start");
    fsmP_->addStateTransition('i', 'R', "StartingDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::startAction);

    // Pause: R -> P.
    fsmP_->addStateTransition('R', 'p', "Pause");
    fsmP_->addStateTransition('p', 'P', "PausingDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::pauseAction);

    // Resume: P -> R.
    fsmP_->addStateTransition('P', 'r', "Resume");
    //fsmP_->addStateTransition('r', 'E', "ResumingDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::resumeAction);
    fsmP_->addStateTransition('r', 'R', "ResumingDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::resumeAction);

    // Stop: R/P -> C.
    //fsmP_->addStateTransition('E', 'T', "Stop");
    fsmP_->addStateTransition('R', 'T', "Stop");
    fsmP_->addStateTransition('P', 'T', "Stop");
    fsmP_->addStateTransition('T', 'C', "StoppingDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::stopAction);

    // Halt: C/R/F/H/P -> H.
    fsmP_->addStateTransition('C', 'h', "Halt", xdaqAppP_, &XDAQAppWithFSMPixelBase::transitionConfiguredToHaltingAction);
    //fsmP_->addStateTransition('E', 'h', "Halt");
    fsmP_->addStateTransition('R', 'h', "Halt", xdaqAppP_, &XDAQAppWithFSMPixelBase::transitionRunningOrPausedToHaltingAction);
    fsmP_->addStateTransition('F', 'h', "Halt");
    fsmP_->addStateTransition('H', 'h', "Halt");
    fsmP_->addStateTransition('P', 'h', "Halt", xdaqAppP_, &XDAQAppWithFSMPixelBase::transitionRunningOrPausedToHaltingAction);
    ////fsmP_->addStateTransition('R', 'h', "Halt");
    fsmP_->addStateTransition('h', 'H', "HaltingDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::haltAction);

    // SoftErrorRecovery
    fsmP_->addStateTransition('R', 's', "FixSoftError");
    fsmP_->addStateTransition('s', 'S', "FixingSoftErrorDone", xdaqAppP_, &XDAQAppWithFSMPixelBase::fixSoftErrorAction);
    fsmP_->addStateTransition('S', 'R', "ResumeFromSoftError", xdaqAppP_, &XDAQAppWithFSMPixelBase::resumeFromSoftErrorAction);


    // The following are special, for the special case of failure.
    // NOTE: The following automatically creates an 'X' -> 'F'
    // transition for all states 'X'.
    std::vector<toolbox::fsm::State> const states = fsmP_->getStates();
    for (std::vector<toolbox::fsm::State>::const_iterator it = states.begin();
         it != states.end();
         ++it) {
        fsmP_->addStateTransition(*it, 'F', "Fail");
    }
    fsmP_->setFailedStateTransitionAction(xdaqAppP_, &XDAQAppWithFSMPixelBase::failAction);
    fsmP_->setFailedStateTransitionChanged(this, &FSMPixel::stateChangedToFailedWithNotification);
    fsmP_->setStateName('F', "Failed");
    fsmP_->setInvalidInputStateTransitionAction(this, &FSMPixel::invalidStateTransitionAction);

    //----------

    // Now bolt on a little bit of extra functionality: automatic transitions.
    // NOTE: The XDAQ FSMPixelPixel only supports methods that are called upon
    // state changes, i.e., when _leaving_ a state, the following looks
    // a bit clumsy, name-wise.

    // One entry for each of the intermediate '***ing' transition states.
    // E.g.: when done with 'ColdResetting' continue with 'ColdResettingDone'.
    automaticTransitions_["ColdResetting"] = "ColdResettingDone";
    automaticTransitions_["Configuring"] = "ConfiguringDone";
    automaticTransitions_["Enabling"] = "EnablingDone";
    automaticTransitions_["Starting"] = "StartingDone";
    automaticTransitions_["Halting"] = "HaltingDone";
    automaticTransitions_["Pausing"] = "PausingDone";
    automaticTransitions_["Resuming"] = "ResumingDone";
    automaticTransitions_["Stopping"] = "StoppingDone";
    automaticTransitions_["Starting"] = "StartingDone";//?
    automaticTransitions_["FixingSoftError"] = "FixingSoftErrorDone";//?

    //----------

    // Start out with the FSM in its initial state: Initial.
    fsmP_->setInitialState('I');
    reset("Startup.");
}

pixel::utils::FSMPixel::~FSMPixel() {
    if (fsmP_ != 0) {
        delete fsmP_;
        fsmP_ = 0;
    }
}

xoap::MessageReference
pixel::utils::FSMPixel::changeState(xoap::MessageReference msg) {
    // NOTE: Since we only get here after a successful path through the
    // XDAQ SOAP callback system, the SOAP command name extraction
    // should not pose any problem. Extracting the parameters can prove
    // tricky though.

    std::string errMsg = "";
    std::string commandName = "undefined";
    bool haveCommandName = false;
    bool foundProblem = false;
    try {
        commandName = extractSOAPCommandName(msg);
        haveCommandName = true;
    }
    catch (xcept::Exception &err) {
        foundProblem = true;
        std::string const msgBase = "Failed to extract command name from SOAP message";
        ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
        errMsg = msgBase + ": '" + err.message() + "'.";
    }

    // uint32_t rcmsSessionId = 0;
    // bool haveRCMSSessionId = false;
    // try
    //   {
    //     rcmsSessionId = extractSOAPCommandRCMSSessionId(msg);
    //     haveRCMSSessionId = true;
    //   }
    // catch(xcept::Exception& err)
    //   {
    //     foundProblem = true;
    //     std::string const msgBase = "Failed to extract RCMS session ID attribute from SOAP message";
    //     ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
    //     errMsg = msgBase + ": '" + err.message() + "'.";
    //   }

    // std::string requestorId = "undefined";
    // bool haveRequestorId = false;
    // try
    //   {
    //     requestorId = extractSOAPCommandRequestorId(msg);
    //     haveRequestorId = true;
    //   }
    // catch(xcept::Exception& err)
    //   {
    //     foundProblem = true;
    //     std::string const msgBase = "Failed to extract requestor ID attribute from SOAP message";
    //     ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
    //     errMsg = msgBase + ": '" + err.message() + "'.";
    //   }

    // Extracting Requestor ID

    pixel::utils::OwnerId requestorId;
    requestorId=pixel::utils::OwnerId(1,"dummysession");

    bool haveRequestorId = false;
    try {
        requestorId = extractSOAPCommandOwnerId(msg);
        haveRequestorId = true;
    }
    catch (xcept::Exception &err) {
        //foundProblem = true;
        //for now we ignore the session ID

        haveRequestorId = true;
        std::string const msgBase = "Failed to extract requestor ID attribute from SOAP message";
        ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
        errMsg = msgBase + ": '" + err.message() + "'.";
    }
    if (foundProblem) {
        // Somehow we don't understand the SOAP message. Log an error
        // and flag that we are having trouble.
        std::string histMsg = "Ignoring ununderstood FSM SOAP command";
        if (haveCommandName) {
            histMsg += " '" + commandName + "'";
        }
        // if (haveRCMSSessionId)
        //   {
        //     histMsg += toolbox::toString(" RCMS session ID: '%u'.",
        //                                  rcmsSessionId);
        //   }
        if (haveRequestorId) {
            histMsg += " Requestor: " + requestorId.asString();
        }
        xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
        std::string const fullMsg = histMsg + " " + errMsg;
        ERROR(fullMsg);
        XCEPT_DECLARE(pixel::exception::FSMTransitionProblem,
                      top,
                      fullMsg);
        xdaqAppP_->notifyQualified("error", top);
        pixel::utils::SOAPFaultCodeType faultCode = pixel::utils::SOAPFaultCodeSender;
        std::string const faultString = histMsg.c_str();
        std::string const faultDetail = errMsg.c_str();
        xoap::MessageReference reply =
            makeSOAPFaultReply(msg,
                               faultCode,
                               faultString,
                               faultDetail);
        return reply;
    }

    //----------

    // Extracting Global Key / Run Key if available from SOAP

    std::string GlobalKey = "-1";
    std::string RunKey = "None";

    //bool haveGlobalKey = false;
    //bool haveRunKey = false;

    try{
        GlobalKey = extractSOAPCommandAttribute(msg, "GlobalKey");
        //haveGlobalKey = true;
    }
    catch (xcept::Exception &err){
        DEBUG("No Global Key sent with command " + commandName);
    }

    try{
        RunKey = extractSOAPCommandAttribute(msg, "RunKey");
        //haveRunKey = true;
    }
    catch (xcept::Exception &err){
        DEBUG("No Run Key sent with command " + commandName);
    }

    if (GlobalKey == "")
        GlobalKey = "-1";
    if (RunKey == "")
        RunKey = "None";

    //if (haveGlobalKey)
    //    INFO("Global Key sent for command : " + commandName + " is " + GlobalKey);
    //if (haveRunKey)
    //    INFO("Run Key sent for command : " + commandName + " is " + RunKey);
    if (commandName == "Configure" || commandName == "Reconfigure"){
        xdaqAppP_->cfgInfoSpaceP_->setString("GlobalKey",GlobalKey);
        xdaqAppP_->cfgInfoSpaceP_->setString("RunKey",RunKey);
    }
    //----------

    std::string RunNumber = "0";
    //bool haveRunNumber = false;

    try{
        RunNumber = extractSOAPCommandAttribute(msg, "RUN_NUMBER");
        //haveRunNumber = true;
    }
    catch (xcept::Exception &err){
        DEBUG("No Run Number sent with command " + commandName);
    }

    if (RunNumber == "")
        RunNumber = "0";

    //if (haveRunNumber)
    //    INFO("Run Number sent for command : " + commandName + " is " + RunNumber);

    if (commandName == "Start")
        xdaqAppP_->cfgInfoSpaceP_->setUInt32("runNumber",atoi((RunNumber).c_str()));

    //----------

    // Now a little bit of checking; we should have received at least
    // one of 'xdaq:rcmsSessionId' or 'xdaq:actionrequestorId'.
    if (!requestorId.isValid()) {
        std::string const histMsg =
            toolbox::toString("Ignoring FSM SOAP command '%s' from unidentified requestor.",
                              commandName.c_str());
        // if (rcmsSessionId != 0)
        //   {
        //     histMsg += toolbox::toString(" RCMS session ID: '%u'.",
        //                                  rcmsSessionId);
        //   }
        // if (!requestorId.empty())
        //   {
        //     histMsg += " Requestor: '" + requestorId + "'";
        //   }
        xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
        std::string const errMsg =
            "The '" + commandName + "' SOAP command requires at least one"
                                    " of the 'xdaq:rcmsSessionId' or 'xdaq:actionrequestorId' attributes.";
        std::string const fullMsg = histMsg + " " + errMsg;
        ERROR(fullMsg);
        XCEPT_DECLARE(pixel::exception::FSMTransitionProblem,
                      top,
                      fullMsg);
        xdaqAppP_->notifyQualified("error", top);
        pixel::utils::SOAPFaultCodeType faultCode = pixel::utils::SOAPFaultCodeSender;
        std::string const faultString = histMsg.c_str();
        std::string const faultDetail = errMsg.c_str();
        xoap::MessageReference reply =
            makeSOAPFaultReply(msg,
                               faultCode,
                               faultString,
                               faultDetail);
        return reply;
    }

    //----------

    // // Now do a little bit of data-massage: fill the hardware lease
    // // owner based on the RCMS session ID if only the latter was
    // // specified.
    // if (requestorId.empty())
    //   {
    //     requestorId = toolbox::toString("RCMS_session_%u", rcmsSessionId);
    //     INFO("No actionRequestorId specified. "
    //          "Setting actionRequestorId to " << requestorId.asString()
    //          << " based on rcmsSessionId.");
    //   }

    //----------

    INFO(toolbox::toString("Received FSM transition command '%s' from %s.",
                           commandName.c_str(),
                           requestorId.asString().c_str()));

    //----------

    // Figure out if the current FSM command is allowed, based on our
    // (lack of) hardware lease.

    // First question: Is there a lease owner currently?
    if (xdaqAppP_->isHwLeased() ||
        (xdaqAppP_->wasHwLeased() && (requestorId == xdaqAppP_->getExpiredHwLeaseOwnerId()))) {
        // Second question: Who is the current lease owner? Or who was
        // the owner who lost his lease?
        pixel::utils::OwnerId leaseOwnerId;
        if (xdaqAppP_->isHwLeased()) {
            leaseOwnerId = xdaqAppP_->getHwLeaseOwnerId();
        } else if (xdaqAppP_->wasHwLeased()) {
            leaseOwnerId = xdaqAppP_->getExpiredHwLeaseOwnerId();
        }

        // Third question: Does the current request come from our
        // hardware owner?
        if (requestorId != leaseOwnerId) {
            std::string const histMsg =
                toolbox::toString("Ignoring state transition command '%s' requested by %s: "
                                  "the current hardware lease owner is %s.",
                                  commandName.c_str(),
                                  requestorId.asString().c_str(),
                                  leaseOwnerId.asString().c_str());
            xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
            // No match -> send error back.
            std::string msgBase = histMsg;
            ERROR(msgBase);
            XCEPT_DECLARE(pixel::exception::FSMTransitionProblem, top, msgBase);
            xdaqAppP_->notifyQualified("error", top);
            pixel::utils::SOAPFaultCodeType faultCode = pixel::utils::SOAPFaultCodeReceiver;
            std::string faultString = msgBase;
            xoap::MessageReference reply =
                makeSOAPFaultReply(msg,
                                   faultCode,
                                   faultString);
            return reply;
        }
    } else {
        // No lease holder, and no expired lease either. In this case we
        // can either 'ColdReset', 'Initialize', 'Configure' (the usual path), or
        // 'Halt' (to recover from a lost RunControl session). Any other
        // request will result in an error.
        if ((commandName != "ColdReset") &&
            (commandName != "Initialize") &&
            (commandName != "Configure") &&
            (commandName != "Halt")) {
            std::string const histMsg =
                toolbox::toString("Ignoring state transition command '%s' requested by %s: "
                                  "there is currently no hardware lease owner. "
                                  "Without hardware lease holder "
                                  "only 'ColdReset', 'Initialize', 'Configure', and 'Halt' "
                                  "FSM commands are allowed.",
                                  commandName.c_str(),
                                  requestorId.asString().c_str());
            xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
            std::string msgBase = histMsg;
            ERROR(msgBase);
            XCEPT_DECLARE(pixel::exception::FSMTransitionProblem, top, msgBase);
            xdaqAppP_->notifyQualified("error", top);
            pixel::utils::SOAPFaultCodeType faultCode = pixel::utils::SOAPFaultCodeReceiver;
            std::string faultString = msgBase;
            xoap::MessageReference reply =
                makeSOAPFaultReply(msg,
                                   faultCode,
                                   faultString);
            return reply;
        }
    }

    //----------

    // Once we get here, let's see if the requested state transition is
    // a valid one for the current state.
    std::map<std::string, toolbox::fsm::State> allowedTransitions =
        fsmP_->getTransitions(fsmP_->getCurrentState());
    if (allowedTransitions.find(commandName) == allowedTransitions.end()) {
        std::string const histMsg =
            toolbox::toString("Ignoring invalid state transition '%s' from state '%s', "
                              "requested by %s.",
                              commandName.c_str(),
                              getCurrentStateName().c_str(),
                              requestorId.asString().c_str());
        xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
        std::string msgBase = histMsg;
        ERROR(msgBase);
        XCEPT_DECLARE(pixel::exception::FSMTransitionProblem, top, msgBase);
        xdaqAppP_->notifyQualified("error", top);
        pixel::utils::SOAPFaultCodeType faultCode = pixel::utils::SOAPFaultCodeReceiver;
        std::string faultString = msgBase;
        xoap::MessageReference reply =
            makeSOAPFaultReply(msg,
                               faultCode,
                               faultString);
        return reply;
    }

    //----------

    try {
        xdaqAppP_->loadSOAPCommandParameters(msg);
    }
    catch (xcept::Exception &err) {
        std::string const histMsg =
            toolbox::toString("Ignoring SOAP command '%s' requested by %s: "
                              "failed to import the command parameters: '%s'.",
                              commandName.c_str(),
                              requestorId.asString().c_str(),
                              err.what());
        xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
        std::string msgBase = histMsg;
        ERROR(toolbox::toString("%s: '%s'.",
                                msgBase.c_str(),
                                xcept::stdformat_exception_history(err).c_str()));
        XCEPT_DECLARE_NESTED(pixel::exception::FSMTransitionProblem, top,
                             toolbox::toString("%s.", msgBase.c_str()), err);
        xdaqAppP_->notifyQualified("error", top);
        pixel::utils::SOAPFaultCodeType faultCode = pixel::utils::SOAPFaultCodeSender;
        std::string faultString = msgBase.c_str();
        std::string faultDetail = err.message();
        xoap::MessageReference reply =
            makeSOAPFaultReply(msg,
                               faultCode,
                               faultString,
                               faultDetail);
        return reply;
    }

    //----------

    // If we get here, all is good. Take the lease and then perform the
    // state transition.

    // But first mark the transition in the application history.
    std::string histMsg = toolbox::toString("Received '%s' SOAP command from %s.",
                                            commandName.c_str(),
                                            requestorId.asString().c_str());
    xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);

    xdaqAppP_->assignHwLease(requestorId);
    try {
        toolbox::Event::Reference event(new toolbox::Event(commandName, this));
        fsmP_->fireEvent(event);
    }
    catch (toolbox::fsm::exception::Exception &err) {
        std::string const histMsg =
            toolbox::toString("Ignoring SOAP command '%s' requested by %s: "
                              "failed to execute the corresponding FSM transition.",
                              commandName.c_str(),
                              requestorId.asString().c_str());
        xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
        std::string msgBase = histMsg;
        ERROR(toolbox::toString("%s: %s.",
                                msgBase.c_str(),
                                xcept::stdformat_exception_history(err).c_str()));
        XCEPT_DECLARE_NESTED(pixel::exception::FSMTransitionProblem, top,
                             toolbox::toString("%s.", msgBase.c_str()), err);
        xdaqAppP_->notifyQualified("error", top);
        pixel::utils::SOAPFaultCodeType faultCode = pixel::utils::SOAPFaultCodeSender;
        std::string faultString = toolbox::toString("Failed to fire '%s' event.",
                                                    commandName.c_str());
        std::string faultDetail = toolbox::toString("%s: %s.",
                                                    msgBase.c_str(),
                                                    err.message().c_str());
        std::string faultActor = xdaqAppP_->getFullURL();
        xoap::MessageReference reply =
            makeSOAPFaultReply(msg,
                               faultCode,
                               faultString,
                               faultDetail);
        return reply;
    }

    //----------

    // Once we get here, the state transition has been triggered. Notify
    // the requestor that so far everything is fine. Any command 'X'
    // replies with 'XResponse'. Upon arrival in the next state (e.g.,
    // 'Configuring' for 'Configure') an asynchronous notification is
    // sent to RunControl with the name of the new state.
    std::string soapProtocolVersion = msg->getImplementationFactory()->getProtocolVersion();
    try {
        xoap::MessageReference reply =
            makeCommandSOAPReply(soapProtocolVersion,
                                 commandName,
                                 commandName+"Done"
                                 //fsmP_->getStateName(fsmP_->getCurrentState())
                                 );
        return reply;
    }
    catch (xcept::Exception &err) {
        std::string msgBase =
            toolbox::toString("Failed to create FSM SOAP reply for command '%s'",
                              commandName.c_str());
        ERROR(toolbox::toString("%s: %s.",
                                msgBase.c_str(),
                                xcept::stdformat_exception_history(err).c_str()));
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top,
                             toolbox::toString("%s.", msgBase.c_str()), err);
        xdaqAppP_->notifyQualified("error", top);
        XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
    }

    //----------

    // Should only get here in case the above SOAP reply fails. In that
    // case: return an empty message.
    return xoap::createMessage();
}

void
pixel::utils::FSMPixel::stateChangedWithNotification(toolbox::fsm::FiniteStateMachine &fsm) {
    logStateChangeDetails();

    //----------

    std::string const stateName = getCurrentStateName();
    uint32_t const runNumber = xdaqAppP_->cfgInfoSpaceP_->getUInt32("runNumber");

    // Mark the transition in the application history.
    std::string histMsg = toolbox::toString("FSM state changed to '%s'.",
                                            stateName.c_str());
    if (stateName == "Started") {
        histMsg.append(toolbox::toString(" (run #%" PRIu32 ")", runNumber));
    }
    xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
    xdaqAppP_->appStateInfoSpace_.setFSMState(stateName);

    //notifyRCMS(stateName, "Normal state change.");
    Attributes parameters;
    parameters["Supervisor"] = xdaqAppP_->getApplicationDescriptor()->getClassName();
    parameters["Instance"] = pixel::utils::to_string(xdaqAppP_->getApplicationDescriptor()->getInstance());
    parameters["FSMState"] = stateName;

    xoap::MessageReference  msg = makeSOAPMessageReference("FSMStateNotification", parameters);
    INFO(xdaqAppP_->getApplicationDescriptor()->getInstance());

    //addSOAPReplyParameter(msg,"Instance",xdaqAppP_->getApplicationDescriptor()->getInstance());
    //addSOAPReplyParameter(msg,"FSMState",stateName);
    const xdaq::ApplicationDescriptor *PixelSupervisor_=0;
    try {
        PixelSupervisor_ = xdaqAppP_->getApplicationContext()->getDefaultZone()->getApplicationGroup("daq")->getApplicationDescriptor("PixelSupervisor", 0);
        INFO("FSMPixel::stateChangedWithNotification - Instance 0 of PixelSupervisor found.");
    }
    catch (xdaq::exception::Exception &e) {
        ERROR("FSMPixel::stateChangedWithNotification - Instance 0 of PixelSupervisor not found!");
    }
    sendWithSOAPReply(PixelSupervisor_, msg);
    //----------

    // Check if from this state an automatic 'forward' should be
    // performed
    std::map<std::string, std::string>::const_iterator const
    tmp = automaticTransitions_.find(stateName);
    if (tmp != automaticTransitions_.end()) {
        std::string const commandName = tmp->second;
        DEBUG("'"
              << stateName
              << "' is an intermediate/auto-forward state --> forwarding to '"
              << commandName
              << "'");

        try {
            toolbox::Event::Reference event(new toolbox::Event(commandName, this));
            fsmP_->fireEvent(event);
        }
        catch (toolbox::fsm::exception::Exception &err) {
            std::string msgBase =
                toolbox::toString("Problem executing the FSM '%s' command",
                                  commandName.c_str());
            ERROR(toolbox::toString("%s: %s.",
                                    msgBase.c_str(),
                                    xcept::stdformat_exception_history(err).c_str()));
            XCEPT_DECLARE_NESTED(pixel::exception::FSMTransitionProblem, top,
                                 toolbox::toString("%s.", msgBase.c_str()), err);
            xdaqAppP_->notifyQualified("error", top);
            XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
        }
    }
}

void
pixel::utils::FSMPixel::stateChangedToFailedWithNotification(toolbox::fsm::FiniteStateMachine &fsm) {
    logStateChangeDetails();

    // Mark the transition in the application history.
    std::string const msg = toolbox::toString("FSM state changed to '%s'.",
                                              getCurrentStateName().c_str());
    xdaqAppP_->appStateInfoSpace_.addHistoryItem(msg);

    ERROR("State has changed to 'Failed.'");

    // NOTE: Don't notify RCMS. This has already happened from
    // gotoFailed(), where more detailed failure information is
    // available.
}

void
pixel::utils::FSMPixel::coldReset() {
    toolbox::Event::Reference const event(new toolbox::Event("ColdReset", this));
    fsmP_->fireEvent(event);
}

void
pixel::utils::FSMPixel::configure() {
    toolbox::Event::Reference const event(new toolbox::Event("Configure", this));
    fsmP_->fireEvent(event);
}

void
pixel::utils::FSMPixel::reconfigure() {
    toolbox::Event::Reference const event(new toolbox::Event("Reconfigure", this));
    fsmP_->fireEvent(event);
}

void
pixel::utils::FSMPixel::enable() {
    toolbox::Event::Reference const event(new toolbox::Event("Enable", this));
    fsmP_->fireEvent(event);
}

void
pixel::utils::FSMPixel::start() {
    toolbox::Event::Reference const event(new toolbox::Event("Start", this));
    fsmP_->fireEvent(event);
}

void
pixel::utils::FSMPixel::pause() {
    toolbox::Event::Reference const event(new toolbox::Event("Pause", this));
    fsmP_->fireEvent(event);
}

void
pixel::utils::FSMPixel::resume() {
    toolbox::Event::Reference const event(new toolbox::Event("Resume", this));
    fsmP_->fireEvent(event);
}

void
pixel::utils::FSMPixel::stop() {
    toolbox::Event::Reference const event(new toolbox::Event("Stop", this));
    fsmP_->fireEvent(event);
}

void
pixel::utils::FSMPixel::halt() {
    toolbox::Event::Reference const event(new toolbox::Event("Halt", this));
    fsmP_->fireEvent(event);
}

void
pixel::utils::FSMPixel::fail() {
    toolbox::Event::Reference const event(new toolbox::Event("Fail", this));
    fsmP_->fireEvent(event);
}

std::string
pixel::utils::FSMPixel::getCurrentStateName() const {
    toolbox::fsm::State currentState = fsmP_->getCurrentState();
    std::string stateName = fsmP_->getStateName(currentState);
    return stateName;
}

void
pixel::utils::FSMPixel::gotoFailed(std::string const &reason) {
    xdaqAppP_->appStateInfoSpace_.setFSMState("Failed", reason);
    ERROR("Going to 'Failed' state. Reason: '" << reason << "'.");

    // Notify RCMS from here, since here we still have the detailed
    // failure information.
    std::string const className =
        xdaqAppP_->getApplicationDescriptor()->getClassName();
    std::string const serviceName =
        xdaqAppP_->getApplicationDescriptor()->getAttribute("service");
    std::string const rcmsMsg =
        toolbox::toString("%s for service '%s' failed: '%s'. More detail at: %s.",
                          className.c_str(),
                          serviceName.c_str(),
                          reason.c_str(),
                          xdaqAppP_->getFullURL().c_str());
    notifyRCMS(fsmP_->getStateName('F'), rcmsMsg);

    // Raise an exception so the FSM indeed goes to 'Failed.'
    XCEPT_RAISE(toolbox::fsm::exception::Exception, reason);
}

void
pixel::utils::FSMPixel::gotoFailed(xcept::Exception &err) {
    std::string reason = err.message();
    gotoFailed(reason);
}

void
pixel::utils::FSMPixel::invalidStateTransitionAction(toolbox::Event::Reference event) {
    toolbox::fsm::InvalidInputEvent &invalidInputEvent =
        dynamic_cast<toolbox::fsm::InvalidInputEvent &>(*event);
    std::string const fromState = fsmP_->getStateName(invalidInputEvent.getFromState());
    std::string const toState = invalidInputEvent.getInput();
    std::string const histMsg =
        toolbox::toString("Ignoring invalid state transition '%s' from state '%s'.",
                          toState.c_str(),
                          fromState.c_str());
    xdaqAppP_->appStateInfoSpace_.addHistoryItem(histMsg);
    std::string msgBase = histMsg;
    std::string const msg =
        toolbox::toString("%s I'm not deaf, "
                          "I'm just ignoring you.",
                          msgBase.c_str());
    ERROR(msg);
}

void
pixel::utils::FSMPixel::notifyRCMS(std::string const &stateName, std::string const &msg) {
    // Notify RCMS of a state change.
    // NOTE: Should only be used for state _changes_.

    // NOTE: An empty RCMS URL signifies a wish to skip such
    // notifications.
    xdata::Serializable const *const tmp = xdaqAppP_->getApplicationInfoSpace()->find("rcmsStateListener");
    xdata::Bag<xdaq2rc::ClassnameAndInstance> const *const bag = dynamic_cast<xdata::Bag<xdaq2rc::ClassnameAndInstance> const *const>(tmp);
    xdata::String const *const tmpStr = dynamic_cast<xdata::String const *const>(bag->getField("url"));
    std::string const url = *tmpStr;

    if (url.empty()) {
        INFO("Not notifying RCMS of state change to '"
             << stateName
             << "' (since the RCMS URL was set to the empty string).");
    } else {
        INFO("Notifying RCMS of state change to '" << stateName << "'.");
        try {
            rcmsNotifier_.stateChanged(stateName, msg);
        }
        catch (xcept::Exception &err) {
            ERROR("Failed to notify RCMS of state change: "
                  << xcept::stdformat_exception_history(err));
            XCEPT_DECLARE_NESTED(pixel::exception::RCMSNotificationError, top,
                                 "Failed to notify RCMS of state change.", err);
            xdaqAppP_->notifyQualified("error", top);
        }
    }
}

void
pixel::utils::FSMPixel::reset(std::string const &msg) {
    fsmP_->reset();
    std::string stateName = fsmP_->getStateName(fsmP_->getCurrentState());
    xdaqAppP_->appStateInfoSpace_.setFSMState(stateName);
    notifyRCMS(getCurrentStateName(), msg);
}

void
pixel::utils::FSMPixel::logStateChangeDetails() const {
    std::string const className =
        xdaqAppP_->getApplicationDescriptor()->getClassName();
    std::string const serviceName =
        xdaqAppP_->getApplicationDescriptor()->getAttribute("service");
    std::string const stateName = getCurrentStateName();
    uint32_t const runNumber = xdaqAppP_->cfgInfoSpaceP_->getUInt32("runNumber");

    INFO("State for " << className << "' " << serviceName << "' has changed."
                      << " Current state is now '" << stateName << "'.");
    INFO("RunControl session in charge: '"
         << xdaqAppP_->appStateInfoSpace_.getUInt32("rcmsSessionId")
         << "'.");
    INFO("Hardware lease owner: '"
         << xdaqAppP_->appStateInfoSpace_.getString("hwLeaseOwnerId")
         << "'.");
    INFO("Run number: " << runNumber << ".");
}

std::set<std::string>
pixel::utils::FSMPixel::listOfAllowedStates() {
  std::set<std::string> clickableInputs = fsmP_->getInputs(fsmP_->getCurrentState());
  return clickableInputs;
}

std::set<std::string>
pixel::utils::FSMPixel::listOfAllStates() {
  std::set<std::string> allInputs = fsmP_->getInputs();
  return allInputs;
}

bool
pixel::utils::FSMPixel::setMessage(xoap::MessageReference const &msg, bool received){
    return xdaqAppP_->setMessage(msg, received);
}
