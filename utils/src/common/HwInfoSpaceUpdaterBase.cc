#include "pixel/utils/HwInfoSpaceUpdaterBase.h"

#include <string>

#include "pixel/hwlayer/DeviceBase.h"
#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/XDAQAppBase.h"

pixel::utils::HwInfoSpaceUpdaterBase::HwInfoSpaceUpdaterBase(pixel::utils::XDAQAppBase &xdaqApp,
                                                             pixel::hwlayer::DeviceBase const &hw)
    : pixel::utils::InfoSpaceUpdater(xdaqApp),
      pixel::utils::PIXELObject(xdaqApp),
      hw_(hw) {
}

pixel::utils::HwInfoSpaceUpdaterBase::~HwInfoSpaceUpdaterBase() {
}

void
pixel::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceImpl(InfoSpaceHandler *const infoSpaceHandler) {
    pixel::hwlayer::DeviceBase const &hw = getHw();
    if (hw.isReadyForUse()) {
        InfoSpaceHandler::ItemVec &items = infoSpaceHandler->getItems();
        InfoSpaceHandler::ItemVec::iterator iter;

        for (iter = items.begin(); iter != items.end(); ++iter) {
            try {
                updateInfoSpaceItem(*iter, infoSpaceHandler);
            }
            catch (...) {
                iter->setInvalid();
                throw;
            }
        }

        // Now sync everything from the cache to the InfoSpace itself.
        infoSpaceHandler->writeInfoSpace();
    } else {
        infoSpaceHandler->setInvalid();
    }
}

bool
pixel::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(pixel::utils::InfoSpaceItem &item,
                                                          pixel::utils::InfoSpaceHandler *const infoSpaceHandler) {
    bool updated = false;
    pixel::hwlayer::DeviceBase const &hw = getHw();
    if (hw.isReadyForUse()) {
        std::string name = item.name();
        pixel::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
        if (updateType == pixel::utils::InfoSpaceItem::HW32) {
            uint32_t const tmp = hw_.readRegister(item.hwName());
            pixel::utils::InfoSpaceItem::ItemType itemType = item.type();
            if (itemType == pixel::utils::InfoSpaceItem::UINT32) {
                infoSpaceHandler->setUInt32(name, tmp);
                updated = true;
            } else if (itemType == pixel::utils::InfoSpaceItem::BOOL) {
                infoSpaceHandler->setBool(name, (tmp != 0));
                updated = true;
            }
        }
        if (!updated) {
            updated = pixel::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

    if (updated) {
        item.setValid();
    } else {
        item.setInvalid();
    }

    return updated;
}

pixel::hwlayer::DeviceBase const &
pixel::utils::HwInfoSpaceUpdaterBase::getHw() const {
    return hw_;
}
