#include "pixel/utils/Lock.h"

pixel::utils::Lock::Lock(toolbox::BSem::State state, bool recursive)
    : semaphore_(state, recursive) {
}

pixel::utils::Lock::~Lock() {
    unlock();
}

void
pixel::utils::Lock::lock() {
    semaphore_.take();
}

void
pixel::utils::Lock::unlock() {
    semaphore_.give();
}
