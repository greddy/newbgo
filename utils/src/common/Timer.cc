#include <pixel/utils/Timer.h>
#include "pixel/utils/Utils.h"
#include "log4cplus/logger.h"
#include "pixel/utils/LogMacros.h"
#include <math.h>
#include <sys/time.h>
#include <ctime>

pixel::utils::Timer::Timer(log4cplus::Logger *p_sv_logger_) {
    ntimes_ = 0;
    ttot_ = 0.0;
    ttotsq_ = 0.0;
    
    sv_logger_=p_sv_logger_;
    startflag_ = false;
    verbose_ = false;
    name_ = "";
}

pixel::utils::Timer::Timer(): sv_logger_(0) {
    ntimes_ = 0;
    ttot_ = 0.0;
    ttotsq_ = 0.0;

    startflag_ = false;
    verbose_ = false;
    name_ = "";
    sv_logger_=0;
}

pixel::utils::Timer::~Timer() {}

void pixel::utils::Timer::start(const std::string msg) {
    gettimeofday(&tstart_, 0);

    startflag_ = true;
    if (verbose_)
        printTime(tstart_, msg);
}

void pixel::utils::Timer::reset() {
    ntimes_ = 0;
    ttot_ = 0.0;
    ttotsq_ = 0.0;
    startflag_ = false;
}

void pixel::utils::Timer::stop(const std::string msg) {
    startflag_ = false;
    timeval tstop;
    gettimeofday(&tstop, 0);
    if (verbose_)
        printTime(tstop, msg);
    float tsec = tstop.tv_sec - tstart_.tv_sec;
    float tusec = tstop.tv_usec - tstart_.tv_usec;
    if (tusec < 0) {
        tusec += 1000000.0;
        tsec -= 1.0;
    }
    double tmp = tsec + tusec / 1000000.0;
    ttot_ += tmp;
    ttotsq_ += tmp * tmp;
    ntimes_++;
}

double pixel::utils::Timer::elapsedtime() { //return time since last start in seconds
    double deltat = 0.;
    if (!startflag_)
        return deltat;
    timeval tnow;
    gettimeofday(&tnow, 0);
    if (verbose_)
        printTime(tnow);
    float tsec = tnow.tv_sec - tstart_.tv_sec;
    float tusec = tnow.tv_usec - tstart_.tv_usec;
    if (tusec < 0) {
        tusec += 1000000.0;
        tsec -= 1.0;
    }
    deltat = tsec + tusec / 1000000.0;
    return deltat;
}

void pixel::utils::Timer::restart() {
    stop();
    start();
}

unsigned int pixel::utils::Timer::ntimes() const {
    return ntimes_;
}

double pixel::utils::Timer::tottime() const {
    return ttot_;
}

double pixel::utils::Timer::avgtime() const {
    if (0 == ntimes_)
        return 0;
    return ttot_ / ntimes_;
}

double pixel::utils::Timer::rms() const {
    if (0 == ntimes_)
        return 0;
    return sqrt((ttot_ * ttot_ - ttotsq_)) / ntimes_;
}

std::string pixel::utils::Timer::printStats() const {
  std::string stats = name_+" total calls:"+pixel::utils::to_string(ntimes())+ " total time: "+pixel::utils::to_string(tottime())+ " avg time: "+pixel::utils::to_string(avgtime())+"\n";
  return stats;
}

std::string pixel::utils::Timer::printTime(const std::string msg) {
  std::string time_now;
    timeval tnow;
    gettimeofday(&tnow, 0);
    char buffer[10];
    time_t mytime = tnow.tv_sec;
    struct tm *timeinfo = gmtime(&mytime);
    strftime(buffer, 10, "%H:%M:%S", timeinfo);
    time_now = "["+name_+": "+msg+"] " +buffer+" "+pixel::utils::to_string(tnow.tv_usec)+"\n";
    return time_now;
    //printTime(tnow, msg);
}

void pixel::utils::Timer::printTime(const timeval t, const std::string msg) const {
    char buffer[10];
    time_t mytime = t.tv_sec;
    struct tm *timeinfo = gmtime(&mytime);
    strftime(buffer, 10, "%H:%M:%S", timeinfo);
    //sv_logger_
    if(sv_logger_==0){
        std::cout << "[" << name_ << ": " << msg << "] " << buffer << " " << t.tv_usec << std::endl;
    }else{
        log4cplus::Logger logger_(*sv_logger_);
        INFO("[" << name_ << ": " << msg << "] " << buffer << " " << t.tv_usec);
    }
}
