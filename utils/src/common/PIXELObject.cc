#include "pixel/utils/PIXELObject.h"

#include "pixel/utils/XDAQAppBase.h"

pixel::utils::PIXELObject::PIXELObject(pixel::utils::XDAQAppBase &owner)
    : owner_(owner) {
}

pixel::utils::PIXELObject::~PIXELObject() {
}

pixel::utils::XDAQAppBase &
pixel::utils::PIXELObject::getOwnerApplication() const {
    return owner_;
}
