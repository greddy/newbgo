#include "pixel/utils/ConfigurationInfoSpaceHandler.h"

#include "toolbox/string.h"
#include "xdaq/Application.h"
// OBSOLETE OBSOLETE OBSOLETE
//#include "xdata/Event.h"
// OBSOLETE OBSOLETE OBSOLETE end

#include "pixel/hwlayer/RegDumpConfigurationProcessor.h"
#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/WebServer.h"
#include "pixel/utils/WebTableNoLabels.h"

pixel::utils::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application &xdaqApp,
                                                                           std::string const &name)
    : pixel::utils::InfoSpaceHandler(xdaqApp, name, 0, xdaqApp.getApplicationInfoSpace(), true) {
    // Create all InfoSpace variables.

    // The monitoring update interval. Stored as string, so we can have
    // nice formats like described here:
    //   http://www.w3schools.com/schema/schema_dtypes_date.asp.
    createString("monitoringInterval", "PT1S", "", InfoSpaceItem::NOUPDATE, true);

    // The hardware lease expiration period. Same format as above.
    createString("hardwareLeaseDuration", "PT10S", "", InfoSpaceItem::NOUPDATE, true);

    // This is the path to the text file containing the default hardware
    // configuration.
    // NOTE: This needs to be set on a per-application basis in order to
    // point to the right file for the hardware in question.
    createString("defaultHwConfigurationFilePath",
                 "unset",
                 "",
                 InfoSpaceItem::NOUPDATE,
                 true);

    //----------

    // The CMS DAQ run number.
    createUInt32("runNumber", 0, "", InfoSpaceItem::NOUPDATE, true);

    // The Global Key we received by SOAP.
    createString("GlobalKey", "0", "", InfoSpaceItem::NOUPDATE, true);

    // The Run Key we received by SOAP
    createString("RunKey", "0", "", InfoSpaceItem::NOUPDATE, true);

    // The various versions of the hardware configuration string.
    std::string const tmp = toolbox::toString("%c Hardware configuration string not yet set.",
                                              pixel::hwlayer::RegDumpConfigurationProcessor::kCommentChar);

    // The default hardwareConfigurationString (read from file).
    createString("hardwareConfigurationStringDefault", tmp, "", InfoSpaceItem::NOUPDATE, true);

    // The hardwareConfigurationString we received by SOAP.
    createString("hardwareConfigurationStringReceived", tmp, "", InfoSpaceItem::NOUPDATE, true);

    // The hardwareConfigurationString applied to the hardware (i.e.,
    // after merging with the default settings).
    createString("hardwareConfigurationStringApplied", tmp, "", InfoSpaceItem::NOUPDATE, true);

    //----------

    // OBSOLETE OBSOLETE OBSOLETE
    // // Register to be notified when the XDAQ framework loads the
    // // configuration values from the XML file.
    // xdaqApp->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    // OBSOLETE OBSOLETE OBSOLETE end
}

pixel::utils::ConfigurationInfoSpaceHandler::~ConfigurationInfoSpaceHandler() {
}

// OBSOLETE OBSOLETE OBSOLETE
// void
// pixel::utils::ConfigurationInfoSpaceHandler::actionPerformed(xdata::Event& event)
// {
//   // This is called after all default configuration values have been
//   // loaded (from the XDAQ configuration file). This means the
//   // underlying InfoSpace has been modified -> resync our
//   // InfoSpaceHandler buffer to the underlying InfoSpace.
//   if (event.type() == "urn:xdaq-event:setDefaultValues")
//     {
//       readInfoSpace();
//     }
// }
// OBSOLETE OBSOLETE OBSOLETE end

void
pixel::utils::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(Monitor &monitor) {
    // Application configuration items.
    std::string itemSetName = "Application configuration";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "monitoringInterval",
                    "Monitoring update interval",
                    this);
    monitor.addItem(itemSetName,
                    "hardwareLeaseDuration",
                    "Hardware lease duration",
                    this);
    monitor.addItem(itemSetName,
                    "defaultHwConfigurationFilePath",
                    "Path to default hardware configuration file",
                    this);

    // Run configuration items (from RCMS).
    itemSetName = "Run configuration";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "runNumber",
                    "Run number",
                    this);
    //----------

    // Keys read from Configure command
    itemSetName = "Global Key";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "GlobalKey",
                    "Global Key",
                    this);

    itemSetName = "Run Key";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "RunKey",
                    "Run Key",
                    this);
    //---------

    // Hardware configuration items.
    itemSetName = "itemset-hardware-configuration-default";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "hardwareConfigurationStringDefault",
                    "Configuration string (default)",
                    this);
    itemSetName = "itemset-hardware-configuration-received";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "hardwareConfigurationStringReceived",
                    "Configuration string (received)",
                    this);
    itemSetName = "itemset-hardware-configuration-applied";
    monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "hardwareConfigurationStringApplied",
                    "Configuration string (applied)",
                    this);
}

void
pixel::utils::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(WebServer &webServer,
                                                                           Monitor &monitor,
                                                                           std::string const &forceTabName) {
    std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

    webServer.registerTab(tabName,
                          "Configuration parameters",
                          3);
    // webServer.registerTable("Application configuration",
    //                         "Application configuration parameters",
    //                         monitor,
    //                         "Application configuration",
    //                         tabName,
    //                         1);
    webServer.registerTable("Run configuration",
                            "Run configuration parameters (from RCMS/SOAP commands)",
                            monitor,
                            "Run configuration",
                            tabName,
                            1);
    // webServer.registerSpacer("dummy", "", monitor, "", tabName, 1);
    webServer.registerTable("Global Key",
                            "Global Key from Configure command",
                            monitor,
                            "Global Key",
                            tabName,
                            1);
   // webServer.registerSpacer("dummy", "", monitor, "", tabName, 1);

    // webServer.registerTable("Run Key",
    //                         "Run Key from Configure command",
    //                         monitor,
    //                         "Run Key",
    //                         tabName,
    //                         1);

    // webServer.registerWebObject<pixel::utils::WebTableNoLabels>("Hardware configuration (default)",
    //                                                             "Default hardware configuration parameters read from file",
    //                                                             monitor,
    //                                                             "itemset-hardware-configuration-default",
    //                                                             tabName);
    // webServer.registerWebObject<pixel::utils::WebTableNoLabels>("Hardware configuration (received)",
    //                                                             "Hardware configuration parameters received by SOAP",
    //                                                             monitor,
    //                                                             "itemset-hardware-configuration-received",
    //                                                             tabName);
    // webServer.registerWebObject<pixel::utils::WebTableNoLabels>("Hardware configuration (applied)",
    //                                                             "Final configuration parameters applied to the hardware",
    //                                                             monitor,
    //                                                             "itemset-hardware-configuration-applied",
    //                                                             tabName);
}
