#include "pixel/utils/FSMStateSummaryUpdater.h"

#include <string>

#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/XDAQAppWithFSMPixelBase.h"

pixel::FSMStateSummaryUpdater::FSMStateSummaryUpdater(pixel::utils::XDAQAppBase& xdaqApp) :
  pixel::utils::InfoSpaceUpdater(xdaqApp),
  xdaqApp_(xdaqApp)
  {
  }

bool pixel::FSMStateSummaryUpdater::updateInfoSpaceItem(pixel::utils::InfoSpaceItem& item,
                                                        pixel::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;

  std::string name = item.name();
  pixel::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
  if (updateType == pixel::utils::InfoSpaceItem::PROCESS) {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
    if (name == "fsmState") {
      pixel::utils::XDAQAppWithFSMPixelBase& AppBase = dynamic_cast<pixel::utils::XDAQAppWithFSMPixelBase&>(xdaqApp_);
      std::string res = AppBase.getCurrentStateName();
      infoSpaceHandler->setString(name, res);
      updated = true;
    }
    else if (name == "allowedStates") {
      pixel::utils::XDAQAppWithFSMPixelBase& AppBase = dynamic_cast<pixel::utils::XDAQAppWithFSMPixelBase&>(xdaqApp_);
      std::set<std::string> allowedFSMStatesSet = AppBase.listOfAllowedStates();
      allowedFSMStatesSet.erase("Fail");
      std::vector<std::string> allowedFSMStatesVec( allowedFSMStatesSet.begin(), allowedFSMStatesSet.end() );
      infoSpaceHandler->setStringVec(name, allowedFSMStatesVec);
      updated = true;
    }

  }
  if (!updated) {
    updated = pixel::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
  }
  if (updated) {
    item.setValid();
  }
  else {
    item.setInvalid();
  }
  return updated;
}
