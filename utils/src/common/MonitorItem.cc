#include "pixel/utils/MonitorItem.h"

pixel::utils::MonitorItem::MonitorItem(std::string const &name,
                                       std::string const &description,
                                       InfoSpaceHandler *const infoSpaceHandler,
                                       std::string const &docString)
    : name_(name),
      description_(description),
      infoSpaceHandlerP_(infoSpaceHandler),
      docString_(docString) {
}

std::string
pixel::utils::MonitorItem::getName() const {
    return name_;
}

std::string
pixel::utils::MonitorItem::getDescription() const {
    return description_;
}

pixel::utils::InfoSpaceHandler *
pixel::utils::MonitorItem::getInfoSpaceHandler() const {
    return infoSpaceHandlerP_;
}

std::string
pixel::utils::MonitorItem::getDocString() const {
    return docString_;
}
