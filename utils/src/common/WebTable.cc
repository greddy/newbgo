#include "pixel/utils/WebTable.h"

#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "toolbox/string.h"

#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"


pixel::utils::WebTable::WebTable(std::string const &name,
                                 std::string const &description,
                                 Monitor const &monitor,
                                 std::string const &itemSetName,
                                 std::string const &tabName,
                                 size_t const colSpan)
    : WebObject(name, description, monitor, itemSetName, tabName, colSpan) {
}

std::string
pixel::utils::WebTable::getHTMLString() const {
    // NOTE: This has been written to work with the new XDAQ12-style
    // HyperDAQ tabs and doT.js.


    std::stringstream res;

    res << "<div class=\"pixel-item-table-wrapper\">"
        << "\n";

    res << "<p class=\"pixel-item-table-title\">"
        << getName()
        << "</p>";
    res << "\n";

    std::string const desc = getDescription();
    if (!desc.empty()) {
        res << "<p class=\"pixel-item-table-description\">"
            << desc
            << "</p>";
        res << "\n";
    }

    res << "<table class=\"pixel-item-table xdaq-table\">"
        << "\n";

    res << "<tbody>";
    res << "\n";

    // And now the actual table contents.
    // NOTE: The binding is specific to doT.js.


    Monitor::StringPairVector items = monitor_.getFormattedItemSet(itemSetName_);
    Monitor::StringPairVector::const_iterator iter;


    for (iter = items.begin(); iter != items.end(); ++iter) {
        std::string const itemTitle = iter->first;
        std::string const tmp = "[\"" + toolbox::jsonquote(itemSetName_) + "\"][\"" + toolbox::jsonquote(iter->first) + "\"]";
        std::string const value = toolbox::unquote(iter->second);

        res << "<tr";
        // if (!value.empty()) {
        //     res << " class=\"xdaq-tooltip\"";
        // }
        res << ">";
        res << "<td class=\"pixel-item-header\" id=\""+ itemSetName_+"_table\">"
            << "<div>" << itemTitle << "</div>";
            if (!value.empty())
                res << "<span class=\"xdaq-tooltip-msg\">" << value << "</span>";
        res << "</td>";
        if (itemSetName_=="Application state"){
          res << "<td class=\"pixel-item-value\" id=\"itemset-application-state\"> ";
          res << "<script type=\"text/x-dot-template\">"<< "{{=it"<<tmp<<"}}"<< "</script>";
        }
        else {
            res << "<td class=\"pixel-item-value\" id=\""+ itemSetName_+"\"> ";
            res << "<script type=\"text/x-dot-template\">"<< "{{=it"<<tmp<<"}}"<< "</script>";
        }
        // res << "<div class=\"target\"></div>";

        if (!value.empty() ) {
          res << "<div class=\"target\"></div>";
        }
        res << "</td>"
            << "</tr>"
            << "\n";
    }
    // jQuery("script[type='text/x-dot-template']");

    res << "</tbody>";
    res << "\n";

    res << "</table>";
    res << "\n";

    res << "</div>";
    res << "\n";

    return res.str();
}
