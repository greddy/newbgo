#include "pixel/utils/LOGMsgHistoryHandler.h"

#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/WebServer.h"

#include "toolbox/string.h"

using namespace std;


pixel::utils::LOGMsgHistoryHandler::LOGMsgHistoryHandler(xdaq::Application& xdaqApp,
                                                              pixel::utils::InfoSpaceUpdater* updater,
                                                              log4cplus::LoggerHistory* loggerHistory,
                                                              std::string const&id) :
    InfoSpaceHandler(xdaqApp, id, updater),
    loggerHistory_(loggerHistory),
    id_(id)
{
    itemSetNameVec = {"itemset-log-header"};
    createString("header-LOGTimestamp", "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    createString("header-LOGType",      "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    createString("header-LOGmessage",   "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    for (size_t i=1; i<=loggerHistory_->maxSize(); ++i) {
        const string index = to_string(i);
        itemSetNameVec.push_back("itemset-log-msg"+index);
        createString("log-tmsg" +index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true); // Timestamp
        createString("log-tymsg"+index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true); // Message type
        createString("log-msg"  +index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true); // LOG message
    }
}

pixel::utils::LOGMsgHistoryHandler::~LOGMsgHistoryHandler() {}

void
pixel::utils::LOGMsgHistoryHandler::registerItemSetsWithMonitor(pixel::utils::Monitor& monitor) {
    monitor.newItemSet(itemSetNameVec.at(0));
    monitor.addItem(itemSetNameVec.at(0), "header-LOGTimestamp", "Timestamp",   this);
    monitor.addItem(itemSetNameVec.at(0), "header-LOGType",      "Type",        this);
    monitor.addItem(itemSetNameVec.at(0), "header-LOGmessage",   "LOG message", this);

    for (size_t i=1; i<=loggerHistory_->maxSize(); ++i) {
        const string index = to_string(i);
        monitor.newItemSet(itemSetNameVec.at(i));
        monitor.addItem(itemSetNameVec.at(i), "log-tmsg" +index, "", this);
        monitor.addItem(itemSetNameVec.at(i), "log-tymsg"+index, "", this);
        monitor.addItem(itemSetNameVec.at(i), "log-msg"  +index, "", this);
    }
}

void
pixel::utils::LOGMsgHistoryHandler::registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
                                                                 pixel::utils::Monitor& monitor,
                                                                 std::string const& forceTabName) {
    const string tabName = forceTabName.empty() ? "LOG message history" : forceTabName;

    webServer.registerTab(tabName, "", 1);
    webServer.registerMultiColTable("Message history", "", monitor, itemSetNameVec, tabName, id_);
}

void
pixel::utils::LOGMsgHistoryHandler::displayLOGMessage(pixel::utils::Monitor& monitor) {
    for (size_t i=loggerHistory_->size()-1; i>0; --i) {
        const string index     = to_string(i+1),
                     index_ant = to_string(i);
        setString("log-tmsg" +index, getString("log-tmsg" +index_ant));
        setString("log-tymsg"+index, getString("log-tymsg"+index_ant));
        setString("log-msg"  +index, getString("log-msg"  +index_ant));
    }

     // Add the new LOG message to the first row
    setString("log-tmsg1",  loggerHistory_->getTimestamp());
    setString("log-tymsg1", loggerHistory_->getType());
    setString("log-msg1",   loggerHistory_->getlog());
}
