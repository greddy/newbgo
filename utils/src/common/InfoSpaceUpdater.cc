#include "pixel/utils/InfoSpaceUpdater.h"

#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "pixel/exception/Exception.h"
#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceItem.h"

pixel::utils::InfoSpaceUpdater::InfoSpaceUpdater(xdaq::Application &xdaqApp)
    : XDAQObject(xdaqApp) {
}

pixel::utils::InfoSpaceUpdater::~InfoSpaceUpdater() {
}

void
pixel::utils::InfoSpaceUpdater::updateInfoSpace(InfoSpaceHandler *const infoSpaceHandler) {
    try {
        updateInfoSpaceImpl(infoSpaceHandler);
    }
    catch (...) {
        infoSpaceHandler->setInvalid();
        throw;
    }
}

void
pixel::utils::InfoSpaceUpdater::updateInfoSpaceImpl(InfoSpaceHandler *const infoSpaceHandler) {
    InfoSpaceHandler::ItemVec &items = infoSpaceHandler->getItems();
    InfoSpaceHandler::ItemVec::iterator iter;

    for (iter = items.begin(); iter != items.end(); ++iter) {
        try {
            updateInfoSpaceItem(*iter, infoSpaceHandler);
        }
        catch (...) {
            iter->setInvalid();
            throw;
        }
    }

    // Now sync everything from the cache to the InfoSpace itself.
    infoSpaceHandler->writeInfoSpace();
}

bool
pixel::utils::InfoSpaceUpdater::updateInfoSpaceItem(InfoSpaceItem &item,
                                                    InfoSpaceHandler *const infoSpaceHandler) {
    bool isUpToDate = false;

    // Nothing is implemented here. It's simply a catch-all which raises
    // an exception to show that something is missing.
    pixel::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
    if (updateType == pixel::utils::InfoSpaceItem::NOUPDATE) {
        isUpToDate = true;
    } else {
        std::string msg =
            toolbox::toString("Updating not implemented for item with name '%s'.",
                              item.name().c_str());
        item.setInvalid();
        XCEPT_RAISE(pixel::exception::SoftwareProblem, msg);
        isUpToDate = false;
    }

    return isUpToDate;
}
