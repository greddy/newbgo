#include "pixel/utils/Utils.h"

#include <algorithm>
#include <cassert>
#include <cctype>
#include <cerrno>
#include <climits>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <list>
#include <memory>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <zconf.h>
#include <zlib.h>

#include "toolbox/Runtime.h"
#include "toolbox/TimeVal.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdata/exception/Exception.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/TableIterator.h"

#include "pixel/exception/Exception.h"
#include "pixel/utils/ConfigurationInfoSpaceHandler.h"
#include "pixel/utils/CurlGetter.h"

long
pixel::utils::parseIntFromString(std::string const &numberAsString) {
    errno = 0;
    char *tmp = 0;
    char const *const rawStr = numberAsString.c_str();

    long const res = strtol(rawStr, &tmp, 0);

    if ((tmp == rawStr) ||
        (*tmp != '\0') ||
        ((errno == ERANGE) && ((res == LONG_MIN) || (res == LONG_MAX)))) {
        std::string const msg =
            "Failed to convert '" + numberAsString + "' to an integer";
        XCEPT_RAISE(pixel::exception::ValueError, msg);
    }

    return res;
}

std::string
pixel::utils::expandPathName(std::string const &pathNameIn) {
    std::vector<std::string> expandedPaths;
    try {
        expandedPaths = toolbox::getRuntime()->expandPathName(pathNameIn);
    }
    catch (toolbox::exception::Exception &err) {
        std::string const msgBase = "Cannot expand filename";
        XCEPT_RAISE(pixel::exception::ConfigurationProblem,
                    toolbox::toString("%s %s.", msgBase.c_str(), pathNameIn.c_str()));
    }

    if (expandedPaths.size() != 1) {
        XCEPT_RAISE(pixel::exception::ConfigurationProblem,
                    toolbox::toString("Expanding %s leads to ambiguities.",
                                      pathNameIn.c_str()));
    }

    return expandedPaths.at(0);
}

bool
pixel::utils::pathExists(std::string const &pathName) {
    struct stat sb;
    memset(&sb, 0, sizeof(sb));
    return (stat(pathName.c_str(), &sb) == 0);
}

bool
pixel::utils::dirExists(std::string const &dirName) {
    bool res = false;
    bool const exists = pathExists(dirName);
    if (exists) {
        res = pathIsDir(dirName);
    }
    return res;
}

bool
pixel::utils::pathIsDir(std::string const &pathName) {
    // NOTE: This implementation is not super complete, nor pretty.
    bool res = false;
    if (pathExists(pathName)) {
        struct stat sb;
        memset(&sb, 0, sizeof(sb));
        if (stat(pathName.c_str(), &sb) == 0) {
            res = S_ISDIR(sb.st_mode);
        }
    }
    return res;
}

void
pixel::utils::makeDir(std::string const &dirName) {
    std::list<std::string> const pieces =
        toolbox::parseTokenList(dirName, "/");

    std::string tmp = "";
    for (std::list<std::string>::const_iterator it = pieces.begin();
         it != pieces.end();
         ++it) {
        tmp += ("/" + *it);
        if (mkdir(tmp.c_str(), S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) != 0) {
            if (errno == EEXIST) {
                if (!pathIsDir(tmp)) {
                    // Path already exists but is not a directory.
                    std::string const msg =
                        toolbox::toString("Could not create directory '%s': path '%s' already exists but is not a directory.",
                                          dirName.c_str(),
                                          tmp.c_str());
                    XCEPT_RAISE(pixel::exception::RuntimeProblem, msg.c_str());
                }
            } else {
                // Something went wrong, but what?
                std::string const msg =
                    toolbox::toString("Could not create directory '%s': '%s'.",
                                      dirName.c_str(),
                                      std::strerror(errno));
                XCEPT_RAISE(pixel::exception::RuntimeProblem, msg.c_str());
            }
        }
    }
}
// BUG BUG BUG end

void
pixel::utils::rename(std::string const &from, std::string const &to) {
    int const res = std::rename(from.c_str(), to.c_str());
    if (res != 0) {
        // Something went wrong...
        std::string const msg =
            toolbox::toString("Could not create rename '%s' to '%s': '%s'.",
                              from.c_str(),
                              to.c_str(),
                              std::strerror(errno));
        XCEPT_RAISE(pixel::exception::RuntimeProblem, msg.c_str());
    }
}

/**
 * Escape a string such that it's safe to embed as string in JSON
 * format. Takes care of slashes, backslashes, double quotes, and
 * special characters like tabs and line feeds.
 */
std::string
pixel::utils::escapeAsJSONString(std::string const &stringIn) {
    return "\"" + toolbox::jsonquote(stringIn) + "\"";
}

std::string
pixel::utils::trimString(std::string const &stringToTrim,
                         std::string const &whitespace) {
    size_t const strBegin = stringToTrim.find_first_not_of(whitespace);
    if (strBegin == std::string::npos) {
        // No content at all in this string.
        return "";
    }

    size_t const strEnd = stringToTrim.find_last_not_of(whitespace);
    size_t const strRange = strEnd - strBegin + 1;

    return stringToTrim.substr(strBegin, strRange);
}

std::string
pixel::utils::capitalizeString(std::string const &stringIn) {
    std::string res = stringIn;
    if (!stringIn.empty()) {
        std::transform(res.begin(), res.end(), res.begin(), convertDown());
        res[0] = convertUp()(res[0]);
    }
    return res;
}

std::string
pixel::utils::chunkifyFEDVector(std::string const &fedVecIn) {
    // This thing can become annoyingly long. The idea here is to insert
    // some zero-width spaces in order to make it break into pieces when
    // formatted in the web interface.
    // NOTE: Not very efficient/pretty, but it works.

    std::string const zeroWidthSpace = "&#8203;";
    std::string const separator = "%";

    std::string fedVecOut = "";
    std::list<std::string> pieces = toolbox::parseTokenList(fedVecIn, separator);
    for (std::list<std::string>::const_iterator i = pieces.begin();
         i != pieces.end();
         ++i) {
        if (!fedVecOut.empty()) {
            fedVecOut += zeroWidthSpace;
        }
        fedVecOut += (*i + separator);
    }

    return fedVecOut;
}

std::string
pixel::utils::getTimestamp(bool const isoFormat,
                           bool const shortIsoFormat) {
    std::string res;
    toolbox::TimeVal const timeVal = toolbox::TimeVal::gettimeofday();
    if (isoFormat) {
        if (shortIsoFormat) {
            res = pixel::utils::formatTimestampISO(timeVal, true);
        } else {
            res = pixel::utils::formatTimestampISO(timeVal);
        }
    } else {
        res = pixel::utils::formatTimestamp(timeVal);
    }

    return res;
}

std::string
pixel::utils::formatTimestamp(toolbox::TimeVal const timestamp,
                              toolbox::TimeVal::TimeZone const tz) {
    std::string res = timestamp.toString("%Y-%m-%d %H:%M:%S", tz);
    if (tz == toolbox::TimeVal::gmt) {
        res += " UTC";
    }
    return res;
}

std::string
pixel::utils::formatTimestampISO(toolbox::TimeVal const timestamp,
                                 bool const shortIsoFormat) {
    return formatTimestampISO(timestamp, toolbox::TimeVal::gmt, shortIsoFormat);
}

std::string
pixel::utils::formatTimestampISO(toolbox::TimeVal const timestamp,
                                 toolbox::TimeVal::TimeZone const tz,
                                 bool const shortIsoFormat) {
    // NOTE: This code is not very elegant. But it does have the benefit
    // of sharing its core with the 'default' XDAQ TimeVal
    // implementation.
    std::string res = timestamp.toString("", tz);
    if (shortIsoFormat) {
        // Remove the decimal part.
        size_t const pos = res.find(".");
        res.erase(std::remove_if(res.begin() + pos, res.end(), &isdigit), res.end());
        // Remove the ':' and '.'.
        res.erase(std::remove_if(res.begin(), res.end(), &ispunct), res.end());
    }
    return res;
}

std::string
pixel::utils::formatTimestampDate(toolbox::TimeVal const timestamp,
                                  toolbox::TimeVal::TimeZone const tz) {
    std::string const res = timestamp.toString("%Y-%m-%d", tz);
    return res;
}

std::string
pixel::utils::formatTimestampTime(toolbox::TimeVal const timestamp,
                                  toolbox::TimeVal::TimeZone const tz) {
    std::string res = timestamp.toString("%H:%M:%S", tz);
    if (tz == toolbox::TimeVal::gmt) {
        res += " UTC";
    }
    return res;
}

std::string
pixel::utils::formatTimeRange(toolbox::TimeVal const timeBegin,
                              toolbox::TimeVal const timeEnd,
                              toolbox::TimeVal::TimeZone const tz) {
    std::stringstream result;

    time_t const secBegin = timeBegin.sec();
    struct tm tmpBegin;
    gmtime_r(&secBegin, &tmpBegin);
    time_t const secEnd = timeEnd.sec();
    struct tm tmpEnd;
    gmtime_r(&secEnd, &tmpEnd);

    // Case 1: begin and end times on the same day.
    if ((tmpBegin.tm_year == tmpEnd.tm_year) &&
        (tmpBegin.tm_mon == tmpEnd.tm_mon) &&
        (tmpBegin.tm_mday == tmpEnd.tm_mday)) {
        // Show the day once, and the times individually.
        result << pixel::utils::formatTimestampDate(timeBegin, tz)
               << " "
               << pixel::utils::formatTimestampTime(timeBegin, tz)
               << " - "
               << pixel::utils::formatTimestampTime(timeEnd, tz);
    }
    // Case 2: begin and end times on different days.
    else {
        // Show the full begin and end dates plus times individually.
        result << pixel::utils::formatTimestamp(timeBegin, tz)
               << " - "
               << pixel::utils::formatTimestamp(timeEnd, tz);
    }

    return result.str();
}

std::string
pixel::utils::formatDeltaTString(toolbox::TimeVal const timeBegin,
                                 toolbox::TimeVal const timeEnd,
                                 bool const verbose) {
    std::stringstream result;
    toolbox::TimeVal deltaT = timeEnd - timeBegin;
    if (deltaT.sec() != 0) {
        result << deltaT.sec() << " second";
        if (deltaT.sec() > 1) {
            result << "s";
        }
    }
    if (deltaT.millisec() != 0) {
        if (result.str().size() != 0) {
            result << " and ";
        }
        result << deltaT.millisec() << " millisecond";
        if (deltaT.millisec() > 1) {
            result << "s";
        }
    }
    if (result.str().size() == 0) {
        result << "negligible time";
    }

    // If requested: add the begin and end times explicitly.
    if (verbose) {
        result << " ("
               << "from: " << timeBegin.toString(toolbox::TimeVal::gmt)
               << ", "
               << "to: " << timeEnd.toString(toolbox::TimeVal::gmt)
               << ")";
    }

    return result.str();
}

std::string
pixel::utils::compressString(std::string const stringIn) {
    size_t srcSize = stringIn.size();
    Bytef const *src = reinterpret_cast<Bytef const *>(stringIn.c_str());
    // NOTE: The magic number below is a rounded-up result of reading
    // the zlib docs (for compress()).
    size_t dstSize = srcSize + (.1 * srcSize) + 16;
    std::vector<Bytef> dst(dstSize);

    z_stream zStream;
    // Not pretty, but necessary here.
    zStream.next_in = const_cast<Bytef *>(src);
    zStream.avail_in = srcSize;
    zStream.next_out = &dst[0];
    zStream.avail_out = dstSize;
    zStream.zalloc = Z_NULL;
    zStream.zfree = Z_NULL;
    zStream.opaque = Z_NULL;

    // NOTE: The '8' below is the value of DEF_MEM_LEVEL, the default
    // memory-usage level. This is defined in zutil.h, which is not
    // installed (for whatever reason) by the zlib-devel RPM.
    // NOTE: The '+ 16' changes the behaviour from zlib to gzip format.
    int res = deflateInit2(&zStream,
                           Z_DEFAULT_COMPRESSION,
                           Z_DEFLATED,
                           MAX_WBITS + 16,
                           8,
                           Z_DEFAULT_STRATEGY);

    if (res != Z_OK) {
        std::string const msg = "Failed to initialize zlib.";
        XCEPT_RAISE(pixel::exception::RuntimeProblem, msg.c_str());
    } else {
        res = deflate(&zStream, Z_FINISH);
        if ((res != Z_STREAM_END) && (res != Z_OK)) {
            deflateEnd(&zStream);
            std::string const msg = "Failed to compress string using zlib "
                                    "(looks like a buffer size problem).";
            XCEPT_RAISE(pixel::exception::RuntimeProblem, msg.c_str());
        }
        dstSize = zStream.total_out;
        res = deflateEnd(&zStream);
        if (res != Z_OK) {
            std::string const msg = "Failed to compress string using zlib.";
            XCEPT_RAISE(pixel::exception::RuntimeProblem, msg.c_str());
        }
    }

    // Turn the result back into an std::string.
    std::string const stringOut(dst.begin(), dst.begin() + dstSize);
    return stringOut;
}

unsigned int pixel::utils::countTrailingZeros(uint32_t const val) {
    unsigned int numZeros = 0;
    uint32_t tmp = val;
    while ((tmp & 0x1) == 0) {
        numZeros += 1;
        tmp >>= 1;
    }
    return numZeros;
}

std::string pixel::utils::formatLabel(std::string const &type,
                                      unsigned int const lpmNumber,
                                      unsigned int const number,
                                      pixel::utils::ConfigurationInfoSpaceHandler const &cfgInfoSpace,
                                      bool const labelForExternal) {
    std::string const typeUp = toolbox::toupper(type);
    std::string res = "";
    std::string parName;

    if (lpmNumber == 0) {
        // LPMController etc.
        parName = toolbox::toString("partitionLabel%s%d", typeUp.c_str(), number);
    } else {
        // CPMController.
        parName = toolbox::toString("partitionLabelLPM%d%s%d",
                                    lpmNumber, typeUp.c_str(), number);
    }

    std::string const label = cfgInfoSpace.getString(parName);
    if (label.empty()) {
        if (!labelForExternal) {
            res = toolbox::toString("%s%d", typeUp.c_str(), number);
        } else {
            if (lpmNumber == 0) {
                res = toolbox::toString("%s%d", typeUp.c_str(), number);
            } else {
                res = toolbox::toString("LPM%d:%s%d", lpmNumber, typeUp.c_str(), number);
            }
        }
    } else {
        if (!labelForExternal) {
            res = toolbox::toString("%s%d (%s)", typeUp.c_str(), number, label.c_str());
        } else {
            res = toolbox::toString("%s", label.c_str());
        }
    }

    return res;
}

xdata::Table
pixel::utils::getFlashList(std::string const &lasURL,
                           std::string const &flashlistName) {
    std::string const lasPath = "retrieveCollection";
    // The data we get will have (E)XDR format
    std::string lasQuery =
        "fmt=exdr&flash=" + flashlistName;

    pixel::utils::CurlGetter getter;
    std::string rawTableData =
        getter.get(lasURL + "/" + lasPath, "", lasQuery, "");

    // // NOTE: This is a bit of a short-cut. One should retrieve and check
    // // the catalog, really. On the other hand, this reduces the amount
    // // of required network traffic.
    // std::string const tmp = toolbox::tolower(rawTableData);
    // std::string const failMsg = "failed to find flashlist";
    // if (tmp.find(failMsg) != std::string::npos)
    //   {
    //     std::string const fullURL =
    //       getter.buildFullURL(lasURL + "/" + lasPath, "", lasQuery, "");
    //     std::string const msg =
    //       toolbox::toString("Failed to find flashlist '%s' on LAS '%s' (full query URL: '%s').",
    //                         flashlistName.c_str(),
    //                         lasURL.c_str(),
    //                         fullURL.c_str());
    //     XCEPT_RAISE(pixel::exception::HTTPProblem, msg);
    //   }

    xdata::exdr::FixedSizeInputStreamBuffer
    inBuffer(static_cast<char *>(&rawTableData[0]), rawTableData.size());

    xdata::Table table;
    xdata::exdr::Serializer serializer;
    try {
        serializer.import(&table, &inBuffer);
    }
    catch (xdata::exception::Exception &err) {
        std::string const fullURL =
            getter.buildFullURL(lasURL + "/" + lasPath, "", lasQuery, "");
        std::string const msgBase =
            toolbox::toString("Failed to deserialize incoming flashlist table "
                              "for flashlist '%s' on LAS '%s' (full query URL: '%s') ",
                              flashlistName.c_str(),
                              lasURL.c_str(),
                              fullURL.c_str());
        std::string const msg =
            toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        XCEPT_RETHROW(pixel::exception::XDataProblem, msg, err);
    }

    return table;
}

std::string pixel::utils::to_string(int value) {
    char buffer[50];
    sprintf(buffer, "%d", value);
    std::string str(buffer);
    return str;
}

std::string pixel::utils::to_string(unsigned int value) {
    char buffer[50];
    sprintf(buffer, "%d", value);
    std::string str(buffer);
    return str;
}

std::string pixel::utils::to_string(long unsigned int value) {
    char buffer[50];
    sprintf(buffer, "%lu", value);
    std::string str(buffer);
    return str;
}

std::string pixel::utils::to_string(long int value) {
    char buffer[50];
    sprintf(buffer, "%lu", value);
    std::string str(buffer);
    return str;
}

std::string pixel::utils::to_string(float value) {
    char buffer[50];
    sprintf(buffer, "%f", value);
    std::string str(buffer);
    return str;
}

std::string pixel::utils::to_string(double value) {
    char buffer[50];
    sprintf(buffer, "%f", value);
    std::string str(buffer);
    return str;
}

std::string pixel::utils::to_string(const char* value) {
    std::stringstream ss;
    ss << value;
    return ss.str();
}

std::string pixel::utils::hex_to_string(int value) {
    char buffer[50];
    sprintf(buffer, "%x", value);
    std::string str(buffer);
    return str;
}
