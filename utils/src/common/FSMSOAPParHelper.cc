#include "pixel/utils/FSMSOAPParHelper.h"

pixel::utils::FSMSOAPParHelper::FSMSOAPParHelper(std::string const &commandName,
                                                 std::string const &parameterName,
                                                 bool const isRequired // ,
                                                 // std::string const& defaultValue
                                                 )
    : commandName_(commandName),
      parameterName_(parameterName),
      isRequired_(isRequired) // ,
                              // defaultValue_(defaultValue)
{
}

pixel::utils::FSMSOAPParHelper::~FSMSOAPParHelper() {
}

std::string
pixel::utils::FSMSOAPParHelper::commandName() const {
    return commandName_;
}

std::string
pixel::utils::FSMSOAPParHelper::parameterName() const {
    return parameterName_;
}

bool
pixel::utils::FSMSOAPParHelper::isRequired() const {
    return isRequired_;
}

// std::string
// pixel::utils::FSMSOAPParHelper::defaultValue() const
// {
//   return defaultValue_;
// }
