#include "pixel/utils/XDAQAppWithFSMPixelBase.h"

#include <algorithm>
#include <fstream>
#include <map>
#include <set>
#include <utility>
#include <vector>

#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "toolbox/fsm/FailedEvent.h"
#include "toolbox/exception/Handler.h"
#include "xcept/Exception.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"
#include "xoap/Method.h"

#include "pixel/hwlayer/ConfigurationProcessor.h"
#include "pixel/hwlayer/RegDumpConfigurationProcessor.h"
#include "pixel/hwlayer/RegisterInfo.h"
#include "pixel/exception/Exception.h"
#include "pixel/utils/ConfigurationInfoSpaceHandler.h"
#include "pixel/utils/Lock.h"
#include "pixel/utils/LockGuard.h"
#include "pixel/utils/LogMacros.h"
#include "pixel/utils/Utils.h"

pixel::utils::XDAQAppWithFSMPixelBase::XDAQAppWithFSMPixelBase(xdaq::ApplicationStub *const stub,
                                                               std::unique_ptr<pixel::hwlayer::DeviceBase> hw)
    : XDAQAppBase(stub, std::move(hw)),
      fsm_(this) {

    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "ColdReset", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "Initialize", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "Configure", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "Enable", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "Start", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "Halt", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "Pause", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "Reconfigure", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "Resume", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "Stop", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "TTCResync", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "TTCHardReset", XDAQ_NS_URI);

    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "FixSoftError", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "FixingSoftErrorDone", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::changeState, "ResumeFromSoftError", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::fsmStateRequest, "FSMStateRequest", XDAQ_NS_URI);

    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::beginCalibration, "BeginCalibration", XDAQ_NS_URI);
    xoap::bind<XDAQAppWithFSMPixelBase>(this, &XDAQAppWithFSMPixelBase::endCalibration, "EndCalibration", XDAQ_NS_URI);
}

pixel::utils::XDAQAppWithFSMPixelBase::~XDAQAppWithFSMPixelBase() {
}

void
pixel::utils::XDAQAppWithFSMPixelBase::coldResetAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::coldResetAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        coldResetActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'ColdReset' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::coldResetAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::coldReset() {
    fsm_.coldReset();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::configure() {
    fsm_.configure();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::enable() {
    fsm_.enable();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::start() {
    fsm_.start();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::fail() {
    fsm_.fail();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::halt() {
    fsm_.halt();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::pause() {
    fsm_.pause();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::reconfigure() {
    fsm_.reconfigure();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::resume() {
    fsm_.resume();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::stop() {
    fsm_.stop();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::fixSoftError() {
    fsm_.fixSoftError();
}

xoap::MessageReference
#ifndef XDAQ_TARGET_XDAQ15
pixel::utils::XDAQAppWithFSMPixelBase::fsmStateRequest(xoap::MessageReference msg) 
#else
pixel::utils::XDAQAppWithFSMPixelBase::fsmStateRequest(xoap::MessageReference msg)
#endif
{
    return makeCommandSOAPReply(msg,getCurrentStateName());
}

std::string
pixel::utils::XDAQAppWithFSMPixelBase::getCurrentStateName() const {
    return fsm_.getCurrentStateName();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::initializeAction(toolbox::Event::Reference event){
    INFO("XDAQAppWithFSMPixelBase::initializeAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try{
        initializeActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "initialize failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::initializeAction() took "
        << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
        << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::transitionHaltedToConfiguringAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::transitionHaltedToConfiguringAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try{
        transitionHaltedToConfiguringActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "transitionHaltedToConfiguring failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::transitionHaltedToConfiguringAction() took "
        << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
        << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::configureAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::configureAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        configureActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'Configure' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    // Make sure the configuration InfoSpace stays in-sync.
    cfgInfoSpaceP_->writeInfoSpace();

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::configureAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::startAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::startAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        startActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'Start' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::startAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::failAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::failAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        failActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'Fail' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        // NOTE: The failAction() method is called upon arriving in the
        // 'Failed' state, so if anything goes wrong, we should _not_
        // retrigger the same procedure by triggering another transition
        // into the 'Failed' state. So no gotoFailed() here.
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::failAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::transitionConfiguredToHaltingAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::transitionConfiguredToHaltingAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try{
        transitionConfiguredToHaltingActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msg = toolbox::toString("transitionConfiguredToHalting failed: '%s'.", err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::transitionConfiguredToHaltingAction() took "
        << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
        << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::transitionRunningOrPausedToHaltingAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::transitionRunningOrPausedToHaltingAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    try{
        transitionRunningOrPausedToHaltingActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msg = toolbox::toString("transitionRunningOrPausedToHalting failed: '%s'.", err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::transitionRunningOrPausedToHaltingAction() took "
        << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
        << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::haltAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::haltAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        haltActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'Halt' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    // If we got here, all is well. Hardware has been released. Revoke
    // the hardware lease as well now.
    revokeHwLease();

    // Reset the run number as well.
    cfgInfoSpaceP_->setUInt32("runNumber", 0);
    cfgInfoSpaceP_->writeInfoSpace();

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::haltAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::pauseAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::pauseAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        pauseActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'Pause' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::pauseAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::resumeAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::resumeAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        resumeActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'Resume' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::resumeAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::stopAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::stopAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        stopActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'Stop' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::stopAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::reconfigureAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::reconfigureAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();

    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        reconfigureActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'Reconfigure' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    // Make sure the configuration InfoSpace stays in-sync.
    cfgInfoSpaceP_->writeInfoSpace();

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::reconfigureAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::fixSoftErrorAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::fixSoftErrorAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        fixSoftErrorActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'FixSoftError' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::fixSoftErrorAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::resumeFromSoftErrorAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::resumeFromSoftErrorAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        resumeFromSoftErrorActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'ResumeFromSoftError' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::resumeFromSoftErrorAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::ttcHardResetAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::ttcHardResetAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        ttcHardResetActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'TTCHardReset' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::ttcHardResetAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

void
pixel::utils::XDAQAppWithFSMPixelBase::ttcResyncAction(toolbox::Event::Reference event) {
    INFO("XDAQAppWithFSMPixelBase::ttcResyncAction()");
    toolbox::TimeVal timeBegin = toolbox::TimeVal::gettimeofday();
    LockGuard<Lock> guardedLock(monitoringLock_);

    try {
        ttcResyncActionImpl(event);
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "State transition 'TTCResync' failed";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::RuntimeProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }

    toolbox::TimeVal timeEnd = toolbox::TimeVal::gettimeofday();
    INFO("XDAQAppWithFSMPixelBase::ttcResyncAction() took "
         << pixel::utils::formatDeltaTString(timeBegin, timeEnd, true)
         << ".");
}

xoap::MessageReference
#ifndef XDAQ_TARGET_XDAQ15
pixel::utils::XDAQAppWithFSMPixelBase::changeState(xoap::MessageReference msg) 
#else
pixel::utils::XDAQAppWithFSMPixelBase::changeState(xoap::MessageReference msg)
#endif
{
    return changeStateImpl(msg);
}

void
pixel::utils::XDAQAppWithFSMPixelBase::coldResetActionImpl(toolbox::Event::Reference event) {
    try {
        hwColdReset();
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "Could not ColdReset the hardware";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::HardwareProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }
}

void
pixel::utils::XDAQAppWithFSMPixelBase::initializeActionImpl(toolbox::Event::Reference event) {

}

void
pixel::utils::XDAQAppWithFSMPixelBase::transitionHaltedToConfiguringActionImpl(toolbox::Event::Reference event) {

}

void
pixel::utils::XDAQAppWithFSMPixelBase::configureActionImpl(toolbox::Event::Reference event) {
    try {
        hwConnect();
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "Could not connect to the hardware";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::HardwareProblem, top, msg, err);
        notifyQualified("fatal", top);
        fsm_.gotoFailed(top);
    }
    /*
    try {
        hwConfigure();
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "Could not configure the hardware";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::HardwareProblem, top, msg, err);
        notifyQualified("error", top);
        fsm_.gotoFailed(top);
    }
    */
}

void
pixel::utils::XDAQAppWithFSMPixelBase::startActionImpl(toolbox::Event::Reference event) {
    // try
    //   {
    //     hwP_->hwStart();
    //   }
    // catch (xcept::Exception& err)
    //   {
    //     std::string msgBase = "Could not start the hardware";
    //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
    //     ERROR(msg);
    //     XCEPT_DECLARE_NESTED(pixel::exception::HardwareProblem, top, msg, err);
    //     notifyQualified("fatal", top);
    //     fsm_.gotoFailed(top);
    //   }
}

void
pixel::utils::XDAQAppWithFSMPixelBase::failActionImpl(toolbox::Event::Reference event) {
    // BUG BUG BUG
    // Figure out if we really want to do this.
    // if (hwP_ != 0)
    //   {
    //     // BUG BUG BUG
    //     // This is a mess (and may need a lock, actually).
    //     hwP_->hwRelease();
    //     // BUG BUG BUG end
    //   }
    // BUG BUG BUG end
}


void
pixel::utils::XDAQAppWithFSMPixelBase::transitionConfiguredToHaltingActionImpl(toolbox::Event::Reference event) {

}

void
pixel::utils::XDAQAppWithFSMPixelBase::transitionRunningOrPausedToHaltingActionImpl(toolbox::Event::Reference event) {

}

void
pixel::utils::XDAQAppWithFSMPixelBase::haltActionImpl(toolbox::Event::Reference event) {
    // try
    //   {
    //     hwP_->hwHalt();
    //   }
    // catch (xcept::Exception& err)
    //   {
    //     std::string msgBase = "Could not halt the hardware";
    //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
    //     ERROR(msg);
    //     XCEPT_DECLARE_NESTED(pixel::exception::HardwareProblem, top, msg, err);
    //     notifyQualified("fatal", top);
    //     fsm_.gotoFailed(top);
    //   }

    try {
        hwRelease();
    }
    catch (xcept::Exception &err) {
        std::string msgBase = "Could not release the hardware";
        std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
        ERROR(msg);
        XCEPT_DECLARE_NESTED(pixel::exception::HardwareProblem, top, msg, err);
        notifyQualified("fatal", top);
        fsm_.gotoFailed(top);
    }
}

void
pixel::utils::XDAQAppWithFSMPixelBase::pauseActionImpl(toolbox::Event::Reference event) {
    // try
    //   {
    //     hwP_->hwPause();
    //   }
    // catch (xcept::Exception& err)
    //   {
    //     std::string msgBase = "Could not pause the hardware";
    //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
    //     ERROR(msg);
    //     XCEPT_DECLARE_NESTED(pixel::exception::HardwareProblem, top, msg, err);
    //     notifyQualified("fatal", top);
    //     fsm_.gotoFailed(top);
    //   }
}

void
pixel::utils::XDAQAppWithFSMPixelBase::resumeActionImpl(toolbox::Event::Reference event) {
    // try
    //   {
    //     hwP_->hwResume();
    //   }
    // catch (xcept::Exception& err)
    //   {
    //     std::string msgBase = "Could not resume the hardware";
    //     std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
    //     ERROR(msg);
    //     XCEPT_DECLARE_NESTED(pixel::exception::HardwareProblem, top, msg, err);
    //     notifyQualified("fatal", top);
    //     fsm_.gotoFailed(top);
    //   }
}

void
pixel::utils::XDAQAppWithFSMPixelBase::stopActionImpl(toolbox::Event::Reference event) {
}

void
pixel::utils::XDAQAppWithFSMPixelBase::reconfigureActionImpl(toolbox::Event::Reference event) {
}

void
pixel::utils::XDAQAppWithFSMPixelBase::fixSoftErrorActionImpl(toolbox::Event::Reference event) {
}

void
pixel::utils::XDAQAppWithFSMPixelBase::resumeFromSoftErrorActionImpl(toolbox::Event::Reference event) {
}

void
pixel::utils::XDAQAppWithFSMPixelBase::ttcHardResetActionImpl(toolbox::Event::Reference event) {
}

void
pixel::utils::XDAQAppWithFSMPixelBase::ttcResyncActionImpl(toolbox::Event::Reference event) {
}

void
pixel::utils::XDAQAppWithFSMPixelBase::hwColdReset() {
    hwColdResetImpl();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::hwColdResetImpl() {
    // The default does nothing.
}

void
pixel::utils::XDAQAppWithFSMPixelBase::hwConfigure() {
    hwConfigureImpl();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::hwConfigureImpl() {
    // Extract the hardware configuration string and parse it into a
    // register-value list.
    std::string const configurationString =
        cfgInfoSpaceP_->getString("hardwareConfigurationStringReceived");
    pixel::hwlayer::RegDumpConfigurationProcessor cfgProcessor;
    pixel::hwlayer::ConfigurationProcessor::RegValVec const cfgInfoUser =
        cfgProcessor.parse(configurationString);

    // Load and parse the default configuration file.
    std::string const tmp = cfgInfoSpaceP_->getString("defaultHwConfigurationFilePath");
    std::string const defaultConfigurationFileName = pixel::utils::expandPathName(tmp);
    std::ifstream inputFile(defaultConfigurationFileName.c_str());
    std::string defaultConfigurationString = "";
    if (!inputFile.is_open() || !inputFile.good()) {
        std::string const msg =
            toolbox::toString("Failed to read default hardware configuration from file '%s'.",
                              defaultConfigurationFileName.c_str());
        XCEPT_RAISE(pixel::exception::ConfigurationProblem, msg);
    } else {
        inputFile.seekg(0, std::ios::end);
        defaultConfigurationString.resize(inputFile.tellg());
        inputFile.seekg(0, std::ios::beg);
        inputFile.read(&defaultConfigurationString[0], defaultConfigurationString.size());
    }
    inputFile.close();
    cfgInfoSpaceP_->setString("hardwareConfigurationStringDefault", defaultConfigurationString);
    pixel::hwlayer::ConfigurationProcessor::RegValVec const cfgInfoDefault =
        cfgProcessor.parse(defaultConfigurationString);

    // Merge the default and the user configurations.
    pixel::hwlayer::ConfigurationProcessor::RegValVec cfgInfo = cfgInfoUser;
    std::vector<std::string> regNamesInUserConfig;
    for (pixel::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = cfgInfoUser.begin();
         i != cfgInfoUser.end();
         ++i) {
        regNamesInUserConfig.push_back(i->first);
    }
    for (pixel::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = cfgInfoDefault.begin();
         i != cfgInfoDefault.end();
         ++i) {
        if (std::find(regNamesInUserConfig.begin(),
                      regNamesInUserConfig.end(),
                      i->first) == regNamesInUserConfig.end()) {
            cfgInfo.push_back(*i);
        }
    }

    // Be explicit about empty 'applied' hardware configuration strings.
    std::string appliedCfgStr = cfgProcessor.compose(cfgInfo);
    if (appliedCfgStr.empty()) {
        appliedCfgStr = "# Empty.";
    }
    cfgInfoSpaceP_->setString("hardwareConfigurationStringApplied", appliedCfgStr);

    //----------

    // Make sure that we're not accessing any registers that we are not
    // allowed to access.

    // Start by building a list of registers that exist in the hardware
    // (or at least in the address table).
    std::vector<std::string> regNamesVec = hwP_->getRegisterNames();
    std::set<std::string> regNamesSet(regNamesVec.begin(), regNamesVec.end());

    // Build a list of registers mentioned in the configuration.
    std::vector<std::string> cfgRegNamesVec;
    cfgRegNamesVec.reserve(cfgInfo.size());
    for (pixel::hwlayer::ConfigurationProcessor::RegValVec::const_iterator i = cfgInfo.begin();
         i != cfgInfo.end();
         ++i) {
        cfgRegNamesVec.push_back(i->first);
    }
    std::set<std::string> const cfgRegNamesSet(cfgRegNamesVec.begin(), cfgRegNamesVec.end());
    pixel::hwlayer::RegisterInfo::RegInfoVec regInfos = hwP_->getRegisterInfos();

    for (std::set<std::string>::const_iterator i = cfgRegNamesSet.begin();
         i != cfgRegNamesSet.end();
         ++i) {
        // Check if the register that is specified in the configuration
        // actually exists.
        if ((regNamesSet.erase(*i)) < 1) {
            // Find out if this register came from the user-config or
            // from the default config.
            bool const isRegFromUserConfig = (std::find(regNamesInUserConfig.begin(),
                                                        regNamesInUserConfig.end(),
                                                        *i) != regNamesInUserConfig.end());
            std::string tmp = "which came from the default configuration";
            if (isRegFromUserConfig) {
                tmp = "which came from the user-specified configuration";
            }
            std::string const msg =
                toolbox::toString("Register '%s' (%s) does not exist.",
                                  i->c_str(),
                                  tmp.c_str());
            XCEPT_RAISE(pixel::exception::ValueError, msg);
        }

        // Check if the register that is specified in the configuration
        // is allowed to be configured.
        pixel::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo;
        regInfo = std::find_if(regInfos.begin(),
                               regInfos.end(),
                               pixel::hwlayer::RegisterInfo::RegInfoNameMatches(*i));
        // This should never happen, really.
        if (regInfo == regInfos.end()) {
            std::string const msg =
                toolbox::toString("Register '%s' (%s) does not exist "
                                  "(but somehow passed the check on register existence?).",
                                  i->c_str(),
                                  tmp.c_str());
            XCEPT_RAISE(pixel::exception::SoftwareProblem, msg);
        }
        if (!isRegisterAllowed(*regInfo)) {
            // Find out if this register came from the user-config or
            // from the default config.
            bool const isRegFromUserConfig = (std::find(regNamesInUserConfig.begin(),
                                                        regNamesInUserConfig.end(),
                                                        *i) != regNamesInUserConfig.end());
            std::string tmp = "which came from the default configuration";
            if (isRegFromUserConfig) {
                tmp = "which came from the user-specified configuration";
            }
            std::string const msg =
                toolbox::toString("Register '%s' (%s) is not allowed to be configured.",
                                  i->c_str(),
                                  tmp.c_str());
            XCEPT_RAISE(pixel::exception::ValueError, msg);
        }
    }

    //----------

    // Initialize (or reset) the hardware (or at least the functional
    // firmware block) we're connected to.
    hwCfgInitialize();

    // Do the actual configuration.
    hwP_->writeHardwareConfiguration(cfgInfo);

    // Finalize the configuration.
    hwCfgFinalize();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::hwCfgInitialize() {
    hwCfgInitializeImpl();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::hwCfgInitializeImpl() {
}

void
pixel::utils::XDAQAppWithFSMPixelBase::hwCfgFinalize() {
    hwCfgFinalizeImpl();
}

void
pixel::utils::XDAQAppWithFSMPixelBase::hwCfgFinalizeImpl() {
}

// This simply forwards the message to the FSM object, since it is
// technically not possible to bind directly to anything but an
// xdaq::Application.
//////////// OVERLOAD
xoap::MessageReference
pixel::utils::XDAQAppWithFSMPixelBase::changeStateImpl(xoap::MessageReference msg) {
    return fsm_.changeState(msg);
}


std::set<std::string>
pixel::utils::XDAQAppWithFSMPixelBase::listOfAllowedStates() {
    return fsm_.listOfAllowedStates();
}

std::set<std::string>
pixel::utils::XDAQAppWithFSMPixelBase::listOfAllStates() {
    return fsm_.listOfAllStates();
}

xoap::MessageReference
pixel::utils::XDAQAppWithFSMPixelBase::beginCalibration(xoap::MessageReference msg) {
    return makeSOAPMessageReference("pixel::utils::XDAQAppWithFSMPixelBase::beginCalibration Default");
}
xoap::MessageReference
pixel::utils::XDAQAppWithFSMPixelBase::endCalibration(xoap::MessageReference msg){
    return makeSOAPMessageReference("pixel::utils::XDAQAppWithFSMPixelBase::endCalibration Default");

}
