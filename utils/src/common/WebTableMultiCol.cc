#include "pixel/utils/WebTableMultiCol.h"

#include <sstream>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>
#include <iterator>


#include "toolbox/string.h"

#include "pixel/exception/Exception.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"

pixel::utils::WebTableMultiCol::WebTableMultiCol(std::string const &name,
                                                             std::string const &description,
                                                             Monitor const &monitor,
                                                             std::vector<std::string> const &itemSetNameVec,
                                                             std::string const &tabName,
                                                             std::string const &id)
    : WebObject(name, description, monitor, itemSetNameVec, tabName, 1),
    id_(id) {}

std::string
pixel::utils::WebTableMultiCol::getHTMLString() const {
    std::stringstream res;

    res << "<div class=\"pixel-item-table-wrapper\">"
        << "\n";

    res << "<p class=\"pixel-item-table-title\">"
        << getName()
        << "</p>";
    res << "\n";

    std::string const desc = getDescription();
    if (!desc.empty()) {
        res << "<p class=\"pixel-item-table-description\">"
            << desc
            << "</p>";
        res << "\n";
    }

    res << "<table class=\"pixel-item-table xdaq-table pixel-table-compact\" ";
    if (!id_.empty())
        res << "id=\"" <<  id_ << "\"";
    res << " cellspacing=\"0\">"
        << "\n";

    //----------------------------------------------------------------------------
    res << getHTMLthead();
    res << getHTMLtbody();
    //----------------------------------------------------------------------------

    res << "</table>";
    res << "\n";

    res << "</div>";
    res << "\n";

    return res.str();
}

std::string
pixel::utils::WebTableMultiCol::getHTMLthead() const {
    std::stringstream res;
    res << "<thead>\n";
    res << "  <tr>\n";

    Monitor::StringPairVector firstItem = monitor_.getItemSetDocStrings(itemSetNameVec_[0]);

    for (Monitor::StringPairVector::const_iterator  fiIter = firstItem.begin(); fiIter != firstItem.end(); ++fiIter) {
        res << "    <th";
        if (!fiIter->second.empty())
            res << " class=\"xdaq-tooltip\"";

        res << "><div> <p style=\"text-align:center;\">" <<  fiIter->first << "</p> </div>";

        if (!fiIter->second.empty())
            res << "<span class=\"xdaq-tooltip-msg\">" << fiIter->second << "</span>";

        res << "</th>\n";
    }

    res << "  </tr>\n";
    res << "</thead>\n";

    return res.str();
}

std::string
pixel::utils::WebTableMultiCol::getHTMLtbody() const {
    std::stringstream res;
    res << "<tbody>\n";

    for (std::vector<std::string>::const_iterator isNameiter = itemSetNameVec_.begin()+1; isNameiter != itemSetNameVec_.end(); ++isNameiter) {
        Monitor::StringTupleVector items = monitor_.getNameFormattedItemSet(*isNameiter);
        res << "  <tr>\n";

        for (Monitor::StringTupleVector::const_iterator iter = items.begin(); iter != items.end(); ++iter) {
            std::string const value = toolbox::unquote(std::get<2>(*iter));
            ////std::string const tmp = "[\"" + toolbox::jsonquote(itemSetName_) + "\"][\"" + toolbox::jsonquote(iter->first) + "\"]";
            res // << "    <td class=\"xdaq-tooltip\">"
                << "<span class=\"xdaq-tooltip-msg\">" << std::get<1>(*iter) << "</span>"
                //////<< "<script type=\"text/x-dot-template\">"
                //////<< "{{=it" << tmp << "}}"
                //////<< "</script>"
                << "<td class=\"pixel-item-value\" id=\""+ std::get<0>(*iter) +"\"> " << value
                << "<div class=\"target\"></div>"
                << "</td>\n";
        }
        res << "  </tr>\n";
    }

    res << "</tbody>\n";
    return res.str();
}
