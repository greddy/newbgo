#include "pixel/utils/FSMStateSummaryHandler.h"


#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/InfoSpaceUpdater.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/WebServer.h"

#include "toolbox/string.h"



pixel::utils::FSMStateSummaryHandler::FSMStateSummaryHandler(xdaq::Application& xdaqApp, pixel::utils::InfoSpaceUpdater* updater) :
pixel::utils::InfoSpaceHandler(xdaqApp, "pixel-fsm-summary", updater) //This will probably need to be changed
{
  std::vector<std::string> placeHolderStates; placeHolderStates.push_back("N/A");
  createString("fsmState", "PlaceholderState", "", pixel::utils::InfoSpaceItem::PROCESS, false);
  createStringVec("allowedStates", placeHolderStates, "", pixel::utils::InfoSpaceItem::PROCESS, true);
  createStringVec("allStates", placeHolderStates, "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
}

pixel::utils::FSMStateSummaryHandler::~FSMStateSummaryHandler() {
}

void pixel::utils::FSMStateSummaryHandler::registerItemSetsWithMonitor(pixel::utils::Monitor& monitor) {

  // Hardware status.
  std::string itemSetName = "itemset-fsm-summary";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName, "fsmState", "Current FSM state", this);

  monitor.newItemSet("allStatesItemset");
  monitor.addItem("allStatesItemset", "allStates", "allStates", this);

  monitor.newItemSet("allowedStatesItemset");
  monitor.addItem("allowedStatesItemset", "allowedStates", "allowedStates", this);
}

void pixel::utils::FSMStateSummaryHandler::registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
  pixel::utils::Monitor& monitor,
  std::string const& forceTabName)
  {
    std::string const tabName = forceTabName.empty() ? "FSM State" : forceTabName;

    webServer.registerTab(tabName, "", 1);
    webServer.registerTable("", "", monitor, "itemset-fsm-summary", tabName);
    webServer.registerTable("", "", monitor, "allowedStatesItemset", tabName);
    webServer.registerTable("", "", monitor, "allStatesItemset", tabName);
  }

  std::string pixel::utils::FSMStateSummaryHandler::formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const {
    std::string res = pixel::utils::escapeAsJSONString(pixel::utils::InfoSpaceHandler::kInvalidItemString);
    if (item->isValid()) {
      std::string name = item->name();

      res = InfoSpaceHandler::formatItem(item);
    }
    return res;
  }
