#include "pixel/utils/SOAPMsgHistoryHandler.h"

#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/WebServer.h"

#include "toolbox/string.h"

using namespace std;


pixel::utils::SOAPMsgHistoryHandler::SOAPMsgHistoryHandler(xdaq::Application& xdaqApp,
                                                              pixel::utils::InfoSpaceUpdater* updater,
                                                              pixel::utils::SOAPER* soaper,
                                                              std::string const&id) :
    InfoSpaceHandler(xdaqApp, id, updater),
    soaper_(soaper),
    id_(id)
{
    itemSetNameVec = {"itemset-soap-header"};
    createString("header-SOAPtimestamp", "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    createString("header-SOAPreceived", "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    createString("header-SOAPsent", "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    createString("header-SOAPmessage", "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true);
    for (size_t i=1; i<=soaper_->maxSize(); ++i) {
        const string index = to_string(i);
        itemSetNameVec.push_back("itemset-soap-msg"+index);
        createString("soap-tmsg"+index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true); // Timestamp
        createString("soap-rmsg"+index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true); // Received
        createString("soap-smsg"+index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true); // Sent
        createString( "soap-msg"+index, "", "", pixel::utils::InfoSpaceItem::NOUPDATE, true); // SOAP message (body)
    }
}

pixel::utils::SOAPMsgHistoryHandler::~SOAPMsgHistoryHandler() {}

void
pixel::utils::SOAPMsgHistoryHandler::registerItemSetsWithMonitor(pixel::utils::Monitor& monitor) {
    monitor.newItemSet(itemSetNameVec.at(0));
    monitor.addItem(itemSetNameVec.at(0), "header-SOAPtimestamp",   "Timestamp",           this);
    monitor.addItem(itemSetNameVec.at(0), "header-SOAPreceived",    "Received",            this);
    monitor.addItem(itemSetNameVec.at(0), "header-SOAPsent",        "Sent",                this);
    monitor.addItem(itemSetNameVec.at(0), "header-SOAPmessage", "SOAP message (body)", this);

    for (size_t i=1; i<=soaper_->maxSize(); ++i) {
        const string index = to_string(i);
        monitor.newItemSet(itemSetNameVec.at(i));
        monitor.addItem(itemSetNameVec.at(i), "soap-tmsg"+index, "", this);
        monitor.addItem(itemSetNameVec.at(i), "soap-rmsg"+index, "", this);
        monitor.addItem(itemSetNameVec.at(i), "soap-smsg"+index, "", this);
        monitor.addItem(itemSetNameVec.at(i),  "soap-msg"+index, "", this);
    }
}

void
pixel::utils::SOAPMsgHistoryHandler::registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
                                                                 pixel::utils::Monitor& monitor,
                                                                 std::string const& forceTabName) {
    const string tabName = forceTabName.empty() ? "SOAP message history" : forceTabName;

    webServer.registerTab(tabName, "", 1);
    webServer.registerMultiColTable("Message history", "", monitor, itemSetNameVec, tabName, id_);
}

void
pixel::utils::SOAPMsgHistoryHandler::displaySOAPMessage(pixel::utils::Monitor& monitor) {
    for (size_t i=soaper_->size()-1; i>0; --i) {
        const string index     = to_string(i+1),
                     index_ant = to_string(i);
        setString("soap-tmsg"+index,getString("soap-tmsg"+index_ant));
        setString("soap-rmsg"+index,getString("soap-rmsg"+index_ant));
        setString("soap-smsg"+index,getString("soap-smsg"+index_ant));
        setString( "soap-msg"+index,getString( "soap-msg"+index_ant));
    }

     // Add the new SOAP message to the first row
    setString("soap-tmsg1", soaper_->getTimestamp());
    if (soaper_->received()) {
        setString("soap-rmsg1", "<div style=\"text-align: center;\"> <span style='font-size:20px;'>&#10007;</span> </div>");
        setString("soap-smsg1", "");
    } else {
        setString("soap-rmsg1", "");
        setString("soap-smsg1", "<div style=\"text-align: center;\"> <span style='font-size:20px;'>&#10007;</span> </div>");
    }
    setString("soap-msg1", syntaxHighlightBodyHTML());
}

// -------------------------------- syntaxHighlightBodyHTML --------------------------------
std::string
pixel::utils::SOAPMsgHistoryHandler::syntaxHighlightBodyHTML(size_t const i){
    string body = soaper_->getBody(i);
    size_t start, end, ngreater;

    ngreater = body.find(">");
    while (ngreater!=string::npos) {
        body.replace(ngreater,1,"&gt;");

        ngreater = body.find(">", ngreater+4);
    }

    // Attributes
    start = body.find("<");
    if (start!=string::npos)
        body.replace(start,1,"&lt;");

    // Variables
    start = body.find("<", start+4);
    end = body.find("</");
    while (start<end) {
        body.replace(start,1,"&lt;");
        body.replace(end+3,1,"&lt;");
        body.insert(start, "</p>&nbsp;&nbsp;&nbsp;&nbsp;");

        start = body.find("<", end+28);
        end = body.find("</", end+28);
    }

    // End of attributes
    if (end!=string::npos) {
        body.replace(end,1,"&lt;");
        body.insert(end, "</p>");
    }

    // COLOR ^^
    size_t nend_var = body.find("="),
           ncolon, nspace, nstart_var;
    while (nend_var!=string::npos) {
        ncolon = body.find_last_of(":", nend_var);
        nspace = body.find_last_of(" ", nend_var);

        if (ncolon<nspace)
            nstart_var = nspace;
        else
            nstart_var = ncolon;

        if (nstart_var!=string::npos){
            body.insert(nend_var+1, "</font>");
            body.insert(nstart_var+1, "<font color=\"orange\">");
        }

        nend_var = body.find("=", nend_var+33);
    }

    size_t nstart_quotemark = body.find("\""),
           nend_quotemark, n;
    while (nstart_quotemark!=string::npos) {
        nend_quotemark = body.find("\"", nstart_quotemark+1);

        n = 1;
        if (nend_quotemark!=string::npos && body.at(nend_quotemark+1)!='>'){
            body.insert(nend_quotemark+1, "</font>");
            body.insert(nstart_quotemark, "<font color=\"mediumaquamarine\">");
            n = 39;
        }


        nstart_quotemark = body.find("\"", nend_quotemark+n);
    }

    return "<font color=\"dodgerblue\">" + body + "</font>";
}
