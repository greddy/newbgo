#include "pixel/utils/version.h"

#include "config/version.h"
#include "hyperdaq/version.h"
#include "pt/version.h"
#include "sentinel/utils/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xdaq2rc/version.h"
#include "xdata/version.h"
#include "xgi/version.h"
#include "xoap/version.h"
#include "xoap/filter/version.h"

#include "pixel/exception/version.h"
#include "pixel/hwlayer/version.h"

GETPACKAGEINFO(pixelutils)

void
pixelutils::checkPackageDependencies() {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(hyperdaq);
    CHECKDEPENDENCY(pt);
    CHECKDEPENDENCY(sentinelutils);
    CHECKDEPENDENCY(toolbox);
    CHECKDEPENDENCY(xcept);
    CHECKDEPENDENCY(xdaq);
    CHECKDEPENDENCY(xdaq2rc);
    CHECKDEPENDENCY(xdata);
    CHECKDEPENDENCY(xgi);
    CHECKDEPENDENCY(xoap);
    CHECKDEPENDENCY(xoapfilter);

    CHECKDEPENDENCY(pixelexception);
    CHECKDEPENDENCY(pixelhwlayer);
}

std::set<std::string, std::less<std::string> >
pixelutils::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, hyperdaq);
    ADDDEPENDENCY(dependencies, pt);
    ADDDEPENDENCY(dependencies, sentinelutils);
    ADDDEPENDENCY(dependencies, toolbox);
    ADDDEPENDENCY(dependencies, xcept);
    ADDDEPENDENCY(dependencies, xdaq);
    ADDDEPENDENCY(dependencies, xdaq2rc);
    ADDDEPENDENCY(dependencies, xdata);
    ADDDEPENDENCY(dependencies, xgi);
    ADDDEPENDENCY(dependencies, xoap);
    ADDDEPENDENCY(dependencies, xoapfilter);

    ADDDEPENDENCY(dependencies, pixelexception);
    ADDDEPENDENCY(dependencies, pixelhwlayer);

    return dependencies;
}
