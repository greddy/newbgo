/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2019, CERN.                                             *
 * All rights reserved.                                                  *
 * Authors: M. Alguacil, A. Datta and K. Padeken                         *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 *************************************************************************/

#include "pixel/utils/SOAPER.h"

#include <cassert>
#include <boost/filesystem.hpp>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/Application.h"
#include "xdaq/NamespaceURI.h"
#include "xdata/Boolean.h"
#include "xdata/soap/Serializer.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xoap/domutils.h"
#include "xoap/filter/MessageFilter.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPConstants.h"
#include "xoap/SOAPElement.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPFault.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPPart.h"

#include "pixel/exception/Exception.h"
#include "pixel/utils/Utils.h"

using namespace std;


pixel::utils::SOAPER::SOAPER(xdaq::Application *app, size_t const history_size) : app_(app),
                                                                                  sv_logger_(app_->getApplicationLogger()){
    if (history_size >= history_.max_size()) {
        history_size_ = history_.max_size()-1;
        LOGWARN(sv_logger_, "That size exceeds the limit. The new message history size is " << history_size_ << ".");
    }
    else
        history_size_ = history_size;
}

pixel::utils::SOAPER::SOAPER(const SOAPER &aSOAPER) : app_(aSOAPER.app_),
                                                      history_size_(aSOAPER.history_size_),
                                                      sv_logger_(aSOAPER.sv_logger_) {
    history_ = aSOAPER.history_;
}

// -------------------------------- setMessage --------------------------------
bool
pixel::utils::SOAPER::setMessage(xoap::MessageReference const &msg, bool received){
    if (history_size_==0) return false;

    string message, timestamp;
    char buf[30];
    timeval time;

    // Formatted time - string
    gettimeofday(&time, NULL); // Get time now
    tm * timeinfo = localtime(&time.tv_sec);
    strftime(buf, sizeof(buf), "%d %b %Y %T", timeinfo);
    timestamp = toolbox::toString("%s.%03d\n", buf, time.tv_usec/1000);

    // SOAP Message -> string
    msg->writeTo(message);

    if (history_.empty() || message.find(getBody())==string::npos) {
        if (history_.size() >= history_size_){
            ////LOGWARN(sv_logger_, "The message history is full. The first message will be removed.");
            history_.pop_front();
        }

        // Add S if the message was sent, or R if it was received
        history_.push_back(timestamp + (received ? "R " : "S ") + message);

        return true;
    }

    return false;
}


// -------------------------------- getLastMessages --------------------------------
std::deque<std::string>
pixel::utils::SOAPER::getLastMessages(size_t const n) const{
    if (n<history_.size()) {
        deque<string> lastMessages(history_.end()-n, history_.end());
        return lastMessages;
    }
    else
        return history_;
}

// -------------------------------- getFormattedSOAP --------------------------------
std::string
pixel::utils::SOAPER::getFormattedSOAP(std::string const &msg){
    string msg_return = msg,
           tabs;
    size_t end;

    // Envelope
    size_t start = msg_return.find("<");
    if (start!=string::npos)
        msg_return.insert(start, tabs);

    // Header
    start = msg_return.find("<", start+1);
    tabs = "\n\t";
    if (start!=string::npos)
        msg_return.insert(start, tabs);

    // Body
    start = msg_return.find("<", start+3);
    if (start!=string::npos)
        msg_return.insert(start, tabs);

    // Attributes
    start = msg_return.find("<", start+3);
    tabs += "\t";
    if (start!=string::npos)
        msg_return.insert(start, tabs);

    // Variables
    start = msg_return.find("<", start+4);
    end = msg_return.find("</");
    if (start<end)
        tabs += "\t";
    while (start<end){
        msg_return.insert(start, tabs);

        start = msg_return.find("<", end+5);
        end = msg_return.find("</", end+5);
    }

    // End of attributes, body and envelope
    while (end!=string::npos){
        if (tabs.size()>1)
            tabs = tabs.substr(0, tabs.size()-1);
        msg_return.insert(end, tabs);
        end = msg_return.find("</", end+tabs.size()+1);
    }

    return msg_return;
}

// -------------------------------- writeTo --------------------------------
void
pixel::utils::SOAPER::writeTo(std::ostream &stream, size_t const n) {
    for (deque<string>::iterator tmsg=(n>=history_.size() ? history_.begin():history_.end()-n); tmsg!=history_.end(); tmsg++)
        stream << ">> " << getFormattedSOAP(*tmsg) << endl << endl;
}

void
pixel::utils::SOAPER::writeTo(std::string const &filepath, size_t const n) {
    ofstream file;
    file.open(filepath.c_str(), ofstream::out);

    if (file.fail())
        LOGERROR(sv_logger_, "Error opening file: " << filepath);
    else {
        LOGINFO(sv_logger_, "Writing message history to the file " << filepath << " ...");

        file << "<html>" << endl;
        writeTo(file, n);
        file << "</html>" << endl;

        file.close();
    }
}

// -------------------------------- getTimestamp --------------------------------
std::string
pixel::utils::SOAPER::getTimestamp(size_t const i){
    return (i<history_.size() ? history_.at(i).substr(0, 24) : history_.back().substr(0, 24));
}

// -------------------------------- getBody --------------------------------
std::string
pixel::utils::SOAPER::getBody(size_t const i){
    string body = (i<history_.size() ? history_.at(i) : history_.back());
    size_t start = body.find("<env:Body>")+10,
           end = body.find("</env:Body>", start);

    if (start!=string::npos && end!=string::npos)
        body = body.substr(start, end-start);
    else
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Failed to extract the body from the SOAP message %d.", i));

    return body;
}

// -------------------------------- received or sent --------------------------------
bool
pixel::utils::SOAPER::received(size_t const i){
    const char value = (i<history_.size() ? history_.at(i).at(25) : history_.back().at(25));

    return (value=='R' ? true : false);
}


// --------------------------------  --------------------------------
std::string
pixel::utils::SOAPER::extractSOAPCommandName(xoap::MessageReference const &msg) {
    // Save the SOAP message in the history
    setMessage(msg, true);

    // The body should contain a single node with the name of the
    // command to execute.
    return extractBodyNode(msg).getElementName().getLocalName();
}

std::string
pixel::utils::SOAPER::extractSOAPCommandAttribute(xoap::MessageReference const &msg, std::string attribute){
    // Global Key should be an attribute for the command element
    xoap::SOAPName AttributeSOAPName(attribute, "", "");

    return extractBodyNode(msg).getAttributeValue(AttributeSOAPName);
}

std::string
pixel::utils::SOAPER::extractSOAPCommandRCMSURL(xoap::MessageReference const &msg) {
    // The body should contain a single node with the name of the
    // command to execute. The originator can include a new value of the
    // URL of where to send RCMS state change notifications using the
    // 'xdaq:rcmsURL' attribute.
    // NOTE: An empty string as rcmsURL is used to disable the
    // asynchronous state change notifications.
    xoap::SOAPName rcmsURLSOAPName("rcmsURL", "xdaq", "");

    return extractBodyNode(msg).getAttributeValue(rcmsURLSOAPName);
}

uint32_t
pixel::utils::SOAPER::extractSOAPCommandRCMSSessionId(xoap::MessageReference const &msg) {
    // The body should contain a single node with the name of the
    // command to execute. The originator may identify itself using the
    // 'xdaq:rcmsSessionId' attribute.

    //  // NOTE: The default value here also serves to flag 'invalid'
    //  // values.
    uint32_t const kDefaultVal = 0;

    uint32_t rcmsSessionId = kDefaultVal;
    // NOTE: The 'xdaq:rcmsSessionId' attribute is optional.
    if (hasSOAPCommandAttribute(msg, "rcmsSessionId")) {
        // The attribute is present. Now extract it and check its value.
        xoap::SOAPName rcmsSessionIdSOAPName("rcmsSessionId", "xdaq", "");
        std::string rcmsSessionIdTmp = extractBodyNode(msg).getAttributeValue(rcmsSessionIdSOAPName);
        try {
            rcmsSessionId = parseIntFromString(rcmsSessionIdTmp);
        }
        catch (xcept::Exception &err) {
            XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                        toolbox::toString("Failed to extract attribute 'xdaq:rcmsSessionId': '%s'.",
                                          err.message().c_str()));
        }

        // if (rcmsSessionId == kDefaultVal)
        //   {
        //     std::string const msg =
        //       toolbox::toString("The SOAP command attribute 'xdaq:rcmsSessionId' is optional, "
        //                         "but if present it should be non-zero.",
        //                         kDefaultVal);
        //     XCEPT_RAISE(pixel::exception::SOAPFormatProblem, msg);
        //   }
    }

    return rcmsSessionId;
}

std::string
pixel::utils::SOAPER::extractSOAPCommandRequestorId(xoap::MessageReference const &msg) {
    // The body should contain a single node with the name of the
    // command to execute. The originator may identify itself using the
    // 'xdaq:actionRequestorId' attribute.

    //  // NOTE: The default value here also serves to flag 'invalid'
    //  // values.
    std::string const kDefaultVal = "";

    std::string requestorId = kDefaultVal;
    // NOTE: The 'xdaq:actionRequestorId' attribute is optional.
    if (hasSOAPCommandAttribute(msg, "actionRequestorId")) {
        // The attribute is present. Now extract it.
        xoap::SOAPName requestorIdSOAPName("actionRequestorId", "xdaq", "");
        requestorId = extractBodyNode(msg).getAttributeValue(requestorIdSOAPName);

        // if (requestorId == kDefaultVal)
        //   {
        //     XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
        //                 "The SOAP command attribute attribute 'xdaq:actionRequestorId' is optional, "
        //                 "but if present it should be non-empty.");
        //   }
    }

    return requestorId;
}

pixel::utils::OwnerId
pixel::utils::SOAPER::extractSOAPCommandOwnerId(xoap::MessageReference const &msg) {
    return pixel::utils::OwnerId(extractSOAPCommandRCMSSessionId(msg), extractSOAPCommandRequestorId(msg));
}


// -------------------------------- extractBodyNode/s --------------------------------
xoap::SOAPElement
pixel::utils::SOAPER::extractBodyNode(xoap::MessageReference const &msg) {
    std::vector<xoap::SOAPElement> bodyList = extractBodyNodes(msg);
    if (bodyList.size() != 1) {
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Expected exactly one element "
                                      "in the body of the SOAP command message "
                                      "but found %d.",
                                      bodyList.size()));
    }

    return bodyList.at(0);
}

std::vector<xoap::SOAPElement>
pixel::utils::SOAPER::extractBodyNodes(xoap::MessageReference const &msg) {
    std::vector<xoap::SOAPElement> bodyList = msg->getSOAPPart().getEnvelope().getBody().getChildElements();
    std::vector<xoap::SOAPElement> res;

    // Remove 'empty' text nodes (e.g., line-breaks, etc.).
    for (std::vector<xoap::SOAPElement>::iterator i = bodyList.begin(); i != bodyList.end(); ++i) {
        if (i->getElementName().getQualifiedName() != "#text")
            res.push_back(*i);
    }

    return res;
}


// -------------------------------- extractChildNode --------------------------------
xoap::SOAPElement
pixel::utils::SOAPER::extractChildNode(xoap::SOAPElement &node,
                                       xoap::SOAPName &childName) {
    std::vector<xoap::SOAPElement> children = node.getChildElements(childName);
    if (children.size() != 1) {
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Expected exactly one child element "
                                      "with name '%s' "
                                      "but found %d.",
                                      childName.getQualifiedName().c_str(),
                                      children.size()));
    }
    return children.at(0);
}


// --------------------------------  --------------------------------
bool
pixel::utils::SOAPER::hasFault(xoap::MessageReference const &msg) const {
    return msg->getSOAPPart().getEnvelope().getBody().hasFault();
}

bool
pixel::utils::SOAPER::hasFaultDetail(xoap::MessageReference const &msg) const {
    return (hasFault(msg) ? msg->getSOAPPart().getEnvelope().getBody().getFault().hasDetail() : false);
}

std::string
pixel::utils::SOAPER::extractFaultString(xoap::MessageReference const &msg) const {
    return (hasFault(msg) ? msg->getSOAPPart().getEnvelope().getBody().getFault().getFaultString() :  "");
}

std::string
pixel::utils::SOAPER::extractFaultDetail(xoap::MessageReference const &msg) const {
    return (hasFaultDetail(msg) ? msg->getSOAPPart().getEnvelope().getBody().getFault().getDetail().getTextContent() : "");
}


// --------------------------------  --------------------------------
bool
pixel::utils::SOAPER::hasSOAPCommandAttribute(xoap::MessageReference const &msg,
                                            std::string const &attributeName) {
    bool res = false;
    xoap::SOAPName soapName(attributeName, "xdaq", "");
    try {
        // NOTE: Not very pretty, but this is what xoap::SOAPElement
        // does as well.
        DOMElement const *const node = dynamic_cast<DOMElement *>(extractBodyNode(msg).getDOM());
        res = node->hasAttribute(xoap::XStr(soapName.getQualifiedName()));
    }
    catch (DOMException &de) {
        std::string const msg =
            toolbox::toString("Failed to check SOAP message for 'xdaq:%s' attribute: '%s'.",
                              attributeName.c_str(),
                              xoap::XMLCh2String(de.msg).c_str());
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem, msg);
    }

    return res;
}

bool
pixel::utils::SOAPER::hasSOAPCommandParameter(xoap::MessageReference const &msg,
                                            std::string const &parameterName) {
    bool res = false;
    try {
        extractSOAPCommandParameterElement(msg, parameterName);
        res = true;
    }
    catch (pixel::exception::SOAPFormatProblem &e) {
        // Nothing to be done.
    }

    return res;
}

bool
pixel::utils::SOAPER::extractSOAPCommandParameterBool(xoap::MessageReference const &msg,
                                                    std::string const &parameterName) {
    xdata::soap::Serializer serializer;
    xdata::Boolean tmpVal;
    try {
        serializer.import(&tmpVal, extractSOAPCommandParameterElement(msg, parameterName).getDOM());
    }
    catch (xcept::Exception &err) {
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
                                      parameterName.c_str(),
                                      err.message().c_str()));
    }

    return bool(tmpVal);
}

std::string
pixel::utils::SOAPER::extractSOAPCommandParameterString(xoap::MessageReference const &msg,
                                                      std::string const &parameterName) {
    xdata::soap::Serializer serializer;
    xdata::String tmpVal;
    try {
        serializer.import(&tmpVal, extractSOAPCommandParameterElement(msg, parameterName).getDOM());
    }
    catch (xcept::Exception &err) {
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
                                      parameterName.c_str(),
                                      err.message().c_str()));
    }

    return string(tmpVal);
}

uint32_t
pixel::utils::SOAPER::extractSOAPCommandParameterUnsignedInteger(xoap::MessageReference const &msg,
                                                               std::string const &parameterName) {
    xdata::soap::Serializer serializer;
    xdata::UnsignedInteger32 tmpVal;
    try {
        serializer.import(&tmpVal, extractSOAPCommandParameterElement(msg, parameterName).getDOM());
    }
    catch (xcept::Exception &err) {
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Failed to import SOAP command parameter 'xdaq:%s': '%s'.",
                                      parameterName.c_str(),
                                      err.message().c_str()));
    }

    return uint32_t(tmpVal);
}


// -------------------------------- makePSXDPGetSOAPCmd --------------------------------
xoap::MessageReference
pixel::utils::SOAPER::makePSXDPGetSOAPCmd(std::string const &dpName) {
    string const PSX_NS_URI = "http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd";

    xoap::MessageReference msg = xoap::createMessage();
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration("xsi",
                                     "http://www.w3.org/2001/XMLSchema-instance");
    envelope.addNamespaceDeclaration("xsd",
                                     "http://www.w3.org/2001/XMLSchema");
    envelope.addNamespaceDeclaration("soapenc",
                                     "http://schemas.xmlsoap.org/soap/encoding/");
    xoap::SOAPName commandName = envelope.createName("dpGet",
                                                     "psx",
                                                     PSX_NS_URI);
    xoap::SOAPElement commandElement = envelope.getBody().addBodyElement(commandName);

    // The main datapoint.
    xoap::SOAPName dataPointName = envelope.createName("dp",
                                                       "psx",
                                                       PSX_NS_URI);
    xoap::SOAPName nameAttribute = envelope.createName("name",
                                                       "",
                                                       "");
    commandElement.addChildElement(dataPointName).addAttribute(nameAttribute,
                                                               dpName + ":_online.._value");

    // The (first two) user-bits encoding the DIP quality.
    // - User-bit 1.
    xoap::SOAPName dataPointUserBit1Name = envelope.createName("dp",
                                                               "psx",
                                                               PSX_NS_URI);
    xoap::SOAPName nameUserBit1Attribute = envelope.createName("name",
                                                               "",
                                                               "");
    commandElement.addChildElement(dataPointUserBit1Name).addAttribute(nameUserBit1Attribute, dpName + ":_online.._userbit1");
    // - User-bit 2.
    xoap::SOAPName dataPointUserBit2Name = envelope.createName("dp",
                                                               "psx",
                                                               PSX_NS_URI);
    xoap::SOAPName nameUserBit2Attribute = envelope.createName("name",
                                                               "",
                                                               "");
    commandElement.addChildElement(dataPointUserBit2Name).addAttribute(nameUserBit2Attribute, dpName + ":_online.._userbit2");

    // The 'invalid' bit from PVSS.
    xoap::SOAPName dataPointInvalidFlagName = envelope.createName("dp",
                                                                  "psx",
                                                                  PSX_NS_URI);
    xoap::SOAPName nameInvalidFlagAttribute = envelope.createName("name",
                                                                  "",
                                                                  "");
    commandElement.addChildElement(dataPointInvalidFlagName).addAttribute(nameInvalidFlagAttribute, dpName + ":_online.._invalid");

    return msg;
}


// -------------------------------- extractPSXDPGetReply --------------------------------
std::string
pixel::utils::SOAPER::extractPSXDPGetReply(xoap::MessageReference &msg,
                                         std::string const &dpName) {
    xoap::filter::MessageFilter filter("//psx:dp[@name='" + dpName + "']");
    if (!filter.match(msg)) {
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Could not find parameter '%s' in PSX dpGet SOAP reply.",
                                      dpName.c_str()));
    }
    std::list<xoap::SOAPElement> elements = filter.extract(msg);
    if (elements.size() == 0) {
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Could not find parameter '%s' in PSX dpGet SOAP reply.",
                                      dpName.c_str()));
    } else if (elements.size() > 1) {           ;
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Found %d replies (instead of 1) for parameter '%s' in PSX dpGet SOAP reply.",
                                      elements.size(),
                                      dpName.c_str()));
    }

    return elements.front().getTextContent();
}


// --------------------------------  --------------------------------
xoap::SOAPElement
pixel::utils::SOAPER::extractSOAPCommandParameterElement(xoap::MessageReference const &msg,
                                                       std::string const &parameterName) {
    // The body should contain a single node with the name of the
    // command to execute, with (sometimes optional) parameters.
    xoap::SOAPElement commandNode = extractBodyNode(msg);
    xoap::SOAPName soapName(parameterName, "xdaq", "");

    return extractChildNode(commandNode, soapName);
}

// --------------------------------  --------------------------------
void
pixel::utils::SOAPER::exportAll(bool const &val,
                                xoap::SOAPElement element){
    // https://gitlab.cern.ch/cmsos/core/blob/baseline_silicon_14/xdata/include/xdata/soap/Serializer.h
    xdata::soap::Serializer serializer;

    xdata::Boolean tmpVal(val);
    serializer.exportAll(&tmpVal, dynamic_cast<DOMElement *>(element.getDOM()), true);
}

void
pixel::utils::SOAPER::exportAll(string const &val,
                                xoap::SOAPElement element){
    xdata::soap::Serializer serializer;

    xdata::String tmpVal(val);
    serializer.exportAll(&tmpVal, dynamic_cast<DOMElement *>(element.getDOM()), true);
}

void
pixel::utils::SOAPER::exportAll(uint32_t const &val,
                                xoap::SOAPElement element){
    xdata::soap::Serializer serializer;

    xdata::UnsignedInteger32 tmpVal(val);
    serializer.exportAll(&tmpVal, dynamic_cast<DOMElement *>(element.getDOM()), true);
}

template <typename T>
void
pixel::utils::SOAPER::addSOAPReplyParameter(xoap::MessageReference const &msg,
                                          string const &parameterName,
                                          T const &val) {
    xoap::SOAPName elementName = msg->getSOAPPart().getEnvelope().createName(parameterName, "xdaq", XDAQ_NS_URI);

    // Export the data type into an existing DOM document.
    try {
        exportAll(val, extractBodyNode(msg).addChildElement(elementName));
    }
    catch (xcept::Exception &err) {
        XCEPT_RAISE(pixel::exception::RuntimeProblem,
                    toolbox::toString("Failed to export SOAP command parameter 'xdaq:%s': '%s'.",
                                      parameterName.c_str(),
                                      err.message().c_str()));
    }
}
//Explicit instantiations
template void pixel::utils::SOAPER::addSOAPReplyParameter<bool>(xoap::MessageReference const &msg, string const &parameterName,
                                               bool const &val);
template void pixel::utils::SOAPER::addSOAPReplyParameter<string>(xoap::MessageReference const &msg, string const &parameterName,
                                               string const &val);
template void pixel::utils::SOAPER::addSOAPReplyParameter<uint32_t>(xoap::MessageReference const &msg, string const &parameterName,
                                               uint32_t const &val);


// -------------------------------- makeParameterGetSOAPCmd --------------------------------
xoap::MessageReference
pixel::utils::SOAPER::makeParameterGetSOAPCmd(std::string const &className,
                                              std::string const &parName,
                                              std::string const &parType) {
    std::string const applicationNameSpace =
        toolbox::toString("urn:xdaq-application:%s", className.c_str());
    xoap::MessageReference msg = xoap::createMessage();
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration("xsi",
                                     "http://www.w3.org/2001/XMLSchema-instance");
    envelope.addNamespaceDeclaration("xsd",
                                     "http://www.w3.org/2001/XMLSchema");
    envelope.addNamespaceDeclaration("soapenc",
                                     "http://schemas.xmlsoap.org/soap/encoding/");
    xoap::SOAPName commandName = envelope.createName("ParameterGet",
                                                     "xdaq",
                                                     XDAQ_NS_URI);
    xoap::SOAPElement commandElement = envelope.getBody().addBodyElement(commandName);
    xoap::SOAPName propertiesName = envelope.createName("properties",
                                                        "p",
                                                        applicationNameSpace);
    xoap::SOAPElement propertiesElement = commandElement.addChildElement(propertiesName);
    xoap::SOAPName propertiesTypeName = envelope.createName("type",
                                                            "xsi",
                                                            "http://www.w3.org/2001/XMLSchema-instance");
    propertiesElement.addAttribute(propertiesTypeName, "soapenc:Struct");

    xoap::SOAPName propertyName = envelope.createName(parName, "p", applicationNameSpace);
    xoap::SOAPName propertyTypeName = envelope.createName("type",
                                                          "xsi",
                                                          "http://www.w3.org/2001/XMLSchema-instance");
    propertiesElement.addChildElement(propertyName).addAttribute(propertyTypeName, parType);

    return msg;
}


// -------------------------------- makeSOAPFaultReply & makeCommandSOAPReply --------------------------------
xoap::MessageReference
pixel::utils::SOAPER::makeSOAPFaultReply(xoap::MessageReference const &msg,
                                         SOAPFaultCodeType const faultCode,
                                         std::string const &faultString,
                                         std::string const &faultDetail) {
    return makeSOAPFaultReply(msg->getImplementationFactory()->getProtocolVersion(), faultCode,
                              toolbox::toString("Service %s: %s",
                                                app_->getApplicationDescriptor()->getAttribute("service").c_str(),
                                                faultString.c_str()),
                              faultDetail);
}

xoap::MessageReference
pixel::utils::SOAPER::makeCommandSOAPReply(xoap::MessageReference const &msg,
                                         std::string const &fsmState) {
    return makeCommandSOAPReply(msg->getImplementationFactory()->getProtocolVersion(), extractSOAPCommandName(msg), fsmState);
}

xoap::MessageReference
pixel::utils::SOAPER::makeCommandSOAPReply(xoap::MessageReference const &msg,
                                         std::string const &replyMsg,
                                         std::string const &fsmState) {
    return makeCommandSOAPReply(msg->getImplementationFactory()->getProtocolVersion(), extractSOAPCommandName(msg), replyMsg, fsmState);
}


// -------------------------------- makeSOAPFaultReply & makeCommandSOAPReply --------------------------------
xoap::MessageReference
pixel::utils::SOAPER::makeSOAPFaultReply(std::string const &soapProtocolVersion,
                                       SOAPFaultCodeType const faultCode,
                                       std::string const &faultString,
                                       std::string const &faultDetail) {
    xoap::MessageFactory *messageFactory = xoap::MessageFactory::getInstance(soapProtocolVersion);
    xoap::MessageReference reply = messageFactory->createMessage();
    xoap::SOAPFault fault = reply->getSOAPPart().getEnvelope().getBody().addFault();
    if (messageFactory->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL) {
        switch (faultCode) {
        case SOAPFaultCodeSender:
            fault.setFaultCode("Client");
            break;
        case SOAPFaultCodeReceiver:
            fault.setFaultCode("Server");
            break;
        default:
            // Should never get here.
            XCEPT_RAISE(pixel::exception::SOAPFormatProblem, "Unknown SOAP fault code type.");
            break;
        }
        fault.setFaultString(faultString);
    } else if (messageFactory->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_2_PROTOCOL) {
        std::string faultCodeString = "";
        switch (faultCode) {
        case SOAPFaultCodeSender:
            faultCodeString = "Sender";
            break;
        case SOAPFaultCodeReceiver:
            faultCodeString = "Receiver";
            break;
        default:
            // Should never get here.
            XCEPT_RAISE(pixel::exception::SOAPFormatProblem, "Unknown SOAP fault code type.");
            break;
        }
        xoap::SOAPName faultCodeQName(faultCodeString,
                                      messageFactory->getEnvelopePrefix(),
                                      xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
        fault.setFaultCode(faultCodeQName);
        fault.addFaultReasonText(faultString, std::locale("en_US"));
    } else {
        // Should never get here.
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Unknown SOAP protocol: %s.", messageFactory->getProtocolVersion().c_str()));
    }

    if (faultDetail != "") {
        xoap::SOAPElement detail = fault.addDetail();
        detail.addTextNode(faultDetail);
    }

    return reply;
}

xoap::MessageReference
pixel::utils::SOAPER::makeCommandSOAPReply(std::string const &soapProtocolVersion,
                                         std::string const &command,
                                         std::string const &fsmState) {
    xoap::MessageFactory *messageFactory = xoap::MessageFactory::getInstance(soapProtocolVersion);
    xoap::MessageReference reply = messageFactory->createMessage();
    xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPBody body = envelope.getBody();
    xoap::SOAPName _command = envelope.createName(fsmState, "xdaq", XDAQ_NS_URI);


    body.addBodyElement(_command);

    return reply;
}

xoap::MessageReference
pixel::utils::SOAPER::makeCommandSOAPReply(std::string const &soapProtocolVersion,
                                         std::string const &command,
                                         std::string const &replyMsg,
                                         std::string const &fsmState) {
    xoap::MessageReference reply =
        makeCommandSOAPReply(soapProtocolVersion, command, fsmState);
    xoap::SOAPName elementName = reply->getSOAPPart().getEnvelope().createName("contents", "xdaq", XDAQ_NS_URI);
    extractBodyNode(reply).addChildElement(elementName).addTextNode(replyMsg);

    return reply;
}


// -------------------------------- getSOAPCommandVariables --------------------------------
Variables
pixel::utils::SOAPER::getSOAPCommandVariables(xoap::MessageReference const &msg){
    Variables returnMap;

    // Retrieve all child elements
    vector<xoap::SOAPElement> bodyList = msg->getSOAPPart().getEnvelope().getBody().getChildElements();

    // Store the variable name and value, and ignore the types
    for (vector<xoap::SOAPElement>::iterator it = bodyList.begin(); it != bodyList.end(); it++){
        vector<xoap::SOAPElement> elements = it->getChildElements();
        for (vector<xoap::SOAPElement>::iterator jt = elements.begin(); jt != elements.end(); jt++) {
            returnMap[jt->getElementName().getLocalName()].push_back(jt->getTextContent());
        }
    }

    // Return names and associated values
    return returnMap;
}


// -------------------------------- getSOAPCommandAttributes --------------------------------
Attributes
pixel::utils::SOAPER::getAllSOAPCommandAttributes(xoap::MessageReference const &msg,
                                               std::vector<std::string> const &parameterNames) {
    // Map that will contain the attribute names and the value associated with each one.
    Attributes parameters;

    //Get all attributes of the SOAP message
    try {
        DOMNamedNodeMap * attributes = extractBodyNode(msg).getDOM()->getAttributes();

        for (XMLSize_t i=0; i<attributes->getLength()-1; i++){
            string name = xoap::XMLCh2String(attributes->item(i)->getNodeName());
            try {
                parameters[name] = xoap::XMLCh2String(attributes->item(i)->getNodeValue());

                if (name != "" && parameters[name] == "")
                    XCEPT_RAISE(xoap::exception::Exception, toolbox::toString("Empty string passed for the parameter %s!", name.c_str()));
            }
            catch (xoap::exception::Exception &e) {
                string const msg_error_rcv = toolbox::toString("Looking for parameter that does not exist! Empty string passed for the parameter %s!", name.c_str());
                LOGERROR(sv_logger_, msg_error_rcv);
                XCEPT_RETHROW(xoap::exception::Exception, msg_error_rcv, e);
            }
        }
    }
    catch (DOMException& de) {
        XCEPT_RAISE(pixel::exception::SOAPFormatProblem,
                    toolbox::toString("Failed to extract attributes. %s", xoap::XMLCh2String(de.msg).c_str()));
    }

    // Check if the specified parameters are present
    for (vector<string>::const_iterator name=parameterNames.begin(); name!=parameterNames.end(); name++){

        try {
            if (*name != "" && parameters[*name] == "")
                XCEPT_RAISE(xoap::exception::Exception, toolbox::toString("Parameter %s does not exist in the list of incoming parameters!", name->c_str()));
        }
        catch (xoap::exception::Exception &e) {
            string const msg_error_rcv = toolbox::toString("Looking for parameter that does not exist! Parameter %s does not exist in the list of incoming parameters!", name->c_str());
            LOGERROR(sv_logger_, msg_error_rcv);
            XCEPT_RETHROW(xoap::exception::Exception, msg_error_rcv, e);
        }
    }

    return parameters;
}

Attributes
pixel::utils::SOAPER::getSomeSOAPCommandAttributes(xoap::MessageReference const &msg,
                                                   std::vector<std::string> const &parameterNames) {
    // Map that will contain the attribute names and the value associated with each one.
    Attributes parameters;

    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPElement command = extractBodyNode(msg);

    for (vector<string>::const_iterator paramName = parameterNames.begin(); paramName != parameterNames.end(); paramName++) {
        xoap::SOAPName name = envelope.createName(*paramName);

        try{
            parameters[*paramName] = command.getAttributeValue(name);

            if (*paramName != "" && parameters[*paramName] == "")
                XCEPT_RAISE(xoap::exception::Exception, "Parameter "+*paramName+" does not exist in the list of incoming parameters! It could also be because you passed an empty string");
        }
        catch (xoap::exception::Exception &e) {
            string const msg_error_rcv = "Looking for parameter that does not exist! Parameter " + *paramName + " does not exist in the list of incoming parameters! It could also be because you passed an empty string.";
            LOGERROR(sv_logger_, msg_error_rcv);
            XCEPT_RETHROW(xoap::exception::Exception, msg_error_rcv, e);
        }
    }

    return parameters;
}

// -------------------------------- addSOAPCommandAttribute/s --------------------------------
void
pixel::utils::SOAPER::addSOAPCommandAttribute(xoap::MessageReference const &msg,
                                              std::string const &attributeName,
                                              std::string const &attributeValue) {
    xoap::SOAPName elementName = msg->getSOAPPart().getEnvelope().createName(attributeName);
    extractBodyNode(msg).addAttribute(elementName, attributeValue);
}


void
pixel::utils::SOAPER::addSOAPCommandAttributes(xoap::MessageReference const &msg,
                                               Attributes const &attributes) {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPElement bodyElement = extractBodyNode(msg);

    for(Attributes::const_iterator attrib = attributes.begin(); attrib != attributes.end(); attrib++){
        // name = attrib->first
        // value = attrib->second
        xoap::SOAPName attributeName = envelope.createName(attrib->first);
        bodyElement.addAttribute(attributeName, attrib->second);
    }
}

// -------------------------------- addSOAPCommandVariable/s --------------------------------
void
pixel::utils::SOAPER::addSOAPCommandVariable(xoap::MessageReference const &msg,
                                             std::string const &variableName,
                                             std::string const &variableValue,
                                             std::string const &type = "") {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();

    xoap::SOAPName subelementName = envelope.createName(variableName, "xdaq", XDAQ_NS_URI);
    xoap::SOAPElement subelementElement = extractBodyNode(msg).addChildElement(subelementName);

    xoap::SOAPName attributeName = envelope.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
    subelementElement.addAttribute(attributeName, "xsd:" + type);

    subelementElement.setTextContent(variableValue);
}

void
pixel::utils::SOAPER::addSOAPCommandVariables(xoap::MessageReference const &msg,
                                              Variables const &vars,
                                              std::vector<std::string> const &alterVars) {
    if (alterVars.empty())
        return addSOAPCommandVariables(msg, vars);

    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPElement bodyElement = extractBodyNode(msg);

    xoap::SOAPName attributeName = envelope.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance");

    try {
        for (size_t i=0; i<vars.at(alterVars.at(0)).size(); ++i) {
            for (vector<string>::const_iterator name=alterVars.begin(); name!=alterVars.end(); ++name) {
                xoap::SOAPName subelementName = envelope.createName(*name, "xdaq", XDAQ_NS_URI);
                xoap::SOAPElement subelementElement = bodyElement.addChildElement(subelementName);

                subelementElement.addAttribute(attributeName, "xsd:");
                subelementElement.setTextContent(vars.at(*name).at(i));
            }
        }
    }
    catch(std::out_of_range &oor) {
        LOGERROR(sv_logger_, "Not all variables could be added to the SOAP message.");
        XCEPT_RAISE(xoap::exception::Exception, "Not all variables could be added to the SOAP message.");
    }


    //  - name = var->first
    //  - values = var->second
    size_t nfind = 0;
    for (Variables::const_iterator var=vars.begin(); var!=vars.end(); ++var) {
        if(nfind==alterVars.size() && find(alterVars.begin(), alterVars.end(), var->first)==alterVars.end()) {
            xoap::SOAPName subelementName = envelope.createName(var->first, "xdaq", XDAQ_NS_URI);

            for (vector<string>::const_iterator it=var->second.begin(); it!=var->second.end(); it++) {
                xoap::SOAPElement subelementElement = bodyElement.addChildElement(subelementName);

                subelementElement.addAttribute(attributeName, "xsd:");
                subelementElement.setTextContent(*it);
            }
        }
        else
            ++nfind;
    }
}

void
pixel::utils::SOAPER::addSOAPCommandVariables(xoap::MessageReference const &msg,
                                              Variables const &vars) {
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPElement bodyElement = extractBodyNode(msg);

    //  - name = var->first
    //  - values = var->second
    xoap::SOAPName attributeName = envelope.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
    for (Variables::const_iterator var=vars.begin(); var!=vars.end(); var++) {
        xoap::SOAPName subelementName = envelope.createName(var->first, "xdaq", XDAQ_NS_URI);

        for (vector<string>::const_iterator it=var->second.begin(); it!=var->second.end(); it++) {
            xoap::SOAPElement subelementElement = bodyElement.addChildElement(subelementName);

            subelementElement.addAttribute(attributeName, "xsd:");
            subelementElement.setTextContent(*it);
        }
    }
}

void
pixel::utils::SOAPER::addSOAPCommandVariables(xoap::MessageReference const &msg,
                                              VariablesWithType const &vars){
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPElement bodyElement = extractBodyNode(msg);

    //  - type = var->first
    //  - name = it->first
    //  - values = it->second
    xoap::SOAPName attributeName = envelope.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
    for (VariablesWithType::const_iterator var=vars.begin(); var!=vars.end(); var++) {
        for (Variables::const_iterator it=var->second.begin(); it!=var->second.end(); it++) {
            xoap::SOAPName subelementName = envelope.createName(it->first, "xdaq", XDAQ_NS_URI);

            for (vector<string>::const_iterator value=it->second.begin(); value!=it->second.end(); value++) {
                xoap::SOAPElement subelementElement = bodyElement.addChildElement(subelementName);

                subelementElement.addAttribute(attributeName, "xsd:" + var->first);
                subelementElement.setTextContent(*value);
            }
        }
    }
}
void
pixel::utils::SOAPER::addSOAPCommandVariables(xoap::MessageReference const &msg,
                                              VariablesWithType const &vars,
                                              std::vector<std::string> const &alterVars){
    if (alterVars.empty())
        return addSOAPCommandVariables(msg, vars);

    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPElement bodyElement = extractBodyNode(msg);

    xoap::SOAPName attributeName = envelope.createName("type", "xsi", "http://www.w3.org/2001/XMLSchema-instance");
    map<string, string> types;

    //  - type = var->first
    //  - name = it->first
    //  - values = it->second
    size_t nfind = 0,
           sizeVars = 0;
    for (VariablesWithType::const_iterator var=vars.begin(); var!=vars.end(); ++var) {
        for (Variables::const_iterator it=var->second.begin(); it!=var->second.end(); ++it) {
            if(nfind==alterVars.size() && find(alterVars.begin(), alterVars.end(), it->first)==alterVars.end()) {
                xoap::SOAPName subelementName = envelope.createName(it->first, "xdaq", XDAQ_NS_URI);

                for (vector<string>::const_iterator value=it->second.begin(); value!=it->second.end(); ++value) {
                    xoap::SOAPElement subelementElement = bodyElement.addChildElement(subelementName);

                    subelementElement.addAttribute(attributeName, "xsd:" + var->first);
                    subelementElement.setTextContent(*value);
                }
            }
            else {
                if (nfind==0) sizeVars = it->second.size();
                else if(it->second.size()!=sizeVars) {
                    LOGERROR(sv_logger_, "Not all variables could be added to the SOAP message.");
                    XCEPT_RAISE(xoap::exception::Exception, "Not all variables could be added to the SOAP message.");
                }
                ++nfind;
                types[it->first] = var->first;
            }
        }
    }

    for (size_t i=0; i<sizeVars; ++i) {
        for (vector<string>::const_iterator name=alterVars.begin(); name!=alterVars.end(); ++name) {
            xoap::SOAPName subelementName = envelope.createName(*name, "xdaq", XDAQ_NS_URI);
            xoap::SOAPElement subelementElement = bodyElement.addChildElement(subelementName);

            subelementElement.addAttribute(attributeName, "xsd:"+types[*name]);
            subelementElement.setTextContent(vars.at(types[*name]).at(*name).at(i));
        }
    }
}

// -------------------------------- makeSOAPMessageReference --------------------------------
xoap::MessageReference
pixel::utils::SOAPER::makeSOAPMessageReference(string const &cmd,
                                               Attributes const &attributes){
    xoap::MessageReference msg = xoap::createMessage();
    msg->getMimeHeaders()->setHeader("xdaq-pthttp-connection-timeout", "60");
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    xoap::SOAPName command = envelope.createName(cmd, "xdaq", XDAQ_NS_URI);
    envelope.getBody().addBodyElement(command);

    if (!attributes.empty())
        addSOAPCommandAttributes(msg, attributes);

    return msg;
}

template <typename U>
xoap::MessageReference
pixel::utils::SOAPER::makeSOAPMessageReference(std::string const &cmd,
                                               Attributes const &attributes,
                                               U const &vars,
                                               std::vector<std::string> const &alterVars){
    xoap::MessageReference msg = xoap::createMessage();
    msg->getMimeHeaders()->setHeader("xdaq-pthttp-connection-timeout", "60");
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
    envelope.addNamespaceDeclaration("xsi", "http://www.w3.org/2001/XMLSchema-instance");
    envelope.addNamespaceDeclaration("xsd", "http://www.w3.org/2001/XMLSchema");
    xoap::SOAPName command = envelope.createName(cmd, "xdaq", XDAQ_NS_URI);
    envelope.getBody().addBodyElement(command);

    addSOAPCommandAttributes(msg, attributes);
    addSOAPCommandVariables(msg, vars, alterVars);

    return msg;
}
//Explicit instantiations
template xoap::MessageReference pixel::utils::SOAPER::makeSOAPMessageReference(string const &cmd,
                                                                               Attributes const &attributes,
                                                                               Variables const &vars,
                                                                               std::vector<std::string> const &alterVars);
template xoap::MessageReference pixel::utils::SOAPER::makeSOAPMessageReference(string const &cmd,
                                                                               Attributes const &attributes,
                                                                               VariablesWithType const &vars,
                                                                               std::vector<std::string> const &alterVars);


xoap::MessageReference
pixel::utils::SOAPER::makeSOAPMessageReference(std::string const &cmd,
                                               std::string const &filepath) {
    LOGINFO(sv_logger_, "SOAP XML file path : " << filepath);

    xoap::MessageReference msg = xoap::createMessage();
    msg->getMimeHeaders()->setHeader("xdaq-pthttp-connection-timeout", "60");
    xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();

    xoap::AttachmentPart *attachment = msg->createAttachmentPart();
    attachment->setContent(filepath);
    attachment->setContentId("SOAPTEST1");
    attachment->addMimeHeader("Content-Description", "This is a SOAP message with attachments");
    msg->addAttachmentPart(attachment);

    xoap::SOAPName command = envelope.createName(cmd, "xdaq", XDAQ_NS_URI);

    envelope.getBody().addBodyElement(command);

    return msg;
}


// -------------------------------- send --------------------------------
std::string
pixel::utils::SOAPER::send(const xdaq::ApplicationDescriptor *d,
                           std::string const &cmd, Attributes const &attrib)
                             {
    xoap::MessageReference msg;
    try {
        msg = makeSOAPMessageReference(cmd, attrib);
        // Save the SOAP message in the history
        setMessage(msg, false);

        msg->getMimeHeaders()->setHeader("Content-Location", d->getURN());
        msg->getMimeHeaders()->setHeader("xdaq-pthttp-connection-timeout", "60");

        return extractSOAPCommandName(app_->getApplicationContext()->postSOAP(msg, *(app_->getApplicationDescriptor()), *d));
    }
    catch (xdaq::exception::Exception &e) {
        string message;
        msg->writeTo(message);

        LOGERROR(sv_logger_, "This application failed to send a SOAP message to "
                             << d->getClassName() << " instance " << d->getInstance()
                             << " with command = " << cmd
                             << " re-throwing exception = " << xcept::stdformat_exception_history(e) << endl
                             << message << endl);
        XCEPT_RETHROW(xdaq::exception::Exception, "Failed to send SOAP command.", e);
    }
}

// CHANGE - C++14
template <typename U>
std::string
pixel::utils::SOAPER::send(const xdaq::ApplicationDescriptor *d,
                           std::string const &cmd, Attributes const &attrib,
                           U const &vars,
                           std::vector<std::string> const &alterVars){

    xoap::MessageReference msg;
    try {
        msg = makeSOAPMessageReference(cmd, attrib, vars, alterVars);
        // Save the SOAP message in the history
        setMessage(msg, false);

        msg->getMimeHeaders()->setHeader("Content-Location", d->getURN());

        return extractSOAPCommandName(app_->getApplicationContext()->postSOAP(msg, *(app_->getApplicationDescriptor()), *d));
    }
    catch (xdaq::exception::Exception &e) {
        string message;
        msg->writeTo(message);

        LOGERROR(sv_logger_, "This application failed to send a SOAP message to "
                             << d->getClassName() << " instance " << d->getInstance()
                             << " with command = " << cmd
                             << " re-throwing exception = " << xcept::stdformat_exception_history(e) << endl
                             << message << endl);
        XCEPT_RETHROW(xdaq::exception::Exception, "Failed to send SOAP command.", e);
    }
}
//Explicit instantiations
template std::string pixel::utils::SOAPER::send(const xdaq::ApplicationDescriptor *d,
                                                string const &cmd, Attributes const &attrib,
                                                Variables const &vars,
                                                std::vector<std::string> const &alterVars);
template std::string pixel::utils::SOAPER::send(const xdaq::ApplicationDescriptor *d,
                                                string const &cmd, Attributes const &attrib,
                                                VariablesWithType const &vars,
                                                std::vector<std::string> const &alterVars);




// -------------------------------- sendWithSOAPReply --------------------------------
xoap::MessageReference
pixel::utils::SOAPER::sendWithSOAPReply(const xdaq::ApplicationDescriptor *d,
                                        xoap::MessageReference msg)
                                         {
    try {
        xoap::MessageReference reply = app_->getApplicationContext()->postSOAP(msg, *(app_->getApplicationDescriptor()), *d);
        // Save the SOAP message in the history
        setMessage(msg, false);

        msg->getMimeHeaders()->setHeader("Content-Location", d->getURN());
        msg->getMimeHeaders()->setHeader("xdaq-pthttp-connection-timeout", "60");

        // Save the SOAP response message in the history
        setMessage(reply, true);

        return reply;
    }
    catch (xdaq::exception::Exception &e) {
        string message;
        msg->writeTo(message);

        LOGERROR(sv_logger_, "This application failed to send a SOAP message to "
                             << d->getClassName() << " instance " << d->getInstance()
                             << " re-throwing exception = " << xcept::stdformat_exception_history(e) << endl
                             << message << endl);
        XCEPT_RETHROW(xdaq::exception::Exception, "Failed to send SOAP command.", e);
    }
}

////template <typename U>
xoap::MessageReference
pixel::utils::SOAPER::sendWithSOAPReply(const xdaq::ApplicationDescriptor *d,
                                        std::string const &cmd, Attributes const &attrib)
                                        ////U vars)
                                         {
    xoap::MessageReference msg;
    try{
        msg = makeSOAPMessageReference(cmd, attrib);
        // Save the SOAP message in the history
        setMessage(msg, false);

        msg->getMimeHeaders()->setHeader("Content-Location", d->getURN());
        msg->getMimeHeaders()->setHeader("xdaq-pthttp-connection-timeout", "60");
        xoap::MessageReference reply = app_->getApplicationContext()->postSOAP(msg, *(app_->getApplicationDescriptor()), *d);

        // Save the SOAP response message in the history
        setMessage(reply, true);

        return reply;
    }
    catch (xdaq::exception::Exception &e) {
        string message;
        msg->writeTo(message);

        LOGERROR(sv_logger_, "This application failed to send a SOAP message to "
                             << d->getClassName() << " instance " << d->getInstance()
                             << " with command = " << cmd
                             << " re-throwing exception = " << xcept::stdformat_exception_history(e) << endl
                             << message << endl);
        XCEPT_RETHROW(xdaq::exception::Exception, "Failed to send SOAP command.", e);
    }
}

// CHANGE - C++14
template <typename U>
xoap::MessageReference
pixel::utils::SOAPER::sendWithSOAPReply(const xdaq::ApplicationDescriptor *d,
                                        std::string const &cmd, Attributes const &attrib,
                                        U const &vars,
                                        std::vector<std::string> const &alterVars){

    xoap::MessageReference msg;
    try{
        msg = makeSOAPMessageReference(cmd, attrib, vars, alterVars);
        // Save the SOAP message in the history
        setMessage(msg, false);

        msg->getMimeHeaders()->setHeader("Content-Location", d->getURN());
        msg->getMimeHeaders()->setHeader("xdaq-pthttp-connection-timeout", "60");
        xoap::MessageReference reply = app_->getApplicationContext()->postSOAP(msg, *(app_->getApplicationDescriptor()), *d);

        // Save the SOAP response message in the history
        setMessage(reply, true);

        return reply;
    }
    catch (xdaq::exception::Exception &e) {
        string message;
        msg->writeTo(message);

        LOGERROR(sv_logger_, "This application failed to send a SOAP message to "
                             << d->getClassName() << " instance " << d->getInstance()
                             << " with command = " << cmd
                             << " re-throwing exception = " << xcept::stdformat_exception_history(e) << endl
                             << message << endl);
        XCEPT_RETHROW(xdaq::exception::Exception, "Failed to send SOAP command.", e);
    }
}
//Explicit instantiations
template xoap::MessageReference pixel::utils::SOAPER::sendWithSOAPReply(const xdaq::ApplicationDescriptor *d,
                                                                      string const &cmd, Attributes const &attrib,
                                                                      Variables const &vars,
                                                                      std::vector<std::string> const &alterVars);
template xoap::MessageReference pixel::utils::SOAPER::sendWithSOAPReply(const xdaq::ApplicationDescriptor *d,
                                                                      string const &cmd, Attributes const &attrib,
                                                                      VariablesWithType const &vars,
                                                                      std::vector<std::string> const &alterVars);

// -------------------------------- sendStatus --------------------------------
std::string
pixel::utils::SOAPER::sendStatus(const xdaq::ApplicationDescriptor *d,
                                 std::string const &msg){
        string cmd = "StatusNotification";
        try {
            timeval tv; //keep track of when the message comes
            gettimeofday(&tv, NULL);

            Attributes parameters;
            parameters["Description"] = msg;
            parameters["Time"] = toolbox::toString("%d", tv.tv_sec);
            parameters["usec"] = toolbox::toString("%d", tv.tv_usec);

            return send(d, cmd, parameters);
        }
        catch (xdaq::exception::Exception &e) {
            LOGERROR(sv_logger_, "This application failed to send a SOAP message to "
                                 << d->getClassName() << " instance " << d->getInstance()
                                 << " with command = " << cmd
                                 << " re-throwing exception = " << xcept::stdformat_exception_history(e) << endl);
            XCEPT_RETHROW(xdaq::exception::Exception, "Failed to send SOAP command.", e);
        }
    }

// -------------------------------- sendSOAPBlock --------------------------------
void
pixel::utils::SOAPER::sendSOAPParallel(std::map<xdata::UnsignedIntegerT, const xdaq::ApplicationDescriptor *>::const_iterator &s,
                                       std::string const &message, std::string const &replyNormal,
                                       boost::promise<std::string> &ps,
                                       Attributes const &parameters) {
    std::string reply,
                replyOutside = replyNormal;

    try {
        reply = send(s->second, message, parameters);
    }
    catch (xdaq::exception::Exception &e) {
        string const msg_error_hmy = "Failure while sending Start SOAP to Supervisors. Exception: " + string(e.what());
        LOGERROR(sv_logger_, msg_error_hmy);
        XCEPT_DECLARE_NESTED(xdaq::exception::Exception, f, msg_error_hmy, e);
        app_->notifyQualified("fatal", f);
        replyOutside = message + "Failed";
    }

    // Check if reply ok
    if (reply != replyNormal) {
        replyOutside = s->second->getClassName() +
                       toolbox::toString(" instance #%d execute ", s->first) +
                       message + " reply was " + reply;
        LOGERROR(sv_logger_, replyOutside);
    }

    ps.set_value(replyOutside);
}

boost::thread*
pixel::utils::SOAPER::makeSOAPThread(std::map<xdata::UnsignedIntegerT, const xdaq::ApplicationDescriptor *>::const_iterator &s,
                                 std::string const &message, std::string const &replynormal,
                                 Attributes const &parameters){
    boost::promise<std::string> *ps = new boost::promise<std::string>();
    boost::thread *this_thread = new boost::thread(&pixel::utils::SOAPER::sendSOAPParallel, this,
                                                    s, message, replynormal, boost::ref(*ps), parameters);

    string thisreply = ps->get_future().get();
    if (thisreply != replynormal)
        XCEPT_RAISE(xoap::exception::Exception, thisreply);

    return this_thread;
}

void
pixel::utils::SOAPER::sendSOAPBlock(std::map<xdata::UnsignedIntegerT, const xdaq::ApplicationDescriptor *> const &SuperV,
                                    std::string const &message, std::string const &replynormal,
                                    Attributes const &parameters) {
    boost::thread_group allTreads;

    for (std::map<xdata::UnsignedIntegerT, const xdaq::ApplicationDescriptor *>::const_iterator s = SuperV.begin(); s != SuperV.end(); ++s) {
        boost::thread *this_thread;
        try{
            this_thread = makeSOAPThread(s, message, replynormal, parameters);
        }
        catch(xoap::exception::Exception &e) {
            string const msg_err_mth = toolbox::toString("Problem in Supervisor %d.", s->first);
            LOGERROR(sv_logger_,  msg_err_mth);
            XCEPT_DECLARE_NESTED(xoap::exception::Exception, err, msg_err_mth, e);
            throw(err);
        }

        allTreads.add_thread(this_thread);
    }
    allTreads.join_all();
}
