#include "pixel/utils/LoggerHistory.h"
#include <log4cplus/spi/loggingevent.h>
#include <log4cplus/helpers/timehelper.h>
#include <log4cplus/thread/syncprims-pub-impl.h>

#include "toolbox/string.h"
#include <sstream>
#include <chrono>
#include <typeinfo>



namespace log4cplus
{

  const std::map<std::string , std::string> translate { // console to html
    {"\033[22;30m" ,"<font color=\"black\">"},
    {"\033[22;31m" ,"<font color=\"red\">"},
    {"\033[22;32m" ,"<font color=\"green\">"},
    {"\033[22;33m" ,"<font color=\"brown\">"},
    {"\033[22;34m" ,"<font color=\"blue\">"},
    {"\033[22;35m" ,"<font color=\"magenta\">"},
    {"\033[22;36m" ,"<font color=\"cyan\">"},
    {"\033[22;37m" ,"<font color=\"gray\">"},
    {"\033[01;30m" ,"<font color=\"#504A4B\">"},
    {"\033[01;31m" ,"<font color=\"#E77471\">"},
    {"\033[01;32m" ,"<font color=\"#52D017\">"},
    {"\033[01;33m" ,"<font color=\"#838300\">"},
    {"\033[01;34m" ,"<font color=\"#736AFF\">"},
    {"\033[01;35m" ,"<font color=\"#F433FF\">"},
    {"\033[01;36m" ,"<font color=\"#0066ff\">"},
    {"\033[01;37m" ,"<font color=\"white\">"},
    {"\033[0m"     ,"</font>"}
  };

  const std::map<std::string, std::string> msg_types {
      {"INFO", "<font color=\"blue\">INFO</font>"},
      {"WARN", "<font color=\"#FF9933\">WARN</font>"},
      {"ERROR","<font color=\"#DC1D18\">ERROR</font>"},
      {"FATAL","<font color=\"#CC0000\">FATAL</font>"},
      {"DEBUG","<font color=\"#FFA500\">DEBUG</font>"}
  };


///////////////////////////////////////////////////////////////////////////////
// LoggerHistory:: ctors and dtor
///////////////////////////////////////////////////////////////////////////////

LoggerHistory::LoggerHistory():llmCache(getLogLevelManager()), handler(nullptr), monitor(nullptr)
{
}

LoggerHistory::LoggerHistory(size_t const history_size):llmCache(getLogLevelManager()), handler(nullptr), monitor(nullptr){
  if (history_size >= history_.max_size()) {
      history_size_ = history_.max_size()-1;
  }
  else
      history_size_ = history_size;

}

LoggerHistory::LoggerHistory(const helpers::Properties& properties)
: Appender(properties), llmCache(getLogLevelManager()), handler(nullptr), monitor(nullptr)
{
}

LoggerHistory::LoggerHistory(LoggerHistory* aLogger): llmCache(getLogLevelManager()), history_size_(aLogger->history_size_), handler(nullptr), monitor(nullptr){
  history_ = aLogger->history_;
}



LoggerHistory::~LoggerHistory()
{
    destructorImpl();
}



///////////////////////////////////////////////////////////////////////////////
// LoggerHistory:: public methods
///////////////////////////////////////////////////////////////////////////////

void
LoggerHistory::close()
{
}

void LoggerHistory::registerInfoSpaceHandler(pixel::utils::LOGMsgHistoryHandler* _handler,pixel::utils::Monitor* _monitor){
  handler = std::unique_ptr<pixel::utils::LOGMsgHistoryHandler> (_handler);
  monitor = std::unique_ptr<pixel::utils::Monitor> (_monitor);

}

///////////////////////////////////////////////////////////////////////////////
// LoggerHistory:: protected methods
///////////////////////////////////////////////////////////////////////////////

// This method does not need to be locked since it is called by
// doAppend() which performs the locking
void
LoggerHistory::append(const spi::InternalLoggingEvent& event)
{
  if (history_size_==0) return;

  std::string timestamp="";
  char buf[30];
  time_t t_c= std::chrono::system_clock::to_time_t(event.getTimestamp());
  tm * log_time= localtime(&t_c);
  strftime(buf, sizeof(buf), "%d %b %Y %T", log_time);
  timestamp = toolbox::toString("%s.%03d", buf, log4cplus::helpers::microseconds_part(event.getTimestamp())/1000);
  //std::cerr<<"Test append "<<timestamp<<"  "<<llmCache.toString(event.getLogLevel()) <<" "<< event.getMessage()<<std::endl;
  //std::cerr<<"Test append "<<timestamp<<"   "+ event.getMessage()<<std::endl;
  if (history_.size() >= history_size_){
    ////LOGWARN(sv_logger_, "The message history is full. The first message will be removed.");
    history_.pop_front();
  }
  history_.push_back(std::make_pair(timestamp,  llmCache.toString(event.getLogLevel()) +" "+ event.getMessage()));

  if(handler.get()!=NULL && monitor.get()!=NULL){
    handler->displayLOGMessage( *monitor );
  }
}


// -------------------------------- get_history --------------------------------
std::vector<std::string> LoggerHistory::get_history(){

  std::vector<std::string> retVector;
  for(std::deque<std::pair<std::string, std::string> >::iterator it =history_.begin(); it!=history_.end(); it++){
    retVector.push_back(it->first +": "+it->second);
  }
  return retVector;
}

// -------------------------------- getTimestamp --------------------------------
std::string
LoggerHistory::getTimestamp(size_t const i){
    return (i<history_.size() ? history_.at(i).first : history_.back().first);
}

std::string
LoggerHistory::getType(size_t const i){
  std::pair<std::string, std::string> msg = (i<history_.size() ? history_.at(i) : history_.back());

  return msg_types.at(msg.second.substr(0, msg.second.find(" ")));
}

std::string
LoggerHistory::getlog(size_t const i){
  std::pair<std::string, std::string> msg = (i<history_.size() ? history_.at(i) : history_.back());

  std::string body = msg.second.substr(msg.second.find(" "));

  //std::cout<<"before "<<body<<std::endl;

  size_t ngreater = std::string::npos;
  for(auto& itranslate : translate){
    ngreater = body.find(itranslate.first);
    while (ngreater!=std::string::npos) {
      body.replace(ngreater,itranslate.first.size(),itranslate.second);

      ngreater = body.find(itranslate.first, ngreater+itranslate.second.size());
    }
  }
  return body;

}

}// namespace log4cpp


