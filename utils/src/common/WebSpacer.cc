#include "pixel/utils/WebSpacer.h"

#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"

pixel::utils::WebSpacer::WebSpacer(std::string const &name,
                                   std::string const &description,
                                   Monitor const &monitor,
                                   std::string const &itemSetName,
                                   std::string const &tabName,
                                   size_t const colSpan)
    : WebObject(name, description, monitor, itemSetName, tabName, colSpan) {
}

std::string
pixel::utils::WebSpacer::getHTMLString() const {
    std::stringstream res;

    res << "<div class=\"pixel-item-table-wrapper\">";
    res << "</div>";
    res << "\n";

    return res.str();
}

std::string
pixel::utils::WebSpacer::getJSONString() const {
    return "";
}
