#include <iomanip>
#include <iostream>
#include <string>
#include "PixelUtilities/PixeluTCAUtilities/include/PixelAMC13Interface.h"

//#define AMC13_TRIGGERS

// needs T1 0x6057 and T2 0x2e
// JMTBAD check for the versions in configure or something

namespace {
template <typename T>
void throw_simple(T t, std::string extra) {
    t.Append(extra);
    throw t;
}
}

PixelAMC13Interface::PixelAMC13Interface(const std::string &uriT1,
                                         const std::string &addressT1,
                                         const std::string &uriT2,
                                         const std::string &addressT2)
    : fAMC13(new amc13::AMC13(uriT1, addressT1, uriT2, addressT2)),
      fT1(fAMC13->getT1()), // we don't own these
      fT2(fAMC13->getT2()),
      fBGONodeFireAddr(fT1->getNode("ACTION.TTC.SINGLE_COMMAND").getAddress()),
      fBGONodeFireMask(fT1->getNode("ACTION.TTC.SINGLE_COMMAND").getMask()),
      fCrate(0),
      fglobalRun(true),
      fMask(0),
      fSimTTC(false),
      fDebugPrints(false),
      fCalBX(381),
      fL1ADelay(123),
      fNewWay(true),
      fVerifyL1A(false),
      fWatchTTCHistory(false),
      fLastCountCalSync(0),
      fLastTTCHistoryEvent(0),
      countLevelOne(0),
      countCalSync(0),
      countResetTBM(0),
      countResetROC(0),
      perBurstForMeasurement(-1) {
    fBGONodeEnable[0] = &fT1->getNode("CONF.TTC.BGO0.ENABLE_SINGLE");
    fBGONodeEnable[1] = &fT1->getNode("CONF.TTC.BGO1.ENABLE_SINGLE");
    fBGONodeEnable[2] = &fT1->getNode("CONF.TTC.BGO2.ENABLE_SINGLE");
    fBGONodeEnable[3] = &fT1->getNode("CONF.TTC.BGO3.ENABLE_SINGLE");

    {
        const char *fname = "/home/tucker/secret";
        if (access(fname, F_OK) != -1) {
            FILE *f = fopen(fname, "rb");
            assert(f);
            fread(&perBurstForMeasurement, 4, 1, f);
            fclose(f);
            std::cout << "\n\n\nperBurstForMeasurement is " << perBurstForMeasurement << "\n\n\n";
        }
    }
}

PixelAMC13Interface::~PixelAMC13Interface() {
    delete fAMC13;
}

void PixelAMC13Interface::DoResets(bool resetAll = true) {
    if (fDebugPrints)
        std::cout << "Resetting T1, T2, counters, DAQ" << std::endl;
    fAMC13->reset(amc13::AMC13Simple::T1);
    fAMC13->reset(amc13::AMC13Simple::T2);
    fAMC13->resetCounters();
    fAMC13->resetDAQ();

    ClearTTCHistoryFilter();
    fAMC13->setTTCHistoryFilter(0, 0x10101); // filter BC0
    fAMC13->setTTCFilterEna(true);

    ClearTTCHistory();
    fAMC13->setTTCHistoryEna(true);

    countLevelOne = 0;
    countCalSync = 0;
    countResetROC = 0;
    countResetTBM = 0;
    if (!fglobalRun && resetAll) {
        for (int i = 0; i < 4; ++i)
            fAMC13->configureBGOShort(i, 0, 0, 0, false);
    }
}

void PixelAMC13Interface::reconfigureFPGAs() {
    if (fDebugPrints)
        std::cout << "Reconfiguring FPGAs" << std::endl;
    fAMC13->getFlash()->loadFlashT1();
}

void PixelAMC13Interface::Configure() {
    if (fDebugPrints)
        std::cout << "AMC13 Interface Configuring" << std::endl;
    if (!fNewWay) {
        std::cout << "only NewWay is supported in PixelAMC13Interface now\n";
        assert(0);
    }

    DoResets(); // maybe redundant with just before amc13->startRun
#ifdef AMC13_TRIGGERS
    fAMC13->localTtcSignalEnable(fSimTTC);
    fAMC13->fakeDataEnable(true);
#endif
#ifndef AMC13_TRIGGERS
    if (!fglobalRun) {
        fAMC13->localTtcSignalEnable(fSimTTC);
        fAMC13->fakeDataEnable(true);
    } else {
        fAMC13->localTtcSignalEnable(false);
        fAMC13->fakeDataEnable(false);
    }
#endif
    fAMC13->write(amc13::AMC13Simple::T1, 0x3, 0x80000000);
    fAMC13->AMCInputEnable(fMask);
    //react with EC0 on a resync
    fAMC13->write(amc13::AMC13Simple::T1, "CONF.TTC.RESYNC.COMMAND", 0x02);
    fAMC13->write(amc13::AMC13Simple::T1, "CONF.TTC.RESYNC.MASK", 0x1);
    //react with a reset on a resync
    //fAMC13->write(amc13::AMC13Simple::T1, "CONF.TTC.RESYNC.COMMAND", 0x14);
    //fAMC13->write(amc13::AMC13Simple::T1, "CONF.TTC.RESYNC.MASK", 0x3);

    if (fCrate >= 1 && fCrate <= 8) {
        if (fDebugPrints)
            std::cout << "BPIX Crate " << fCrate << std::endl;
        uint32_t fedMask = 0;
        for (int i = 0; i < 10; ++i) {
            if (!(fMask & (1 << i))) {
                if (fDebugPrints)
                    std::cout << "Slot " << i + 1 << " masked" << std::endl;
                fedMask = fedMask | (1 << i);
            }
        }
        fAMC13->write(amc13::AMC13Simple::T2, "CONF.TTC.OVERRIDE_MASK", 0x400 | fedMask);
    } else if (fCrate >= 9 && fCrate <= 12) {
        if (fDebugPrints)
            std::cout << "FPIX Crate " << fCrate << std::endl;
        uint32_t fedMask = 0;
        for (int i = 0; i < 7; ++i) {
            if (!(fMask & (1 << i))) {
                if (fDebugPrints)
                    std::cout << "Slot " << i + 1 << " masked" << std::endl;
                fedMask = fedMask | (1 << i);
            }
        }
        fAMC13->write(amc13::AMC13Simple::T2, "CONF.TTC.OVERRIDE_MASK", 0x300 | fedMask);
    } else {
        std::cout << "Error: invalid crate number" << std::endl;
        assert(0);
    }

    if (fDebugPrints) {
        std::cout << "Enabled TTC links for these AMCs: ";
        for (int i = 0; i < 12; ++i)
            if (fMask & (1 << i))
                std::cout << i + 1 << " ";
        std::cout << std::endl;
    }

    if (!fglobalRun) {
        ConfigureBGO(0, BGO(0x2c, false, false, 0, fCalBX)); // CAL
        fAMC13->write(amc13::AMC13Simple::T1, 0x24, 0x80000000 | ((fCalBX & 0xFFF) << 16));
        fAMC13->write(amc13::AMC13Simple::T1, 0x2e, fL1ADelay);
        ConfigureBGO(1, BGO(0x14, false, false, 0, 100)); // RESET TBM
        ConfigureBGO(2, BGO(0x1c, false, false, 0, 100)); // RESET ROC
        fAMC13->configureLocalL1A(true, 0, 1, 1, 0);      // trigger burst 1 after 1 orbit =
        fAMC13->sendLocalEvnOrnReset(true, true);
    }

    DoResets(false); // after register programming need a general reset (dixit Mr. Wu)

    if (fglobalRun) {
        fAMC13->startRun();
    }
}

void PixelAMC13Interface::Halt() {
    if (fDebugPrints)
        std::cout << "Halt";
    DoResets();
}

void PixelAMC13Interface::Reset() {
    if (fDebugPrints)
        std::cout << "Reset" << std::endl;
    DoResets();
}

void PixelAMC13Interface::VerifyL1ASetup() {
    previousL1ACount = 0;
    nL1ARetries = 0;
    if (fVerifyL1A)
        previousL1ACount = GetL1ACount();
}

uint64_t PixelAMC13Interface::VerifyL1ACheck(uint64_t Ntrig) {
    if (!fVerifyL1A)
        return true;

    const uint64_t L1ACount = GetL1ACount();
    uint64_t delta = L1ACount - previousL1ACount;
    if (fDebugPrints)
        std::cout << "before, L1A count was " << previousL1ACount << "; after it is " << L1ACount << std::endl;
    if (delta > Ntrig)
        std::cout << "\033[1m\033[31mmore than " << delta << " L1A? before " << previousL1ACount << " after " << L1ACount << std::endl;
    if (delta != Ntrig) {
        std::cout << "\033[1m\033[31mL1As DIDN'T GO THROUGH, # retries = " << nL1ARetries++ << "\033[0m" << std::endl;
        if (nL1ARetries == 10000) {
            std::cout << "\033[1m\033[31mGIVING UP after 10000 retries\033[0m" << std::endl;
            delta = Ntrig;
        }
    }

    return delta;
}

void PixelAMC13Interface::StartContinuesL1A(int rate) {
    fAMC13->localTtcSignalEnable(fSimTTC);
    fAMC13->fakeDataEnable(true);

    //http://bucms.bu.edu/twiki/bin/view/BUCMSPublic/AMC13UserManual
    //AMC13::configureLocalL1A( bool ena, int mode, uint32_t burst, uint32_t rate, int rules) documentation:
    //Parameter 	Description
    //ena 	true to enable the L1A generator
    //mode 	0 - periodic triggers spaced by rate orbits at BX=500
    //1 - periodic triggers spaced by rate bx
    //2 - random trigger at 2* rate Hz
    // COMMENT there is no factor 2!!!!
    //burst 	number of triggers in a burst (1-4096)
    //rate 	sets the rate based on mode (1-65536)
    //rules 	set to 0 normally to enforce the "standard" CMS trigger rules
    fAMC13->configureLocalL1A(true, 2, 1, rate, 0);
    fAMC13->startContinuousL1A();
    return;
}

void PixelAMC13Interface::StopContinuesL1A() {
    fAMC13->stopContinuousL1A();
    return;
}

void PixelAMC13Interface::CalSync(int n) {
    calSyncTimer.start();
    if (fDebugPrints)
        std::cout << "CalSync" << std::endl;

    const uint64_t Ntrig = n;
    uint64_t Ntrigleft = Ntrig;

    VerifyL1ASetup();
    while (1) {
        for (uint64_t i = 0; i < Ntrigleft; ++i) {
            FireBGO(0);
            ++countCalSync;
        }

        const uint64_t delta = VerifyL1ACheck(Ntrig);
        if (delta >= Ntrig)
            break;
        else
            Ntrigleft = Ntrig - delta;
    }

    if (perBurstForMeasurement > 0 && countCalSync % perBurstForMeasurement == 0)
        std::cout << "after perBurstForMeasurement=" << perBurstForMeasurement << " rate " << GetL1ARate() << "\n";

    if (fWatchTTCHistory && countCalSync % 256 == 0) { // it's 512 deep
        std::vector<uint32_t> h = GetTTCHistory();
        uint32_t event = 0;
        size_t n = h.size();
        assert(n % 4 == 0);

        for (size_t i = 0; i < n / 4; ++i) {
            event = h.at(i * 4 + 3);

            if (event > fLastTTCHistoryEvent) {
                const uint32_t command = h.at(i * 4);
                const uint32_t orbit = h.at(i * 4 + 1);
                const uint32_t bx = h.at(i * 4 + 2);

                fTTCHistoryByCommand[command].fill(orbit, bx);
            }
        }

        if (countCalSync && countCalSync % 1024 == 0) {
            std::cout << "After " << countCalSync << " calsyncs, TTC history has seen:\n";
            for (TTCHistoryByCommandMap::const_iterator it = fTTCHistoryByCommand.begin(), ite = fTTCHistoryByCommand.end(); it != ite; ++it) {
                const OrbitBXHisto &h = it->second;
                std::cout << std::hex << "0x" << it->first << std::dec << " : " << h.size() << "  bxs:";
                for (std::map<uint32_t, int>::const_iterator jt = h.bx.begin(), jte = h.bx.end(); jt != jte; ++jt)
                    std::cout << " " << jt->first << ":" << jt->second;
                //std::cout << "\n  orbits:";
                //for (std::map<uint32_t, int>::const_iterator jt = h.orbit.begin(), jte = h.orbit.end(); jt != jte; ++jt)
                //  std::cout << " " << jt->first << ":" << jt->second;
                std::cout << std::endl;
            }
        }
        fLastCountCalSync = countCalSync;
        fLastTTCHistoryEvent = event;
    }
    calSyncTimer.stop();
    if (countCalSync && countCalSync % 1024 == 0) {
        std::cout << "Amount of time spent in the CalSync method after " << countCalSync << " CalSyncs: " << calSyncTimer.tottime() << std::endl;
    }
}

void PixelAMC13Interface::LevelOne(int n) {
    ++countLevelOne;
    if (fDebugPrints)
        std::cout << "LevelOne" << countLevelOne << std::endl;

    const uint64_t Ntrig = n;
    uint64_t Ntrigleft = Ntrig;

    VerifyL1ASetup();
    while (1) {
        for (uint64_t i = 0; i < Ntrigleft; ++i)
            fAMC13->sendL1ABurst();

        const uint64_t delta = VerifyL1ACheck(Ntrig);
        if (delta >= Ntrig)
            break;
        else
            Ntrigleft = Ntrig - delta;
    }
}

void PixelAMC13Interface::ResetTBM() {
    ++countResetTBM;
    if (fDebugPrints)
        std::cout << "ResetTBM" << std::endl;
    FireBGO(1);
}

void PixelAMC13Interface::ResetROC() {
    ++countResetROC;
    if (fDebugPrints)
        std::cout << "ResetROC" << std::endl;
    FireBGO(2);
}

void PixelAMC13Interface::ResetCounters() {
    if (fDebugPrints)
        std::cout << "ResetCounters" << std::endl;
    fAMC13->resetCounters();
    countCalSync = 0;
    countLevelOne = 0;
    countResetTBM = 0;
    countResetROC = 0;
}

uint32_t PixelAMC13Interface::GetClockFreq() {
    uint32_t v = fAMC13->read(amc13::AMC13Simple::T2, "STATUS.TTC.CLK_FREQ") * 50;
    if (fDebugPrints)
        std::cout << "ClockFreq " << v << std::endl;
    return v;
}

uint64_t PixelAMC13Interface::GetL1ACount() {
    uint64_t v = (uint64_t(fAMC13->read(amc13::AMC13Simple::T1, "STATUS.GENERAL.L1A_COUNT_HI")) << 32) | fAMC13->read(amc13::AMC13Simple::T1, "STATUS.GENERAL.L1A_COUNT_LO");
    if (fDebugPrints)
        std::cout << "L1ACount " << v << std::endl;
    return v;
}

uint32_t PixelAMC13Interface::GetL1ARate() {
    uint32_t v = fAMC13->read(amc13::AMC13Simple::T1, "STATUS.GENERAL.L1A_RATE_HZ");
    if (fDebugPrints)
        std::cout << "L1ARate " << v << std::endl;
    return v;
}

uint32_t PixelAMC13Interface::GetT1Version() {
    uint32_t v = fAMC13->read(amc13::AMC13Simple::T1, "STATUS.FIRMWARE_VERS");
    if (fDebugPrints)
        std::cout << "T1Version " << v << std::endl;
    return v;
}

uint32_t PixelAMC13Interface::GetT2Version() {
    uint32_t v = fAMC13->read(amc13::AMC13Simple::T2, "STATUS.FIRMWARE_VERS");
    if (fDebugPrints)
        std::cout << "T2Version " << v << std::endl;
    return v;
}

uint32_t PixelAMC13Interface::GetSlotMask() {
    uint32_t v = fAMC13->read(amc13::AMC13Simple::T1, "CONF.AMC.ENABLE_MASK");
    if (fDebugPrints)
        std::cout << "SlotMask " << v << std::endl;
    return v;
}

uint32_t PixelAMC13Interface::GetTTCOverrideMask() {
    uint32_t v = fAMC13->read(amc13::AMC13Simple::T2, "CONF.TTC.OVERRIDE_MASK");
    if (fDebugPrints)
        std::cout << "TTC Override Mask " << v << std::endl;
    return v;
}

bool PixelAMC13Interface::GetTTCSimulator() {
    uint32_t v = fAMC13->read(amc13::AMC13Simple::T1, "CONF.DIAG.FAKE_TTC_ENABLE");
    if (fDebugPrints)
        std::cout << "TTCSimulator " << v << std::endl;
    assert(v == 0 || v == 1);
    return bool(v);
}

uint64_t PixelAMC13Interface::GetLevelOneCount() {
    uint64_t v = countLevelOne;
    if (fDebugPrints)
        std::cout << "LevelOneCount " << v << std::endl;
    return v;
}

uint64_t PixelAMC13Interface::GetCalSyncCount() {
    uint64_t v = countCalSync;
    if (fDebugPrints)
        std::cout << "CalSyncCount " << v << std::endl;
    return v;
}

uint64_t PixelAMC13Interface::GetResetROCCount() {
    uint64_t v = countResetROC;
    if (fDebugPrints)
        std::cout << "ResetROCCount " << v << std::endl;
    return v;
}

uint64_t PixelAMC13Interface::GetResetTBMCount() {
    uint64_t v = countResetTBM;
    if (fDebugPrints)
        std::cout << "ResetTBMCount " << v << std::endl;
    return v;
}

void PixelAMC13Interface::ClearTTCHistory() {
    fAMC13->clearTTCHistory();
}

void PixelAMC13Interface::ClearTTCHistoryFilter() {
    fAMC13->clearTTCHistoryFilter();
}

uint32_t PixelAMC13Interface::getTTCHistoryItemAddress(int item) {
    if (item > -1 || item < -512)
        throw_simple(amc13::Exception::UnexpectedRange(), "TTC history item offset out of range");

    const uint32_t base = fAMC13->getT2()->getNode("STATUS.TTC_HISTORY.BUFFER.BASE").getAddress();
    const uint32_t wp = fAMC13->read(amc13::AMC13Simple::T2, "STATUS.TTC_HISTORY.COUNT");
    uint32_t a = base + 4 * (wp + item);
    if (a < base)
        a += 0x800;
    return a;
}

std::vector<uint32_t> PixelAMC13Interface::GetTTCHistory() {
    std::vector<uint32_t> cVec;
    const uint32_t base = fAMC13->getT2()->getNode("STATUS.TTC_HISTORY.BUFFER.BASE").getAddress();
    const int nhist = fAMC13->getTTCHistoryCount();
    if (nhist > 0) {
        uint32_t adr = getTTCHistoryItemAddress(-nhist);
        for (int i = 0; i < nhist; i++) {
            for (int k = 0; k < 4; k++)
                cVec.push_back(fAMC13->read(amc13::AMC13Simple::T2, adr + k));
            adr = base + ((adr + 4) % 0x800);
        }
    }
    return cVec;
}

void PixelAMC13Interface::DumpTTCHistory(const std::vector<uint32_t> &cVec) {
    std::cout << "TTC History:\n"
              << "Index  Cmd         Orbit    BX     Event" << std::endl;
    for (size_t index = 0; index < cVec.size() / 4; ++index) {
        const uint32_t command = cVec.at(index * 4 + 0);
        const uint32_t orbit = cVec.at(index * 4 + 1);
        const uint32_t bx = cVec.at(index * 4 + 2);
        const uint32_t event = cVec.at(index * 4 + 3);

        if (command || orbit || bx || event)
            std::cout << std::setfill(' ')
                      << std::setw(5) << index
                      << std::setw(5) << std::hex << command << std::dec
                      << std::setw(14) << orbit
                      << std::setw(6) << bx
                      << std::setw(10) << event << std::endl;
    }
}

void PixelAMC13Interface::DumpTriggers() {
    std::vector<uint32_t> cVec(512, 0);
    for (int i = 0; i < 512; ++i)
        cVec[i] = fAMC13->read(amc13::AMC13Simple::T1, 0x200 + i);

    //now decode the Info in here!
    std::cout << "L1A History:\n"
              << "Index         Orbit    BX     Event     Flags" << std::endl;
    for (size_t index = 0; index < cVec.size() / 4; ++index) {
        uint32_t orbit = cVec.at(index * 4 + 0);
        uint32_t bx = cVec.at(index * 4 + 1);
        uint32_t event = cVec.at(index * 4 + 2);
        uint32_t flags = cVec.at(index * 4 + 3);

        if (orbit || bx || event || flags)
            std::cout << std::setfill(' ')
                      << std::setw(5) << index
                      << std::setw(14) << orbit
                      << std::setw(6) << bx
                      << std::setw(10) << event
                      << std::setw(10) << std::hex << flags << std::dec << std::endl;
    }
}

void PixelAMC13Interface::ConfigureBGO(unsigned i, BGO bgo) {
    if (i > 3)
        throw_simple(amc13::Exception::UnexpectedRange(), "AMC13::ConfigureBGO() - channel must be in range 0 to 3");

    if (bgo.fBX > 3563)
        throw_simple(amc13::Exception::UnexpectedRange(), "AMC13::ConfigureBGO() - bx must be in range 0 to 3563");

    static const std::string cmds[4] = {
        "CONF.TTC.BGO0.",
        "CONF.TTC.BGO1.",
        "CONF.TTC.BGO2.",
        "CONF.TTC.BGO3."
    };

    const std::string &bgo_base = cmds[i];

    fAMC13->write(amc13::AMC13Simple::T1, bgo_base + "COMMAND", bgo.fCommand);
    fAMC13->write(amc13::AMC13Simple::T1, bgo_base + "LONG_CMD", bgo.isLong);
    fAMC13->write(amc13::AMC13Simple::T1, bgo_base + "ENABLE", bgo.fEnable);
    fAMC13->write(amc13::AMC13Simple::T1, bgo_base + "ENABLE_SINGLE", bgo.fEnableSingle);
    fAMC13->write(amc13::AMC13Simple::T1, bgo_base + "BX", bgo.fBX);
    fAMC13->write(amc13::AMC13Simple::T1, bgo_base + "ORBIT_PRESCALE", bgo.fPrescale);
}

void PixelAMC13Interface::FireBGO(unsigned which) {
    if (which > 3)
        throw_simple(amc13::Exception::UnexpectedRange(), "AMC13::FireBGO() - channel must be in range 0 to 3");

    // each amc13 library call has a dispatch in it. batch the writes
    // before doing dispatch -- much cheaper!

    // fAMC13->write(amc13::AMC13Simple::T1, cmds[i], 1);
    // fAMC13->sendBGO(); // writeMask(amc13::AMC13Simple::T1, "ACTION.TTC.SINGLE_COMMAND");
    // fAMC13->write(amc13::AMC13Simple::T1, cmds[i], 0);
    fireBGOTimer.start();
    fBGONodeEnable[which]->write(1);
    fT1->getClient().write(fBGONodeFireAddr, fBGONodeFireMask);
    fBGONodeEnable[which]->write(0);
    fT1->dispatch();
    fireBGOTimer.stop();
}
