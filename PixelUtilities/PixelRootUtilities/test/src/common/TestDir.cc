#include "PixelRootUtilities/PixelHisto/scan_directory.h"
#include "PixelUtilities/PixelXmlUtilities/include/PixelXmlReader.h"
#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>

using namespace std;

int main() {
    string mthn = "[main()]\t";
    const string projectHome =
        string(getenv("BUILD_HOME")) + "/pixel/PixelHistoViewer/";
    const string cfg = projectHome + "Configuration/SearchPaths.xml";
    PixelHistoScanDirectory sD(cfg);
    //	vector<string> v;
    //	sD.ls(v,dirsToScan,"root");
    vector<string> v = sD.ls();
    for (vector<string>::iterator it = v.begin(); it != v.end(); it++) {
        cout << "[main()]\t" << *it << endl;
    }
    return 0;
}
