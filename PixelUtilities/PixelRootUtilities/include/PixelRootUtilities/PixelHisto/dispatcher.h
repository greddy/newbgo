#ifndef PixelRootUtilities__PixelHisto__dispatcher_h
#define PixelRootUtilities__PixelHisto__dispatcher_h

#include "PixelUtilities/PixelRootUtilities/include/PixelHistoThreadFrame.h"
#include "PixelRootUtilities/PixelHisto/ProducerDescriptor.h"
#include "TList.h"
#include <map>
#include <string>

///////////////////////
class TThread;
class TServerSocket;
class TMonitor;
class TSocket;
class TObject;
// class TMessage;
class PixelHistoReadFileElements;
///////////////////////

namespace PixelHisto {
class dispatcher : public PixelHistoThreadFrame {
  public:
    dispatcher();
    virtual ~dispatcher();

    std::string xml_cfg;

    void init(void);
    void destroy(void);
    int startThreads(void);
    int stopThreads(void);

    void displayProdMap(void);
    TList *uGetCompleteFileList(bool forceRefresh = true,
                                std::string dirName = "");
    TList *uGetFileContent(TString file, bool forceRefresh = true);
    TObject *uGetHistogram(TString histoName);
    std::string getClassName() {
        return className_;
    }

  private:
    enum {
        U_COMPLETE_LIST,
        U_FILE_CONTENT,
        U_HISTOGRAM
    };
    // private member functions
    TObject *getHistogram(TString histoName);
    TList *getCompleteFileList(bool forceRefresh = false);
    TList *getProducerList();
    TList *getFileList(TString prodName, bool forceRefresh = true,
                       bool initiate = true);
    TList *getFileContent(TString fileName, bool forceRefresh = true,
                          bool initiate = true);

    void userFunc0(void);
    void HandleSocket(TSocket *s);

    void closeSocket(TSocket *s);
    void consumerRefresh(TSocket *s);
    void addProducerToMap(TSocket *s);
    TString createProducerName(TSocket *s);

    TList *recvFileList(TSocket *s);
    TList *recvFileContent(TSocket *s, TString file);

    // private member variables
    TServerSocket *serverSocket_;
    TMonitor *socketMonitor_;

    std::map<TString, ProducerDescriptor *> *prodMap_;

    int uCommandReq_;
    bool uCommandDone_;
    bool uForceRefreshParam_;
    std::string uDirName_;
    TString uFileParam_;
    TList *uReturnList_;
    TObject *uReturnMessage_;
    std::string className_;
    PixelHistoReadFileElements *rFE_;
};
}

#endif
