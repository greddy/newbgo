#ifndef PixelRootUtilities__PixelHisto__ProducerDescriptor_h
#define PixelRootUtilities__PixelHisto__ProducerDescriptor_h

#include <map>
#include "TString.h"
#include "TList.h"
#include "TSocket.h"

namespace PixelHisto {
class ProducerDescriptor {
    TString name;
    TSocket *sock;

    TList *fileList;
    std::map<TString, TList *> *contentMap;

  public:
    ProducerDescriptor();
    ProducerDescriptor(TString s, TList *l, TSocket *sk);
    ~ProducerDescriptor();

    TSocket *getSocket() {
        return sock;
    }
    TString getName() {
        return name;
    }
    TList *getFileList() {
        return fileList;
    }
    std::map<TString, TList *> *getContentMap() {
        return contentMap;
    }

    TList *getContentList(TString file);

    void setFileList(TList *fl);
    void setContentList(TString file, TList *cl);
};
}

#endif
