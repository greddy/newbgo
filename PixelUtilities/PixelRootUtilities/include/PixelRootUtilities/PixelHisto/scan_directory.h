/*
 * Provides basic file listing for some PixelHisto classes.
 *
 * NOTE: dirent.h should be removed in favor of
 * C++17 std::filesystem::directory_iterator
 * see: http://en.cppreference.com/w/cpp/filesystem/directory_iterator
 */
#ifndef PixelRootUtilities__PixelHisto__scan_directory_h
#define PixelRootUtilities__PixelHisto__scan_directory_h

#include <vector>
#include <string>
#include <dirent.h>

namespace PixelHisto {
class scan_directory {
  public:
    scan_directory(const std::string &xml_cfg);
    ~scan_directory();

    std::vector<std::string> ls(const std::string &dir,
                                const std::string &filter = "*");
    void ls(std::vector<std::string> &list, const std::string &dir,
            const std::string &filter = "*");
    void ls(std::vector<std::string> &list, const std::string &dir,
            const bool recursive, const std::string &filter = "*");
    void ls(std::vector<std::string> &list,
            std::vector<std::pair<std::string, bool> > &dirs,
            const std::string &filter = "*");
    std::vector<std::string> ls(const bool overrideRecursive = false,
                                const bool recursive = true);

  private:
    // ignore files and dirs; for legacy C++, thus not arrays
    const std::string ign_fn_starts_0;
    const std::string ign_fn_is_0;
    const std::string ign_fn_is_1;

    std::vector<std::pair<std::string, bool> > dirs_to_scan_;

    inline void update_file_list_(bool recursive, const std::string &dir,
                                  const std::string &filter, DIR *&pDir,
                                  std::vector<std::string> &list);
};
}
#endif
