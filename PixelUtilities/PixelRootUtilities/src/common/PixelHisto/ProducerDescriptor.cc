#ifndef PixelRootUtilities__PixelHisto__ProducerDescriptor_cc
#define PixelRootUtilities__PixelHisto__ProducerDescriptor_cc

#include "PixelRootUtilities/PixelHisto/ProducerDescriptor.h"

namespace PixelHisto {
ProducerDescriptor::ProducerDescriptor() {
    name = "unnamed";
    fileList = 0;
    sock = 0;
    contentMap = new std::map<TString, TList *>();
}

ProducerDescriptor::ProducerDescriptor(TString s, TList *l, TSocket *sk) {
    name = s;
    fileList = l;
    sock = sk;
    contentMap = new std::map<TString, TList *>();
}

ProducerDescriptor::~ProducerDescriptor() {
    // dont delete socket... other structures will
    if (fileList) {
        delete fileList;
        fileList = 0;
    }
    if (contentMap) {
        delete contentMap;
        contentMap = 0;
    }
}

TList *ProducerDescriptor::getContentList(TString file) {
    if (!contentMap) {
        return 0;
    }
    std::map<TString, TList *>::iterator contentIt = contentMap->find(file);
    if (contentIt == contentMap->end()) {
        return 0;
    }
    return contentIt->second;
}

void ProducerDescriptor::setFileList(TList *fl) {
    if (fileList) {
        delete fileList;
        fileList = 0;
    }
    fileList = fl;
}

void ProducerDescriptor::setContentList(TString file, TList *cl) {
    contentMap->erase(file); // erase if already present
    (*contentMap)[file] = cl;
}
}

#endif
