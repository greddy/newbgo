#ifndef PixelRootUtilities__PixelHisto__scan_directory_cc
#define PixelRootUtilities__PixelHisto__scan_directory_cc

#include "PixelRootUtilities/PixelHisto/scan_directory.h"
#include "PixelUtilities/PixelXmlUtilities/include/PixelXmlReader.h"

#include <iostream>
// #include <dirent.h>
#include <errno.h>
#include <algorithm>

using namespace std;
using namespace PixelHisto;

#ifndef nullptr
#define nullptr NULL
#endif

////////////////////////////////////////////////////////////////////
scan_directory::scan_directory(const string &xml_cfg)
    : ign_fn_starts_0("."), ign_fn_is_0("lost+found"), ign_fn_is_1("CVS") {
#ifdef DEBUG
    const string mthn = "[scan_directory::scan_directory()]";
    cout << mthn << endl;
#endif
    // Process xml config
    PixelXmlReader xml_reader;
    xml_reader.readDocument(xml_cfg);
    const unsigned int ndir = xml_reader.getNumberOfBranches("Path");
    dirs_to_scan_.reserve(ndir);
    for (unsigned int i = 0; i < ndir; ++i) {
        const string subDirAttribute =
            xml_reader.getXMLAttribute("Path", "ScanSubdirectories", i);
        bool scanSubDir = true;
        if (subDirAttribute == "No") {
            scanSubDir = false;
        }
        string dir = xml_reader.getXMLAttribute("Path", "Directory", i);
        if ('/' != dir[dir.length() - 1]) {
            dir += "/";
        }
        xml_reader.convertEnvVariables(dir);
        dirs_to_scan_.push_back(make_pair(dir, scanSubDir));
    }
#ifdef DEBUG
    cout << mthn << " Directories to scan:\n";
    vector<pair<string, bool> >::iterator it = dirs_to_scan_.begin();
    for (; it != dirs_to_scan_.end(); ++it)
        cout << (*it).first << " " << (*it).second << endl;
#endif
}

////////////////////////////////////////////////////////////////////
scan_directory::~scan_directory() {}

inline bool open_dir_(const string &dir, DIR *&dr) {
    const string mthn = "[scan_directory::open_dir_()]";

    dr = opendir(dir.c_str());
    if (dr == nullptr) {
        cout << mthn << " The directory " << dir
             << " doesn't exist or you don't have the permission to read it!"
             << endl;
        return false;
    }
    return true;
}

inline bool is_dir(const string &dir) {
    DIR *d = nullptr;
    const int errno_ = errno;
    d = opendir(dir.c_str());
    if (d == nullptr) {
        errno = errno_;
        return false;
    }

    closedir(d);
    errno = errno_;
    return true;
}

inline void update_file_list0_(const string &dir, const string &filter,
                               DIR *&dr, vector<string> &list) {
    errno = 0;
    struct dirent *entries;
    while ((entries = readdir(dr))) {
        const string name_ = entries->d_name;
#ifdef DEBUG
        cout << "readdir(): " << name_ << "; errno: " << errno << endl;
#endif
        if (filter != "*" && (name_.find(filter) == string::npos))
            continue;

        const string path = dir + entries->d_name;
        list.push_back(path);
    }
}

inline void scan_directory::update_file_list_(bool recursive, const string &dir,
                                              const string &filter, DIR *&dr,
                                              vector<string> &list) {
    errno = 0;
    struct dirent *entries; // directory entries
    while ((entries = readdir(dr))) {
        const string name_ = entries->d_name;
#ifdef DEBUG
        cout << "readdir(): " << name_ << "; errno: " << errno << endl;
#endif
        const string path_ = dir + name_;
        if (is_dir(path_)) {
            const string starts0 = name_.substr(0, ign_fn_starts_0.size());
            if (starts0 == ign_fn_starts_0 || name_ == ign_fn_is_0 ||
                name_ == ign_fn_is_1)
                continue;

            const string path0 = path_ + "/";
            if (recursive) {
                ls(list, path0, recursive, filter);
                continue;
            }
            list.push_back(path0);
            continue;
        }

        if (filter != "*" && (name_.find(filter) == string::npos))
            continue;

        list.push_back(path_);
    }
}

////////////////////////////////////////////////////////////////////
vector<string> scan_directory::ls(const string &dir, const string &filter) {
    vector<string> list;
    ls(list, dir, filter);

    return list;
}

////////////////////////////////////////////////////////////////////
void scan_directory::ls(vector<string> &list, const string &dir,
                        const string &filter) {
    const string mthn = "[scan_directory::ls(vector)]\t";
#ifdef DEBUG
    cout << mthn << endl;
#endif
    DIR *dr = nullptr;
    if (!open_dir_(dir, dr))
        return;

    update_file_list0_(dir, filter, dr, list);
    closedir(dr);

    if (errno != 0) {
        cerr << mthn << "readdir() failure; terminating" << endl;
        return;
    }
}

////////////////////////////////////////////////////////////////////
void scan_directory::ls(vector<string> &list, const string &dir,
                        const bool recursive, const string &filter) {
    const string mthn = "[scan_directory::ls(vector, bool)]\t";
#ifdef DEBUG
    cout << mthn << endl;
#endif
    DIR *dr = nullptr;
    if (!open_dir_(dir, dr))
        return;

    update_file_list_(recursive, dir, filter, dr, list);
    closedir(dr);

    if (errno != 0) {
        cerr << mthn << "readdir() failure; terminating" << endl;
        return;
    }
}

////////////////////////////////////////////////////////////////////
vector<string> scan_directory::ls(const bool overrideRecursive,
                                  const bool recursive) {
#ifdef DEBUG
    const string mthn = "[scan_directory::ls(bool, bool)]";
    cout << mthn << endl;
#endif
    vector<string> fileNames;
    if (overrideRecursive) {
        vector<pair<string, bool> >::iterator it = dirs_to_scan_.begin();
        for (; it != dirs_to_scan_.end(); ++it) {
            ls(fileNames, (*it).first, recursive, ".root");
        }
        return fileNames;
    }

    ls(fileNames, dirs_to_scan_, ".root");
    return fileNames;
}

////////////////////////////////////////////////////////////////////
void scan_directory::ls(vector<string> &list, vector<pair<string, bool> > &dirs,
                        const string &filter) {
    for (vector<pair<string, bool> >::iterator it = dirs.begin();
         it != dirs.end(); ++it) {
        ls(list, (*it).first, (*it).second, filter);
    }
    sort(list.rbegin(), list.rend());
}

#endif
