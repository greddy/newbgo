#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "pixel/PixelRootUtilities/version.h"

GETPACKAGEINFO(PixelRootUtilities)

void PixelRootUtilities::checkPackageDependencies()  {

    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(xcept);
    CHECKDEPENDENCY(xdaq);
}

std::set<std::string, std::less<std::string> > PixelRootUtilities::getPackageDependencies() {

    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, xcept);
    ADDDEPENDENCY(dependencies, xdaq);
    return dependencies;
}
