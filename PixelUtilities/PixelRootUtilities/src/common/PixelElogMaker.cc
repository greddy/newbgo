/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2008 Cornell.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Ryd	                				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "PixelUtilities/PixelRootUtilities/include/PixelElogMaker.h"

PixelElogMaker::PixelElogMaker(const std::string name) {
    titlename = name;
}

void PixelElogMaker::post(const std::string run, const std::string outputdir) {

    //  string cmd = "/home/cmspixel/user/local/elog -h elog.physik.uzh.ch -p 8080 -s -u cmspixel uzh2014 -n 0 -l PixelPOS -a Filename=\"";
    //  cmd += run;
    //  cmd += " : ";
    //  cmd += titlename;
    //  cmd += "\"";
    ////  cmd += "\" -m ";
    ////  cmd += logoutput;
    ////  cmd += plotcommand;
    //
    //  //  cout << "---------------------------" << endl;
    //  //  cout << "e-log post:" << cmd << endl;
    //  system(cmd.c_str());
    //  std::cout << "---------------------------" << std::endl;

    std::string cmd_html = "python /home/cmspixel/user/local/DirectoryTree.py ";
    cmd_html += outputdir;
    //  cmd += "\" -m ";
    //  cmd += logoutput;
    //  cmd += plotcommand;

    system(cmd_html.c_str());

    std::cout << std::endl;
    std::cout << "**********************************************************" << std::endl;
    std::cout << "*  output plots can be found : " << std::endl;
    std::cout << "*  file://" << outputdir << "/index.html" << std::endl;
    std::cout << "*  (use this URL for your firefox !" << std::endl;
    std::cout << "**********************************************************" << std::endl;
}
