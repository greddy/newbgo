
#ifndef _FIFO3Decoder_h_
#define _FIFO3Decoder_h_

#include <vector>
#include <stdint.h>
#include <stddef.h>

class FIFO3Decoder {
  public:
    //FIFO3Decoder(uint64_t *buffer);
    FIFO3Decoder(uint64_t *buffer, bool Verbose = false);
    FIFO3Decoder(uint64_t *buffer, size_t bufsize, size_t nevents_expected);

    unsigned int nhits() const {
        return hits_.size();
    }
    unsigned int nevents() const {
        return events_.size();
    }

    unsigned int channel(unsigned int ihit) const {
        return (hits_[ihit] >> 26) & 0x3f;
    }
    unsigned int rocid(unsigned int ihit) const {
        return (hits_[ihit] >> 21) & 0x1f;
    }

    unsigned int dcol(unsigned int ihit) const {
        return (hits_[ihit] >> 16) & 0x1f;
    }
    unsigned int pxl(unsigned int ihit) const {
        return (hits_[ihit] >> 8) & 0xff;
    }

    unsigned int coll1(unsigned int ihit) const {
        return (hits_[ihit] >> 15) & 0x3f;
    }
    unsigned int rowl1(unsigned int ihit) const {
        return (hits_[ihit] >> 8) & 0x7f;
    }

    unsigned int pulseheight(unsigned int ihit) const {
        return (hits_[ihit] >> 0) & 0xff;
    }

    unsigned int column(unsigned int ihit) const {
        return dcol(ihit) * 2 + pxl(ihit) % 2;
    }
    unsigned int row(unsigned int ihit) const {
        return 80 - (pxl(ihit) / 2);
    }

    bool is_header_word(uint64_t word) const;
    bool is_trailer_word(uint64_t) const;
    bool error(unsigned int, bool Verbose = false) const;

    // Static methods, to call outside class instance
    static unsigned int getChannel(unsigned int hit) {
        return (hit >> 26) & 0x3f;
    }
    static unsigned int geRocid(unsigned int hit) {
        return (hit >> 21) & 0x1f;
    }

    static unsigned int getDcol(unsigned int hit) {
        return (hit >> 16) & 0x1f;
    }
    static unsigned int getPxl(unsigned int hit) {
        return (hit >> 8) & 0xff;
    }

    static unsigned int getColl1(unsigned int hit) {
        return (hit >> 15) & 0x3f;
    }
    static unsigned int getRowl1(unsigned int hit) {
        return (hit >> 8) & 0x7f;
    }

    static unsigned int getPulseheight(unsigned int hit) {
        return (hit >> 0) & 0xff;
    }

    static unsigned int getColumn(unsigned int dcol, unsigned int pxl) {
        return dcol * 2 + pxl % 2;
    }
    static unsigned int getRow(unsigned int pxl) {
        return 80 - (pxl / 2);
    }

  private:
    std::vector<unsigned int> hits_;
    std::vector<unsigned int> hits_ev_;
    std::vector<unsigned int> events_;
};

#endif
