#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include <iostream>
#include <assert.h>

using namespace std;

namespace {
const uint32_t channelMask = 0xfc000000; // channel num mask
const uint32_t errCodeMask = 0x03e00000;
const uint32_t ttcrxEventMask = 0x001fe000;
const uint32_t errDetailMask = 0x00000f00;
const uint32_t dataWordMask = 0x000000ff;
const bool printErrors = true; // print the important erros
const bool PRINT_ALL = false;  // print all errors
}

ErrorFIFODecoder::ErrorFIFODecoder(uint32_t *buffer, unsigned int size) {

    buffer_ = buffer;
    size_ = size;

    //errCodeMask_=   0x03e00000;
    //ttcrxEventMask_=0x001fe000;
    //dataWordMask_=  0x000000ff;
    //errDetailMask_= 0x00000f00;
}

void ErrorFIFODecoder::printToStream(std::ostream &out) {
    //const uint32_t channelMask    = 0xfc000000; // channel num mask

    if (PRINT_ALL)
        out << "--- Error FIFO Data ---" << std::endl;
    for (unsigned int i = 0; i < size_; ++i) {
        uint32_t word = buffer_[i];
        if (PRINT_ALL)
            out << "Word #" << i << " is 0x" << std::hex << word << std::dec;

        int stat = error(word, out);

        // continue;

        // const unsigned int channel   =(word & channelMask) >>26;
        // const unsigned int errCode   =(word & errCodeMask)>>21;
        // const unsigned int ttcrxEvent=(word & ttcrxEventMask)>>13;
        // const unsigned int dataWord  =(word & dataWordMask);

        // int status=0;

        // switch (errCode) {

        // case 0x1f: { // Event Number Error
        //   const unsigned int bit12   =(word & 0x1f00) >>12;
        //   out<<" Event Number Error Channel="<<channel<<" Event # "
        // 	  <<ttcrxEvent<<", TBM Event #"<<dataWord<<" bit12 "<<bit12<<std::endl;
        //   status=1;
        //   break;
        // }
        // case 0x1e: { // Trailer
        //   out<<" Trailer Error. channel= "<<channel<<" Event #"<<ttcrxEvent
        // 	  <<", TBM Trailer="<<hex<<dataWord<<dec;
        //   int errorDetail=(word & errDetailMask)>>8;
        //   if ((errorDetail & 0x8)>>3) {out<<", Invalid number of ROCs ";status=1;}
        //   if ((errorDetail & 0x4)>>2) out<<", AutoReset ";
        //   if ((errorDetail & 0x2)>>1) out<<", PKAM ";
        //   if (errorDetail & 0x1) {out<<", Overflow ";status=1;}
        //   if (errorDetail ==0) { // read trailer message

        // 	out<<", Trailer Message ";
        // 	if( (dataWord&0x60)== 0x60 ) {
        // 	  if(printErrors) out<<" TBM Reset - "<<"channel: "<<channel;
        // 	}
        // 	if( (dataWord&0x60)== 0x20 ) {
        // 	  if(PRINT_DUMMY) out<<" ROC Reset - "<<"channel: "<<channel;
        // 	}
        // 	if( (dataWord&0x02)== 0x02 ) {
        // 	  if(PRINT_DUMMY) out<<"  CalInject - "<<"channel: "<<channel;
        // 	}
        // 	if( (dataWord&0x9D)!= 0x0 ) { // mask off bit 1,5,6
        // 	  if(printErrors) out<<"  Trailer Error- "<<"channel: "<<channel
        // 			      <<" TBM status:0x"<<hex<<dataWord<<dec;
        // 	  status=true;
        // 	}
        // 	//cout<<endl;
        //   }
        //   out<<std::endl;
        //   break;
        // }
        // case 0x1d: { // Timeout
        //   unsigned int bit20 = (word & 0x100000)>>20; // to distuiguish word1 & 2

        //   if(bit20==1) { // 1st word

        // 	//int chan  = (word&0xfc000000)>>26;
        // 	int stack = word&0x3f;
        // 	//int dum1 = (word&0x1fc0)>>6;
        // 	//int event  = (word&0x1fe000)>>13;
        // 	out<<" Timeout 1st word: channel "<<channel<<" "<<" event "
        // 	    <<ttcrxEvent<<" stack "<<stack;

        //   } else if(bit20 == 0) { // 2nd word

        // 	//int chan1  = (word&0xfc000000)>>26;
        // 	int chan  = (word&0x3f);
        // 	//int dum1 = (word&0x1fc0)>>6;
        // 	//int dum2 = (word&0xfe000)>>13;
        // 	out<<" Timeout 2nd word: channel "<<channel<<"/"<<chan<<" event "
        // 	    <<ttcrxEvent<<endl;

        //   }

        //   //out<<" : Time Out Error. TTCrx Event #"<<ttcrxEvent<<", Error on Channel "
        //   // <<dataWord<<std::endl; break;
        //   status=1;
        // }
        // case 0x1c:
        //   out<<" : Nearly Full. TTCrx Event #"<<ttcrxEvent
        // 	  <<", Nearly Full on Channel "<<dataWord<<std::endl; break;
        // case 0x1b: out<<" : Dummy"<<std::endl; break;
        // case 0x1a: out<<" : Gap"<<std::endl; break;
        // default: out << " ???" << std::endl; break;
        // }

    } // loop over error words

    out << "---------------------" << std::endl;
}

unsigned int ErrorFIFODecoder::size() { return size_; }

uint32_t ErrorFIFODecoder::word(unsigned int i) {
    if (i > size_)
        assert(0);
    return buffer_[i];
}

std::string ErrorFIFODecoder::errorType(uint32_t word) {
    int errCode = (word & errCodeMask) >> 21;
    std::string errString;

    if (errCode == 0x1f)
        errString = "EventNumber";
    else if (errCode == 0x1e)
        errString = "Trailer";
    else if (errCode == 0x1d)
        errString = "TimeOut";
    else if (errCode == 0x1c)
        errString = "NearlyFull";
    else if (errCode == 0x1b)
        errString = "Dummy";
    else if (errCode == 0x1a)
        errString = "Gap";
    else
        errString = "WTF?";

    return errString;
}

int ErrorFIFODecoder::error(uint32_t word, std::ostream &out) {

    //const uint32_t channelMask    = 0xfc000000; // channel num mask
    //const uint32_t errCodeMask_   = 0x03e00000;
    //const uint32_t ttcrxEventMask_= 0x001fe000;
    //const uint32_t errDetailMask_ = 0x00000f00;
    //const uint32_t dataWordMask_  = 0x000000ff;

    const unsigned int channel = (word & channelMask) >> 26;
    const unsigned int errCode = (word & errCodeMask) >> 21;
    const unsigned int ttcrxEvent = (word & ttcrxEventMask) >> 13;
    const unsigned int dataWord = (word & dataWordMask);

    int status = 0;

    switch (errCode) {

    case 0x1f: { // Event Number Error
        const unsigned int bit12 = (word & 0x1f00) >> 12;
        cout << " Event Number Error Channel=" << channel << " Event # "
             << ttcrxEvent << ", TBM Event #" << dataWord << " bit12 " << bit12 << std::endl;
        status = 1;
        break;
    }
    case 0x1e: { // Trailer
        if (PRINT_ALL)
            cout << " Trailer Error. channel= " << channel << " Event #" << ttcrxEvent
                 << ", TBM Trailer=" << hex << dataWord << dec;
        int errorDetail = (word & errDetailMask) >> 8;
        if ((errorDetail & 0x8) >> 3) {
            cout << ", Invalid number of ROCs ";
            status = 1;
        }
        if ((errorDetail & 0x4) >> 2)
            cout << ", AutoReset ";
        if ((errorDetail & 0x2) >> 1)
            cout << ", PKAM ";
        if (errorDetail & 0x1) {
            cout << ", Overflow ";
            status = 1;
        }
        if (errorDetail == 0) { // read trailer message

            cout << ", Trailer Message ";
            if ((dataWord & 0x60) == 0x60) {
                if (printErrors)
                    cout << " TBM Reset - "
                         << "channel: " << channel;
            }
            if ((dataWord & 0x60) == 0x20) {
                if (PRINT_ALL)
                    cout << " ROC Reset - "
                         << "channel: " << channel;
            }
            if ((dataWord & 0x02) == 0x02) {
                if (PRINT_ALL)
                    cout << "  CalInject - "
                         << "channel: " << channel;
            }
            if ((dataWord & 0x9D) != 0x0) { // mask off bit 1,5,6
                if (printErrors)
                    cout << "  Trailer Error- "
                         << "channel: " << channel
                         << " TBM status:0x" << hex << dataWord << dec;
                status = true;
            }
            //cout<<endl;
        }
        cout << std::endl;
        break;
    }
    case 0x1d: {                                      // Timeout
        unsigned int bit20 = (word & 0x100000) >> 20; // to distuiguish word1 & 2

        if (bit20 == 1) { // 1st word

            //int chan  = (word&0xfc000000)>>26;
            int stack = word & 0x3f;
            //int dum1 = (word&0x1fc0)>>6;
            //int event  = (word&0x1fe000)>>13;
            cout << " Timeout 1st word: channel " << channel << " "
                 << " event "
                 << ttcrxEvent << " stack " << stack;

        } else if (bit20 == 0) { // 2nd word

            //int chan1  = (word&0xfc000000)>>26;
            int chan = (word & 0x3f);
            //int dum1 = (word&0x1fc0)>>6;
            //int dum2 = (word&0xfe000)>>13;
            cout << " Timeout 2nd word: channel " << channel << "/" << chan << " event "
                 << ttcrxEvent << endl;
        }

        //cout<<" : Time Out Error. TTCrx Event #"<<ttcrxEvent<<", Error on Channel "
        // <<dataWord<<std::endl; break;
        status = 1;
    }
    case 0x1c:
        cout << " : Nearly Full. TTCrx Event #" << ttcrxEvent
             << ", Nearly Full on Channel " << dataWord << std::endl;
        break;
    case 0x1b:
        cout << " : Dummy" << std::endl;
        break;
    case 0x1a:
        cout << " : Gap" << std::endl;
        break;
    default:
        cout << " ???" << std::endl;
        break;
    }

    return status;
}
