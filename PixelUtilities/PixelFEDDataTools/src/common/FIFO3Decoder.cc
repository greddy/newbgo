#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelHit.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelSLinkHeader.h"
#include <iostream>
#include <assert.h>

using namespace std;
using namespace pos;

bool FIFO3Decoder::is_header_word(uint64_t word) const {
    return (word >> 56) == 0x50;
}

bool FIFO3Decoder::is_trailer_word(uint64_t word) const {
    return (word >> 46) == 0x28000; // utca channel 40 rocs 0-7 can reproduce the old 0xa0
}

FIFO3Decoder::FIFO3Decoder(uint64_t *buffer, bool Verbose) {
    static int event = 0;
    static bool firstError = true;

    unsigned int *buf = (unsigned int *)buffer;

    if (Verbose) {
        cout << " Look at event " << event;
        PixelSLinkHeader header(buffer[0]);
        int event_id = (buffer[0] >> 32) & 0xffffff;
        cout << " FED event num " << header.getLV1_id() << " " << header.getBX_id()
             << " " << event_id << " " << (event_id % 256) << endl;
    }

    assert((buffer[0] >> 60) == 0x5);
    event++;
    unsigned int counter = 1, countErrors = 0;
    bool errorFlag = false;
    bool status = false;
    while (!is_trailer_word(buffer[counter])) { // loop over FIFO3

        // unpack 2 words
        for (unsigned int index = counter * 2; index < counter * 2 + 2; index++) {
            unsigned int rocid = (buf[index] >> 21) & 0x1f;

            if (rocid < 25)
                hits_.push_back(buf[index]);
            else {
                //if (rocid>27&&(rocid!=30)) {
                //unsigned int channel=(buf[index]>>26)&0x3f;
                //FIXME Code should set some error FLAG but can not print message here
                //std::cout << "buf="<<std::hex<<buf[index]<<std::dec<<" channel="<<channel
                //	  <<" rocid="<<rocid<<std::endl;
                if (firstError)
                    status = error(buf[index], Verbose);
                countErrors++;
                if (status)
                    errorFlag = true;
                //errorFlag=true;
            } // if error

        } // for 2 words

        counter++;

        //FIXME what should this check actually be???
        if (counter > 7000) {
            std::cout << "counter=" << counter << " will exit loop!" << std::endl;
            break;
        }

    } // while not trailer

    // Only do when error and verbosity is set
    if (Verbose) {
        ///firstError = false;
        cout << " readout " << counter << " hits " << hits_.size() << " errors " << countErrors << endl;

        std::cout << "Contents of Spy FIFO 3" << std::endl;
        std::cout << "----------------------" << std::endl;
        for (unsigned int i = 0; i <= counter; ++i) {
            unsigned int w1 = buffer[i] & 0xffffffff;
            unsigned int w2 = (buffer[i] >> 32) & 0xffffffff;
            PixelHit h1(w1);
            PixelHit h2(w2);
            if (i == 0) {
                cout << "Clock " << i << " = 0x" << hex << buffer[i] << dec
                     << " Slink Header " << endl;
            } else if (i == counter) {
                cout << "Clock " << i << " = 0x" << hex << buffer[i] << dec
                     << " Slink Trailer " << endl;
            } else {
                std::cout << "Clock " << i << " = 0x" << std::hex << buffer[i]
                          << " " << w1 << " " << w2 << " " << std::dec
                          << h1.getLink_id() << " " << h1.getROC_id() << " "
                          << h1.getDCol_id();
                if ((h1.getROC_id()) < 25)
                    cout << "(" << h1.getColumnL1() << ")";
                cout << " " << h1.getPix_id();
                if ((h1.getROC_id()) < 25)
                    cout << "(" << h1.getRowL1() << ")";
                cout << " " << h2.getLink_id() << " " << h2.getROC_id() << " " << h2.getDCol_id();
                if ((h2.getROC_id()) < 25)
                    cout << "(" << h2.getColumnL1() << ")";
                cout << " " << h2.getPix_id();
                if ((h2.getROC_id()) < 25)
                    cout << "(" << h2.getRowL1() << ")";
                cout << std::endl;
            } // hits
        }     // for loop

        // For testing only, stop on error
        if (Verbose && errorFlag) {
            int dum = 0;
            cout << " error " << endl;
            cin >> dum;
        }
    }
}

// FIFO3Decoder::FIFO3Decoder(uint64_t *buffer)
// {

//   uint32_t *buf=(uint32_t*)buffer;

//   //std::cout << "buf[0]=0x"<<std::hex<<buf[0]<<std::dec<<std::endl;

//   assert(is_header_word(buffer[0]));

//   unsigned int counter=1;

//   while (!is_trailer_word(buffer[counter])) {

//     for(unsigned int index=counter*2;index<counter*2+2;index++){

//       //unsigned int channel=(buf[index]>>26)&0x3f;
//       unsigned int rocid=(buf[index]>>21)&0x1f;

//       if (rocid<25) hits_.push_back(buf[index]);
//       if (rocid>27&&(rocid!=30)) {
//         //FIXME Code should set some error FLAG but can not print message here
//         //std::cout << "buf="<<std::hex<<buf[index]<<std::dec<<" channel="<<channel<<" rocid="<<rocid<<std::endl;
//       }

//     }

//     counter++;

//     //FIXME what should this check actually be???

//     if (counter>20000) {
//       std::cout << "counter="<<counter<<" will exit loop!"<<std::endl;
//       break;
//     }

//   }

// }

FIFO3Decoder::FIFO3Decoder(uint64_t *buffer, size_t bufsize, size_t nevents_expected) {
    uint32_t *buf = (uint32_t *)buffer;
    assert(is_header_word(buffer[0]));

    size_t ntrailers = 0;

    for (size_t counter = 1; counter < bufsize; ++counter) {
        if (is_header_word(buffer[counter]) && is_trailer_word(buffer[counter - 1]))
            continue;

        if (is_trailer_word(buffer[counter])) {
            if (++ntrailers == nevents_expected)
                break;
            else
                continue;
        }

        for (size_t index = counter * 2; index < counter * 2 + 2; ++index) {
            const unsigned int rocid = (buf[index] >> 21) & 0x1f;
            if (rocid < 25)
                hits_.push_back(buf[index]);
        }
    }
}
//----------------------------------------------------------
bool FIFO3Decoder::error(unsigned int word, bool printErrors) const {
    bool status = false;
    const bool PRINT_ALL = false; // print all errors including DUMMY & GAP words
    const bool forceTO = false;
    const bool forceENE = false;
    const bool forceOV = false;
    const bool forceNOR = false;
    const bool forceAutoReset = false;
    const bool forcePKAM = false;
    const bool forceTBMRes = false;

    const unsigned int errorMask = 0x03e00000;
    const unsigned int dummyMask = 0x03600000; // is it used?
    const unsigned int gapMask = 0x03400000;   // is it used?
    const unsigned int timeOut = 0x03a00000;
    const unsigned int eventNumError = 0x03e00000;
    const unsigned int trailError = 0x03c00000;
    const unsigned int fifoError = 0x03800000; // does it exist

    //  const unsigned int  timeOutChannelMask = 0x1f;  // channel mask for timeouts
    //const unsigned int  eventNumMask = 0x1fe000; // event number mask
    const unsigned int channelMask = 0xfc000000; // channel num mask
    const unsigned int tbmEventMask = 0xff;      // tbm event num mask
    //const unsigned int  overflowMask = 0x100;   // data overflow
    const unsigned int tbmStatusMask = 0xff; //TBM trailer info
    //const unsigned int  BlkNumMask = 0x700;   //pointer to error fifo #
    //const unsigned int  FsmErrMask = 0x600;   //pointer to FSM errors
    //const unsigned int  RocErrMask = 0x800;   //pointer to #Roc errors
    const unsigned int ChnFifMask = 0x1f; //channel mask for fifo error

    const unsigned long ChnFifMask0 = 0x1;  //channel mask for fifo error
    const unsigned long ChnFifMask1 = 0x2;  //channel mask for fifo error
    const unsigned long ChnFifMask2 = 0x4;  //channel mask for fifo error
    const unsigned long ChnFifMask3 = 0x8;  //channel mask for fifo error
    const unsigned long ChnFifMask4 = 0x10; //channel mask for fifo error

    const unsigned int Fif2NFMask = 0x40; //mask for fifo2 NF
    const unsigned int TrigNFMask = 0x80; //mask for trigger fifo NF

    //const int offsets[8] = {0,4,9,13,18,22,27,31};

    //cout<<"error word "<<hex<<word<<dec<<endl;
    if ((word & errorMask) == dummyMask) { // DUMMY WORD
        if (PRINT_ALL && printErrors)
            cout << " Dummy word" << endl;
        return false;

    } else if ((word & errorMask) == gapMask) { // GAP WORD
        if (PRINT_ALL && printErrors)
            cout << " Gap word" << endl;
        return false;

    } else if ((word & errorMask) == timeOut) {       // TIMEOUT
        unsigned int bit20 = (word & 0x100000) >> 20; // to distuiguish word1 & 2
        status = true;

        if (bit20 == 0) { // 2nd word

            int chan1 = (word & 0xfc000000) >> 26;
            int chan2 = (word & 0x3f);
            int dum1 = (word & 0x1fc0) >> 6;
            int dum2 = (word & 0xfe000) >> 13;
            if (printErrors || forceTO)
                cout << " Timeout 2nd word: channel " << chan1 << "/" << chan2 << " (" << hex << dum1 << " " << dum2 << ")"
                     << dec << endl;

            // // this part might be invalid now
            // unsigned int timeoutCnt = (word &  0x7f800)>>11; // only for slink
            // // unsigned int timeoutCnt = ((word&0xfc000000)>>24) + ((word&0x1800)>>11); // only for fifo
            // // More than 1 channel within a group can have a timeout error
            // unsigned int index = (word & 0x1F);  // index within a group of 4/5
            // unsigned int chip = (word& BlkNumMask)>>8;
            // int offset = offsets[chip];
            // cout<<"Timeout Error- channels: ";
            // for(int i=0;i<5;i++) {
            //   if( (index & 0x1) != 0) {
            //     int chan = offset + i + 1;
            //     cout<<chan<<" ";
            //   }
            //   index = index >> 1;
            // }
            // cout<<endl;
            // //end of timeout  chip and channel decoding
            // cout << " TimeoutCount: " << timeoutCnt<<endl;;

        } else if (bit20 == 1) { // 1st word

            int chan = (word & 0xfc000000) >> 26;
            int stack = word & 0x3f;
            int dum1 = (word & 0x1fc0) >> 6;
            int dum2 = (word & 0xfe000) >> 13;

            if (printErrors)
                cout << "Timeout 1st word: channel " << chan << hex << " (" << stack << " " << dum1 << " " << dum2 << dec << ") ";
        }

    } else if ((word & errorMask) == eventNumError) { // EVENT NUMBER ERROR
        unsigned int channel = (word & channelMask) >> 26;
        unsigned int tbm_event = (word & tbmEventMask);
        unsigned int tbm_status = (word & 0x1000) >> 12;
        int dum1 = (word & 0xf00) >> 8;
        int dum2 = (word & 0x1fe000) >> 13;
        status = true;

        if (printErrors || forceENE)
            cout << "  Event Number Error- channel: " << channel << " tbm event nr. "
                 << tbm_event << " (status " << tbm_status << " " << dum1 << " " << dum2 << ")" << endl;

    } else if (((word & errorMask) == trailError)) {
        unsigned int channel = (word & channelMask) >> 26;
        unsigned int tbm_status = (word & tbmStatusMask);
        unsigned int errBits = (word & 0xf00) >> 8;
        //int dum = (word&0x1ff000)>>12;

        //cout<<"  Trailer message - "<<"channel: "<<channel<<" status "
        //	<<hex<<tbm_status<<" "<<errBits<<" "<<dum<<dec<<endl;

        //if(word & RocErrMask) {
        if ((errBits & 0x8) != 0) {
            if (printErrors || forceNOR)
                cout << "  Number of Rocs Error- "
                     << "channel: " << channel << " (" << hex << tbm_status << dec << ")" << endl;
            status = true;
        }
        if ((errBits & 0x4) != 0) {
            if (printErrors || forceAutoReset)
                cout << "  Auto Reset - "
                     << "channel: " << channel << " " << hex << tbm_status << dec << endl;
            status = true;
        }
        if ((errBits & 0x2) != 0) {
            if (printErrors || forcePKAM)
                cout << "  PKAM- "
                     << "channel: " << channel << " " << hex << tbm_status << dec << endl;
            status = true;
        }
        if ((errBits & 0x1) != 0) {
            if (printErrors || forceOV)
                cout << "  Overflow- "
                     << "channel: " << channel << " " << hex << tbm_status << dec << endl;
            status = true;
        }

        //if(!((word & RocErrMask)|(word & FsmErrMask)|(word & overflowMask)))
        if (errBits == 0) {

            if ((tbm_status & 0x60) == 0x60) {
                if (printErrors || forceTBMRes)
                    cout << " TBM Reset - "
                         << "channel: " << channel << endl;
            }
            if ((tbm_status & 0x60) == 0x20) {
                if (PRINT_ALL && printErrors)
                    cout << " ROC Reset - "
                         << "channel: " << channel << endl;
            }
            if ((tbm_status & 0x02) == 0x02) {
                if (PRINT_ALL && printErrors)
                    cout << "  CalInject - "
                         << "channel: " << channel << endl;
            }
            if ((tbm_status & 0x9D) != 0x0) { // mask off bit 1,5,6
                if (printErrors)
                    cout << "  Trailer Error- "
                         << "channel: " << channel << " TBM status:0x"
                         << hex << tbm_status << dec << " " << endl;
                status = true;
            }
        } // !tbm_status

    } else if ((word & errorMask) == fifoError) { // Does it exist?
        status = true;
        if (word & Fif2NFMask)
            if (printErrors)
                cout << " A fifo 2 is Nearly full- ";
        if (word & TrigNFMask)
            if (printErrors)
                cout << " The trigger fifo is nearly Full - ";
        if ((word & ChnFifMask) && printErrors) {
            //cout<<"fifo-1 is nearly full for channel"<<(word & ChnFifMask);
            cout << " fifo-1 is nearly full for channel(s) of this FPGA//";
            if (word & ChnFifMask0)
                cout << " 1 //";
            if (word & ChnFifMask1)
                cout << " 2 //";
            if (word & ChnFifMask2)
                cout << " 3 //";
            if (word & ChnFifMask3)
                cout << " 4 //";
            if (word & ChnFifMask4)
                cout << " 5 ";
        }
        if (printErrors)
            cout << endl;

    } else {
        if (PRINT_ALL && printErrors)
            cout << " Unknown error?";
    }

    return status;
}
