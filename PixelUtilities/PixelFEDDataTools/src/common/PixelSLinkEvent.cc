#include "PixelSLinkEvent.h"

using namespace pos;

// -------------- Constructors -----------------

PixelSLinkEvent::PixelSLinkEvent(const std::vector<Word64> &buffer) {
    buffer_ = buffer;
}

PixelSLinkEvent::PixelSLinkEvent(uint64_t *buffer, unsigned int length) {
    for (unsigned int i = 0; i < length; ++i) {
        buffer_.push_back(Word64(buffer[i]));
    }
}

// --------------------------------------------------------

void PixelSLinkEvent::push_back(const Word64 &word) { buffer_.push_back(word); }

void PixelSLinkEvent::clearBuffer() { buffer_.clear(); }

inline bool PixelSLinkEvent::decodeEvent_proc_word_(const std::string wtype,
                                                    Word32 w) {
    const std::string mthn = "[PixelSLinkEvent::decodeEvent()]";
    const unsigned int rocid = w.getBitsFast(21, 25); // needs a nonconst
    if (rocid > 0 && rocid <= 24) {
        hits_.push_back(PixelHit(w));
        return true;
    }
    if (rocid >= 25) { // 0x19
        errors_.push_back(PixelError(w));
        return true;
    }

    std::cout << mthn << " - " << wtype << " word"
              << " has ROC ID = " << rocid << " which is invalid!" << std::endl;
    return false;
}

bool PixelSLinkEvent::decodeEvent() {
    const std::string mthn = "[PixelSLinkEvent::decodeEvent()]";
    bool decoded = true;

    const unsigned int sizeBuffer = buffer_.size();
    if (sizeBuffer == 0) {
        std::cout << mthn << "\tBufferSize=0" << std::endl;
        return false;
    }
    // std::cout << "sizeBuffer:"<<sizeBuffer<<std::endl;

    header_ = PixelSLinkHeader(buffer_.at(0));
    if (header_.getWord64().getWord() == 0) {
        std::cout << mthn << " - First 64 bit word 0x" << std::hex
                  << buffer_.at(0).getWord() << std::dec
                  << " is not an SLink Header!" << std::endl;
        decoded = false;
    }
    hits_.clear();
    hits_.reserve(sizeBuffer - 2);
    errors_.clear();
    errors_.reserve(sizeBuffer - 2);
    for (unsigned int i = 1; i < sizeBuffer - 1; ++i) {
        if (!decodeEvent_proc_word_("low", buffer_[i].getLowWord())) {
            decoded = false;
            std::cout << mthn << " - for word: 0x" << std::hex
                      << buffer_.at(i).getWord() << std::dec << std::endl;
        }

        if (!decodeEvent_proc_word_("high", buffer_[i].getHighWord())) {
            decoded = false;
            std::cout << mthn << " - for word: 0x" << std::hex
                      << buffer_.at(i).getWord() << std::dec << std::endl;
        }
    }

    trailer_ = PixelSLinkTrailer(buffer_.at(sizeBuffer - 1));
    if (trailer_.getWord64().getWord() == 0) {
        std::cout << mthn << " - Last 64 bit word 0x" << std::hex
                  << buffer_.at(sizeBuffer - 1).getWord() << std::dec
                  << " is not an SLink Trailer!" << std::endl;
        decoded = false;
    }

    return decoded;
}

// -------------- Getters --------------------------------

PixelSLinkHeader PixelSLinkEvent::getHeader() { return header_; }

PixelSLinkTrailer PixelSLinkEvent::getTrailer() { return trailer_; }

std::vector<PixelHit> PixelSLinkEvent::getHits() { return hits_; }

std::vector<PixelError> PixelSLinkEvent::getErrors() { return errors_; }

// ----------------------------------------------------
