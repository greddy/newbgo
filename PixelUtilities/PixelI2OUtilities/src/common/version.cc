#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "pixel/PixelI2OUtilities/version.h"

GETPACKAGEINFO(PixelI2OUtilities)

std::set<std::string, std::less<std::string> >
PixelI2OUtilities::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, xcept);
    ADDDEPENDENCY(dependencies, xdaq);

    return dependencies;
}
