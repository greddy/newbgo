#include "PixelTCDSSupervisor/PixeliCISupervisor.h"

XDAQ_INSTANTIATOR_IMPL(pixel::tcds::PixeliCISupervisor);

pixel::tcds::PixeliCISupervisor::PixeliCISupervisor(xdaq::ApplicationStub *stub)
    : PixelTCDSSupervisor(stub) {
    tcdsType_ = "iCI";
    supervisorType_ = "PixeliCISupervisor";
}

pixel::tcds::PixeliCISupervisor::~PixeliCISupervisor() {
}
