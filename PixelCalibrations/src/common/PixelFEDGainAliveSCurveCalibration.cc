// $Id: PixelAddressLevelCalibration.cc,v 1.1

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "PixelCalibrations/include/PixelFEDGainAliveSCurveCalibration.h"



PixelFEDGainAliveSCurveCalibration::PixelFEDGainAliveSCurveCalibration(const PixelFEDSupervisorConfiguration &tempConfiguration, const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper) {
          std::cout << "Greetings from the PixelFEDGainAliveSCurveCalibration copy constructor." << std::endl;
}

xoap::MessageReference PixelFEDGainAliveSCurveCalibration::execute(xoap::MessageReference msg) {

    cout << "PixelFEDGainAliveSCurveCalibration::execute should never be called"
         << endl;

    return makeSOAPMessageReference("GainAliveSCurveCalibrationWithPixelsDone");
}

void PixelFEDGainAliveSCurveCalibration::initializeFED() {
    setFEDModeAndControlRegister(0x8, 0x30010);
}

xoap::MessageReference PixelFEDGainAliveSCurveCalibration::beginCalibration(xoap::MessageReference msg) {

    //tempCalibObject_=dynamic_cast<PixelCalibConfiguration*>(theCalibObject_);
    //assert(tempCalibObject_!=0);
    //Verbose = tempCalibObject_->parameterValue("Verbose") == "yes";
    //cout<<" Verbose "<<Verbose<<endl;

    return makeSOAPMessageReference("BeginCalibrationDone");
}

xoap::MessageReference PixelFEDGainAliveSCurveCalibration::endCalibration(xoap::MessageReference msg) {
    cout << "In PixelFEDGainAliveSCurveCalibration::endCalibration()" << endl;
    return makeSOAPMessageReference("EndCalibrationDone");
}
