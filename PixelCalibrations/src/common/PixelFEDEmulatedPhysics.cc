// $Id: PixelAddressLevelCalibration.cc,v 1.1

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "PixelCalibrations/include/PixelFEDEmulatedPhysics.h"



PixelFEDEmulatedPhysics::PixelFEDEmulatedPhysics(const PixelFEDSupervisorConfiguration &tempConfiguration, const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper) {
          std::cout << "Greetings from the PixelFEDEmulatedPhysics copy constructor." << std::endl;
}

xoap::MessageReference PixelFEDEmulatedPhysics::execute(xoap::MessageReference msg) {

    //FIXME this code needs to be fixed! (ryd)
    //Broken in migration.
    assert(0);
    //workloop_->submit(physicsRunning_);
    diagService_->reportError("Emulated Physics data taking job submitted to the workloop", DIAGINFO);
    //*console_<<"Configure. Emulated Physics data taking job submitted to the workloop"<<std::endl;

    return makeSOAPMessageReference("EmulatedPhysicsWithPixelsDone");
}

void PixelFEDEmulatedPhysics::initializeFED() {

    setFEDModeAndControlRegister(0x8, 0x30014);
    // FIXME - setSpecialDac uses PixelFEDInterface's methods, which are not implemented in PixelPh1FEDInterface.
    //setSpecialDac(1);
    workloop_->submit(physicsRunning_);
    // Fill playback memory with contents of Calib file
    // FIXME - fillTestDAC uses PixelFEDInterface's methods, which are not implemented in PixelPh1FEDInterface.
    //xoap::MessageReference fillTestDACmsg = makeSOAPMessageReference("FillTestDAC");
    //fillTestDAC(fillTestDACmsg);
}

xoap::MessageReference PixelFEDEmulatedPhysics::beginCalibration(xoap::MessageReference msg) {

    return makeSOAPMessageReference("BeginCalibrationDone");
}

xoap::MessageReference PixelFEDEmulatedPhysics::endCalibration(xoap::MessageReference msg) {
    cout << "In PixelFEDEmulatedPhysics::endCalibration()" << endl;
    return makeSOAPMessageReference("EndCalibrationDone");
}
