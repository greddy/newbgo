#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelFEDTBMDelayCalibrationWithScoresBPIX.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TROOT.h"
#include "TStyle.h"
#include <iomanip>

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "pixel/utils/Utils.h"

PixelFEDTBMDelayCalibrationWithScoresBPIX::PixelFEDTBMDelayCalibrationWithScoresBPIX(const PixelFEDSupervisorConfiguration &tempConfiguration, const pixel::utils::SOAPER *soaper)
: PixelFEDCalibrationBase(tempConfiguration, *soaper), rootf_(0), logger_(app_->getApplicationLogger()) {
}

void PixelFEDTBMDelayCalibrationWithScoresBPIX::initializeFED() {
    pos::PixelCalibConfiguration *tempCalibObject = getCalibObject();

    headersOnly_ = tempCalibObject->parameterValue("HeadersOnly") == "yes";
    ignoreWordCount_ = tempCalibObject->parameterValue("IgnoreWordCount") == "yes";
    orChannels_ = tempCalibObject->parameterValue("OrChannels") == "yes";
    checkReadback_ = tempCalibObject->parameterValue("CheckReadback") == "yes";

    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(headersOnly_) << " " << ignoreWordCount_ << " " << orChannels_ << " " << checkReadback_);

    typedef std::set<std::pair<uint32_t, uint32_t> > colrow_t;
    const colrow_t colrows = tempCalibObject->pixelsWithHits(0);
    if (colrows.size() != 1) {
        LOG4CPLUS_FATAL(logger_, "must use exactly one pixel for score scan!");
        assert(0);
    }
    theCol_ = colrows.begin()->first;
    theRow_ = colrows.begin()->second;
    const uint32_t dc = theCol_ / 2;
    const uint32_t pxl = 160 - 2 * theRow_ + theCol_ % 2;
    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("Expected hit in col ") << theCol_ << " row " << theRow_ << " -> dc " << dc << " pxl " << pxl);

    const auto& fedsAndChannels = getFedsAndChannels();
    for (uint32_t ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const uint32_t fednumber = fedsAndChannels[ifed].first;
        const uint32_t vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];
        iFED->disableBE(true);
        iFED->setAutoPhases(false);
        iFED->setPixelForScore(dc, pxl);
    }
}

xoap::MessageReference PixelFEDTBMDelayCalibrationWithScoresBPIX::beginCalibration(xoap::MessageReference msg) {
    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("In PixelFEDTBMDelayCalibrationWithScoresBPIX::beginCalibration()"));

    nCalls_ = 0;

    readTimer_.setName("FED readout");
    osdTimer_.setName("OSD readout");
    otherTimer_.setName("FED rest-of");

    pos::PixelCalibConfiguration *tempCalibObject = getCalibObject();

    tempCalibObject->writeASCII(outputDir());

    dumps_ = tempCalibObject->parameterValue("Dumps") == "yes";
    forcePort1EqualToPort0_ = tempCalibObject->parameterValue("ForcePort1EqualToPort0") != "no";
    assert(forcePort1EqualToPort0_);
    keepTTBits_ = (tempCalibObject->parameterValue("KeepTTBits") != "no");

    for (uint32_t dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        std::vector<uint32_t> dacvals = tempCalibObject->scanValues(dacname);
        if (dacvals.size() > 1) {
            dacsToScan_.push_back(dacname);
            assert(dacname == "TBMPLL" || dacname == "TBMADelay" || dacname == "TBMBDelay");
        }
    }

    assert(dacsToScan_.size() == 2);

    if (dacsToScan_.empty() && tempCalibObject->parameterValue("NoScanOK") != "yes") {
        LOG4CPLUS_FATAL(logger_, LOG4CPLUS_TEXT("no dacs in scan?"));
        assert(0);
    }

    BookEm();

    return makeSOAPMessageReference("BeginCalibrationDone");
}

xoap::MessageReference PixelFEDTBMDelayCalibrationWithScoresBPIX::execute(xoap::MessageReference msg) {
    vector<string> parameters_names(2);
    parameters_names.push_back("WhatToDo");
    parameters_names.push_back("StateNum");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, parameters_names);

    const uint32_t state = stoul(parameters["StateNum"]);

    if (parameters["WhatToDo"] == "RetrieveData")
        RetrieveData(state);
    else if (parameters["WhatToDo"] == "Analyze")
        Analyze();
    else {
        LOG4CPLUS_FATAL(logger_, LOG4CPLUS_TEXT("PixelFEDTBMDelayCalibrationWithScoresBPIX::execute() does not understand the WhatToDo command, ") << parameters[ "WhatToDo"] << ", sent to it.");
        assert(0);
    }

    return makeSOAPMessageReference("FEDCalibrationsDone");
}

xoap::MessageReference PixelFEDTBMDelayCalibrationWithScoresBPIX::endCalibration(xoap::MessageReference msg) {
    readTimer_.printStats();
    if (checkReadback_)
        osdTimer_.printStats();
    otherTimer_.printStats();
    
    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("In PixelFEDTBMDelayCalibrationBPIX::endCalibration()"));
    
    const auto& fedsAndChannels = getFedsAndChannels();
    for (uint32_t ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const uint32_t fednumber = fedsAndChannels[ifed].first;
        const uint32_t vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface* iFED = FEDInterface_[vmeBaseAddress];
        iFED->setAutoPhases(true);
        iFED->disableBE(false);
    }

    return makeSOAPMessageReference("EndCalibrationDone");
}

void PixelFEDTBMDelayCalibrationWithScoresBPIX::RetrieveData(uint32_t state) {
    otherTimer_.start();

    static uint32_t state0 = 0, count = 0;
    if (state != state0) { // new state
        state0 = state;
        count = 0;
    }

    pos::PixelCalibConfiguration *tempCalibObject = getCalibObject();

    typedef std::set<std::pair<uint32_t, uint32_t> > colrow_t;
    const colrow_t colrows = tempCalibObject->pixelsWithHits(state);
    if (colrows.size() != 1) { // checked in initialize fed but need to keep checking
        LOG4CPLUS_FATAL(logger_, LOG4CPLUS_TEXT("must use exactly one pixel for score scan!"));
        assert(0);
    } else if (colrows.begin()->first != theCol_ || colrows.begin()->second != theRow_) {
        LOG4CPLUS_FATAL(logger_, LOG4CPLUS_TEXT("must use same pixel always for score scan!")); // JMTBAD or we could re-set the pixel in the fed
        assert(0);
    }

    FillTrigger(state);

    if (dumps_) {
        ostringstream msg; msg << "New FEDTBMDelay event " << event_ << " state " << state << " ";
        for (uint32_t dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
            const std::string &dacname = tempCalibObject->scanName(dacnum);
            const uint32_t dacvalue = tempCalibObject->scanValue(tempCalibObject->scanName(dacnum), state);
            msg << dacname << " -> " << dacvalue << " ";
        }
        LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
    }
    
    const bool read_osd_now = (nCalls_ + 1) % 32 == 0; // at every 32nd trigger
    const uint32_t osd_roc = (nCalls_ / 32) % 8; // loops over 0-7, changes every 32 triggers
    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(" read osd now is ") << read_osd_now << " roc " << osd_roc);

    ++count;
    ++nCalls_;
    
    const auto& fedsAndChannels = getFedsAndChannels();

    for (size_t ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const uint32_t fednumber = fedsAndChannels[ifed].first;
        std::vector<bool> channels_used(49, false); // channels 1-48
        for (uint32_t ich = 0; ich < fedsAndChannels[ifed].second.size(); ++ich)
            channels_used[fedsAndChannels[ifed].second[ich]] = true;

        if (dumps_)
            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("FED NUMBER ") << fednumber);
        const uint32_t vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface *fed = FEDInterface_[vmeBaseAddress];

        readTimer_.start();
        std::vector<uint32_t> scores = fed->getScores();
        readTimer_.stop();

        std::vector<uint32_t> osds(24, 0);
        if (checkReadback_ && read_osd_now) {
            osdTimer_.start();
            osds = fed->readOSDValues();
            osdTimer_.stop();
        }

        for (size_t fiber = 1; fiber <= 24; ++fiber) {
            const uint32_t chA = fiber * 2 - 1;
            const uint32_t chB = fiber * 2;

            const bool chAused = channels_used[chA];
            const bool chBused = channels_used[chB];

            if (!chAused && !chBused)
                continue;

            const uint32_t rocSizeA = (theNameTranslation_->getROCsFromFEDChannel(fednumber, chA)).size();
            const uint32_t rocSizeB = (theNameTranslation_->getROCsFromFEDChannel(fednumber, chB)).size();

            if (rocSizeA != rocSizeB)
                LOG4CPLUS_WARN(logger_, LOG4CPLUS_TEXT("ROC size A is different from B: should not happen?"));

            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(" fiber: ") << fiber << " chA: " << chA << " rocSizeA: " << rocSizeA << " chB: " << chB << " rocSizeB: " << rocSizeB);

            const uint32_t scoreA = scores[chA - 1];
            const uint32_t scoreB = scores[chB - 1];

            const uint32_t AOSD = osds[fiber - 1] & 0xFFFF;
            const uint32_t BOSD = osds[fiber - 1] >> 16;

            if (dumps_) {
                const uint32_t chs[2] = { chA, chB };
                const bool chused[2] = { chAused, chBused };
                const uint32_t scores[2] = { scoreA, scoreB };
                const uint32_t osds[2] = { AOSD, BOSD };
                std::ostringstream msg; msg << "fiber " << fiber << " scores: " << count << " " << nCalls_ << "\n";
                for (size_t i = 0; i < 2; ++i) {
                    if (chused[i])
                        msg << "ch " << std::setw(2) << chs[i] << ": wc DDDDDDDD rrrrrrrr RRRRRRRR TH\n"
                        << "       " << std::setw(2) << (scores[i] >> 26) << " "
                        << std::bitset<8>((scores[i] >> 18) & 0xFF) << " "
                        << std::bitset<8>((scores[i] >> 10) & 0xFF) << " "
                        << std::bitset<8>((scores[i] >> 2) & 0xFF) << " "
                        << std::bitset<2>(scores[i] & 0x3)
                        << "\n";
                    if (checkReadback_ && read_osd_now) {
                        msg << "fiber " << fiber << " osds:\n";
                        for (size_t i = 0; i < 2; ++i)
                            if (chused[i])
                                msg << "ch " << std::setw(2) << chs[i] << ": iiii rrrr oooooooo\n"
                                << "       "
                                << std::bitset<4>((osds[i] >> 12) & 0xF) << " "
                                << std::bitset<4>((osds[i] >>  8) & 0xF) << " "
                                << std::bitset<8>(osds[i] & 0xFF)
                                << "\n";
                    }
                }
                LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
            } // if dumps_

            // format is ccccccDDDDDDDDrrrrrrrrRRRRRRRRTH
            // c counts the number of words (0-63)
            // the D bits are one per roc, 1 if has the right pixel hit
            // r should all be 0 if no additional roc headers
            // R is whether that roc header was there
            // T for tbm trailer
            // H for tbm header
            bool AOK, BOK;
            uint32_t perfect_score = 0x4bfc03ff; // 18 words, 8 hits, 8 ROC headers, TBM header+trailer
            if (rocSizeA == 4)
                perfect_score = 0x283C003F; // 10 words, 4 hits, 4 ROC headers, TBM header+trailer
            else if (rocSizeA == 2)
                perfect_score = 0x180C000F; // 6 words, 2 hits, 2 ROC headers, TBM header+trailer

            const uint32_t perfect_score_ignore_wordcount = perfect_score & 0x3ffffff;
            const uint32_t perfect_score_headers_only = perfect_score & 0x3ff;

            if (headersOnly_) {
                // so perfect headers is (score & 0x3ffff) == 0x3ff
                AOK = (scoreA & 0x3ffff) == perfect_score_headers_only;
                BOK = (scoreB & 0x3ffff) == perfect_score_headers_only;
            } else if (ignoreWordCount_) {
                AOK = (scoreA & 0x3ffffff) == perfect_score_ignore_wordcount;
                BOK = (scoreB & 0x3ffffff) == perfect_score_ignore_wordcount;
            } else {
                AOK = scoreA == perfect_score;
                BOK = scoreB == perfect_score;
            }

            if (checkReadback_ && read_osd_now) {
                // can only use readback when OSD was armed for a ROC number that makes sense for this channel
                if (osd_roc < rocSizeA) {
                        // check we have the current ROC id
                    const bool AOSD_OK = ((AOSD >> 12) % rocSizeA == osd_roc)
                        // check last value written to register 255 corresponds to what is set in PixelTBMDelayCalibrationWithScoresBPIX::beginCalibration()
                        && (((AOSD >> 8) & 0xF) == 1);
                    AOK = AOK && AOSD_OK;
                    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("AOSD is ") << AOSD_OK);
                }
                if (osd_roc < rocSizeB) {
                    const bool BOSD_OK = ((BOSD >> 12) % rocSizeB == osd_roc)
                        && (((BOSD >> 8) & 0xF) == 1);
                    BOK = BOK && BOSD_OK;
                    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("BOSD is ") << BOSD_OK);
                }
            }

            if (!chAused)
                AOK = orChannels_ ? false : true;
            if (!chBused)
                BOK = orChannels_ ? false : true;

            const bool fiber_OK = orChannels_ ? (AOK || BOK) : (AOK && BOK);
            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(AOK) << " " << BOK << " " << rocSizeA << " " << hex << scoreA << " " << scoreB << " " << perfect_score << dec << " " << fiber_OK);

            FillEm(state, fiber_OK, fednumber, chA);
            FillEm(state, fiber_OK, fednumber, chB);
        }
    }

    otherTimer_.stop();
}

void PixelFEDTBMDelayCalibrationWithScoresBPIX::Analyze() {
    // maps per module
    std::map<std::string, std::pair<uint32_t, std::vector<uint32_t>> > crateAndFEDchannelsPerModule;
    
    const auto& fedsAndChannels = getFedsAndChannels();
    
    for (const auto& fedNumberAndChannels: fedsAndChannels) {
        const uint32_t fedNumber = fedNumberAndChannels.first;
        const uint32_t crate = theFEDConfiguration_->crateFromFEDNumber(fedNumber);
        
        for (const uint32_t channelNumber: fedNumberAndChannels.second) {
            pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(fedNumber, channelNumber);
            std::string moduleName = theChannel.modulename();
            crateAndFEDchannelsPerModule[moduleName].first = crate;
            crateAndFEDchannelsPerModule[moduleName].second.push_back(channelNumber);
        }
    }
    
    // normalize by number of triggers per point and fill histo with sum of channels
    for (std::map<uint32_t, std::map<uint32_t, TH2F*> >::iterator it1 = scanResults_.begin(); it1 != scanResults_.end(); ++it1) {
        pos::PixelChannel theChannel;
        for (std::map<uint32_t, TH2F*>::iterator it2 = it1->second.begin(); it2 != it1->second.end(); ++it2) {
            theChannel = theNameTranslation_->ChannelFromFEDChannel(it1->first, it2->first);
            for (int32_t xbin = 1; xbin <= it2->second->GetNbinsX(); xbin++) {
                for (int32_t ybin = 1; ybin <= it2->second->GetNbinsY(); ybin++) {
                    float nTriggers = nTrigsPerPoint_->GetBinContent(xbin, ybin);
                    if (nTriggers > 0)
                        it2->second->SetBinContent(xbin, ybin, it2->second->GetBinContent(xbin, ybin) / nTriggers);
                }
            }
            TBMsHistoSum_[theChannel.modulename()]->Add(it2->second);
        } //close loop on channels
    } //close loop on fed
    
    // to print summary and save it on text file
    string s = pixel::utils::to_string(crate_);
    ofstream out((outputDir() + "/summary_" + s + ".txt").c_str());
    assert(out.good()); //file method
    stringstream mout; mout << "\n";
    
    out <<  "Module                            | FED channels | New 160MHz delay | New TBM A/B delay | Old 160MHz delay | Old TBM A/B delay | Max eff. \n";
    mout << "Module                            | FED channels | New 160MHz delay | New TBM A/B delay | Old 160MHz delay | Old TBM A/B delay | Max eff. \n";
    
    // loop over modules, find best settings, write ASCII files
    for (auto &mod_hist: TBMsHistoSum_) {
        std::string moduleName = mod_hist.first;
        TH2F* hist = mod_hist.second;
        
        //normalize the sum histo by number of fed channels
        hist->Scale(1. / (crateAndFEDchannelsPerModule[moduleName].second.size()));
        
        // prevent the calibration to overwrite ASCII output files which are not of this FEDSupervisor's business.
        if (crate_ != crateAndFEDchannelsPerModule[moduleName].first)
            continue;
        
        const float zmax = hist->GetMaximum();
        
        float maxeff = -1;
        int32_t bestx = -1;
        int32_t besty = -1;
        const uint32_t nSettingsX = 8;
        const uint32_t nSettingsY = 8;
        assert(hist->GetNbinsX() == nSettingsX && hist->GetNbinsY() % nSettingsY == 0);
        
        // find new best values
        for (int32_t xbin = 1; xbin <= hist->GetNbinsX(); xbin++) {
            for (int32_t ybin = 1; ybin <= hist->GetNbinsY(); ybin++) {
                // only look at max. efficiency bins
                if (hist->GetBinContent(xbin, ybin) != zmax)
                    continue;
                
                // check neighboring bins
                float sum = 0.;
                
                for (int32_t ix = -1; ix <= 1; ix++) {
                    for (int32_t iy = -1; iy <= 1; iy++) {
                        if (ix == 0 && iy == 0)
                            continue;
                        
                        int32_t newx = xbin + ix;
                        int32_t newy = ybin + iy;
                        
                        if (newx == nSettingsX + 1)
                            newx = 1;
                        if (newx == 0)
                            newx = nSettingsX;
                        
                        if (ybin % nSettingsY == 0 && iy > 0)
                            newy -= nSettingsY;
                        else if (newy % nSettingsY == 0 && iy < 0)
                            newy += nSettingsY;
                        
                        sum += hist->GetBinContent(newx, newy);
                    }
                }
                
                if (sum > maxeff) {
                    maxeff = sum;
                    bestx = xbin;
                    besty = ybin;
                }
            }
        }
        
        pos::PixelTBMSettings *tbm = 0;
        PixelConfigInterface::get(tbm, "pixel/tbm/" + moduleName, *theGlobalKey_);
        assert(tbm != 0);
        
        const uint32_t tmba_dac_old = tbm->getTBMADelay();
        //uint32_t tmbb_dac_old = tbm->getTBMBDelay();
        const uint32_t tbmpll_dac_old = tbm->getTBMPLLDelay();
        //const uint32_t tbma_port0_old = tmba_dac_old & 0b111; // old ROC port delay
        const uint32_t ttbits_tbma_old = tmba_dac_old >> 6; // old TT bits
        
        out << std::setw(33) << std::left << moduleName << " | ";
        mout << std::setw(33) << std::left << moduleName << " | ";
        for (auto ch: crateAndFEDchannelsPerModule[moduleName].second)  {
            out << std::setw(2) << ch << " ";
            mout << std::setw(2) << ch << " ";
        }
        if (crateAndFEDchannelsPerModule[moduleName].second.size() < 4) {
            for (std::size_t r = 0; r < 4 - crateAndFEDchannelsPerModule[moduleName].second.size(); r++) {
                out << "   ";
                mout << "   ";
            }
        }
        
        if (bestx == -1 || besty == -1) {
            // no best bin found, keep previous settings
            out << " | " << std::setw(17) << -1 << " | " << std::setw(17) << -1;
            mout << " | " << std::setw(17) << -1 << " | " << std::setw(17) << -1;
        } else {
            const uint32_t tbmpll_new = bestx - 1; // new 160MHz setting
            const uint32_t final_tbmpll = (tbmpll_dac_old & 0b11100) + (tbmpll_new << 5); // new register, keeping existing 400MHz value
            
            tbm->setTBMPLLDelay(final_tbmpll);
            
            uint32_t ttbits_tbma_new; // new TT bits
            if (keepTTBits_)
                ttbits_tbma_new = ttbits_tbma_old;
            else
                ttbits_tbma_new = (besty - 1) / 8;
            
            const uint32_t tbma_port0_new = (besty - 1) % 8; // new ROC port delay
            const uint32_t roc_tbma_new = (ttbits_tbma_new << 6) + (tbma_port0_new << 3) + tbma_port0_new;
            
            tbm->setTBMADelay(roc_tbma_new);
            tbm->setTBMBDelay(roc_tbma_new);
            
            out << " | " << std::setw(16) << tbmpll_new;
            out << " | " << std::setw(17) << tbma_port0_new;
            mout << " | " << std::setw(16) << tbmpll_new;
            mout << " | " << std::setw(17) << tbma_port0_new;
        }
        
        out << " | " << std::setw(16) << ((tbmpll_dac_old >> 5) & 0b111);
        out << " | " << std::setw(17) << (tmba_dac_old & 0b111);
        out << " | " << zmax << std::endl;
        mout << " | " << std::setw(16) << ((tbmpll_dac_old >> 5) & 0b111);
        mout << " | " << std::setw(17) << (tmba_dac_old & 0b111);
        mout << " | " << zmax << std::endl;
        
        tbm->writeASCII(outputDir());
        delete tbm;
    } // end loop over modules and histos
    
    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(mout.str()));

    if (rootf_) {
        rootf_->Write();
        rootf_->Close();
        delete rootf_;
    }
}

void PixelFEDTBMDelayCalibrationWithScoresBPIX::BookEm() {
    auto bookHist = [this](TString title) -> TH2F* {
        TH2F* hist;

        if (keepTTBits_) {
            hist = new TH2F(title, title, 8,0,8, 8,0,8);
            hist->GetYaxis()->SetTitle("TBMA/BDelay");
        } else {
            hist = new TH2F(title, title, 8,0,8, 32,0,32);
            hist->GetYaxis()->SetTitle("TT bits + TBMA/BDelay");
            
            for (int32_t ybin = 1; ybin <= hist->GetYaxis()->GetNbins(); ybin++) {
                TString ylabel = "";
                
                if ((ybin - 1) / 8 == 0)
                    ylabel += "00";
                else if ((ybin - 1) / 8 == 1)
                    ylabel += "01";
                else if ((ybin - 1) / 8 == 2)
                    ylabel += "10";
                else if ((ybin - 1) / 8 == 3)
                    ylabel += "11";
                else {
                    LOG4CPLUS_ERROR(logger_, LOG4CPLUS_TEXT("NOT possible !"));
                }
                
                ylabel += ", ";
                ylabel += (ybin - 1) % 8;
                
                hist->GetYaxis()->SetBinLabel(ybin, ylabel);
            }
        }
        hist->GetXaxis()->SetTitle("160MHz phase");
        return hist;
    };
    
    scanResults_.clear();
    TBMsHistoSum_.clear();

    rootf_ = new TFile(TString::Format("%s/TBMDelay_%lu.root", outputDir().c_str(), crate_), "create");

    assert(rootf_->IsOpen());

    const auto& fedsAndChannels = getFedsAndChannels();
    std::set<pos::PixelChannel> channels = theNameTranslation_->getChannels();

    PixelRootDirectoryMaker rootDirs(fedsAndChannels, gDirectory);
    rootf_->cd(); // make sure we create the next directory structure from the file root
    PixelRootDirectoryMaker rootDirsModules(channels, gDirectory);

    // book one histograms per channel and one histogram per module, for sum of channels of that module
    for (const auto& fedNumberAndChannels: fedsAndChannels) {
        const uint32_t fedNumber = fedNumberAndChannels.first;
        std::map<uint32_t, TH2F*> histopack;

        for (const uint32_t channelNumber: fedNumberAndChannels.second) {
            rootDirs.cdDirectory(fedNumber, channelNumber);

            TString title = "FED";
            title += fedNumber;
            title += "_channel";
            title += channelNumber;
            title += "_TBMDelayVsTBMPLL";

            histopack[channelNumber] = bookHist(title);
            
            pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(fedNumber, channelNumber);
            rootDirsModules.cdDirectory(theChannel);
            const std::string moduleName = theChannel.modulename();
            if (TBMsHistoSum_.find(moduleName) != TBMsHistoSum_.end())
                continue;
            TBMsHistoSum_[moduleName] = bookHist(moduleName);
        }
        scanResults_[fedNumber] = histopack;
    }

    rootf_->cd(0);

    // book one histo to monitor number of triggers per point
    // depending on the scan range and on keepTTBits_, it is likely different from theCalibObject_->nTriggersPerPattern();
    if (keepTTBits_)
        nTrigsPerPoint_ = new TH2F("nTrigsPerPoint", "nTrigsPerPoint", 8, 0, 8, 8, 0, 8);
    else
        nTrigsPerPoint_ = new TH2F("nTrigsPerPoint", "nTrigsPerPoint", 8, 0, 8, 32, 0, 32);
}

void PixelFEDTBMDelayCalibrationWithScoresBPIX::GetCurrentScanValues(uint32_t& tbm160delay, uint32_t& rocdelay_port0, uint32_t& rocdelay_port1, uint32_t& ttBits, uint32_t state) {
    pos::PixelCalibConfiguration *tempCalibObject = getCalibObject();

    int32_t ival = -1;
    int32_t jval = -1;

    for (size_t i = 0; i < dacsToScan_.size(); ++i) {
        const std::string &name = dacsToScan_[i];
        if (name == "TBMPLL")
            ival = tempCalibObject->scanValue(name, state);
        else if (name == "TBMADelay" || name == "TBMBDelay")
            jval = tempCalibObject->scanValue(name, state);
    }
    assert(ival >= 0 && jval >= 0);

    tbm160delay = (ival >> 5) & 0b111;
    rocdelay_port0 = jval & 0b111;
    if (forcePort1EqualToPort0_)
        rocdelay_port1 = rocdelay_port0;
    else
        rocdelay_port1 = (jval >> 3) & 0b111;
    ttBits = (jval >> 6) & 0b11;
}

void PixelFEDTBMDelayCalibrationWithScoresBPIX::FillTrigger(uint32_t state) {
    uint32_t tbm160delay, rocdelay_port0, rocdelay_port1, ttBits;
    GetCurrentScanValues(tbm160delay, rocdelay_port0, rocdelay_port1, ttBits, state);
    nTrigsPerPoint_->Fill(tbm160delay, (keepTTBits_ ? 0 : 8 * ttBits) + rocdelay_port0);
}

void PixelFEDTBMDelayCalibrationWithScoresBPIX::FillEm(uint32_t state, float c, uint32_t fedid, uint32_t ch) {
    if (!theNameTranslation_->FEDChannelExist(fedid, ch)) {
        LOG4CPLUS_ERROR(logger_, LOG4CPLUS_TEXT("Trying to fill for FED ") << fedid << " and channel " << ch);
        return;
    }

    uint32_t tbm160delay, rocdelay_port0, rocdelay_port1, ttBits;
    GetCurrentScanValues(tbm160delay, rocdelay_port0, rocdelay_port1, ttBits, state);

    scanResults_[fedid][ch]->Fill(tbm160delay, (keepTTBits_ ? 0 : 8 * ttBits) + rocdelay_port0, c);
}
