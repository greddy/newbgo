#include "PixelCalibrations/include/PixelFEDCalDelCalibration.h"
#include "PixelUtilities/PixelFEDDataTools/include/Moments.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO1Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO2Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelFEDDataTypes.h"

#include "PixelUtilities/PixelFEDDataTools/include/PixelHit.h"

#include "pixel/utils/Utils.h"

#include "iomanip"
#include "TH2F.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TStyle.h"
#include "TTree.h"
#include "TStyle.h"


using namespace std;

PixelFEDCalDelCalibration::PixelFEDCalDelCalibration(const PixelFEDSupervisorConfiguration &tempConfiguration,
                                                     const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper) {
}

xoap::MessageReference PixelFEDCalDelCalibration::execute(xoap::MessageReference msg) {
    const bool lookAtErrors = false; ///????
    const uint state = event_ / tempCalibObject_->nTriggersPerPattern();

    const uint ithreshold = tempCalibObject_->scanCounter(name1_, state),
               icaldelay = tempCalibObject_->scanCounter(name2_, state);

    try {

        for (map<uint, set<uint> >::iterator fedAndChannel = fedsAndChannels_.begin(); fedAndChannel != fedsAndChannels_.end(); ++fedAndChannel) {
            const uint fednumber = fedAndChannel->first,
                       fedcrate = theFEDConfiguration_->crateFromFEDNumber(fednumber);
            if (fedcrate != crate_)
                continue;

            PixelPh1FEDInterface *iFED = FEDInterface_[theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber)];

            uint64_t buffer64[pos::fifo3Depth];
            const int status = iFED->spySlink64(buffer64); // read data

            if (status > 0) {
                // Get the number of hits
                if (Verbose)
                    cout << " Look at FIFO3 from FED " << fednumber << " in fedcrate " << fedcrate << endl;

                FIFO3Decoder decode(buffer64, Verbose); // decode data, look for errors
                const uint nhits = decode.nhits();
                if (Verbose && nhits > 0)
                    cout << " -----------> nhits:" << nhits << endl;

                for (uint ihit = 0; ihit < nhits; ihit++) {
                    const uint rocid = decode.rocid(ihit),
                               channel = decode.channel(ihit);

                    if (rocid == 0) {
                        cout << " ROC<=0 problem: fed " << fednumber << " Channel=" << channel
                                  << " rocid=" << rocid << " " << ihit << " " << nhits << endl;
                        cout << "Contents of Spy FIFO 3" << endl;
                        cout << "----------------------" << endl;
                        //PixelFEDFifoData::decodeSlink64(buffer64, status, (dataFIFO_[vmeBaseAddress]));
                        for (int i = 0; i <= status; ++i) {
                            const uint w1 = buffer64[i] & 0xffffffff,
                                       w2 = (buffer64[i] >> 32) & 0xffffffff;
                            pos::PixelHit h1(w1),
                                          h2(w2);

                            cout << "Clock " << i << " = 0x" << hex << buffer64[i]
                                      << " " << w1 << " " << w2 << " " << dec
                                      << h1.getLink_id() << " " << h1.getROC_id() << " " << h1.getDCol_id() << " " << h1.getPix_id() << " "
                                      << h2.getLink_id() << " " << h2.getROC_id() << " " << h2.getDCol_id() << " " << h2.getPix_id() << " "
                                      << endl;
                        } // for 2 words

                        assert(rocid > 0);

                    } else if (rocid > 8) { // for phase1 there are only 8 rocs 1-8
                        cout << " for fed " << fednumber << " Channel=" << channel
                                  << " rocid=" << rocid << " " << ihit << " " << nhits << " roc>7, skip this hit" << endl;
                        continue;

                    }

                    const pos::PixelROCName roc = theNameTranslation_->ROCNameFromFEDChannelROC(fednumber,
                                                                                     channel,
                                                                                     rocid - 1);

                    //cout << "roc:"<<roc.rocname()<<endl;

                    map<pos::PixelROCName, PixelEfficiency2DWBCCalDel>::iterator it = eff_.find(roc);

                    if (it != eff_.end())
                        it->second.add(icaldelay, ithreshold);
                    else {
                        cout << "Could not find ROC with fednumber=" << fednumber
                             << " channel=" << channel << " rocid=" << rocid << endl;
                    }
                }

                // Look at errors in the ErrorFIFO
                if (lookAtErrors && Verbose) { // for testing only
                    if (Verbose)
                        cout << " Look at error-FIFO for FED " << fednumber << " fedcrate " << fedcrate << endl;
                    uint32_t errBuffer[(36 * 1024)]; //max size?
                    const int errCount = iFED->drainErrorFifo(errBuffer);
                    if (errCount > 0) {
                        ErrorFIFODecoder errDecode(errBuffer, errCount);
                        errDecode.printToStream(cout);
                    }
                }

            } else {
                cout << "Error reading spySlink64 status=" << status << endl;
            } // uf status
        }     // for loop FEDs
    }
    catch (exception& e) {
        diagService_->reportError("*** Unknown exception occurred", DIAGWARN);
        // cout << "*** Unknown exception occurred" << endl;
    } // try

    ++event_;
    return makeSOAPMessageReference("ThresholdCalDelayDone");
}

void PixelFEDCalDelCalibration::initializeFED() {
    setFEDModeAndControlRegister(0x8, 0x30010);
}

xoap::MessageReference PixelFEDCalDelCalibration::beginCalibration(xoap::MessageReference msg) {
    tempCalibObject_ = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject_ != 0);
    Verbose = tempCalibObject_->parameterValue("Verbose") == "yes";
    if (Verbose)
        cout << " Verbose set ON" << endl;

    fedsAndChannels_ = tempCalibObject_->getFEDsAndChannels(theNameTranslation_);

    name1_ = tempCalibObject_->scanName(1);
    name2_ = tempCalibObject_->scanName(0);

    assert(tempCalibObject_->numberOfScanVariables() > 1);

    fract_ = 0.2;

    // Change the Fraction if it is defined in the claib.dat file. d.k.3/7/08
    if (tempCalibObject_->parameterValue("Fraction") != "") {
        fract_ = atof(tempCalibObject_->parameterValue("Fraction").c_str());
        cout << " Set the mean fraction to " << fract_ << endl;
    }

    const vector<pos::PixelROCName> ROCnames = tempCalibObject_->rocList();

    assert(ROCnames.size() > 0);

    const uint nThr = tempCalibObject_->nScanPoints(name1_),
               nCal = tempCalibObject_->nScanPoints(name2_);

    const double VcThrMin = tempCalibObject_->scanValueMin(name1_),
                 VcThrMax = tempCalibObject_->scanValueMax(name1_),
                 VcThrStep = tempCalibObject_->scanValueStep(name1_);

    const double CalDelMin = tempCalibObject_->scanValueMin(name2_),
                 CalDelMax = tempCalibObject_->scanValueMax(name2_),
                 CalDelStep = tempCalibObject_->scanValueStep(name2_);

    for (vector<pos::PixelROCName>::const_iterator iROC = ROCnames.begin(); iROC < ROCnames.end(); ++iROC) {
        pos::PixelModuleName module(iROC->rocname());
        if (crate_ == theFEDConfiguration_->crateFromFEDNumber(theNameTranslation_->firstHdwAddress(module).fednumber())) {
            PixelEfficiency2DWBCCalDel tmp(iROC->rocname(), name2_, nCal,
                                           CalDelMin - 0.5 * CalDelStep,
                                           CalDelMax + 0.5 * CalDelStep,
                                           name1_, nThr,
                                           VcThrMin - 0.5 * VcThrStep,
                                           VcThrMax + 0.5 * VcThrStep);
            eff_[*iROC] = tmp;
        }
    }

    return makeSOAPMessageReference("BeginCalibrationDone");
}

xoap::MessageReference PixelFEDCalDelCalibration::endCalibration(xoap::MessageReference msg) {
    //First we need to get the DAC settings for the ROCs

    const vector<pos::PixelModuleName> modules = theDetectorConfiguration_->getModuleList();

    map<pos::PixelModuleName, pos::PixelDACSettings *> theDACs;

    for (vector<pos::PixelModuleName>::const_iterator module_name = modules.begin(); module_name != modules.end(); ++module_name) {

        if (crate_ != theFEDConfiguration_->crateFromFEDNumber(theNameTranslation_->firstHdwAddress(*module_name).fednumber()))
            continue;

        pos::PixelDACSettings *tempDACs = 0;

        PixelConfigInterface::get(tempDACs, "pixel/dac/" + module_name->modulename(), *theGlobalKey_);
        assert(tempDACs != 0);
        theDACs[*module_name] = tempDACs;
    }

    vector<int> CalDel;
    vector<string> rocsname;

    outputFile_ = new TFile((outputDir() + "/CalDel_" + pixel::utils::to_string(crate_) + ".root").c_str(), "recreate",
                            ("CalDel_" + pixel::utils::to_string(crate_) + ".root").c_str());

    branch theBranch;
    branch_sum theBranch_sum;
    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("PassState", "PassState");
    TTree *tree_sum = new TTree("SummaryInfo", "SummaryInfo");

    tree->Branch("PassState", &theBranch, "pass/F:rocName/C", 4096000);
    tree_sum->Branch("SummaryInfo", &theBranch_sum, "new_CalDel/F:delta_CalDel/F:rocName/C", 4096000);
    outputFile_->cd();

    for (map<pos::PixelROCName, PixelEfficiency2DWBCCalDel>::iterator it = eff_.begin(), itend = eff_.end(); it != itend; ++it)
        rocsname.push_back(it->first.rocname());

    PixelRootDirectoryMaker rootDirs(rocsname, gDirectory);

    for (map<pos::PixelROCName, PixelEfficiency2DWBCCalDel>::iterator it = eff_.begin(), itend = eff_.end(); it != itend; ++it) {
        string rocsname = it->first.rocname();

        theBranch.pass = 0;
        strcpy(theBranch.rocName, rocsname.c_str());
        strcpy(theBranch_sum.rocName, rocsname.c_str());

        rootDirs.cdDirectory(rocsname);

        //FIXME slow way to make a module ame
        pos::PixelModuleName module(it->first.rocname());

        double WBC = theDACs[module]->getDACSettings(it->first)->getWBC();

        it->second.findSettings(tempCalibObject_->nTriggersPerPattern() *
                                tempCalibObject_->nPixelPatterns(),
                                WBC, fract_);

        int calDel = (int)it->second.getCalDel();

        if (it->second.validSlope()) {
            // additinal protection in case the slope did not catch the problem it
            if (calDel <= 0 || calDel > 255) {
                cout << " CalDel = 0 " << it->second.validSlope() << " " << rocsname << " " << calDel
                     << " use old value " << theDACs[module]->getDACSettings(it->first)->getCalDel() << endl;
            } else {
                theBranch_sum.new_CalDel = (float)calDel;

                int old_calDel = theDACs[module]->getDACSettings(it->first)->getCalDel();

                theBranch_sum.delta_CalDel = (float)(old_calDel - calDel);

                theDACs[module]->getDACSettings(it->first)->setCalDel(calDel);

                CalDel.push_back(calDel);

                theBranch.pass = 1;

                tree->Fill();

                tree_sum->Fill();

                //cout<<it->second.validSlope() <<" "<<rocsname<<" "<<calDel<<" old value "<<old_calDel<<endl;
            }

        } else
            cout << " Failed for " << rocsname << " " << calDel << " use old value " << (int)theDACs[module]->getDACSettings(it->first)->getCalDel() << endl;


        tree->Fill();

        TCanvas *canvas = new TCanvas((rocsname + "_c").c_str(), (rocsname + "_c").c_str(), 800, 600);

        TH2F histo2D = it->second.FillEfficiency(tempCalibObject_->nTriggersPerPattern() * tempCalibObject_->nPixelPatterns());

        histo2D.GetXaxis()->SetTitle(name2_.c_str());
        histo2D.GetYaxis()->SetTitle(name1_.c_str());

        gStyle->SetOptStat("ne");
        histo2D.Draw("colz");
        if (it->second.validSlope()) {
            TLine *determinedCalDel = new TLine(calDel, it->second.getmin2(), calDel, it->second.getmax2());
            TLine *setWBC = new TLine(0, WBC, 250, WBC);
            determinedCalDel->SetLineColor(kMagenta);
            determinedCalDel->SetLineStyle(kDashed);
            setWBC->SetLineColor(kRed);
            setWBC->SetLineStyle(kDashed);
            determinedCalDel->Draw("same");
            setWBC->Draw("same");
        }
        canvas->Write();
    }

    outputFile_->cd();

    TCanvas *canvas1 = new TCanvas("summary_c", "summary_c", 800, 600);

    TH1F *histo_CalDel = new TH1F("summary", "summary", 256, 0, 255);

    for (vector<int>::iterator it = CalDel.begin(), itend = CalDel.end(); it != itend; ++it)
        histo_CalDel->Fill(*(it));

    histo_CalDel->Draw();
    histo_CalDel->GetXaxis()->SetTitle("CalDel");
    canvas1->Write();

    outputFile_->Write();
    outputFile_->Close();

    for (map<pos::PixelModuleName, pos::PixelDACSettings *>::iterator dacs = theDACs.begin(); dacs != theDACs.end(); ++dacs)
        dacs->second->writeASCII(outputDir());

    cout << "In PixelFEDCalDelCalibration::endCalibration()" << endl;
    return makeSOAPMessageReference("EndCalibrationDone");
}
