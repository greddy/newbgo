#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelFEDIanaCalibrationBPIX.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelFEDDataTypes.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ColRowAddrDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigScopeDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigTransDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"
#include "PixelCalibrations/include/PixelIanaAnalysisBPIX.h"
#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"

#include "TH1F.h"
#include "TH2F.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TFile.h"
#include "TLine.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TTree.h"

#include <iomanip>
#include <algorithm>

// This class include readout and analysis for Iana, Vdig, Vana, Va, Vbg. However only Iana is used.
// The V* calibrations are all done in another class PixelFEDVClibrationBPIX.

///////////////////////////////////////////////////////////////////////////////////////////////
PixelFEDIanaCalibrationBPIX::PixelFEDIanaCalibrationBPIX(const PixelFEDSupervisorConfiguration &tempConfiguration, const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper), rootf(0) {
                                                                   std::cout << "In PixelFEDIanaCalibrationBPIX copy ctor()" << std::endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::initializeFED() {
    //setFEDModeAndControlRegister(0x8, 0x30010);

    cout << " Disable back-end in order to avoid spyFIFO1 filling " << endl;
    for (FEDInterfaceMap::iterator iFED = FEDInterface_.begin(); iFED != FEDInterface_.end();
         ++iFED) {
        iFED->second->disableBE(true);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDIanaCalibrationBPIX::beginCalibration(xoap::MessageReference msg) {
    std::cout << "In PixelFEDIanaCalibrationBPIX::beginCalibration()" << std::endl;

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    tempCalibObject->writeASCII(outputDir());

    vector<pos::PixelModuleName>::const_iterator module_name = theDetectorConfiguration_->getModuleList().begin();
    for (; module_name != theDetectorConfiguration_->getModuleList().end(); ++module_name) {
        // First we need to get the DAC settings for the ROCs on this module.
        pos::PixelDACSettings *dacs = 0;
        string modulePath = module_name->modulename();
        PixelConfigInterface::get(dacs, "pixel/dac/" + modulePath, *theGlobalKey_);
        assert(dacs != 0);
        dacsettings_[*module_name] = dacs;
    }

    for (unsigned dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        std::vector<unsigned int> dacvals = tempCalibObject->scanValues(dacname);
        if (dacvals.size() > 1)
            dacsToScan.push_back(dacname);

        for (unsigned int i = 0; i < dacvals.size(); ++i)
            std::cout << " dac value " << i << " is " << dacvals[i] << std::endl;
    }

    string init_str = "unknown";
    if (tempCalibObject->mode() == "IanaBPIX")
        init_str = "IanaBPIX calibration";
    else if (tempCalibObject->mode() == "VdigBPIX")
        init_str = "VdigBPIX calibration";
    else if (tempCalibObject->mode() == "VaBPIX")
        init_str = "VaBPIX calibration";
    else if (tempCalibObject->mode() == "VbgBPIX")
        init_str = "VbgBPIX calibration";
    else if (tempCalibObject->mode() == "VanaBPIX")
        init_str = "VanaBPIX calibration";

    BookEm("");

    return makeSOAPMessageReference("BeginCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDIanaCalibrationBPIX::execute(xoap::MessageReference msg) {
    vector<string> parameters_names(2);
    parameters_names.push_back( "WhatToDo");
    parameters_names.push_back( "StateNum");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, parameters_names);

    const unsigned state = atoi(parameters[ "StateNum"].c_str());

    if (parameters[ "WhatToDo"] == "RetrieveData")
        RetrieveData(state);
    else if (parameters[ "WhatToDo"] == "Analyze")
        Analyze();
    else {
        cout << "ERROR: PixelFEDIanaCalibrationBPIX::execute() does not understand the WhatToDo command, " << parameters[ "WhatToDo"] << ", sent to it.\n";
        assert(0);
    }

    return makeSOAPMessageReference("FEDCalibrationsDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDIanaCalibrationBPIX::endCalibration(xoap::MessageReference msg) {

    std::cout << "In PixelFEDIanaCalibrationBPIX::endCalibration()" << std::endl;
    return makeSOAPMessageReference("EndCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::RetrieveData(unsigned state) {
    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    //lastDACValue = 0;

    std::map<std::string, unsigned int> currentDACValues;
    for (unsigned dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        const unsigned dacvalue = tempCalibObject->scanValue(tempCalibObject->scanName(dacnum), state);
        currentDACValues[dacname] = dacvalue;
    }
    if (tempCalibObject->mode() == "IanaBPIX" && currentDACValues["Vana"] != lastDACValue) {
        event_ = 0;
        lastDACValue = currentDACValues["Vana"];
        last_dacs.clear();
    }
    if (tempCalibObject->mode() == "VdigBPIX" && currentDACValues["Vdd"] != lastDACValue) {
        event_ = 0;
        lastDACValue = currentDACValues["Vdd"];
        last_dacs.clear();
    }
    if (tempCalibObject->mode() == "VaBPIX" && currentDACValues["Vana"] != lastDACValue) {
        event_ = 0;
        lastDACValue = currentDACValues["Vana"];
        last_dacs.clear();
    }
    if (tempCalibObject->mode() == "VbgBPIX" && currentDACValues["Vana"] != lastDACValue) {
        event_ = 0;
        lastDACValue = currentDACValues["Vana"];
        last_dacs.clear();
    }
    if (tempCalibObject->mode() == "VanaBPIX" && currentDACValues["Vana"] != lastDACValue) {
        event_ = 0;
        lastDACValue = currentDACValues["Vana"];
        last_dacs.clear();
    }

    const std::vector<std::pair<unsigned, std::vector<unsigned> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);

    for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {

        const unsigned fednumber = fedsAndChannels[ifed].first;
        const unsigned long vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        //PixelFEDInterface* iFED = FEDInterface_[vmeBaseAddress];
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];

        for (unsigned int ch = 0; ch < fedsAndChannels[ifed].second.size(); ch++) {

            unsigned int channel = (fedsAndChannels[ifed].second)[ch];
            //***
            if (channel % 2 == 1) {
                int fib = (channel - 1) / 2;
                iFED->setFIFO1(fib);
                PixelPh1FEDInterface::digfifo1 d = iFED->readFIFO1(false);
                // printf("ch a: %i ch b: %i n tbm h a: %i b: %i  tbm t a: %i b: %i  roc h a: %i b: %i\n", d.a.ch, d.b.ch, d.a.n_tbm_h, d.b.n_tbm_h, d.a.n_tbm_t, d.b.n_tbm_t, d.a.n_roc_h, d.b.n_roc_h);
                unsigned int chA = d.a.ch;
                unsigned int chB = d.b.ch;

                if ((channel != chA) || ((channel + 1) != chB))
                    continue;

                //**********
                //cout << "ROCs A " << endl;
                for (unsigned int i = 0; i < d.a.rocs.size(); i++) {
                    //cout << i << ": d.a.rocs.roc " << d.a.rocs[i].roc << " d.a.rocs.rb " << d.a.rocs[i].rb << endl;
                    unsigned int decCh = d.a.rocs[i].ch; // roc.ch
                    if (decCh != channel)
                        continue;
                    uint32_t mk = d.a.rocs[i].roc; //roc.roc
                    uint32_t f8 = d.a.rocs[i].rb;  //roc.rb
                    if (mk <= 8) {
                        last_dacs[fedsAndChannels[ifed].first][channel][mk].push_back(f8);
                    }
                }
                //cout << "ROCs B " << endl;
                for (unsigned int i = 0; i < d.b.rocs.size(); i++) {
                    //cout << i << ": d.b.rocs.roc " << d.b.rocs[i].roc << " d.b.rocs.rb " << d.b.rocs[i].rb << endl;
                    unsigned int decCh = d.b.rocs[i].ch; // roc.ch
                    if (decCh != (channel + 1))
                        continue;
                    uint32_t mk = d.b.rocs[i].roc; //roc.roc
                    uint32_t f8 = d.b.rocs[i].rb;  //roc.rb
                    if (mk <= 8) {
                        last_dacs[fedsAndChannels[ifed].first][channel + 1][mk].push_back(f8);
                    }
                }

            } //close if channel%2
        }     // close loop on channels

    } // close loop on feds

    if (event_ == tempCalibObject->nTriggersPerPattern() - 1) {

        if (tempCalibObject->mode() == "IanaBPIX") {
            ReadIana();
        } else if (tempCalibObject->mode() == "VdigBPIX")
            ReadVdig();
        else if (tempCalibObject->mode() == "VaBPIX")
            ReadVdig();
        else if (tempCalibObject->mode() == "VbgBPIX")
            ReadVdig();
        else if (tempCalibObject->mode() == "VanaBPIX")
            ReadVdig();
    }

    event_++;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::Analyze() {
    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    std::string mode = tempCalibObject->mode();

    if (tempCalibObject->mode() == "IanaBPIX")
        IanaAnalysis(tempCalibObject);
    else if (tempCalibObject->mode() == "VdigBPIX")
        VdigAnalysis(tempCalibObject);
    else if (tempCalibObject->mode() == "VaBPIX")
        VaAnalysis(tempCalibObject);
    else if (tempCalibObject->mode() == "VbgBPIX")
        VbgAnalysis(tempCalibObject);
    else if (tempCalibObject->mode() == "VanaBPIX")
        VanaAnalysis(tempCalibObject);
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::IanaAnalysis(pos::PixelCalibConfiguration *tmpCalib) {

  const bool print = false;
  ofstream out((outputDir() + "/iana.dat").c_str()); //leave the file method intact for now
  assert(out.good());                                //file method

    branch theBranch;
    branch_sum theBranch_sum;
    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    string cmd = "";

    TTree *tree = new TTree("PassState", "PassState");
    TTree *tree_sum = new TTree("SummaryInfo", "SummaryInfo");

    tree->Branch("PassState", &theBranch, "pass/F:efficiency/F:rocName/C", 4096000);
    tree_sum->Branch("SummaryInfo", &theBranch_sum, 
		     "deltaVana/F:newVana/F:newIana/F:fitChisquare/F:oldVana/F:oldIana/F:adc/F:pass/F:rocName/C", 4096000);

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tmpCalib->rocList(), gDirectory);

    for (std::map<int, std::map<int, std::map<int, std::map<int, int> > > >::iterator it = values.begin(); it != values.end(); ++it) {

      for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 = (it->second).begin(); it2 != (it->second).end(); ++it2) {

	const std::vector<pos::PixelROCName> &rocs = theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
	pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);
	
	static std::string directory;
	directory = getenv("PIXELCONFIGURATIONBASE");
	std::ifstream in((directory + "/iana/0/ROC_Iana_" + theChannel.modulename() + ".dat").c_str()); // open iana file
	//std::cout<<"Open "<<theChannel.modulename()<<std::endl; //d.k.
	for (std::map<int, std::map<int, int> >::iterator it3 = (it2->second).begin(); it3 != (it2->second).end(); ++it3) {
	  
	  if (theNameTranslation_->ROCNameFromFEDChannelROCExists(it->first, it2->first, it3->first - 1)) {
	    
	    pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(it->first, it2->first, it3->first - 1);
	    pos::PixelModuleName theModule(theROC.rocname());
	    
	    if(print) std::cout<<"for module "<<theModule.modulename()<<" for roc "<<theROC.rocname(); // d.k.
	    std::string tmp;
	    std::string tag;
	    float par0ia, par1ia, par2ia;
	    bool rocFound=false;
	    while(!rocFound || (in.good() && !in.eof()) ) { // loop until roc found or end of file 
		in >> tag; //ROC
		if(tag != "ROC:") std::cout<<"PixelFEDIanaCalibrationBPIX:IanaAnalysis - iana file corrupted "<<tag<<std::endl;
		in >> tmp;
		//std::cout << " ROC: " << tmp<<" "; //d.k.
		if( tmp != theROC.rocname() ) { // skip wrong rocs 
		  in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;
		  in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;  
		  continue; 
		} 
		if(print) std::cout<<" found roc "<<tmp<<std::endl;
		rocFound=true;
		in >> tag; //par0vd
		in >> tmp;
		in >> tag; //par1vd
		in >> tmp;
		in >> tag; //par0va
		in >> tmp;
		in >> tag; //par1va
		in >> tmp;
		in >> tag; //par0rbia
		in >> tmp;
		in >> tag; //par1rbia
		in >> tmp;
		in >> tag; //par0tbia
		in >> tmp;
		in >> tag; //par1tbia
		in >> tmp;
		in >> tag; //par2tbia
		in >> tmp;
		in >> tag; //par0ia
		in >> tmp;
		par0ia = atof(tmp.c_str());
		in >> tag; //par1ia
		in >> tmp;
		par1ia = atof(tmp.c_str());
		in >> tag; //par2ia
		in >> tmp;
		par2ia = atof(tmp.c_str());

		if(print) std::cout << " par0ia " << par0ia << " par1ia " << par1ia << " par2ia " << par2ia<<std::endl; //d.k.
		if (par1ia < 0.5 || par1ia > 10.0) {
		  std::cout << "WARNING: " << theROC.rocname() << " par1ia = " << par1ia << " unusual.\n";
		}
		break;

	    } // end while
 
	    if(!rocFound) {std::cout<<"ROC "<<theROC.rocname()<<" was not found, skip analysis "<<std::endl; continue;}

	    theBranch.pass = 0;
	    strcpy(theBranch.rocName, theROC.rocname().c_str());
	    strcpy(theBranch_sum.rocName, theROC.rocname().c_str());
	    
	    TF1 *froc = new TF1("froc", "[0] + [1]*x + [2]*x*x", 0, 200);
	    froc->SetParameters(par0ia, par1ia, par2ia);
	    
	    int npoints = it3->second.size();
	    std::vector<double> x(npoints + 1), y(npoints + 1), ey(npoints + 1);
	    
	    int j = 0;
	    for (std::map<int, int>::iterator it4 = (it3->second).begin(); it4 != (it3->second).end(); ++it4) {
	      
	      if (rocids[it->first][it2->first][it3->first][it4->first] == rocs[it3->first - 1].roc() &&
		  rbreg[it->first][it2->first][it3->first][it4->first] == 12) {
		y[j] = froc->GetX(it4->second); //convert ADC to Iana 
		x[j] = it4->first; // Vana in DAC units 
		ey[j] = 1.; // error
		j++;
		//if(print) {
		//std::cout << " ROC#" << rocs[it3->first-1].roc() 
		//	    << " par0 " << par0ia << " par1 " << par1ia << " par2 " << par2ia;
		//std::cout << " vana " << it4->first << std::endl;
		//std::cout << " iana(ADC) " << it4->second << " iana(mA) " << froc->GetX(it4->second) << std::endl;
		//}
	      }
	      
	    } //close loop on vana values
	    npoints = j;
	    
	    const int oldVana = dacsettings_[theModule]->getDACSettings(theROC)->getVana();
	    
	    rootDirs.cdDirectory(theROC);
	    PixelIanaAnalysisBPIX analysis(true);
	    
	    string filename = outputDir().c_str();
	    filename += "/";
	    filename += theROC.rocname();
	    filename += ".gif";
	    
	    int npoints_total = (it3->second).size();
	    analysis.goIana(theROC.rocname(), oldVana, npoints, x, y, ey);
	    
	    //theBranch_sum.maxIana = analysis.maxIana;
	    theBranch_sum.fitChisquare = analysis.fitChisquare;
	    theBranch_sum.newVana = analysis.newVana;
	    theBranch_sum.deltaVana = analysis.newVana - oldVana;
	    theBranch_sum.newIana = analysis.newIana;
	    theBranch_sum.oldVana = oldVana;
	    theBranch_sum.oldIana = analysis.oldIana;
	    theBranch_sum.pass = analysis.pass;
	    theBranch_sum.pass = analysis.pass;
	    theBranch.pass = analysis.pass;
	    theBranch.efficiency = (float)npoints / (float)npoints_total;
	    
	    float adc = froc->Eval(analysis.oldIana); // convert to ADC units 
	    theBranch_sum.adc = adc;

	    if(print) std::cout<<"ROC "<<theROC.rocname()<<" oldVana "<<oldVana<<" oldIana "<<analysis.oldIana
			       <<" newVana "<<analysis.newVana<<" newIana "<<analysis.newIana<<" old adc "<<adc<<std::endl;
	    out<<"ROC "<<theROC.rocname()<<" oldVana "<<oldVana<<" oldIana "<<analysis.oldIana
	       <<" newVana "<<analysis.newVana<<" newIana "<<analysis.newIana<<" old adc "<<adc<<std::endl;
	    
	    tree->Fill();
	    tree_sum->Fill();
	    
	    unsigned int newdac = analysis.newVana;
	    dacsettings_[theModule]->getDACSettings(theROC)->setVana(newdac);
	    
	  } //close if roc exists
	  
	} //close loop on rocs
	
	in.close();
	//std::cout<<"Close iana file "<<theChannel.modulename()<<std::endl; //d.k.
	
      } //close for2
    }  //close for 1
    
    std::vector<pos::PixelModuleName> modules = theDetectorConfiguration_->getModuleList();
    for (std::map<pos::PixelModuleName, pos::PixelDACSettings *>::const_iterator idacs = dacsettings_.begin(); idacs != dacsettings_.end(); ++idacs) {
        //prevent the calibration to overwrite ASCII output files which are not of this FEDSupervisor's business¬
        std::string moduleName = idacs->first.modulename();
        std::vector<pos::PixelModuleName>::iterator m;
        for (m = modules.begin(); m != modules.end(); m++)
            if (m->modulename() == moduleName)
                if (crate_ != theFEDConfiguration_->crateFromFEDNumber(theNameTranslation_->firstHdwAddress(*m).fednumber()))
                    continue;
        //end - prevent
        idacs->second->writeASCII(outputDir());
    }

    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::VdigAnalysis(pos::PixelCalibConfiguration *tmpCalib) {

    string cmd = "";

    branch_sum_vdig theBranch;
    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("Summary", "Summary");
    tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:rocName/C", 4096000);

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tmpCalib->rocList(), gDirectory);

    for (std::map<int, std::map<int, std::map<int, std::map<int, int> > > >::iterator it = values.begin(); it != values.end(); ++it) {

        for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 = (it->second).begin(); it2 != (it->second).end(); ++it2) {

            const std::vector<pos::PixelROCName> &rocs = theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
            pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);

            static std::string directory;
            directory = getenv("PIXELCONFIGURATIONBASE");

            std::ifstream in((directory + "/iana/0/ROC_Iana_" + theChannel.modulename() + ".dat").c_str());

            for (std::map<int, std::map<int, int> >::iterator it3 = (it2->second).begin(); it3 != (it2->second).end(); ++it3) {

                if (theNameTranslation_->ROCNameFromFEDChannelROCExists(it->first, it2->first, it3->first - 1)) {

                    pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(it->first, it2->first, it3->first - 1);
                    pos::PixelModuleName theModule(theROC.rocname());

                    std::string tmp;
                    std::string tag;
                    float par0vd, par1vd;
                    in >> tag; //ROC
                    in >> tmp;
                    //std::cout << " ROC: " << tmp;
                    in >> tag; //par0vd
                    in >> tmp;
                    par0vd = atof(tmp.c_str());
                    in >> tag; //par1vd
                    in >> tmp;
                    par1vd = atof(tmp.c_str());
                    in >> tag; //par0va
                    in >> tmp;
                    in >> tag; //par1va
                    in >> tmp;
                    in >> tag; //par0rbia
                    in >> tmp;
                    in >> tag; //par1rbia
                    in >> tmp;
                    in >> tag; //par0tbia
                    in >> tmp;
                    in >> tag; //par1tbia
                    in >> tmp;
                    in >> tag; //par2tbia
                    in >> tmp;
                    in >> tag; //par0ia
                    in >> tmp;
                    in >> tag; //par1ia
                    in >> tmp;
                    in >> tag; //par2ia
                    in >> tmp;
                    std::cout << " par0vd " << par0vd << " par1vd " << par1vd << std::endl;

                    strcpy(theBranch.rocName, theROC.rocname().c_str());
                    rootDirs.cdDirectory(theROC);
                    TH1F *hVdig = new TH1F(theROC.rocname().c_str(), theROC.rocname().c_str(), 1000, 0, 10);
                    TF1 *froc = new TF1("froc", "[0] + [1]*x", 0, 200);
                    froc->SetParameters(par0vd, par1vd);
                    for (std::map<int, int>::iterator it4 = (it3->second).begin(); it4 != (it3->second).end(); ++it4) {

                        if (rocids[it->first][it2->first][it3->first][it4->first] == rocs[it3->first - 1].roc() &&
                            rbreg[it->first][it2->first][it3->first][it4->first] == 8) {

                            hVdig->Fill(froc->GetX(it4->second));
                        }

                    } //close loop on vdig values

                    delete froc;

                    hVdig->Draw("HIST");
                    hVdig->Write();

                    theBranch.Mean = hVdig->GetMean();
                    theBranch.RMS = hVdig->GetRMS();

                    tree->Fill();
                }

            } //close loop on rocs per fed channel
            in.close();
        } //close loop on fed channels
    }     //close loop on feds

    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::VaAnalysis(pos::PixelCalibConfiguration *tmpCalib) {

    string cmd = "";

    branch_sum_vdig theBranch;
    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("Summary", "Summary");
    tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:rocName/C", 4096000);

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tmpCalib->rocList(), gDirectory);

    for (std::map<int, std::map<int, std::map<int, std::map<int, int> > > >::iterator it = values.begin(); it != values.end(); ++it) {

        for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 = (it->second).begin(); it2 != (it->second).end(); ++it2) {

            const std::vector<pos::PixelROCName> &rocs = theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
            pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);

            static std::string directory;
            directory = getenv("PIXELCONFIGURATIONBASE");

            std::ifstream in((directory + "/iana/0/ROC_Iana_" + theChannel.modulename() + ".dat").c_str());

            for (std::map<int, std::map<int, int> >::iterator it3 = (it2->second).begin(); it3 != (it2->second).end(); ++it3) {

                if (theNameTranslation_->ROCNameFromFEDChannelROCExists(it->first, it2->first, it3->first - 1)) {

                    pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(it->first, it2->first, it3->first - 1);
                    pos::PixelModuleName theModule(theROC.rocname());

                    std::string tmp;
                    std::string tag;
                    float par0va, par1va;
                    in >> tag; //ROC
                    in >> tmp;
                    //std::cout << " ROC: " << tmp;
                    in >> tag; //par0vd
                    in >> tmp;
                    in >> tag; //par1vd
                    in >> tmp;
                    in >> tag; //par0va
                    in >> tmp;
                    par0va = atof(tmp.c_str()); //stof(tmp);
                    in >> tag;                  //par1va
                    in >> tmp;
                    par1va = atof(tmp.c_str()); //stof(tmp);
                    in >> tag;                  //par0rbia
                    in >> tmp;
                    in >> tag; //par1rbia
                    in >> tmp;
                    in >> tag; //par0tbia
                    in >> tmp;
                    in >> tag; //par1tbia
                    in >> tmp;
                    in >> tag; //par2tbia
                    in >> tmp;
                    in >> tag; //par0ia
                    in >> tmp;
                    in >> tag; //par1ia
                    in >> tmp;
                    in >> tag; //par2ia
                    in >> tmp;
                    std::cout << " par0va " << par0va << " par1va " << par1va << std::endl;

                    strcpy(theBranch.rocName, theROC.rocname().c_str());
                    rootDirs.cdDirectory(theROC);
                    TH1F *hVa = new TH1F(theROC.rocname().c_str(), theROC.rocname().c_str(), 1000, 0, 10);
                    TF1 *froc = new TF1("froc", "[0] + [1]*x", 0, 200);
                    froc->SetParameters(par0va, par1va);
                    for (std::map<int, int>::iterator it4 = (it3->second).begin(); it4 != (it3->second).end(); ++it4) {

                        if (rocids[it->first][it2->first][it3->first][it4->first] == rocs[it3->first - 1].roc() &&
                            rbreg[it->first][it2->first][it3->first][it4->first] == 9) {

                            hVa->Fill(froc->GetX(it4->second));
                        }

                    } //close loop on vdig values

                    delete froc;

                    hVa->Draw("HIST");
                    hVa->Write();

                    theBranch.Mean = hVa->GetMean();
                    theBranch.RMS = hVa->GetRMS();

                    tree->Fill();
                }

            } //close loop on rocs per fed channel
            in.close();
        } //close loop on fed channels
    }     //close loop on feds

    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::VbgAnalysis(pos::PixelCalibConfiguration *tmpCalib) {

    string cmd = "";

    branch_sum_vdig theBranch;
    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("Summary", "Summary");
    tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:rocName/C", 4096000);

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tmpCalib->rocList(), gDirectory);

    for (std::map<int, std::map<int, std::map<int, std::map<int, int> > > >::iterator it = values.begin(); it != values.end(); ++it) {

        for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 = (it->second).begin(); it2 != (it->second).end(); ++it2) {

            const std::vector<pos::PixelROCName> &rocs = theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
            pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);

            static std::string directory;
            directory = getenv("PIXELCONFIGURATIONBASE");

            std::ifstream in((directory + "/iana/0/ROC_Iana_" + theChannel.modulename() + ".dat").c_str());

            for (std::map<int, std::map<int, int> >::iterator it3 = (it2->second).begin(); it3 != (it2->second).end(); ++it3) {

                if (theNameTranslation_->ROCNameFromFEDChannelROCExists(it->first, it2->first, it3->first - 1)) {

                    pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(it->first, it2->first, it3->first - 1);
                    pos::PixelModuleName theModule(theROC.rocname());

                    std::string tmp;
                    std::string tag;
                    float par0va, par1va;
                    in >> tag; //ROC
                    in >> tmp;
                    //std::cout << " ROC: " << tmp;
                    in >> tag; //par0vd
                    in >> tmp;
                    in >> tag; //par1vd
                    in >> tmp;
                    in >> tag; //par0va
                    in >> tmp;
                    par0va = atof(tmp.c_str()); //stof(tmp);
                    in >> tag;                  //par1va
                    in >> tmp;
                    par1va = atof(tmp.c_str()); //stof(tmp);
                    in >> tag;                  //par0rbia
                    in >> tmp;
                    in >> tag; //par1rbia
                    in >> tmp;
                    in >> tag; //par0tbia
                    in >> tmp;
                    in >> tag; //par1tbia
                    in >> tmp;
                    in >> tag; //par2tbia
                    in >> tmp;
                    in >> tag; //par0ia
                    in >> tmp;
                    in >> tag; //par1ia
                    in >> tmp;
                    in >> tag; //par2ia
                    in >> tmp;
                    std::cout << " par0va " << par0va << " par1va " << par1va << std::endl;

                    strcpy(theBranch.rocName, theROC.rocname().c_str());
                    rootDirs.cdDirectory(theROC);
                    TH1F *hVa = new TH1F(theROC.rocname().c_str(), theROC.rocname().c_str(), 1000, 0, 10);
                    TF1 *froc = new TF1("froc", "[0] + [1]*x", 0, 200);
                    froc->SetParameters(par0va, par1va);
                    for (std::map<int, int>::iterator it4 = (it3->second).begin(); it4 != (it3->second).end(); ++it4) {

                        if (rocids[it->first][it2->first][it3->first][it4->first] == rocs[it3->first - 1].roc() &&
                            rbreg[it->first][it2->first][it3->first][it4->first] == 11) {

                            hVa->Fill(froc->GetX(it4->second) / 2.0);
                        }

                    } //close loop on vdig values

                    delete froc;

                    hVa->Draw("HIST");
                    hVa->Write();

                    theBranch.Mean = hVa->GetMean();
                    theBranch.RMS = hVa->GetRMS();

                    tree->Fill();
                }

            } //close loop on rocs per fed channel
            in.close();
        } //close loop on fed channels
    }     //close loop on feds

    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::VanaAnalysis(pos::PixelCalibConfiguration *tmpCalib) {

    string cmd = "";

    branch_sum_vdig theBranch;
    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("Summary", "Summary");
    tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:rocName/C", 4096000);

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tmpCalib->rocList(), gDirectory);

    for (std::map<int, std::map<int, std::map<int, std::map<int, int> > > >::iterator it = values.begin(); it != values.end(); ++it) {

        for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 = (it->second).begin(); it2 != (it->second).end(); ++it2) {

            const std::vector<pos::PixelROCName> &rocs = theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
            pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);

            static std::string directory;
            directory = getenv("PIXELCONFIGURATIONBASE");

            std::ifstream in((directory + "/iana/0/ROC_Iana_" + theChannel.modulename() + ".dat").c_str());

            for (std::map<int, std::map<int, int> >::iterator it3 = (it2->second).begin(); it3 != (it2->second).end(); ++it3) {

                if (theNameTranslation_->ROCNameFromFEDChannelROCExists(it->first, it2->first, it3->first - 1)) {

                    pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(it->first, it2->first, it3->first - 1);
                    pos::PixelModuleName theModule(theROC.rocname());

                    std::string tmp;
                    std::string tag;
                    float par0va, par1va;
                    in >> tag; //ROC
                    in >> tmp;
                    //std::cout << " ROC: " << tmp;
                    in >> tag; //par0vd
                    in >> tmp;
                    in >> tag; //par1vd
                    in >> tmp;
                    in >> tag; //par0va
                    in >> tmp;
                    par0va = atof(tmp.c_str()); //stof(tmp);
                    in >> tag;                  //par1va
                    in >> tmp;
                    par1va = atof(tmp.c_str()); //stof(tmp);
                    in >> tag;                  //par0rbia
                    in >> tmp;
                    in >> tag; //par1rbia
                    in >> tmp;
                    in >> tag; //par0tbia
                    in >> tmp;
                    in >> tag; //par1tbia
                    in >> tmp;
                    in >> tag; //par2tbia
                    in >> tmp;
                    in >> tag; //par0ia
                    in >> tmp;
                    in >> tag; //par1ia
                    in >> tmp;
                    in >> tag; //par2ia
                    in >> tmp;
                    std::cout << " par0va " << par0va << " par1va " << par1va << std::endl;

                    strcpy(theBranch.rocName, theROC.rocname().c_str());
                    rootDirs.cdDirectory(theROC);
                    TH1F *hVa = new TH1F(theROC.rocname().c_str(), theROC.rocname().c_str(), 1000, 0, 10);
                    TF1 *froc = new TF1("froc", "[0] + [1]*x", 0, 200);
                    froc->SetParameters(par0va, par1va);
                    for (std::map<int, int>::iterator it4 = (it3->second).begin(); it4 != (it3->second).end(); ++it4) {

                        if (rocids[it->first][it2->first][it3->first][it4->first] == rocs[it3->first - 1].roc() &&
                            rbreg[it->first][it2->first][it3->first][it4->first] == 10) {

                            hVa->Fill(froc->GetX(it4->second) / 2.0);
                            std::cout << "hVa this is your histvalue " << froc->GetX(it4->second) / 2.0 << std::endl;
                            std::cout << "hVa this is your it4 second value" << it4->second << std::endl;
                        }

                    } //close loop on vdig values

                    delete froc;

                    hVa->Draw("HIST");
                    hVa->Write();

                    theBranch.Mean = hVa->GetMean();
                    theBranch.RMS = hVa->GetRMS();

                    tree->Fill();
                }

            } //close loop on rocs per fed channel
            in.close();
        } //close loop on fed channels
    }     //close loop on feds

    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::CloseRootf() {
    if (rootf) {
        rootf->Write();
        rootf->Close();
        delete rootf;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::BookEm(const TString &path) {

    values.clear();

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    TString root_fn;
    if (tempCalibObject->mode() == "IanaBPIX")
        // root_fn.Form("%s/Iana.root", outputDir().c_str());
        root_fn.Form("%s/Iana_%lu.root", outputDir().c_str(), crate_);
    else if (tempCalibObject->mode() == "VdigBPIX")
        //root_fn.Form("%s/Vd.root", outputDir().c_str());
        root_fn.Form("%s/Vd_%lu.root", outputDir().c_str(), crate_);
    else if (tempCalibObject->mode() == "VaBPIX")
        //root_fn.Form("%s/Vd.root", outputDir().c_str());
        root_fn.Form("%s/Va_%lu.root", outputDir().c_str(), crate_);
    else if (tempCalibObject->mode() == "VbgBPIX")
        //root_fn.Form("%s/Vd.root", outputDir().c_str());
        root_fn.Form("%s/Vbg_%lu.root", outputDir().c_str(), crate_);
    else if (tempCalibObject->mode() == "VanaBPIX")
        //root_fn.Form("%s/Vd.root", outputDir().c_str());
        root_fn.Form("%s/Vana_%lu.root", outputDir().c_str(), crate_);

    cout << "writing histograms to file " << root_fn << endl;
    CloseRootf();
    rootf = new TFile(root_fn, "create");
    assert(rootf->IsOpen());
    rootf->cd(0);
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::ReadIana(void) {

    //std::cout << " **************** VANA = " << lastDACValue << std::endl;
    for (std::map<int, std::map<int, std::map<int, std::vector<int> > > >::iterator it = last_dacs.begin(); it != last_dacs.end(); ++it) {

        for (std::map<int, std::map<int, std::vector<int> > >::iterator it2 = (it->second).begin(); it2 != (it->second).end(); ++it2) {

            for (std::map<int, std::vector<int> >::iterator it3 = (it2->second).begin(); it3 != (it2->second).end(); ++it3) {

                int start = -1;
                int stop = -1;
                int rbvalue = -1;
                int rocid = -1;
                int rbregvalue = -1;
                std::vector<int> bits;

                for (unsigned int i = 0; i < (it3->second).size(); ++i) {

                    //std::cout << " - channel#" << it2->first << " ROC#" << it3->first << " - dac " << i << ":" << (it3->second)[i] << std::endl;
                    if ((((it3->second)[i]) & (0x2)) == 2 && start == -1) {
                        start = i;
                    } else if ((((it3->second)[i]) & (0x2)) == 2 && start != -1) {
                        stop = i;
                    }
                }

                //std::cout << "    channel " << it2->first << ": (found start =  " << start << " and stop = " << stop << " ) " << std::endl;
                if (start != stop && (stop - start) == 16) {

                    //now put together the 16 bits
                    for (int i = start + 1; i <= stop; ++i) {
                        bits.push_back((it3->second)[i] & 0x1);
                    }

                    //now get Iana
                    //for( unsigned int b=0; b<bits.size(); ++b ){ std::cout << bits[b];}
                    //std::cout << std::endl;
                    rbvalue = 0;
                    for (unsigned int b = 8; b < bits.size(); ++b)
                        rbvalue += (bits[b] << (15 - b)); //std::cout << bits[b];
                    rocid = 0;
                    for (unsigned int b = 0; b < 4; ++b)
                        rocid += (bits[b] << (3 - b));
                    rbregvalue = 0;
                    for (unsigned int b = 4; b < 8; ++b)
                        rbregvalue += (bits[b] << (7 - b));
                }

                //std::cout << std::endl;
                //std::cout << " **************** VANA = " << lastDACValue << " FOUND IANA: " << rbvalue << std::endl;
                //std::cout << " **************** VANA = " << lastDACValue << " rbreg " << rb << std::endl;
                //std::cout << " **************** Vdig = " << lastDACValue << " FOUND Vd: " << rbvalue;
                //std::cout << " rocid " << rocid << " rbregvalue " << rbregvalue << std::endl;

                values[it->first][it2->first][it3->first][lastDACValue] = rbvalue;
                rocids[it->first][it2->first][it3->first][lastDACValue] = rocid;
                rbreg[it->first][it2->first][it3->first][lastDACValue] = rbregvalue;
            }
        }
    } //close loop
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDIanaCalibrationBPIX::ReadVdig(void) {

    //std::cout << " **************** VANA = " << lastDACValue << std::endl;
    //std::cout << " **************** JENDEBUG::ReadIana Vdd = " << lastDACValue << std::endl;
    for (std::map<int, std::map<int, std::map<int, std::vector<int> > > >::iterator it = last_dacs.begin(); it != last_dacs.end(); ++it) {

        for (std::map<int, std::map<int, std::vector<int> > >::iterator it2 = (it->second).begin(); it2 != (it->second).end(); ++it2) {

            for (std::map<int, std::vector<int> >::iterator it3 = (it2->second).begin(); it3 != (it2->second).end(); ++it3) {

                std::vector<int> start;
                start.push_back(-1);
                std::vector<int> stop;
                stop.push_back(-1);

                int ii = 0;
                for (unsigned int i = 0; i < (it3->second).size(); ++i) {

                    if ((((it3->second)[i]) & (0x2)) == 2 && start[ii] == -1) {
                        start[ii] = i;
                    } else if ((((it3->second)[i]) & (0x2)) == 2 && start[ii] != -1) {
                        stop[ii] = i;
                        start.push_back(i);
                        stop.push_back(i);
                        ii++;
                    }
                }

                start.pop_back();
                stop.pop_back();
                //std::cout << " - channel#" << it2->first << " ROC#" << it3->first << std::endl;
                //std::cout << "*************** start size " << start.size() << " stop size " << stop.size() << std::endl;
                for (unsigned int i = 0; i < start.size(); ++i) {

                    //std::cout << "    (found start =  " << start[i] << " and stop = " << stop[i] << " ) " << std::endl;
                    int rbvalue = -1;
                    int rocid = -1;
                    int rbregvalue = -1;
                    std::vector<int> bits;

                    if (start[i] != stop[i] && (stop[i] - start[i]) > 13) {

                        //now put together the 16 bits
                        for (int b = start[i] + 1; b <= stop[i]; ++b) {
                            bits.push_back((it3->second)[b] & 0x1);
                        }

                        //now get Iana
                        //for( unsigned int b=0; b<bits.size(); ++b ){ std::cout << bits[b];}
                        //std::cout << std::endl;
                        rbvalue = 0;
                        for (unsigned int b = 8; b < bits.size(); ++b)
                            rbvalue += (bits[b] << (15 - b)); //std::cout << bits[b];
                        rocid = 0;
                        for (unsigned int b = 0; b < 4; ++b)
                            rocid += (bits[b] << (3 - b));
                        rbregvalue = 0;
                        for (unsigned int b = 4; b < 8; ++b)
                            rbregvalue += (bits[b] << (7 - b));
                    }

                    //std::cout << " **************** Vdig = " << lastDACValue << " FOUND Vd: " << rbvalue;
                    //std::cout << " rocid " << rocid << " rbregvalue " << rbregvalue << std::endl;

                    values[it->first][it2->first][it3->first][i] = rbvalue;
                    rocids[it->first][it2->first][it3->first][i] = rocid;
                    rbreg[it->first][it2->first][it3->first][i] = rbregvalue;

                } //close loop on starts and stops
            }
        }
    } //close loop
}
