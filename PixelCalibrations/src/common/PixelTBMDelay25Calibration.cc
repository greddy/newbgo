#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortCardSettingNames.h"
#include "PixelCalibrations/include/PixelTBMDelay25Calibration.h"
//#include "PixelCalibrations/include/PixelPOHBiasCalibrationParametersBPIX.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"
#include <stdlib.h>
#include <fstream>
#include <iomanip>

using namespace pos;
using namespace std;

namespace {
const bool DO_DEBUG = false;
//const bool scanTrig=false;
}

PixelTBMDelay25Calibration::PixelTBMDelay25Calibration(const PixelSupervisorConfiguration &tempConfiguration, SOAPCommander *mySOAPCmdr)
    : PixelCalibrationBase(tempConfiguration, *mySOAPCmdr) {
          std::cout << "Greetings from the PixelTBMDelay25Calibration constructor." << std::endl;
}

void PixelTBMDelay25Calibration::beginCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    SDAMin = 64;
    if (tempCalibObject->parameterValue("ScanMin") != "")
        SDAMin = atoi(tempCalibObject->parameterValue("ScanMin").c_str());

    SDAMax = 127;
    if (tempCalibObject->parameterValue("ScanMax") != "")
        SDAMax = atoi(tempCalibObject->parameterValue("ScanMax").c_str());

    SDAStepSize = 1;
    if (tempCalibObject->parameterValue("ScanStepSize") != "")
        SDAStepSize = atoi(tempCalibObject->parameterValue("ScanStepSize").c_str());

    nTriggersPerPoint = 10;
    if (tempCalibObject->parameterValue("nTriggersPerPoint") != "")
        nTriggersPerPoint = atoi(tempCalibObject->parameterValue("nTriggersPerPoint").c_str());

    writeElog = tempCalibObject->parameterValue("writeElog") == "yes";
    scanTrig = tempCalibObject->parameterValue("scanTrig") == "yes";

    std::cout << "[INFO] Min = " << SDAMin << std::endl;
    std::cout << "[INFO] Max = " << SDAMax << std::endl;
    std::cout << "[INFO] StepSize = " << SDAStepSize << std::endl;
    std::cout << "[INFO] nTriggersPerPoint = " << nTriggersPerPoint << std::endl;
    std::cout << "[INFO] writeElog = " << writeElog << std::endl;
    std::cout << "[INFO] scanTrig = " << scanTrig << std::endl;

    std::cout << "[INFO] OutputDir = " << outputDir() << std::endl;

    outtext.Form("%s/log.txt", outputDir().c_str());

    BookEm();
}

bool PixelTBMDelay25Calibration::execute() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    std::ofstream ofs(outtext);
    ofs << std::endl;
    ofs << "SDA Min = " << SDAMin << std::endl;
    ofs << "SDA Max = " << SDAMax << std::endl;
    ofs << "SDA Stepsize = " << SDAStepSize << std::endl;
    ofs << "nTriggersPerPoint = " << nTriggersPerPoint << std::endl;
    ofs << std::endl;

    const bool firstOfPattern = event_ % tempCalibObject->nTriggersPerPattern() == 0;
    reportProgress(0.05);
    if (DO_DEBUG)
        cout << " patterns " << firstOfPattern << " " << event_ << " " << tempCalibObject->nTriggersPerPattern() << endl;

    event_++;

    // Configure all TBMs and ROCs according to the PixelCalibConfiguration settings, but only when it's time for a new configuration.

    if (firstOfPattern) {
        commandToAllFECCrates("CalibRunning");
    }

    // should take this out
    //commandToAllFEDCrates("JMTJunk");

    const std::set<PixelChannel> &channelsToCalibrate = tempCalibObject->channelList();
    //unsigned int channelsSize = channelsToCalibrate.size();
    cout << " channels1 " << endl;
    std::set<int> feds;
    std::map<int, float> count;
    std::map<int, std::map<int, float> > countAllChans;
    std::map<int, std::map<int, int> > layers;
    std::map<int, std::string> layer_to_portcard; // little bit dirty though ...

    // Scan SDAs
    for (unsigned int SDA = SDAMin; SDA <= SDAMax; SDA += SDAStepSize) {

        std::map<int, int> countPerSDA;
        // Setup all channels
        for (std::set<PixelChannel>::const_iterator channelsToCalibrate_itr = channelsToCalibrate.begin();
             channelsToCalibrate_itr != channelsToCalibrate.end(); channelsToCalibrate_itr++) {
            // Get info about this channel.
            const PixelHdwAddress &channelHdwAddress = theNameTranslation_->getHdwAddress(*channelsToCalibrate_itr);
            const unsigned int fednumber = channelHdwAddress.fednumber();
            feds.insert(fednumber);
            // probably will have to check the fed crate?
            //	const unsigned int fedcrate=theFEDConfiguration_->crateFromFEDNumber( fednumber );
            const unsigned int channel = channelHdwAddress.fedchannel();
            int key = fednumber * 100 + channel;
            if ((channel % 2) == 1)
                countPerSDA[key] = 0;
        }

        // Set AOH bias, send command to TKFECs
        Attribute_Vector parametersToTKFEC(4);
        parametersToTKFEC[0].name_ = "Delay25Setting";
        parametersToTKFEC[0].value_ = itoa(SDA);
        if (scanTrig) { // special TRIG scan
            //parametersToTKFEC[1].name_="DelayType";      parametersToTKFEC[1].value_="SCLK"; //clock
            parametersToTKFEC[1].name_ = "DelayType";
            parametersToTKFEC[1].value_ = "TRIG"; // trig
        } else {                                  // Normall SDA scan
            parametersToTKFEC[1].name_ = "DelayType";
            parametersToTKFEC[1].value_ = "SDATA";
        }
        parametersToTKFEC[2].name_ = "Update";
        parametersToTKFEC[2].value_ = "0";
        parametersToTKFEC[3].name_ = "Write";
        parametersToTKFEC[3].value_ = "0";

        //std::cout << " will request for new SDA, SDA = " << SDA << std::endl;

        // Set the new SDA
        commandToAllTKFECCrates("SetDelayEnMass", parametersToTKFEC);
        //std::cout << "Send request for new SDA, SDA = " << SDA << std::endl;

        for (unsigned int i_event = 0; i_event < nTriggersPerPoint; ++i_event) {

            commandToAllFECCrates("CalibRunning"); // programm

            // How many points do we want per point, for delay25 just 1
            for (unsigned int numTrig = 0; numTrig < tempCalibObject->nTriggersPerPattern(); ++numTrig) {

                sendTTCCalSync(); // send trigger

                // Read all channels
                for (std::set<PixelChannel>::const_iterator channelsToCalibrate_itr = channelsToCalibrate.begin();
                     channelsToCalibrate_itr != channelsToCalibrate.end(); channelsToCalibrate_itr++) {

                    // Get info about this channel.
                    const PixelHdwAddress &channelHdwAddress = theNameTranslation_->getHdwAddress(*channelsToCalibrate_itr);
                    const unsigned int fednumber = channelHdwAddress.fednumber();
                    // probably will have to check the fed crate?
                    const unsigned int fedcrate = theFEDConfiguration_->crateFromFEDNumber(fednumber);
                    const unsigned int channel = channelHdwAddress.fedchannel();
                    if ((channel % 2) == 0)
                        continue; // Skip 2nd multiplexed channel
                    //cout<<" loop channels "<<channel<<endl;
                    int key = fednumber * 100 + channel;

                    const std::pair<std::string, int> portCardAndAOH = thePortcardMap_->PortCardAndAOH(*channelsToCalibrate_itr);
                    const std::string portCardName = portCardAndAOH.first;
                    //	const std::string tbmname = (*channelsToCalibrate_itr).TBMChannelStringFull();

                    int layer = -1;
                    if (portCardName.find("PRT1") != std::string::npos)
                        layer = 0;
                    else if (portCardName.find("PRT2") != std::string::npos)
                        layer = 1;
                    else {
                        std::cout << "ERROR : UNEXPECTED PORTCARDNAME !!!" << std::endl;
                    }

                    layers[fednumber][channel] = layer;
                    layer_to_portcard[layer] = portCardName;

                    // Recover data
                    Attribute_Vector parametersToFED(4);
                    parametersToFED[0].name_ = "WhatToDo";
                    parametersToFED[1].name_ = "SDA";
                    parametersToFED[2].name_ = "fednumber";
                    parametersToFED[3].name_ = "channel";

                    parametersToFED[0].value_ = "RetrieveData";
                    parametersToFED[1].value_ = itoa(SDA);
                    parametersToFED[2].value_ = itoa(fednumber);
                    parametersToFED[3].value_ = itoa(channel);

                    xoap::MessageReference replyFromFED = SendWithSOAPReply(PixelFEDSupervisors_[fedcrate], "FEDCalibrations", parametersToFED);

                    Attribute_Vector returnValuesFromFED(1);
                    returnValuesFromFED[0].name_ = "foundTBMA";
                    Receive(replyFromFED, returnValuesFromFED);

                    //Int_t found_ = (int)atoi(returnValuesFromFED[0].value_.c_str());
                    int found = (int)atoi(returnValuesFromFED[0].value_.c_str());
                    //	  cout<<" got from fed "<<found<<" "<<fednumber<<" "<<channel<<endl;

                    if (found == 1) {
                        histos[fednumber][channel]->Fill(SDA);
                        histo_total->Fill(SDA);
                        countPerSDA[key]++;
                    } // if found

                    //	  cout<<" sda "<<SDA<<" count "<<countPerSDA[channel]<<" channel "<<channel<<endl;

                } // channels

                //cout<<" sda "<<SDA<<" event "<<i_event<<endl;

            } // num triggers per point

        } // nTrigger Loop

        //if(DO_DEBUG) cout<<" for sda "<<SDA<<" channels "<< countPerSDA.size() <<endl;
        float efficiency = 0.;
        for (std::map<int, int>::const_iterator i = countPerSDA.begin(); i != countPerSDA.end(); ++i) {
            efficiency += float(i->second);
            countAllChans[SDA][i->first] = float(i->second) / float(nTriggersPerPoint);
            //cout<<" key "<<i->first<<" count "<<i->second<<endl;
        }

        efficiency = efficiency / float(countPerSDA.size()) / float(nTriggersPerPoint);

        if (DO_DEBUG)
            cout << " for sda " << SDA << " average efficiency is " << efficiency << " " << countPerSDA.size()
                 << " " << nTriggersPerPoint << endl;

        count[SDA] = efficiency;

    } // end of loop over SDA values

    cout << " finish " << event_ << " " << tempCalibObject->nTriggersTotal() << endl;
    for (std::map<int, float>::const_iterator i = count.begin(); i != count.end(); ++i) {
        cout << " sda " << i->first << " eff " << i->second << endl;
        ofs << " sda " << i->first << " eff " << i->second << endl;
    }

    //cout<<feds.size()<<endl;
    for (std::set<int>::const_iterator j = feds.begin(); j != feds.end(); ++j) {
        int fed = *j;
        cout << "--------------------------------------------------------------------------------- " << endl;
        ofs << "--------------------------------------------------------------------------------- " << endl;
        cout << " For FED " << fed << endl;
        ofs << " For FED " << fed << endl;

        cout << "sda   ";
        ofs << "sda   ";
        for (std::map<int, std::map<int, float> >::const_iterator i = countAllChans.begin(); i != countAllChans.end(); ++i) {
            for (std::map<int, float>::const_iterator n = i->second.begin(); n != i->second.end(); ++n) {
                int key = n->first;
                int channel = key % 100;
                int fednumber = key / 100;
                if (fednumber == fed) {
                    cout << setw(3) << channel << " ";
                    ofs << setw(3) << channel << " ";
                }
            }
            cout << endl;
            ofs << endl;
            break;
        }

        for (std::map<int, std::map<int, float> >::const_iterator i = countAllChans.begin(); i != countAllChans.end(); ++i) {
            cout << setw(3) << setprecision(2) << "0x" << std::hex << i->first << std::dec << " ";
            ofs << setw(3) << setprecision(2) << "0x" << std::hex << i->first << std::dec << " ";
            for (std::map<int, float>::const_iterator n = i->second.begin(); n != i->second.end(); ++n) {
                int key = n->first;
                //	int channel=key%100;
                int fednumber = key / 100;
                if (fednumber == fed) {
                    cout << setw(3) << setprecision(2) << n->second << " ";
                    ofs << setw(3) << setprecision(2) << n->second << " ";
                }
            }
            cout << endl;
            ofs << endl;
        }

    } // loop over FEDS

    // Yuta
    int counter_layer[2] = { 0, 0 };

    for (int index = 0; index < (int)fednumber_.size(); index++) {

        int fed = fednumber_[index];
        int channel = channel_[index];
        int layer = layers[fed][channel]; //0 for L12, 1 for L34

        bool flag = false;

        // remove not-working channels (i.e all 0)
        for (int ibin = 1; ibin <= histos[fed][channel]->GetNbinsX(); ibin++) {
            if (histos[fed][channel]->GetBinContent(ibin) != 0)
                flag = true;
        }

        if (flag) {
            std::cout << "fed = " << fed << ", channel = " << channel << ",  layer = " << layer << std::endl;

            histo_layer[layer]->Add(histos[fed][channel]);
            counter_layer[layer]++;

        } else {
            std::cout << "fed = " << fed << ", channel = " << channel << ",  layer = " << layer << " ... dropped for the optimization study (it is always 0)" << std::endl;
        }

        if (nTriggersPerPoint != 0)
            histos[fed][channel]->Scale(1. / nTriggersPerPoint);
    }

    if (nTriggersPerPoint != 0 && fednumber_.size() != 0) {
        histo_total->Scale(1. / (nTriggersPerPoint * fednumber_.size()));
    }

    for (int ilayer = 0; ilayer < 2; ilayer++) {
        if (nTriggersPerPoint != 0 && counter_layer[ilayer] != 0) {
            histo_layer[ilayer]->Scale(1. / (nTriggersPerPoint * counter_layer[ilayer]));
        }
    }

    cout << "-------------------------- optimization --------------------------" << endl;
    ofs << "-------------------------- optimization --------------------------" << endl;

    TString layername[2] = { "L12", "L34" };

    for (int ilayer = 0; ilayer < 2; ilayer++) {
        bool flag_first = false;
        bool flag_last = false;
        int first_sda = -1;
        int last_sda = -1;

        for (int ibin = 1; ibin < histo_layer[ilayer]->GetXaxis()->GetNbins() + 1; ibin++) {
            Int_t SDA = histo_layer[ilayer]->GetXaxis()->GetBinLowEdge(ibin);
            Float_t eff = histo_layer[ilayer]->GetBinContent(ibin);

            if (flag_first == false && eff == 0.5) {
                flag_first = true;
                first_sda = SDA;
            }

            if (flag_first == true && eff != 0.5) {
                flag_last = true;
            }

            if (flag_last == false && eff == 0.5) {
                last_sda = SDA;
            }
        }

        if (first_sda != -1 && last_sda != -1) {
            std::cout << layername[ilayer] << " : eff. = 0.5 for SDA (" << first_sda << ", " << last_sda << ") => optimal SDA = " << (int)(1 + (last_sda + first_sda) / 2) << std::endl;
            ofs << layername[ilayer] << " : eff. = 0.5 for SDA (" << first_sda << ", " << last_sda << ") => optimal SDA = " << (int)(1 + (last_sda + first_sda) / 2) << std::endl;
            sda_values_by_portcard[layer_to_portcard[ilayer]] = 1 + (last_sda + first_sda) / 2;
        } else {
            std::cout << layername[ilayer] << " : !!!! optimal SDA was not found !!!! " << std::endl;
            ofs << layername[ilayer] << " : !!!! optimal SDA was not found !!!! " << std::endl;
            sda_values_by_portcard[layer_to_portcard[ilayer]] = -1;
        }
    }

    cout << "------------------------ optimization done ------------------------" << endl;
    ofs << "------------------------ optimization done ------------------------" << endl;

    ofs.close();

    // Draw graphs

    for (std::map<int, std::map<int, TH1F *> >::iterator it1 = histos.begin(); it1 != histos.end(); ++it1) {

        for (std::map<int, TH1F *>::iterator it2 = it1->second.begin(); it2 != it1->second.end(); ++it2) {

            Int_t fednumber = it1->first;
            Int_t channel = it2->first;

            TString cname = "Summary_FED";
            cname += fednumber;
            cname += "_channel";
            cname += channel;

            TCanvas *c = new TCanvas(cname, cname);

            histos[fednumber][channel]->SetLineColor(1);
            histos[fednumber][channel]->SetLineWidth(2);
            histos[fednumber][channel]->GetYaxis()->SetRangeUser(0, 1.3);
            histos[fednumber][channel]->Draw();

            int layer = layers[fednumber][channel];
            string portcardname = layer_to_portcard[layer];

            // create directory
            std::string cmd = "mkdir -p ";
            cmd += outputDir();
            cmd += "/";
            cmd += portcardname;

            system(cmd.c_str());

            TString tfilename;
            tfilename.Form("%s/%s/sda_FED%i_ch%i.gif", outputDir().c_str(), portcardname.c_str(), fednumber, channel);
            c->Print(tfilename);
        }
    }

    TCanvas *c_total = new TCanvas("total", "total");

    histo_total->SetLineColor(1);
    histo_total->SetLineWidth(2);
    histo_total->GetYaxis()->SetRangeUser(0, 1.3);
    histo_total->Draw();

    TString tfilename;
    tfilename.Form("%s/sda_total.gif", outputDir().c_str());
    c_total->Print(tfilename);

    for (int ilayer = 0; ilayer < 2; ilayer++) {

        TString cname = "canvas_";
        cname += ilayer;

        TCanvas *c = new TCanvas(cname, cname);

        histo_layer[ilayer]->SetLineColor(1);
        histo_layer[ilayer]->SetLineWidth(2);
        histo_layer[ilayer]->GetYaxis()->SetRangeUser(0, 1.3);
        histo_layer[ilayer]->Draw();

        Int_t optimal = sda_values_by_portcard[layer_to_portcard[ilayer]];

        if (optimal != -1) {
            TLine *line = new TLine(optimal, 0, optimal, histo_layer[ilayer]->GetMaximum());
            line->SetLineStyle(2);
            line->SetLineWidth(2);
            line->SetLineColor(4);
            line->Draw();
        }

        TString tfilename;
        tfilename.Form("%s/sda_layer%i.gif", outputDir().c_str(), ilayer);
        c->Print(tfilename);
    }

    if (writeElog) {
        PixelElogMaker *elog = new PixelElogMaker("TBMDelay 25");
        elog->post(runDir(), outputDir());
    }

    return false;
}

void PixelTBMDelay25Calibration::endCalibration() {

    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    std::cout << "Start writing to the portcard config ..." << std::endl;
    //Write out the configs

    //  for (std::map<std::string, std::map<unsigned int, unsigned int> >::iterator portCardName_itr = sda_values_by_portcard.begin();
    for (std::map<std::string, unsigned int>::iterator portCardName_itr = sda_values_by_portcard.begin();
         portCardName_itr != sda_values_by_portcard.end(); portCardName_itr++) {

        std::string portCardName = portCardName_itr->first;

        std::cout << "Writing ... for " << portCardName << " " << sda_values_by_portcard[portCardName] << std::endl;

        if (portCardName == "")
            continue;

        std::map<std::string, PixelPortCardConfig *>::iterator mapNamePortCard_itr = getmapNamePortCard()->find(portCardName);
        assert(mapNamePortCard_itr != getmapNamePortCard()->end());
        PixelPortCardConfig *thisPortCardConfig = mapNamePortCard_itr->second;

        //    unsigned int SDAAddress = thisPortCardConfig->SDAAddressFromAOHNumber(AOHNumber);
        thisPortCardConfig->setdeviceValues(PortCardSettingNames::k_Delay25_SDA, sda_values_by_portcard[portCardName]);
        //}
        //    thisPortCardConfig->setdeviceValues(PortCardSettingNames::k_Delay25_SDA, (tempUnInt & 0x3F) | 0x40);  //the & 0x3F | 0x40 sets the enable bit
        thisPortCardConfig->writeASCII(outputDir());
        cout << "Wrote the portcard config for port card: " << portCardName << endl;
    }

    // create e-log message

    //  if(writeElog){
    //
    //    string cmd = "";
    //
    //    for(std::map<int,std::map<int, std::map<int, TH1F* > > >::iterator it1 = histos.begin(); it1 != histos.end(); ++it1){
    //
    //      Int_t fednumber = it1->first;
    //
    //      TString cname = "Summary_FED";
    //      cname += fednumber;
    //
    //      TCanvas *c = new TCanvas(cname, cname);
    //      summary[fednumber]->Draw("colztext");
    //
    //      TString filename;
    //      filename.Form("%s/summary_FED%i.gif", outputDir().c_str(), fednumber);
    //      c->Print(filename);
    //
    //      cmd += " -f ";
    //      cmd += filename;
    //
    //
    //      TString tname = "Turnon_FED";
    //      tname += fednumber;
    //
    //      TCanvas *c2 = new TCanvas(tname, tname);
    //      TLegend *leg = new TLegend(0.6,0.2,0.8,0.5);
    //
    //      int idraw = 0;
    //      for(std::map<int, std::map<int, TH1F* > >::iterator it2 = it1->second.begin(); it2 != it1->second.end(); ++it2){
    //
    //	Int_t ch = it2->first;
    //
    //	histos[fednumber][ch][2]->SetLineColor(idraw+1);
    //	histos[fednumber][ch][2]->SetLineWidth(2);
    //	histos[fednumber][ch][2]->SetLineStyle(idraw+1);
    //	histos[fednumber][ch][2]->GetYaxis()->SetRangeUser(0, 1.3);
    //
    //	if(idraw==0) histos[fednumber][ch][2]->Draw();
    //	else histos[fednumber][ch][2]->Draw("same");
    //
    //	TString chlabel = "ch = ";
    //	chlabel += ch;
    //	leg->AddEntry(histos[fednumber][ch][2], chlabel, "l");
    //
    //	idraw++;
    //      }
    //
    //      leg->Draw();
    //
    //      TString tfilename;
    //      tfilename.Form("%s/turnon_FED%i.gif", outputDir().c_str(), fednumber);
    //      c2->Print(tfilename);
    //
    //      cmd += " -f ";
    //      cmd += tfilename;
    //
    //    }
    //
    //    elog->post(runDir(), (string)outtext, cmd);
    //  }

    tempCalibObject->writeASCII(outputDir());

    CloseRootf();
}

std::vector<std::string> PixelTBMDelay25Calibration::calibrated() {
    std::vector<std::string> tmp;
    tmp.push_back("portcard");
    return tmp;
}

void PixelTBMDelay25Calibration::CloseRootf() {
    if (rootf) {
        rootf->Write();
        rootf->Close();
        delete rootf;
    }
}

void PixelTBMDelay25Calibration::BookEm() {

    root_fn.Form("%s/TBMDelay25C.root", outputDir().c_str());

    rootf = new TFile(root_fn, "recreate");
    assert(rootf->IsOpen());

    vectorOfPortcards.clear();
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    const std::set<PixelChannel> &channelsToCalibrate = tempCalibObject->channelList();

    for (std::set<PixelChannel>::const_iterator channelsToCalibrate_itr = channelsToCalibrate.begin();
         channelsToCalibrate_itr != channelsToCalibrate.end(); channelsToCalibrate_itr++) {

        const PixelHdwAddress &channelHdwAddress = theNameTranslation_->getHdwAddress(*channelsToCalibrate_itr);
        const unsigned int fednumber = channelHdwAddress.fednumber();
        const unsigned int channel = channelHdwAddress.fedchannel();

        const std::pair<std::string, int> portCardAndAOH = thePortcardMap_->PortCardAndAOH(*channelsToCalibrate_itr);
        const std::string portCardName = portCardAndAOH.first;
        const std::string tbmname = (*channelsToCalibrate_itr).TBMChannelStringFull();

        if (DO_DEBUG) {
            std::cout << "DBG : fednumber = " << fednumber << std::endl;
            std::cout << "DBG : channel = " << channel << std::endl;
            std::cout << "DBG : portCardName = " << portCardName << std::endl;
            std::cout << "DBG : tbmname = " << tbmname << std::endl;
        }

        // This is to escape for A2/B2 channels - YT
        if (tbmname.find("2") != std::string::npos)
            continue;
        assert(portCardName != "none");

        const int AOHNumber = portCardAndAOH.second;

        string vec = portCardName;
        vec += "_POH";
        vec += AOHNumber;

        if (DO_DEBUG)
            std::cout << vec << std::endl;

        vectorOfPortcards.push_back(vec);
        portcard_.push_back(portCardName);
        aohnumber_.push_back(AOHNumber);
        channel_.push_back(channel);
        fednumber_.push_back(fednumber);
    }

    gStyle->SetPaintTextFormat("2.2f");
    gStyle->SetOptStat(0);

    rootDirs = new PixelRootDirectoryMaker(vectorOfPortcards, gDirectory);

    int nbins = ((SDAMax - SDAMin) / SDAStepSize) + 1;

    for (int istr = 0; istr < (int)portcard_.size(); istr++) {
        string dirname = portcard_[istr];
        dirname += "_";
        dirname += aohnumber_[istr];

        if (DO_DEBUG)
            std::cout << dirname << std::endl;

        rootDirs->cdDirectory(dirname);

        Int_t channel = channel_[istr];

        TString hname = "h_eff_fed";
        hname += fednumber_[istr];
        hname += "_channel";
        hname += channel;

        TH1F *h_eff = new TH1F(hname, hname,
                               nbins, SDAMin, SDAMax + 1);

        h_eff->GetXaxis()->SetTitle("SDA");
        h_eff->GetYaxis()->SetTitle("efficiency");

        TString titlename = portcard_[istr];
        titlename += ", FED";
        titlename += fednumber_[istr];
        titlename += ", channel";
        titlename += channel_[istr];

        h_eff->SetTitle(titlename);
        h_eff->SetMinimum(0);

        histos[fednumber_[istr]][channel] = h_eff;
    }

    rootf->cd();

    histo_total = new TH1F("hist_total", "hist_total",
                           nbins, SDAMin, SDAMax + 1);

    histo_total->SetTitle("Total");
    histo_total->GetXaxis()->SetTitle("SDA");
    histo_total->GetYaxis()->SetTitle("efficiency");
    histo_total->SetMinimum(0);

    for (int ilayer = 0; ilayer < 2; ilayer++) {

        TString hname = "histo_layer";
        if (ilayer == 0)
            hname += "12";
        else
            hname += "34";

        histo_layer[ilayer] = new TH1F(hname, hname, nbins, SDAMin, SDAMax + 1);
        histo_layer[ilayer]->GetXaxis()->SetTitle("SDA");
        histo_layer[ilayer]->GetYaxis()->SetTitle("efficiency");
        histo_layer[ilayer]->SetMinimum(0);

        TString titlename = "Total, layer";
        if (ilayer == 0)
            titlename += "1/2";
        else
            titlename += "3/4";

        histo_layer[ilayer]->SetTitle(titlename);
    }
}
