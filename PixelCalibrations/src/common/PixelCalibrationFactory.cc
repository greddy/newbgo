#include "PixelCalibrations/include/PixelCalibrationFactory.h"

//PixelSupervisor calibrations
#include "PixelCalibrations/include/PixelDelay25Calibration.h"
#include "PixelCalibrations/include/PixelPOHBiasCalibration.h"
#include "PixelCalibrations/include/PixelIanaCalibration.h"
#include "PixelCalibrations/include/PixelReadbacker.h"
#include "PixelCalibrations/include/PixelIdigiCalibration.h"
#include "PixelCalibrations/include/PixelGainAliveSCurveCalibration.h"
#include "PixelCalibrations/include/PixelGainAliveSCurveCalibrationWithSLink.h"
#include "PixelCalibrations/include/PixelThresholdCalDelayCalibration.h"
#include "PixelCalibrations/include/Pixel2DEfficiencyScan.h"
#include "PixelCalibrations/include/PixelCalDelCalibration.h"
#include "PixelCalibrations/include/PixelVcThrCalibration.h"
#include "PixelCalibrations/include/PixelTemperatureCalibration.h"
#include "PixelCalibrations/include/PixelThresholdCalDelayCalibrationFIFO1.h"
#include "PixelCalibrations/include/PixelVsfAndVHldDelCalibration.h"
#include "PixelCalibrations/include/PixelLinearityVsVsfCalibration.h"
#include "PixelCalibrations/include/PixelPHRangeCalibration.h"
#include "PixelCalibrations/include/PixelROCDelay25Calibration.h"
#include "PixelCalibrations/include/PixelTBMDelayCalibrationWithScores.h"
#include "PixelCalibrations/include/PixelTBMDelayCalibrationWithScoresBPIX.h"
#include "PixelCalibrations/include/PixelTBMDelayCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelROCDelayCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelIanaCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelPOHBiasCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelTBMDelay25Calibration.h"
#include "PixelCalibrations/include/PixelTBMPLLROCCalibration.h"

//PixelFEDSupervisor calibrations
#include "PixelCalibrations/include/PixelFED2DEfficiencyScan.h"
#include "PixelCalibrations/include/PixelFEDCalDelCalibration.h"
#include "PixelCalibrations/include/PixelFEDVcThrCalibration.h"
#include "PixelCalibrations/include/PixelFEDThresholdCalDelayCalibration.h"
#include "PixelCalibrations/include/PixelFEDThresholdCalDelayCalibrationFIFO1.h"
#include "PixelCalibrations/include/PixelFEDGainAliveSCurveCalibration.h"
#include "PixelCalibrations/include/PixelFEDGainAliveSCurveCalibrationWithSLink.h"
#include "PixelCalibrations/include/PixelFEDVsfAndVHldDelCalibration.h"
#include "PixelCalibrations/include/PixelFEDLinearityVsVsfCalibration.h"
#include "PixelCalibrations/include/PixelFEDPHRangeCalibration.h"
#include "PixelCalibrations/include/PixelFEDROCDelay25Calibration.h"
#include "PixelCalibrations/include/PixelFEDTBMDelayCalibrationWithScores.h"
#include "PixelCalibrations/include/PixelFEDTBMDelayCalibrationWithScoresBPIX.h"
#include "PixelCalibrations/include/PixelFEDTBMDelayCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelFEDROCDelayCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelFEDIanaCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelFEDVCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelFEDPOHBiasCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelFEDTBMDelay25Calibration.h"
#include "PixelCalibrations/include/PixelDelay25VsReadbackerCalibration.h"
#include "PixelCalibrations/include/PixelFEDTBMPLLROCCalibration.h"
#include "PixelCalibrations/include/PixelPowerCycleCalibration.h"

//PixelTKFECSupervisor calibrations
#include "PixelCalibrations/include/PixelTKFECDelay25Calibration.h"

PixelCalibrationBase *PixelCalibrationFactory::getCalibration(const std::string &calibName, const PixelSupervisorConfiguration *pixSupConfPtr, SOAPCommander *soapCmdrPtr, PixelDCSSOAPCommander *dcsSoapCommanderPtr, PixelDCSPVSSCommander *pvssCommanderPtr) const {
    if (calibName == "POHBias") {
        return new PixelPOHBiasCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "Iana") {
        return new PixelIanaCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "Readbacker") {
        return new PixelReadbacker(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "Idigi") {
        return new PixelIdigiCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "VsfAndVHldDel") {
        return new PixelVsfAndVHldDelCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "LinearityVsVsf") {
        return new PixelLinearityVsVsfCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "PHRange") {
        return new PixelPHRangeCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "TemperatureCalibration") {
        assert(0);
        return new PixelTemperatureCalibration(*pixSupConfPtr,
                                               soapCmdrPtr,
                                               dcsSoapCommanderPtr,
                                               pvssCommanderPtr);
    }

    if (calibName == "GainCalibration" || calibName == "PixelAlive" || calibName == "SCurve" || calibName == "SCurveSmartRange") {
        return new PixelGainAliveSCurveCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "GainCalibrationWithSLink" || calibName == "PixelAliveWithSLink" || calibName == "SCurveWithSLink") {
        return new PixelGainAliveSCurveCalibrationWithSLink(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "2DEfficiencyScan") {
        return new Pixel2DEfficiencyScan(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "CalDelCalibration") {
        return new PixelCalDelCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "VcThrCalibration") {
        return new PixelVcThrCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "ThresholdCalDelay") {
        return new PixelThresholdCalDelayCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "ThresholdCalDelayFIFO1") {
        return new PixelThresholdCalDelayCalibrationFIFO1(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "Delay25") {
        return new PixelDelay25Calibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "ROCDelay25") {
        return new PixelROCDelay25Calibration(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "TBMDelayWithScores") {
        return new PixelTBMDelayCalibrationWithScores(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "TBMDelayWithScoresBPIX") {
        return new PixelTBMDelayCalibrationWithScoresBPIX(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "TBMDelayBPIX") {
        return new PixelTBMDelayCalibrationBPIX(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "ROCDelayBPIX") {
        return new PixelROCDelayCalibrationBPIX(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "IanaBPIX" || calibName == "VdigBPIX" || calibName == "VaBPIX" || calibName == "VbgBPIX" || calibName == "VanaBPIX" || calibName == "VallBPIX") {
        return new PixelIanaCalibrationBPIX(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "POHBiasBPIX") {
        return new PixelPOHBiasCalibrationBPIX(*pixSupConfPtr, soapCmdrPtr);
    }

    if (calibName == "TBMDelay25") {
        return new PixelTBMDelay25Calibration(*pixSupConfPtr, soapCmdrPtr);
    }
    if (calibName=="Delay25Readback") {
        return new PixelDelay25VsReadbackerCalibration(*pixSupConfPtr, soapCmdrPtr);
    }
    if (calibName == "TBMPLLROC") {
        return new PixelTBMPLLROCCalibration(*pixSupConfPtr, soapCmdrPtr);
    }
    if (calibName == "PowerCycleCalibration") {
        return new PixelPowerCycleCalibration(*pixSupConfPtr, soapCmdrPtr);
    }

    return 0;
}

PixelFEDCalibrationBase *PixelCalibrationFactory::getFEDCalibration(const std::string &calibName,
                                                                    const PixelFEDSupervisorConfiguration *pixFEDSupConfPtr,
                                                                    const pixel::utils::SOAPER *soaperPtr) const {
    if (calibName == "2DEfficiencyScan")
        return new PixelFED2DEfficiencyScan(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "CalDelCalibration")
        return new PixelFEDCalDelCalibration(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "VcThrCalibration")
        return new PixelFEDVcThrCalibration(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "ThresholdCalDelay")
        return new PixelFEDThresholdCalDelayCalibration(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "ThresholdCalDelayFIFO1")
        return new PixelFEDThresholdCalDelayCalibrationFIFO1(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "VsfAndVHldDel")
        return new PixelFEDVsfAndVHldDelCalibration(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "LinearityVsVsf")
        return new PixelFEDLinearityVsVsfCalibration(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "PHRange")
        return new PixelFEDPHRangeCalibration(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "GainCalibration" ||
        calibName == "PixelAlive" ||
        calibName == "SCurve" ||
        calibName == "SCurveSmartRange")
        return new PixelFEDGainAliveSCurveCalibration(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "GainCalibrationWithSLink" ||
        calibName == "PixelAliveWithSLink" ||
        calibName == "SCurveWithSLink")
        return new PixelFEDGainAliveSCurveCalibrationWithSLink(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "ROCDelay25")
        return new PixelFEDROCDelay25Calibration(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "TBMDelayWithScores")
        return new PixelFEDTBMDelayCalibrationWithScores(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "TBMDelayWithScoresBPIX")
        return new PixelFEDTBMDelayCalibrationWithScoresBPIX(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "TBMDelayBPIX") {
        return new PixelFEDTBMDelayCalibrationBPIX(*pixFEDSupConfPtr, soaperPtr);
    }

    if (calibName == "ROCDelayBPIX")
        return new PixelFEDROCDelayCalibrationBPIX(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "IanaBPIX")
        return new PixelFEDIanaCalibrationBPIX(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "VdigBPIX" || calibName == "VaBPIX" || calibName == "VbgBPIX" || calibName == "VanaBPIX" || calibName == "VallBPIX")
        return new PixelFEDVCalibrationBPIX(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "POHBiasBPIX")
        return new PixelFEDPOHBiasCalibrationBPIX(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "TBMDelay25")
        return new PixelFEDTBMDelay25Calibration(*pixFEDSupConfPtr, soaperPtr);

    if (calibName == "TBMPLLROC")
        return new PixelFEDTBMPLLROCCalibration(*pixFEDSupConfPtr, soaperPtr);

    return 0;
}

PixelTKFECCalibrationBase *PixelCalibrationFactory::getTKFECCalibration(const std::string &calibName,
                                                                        const PixelTKFECSupervisorConfiguration *pixTKFECSupConfPtr,
                                                                        pixel::utils::SOAPER *soaperPtr) const {

    if (calibName == "Delay25") {
        return new PixelTKFECDelay25Calibration(*pixTKFECSupConfPtr, soaperPtr);
    }

    return 0;
}
