// Add the option to ignore some 400MHz settings (0,4,6) d.k. 6/2/18
#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelFEDTBMDelayCalibrationBPIX.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelFEDDataTypes.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ColRowAddrDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigScopeDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigTransDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"
#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TTree.h"
#include <iomanip>
#include <algorithm>

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

#include "pixel/utils/Utils.h"


namespace {
  const uint32_t delay1Original[8] = { 0, 5, 1, 6, 2, 7, 3, 4 };  //
  const uint32_t delay1Corrected[8] = { 0, 2, 4, 6, 7, 1, 3, 5 }; //
  static int scoreMatrix[64][11];
}
///////////////////////////////////////////////////////////////////////////////////////////////
PixelFEDTBMDelayCalibrationBPIX::PixelFEDTBMDelayCalibrationBPIX(const PixelFEDSupervisorConfiguration &tempConfiguration, const pixel::utils::SOAPER *soaper)
: PixelFEDCalibrationBase(tempConfiguration, *soaper), rootf_ (0), logger_(app_->getApplicationLogger()) {
    LOG4CPLUS_DEBUG(logger_, "In PixelFEDTBMDelayCalibrationBPIX copy ctor()");
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDTBMDelayCalibrationBPIX::initializeFED() {
    pos::PixelCalibConfiguration *tempCalibObject = getCalibObject();
    readFifo1_ = tempCalibObject->parameterValue("ReadFifo1") == "yes";

    // Setup FEDs if needed
    if (!readFifo1_) {

        typedef std::set<std::pair<uint32_t, uint32_t> > colrow_t;
        const colrow_t colrows = tempCalibObject->pixelsWithHits(0);
        if (colrows.size() != 1) {
            LOG4CPLUS_FATAL(logger_, "must use exactly one pixel for score scan!");
            assert(0);
        }
        theCol_ = colrows.begin()->first;
        theRow_ = colrows.begin()->second;
        const uint32_t dc = theCol_ / 2;
        const uint32_t pxl = 160 - 2 * theRow_ + theCol_ % 2;
        LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("Expected hit in col ") << theCol_ << " row " << theRow_ << " -> dc " << dc << " pxl " << pxl);

        const auto& fedsAndChannels = getFedsAndChannels();
        for (uint32_t ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
            const uint32_t fednumber = fedsAndChannels[ifed].first;
            const uint32_t vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
            PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];
            iFED->setPixelForScore(dc, pxl);
            iFED->setAutoPhases(false);
            iFED->disableBE(true); //Disable back-end in order to avoid spyFIFO1 filling
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDTBMDelayCalibrationBPIX::beginCalibration(xoap::MessageReference msg) {
    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("In PixelFEDTBMDelayCalibrationBPIX::beginCalibration()"));

    pos::PixelCalibConfiguration *tempCalibObject = getCalibObject();
    tempCalibObject->writeASCII(outputDir());

    // settings for different options
    // The following settings have a menaing only for the Scores readout, that is ReadFifo1=no
    // TBMHeadersOnly_, headersOnly_, ignoreWordCount_, checkReadback_.
    dumpFIFOs_ = tempCalibObject->parameterValue("DumpFIFOs") == "yes";
    keep400MhzFixed_ = (tempCalibObject->parameterValue("Keep400MhzFixed") == "yes");
    keep160MhzFixed_ = (tempCalibObject->parameterValue("Keep160MhzFixed") == "yes");
    headersOnly_ = tempCalibObject->parameterValue("HeadersOnly") == "yes";
    TBMHeadersOnly_ = tempCalibObject->parameterValue("TBMHeadersOnly") == "yes";
    forceNoTokenPass_ = tempCalibObject->parameterValue("ForceNoTokenPass") == "yes";
    ignoreWordCount_ = tempCalibObject->parameterValue("IgnoreWordCount") == "yes";
    checkReadback_ = tempCalibObject->parameterValue("CheckReadback") == "yes"; // does not work
    orChannels_ = tempCalibObject->parameterValue("OrChannels") == "yes";
    swapSettings_ = tempCalibObject->parameterValue("SwapSettings") == "yes";
    findRange_ = tempCalibObject->parameterValue("findRange") == "yes";
    ignoreBadSettings_ = tempCalibObject->parameterValue("IgnoreBadSettings") != "no"; // ignore some 400MHz settings (0,4,6) in analysis part

    nCalls_ = 0;

    inject_ = false;
    const std::vector<std::vector<uint32_t> > cols = tempCalibObject->columnList();
    const std::vector<std::vector<uint32_t> > rows = tempCalibObject->rowList();
    if (cols[0].size() != 0 && rows[0].size() != 0)
        inject_ = true;

    for (uint32_t dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        std::vector<uint32_t> dacvals = tempCalibObject->scanValues(dacname);
        if (dacvals.size() > 1)
            dacsToScan_.push_back(dacname);

        for (uint32_t i = 0; i < dacvals.size(); ++i)
            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(" dac value ") << i << " is " << dacvals[i]);
    }

    BookEm("");

    // find the default settings
    for (std::map<std::string, std::vector<TH2F *> >::iterator it = TBMsHistoSum_.begin();
         it != TBMsHistoSum_.end(); ++it) {

        /*check if the current TBMPLL delay value gives 100% efficiency*/
        pos::PixelTBMSettings *TBMSettingsForThisModule = 0;
        std::string moduleName = it->first;
        PixelConfigInterface::get(TBMSettingsForThisModule, "pixel/tbm/" + moduleName, *theGlobalKey_);
        assert(TBMSettingsForThisModule != 0);

        uint32_t tmp = TBMSettingsForThisModule->getTBMPLLDelay();
        currentTBMPLLdelay_[moduleName] = tmp;
        LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(moduleName) << " 160Mhz " << ((tmp >> 5) & 0x7) << " 400Mhz " << ((tmp >> 2) & 0x7) << hex << " " << tmp << dec);
    }

    for(int i=0;i<64;++i) for(int j=0;j<11;++j) scoreMatrix[i][j]=0;

    return makeSOAPMessageReference("BeginCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDTBMDelayCalibrationBPIX::execute(xoap::MessageReference msg) {
    vector<string> parameters_names(2);
    parameters_names.push_back("WhatToDo");
    parameters_names.push_back("StateNum");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, parameters_names);

    const uint32_t state = std::stoul(parameters["StateNum"]);

    if (parameters["WhatToDo"] == "RetrieveData")
        if (readFifo1_) {
            RetrieveData(state);
        } else {
            RetrieveDataScores(state);
        }
    else if (parameters["WhatToDo"] == "Analyze")
        Analyze();
    else {
        LOG4CPLUS_FATAL(logger_, LOG4CPLUS_TEXT("PixelFEDTBMDelayCalibrationBPIX::execute() does not understand the WhatToDo command, ") << parameters[ "WhatToDo"] << ", sent to it.");
        assert(0);
    }

    return makeSOAPMessageReference("FEDCalibrationsDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDTBMDelayCalibrationBPIX::endCalibration(xoap::MessageReference msg) {
    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("In PixelFEDTBMDelayCalibrationBPIX::endCalibration()"));
    
    const auto& fedsAndChannels = getFedsAndChannels();
    for (uint32_t ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const uint32_t fednumber = fedsAndChannels[ifed].first;
        const uint32_t vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface* iFED = FEDInterface_[vmeBaseAddress];
        iFED->setAutoPhases(true);
        iFED->disableBE(false);
    }
    //cout<<" here 1"<<endl;

    return makeSOAPMessageReference("EndCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDTBMDelayCalibrationBPIX::RetrieveDataScores(uint32_t state) {
    static uint32_t state0 = 0, count = 0;
    bool Dumps = dumpFIFOs_;

    if (state != state0) { // new state
        state0 = state;
        count = 0;
        event_ = 0;
    }
    ++count;
    ++nCalls_;

    pos::PixelCalibConfiguration *tempCalibObject = getCalibObject();

    if (Dumps) {
      ostringstream msg; msg << "New FEDTBMDelay event " << event_ << " state " << state << " "<<" count, ncalls "<<count<<" "<<nCalls_<<" ";
        for (uint32_t dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
            const std::string &dacname = tempCalibObject->scanName(dacnum);
            const uint32_t dacvalue = tempCalibObject->scanValue(tempCalibObject->scanName(dacnum), state);
            msg << dacname << " -> " << dacvalue << " ";
        }
        LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
	cout<<msg.str()<<endl;
    }

    //const bool read_osd_now = nCalls_ % 32 == 0;   // this does not work anyway

    const auto& fedsAndChannels = getFedsAndChannels();

    for (uint32_t ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const uint32_t fednumber = fedsAndChannels[ifed].first;
        std::vector<bool> channels_used(49, false); // channels 1-48
        for (uint32_t ich = 0; ich < fedsAndChannels[ifed].second.size(); ++ich)
            channels_used[fedsAndChannels[ifed].second[ich]] = true;

        if (Dumps) cout<<"FED NUMBER " << fednumber<<endl;

        const uint32_t vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface *fed = FEDInterface_[vmeBaseAddress];

        // Read scores
        std::vector<uint32_t> scores = fed->getScores();

        // Read ODS
        //std::vector<uint32_t> osds(24, 0);
        //if (checkReadback_ && read_osd_now) {
	//  osds = fed->readOSDValues();
        //}

        for (uint32_t fiber = 1; fiber <= 24; ++fiber) {
	  if (Dumps) cout<<"fiber " << fiber<<endl;
	  const uint32_t chA = fiber * 2 - 1;
	  const uint32_t chB = fiber * 2;
	  
	  const bool chAused = channels_used[chA];
	  const bool chBused = channels_used[chB];
	  
	  if (!chAused && !chBused) continue;

	  const uint32_t rocSizeA = (theNameTranslation_->getROCsFromFEDChannel(fednumber, chA)).size();
	  const uint32_t rocSizeB = (theNameTranslation_->getROCsFromFEDChannel(fednumber, chB)).size();
            
	  if (rocSizeA != rocSizeB)
	    cout<<"ROC size A is different from B: should not happen?"<<endl;

	  //const uint32_t osd_roc = ((nCalls_ - 1) / 32) % 8;
	  //const uint32_t osd_roc = ((nCalls_ - 1) / 32) % rocSizeA;
	  if(Dumps) {
	    //cout<<" read osd now is " << read_osd_now << " roc " << osd_roc<<endl;
	    cout<<" roc size " << chA << " " << rocSizeA << " " << chB << " " << rocSizeB<<endl;
	  }
	  const uint32_t scoreA = scores[chA - 1];
	  const uint32_t scoreB = scores[chB - 1];

	  //const uint32_t AOSD = osds[fiber - 1] & 0xFFFF;
	  //const uint32_t BOSD = osds[fiber - 1] >> 16;
	  
	  if (Dumps) {
	    const uint32_t chs[2] = { chA, chB };
	    const bool chused[2] = { chAused, chBused };
	    const uint32_t scores[2] = { scoreA, scoreB };
	    //const uint32_t osds[2] = {AOSD, BOSD};
	    std::ostringstream msg; 
	    msg << "fiber " << fiber << " scores: " << count << " " << nCalls_ << "\n";
	    cout << "fiber " << fiber << " scores: " << count << " " << nCalls_ << "\n";
	    for (size_t i = 0; i < 2; ++i) {
	      if (chused[i]) {
		msg << "ch " << std::setw(2) << chs[i] << ": wc DDDDDDDD rrrrrrrr RRRRRRRR TH\n"
		    << "       " << std::setw(2) << (scores[i] >> 26) << " "
		    << std::bitset<8>((scores[i] >> 18) & 0xFF) << " "
		    << std::bitset<8>((scores[i] >> 10) & 0xFF) << " "
		    << std::bitset<8>((scores[i] >> 2) & 0xFF) << " "
		    << std::bitset<2>(scores[i] & 0x3)
		    << "\n";
		cout << "ch " << std::setw(2) << chs[i] << ": wc DDDDDDDD rrrrrrrr RRRRRRRR TH\n"
		     << "       " << std::setw(2) << (scores[i] >> 26) << " "
		     << std::bitset<8>((scores[i] >> 18) & 0xFF) << " "
		     << std::bitset<8>((scores[i] >> 10) & 0xFF) << " "
		     << std::bitset<8>((scores[i] >> 2) & 0xFF) << " "
		     << std::bitset<2>(scores[i] & 0x3)
		     << endl;
	      }
	      //uint32_t scores1 = fed->getScore(chA);
	      //uint32_t scores2 = fed->getScore(chB);
	      //cout<<hex<<scores1<<" "<<scores2<<dec<<endl;
	      // if (checkReadback_ && read_osd_now) {
	      // 	msg << "fiber " << fiber << " osds:\n";
	      // 	cout << "fiber " << fiber << " osds:\n";
	      // 	for (size_t n = 0; n < 2; ++n) {
	      // 	  cout<<" ch osd "<<n<<endl;
	      // 	  if (chused[n]) {
	      // 	    msg  << "ch " << std::setw(2) << chs[n] << ": iiii rrrr oooooooo\n"
	      // 		 << "       "
	      // 		 << std::bitset<4>((osds[n] >> 12) & 0xF) << " "
	      // 		 << std::bitset<4>((osds[n] >>  8) & 0xF) << " "
	      // 		 << std::bitset<8>(osds[n] & 0xFF)
	      // 		 << "\n";
	      // 	    cout  << "ch " << std::setw(2) << chs[n] << ": iiii rrrr oooooooo\n"
	      // 		  << "       "
	      // 		  << std::bitset<4>((osds[n] >> 12) & 0xF) << " "
	      // 		  << std::bitset<4>((osds[n] >>  8) & 0xF) << " "
	      // 		  << std::bitset<8>(osds[n] & 0xFF)
	      // 		  << "\n";
	      // 	  }
	      // 	}
	      // }
	      //LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
	    } // channels
	  } // if Dumps
	  
            // format is ccccccDDDDDDDDrrrrrrrrRRRRRRRRTH
            // c counts the number of words (0-63)
            // the D bits are one per roc, 1 if has the right pixel hit
            // r should all be 0 if no additional roc headers
            // R is whether that roc header was there
            // T for tbm trailer
            // H for tbm header
            bool AOK, BOK;
            uint32_t perfect_score = 0x4bfc03ff; // 18 words, 8 hits, 8 ROC headers, TBM header+trailer
            if (rocSizeA == 4)
                perfect_score = 0x283C003F; // 10 words, 4 hits, 4 ROC headers, TBM header+trailer
            else if (rocSizeA == 2)
                perfect_score = 0x180C000F; // 6 words, 2 hits, 2 ROC headers, TBM header+trailer

            const uint32_t perfect_score_ignore_wordcount = perfect_score & 0x3ffffff;
            const uint32_t perfect_score_headers_only = perfect_score & 0x3ff;
            const uint32_t perfect_score_tbmheaders_only = perfect_score & 0x3;

            if (TBMHeadersOnly_) {
                if (forceNoTokenPass_) {
                    // In this case we really expect no ROC headers -> check rR bits are 0
                    // so perfect headers is (score & 0x3ffff) == 0x3
                    AOK = (scoreA & 0x3ffff) == perfect_score_tbmheaders_only;
                    BOK = (scoreB & 0x3ffff) == perfect_score_tbmheaders_only;
                } else {
                    // In this case we don't check the result of the token pass, we only care about TBM header+trailer
                    // so perfect headers is (score & 0x3) == 0x3
                    AOK = (scoreA & 0x3) == perfect_score_tbmheaders_only;
                    BOK = (scoreB & 0x3) == perfect_score_tbmheaders_only;
                }
                if(Dumps) cout<<"the tbmonly score is " << AOK << " " << BOK << " " << std::hex << scoreA << " " << scoreB << std::dec<<endl;
            } else if (headersOnly_) {
	      // so perfect headers is (score & 0x3ffff) == 0x3ff
	      AOK = ((scoreA & 0x3ffff) == perfect_score_headers_only);
	      BOK = ((scoreB & 0x3ffff) == perfect_score_headers_only);
	      if(Dumps) cout<<"headers only score is " << AOK << " " << BOK << " " << std::hex << scoreA << " " << scoreB << std::dec<<endl;
            } else if (ignoreWordCount_) {
	      AOK = ((scoreA & 0x3ffffff) == perfect_score_ignore_wordcount);
	      BOK = ((scoreB & 0x3ffffff) == perfect_score_ignore_wordcount);
	      if(Dumps) cout<<"ignore word count score is " << AOK << " " << BOK << " " << std::hex << scoreA << " " << scoreB << std::dec<<endl;
            } else {
	      AOK = (scoreA == perfect_score);
	      BOK = (scoreB == perfect_score);
	      if(Dumps) cout<<"full score is " << AOK << " " << BOK << " " << std::hex << scoreA << " " << scoreB << std::dec<<endl;
            }

            // if (checkReadback_ && read_osd_now) {
            //     if (Dumps)
	    // 	  cout<<" osd " 
	    // 	      << osd_roc << "-" << (AOSD >> 12) % rocSizeA << " "
	    // 	      << osd_roc << "-" << (BOSD >> 12) % rocSizeA
	    // 	      << " for fiber " << fiber << " " << rocSizeA<<endl;

            //     const bool AOSD_OK = ((AOSD >> 12) % rocSizeA == osd_roc); // && ((AOSD >> 8) & 0xF) == 12;
            //     //const bool BOSD_OK = (BOSD >> 12) == osd_roc + 8; // && ((BOSD >> 8) & 0xF) == 12;
            //     const bool BOSD_OK = ((BOSD >> 12) % rocSizeA == osd_roc); // && ((BOSD >> 8) & 0xF) == 12;
            //     AOK = AOK && AOSD_OK;
            //     BOK = BOK && BOSD_OK;
            // } // if checkreadback

            if (!chAused)
                AOK = orChannels_ ? false : true;
            if (!chBused)
                BOK = orChannels_ ? false : true;

            const bool fiber_OK = orChannels_ ? (AOK || BOK) : (AOK && BOK);

            if(Dumps) cout<<"AOK "<<AOK<<" BOK "<< BOK << " size " << rocSizeA << " " << hex << scoreA << " " << scoreB << " " << perfect_score << dec << " ok= " << fiber_OK<<" or chans "<<orChannels_<<endl;

            // found tbm header & trailer in both channels
            FillEm(state, fednumber, chA, 0, fiber_OK);
            FillEm(state, fednumber, chB, 0, fiber_OK);
            if (!TBMHeadersOnly_) { // also histogram roc header
                FillEm(state, fednumber, chA, 1, AOK * rocSizeA);
                FillEm(state, fednumber, chB, 1, BOK * rocSizeA);
            }

        } // for fiber

    } // for fed

    event_++;
}
////////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDTBMDelayCalibrationBPIX::RetrieveData(uint32_t state) {
    pos::PixelCalibConfiguration *tempCalibObject = getCalibObject();

    const auto& fedsAndChannels = getFedsAndChannels();

    if ( dumpFIFOs_ )
        std::cout << "NEW FEDTBMDelay TRIGGER " << event_ << " state " << state << " ";

    std::map<std::string, uint32_t> currentDACValues;
    for (uint32_t dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        const uint32_t dacvalue = tempCalibObject->scanValue(tempCalibObject->scanName(dacnum), state);
        currentDACValues[dacname] = dacvalue;
        if(dumpFIFOs_) cout << dacname << " " << dacvalue << " ";
    }
    if(dumpFIFOs_) cout << " dacs to scan "<<dacsToScan_.size()<<endl;

    if (dacsToScan_.size() < 2 && currentDACValues["TBMPLL"] != lastTBMPLL_) {
        event_ = 0;
        lastTBMPLL_ = currentDACValues["TBMPLL"];
    }

    // Loop over FEDs
    for (uint32_t ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const uint32_t fednumber = fedsAndChannels[ifed].first;
        const uint32_t vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];

        // Loop over channels
        for (uint32_t ch = 0; ch < fedsAndChannels[ifed].second.size(); ch++) {
            uint32_t channel = (fedsAndChannels[ifed].second)[ch];

	    if(dumpFIFOs_) cout<<"fed "<<fednumber<<" channel "<<channel<<endl; 

            if (channel % 2 == 1) {
                int32_t fib = (channel - 1) / 2;
                if ( dumpFIFOs_ )
		  cout << "Reading fiber " << (fib+1) << " channels "<<channel<<" "<<(channel+1)<<endl;

                iFED->setFIFO1(fib);
                PixelPh1FEDInterface::digfifo1 d = iFED->readFIFO1(dumpFIFOs_);

		const  int rocSizeA = 
		  (theNameTranslation_->getROCsFromFEDChannel(fednumber, channel)).size();
		const int rocSizeB = 
		  (theNameTranslation_->getROCsFromFEDChannel(fednumber, (channel+1))).size();

                if(dumpFIFOs_) {
		  cout<<d.a.found<<" "<<d.b.found<<" ";
		  printf("ch a: %i ch b: %i n tbm h a: %i b: %i  tbm t a: %i b: %i  roc h a: %i b: %i\n", 
			 d.a.ch, d.b.ch, d.a.n_tbm_h, d.b.n_tbm_h, d.a.n_tbm_t, d.b.n_tbm_t, d.a.n_roc_h, d.b.n_roc_h);
		  scoreMatrix[state][0]++;

		  // look at hits 
		  cout<<"A Hits "<<d.a.hits.size()<<" rocs "<<d.a.n_roc_h<<" expected size "<<rocSizeA
		      <<", B Hits "<<d.b.hits.size()<<" rocs "<<d.b.n_roc_h<<" expected size "<<rocSizeB
		      <<endl;
		  for (unsigned int i = 0; i < d.a.hits.size(); i++) {
		    cout << i << ": d.a.hits " 
			 << d.a.hits[i].roc <<" "
			 << d.a.hits[i].ch <<" " 
			 << d.a.hits[i].dcol <<" " 
			 << d.a.hits[i].pxl <<" " 
			 << endl;
		  }
		  for (unsigned int i = 0; i < d.b.hits.size(); i++) {
		    cout << i << ": d.b.hits " 
			 << d.b.hits[i].roc <<" "
			 << d.b.hits[i].ch <<" " 
			 << d.b.hits[i].dcol <<" " 
			 << d.b.hits[i].pxl <<" " 
			 << endl;
		  }

		  if (channel     == d.a.ch) scoreMatrix[state][1]++;
		  if (d.a.n_tbm_h == 1) scoreMatrix[state][2]++;
		  if (d.a.n_tbm_t == 1) scoreMatrix[state][3]++;
		  if (d.a.n_roc_h == rocSizeA) scoreMatrix[state][4]++;
		  if (int(d.a.hits.size()) == rocSizeA) scoreMatrix[state][5]++;

		  if ((channel+1) == d.b.ch) scoreMatrix[state][6]++;
		  if (d.b.n_tbm_h == 1) scoreMatrix[state][7]++;
		  if (d.b.n_tbm_t == 1) scoreMatrix[state][8]++;
		  if (d.b.n_roc_h == rocSizeB) scoreMatrix[state][9]++;
		  if (int(d.b.hits.size()) == rocSizeB) scoreMatrix[state][10]++;
		}

                uint32_t chA = d.a.ch; uint32_t chB = d.b.ch;
                if ((channel != chA) || ((channel + 1) != chB)) {
		  if(dumpFIFOs_) cout<<"channel mismatch, skip "<<channel<<" "<<chA<<" "<<(channel+1)<<" "<<chB<<endl;
		  continue;
		}

                // found tbm header & trailer in both channels
                bool found_TBM = d.a.n_tbm_h && d.a.n_tbm_t && d.b.n_tbm_h && d.b.n_tbm_t;
		if(dumpFIFOs_) {
		  if (d.a.n_tbm_h && !d.a.n_tbm_t)
		    cout << " for fiber " << (fib+1) << " channel A, trailer not found " << d.a.n_roc_h << " " << channel << endl;
		  if (d.b.n_tbm_h && !d.b.n_tbm_t)
		    cout << " for fiber " << (fib+1) << " channel B, trailer not found " << d.b.n_roc_h << " " << channel << endl;
		  if (d.a.n_tbm_h != d.b.n_tbm_h)
		    cout << " for fiber " << (fib+1) << " channel " << channel
                         << " found header for 1 channel only " << d.a.n_tbm_h << " " << d.b.n_tbm_h << endl;
		}

                FillEm(state, fedsAndChannels[ifed].first, chA, 0, found_TBM);
                FillEm(state, fedsAndChannels[ifed].first, chB, 0, found_TBM);

                if (dacsToScan_.size() == 0) { // when is this done? we always scan a dac
		  cout<<" is this ever done?"<<endl;
		  for (int32_t r = 0; r < d.a.n_roc_h; ++r) {
		    FillEm(state, fedsAndChannels[ifed].first, chA, 1, r);
		  }
		  for (int32_t r = 0; r < d.b.n_roc_h; ++r) {
		    FillEm(state, fedsAndChannels[ifed].first, chB, 1, r);
		  }
                } else if (dacsToScan_.size() == 1) {  // scan dac TBM
		  uint32_t n1 = d.a.n_roc_h;
		  uint32_t n2 = d.b.n_roc_h;
		  if(!TBMHeadersOnly_ && !headersOnly_) {  // look at hits 
		    if(d.a.hits.size()<n1) n1=d.a.hits.size();
		    if(d.b.hits.size()<n2) n2=d.b.hits.size();
		  }
		  FillEm(state, fedsAndChannels[ifed].first, chA, 1, n1);
		  FillEm(state, fedsAndChannels[ifed].first, chB, 1, n2);
                }

            } // if statement for fibers
        } // end loop on channels
    } //end loop on feds

    event_++;
    //sendResets();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDTBMDelayCalibrationBPIX::Analyze() {
    if (dacsToScan_.size() == 0) {
        uint32_t ntriggers = event_ - 1;
        for (std::map<uint32_t, std::map<uint32_t, std::vector<TH1F *> > >::iterator it1 = nTrigsTBM_.begin(); it1 != nTrigsTBM_.end(); ++it1) {
            for (std::map<uint32_t, std::vector<TH1F *> >::iterator it2 = it1->second.begin(); it2 != it1->second.end(); ++it2) {
                for (uint32_t i = 0; i < it2->second.size(); ++i)
                    it2->second[i]->Scale(1. / ntriggers);
            }
        }
        CloseRootf();
        return;
    }

    const auto& fedsAndChannels = getFedsAndChannels();

    // all maps per module:
    std::map<std::string, uint32_t> bestTBMPLLSettings;
    std::map<std::string, uint32_t> currentTBMPLLdelay;
    std::map<std::string, uint32_t> passState;
    std::map<std::string, double> effForCurrentTBMPLLdelay;
    std::map<std::string, double> effForBestTBMPLLdelay;
    std::map<std::string, std::pair<uint32_t, std::vector<uint32_t>> > crateAndFEDchannelsPerModule;
    
    for (const auto& fedNumberAndChannels: fedsAndChannels) {
        const uint32_t fedNumber = fedNumberAndChannels.first;
        const uint32_t crate = theFEDConfiguration_->crateFromFEDNumber(fedNumber);

        for (const uint32_t channelNumber: fedNumberAndChannels.second) {
            pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(fedNumber, channelNumber);
            std::string moduleName = theChannel.modulename();
            crateAndFEDchannelsPerModule[moduleName].first = crate;
            crateAndFEDchannelsPerModule[moduleName].second.push_back(channelNumber);
        }
    }

    // Normalize by number of triggers and fill histo with sum of channels
    for (std::map<uint32_t, std::map<uint32_t, std::vector<TH2F *> > >::iterator it1 = scansTBM_.begin(); it1 != scansTBM_.end(); ++it1) {
        std::string moduleName = "";
        pos::PixelChannel theChannel;
        for (std::map<uint32_t, std::vector<TH2F *> >::iterator it2 = it1->second.begin(); it2 != it1->second.end(); ++it2) {
            theChannel = theNameTranslation_->ChannelFromFEDChannel(it1->first, it2->first);
            moduleName = theChannel.modulename();
            std::vector<TH2F*> &chScanHists = it2->second;

            // We do the normalization bin-by-bin using the accumulated number of triggers in nTrigsPerPoint_ since
            // keepXXXMhzFixed settings *may* cause the number of triggers to be different from event_ - 1
            // (depending on how the scan range is set)
            TH2F *nTrigsCh = nTrigsPerPoint_[it1->first][it2->first];
            for (uint32_t binX = 1; binX <= 8; binX++) {
                for (uint32_t binY = 1; binY <= 8; binY++) {
                    float nTrigs = nTrigsCh->GetBinContent(binX, binY);
                    if (nTrigs) {
                        for (std::size_t i = 0; i < chScanHists.size(); i++)
                            chScanHists[i]->SetBinContent(binX, binY, chScanHists[i]->GetBinContent(binX, binY) / nTrigs);
                    }
                }
            }

            //count bins with eff = 100%
            uint32_t nGoodBins = 0;
            for (int32_t bx = 1; bx < chScanHists[0]->GetNbinsX() + 1; ++bx) {
                for (int32_t by = 1; by < chScanHists[0]->GetNbinsY() + 1; ++by) {
                    if (chScanHists[0]->GetBinContent(bx, by) == 1)
                        nGoodBins++;
                } //close loop on by
            } //close loop on bx

            for (uint32_t i = 0; i < chScanHists.size(); ++i) {
                // to normalize the sum histo by number of fed channels
                // normalization should be done for TBM Header/Trailer only, not for received ROC headers
                float scale = (string(chScanHists[i]->GetTitle()).find("nROCHeaders") != string::npos) ? 1 :
                                    (1. / crateAndFEDchannelsPerModule[moduleName].second.size());
                TBMsHistoSum_[moduleName][i]->Add(chScanHists[i], scale);
            }

            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(" analyse ") << moduleName << " " << nGoodBins << " " << crateAndFEDchannelsPerModule[moduleName].second.size());
        } //close loop on channels
    } //close loop on fed

    //find best settings for each module
    for (std::map<std::string, std::vector<TH2F *> >::iterator it = TBMsHistoSum_.begin(); it != TBMsHistoSum_.end(); ++it) {

        /*check if the current TBMPLL delay value gives 100% efficiency*/
        pos::PixelTBMSettings *TBMSettingsForThisModule = 0;
        std::string moduleName = it->first; //(module_name->modulename());
        PixelConfigInterface::get(TBMSettingsForThisModule, "pixel/tbm/" + moduleName, *theGlobalKey_);
        assert(TBMSettingsForThisModule != 0);

        currentTBMPLLdelay[moduleName] = TBMSettingsForThisModule->getTBMPLLDelay();
        int32_t bestX = -1, bestY = -1; // best values found in the scan
        bool hasBad400Mhz = false;

        // First check old settings
        uint32_t delayX = (currentTBMPLLdelay[moduleName] >> 2) & 0x7;
        uint32_t delayY = (currentTBMPLLdelay[moduleName] >> 5) & 0x7;

        if (!keep400MhzFixed_ && ignoreBadSettings_ && (delayX == 0 || delayX == 4 || delayX == 6))
            hasBad400Mhz = true;

        if (swapSettings_) {
            std::stringstream msg; msg << " swaping 400MHz setting from " << delayX;
            delayX = delay1Corrected[delayX];
            msg << " to " << delayX;
            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
        }

        bestX = delayX; // set initial value to the old one
        bestY = delayY;

        effForCurrentTBMPLLdelay[moduleName] = it->second[0]->GetBinContent(delayX + 1, delayY + 1);

        std::stringstream msg; msg << moduleName << " old settings " << currentTBMPLLdelay[moduleName] << " "
                    << delayX << " " << delayY << " "
                    << crateAndFEDchannelsPerModule[moduleName].second.size() << " " << effForCurrentTBMPLLdelay[moduleName];
        LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));

        // if efficiency for current tbmpll delay != 100% search for new best value; if efficiency == 100% keep the current value
        // Accept old settings only if the findRange_ is not set to true. findRange_=true forces a new search.
        // If the old 400MHz settings are "bad" and we're scanning 400MHz, also trigger new search.
        if (effForCurrentTBMPLLdelay[moduleName] == 1 && !findRange_ && !hasBad400Mhz) { // check old settings, look OK,
            passState[moduleName] = 1;
            bestTBMPLLSettings[moduleName] = currentTBMPLLdelay[moduleName];
            effForBestTBMPLLdelay[moduleName] = effForCurrentTBMPLLdelay[moduleName];
            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(" good efficiency for old point ") << bestTBMPLLSettings[moduleName] << " " << effForBestTBMPLLdelay[moduleName]);
        } else {
            // Search for best values
            // Because the settings do not trivially map to actual phases, it does not make sense to look for a "good region",
            // unless the exact delays of each setting are known.
            // So we just look for a point with max. efficiency that is not a forbidden one.

            float maxEff = 0;
            
            // loop over 400Mhz
            for (int32_t bx = 0; bx < it->second[0]->GetNbinsX(); ++bx) {
                if (keep400MhzFixed_ && bx != bestX)
                    continue;
                if (ignoreBadSettings_) { // skip invalid settings
                    if ((!swapSettings_ && ((bx == 0) || (bx == 4) || (bx == 6))) ||
                        (swapSettings_ && ((bx == 0) || (bx == 2) || (bx == 3)))) {
                            continue;
                    }
                }

                // loop over 160Mhz
                for (int32_t by = 0; by < it->second[0]->GetNbinsY(); ++by) {
                    if (keep160MhzFixed_ && by != bestY)
                        continue;
                    float eff = it->second[0]->GetBinContent(bx + 1, by + 1);
                    if (eff > maxEff) {
                        bestX = bx;
                        bestY = by;
                        maxEff = eff;
                    }
                } // end loop over 160MHz
            } // end loop over 400MHz

            if (it->second[0]->GetBinContent(bestX + 1, bestY + 1) != 1)
                passState[moduleName] = 0;
            else
                passState[moduleName] = 1;
            effForBestTBMPLLdelay[moduleName] = it->second[0]->GetBinContent(bestX + 1, bestY + 1);
            // Translate back the 400MHz index to the right one
            if (swapSettings_) {
                std::stringstream msg; msg << " swap back 400MHz setting from " << bestX;
                bestX = delay1Original[bestX];
                msg << " to " << bestX;
                LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
            }
            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(moduleName) << " : Best settings (400,160)MHz = " << bestX << " " << bestY);
            bestX = (bestX << 2);
            bestY = (bestY << 5);
            bestTBMPLLSettings[moduleName] = bestX + bestY;
        } //close case eff!=1
    } //close loop on modules

    // Save results
    rootf_->cd();
    branch theBranch;
    branch_sum theBranch_sum;
    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("PassState", "PassState");
    TTree *tree_sum = new TTree("SummaryInfo", "SummaryInfo");

    tree->Branch("PassState", &theBranch, "pass/F:moduleName/C", 4096000);
    tree_sum->Branch("SummaryInfo", &theBranch_sum, "deltaTBMPLLdelayX/I:deltaTBMPLLdelayY/I:newTBMPLLdelayX/I:newTBMPLLdelayY/I:efficiency/D:moduleName/C", 4096000);
    rootf_->cd();

    // Save results in TBM config files
    std::vector<pos::PixelModuleName> modules = theDetectorConfiguration_->getModuleList();
    for (std::map<std::string, std::vector<TH2F *> >::iterator it = TBMsHistoSum_.begin(); it != TBMsHistoSum_.end(); ++it) {
        std::string moduleName = it->first;

        //prevent the calibration to overwrite ASCII output files which are not of this FEDSupervisor's business.
        if (crate_ != crateAndFEDchannelsPerModule[moduleName].first)
            continue;

        uint32_t oldDelayX = (currentTBMPLLdelay[moduleName] >> 2) & 0x7;
        uint32_t oldDelayY = (currentTBMPLLdelay[moduleName] >> 5) & 0x7;
        uint32_t newDelayX = (bestTBMPLLSettings[moduleName] >> 2) & 0x7;
        uint32_t newDelayY = (bestTBMPLLSettings[moduleName] >> 5) & 0x7;

        if (keep400MhzFixed_) { // overwrite for fixed scan
            newDelayX = oldDelayX;
            bestTBMPLLSettings[moduleName] =
                (bestTBMPLLSettings[moduleName] & 0xE3) | ((oldDelayX & 0x7) << 2);
        }

        if (keep160MhzFixed_) { // overwrite for fixed scan
            newDelayY = oldDelayY;
            bestTBMPLLSettings[moduleName] =
                (bestTBMPLLSettings[moduleName] & 0x1F) | ((oldDelayY & 0x7) << 5);
        }

        // Save settings
        pos::PixelTBMSettings *TBMSettingsForThisModule = 0;
        PixelConfigInterface::get(TBMSettingsForThisModule, "pixel/tbm/" + moduleName, *theGlobalKey_);
        assert(TBMSettingsForThisModule != 0);

        if (passState[moduleName] == 1) { // set new values
            TBMSettingsForThisModule->setTBMPLLDelay(bestTBMPLLSettings[moduleName]);
            if (effForBestTBMPLLdelay[moduleName] < 1.) // efficiency below 1
                LOG4CPLUS_INFO(logger_, LOG4CPLUS_TEXT(" For module ") << moduleName << " the efficiency is below 1 " << effForBestTBMPLLdelay[moduleName]
                     << " pass " << passState[moduleName]);
            if (crateAndFEDchannelsPerModule[moduleName].second.size() == 0) // check if there were valid channels
                LOG4CPLUS_ERROR(logger_, LOG4CPLUS_TEXT(" Something wrong for module ") << moduleName << " passState = " << passState[moduleName]
                     << " eff " << effForBestTBMPLLdelay[moduleName] << " but no valid channels");

        } else { // Did not pass, keep old valuse

            TBMSettingsForThisModule->setTBMPLLDelay(currentTBMPLLdelay[moduleName]);
            // overwrite with old values
            bestTBMPLLSettings[moduleName] = currentTBMPLLdelay[moduleName];
            newDelayX = oldDelayX;
            newDelayY = oldDelayY;
            if (crateAndFEDchannelsPerModule[moduleName].second.size() > 0)
                LOG4CPLUS_INFO(logger_, LOG4CPLUS_TEXT(" Module ") << moduleName
                     << " did not pass, the highest efficiency is " << effForBestTBMPLLdelay[moduleName]
                     << " valid channels " << crateAndFEDchannelsPerModule[moduleName].second.size());
        }

        TBMSettingsForThisModule->writeASCII(outputDir());
        LOG4CPLUS_INFO(logger_, LOG4CPLUS_TEXT("Wrote TBM settings for module:") << moduleName
                  << " old settings "
                  << " " << oldDelayX << " " << oldDelayY << " new "
                  << newDelayX << " " << newDelayY << " eff " << effForBestTBMPLLdelay[moduleName] << " chan "
                  << crateAndFEDchannelsPerModule[moduleName].second.size() << " pass "
                  << passState[moduleName]);

        delete TBMSettingsForThisModule;

        // save root tree
        theBranch_sum.deltaTBMPLLdelayX = (int32_t)newDelayX - (int32_t)oldDelayX;
        theBranch_sum.deltaTBMPLLdelayY = (int32_t)newDelayY - (int32_t)oldDelayY;
        theBranch_sum.newTBMPLLdelayX = newDelayX;
        theBranch_sum.newTBMPLLdelayY = newDelayY;
        theBranch_sum.efficiency = effForBestTBMPLLdelay[moduleName];
        strcpy(theBranch_sum.moduleName, moduleName.c_str());
        theBranch.pass = passState[moduleName];
        strcpy(theBranch.moduleName, moduleName.c_str());

        tree->Fill();
        tree_sum->Fill();
    }

    CloseRootf();

    //now print summary and save it on text file
    string s = pixel::utils::to_string(crate_);
    ofstream out((outputDir() + "/summary_" + s + ".txt").c_str());
    assert(out.good()); //file method
    stringstream msg; msg << "\n";

    out << "                                  |              | old    | old    | new    | new    | delta  |        |        |        |\n";
    out << "                                  |              | 400MHz | 160MHz | 400MHz | 160MHz | delta  |  old   |  new   |        |\n";
    out << "Module                            | FED channels | phase  | phase  | phase  | phase  | TBMPLL |  eff.  |  eff.  |  pass  |\n";
    msg << "                                  |              | old    | old    | new    | new    | delta  |        |        |        |\n";
    msg << "                                  |              | 400MHz | 160MHz | 400MHz | 160MHz | delta  |  old   |  new   |        |\n";
    msg << "Module                            | FED channels | phase  | phase  | phase  | phase  | TBMPLL |  eff.  |  eff.  |  pass  |\n";

    for (const auto& module_crateAndFed: crateAndFEDchannelsPerModule) {
        const std::string moduleName = module_crateAndFed.first;
        const std::vector<uint32_t> &channels = module_crateAndFed.second.second;
        out << std::setw(33) << std::left << moduleName << " | ";
        msg << std::setw(33) << std::left << moduleName << " | ";

        for (uint32_t i = 0; i < channels.size(); ++i) {
            out << std::setw(2) << channels[i] << " ";
            msg << std::setw(2) << channels[i] << " ";
        }
        if (channels.size() < 4) {
            for (std::size_t r = 0; r < 4 - channels.size(); r++) {
                out << "   ";
                msg << "   ";
            }
        }
        out << " | ";
        out << std::setw(6) << ((currentTBMPLLdelay[moduleName] >> 2) & 0x7);
        out << " | ";
        out << std::setw(6) << ((currentTBMPLLdelay[moduleName] >> 5) & 0x7);
        out << " | ";
        out << std::setw(6) << ((bestTBMPLLSettings[moduleName] >> 2) & 0x7);
        out << " | ";
        out << std::setw(6) << ((bestTBMPLLSettings[moduleName] >> 5) & 0x7);
        out << " | ";
        out << std::setw(6) << (int32_t)bestTBMPLLSettings[moduleName] - (int32_t)currentTBMPLLdelay[moduleName];
        out.precision(4);
        out << " | ";
        out << std::setw(6) << effForCurrentTBMPLLdelay[moduleName];
        out << " | ";
        out << std::setw(6) << effForBestTBMPLLdelay[moduleName];
        out << " | ";
        out << std::setw(6) << passState[moduleName];
        out << " |\n";

        msg << " | ";
        msg << std::setw(6) << ((currentTBMPLLdelay[moduleName] >> 2) & 0x7);
        msg << " | ";
        msg << std::setw(6) << ((currentTBMPLLdelay[moduleName] >> 5) & 0x7);
        msg << " | ";
        msg << std::setw(6) << ((bestTBMPLLSettings[moduleName] >> 2) & 0x7);
        msg << " | ";
        msg << std::setw(6) << ((bestTBMPLLSettings[moduleName] >> 5) & 0x7);
        msg << " | ";
        msg << std::setw(6) << (int32_t)bestTBMPLLSettings[moduleName] - (int32_t)currentTBMPLLdelay[moduleName];
        msg.precision(4);
        msg << " | ";
        msg << std::setw(6) << effForCurrentTBMPLLdelay[moduleName];
        msg << " | ";
        msg << std::setw(6) << effForBestTBMPLLdelay[moduleName];
        msg << " | ";
        msg << std::setw(6) << passState[moduleName];
        msg << " |\n";
    }

    if(dumpFIFOs_) {
      for(int i=0;i<64;++i) {
	cout<<"bin "<<i<<" ";
	for(int j=0;j<11;++j) {
	  cout<<scoreMatrix[i][j]<<" ";
	}
	cout<<endl;
      }
    } // dumpFIFOs

    LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDTBMDelayCalibrationBPIX::CloseRootf() {
    if ( rootf_ ) {
        rootf_->Write();
        rootf_->Close();
        delete rootf_;
    }
    //cout<<" here 3"<<endl;

}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDTBMDelayCalibrationBPIX::BookEm(const TString &path) {

    nTrigsTBM_.clear();
    scansTBM_.clear();
    nTrigsPerPoint_.clear();
    TBMsHistoSum_.clear();

    TString root_fn;
    if (path == "")
        root_fn.Form("%s/TBMDelay_%lu.root", outputDir().c_str(), crate_);
    else
        root_fn.Form("%s/TBMDelay_%lu_%s.root", outputDir().c_str(), crate_, path.Data());

    CloseRootf();
    rootf_ = new TFile(root_fn, "update");
    assert(rootf_->IsOpen());

    const auto& fedsAndChannels = getFedsAndChannels();

    PixelRootDirectoryMaker rootDirs(fedsAndChannels, gDirectory);

    // book two histograms per channel, for the TBM and ROC headers
    for (uint32_t ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const uint32_t fedNumber = fedsAndChannels[ifed].first;
        std::map<uint32_t, std::vector<TH1F *> > chTBMmap;
        std::map<uint32_t, std::vector<TH2F *> > chTBMmap2D;
        std::map<uint32_t, TH2F*> chNTrigsPerPoint;
        
        for (uint32_t ch = 0; ch < fedsAndChannels[ifed].second.size(); ch++) {
            const uint32_t chNumber = (fedsAndChannels[ifed].second)[ch];

            rootDirs.cdDirectory(fedNumber, chNumber);

            if (dacsToScan_.size() == 0) {

                TString hname;
                hname.Form("Ch%i", chNumber);
                std::vector<TH1F *> histosTBM;
                TH1F *h_TBM_nDecodes = new TH1F(hname + "_TBM_nDecodes", hname + "_TBM_nDecodes", 2, 0, 2);
                histosTBM.push_back(h_TBM_nDecodes);
                TH1F *h_nROCHeaders = new TH1F(hname + "_nROCHeaders", hname + "_nROCHeaders", 9, 0, 9);
                histosTBM.push_back(h_nROCHeaders);

                chTBMmap[chNumber] = histosTBM;

            } // end book histos for zero dacsToScan_ case

            if (dacsToScan_.size() == 1) {

                TString hname;
                hname.Form("Ch%i", chNumber);
                std::vector<TH2F *> histosTBM;
                TH2F *h_TBM_nDecodes = new TH2F(hname + "_TBM_nDecodes", hname + "_TBM_nDecodes", 8, 0, 8, 8, 0, 8);
                h_TBM_nDecodes->SetXTitle("400 MHz phase");
                h_TBM_nDecodes->SetYTitle("160 MHz phase");
                histosTBM.push_back(h_TBM_nDecodes);
                TH2F *h_nROCHeaders = new TH2F(hname + "_nROCHeaders", hname + "_nROCHeaders", 8, 0, 8, 8, 0, 8);
                h_nROCHeaders->SetXTitle("400 MHz phase");
                h_nROCHeaders->SetYTitle("160 MHz phase");
                histosTBM.push_back(h_nROCHeaders);

                chTBMmap2D[chNumber] = histosTBM;
                
                TH2F *h_nTrigsPerPoint_ = new TH2F(hname + "_nTrigsPerPoint_", hname + "_nTrigsPerPoint_", 8, 0, 8, 8, 0, 8);
                h_nTrigsPerPoint_->SetDirectory(0); // will not be stored
                chNTrigsPerPoint[chNumber] = h_nTrigsPerPoint_;
            } // end book histos for 1 dacsToScan_ case (TBMPLL scan)

        } //close loop on channels

        if (dacsToScan_.size() == 0)
            nTrigsTBM_[fedNumber] = chTBMmap;
        if (dacsToScan_.size() == 1) {
            scansTBM_[fedNumber] = chTBMmap2D;
            nTrigsPerPoint_[fedNumber] = chNTrigsPerPoint;
        }

    } //close loop on feds

    rootf_->cd();

    // book two histogram per module, for sum of channels of that module (TBM and ROC headers resp.)
    if (dacsToScan_.size() == 1) {
        std::set<pos::PixelChannel> names = theNameTranslation_->getChannels();
        PixelRootDirectoryMaker rootDirsModules(names, gDirectory);

        for (std::set<pos::PixelChannel>::iterator PixelChannel_itr = names.begin(), PixelChannel_itr_end = names.end(); PixelChannel_itr != PixelChannel_itr_end; ++PixelChannel_itr) {
            rootDirsModules.cdDirectory((*PixelChannel_itr));
            std::string moduleName = (*PixelChannel_itr).modulename();
            std::vector<TH2F *> histosTBM;
            TString hname(moduleName);

            if (TBMsHistoSum_.find(moduleName) != TBMsHistoSum_.end())
                continue;

            TH2F *h_TBM_nDecodes = new TH2F(hname + "_nTBMDecodes", hname + "_nTBMDecodes", 8, 0, 8, 8, 0, 8);
            h_TBM_nDecodes->SetXTitle("400 MHz phase");
            h_TBM_nDecodes->SetYTitle("160 MHz phase");
            histosTBM.push_back(h_TBM_nDecodes);
            TH2F *h_nROCHeaders = new TH2F(hname + "_nROCHeaders", hname + "_nROCHeaders", 8, 0, 8, 8, 0, 8);
            h_nROCHeaders->SetXTitle("400 MHz phase");
            h_nROCHeaders->SetYTitle("160 MHz phase");
            histosTBM.push_back(h_nROCHeaders);

            TBMsHistoSum_[moduleName] = histosTBM;
        }
    } //end booking sum histo

    rootf_->cd(0);
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDTBMDelayCalibrationBPIX::FillEm(uint32_t state, uint32_t fedid, uint32_t ch, uint32_t which, float c) {
    if (!theNameTranslation_->FEDChannelExist(fedid, ch)) {
        LOG4CPLUS_ERROR(logger_, LOG4CPLUS_TEXT("Trying to fill for FED ") << fedid << " and channel " << ch);
        return;
    }

    if (event_ == 0)
        return;

    if (dacsToScan_.size() == 0)
        nTrigsTBM_[fedid][ch][which]->Fill(c, 1);
    
    pos::PixelCalibConfiguration *tempCalibObject = getCalibObject();

    if (dacsToScan_.size() == 1) {
        const std::string &iname = dacsToScan_[0];
        const uint32_t dacValue = tempCalibObject->scanValue(iname, state);
        pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(fedid, ch);
        const std::string moduleName = theChannel.modulename();

        uint32_t delay1 = (dacValue >> 2) & 0x7; // 400MHz
        if (keep400MhzFixed_) {
            std::stringstream msg; msg << " for fed/ch " << fedid << "/" << ch << " " << moduleName;
            delay1 = (currentTBMPLLdelay_[moduleName] >> 2) & 0x7;
            msg << " 400Mhz "<< delay1 << std::hex << " " << currentTBMPLLdelay_[moduleName] << std::dec;
            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
        }

        uint32_t delay2 = (dacValue >> 5) & 0x7; // 160MHz
        if (keep160MhzFixed_) {
            std::stringstream msg; msg << " for fed/ch " << fedid << "/" << ch << " " << moduleName;
            delay2 = (currentTBMPLLdelay_[moduleName] >> 5) & 0x7;
            msg << " 160Mhz "<< delay2 << std::hex << " " << currentTBMPLLdelay_[moduleName] << std::dec;
            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
        }

        if (swapSettings_) {
            std::stringstream msg; msg << " swap 400MHz setting from " << delay1;
            delay1 = delay1Corrected[delay1];
            msg << " to " << delay1;
            LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT(msg.str()));
        }
        scansTBM_[fedid][ch][which]->Fill(delay1, delay2, c);
        if (which == 0) // only count triggers for the TBM headers histo, not the ROC headers
            nTrigsPerPoint_[fedid][ch]->Fill(delay1, delay2, 1);
    }

}
