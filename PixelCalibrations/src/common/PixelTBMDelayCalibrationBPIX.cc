#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelTBMDelayCalibrationBPIX.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

//#include <toolbox/convertstring.h>

using namespace pos;
using namespace std;

PixelTBMDelayCalibrationBPIX::PixelTBMDelayCalibrationBPIX(const PixelSupervisorConfiguration &tempConfiguration, SOAPCommander *mySOAPCmdr)
    : PixelCalibrationBase(tempConfiguration, *mySOAPCmdr), logger_(app_->getApplicationLogger()) {
        LOG4CPLUS_INFO(logger_, "Greetings from the PixelTBMDelayCalibrationBPIX copy constructor.");
}

void PixelTBMDelayCalibrationBPIX::beginCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    fecTimer_.setName("FEC programming");
    phasefindingTimer_.setName("Phase finding");
    trigTimer_.setName("Triggering");
    fedTimer_.setName("FED soaping");
    allTimer_.setName("Total time");
    allTimer_.start();

    // Check that PixelCalibConfiguration settings make sense.

    if (!tempCalibObject->singleROC() && tempCalibObject->maxNumHitsPerROC() > 2 && tempCalibObject->parameterValue("OverflowWarning") != "no") {
        LOG4CPLUS_FATAL(logger_, "FIFO3 will overflow with more than two hits on each ROC.  To run this calibration, use 2 or less hits per ROC, or use SingleROC mode.  Now aborting...");
        assert(0);
    }

    if (!tempCalibObject->containsScan("TBMPLL")) {
        LOG4CPLUS_FATAL(logger_, "TBMPLLDelay not found in scan variable list!");
        assert(0);
    }

    delayBeforeFirstTrigger_ = tempCalibObject->parameterValue("DelayBeforeFirstTrigger") == "yes";
    delayEveryTrigger_ = tempCalibObject->parameterValue("DelayEveryTrigger") == "yes";
}

bool PixelTBMDelayCalibrationBPIX::execute() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    const bool firstOfPattern = event_ % tempCalibObject->nTriggersPerPattern() == 0;
    const unsigned state = event_ / (tempCalibObject->nTriggersPerPattern());
    reportProgress(0.05);

    // Configure all TBMs and ROCs according to the PixelCalibConfiguration settings, but only when it's time for a new configuration.
    if (firstOfPattern) {
        LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("New TBM delay state ") << state);
        fecTimer_.start();
        commandToAllFECCrates("CalibRunning"); // calls PixelCalibConfiguration::nextFECState()
        fecTimer_.stop();
        
        // reset TBM, sleep for safety
        sendTTCTBMReset();
        usleep(1000.);

        phasefindingTimer_.start();
        commandToAllFEDCrates("FindPhasesNow");
        phasefindingTimer_.stop();
    }

    if (delayBeforeFirstTrigger_ && firstOfPattern)
        usleep(1000);

    if (delayEveryTrigger_)
        usleep(100000);

    // Send trigger to all TBMs and ROCs.
    trigTimer_.start();
    sendTTCCalSync();
    trigTimer_.stop();

    // Read out data from each FED.
    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "RetrieveData";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = to_string(state);
    fedTimer_.start();
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);
    fedTimer_.stop();

    return event_ + 1 < tempCalibObject->nTriggersTotal();
}

void PixelTBMDelayCalibrationBPIX::endCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    assert(event_ == tempCalibObject->nTriggersTotal());

    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "Analyze";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = "0";
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);

    fecTimer_.printStats();
    phasefindingTimer_.printStats();
    trigTimer_.printStats();
    fedTimer_.printStats();
    
    allTimer_.stop();
    allTimer_.printStats();
}

std::vector<std::string> PixelTBMDelayCalibrationBPIX::calibrated() {
    return std::vector<std::string>(1, "tbm");
}
