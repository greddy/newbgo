
#include "PixelCalibrations/include/PixelPowerCycleCalibration.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelUtilities/PixelDCSUtilities/include/PixelDCSPVSSDpe.h"
#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"
#include "xdaq/Application.h"
#include <glob.h>

// #include <toolbox/convertstring.h>

using namespace pos;
using namespace std;

PixelPowerCycleCalibration::PixelPowerCycleCalibration(const PixelSupervisorConfiguration &tempConfiguration, SOAPCommander *mySOAPCmdr)
    : PixelCalibrationBase(tempConfiguration, *mySOAPCmdr), sv_logger_(app_->getApplicationLogger()) {
    const xdaq::ApplicationDescriptor *psxDescriptor = 0;
    pvssCommander_ = 0;
    try {
        psxDescriptor = app_->getApplicationContext()->getDefaultZone()->getApplicationGroup("dcs")->getApplicationDescriptor("psx", 0);
        pvssCommander_ = new PixelDCSPVSSCommander(app_, psxDescriptor);
    }
    catch (...) {
        LOG4CPLUS_INFO(sv_logger_, "No DCS in the application group.");
    }
    //  cout << "Greetings from the PixelPowerCycleCalibration copy constructor." <<endl;
}

void PixelPowerCycleCalibration::beginCalibration() {

    PixelConfigInterface::get(theLowVoltageMap_, "pixel/lowvoltagemap/", *theGlobalKey_);
    if (theLowVoltageMap_ == 0) {
        cout << "Could not find the LowVoltageMap in the configuration"
             << "\n";
        assert(0);
    }

    string action = "BeginCalibration";

    /*
  std::string logpathbase="/pixelscratch/pixelscratch/log0/SEU_log/";
  struct stat sb;
  if (!(stat(logpathbase.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)))
  {
      logpathbase="/tmp/";
  }
  //std::time_t t = std::time(nullptr);
  //char buffer[80];
  //strftime(buffer, sizeof(buffer), "%Y_%m_%d_%H_%M_%S", gmtime(&t));
  std::string logpath=logpathbase+"seu_*.log";
  
  //if we use a standard map the values should be sorted by the key!
  std::map<time_t,std::string> time_file_map;
  
  glob_t glob_result;
  glob(logpath.c_str(),GLOB_TILDE,NULL,&glob_result);
  std::vector<std::string> files;
  for(unsigned int i=0;i<glob_result.gl_pathc;++i){
      //files.push_back(string(glob_result.gl_pathv[i]));
      std::string file(glob_result.gl_pathv[i]);
      std::string time_details = file.substr(logpathbase.size()+5,file.size()-5);
      struct tm tm;
      strptime(time_details.c_str(), "%Y_%m_%d_%H_%M_%S", &tm);
      time_t t = mktime(&tm);
      time_file_map[t]=file;
  }
  globfree(&glob_result);
  
  // the data structure will be map[run][time][set<modules>] 
  std::map<std::string, std::map<std::time_t,std::set<std::string> > > modules_per_run;
  for(std::map<time_t,std::string>::iterator it=time_file_map.begin(); it!=time_file_map.end(); it++){
    string line;
    ifstream file(it->second.c_str());
    std::string run_number="None";
    if (file.is_open()){
      while ( getline (file,line) ){
        if(line.substr(0,4)=="# Run"){
          run_number=line.substr(line.find(" ")+1,line.size()-1);
          if(modules_per_run.find(run_number)==modules_per_run.end()){
            std::set<std::string> module_set;
            std::map<std::time_t,std::set<std::string> > time_module_map;
            time_module_map[it->first]=module_set;
            modules_per_run[run_number]=time_module_map;
          }else{
            std::set<std::string> module_set;
            modules_per_run[run_number][it->first]=module_set;
          }
        }else{
          if(line.find("channel:")!=std::string::npos){
            std::string channel=line.substr(line.find(" ")+1,line.size()-1);
            std::string module=channel.substr(0,channel.rfind("_"));
            modules_per_run[run_number][it->first].insert(module);
          }
        }
      }
    }
    file.close();
  }
  */

    //send a lot of triggers to get the stuck TBMs
    sentUserCommandToAMC13("StartContinuesL1A", "10000");
    sleep(5);
    sentUserCommandToAMC13("StopContinuesL1A", "");

    // now get the stuck TBMs from the FEDs
    std::map<int, std::bitset<48> > maskedFEDandChannel;
    Supervisors::iterator i_PixelFEDSupervisor;
    for (i_PixelFEDSupervisor = PixelFEDSupervisors_.begin(); i_PixelFEDSupervisor != PixelFEDSupervisors_.end(); ++i_PixelFEDSupervisor) {

        xoap::MessageReference channelmsg = SendWithSOAPReply(i_PixelFEDSupervisor->second, "SendBackProblemChannels");
        std::map<std::string, std::vector<std::pair<std::string, std::string> > > backcall = ReceiveVector(channelmsg);
        std::vector<std::string> FEDVector;
        std::vector<std::string> ChannelVector;

        for (std::map<std::string, std::vector<std::pair<std::string, std::string> > >::iterator itInfo = backcall.begin(); itInfo != backcall.end(); itInfo++) {
            for (std::vector<std::pair<std::string, std::string> >::iterator it = itInfo->second.begin(); it != itInfo->second.end(); it++) {
                if (it->first == "FED")
                    FEDVector.push_back(it->second);
                if (it->first == "ChannelInfo")
                    ChannelVector.push_back(it->second);
            }
        }

        for (size_t iatt = 0; iatt < FEDVector.size(); iatt++) {
            std::bitset<48> channels(ChannelVector[iatt]);
            maskedFEDandChannel[atoi(FEDVector[iatt].c_str())] = channels;
        }
    }

    maskedModules.clear();
    for (std::map<int, std::bitset<48> >::iterator it = maskedFEDandChannel.begin(); it != maskedFEDandChannel.end(); it++) {
        std::cout << "FED: " << it->first << "  -- " << it->second.to_string() << std::endl;
        for (size_t ichannel = 0; ichannel < it->second.size(); ichannel++) {
            if (it->second[ichannel]) {
                PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(it->first, ichannel + 1);
                maskedModules.insert(theChannel.modulename());
            }
        }
    }
}

std::set<std::string> PixelPowerCycleCalibration::moduleToDP(std::set<std::string> moduleList) {
    std::set<std::string> returnNodes;
    for (std::set<std::string>::iterator it_module = moduleList.begin(); it_module != moduleList.end(); it_module++) {
        pos::PixelModuleName name(*it_module);
        std::string dpName = theLowVoltageMap_->dpName(name);
        if (dpName != "") {
            std::string note_complete = "cms_trk_dcs_1:tkPg_" + dpName;
            std::replace(note_complete.begin(), note_complete.end(), '/', '\\');
            //dpes.push_back(PixelDCSPVSSDpe(note_complete,""));
            returnNodes.insert(note_complete);
            //global variable will always keep the last module of the powergroup
            note_module_map[note_complete] = (*it_module);
        }
    }
    return returnNodes;
}

void PixelPowerCycleCalibration::setDPs(std::set<std::string> dpList, std::string value, std::string desiredValue) {
    if (dpList.size() == 0) {
        return;
    }
    std::list<PixelDCSPVSSDpe> set_dpes;
    for (std::set<std::string>::iterator it = dpList.begin(); it != dpList.end(); it++) {
        set_dpes.push_back(PixelDCSPVSSDpe((*it) + value, desiredValue));
    }

    //let the dp take care of the type and precision:
    std::string dp_desiredValue = set_dpes.back().getValue();

    if (pvssCommander_ != 0) {
        try {
            pvssCommander_->setDpeValues(set_dpes);
        }
        catch (xoap::exception::Exception &e) {
            std::string const msg_error_hmy = "Failure to print the status: " + string(e.what());
            LOG4CPLUS_ERROR(sv_logger_, msg_error_hmy);
        }

        int ntries = 0;
        std::string return_string = "None";
        while (ntries < 100) {
            replaceAll(value, ".settings", ".readBackSettings");
            return_string = getDPs(dpList, value);

            if (return_string == dp_desiredValue) {
                LOG4CPLUS_INFO(sv_logger_, "The dps are all set to " + dp_desiredValue);
                break;
            }
            usleep(5000);
            ntries++;
        }
    }
}

std::string PixelPowerCycleCalibration::getDPs(std::set<std::string> dpList, std::string value) {
    if (dpList.size() == 0) {
        return "None";
    }
    std::list<PixelDCSPVSSDpe> read_dpes;
    for (std::set<std::string>::iterator it = dpList.begin(); it != dpList.end(); it++) {
        read_dpes.push_back(PixelDCSPVSSDpe((*it) + value, ""));
    }
    std::string return_string = "None";
    std::list<PixelDCSPVSSDpe> returndpes;
    try {
        returndpes = pvssCommander_->getDpeValues(read_dpes);
    }
    catch (xdaq::exception::Exception &e) {
        std::string const msg_error_hmy = "Failure to get the status from the PSX server. Exception: " + string(e.what());
        LOG4CPLUS_ERROR(sv_logger_, msg_error_hmy);
        return_string = "STATE_MIXED";
    }
    for (std::list<PixelDCSPVSSDpe>::iterator it_dpe = returndpes.begin(); it_dpe != returndpes.end(); it_dpe++) {
        std::string msg_status = "status " + it_dpe->getName() + " is " + it_dpe->getValue();
        LOG4CPLUS_DEBUG(sv_logger_, msg_status);
        if (return_string == "None") {
            return_string = it_dpe->getValue();
        } else if (return_string != it_dpe->getValue()) {
            return_string = "STATE_MIXED";
            return return_string;
        }
    }
    return return_string;
}

void PixelPowerCycleCalibration::doPowercycle(std::set<std::string> moduleList) {
    std::set<std::string> dpList = moduleToDP(moduleList);

    //std::set<std::string> dpListBpix;
    //for(std::set<std::string>::iterator it=dpList.begin(); it!=dpList.end();it++){
    //pos::PixelModuleName oneModule(note_module_map[*it]);
    //if('B'==oneModule.detsub()){
    //string channel=(*it);
    //std::replace( channel.begin(), channel.end(),'\\','/');
    //replaceAll(channel,"cms_trk_dcs_1:tkPg_","cms_trk_dcs_1:");
    ////now digital LV is channel000
    //channel+="/channel000";
    //dpListBpix.insert(channel);
    //}
    //}

    //std::string defaultVoltage=getDPs(dpListBpix,".readBackSettings.v0");
    //std::string defaultMaxCurrent=getDPs(dpListBpix,".readBackSettings.i0");

    //if(defaultVoltage=="STATE_MIXED"){
    //LOG4CPLUS_ERROR(sv_logger_,"Found a group that is not set to 9V will do so!");
    //defaultVoltage="9";
    //}
    //if(defaultMaxCurrent=="STATE_MIXED"){
    //LOG4CPLUS_ERROR(sv_logger_,"Found a group that is not set to 8A will do so!");
    //defaultMaxCurrent="8";
    //}

    //// Set the voltage for the hole bpix first.
    //// We will take care about the outliers later
    //std::string desiredVoltage = "11";
    //std::string desiredMaxCurrent = "9";

    for (std::set<std::string>::iterator it = dpList.begin(); it != dpList.end(); it++) {
        pos::PixelModuleName oneModule(note_module_map[*it]);
        switchDP(*it, "OFF");
    }
    //setDPs(dpListBpix,".settings.v0",desiredVoltage);
    //setDPs(dpListBpix,".settings.i0",desiredMaxCurrent);
    for (std::set<std::string>::iterator it = dpList.begin(); it != dpList.end(); it++) {
        //pos::PixelModuleName oneModule(note_module_map[*it]);
        //if('B'==oneModule.detsub()){
        //if( (oneModule.layer()==2|| oneModule.layer()==3) && oneModule.sec()==5 && oneModule.mp()=='m' && oneModule.mp()=='I' ){
        //float desiredVoltage_spechial=12;
        //float desiredMaxCurrent_spechial=9;
        ////hardcode this for now!
        //string channel=(*it);
        //std::replace( channel.begin(), channel.end(),'\\','/');
        //replaceAll(channel,"cms_trk_dcs_1:tkPg_","cms_trk_dcs_1:");
        ////now digital LV is channel000
        //channel+="/channel000";

        //setDP(channel,".settings.v0",desiredVoltage_spechial);
        //setDP(channel,".settings.i0",desiredMaxCurrent_spechial);
        //}
        //}
        switchDP(*it, "ON_LV");
    }
    //setDPs(dpListBpix,".settings.v0",defaultVoltage);
    //setDPs(dpListBpix,".settings.i0",defaultMaxCurrent);
}

void PixelPowerCycleCalibration::switchDP(std::string DP, std::string desiredState) {
    LOG4CPLUS_INFO(sv_logger_, "Switch " + DP + " to " + desiredState);
    if (pvssCommander_ != 0) {
        try {
            pvssCommander_->setDpeValue(DP + ".sendCommand", desiredState);
        }
        catch (xoap::exception::Exception &e) {
            std::string const msg_error_hmy = "Failure to print the status: " + string(e.what());
            LOG4CPLUS_ERROR(sv_logger_, msg_error_hmy);
        }
        int ntries = 0;
        while (desiredState != pvssCommander_->getDpeValue(DP + ".getState")) {
            usleep(5000);
            ntries++;
            if (ntries > 100) {
                break;
            }
        }
    }
    LOG4CPLUS_INFO(sv_logger_, "Set state ok!");
}

void PixelPowerCycleCalibration::setDP(std::string DP, std::string value, float desiredValue) {
    LOG4CPLUS_INFO(sv_logger_, "Set " + DP + value + " to " + itoa(desiredValue));
    if (pvssCommander_ != 0) {
        try {
            pvssCommander_->setDpeValue_float(DP + value, desiredValue);
        }
        catch (xoap::exception::Exception &e) {
            std::string const msg_error_hmy = "Failure to print the status: " + string(e.what());
            LOG4CPLUS_ERROR(sv_logger_, msg_error_hmy);
        }

        replaceAll(value, ".settings", ".readBackSettings");
        int ntries = 0;
        while (desiredValue != pvssCommander_->getDpeValue_float(DP + value)) {
            usleep(5000);
            ntries++;
            if (ntries > 100) {
                break;
            }
        }
    }
    LOG4CPLUS_INFO(sv_logger_, "Set value ok!");
}

float PixelPowerCycleCalibration::getDP(std::string DP, std::string value) {
    if (pvssCommander_ != 0) {
        return pvssCommander_->getDpeValue_float(DP + value);
    }
    return 0;
}

void PixelPowerCycleCalibration::sentUserCommandToAMC13(std::string command, std::string value) {
    if (useTTC_) {
        Attribute_Vector parametersToTTC(3);
        parametersToTTC[0].name_ = "xdaq:CommandPar";
        parametersToTTC[0].value_ = "Execute Sequence";
        parametersToTTC[1].name_ = "xdaq:sequence_name";
        parametersToTTC[1].value_ = command;
        parametersToTTC[2].name_ = "PixelAMC13Number";
        parametersToTTC[2].value_ = value;

        Supervisors::iterator i_PixelTTCSupervisor;
        for (i_PixelTTCSupervisor = PixelTTCSupervisors_.begin(); i_PixelTTCSupervisor != PixelTTCSupervisors_.end(); ++i_PixelTTCSupervisor) {
            std::string response = Send(i_PixelTTCSupervisor->second, "userCommand", parametersToTTC);
            if (response != "userTTCciControlResponse") {
                cout << "TTCciControl supervising crate #" << (i_PixelTTCSupervisor->first) << " could not be used!" << endl;
            }
        }
    }
}

void PixelPowerCycleCalibration::replaceAll(string &s, const string &search, const string &replace) {
    for (size_t pos = 0;; pos += replace.length()) {
        // Locate the substring to replace
        pos = s.find(search, pos);
        if (pos == string::npos)
            break;
        // Replace by erasing and inserting
        s.erase(pos, search.length());
        s.insert(pos, replace);
    }
}

void PixelPowerCycleCalibration::endCalibration() {
    //  cout<<"In PixelPowerCycleCalibration::endCalibration()"<<endl;
    string action = "EndCalibration";
}

bool PixelPowerCycleCalibration::execute() {
    Attribute_Vector parameters(1, Attribute());
    parameters.at(0).name_ = "resetParam";
    parameters.at(0).value_ = "all";

    string action = "PIAReset";
    sendToTKFEC(action, parameters);

    for (std::set<std::string>::iterator it = maskedModules.begin(); it != maskedModules.end(); it++) {
        LOG4CPLUS_INFO(sv_logger_, (*it));
    }
    //for real
    doPowercycle(maskedModules);

    Attribute_Vector parametersToTKFEC(1), parametersToFEC(1), parametersToFED(1);

    parametersToFEC[0].name_ = "GlobalKey";
    parametersToFEC[0].value_ = itoa(theGlobalKey_->key());
    parametersToFED[0].name_ = "GlobalKey";
    parametersToFED[0].value_ = itoa(theGlobalKey_->key());
    parametersToTKFEC[0].name_ = "GlobalKey";
    parametersToTKFEC[0].value_ = itoa(theGlobalKey_->key());

    LOG4CPLUS_INFO(sv_logger_, "Reconfigure TKFECs");
    string reply = SendSoapBlock(PixelTKFECSupervisors_, "Reconfigure", "ReconfigureDone", parametersToTKFEC);
    if (reply != "ReconfigureDone")
        XCEPT_RAISE(xdaq::exception::Exception, "Failed to reconfigure TKFEC");

    LOG4CPLUS_INFO(sv_logger_, "Reconfigure FECs");
    reply = SendSoapBlock(PixelFECSupervisors_, "Reconfigure", "ReconfigureDone", parametersToFEC);
    if (reply != "ReconfigureDone")
        XCEPT_RAISE(xdaq::exception::Exception, "Failed to reconfigure FEC");

    LOG4CPLUS_INFO(sv_logger_, "Reconfigure FEDs");
    reply = SendSoapBlock(PixelFEDSupervisors_, "Reconfigure", "ReconfigureDone", parametersToFED);
    if (reply != "ReconfigureDone")
        XCEPT_RAISE(xdaq::exception::Exception, "Failed to reconfigure FED");

    return false;
}

std::vector<std::string> PixelPowerCycleCalibration::calibrated() {

    std::vector<std::string> tmp;

    tmp.push_back("portcard");

    return tmp;
}

void PixelPowerCycleCalibration::SendSoapParallelConst(Supervisors::const_iterator &s, std::string &message, std::string &replynormal, boost::promise<bool> &p, boost::promise<std::string> &ps) {
    std::string replyOutside = message + "Done";
    std::string reply = "";
    try {
        reply = Send(s->second, message);
    }
    catch (xdaq::exception::Exception &e) {
        std::string const msg_error_hmy = "Failure while sending Start SOAP to Supervisors. Exception: " + string(e.what());
        LOG4CPLUS_ERROR(sv_logger_, msg_error_hmy);
        replyOutside = message + "Failed";
    }

    bool isOK = true;
    //check if reply ok
    if (reply != replynormal) {
        std::string msg_error = s->second->getClassName() + " instance #" + stringF(s->first) + " execute " + message;
        LOG4CPLUS_ERROR(sv_logger_, msg_error);
        isOK = false;
        replyOutside = msg_error;
    }
    ps.set_value(replyOutside);
    p.set_value(isOK);
}

//same as previous method but with parameters
//is there any way to combine the two?
void PixelPowerCycleCalibration::SendSoapParallelParam(Supervisors::iterator &s, std::string &message, std::string &replynormal, boost::promise<bool> &p, boost::promise<std::string> &ps, Attribute_Vector &parameters) {
    std::string replyOutside = replynormal;
    std::string reply = "";
    try {
        reply = Send(s->second, message, parameters);
        //cout<<"reply in loop "<<reply<<endl;
    }
    catch (xdaq::exception::Exception &e) {
        std::string const msg_error_hmy = "Failure while sending Start SOAP to Supervisors. Exception: " + string(e.what());
        LOG4CPLUS_ERROR(sv_logger_, msg_error_hmy);
        replyOutside = message + "Failed";
    }

    bool isOK = true;
    //check if reply ok
    if (reply != replynormal) {
        std::string msg_error = s->second->getClassName() + " instance #" + stringF(s->first) + " execute " + message;
        LOG4CPLUS_ERROR(sv_logger_, msg_error);
        isOK = false;
        replyOutside = msg_error;
    }
    ps.set_value(replyOutside);
    p.set_value(isOK);
}

std::string PixelPowerCycleCalibration::SendSoapBlock(Supervisors &SuperV, std::string message, std::string replynormal) {
    Attribute_Vector parameters(0);
    return SendSoapBlock(SuperV, message, replynormal, parameters);
}

std::string PixelPowerCycleCalibration::SendSoapBlock(Supervisors &SuperV, std::string message, std::string replynormal, Attribute_Vector &parameters) {
    std::vector<boost::promise<bool> *> allPromises;
    std::vector<boost::promise<std::string> *> allPromisesString;
    boost::thread_group allTreads;
    Supervisors::iterator s;
    for (s = SuperV.begin(); s != SuperV.end(); ++s) {
        boost::promise<bool> *p = new boost::promise<bool>();
        boost::promise<std::string> *ps = new boost::promise<std::string>();
        boost::thread *this_thread = new boost::thread(&PixelPowerCycleCalibration::SendSoapParallelParam, this, s, message, replynormal, boost::ref(*p), boost::ref(*ps), parameters);
        allPromises.push_back(p);
        allPromisesString.push_back(ps);
        allTreads.add_thread(this_thread);
    }
    allTreads.join_all();

    std::string reply = "";
    std::vector<boost::promise<bool> *>::iterator i;
    std::vector<boost::promise<std::string> *>::iterator is;
    s = SuperV.begin();
    for (is = allPromisesString.begin(); is != allPromisesString.end(); is++, s++) {
        //std::cout<<(*is)->get_future().get()<<std::endl;
        string thisreply = (*is)->get_future().get();
        //std::cout<<"reply was '"<<thisreply<<"'"<<std::endl;
        if (thisreply != replynormal) {
            reply += "Problem in Supervisor " + stringF(s->first) + "\n" + thisreply + "\n";
        }
    }
    if (reply == "") {
        reply = replynormal;
    }
    return reply;
}
