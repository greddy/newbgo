#include "PixelCalibrations/include/PixelDelay25VsReadbackerCalibration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortCardSettingNames.h"
#include <fstream>
#include <iomanip>
#include <stdlib.h>
#include "TH1.h"
#include "TH2.h"
#include "TFile.h"

using namespace pos;
using namespace std;

typedef std::vector<pos::PixelROCName> roclist_t;

namespace {
const bool DO_DEBUG = false;
//const bool scanTrig=false;
} // namespace

PixelDelay25VsReadbackerCalibration::PixelDelay25VsReadbackerCalibration(const PixelSupervisorConfiguration& tempConfiguration, SOAPCommander* mySOAPCmdr)
    : PixelCalibrationBase(tempConfiguration, *mySOAPCmdr), Readback_settings({8, 9, 10, 11, 12}), Readback_names({"vd", "va", "vana", "vbg", "iana"}) {
  std::cout << "Greetings from the PixelDelay25VsReadbackerCalibration constructor." << std::endl;
}

void PixelDelay25VsReadbackerCalibration::beginCalibration() {
  PixelCalibConfiguration* tempCalibObject = dynamic_cast<PixelCalibConfiguration*>(theCalibObject_);
  assert(tempCalibObject != 0);

  SDAMin = 64;
  if (tempCalibObject->parameterValue("ScanMin") != "")
    SDAMin = atoi(tempCalibObject->parameterValue("ScanMin").c_str());

  SDAMax = 127;
  if (tempCalibObject->parameterValue("ScanMax") != "")
    SDAMax = atoi(tempCalibObject->parameterValue("ScanMax").c_str());

  SDAStepSize = 1;
  if (tempCalibObject->parameterValue("ScanStepSize") != "")
    SDAStepSize = atoi(tempCalibObject->parameterValue("ScanStepSize").c_str());

  nTriggersPerPoint = 10;
  if (tempCalibObject->parameterValue("nTriggersPerPoint") != "")
    nTriggersPerPoint = atoi(tempCalibObject->parameterValue("nTriggersPerPoint").c_str());

  writeElog = tempCalibObject->parameterValue("writeElog") == "yes";
  scanTrig  = tempCalibObject->parameterValue("scanTrig") == "yes";
  scan_TBMPLL  = tempCalibObject->parameterValue("scan_TBMPLL") == "yes";

  std::cout << "[INFO] Min = " << SDAMin << std::endl;
  std::cout << "[INFO] Max = " << SDAMax << std::endl;
  std::cout << "[INFO] StepSize = " << SDAStepSize << std::endl;
  std::cout << "[INFO] nTriggersPerPoint = " << nTriggersPerPoint << std::endl;
  std::cout << "[INFO] writeElog = " << writeElog << std::endl;
  std::cout << "[INFO] scanTrig = " << scanTrig << std::endl;
  std::cout << "[INFO] scan_TBMPLL = " << scan_TBMPLL << std::endl;

  std::cout << "[INFO] OutputDir = " << outputDir() << std::endl;

  outtext.Form("%s/log.txt", outputDir().c_str());

  const roclist_t& rocs = tempCalibObject->rocList();
  for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
    const PixelHdwAddress* hdwAddress                                                         = theNameTranslation_->getHdwAddress(*roc);
    const unsigned fednumber                                                                  = hdwAddress->fednumber();
    feds_needed[std::make_pair(theFEDConfiguration_->crateFromFEDNumber(fednumber),
                               theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber))] = true;
  }

  for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
    roc_codes[*roc] = roc->rocname();
  }
}

bool PixelDelay25VsReadbackerCalibration::execute() {
  PixelCalibConfiguration* tempCalibObject = dynamic_cast<PixelCalibConfiguration*>(theCalibObject_);
  assert(tempCalibObject != 0);
  const roclist_t& rocs = tempCalibObject->rocList();

  TFile file(TString::Format("%s/Delay25_vs_Readbacker.root", outputDir().c_str()), "create");
  std::string delay25_type = "SDA";
  if (scanTrig)
    delay25_type = "TRIG";

  std::ofstream ofs(outtext);
  ofs << std::endl;
  ofs << "Min = " << SDAMin << std::endl;
  ofs << "Max = " << SDAMax << std::endl;
  ofs << "Stepsize = " << SDAStepSize << std::endl;
  ofs << "nTriggersPerPoint = " << nTriggersPerPoint << std::endl;
  ofs << "scanTrig = " << scanTrig << std::endl;
  ofs << "scan_TBMPLL = " << scan_TBMPLL << std::endl;
  ofs << std::endl;

  std::ofstream out(TString::Format("%s/Readbacker.dat", outputDir().c_str()).Data(), ios_base::out);
  assert(out.is_open());
  out << "# Readback order:";
  for (int Readback = 0; Readback < 5; ++Readback)
    out << " " << Readback_names[Readback];
  out << "\n# roc codes:\n";
  for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
    out << "# " << *roc << " " << roc_codes[*roc] << "\n";
  }

  const bool firstOfPattern = event_ % tempCalibObject->nTriggersPerPattern() == 0;
  reportProgress(0.05);
  if (DO_DEBUG)
    std::cout << " patterns " << firstOfPattern << " " << event_ << " " << tempCalibObject->nTriggersPerPattern() << endl;

  event_++;

  // Scan Delay25 (SDA or TRIG)
  std::vector<std::pair<int, std::vector<PixelROCName> > > sda_good_rocs;
  int N_SDASteps = 0;
  for (unsigned int SDA = SDAMin; SDA <= SDAMax; SDA += SDAStepSize) {
    N_SDASteps++;

    // Set AOH bias, send command to TKFECs
    Attribute_Vector parametersToTKFEC(4);
    parametersToTKFEC[0].name_  = "Delay25Setting";
    parametersToTKFEC[0].value_ = itoa(SDA);
    if (scanTrig) { // special TRIG scan
      //parametersToTKFEC[1].name_="DelayType";      parametersToTKFEC[1].value_="SCLK"; //clock
      parametersToTKFEC[1].name_  = "DelayType";
      parametersToTKFEC[1].value_ = "TRIG"; // trig
    } else {                                // Normal SDA scan
      parametersToTKFEC[1].name_  = "DelayType";
      parametersToTKFEC[1].value_ = "SDATA";
    }
    parametersToTKFEC[2].name_  = "Update";
    parametersToTKFEC[2].value_ = "0";
    parametersToTKFEC[3].name_  = "Write";
    parametersToTKFEC[3].value_ = "0";

    // Set the new SDA
    commandToAllTKFECCrates("SetDelayEnMass", parametersToTKFEC);
    std::cout<<delay25_type<<" = "<<SDA<<std::endl;

    std::map<PixelROCName, std::vector<unsigned> > rocReadbacks = getRocReadbacks(rocs, Readback_settings);
    std::vector<PixelROCName> good_rocs;
    for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
      out << delay25_type << " : " << SDA << "  " << roc_codes[*roc];
      bool good_roc = true;
      for (int Readback = 0; Readback < 5; ++Readback) {
        out << " " << rocReadbacks[*roc][Readback];
        if (rocReadbacks[*roc][Readback] > 255 || rocReadbacks[*roc][Readback] <= 1) {
          good_roc = false;
        }
      }
      out << "\n";
      if (good_roc) {
        good_rocs.push_back(*roc);
      }
    }
    sda_good_rocs.push_back(make_pair(SDA, good_rocs));

  } // end of loop over SDA values

  const std::set< std::string > portcards= thePortcardMap_->portcards();
  std::vector<std::pair<std::pair<unsigned int, unsigned int>, std::vector<PixelROCName> > > tbmpll_good_rocs;
  if (scan_TBMPLL){

    // Set the Delay25 (SDA and Trig) back to default values (Reconfigure TKFEC)
    std::cout<<"Set Delay25 to Default Values "<<std::endl;
    ofs<<"Set Delay25 to Default Values \n";

    for( std::set< std::string >::const_iterator it_portcard= portcards.begin(); it_portcard!= portcards.end();++it_portcard){
        pos::PixelPortCardConfig *tempPortCard = 0;
        PixelConfigInterface::get(tempPortCard, "pixel/portcard/" + *it_portcard, *theGlobalKey_);
        const string TKFECID = tempPortCard->getTKFECID();
        unsigned int TKFECcrate = theTKFECConfiguration_->crateFromTKFECID(TKFECID);

        std::vector<pair<std::string, unsigned int> > toProgram;
        std::string delay_type;
        unsigned int value = 0;

        delay_type = "SDATA";
        value = tempPortCard->getdeviceValuesForSetting(pos::PortCardSettingNames::k_Delay25_SDA);
        toProgram.push_back(make_pair(delay_type, value));
        std::cout<<*it_portcard<<" Set SDA : "<<value<<std::endl;
        ofs<<*it_portcard<<" Set SDA : "<<value<<"\n";
        if (scanTrig){
            delay_type = "TRIG";
            value = tempPortCard->getdeviceValuesForSetting(pos::PortCardSettingNames::k_Delay25_TRG);
            toProgram.push_back(make_pair(delay_type, value));
            std::cout<<*it_portcard<<" Set TRIG : "<<value<<std::endl;
            ofs<<*it_portcard<<" Set TRIG : "<<value<<"\n";
        }

        for (vector<pair<std::string, unsigned int> >::const_iterator i = toProgram.begin(); i != toProgram.end(); ++i) {
            Attribute_Vector parametersToTKFEC_SetDelay(5);
            parametersToTKFEC_SetDelay[0].name_ = "PortCardName";
            parametersToTKFEC_SetDelay[1].name_ = "Delay";
            parametersToTKFEC_SetDelay[2].name_ = "Value";
            parametersToTKFEC_SetDelay[3].name_ = "Update";
            parametersToTKFEC_SetDelay[4].name_ = "Write";
            parametersToTKFEC_SetDelay[0].value_ = *it_portcard;
            parametersToTKFEC_SetDelay[1].value_ = i->first;
            parametersToTKFEC_SetDelay[2].value_ = itoa(i->second);
            parametersToTKFEC_SetDelay[3].value_ = "0";
            parametersToTKFEC_SetDelay[4].value_ = "0";
            
            std::string reply = Send(PixelTKFECSupervisors_[TKFECcrate], "SetDelay", parametersToTKFEC_SetDelay);
            if (reply != "SetDelayDone"){
                std::string error_msg = "Failed to set delay for portcard : " + *it_portcard + " for delay type : " + i->first + " = " + itoa(i->second);
                XCEPT_RAISE(xdaq::exception::Exception, error_msg);
            }
        }
    }
    std::cout<<std::endl;
    ofs<<"\n";

    // Check whether the set Delay25 is good
    std::map<PixelROCName, std::vector<unsigned> > rocReadbacks_default = getRocReadbacks(rocs, Readback_settings);
    std::vector<PixelROCName> good_rocs_default;
    std::cout<<"Readbacker for default Delay25 setting \n";
    ofs << "Readbacker for default Delay25 setting \n";
    out << "Readbacker for default Delay25 setting \n";
    for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
      out << roc_codes[*roc];
      bool good_roc = true;
      for (int Readback = 0; Readback < 5; ++Readback) {
        out << " " << rocReadbacks_default[*roc][Readback];
        if (rocReadbacks_default[*roc][Readback] > 255 || rocReadbacks_default[*roc][Readback] <= 1) {
          good_roc = false;
        }
      }
      out << "\n";
      if (good_roc) {
        good_rocs_default.push_back(*roc);
      }
    }

    for( std::set< std::string >::const_iterator it_portcard= portcards.begin(); it_portcard!= portcards.end();++it_portcard){
        const std::set< PixelModuleName > modules_on_portcard= thePortcardMap_->modules(*it_portcard);
        std::set< std::string > modules_on_portcard_str;
        for(std::set< PixelModuleName >::const_iterator it =modules_on_portcard.begin(); it!=modules_on_portcard.end(); ++it){
            modules_on_portcard_str.insert(it->modulename());
        }
        int total_rocs = 0;
        for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
            if(modules_on_portcard_str.find(roc->modulename()) != modules_on_portcard_str.end())
                total_rocs++;
        }
        int n_good_rocs=0;
        for(vector<PixelROCName>::const_iterator iroc = good_rocs_default.begin(); iroc!=good_rocs_default.end(); ++iroc ){
            if(modules_on_portcard_str.find(iroc->modulename()) != modules_on_portcard_str.end())
                n_good_rocs++;
        }
        std::cout <<"portcard "<< *it_portcard << " - good ROCs: " << n_good_rocs;
        std::cout << " , total ROCs: " << total_rocs << " , good fraction: "<< (double)n_good_rocs/total_rocs << std::endl;
        ofs<<"portcard "<< *it_portcard << " - good ROCs: " << n_good_rocs;
        ofs<<" , total ROCs: " << total_rocs << " , good fraction: "<< (double)n_good_rocs/total_rocs << "\n";
    }
    std::cout<<std::endl;
    ofs<<"\n";

    std::cout<<"Starting TBMPLL Scan\n\n";
    // Scan TBMPLL
    for (unsigned int i_160=0; i_160<=7; i_160++){ // Scan 160 MHz Setting 0-7
        for (unsigned int i_400=0; i_400<=7; i_400++){ // Scan 400 MHz Setting 0-7
            unsigned int TBMPLL = getTBMPLLsetting(i_160, i_400);

            const set<PixelModuleName> &modulesToCalibrate = tempCalibObject->moduleList();
            for (set<PixelModuleName>::const_iterator modulesToCalibrate_itr = modulesToCalibrate.begin(); modulesToCalibrate_itr != modulesToCalibrate.end(); ++modulesToCalibrate_itr) {
                const PixelHdwAddress &module_firstHdwAddress = theNameTranslation_->firstHdwAddress(*modulesToCalibrate_itr);
                unsigned int crate = theFECConfiguration_->crateFromFECNumber(module_firstHdwAddress.fecnumber());
                unsigned int VMEBaseAddress = theFECConfiguration_->VMEBaseAddressFromFECNumber(module_firstHdwAddress.fecnumber());
                unsigned int mFEC = module_firstHdwAddress.mfec();
                unsigned int mFECChannel = module_firstHdwAddress.mfecchannel();
                unsigned int HubAddress = module_firstHdwAddress.hubaddress();

                int tbmchannel = 14; // = 0xE
                int portaddress = 4;
                int offset = 7; // For TBMPLL
                int direction = 0;

                Attribute_Vector parametersToFEC(9);
                parametersToFEC[0].name_ = "VMEBaseAddress";
                parametersToFEC[0].value_ = itoa(VMEBaseAddress);
                parametersToFEC[1].name_ = "mFEC";
                parametersToFEC[1].value_ = itoa(mFEC);
                parametersToFEC[2].name_ = "mFECChannel";
                parametersToFEC[2].value_ = itoa(mFECChannel);
                parametersToFEC[3].name_ = "TBMChannel";
                parametersToFEC[3].value_ = itoa(tbmchannel);
                parametersToFEC[4].name_ = "HubAddress";
                parametersToFEC[4].value_ = itoa(HubAddress);
                parametersToFEC[5].name_ = "PortAddress";
                parametersToFEC[5].value_ = itoa(portaddress);
                parametersToFEC[6].name_ = "Offset";
                parametersToFEC[6].value_ = itoa(offset);
                parametersToFEC[7].name_ = "DataByte";
                parametersToFEC[7].value_ = itoa(TBMPLL);
                parametersToFEC[8].name_ = "Direction";
                parametersToFEC[8].value_ = itoa(direction);

                // Set the new TBMPLL
                std::string reply = Send(PixelFECSupervisors_[crate], "TBMCommand", parametersToFEC);
                if (reply != "TBMCommandDone"){
                    std::string error_msg = "Failed to set TBMPLL : " + itoa(TBMPLL) + " for VMEBaseAddress : " + itoa(VMEBaseAddress) + " mFEC : " + itoa(mFEC) + " mFECChannel : " + itoa(mFECChannel) + " HubAddress : " + itoa(HubAddress);
                    XCEPT_RAISE(xdaq::exception::Exception, error_msg);
                }
            }
            std::cout<<"160 MHz Setting : "<<i_160<<" , 400 MHz Setting : "<<i_400<<" , TBMPLL : "<<TBMPLL<<std::endl;

            // Required to get better results
            usleep(1000);
            std::cout<<"Finding FED Phases\n";
            commandToAllFEDCrates("FindPhasesNow");
            sleep(2.0); // sleep of 1.0 also works but sometimes results a bit flaky

            std::map<PixelROCName, std::vector<unsigned> > rocReadbacks = getRocReadbacks(rocs, Readback_settings);
            std::vector<PixelROCName> good_rocs;
            for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
                out << "160 MHz Setting : "<<i_160<<" , 400 MHz Setting : "<<i_400<<" , TBMPLL : "<<TBMPLL << "  " << roc_codes[*roc];
                bool good_roc = true;
                for (int Readback = 0; Readback < 5; ++Readback) {
                    out << " " << rocReadbacks[*roc][Readback];
                    if (rocReadbacks[*roc][Readback] > 255 || rocReadbacks[*roc][Readback] <= 1) {
                        good_roc = false;
                    }
                }
                out << "\n";
                if (good_roc) {
                    good_rocs.push_back(*roc);
                }
            }
            tbmpll_good_rocs.push_back(make_pair(make_pair(i_160,i_400), good_rocs));

        } // end of loop over 400 MHz Settings
    } // end of loop over 160 MHz Settings
  }

  std::cout << std::dec;
  ofs<<"Scan Results : \n\n";
  for( std::set< std::string >::const_iterator it_portcard= portcards.begin(); it_portcard!= portcards.end();++it_portcard){
    std::cout << "\nportcard: "<< *it_portcard<<endl;
    ofs << "\nportcard: "<< *it_portcard << "\n";
    const std::set< PixelModuleName > modules_on_portcard= thePortcardMap_->modules(*it_portcard);
    std::set< std::string > modules_on_portcard_str;
    for(std::set< PixelModuleName >::const_iterator it =modules_on_portcard.begin(); it!=modules_on_portcard.end(); ++it){
      modules_on_portcard_str.insert(it->modulename());
    }
    
    int total_rocs = 0;
    for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
        if(modules_on_portcard_str.find(roc->modulename()) != modules_on_portcard_str.end())
            total_rocs++;
    }

    TDirectory *d = file.mkdir((*it_portcard).c_str());
    d->cd();
    std::string name_postfix = "sda";
    if (scanTrig)
        name_postfix = "trg";
    TH1D* h_sda = new TH1D((*it_portcard+"_ngoodrocs_"+name_postfix).c_str(), (*it_portcard+" : Good ROCs vs " + delay25_type + "; " + delay25_type + "; Fraction of Good ROCs").c_str(), N_SDASteps, SDAMin, SDAMax+1);

    // For Delay25 Scan
    for (std::vector<std::pair<int, std::vector<PixelROCName> > >::const_iterator good_roc_it = sda_good_rocs.begin(); good_roc_it != sda_good_rocs.end(); ++good_roc_it) {
      int n_good_rocs=0;
      for(vector<PixelROCName>::const_iterator iroc = good_roc_it->second.begin(); iroc!=good_roc_it->second.end(); ++iroc ){
        if(modules_on_portcard_str.find(iroc->modulename()) != modules_on_portcard_str.end()){
          n_good_rocs++;
          h_sda->Fill(good_roc_it->first);
        }
      }
      std::cout <<"portcard "<< *it_portcard << " , " << delay25_type << ": " << good_roc_it->first << " , good ROCs: " << n_good_rocs;
      std::cout << " , total ROCs: " << total_rocs << " , good fraction: "<< (double)n_good_rocs/total_rocs << std::endl;

      ofs<<"portcard "<< *it_portcard << " , " << delay25_type << ": " << good_roc_it->first << " , good ROCs: " << n_good_rocs;
      ofs<<" , total ROCs: " << total_rocs << " , good fraction: "<< (double)n_good_rocs/total_rocs << "\n";
    }
    std::cout<<std::endl;
    ofs<<"\n";
    h_sda->Scale(1.0/(double)total_rocs);
    h_sda->Write();

    if (scan_TBMPLL){
        for(std::set<std::string>::const_iterator module_name=modules_on_portcard_str.begin(); module_name!=modules_on_portcard_str.end(); ++module_name){
            std::string module_name_it = *module_name;
            std::cout<<module_name_it<<std::endl;
            ofs<<module_name_it<<"\n";

            int total_rocs_module = 0;
            for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
                if (module_name_it.find(roc->modulename())!=std::string::npos)
                    total_rocs_module++;
            }
            if (total_rocs_module==0)
                continue;

            TH2D* h_tbmpll = new TH2D((module_name_it+"_ngoodrocs_tbmpll").c_str(), (module_name_it+" : Good ROCs vs TBMPLL; 400 MHz; 160 MHz").c_str(), 8, 0, 8, 8, 0, 8);

            for (std::vector<std::pair<std::pair<unsigned int, unsigned int>, std::vector<PixelROCName> > >::const_iterator good_roc_it = tbmpll_good_rocs.begin(); good_roc_it != tbmpll_good_rocs.end(); ++good_roc_it) {
                int n_good_rocs=0;
                for(vector<PixelROCName>::const_iterator iroc = good_roc_it->second.begin(); iroc!=good_roc_it->second.end(); ++iroc ){
                    if (module_name_it.find(iroc->modulename())!=std::string::npos){
                        n_good_rocs++;
                        h_tbmpll->Fill(good_roc_it->first.second, good_roc_it->first.first);
                    }
                }
                std::cout <<"module "<< module_name_it << " , 160 : " << good_roc_it->first.first << " , 400 : " << good_roc_it->first.second << " , TBMPLL : "<<getTBMPLLsetting(good_roc_it->first.first, good_roc_it->first.second);
                std::cout << " , good ROCs: " << n_good_rocs << " , total ROCs: " << total_rocs_module << " , good fraction: "<< (double)n_good_rocs/total_rocs_module << std::endl;

                ofs <<"module "<< module_name_it << " , 160 : " << good_roc_it->first.first << " , 400 : " << good_roc_it->first.second << " , TBMPLL : "<<getTBMPLLsetting(good_roc_it->first.first, good_roc_it->first.second);
                ofs << " , good ROCs: " << n_good_rocs << " , total ROCs: " << total_rocs_module << " , good fraction: "<< (double)n_good_rocs/total_rocs_module << "\n";
            }
            std::cout<<std::endl;
            ofs<<"\n";
            h_tbmpll->Scale(1.0/(double)total_rocs_module);
            h_tbmpll->Write();
        }
    }

    file.cd();
  }

  out.close();
  ofs.close();
  file.Close();

  return false;
}

void PixelDelay25VsReadbackerCalibration::endCalibration() {
  PixelCalibConfiguration* tempCalibObject = dynamic_cast<PixelCalibConfiguration*>(theCalibObject_);
  assert(tempCalibObject != 0);
}

std::map<std::pair<unsigned, unsigned>, std::vector<uint32_t> > PixelDelay25VsReadbackerCalibration::getResultsByFEDs() {
  std::map<std::pair<unsigned, unsigned>, std::vector<uint32_t> > results;
  for (feds_needed_t::const_iterator it = feds_needed.begin(), ite = feds_needed.end(); it != ite; ++it) {
    Attribute_Vector pars(1);
    pars[0].name_                = "VMEBaseAddress";
    pars[0].value_               = itoa(it->first.second);
    xoap::MessageReference reply = SendWithSOAPReply(PixelFEDSupervisors_[it->first.first], "ReadOSDValues", pars);

    Attribute_Vector vals(1);
    vals[0].name_ = "Values";
    Receive(reply, vals);
    assert(vals[0].value_ != "problem");

    std::stringstream ss(vals[0].value_);
    uint32_t buf;
    std::vector<uint32_t> values;

    while (ss >> buf) {
      values.push_back(buf);
    }
    assert(values.size() == 24); // one 32-bit word result for each fiber
    results[it->first] = values;
  }
  return results;
}

void PixelDelay25VsReadbackerCalibration::armOSDFifo(int roc) {
  Attribute_Vector pars(4);
  pars[0].name_  = "VMEBaseAddress";
  pars[0].value_ = "*";
  pars[1].name_  = "Channel";
  pars[1].value_ = "*";
  pars[2].name_  = "RocHi";
  pars[2].value_ = itoa(roc);
  pars[3].name_  = "RocLo";
  pars[3].value_ = itoa(roc);
  commandToAllFEDCrates("ArmOSDFifo", pars);
}

std::map<pos::PixelROCName, std::vector<unsigned> > PixelDelay25VsReadbackerCalibration::getRocReadbacks(const std::vector<pos::PixelROCName>& rocs, const unsigned Readback_settings[]) {
  std::map<pos::PixelROCName, std::vector<unsigned> > results;

  for (int Readback = 0; Readback < 5; ++Readback) {
    setDACDDR(k_DACAddress_Readback, Readback_settings[Readback]);
    for (int iroc = 0; iroc < 8; ++iroc) {
      armOSDFifo(iroc);

      sendTTCCalSync(32);

      std::map<std::pair<unsigned, unsigned>, std::vector<uint32_t> > results_by_fed = getResultsByFEDs();

      for (roclist_t::const_iterator roc = rocs.begin(); roc != rocs.end(); ++roc) {
        if ((roc->detsub() == 'B' && roc->layer() == 1 && roc->roc() % 2 != iroc) ||
            (roc->detsub() == 'B' && roc->layer() == 2 && roc->roc() % 4 != iroc) ||
            (roc->detsub() == 'B' && roc->layer() == 3 && roc->roc() % 8 != iroc) ||
            (roc->detsub() == 'B' && roc->layer() == 4 && roc->roc() % 8 != iroc) ||
            (roc->detsub() == 'F' && roc->roc() % 8 != iroc) ||
            (roc->detsub() == 'P' && roc->roc() % 8 != iroc))
          continue;
        const PixelHdwAddress* hdwAddress = theNameTranslation_->getHdwAddress(*roc);
        const unsigned fednumber          = hdwAddress->fednumber();
        const unsigned fedcrate           = theFEDConfiguration_->crateFromFEDNumber(fednumber);
        const unsigned fedaddr            = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        const unsigned fedchannel         = hdwAddress->fedchannel();

        const std::vector<uint32_t>& words = results_by_fed[std::make_pair(fedcrate, fedaddr)];
        uint32_t word                      = words[(fedchannel - 1) / 2];
        const unsigned tbmch               = fedchannel % 2 == 0;
        if (tbmch == 1)
          word = (word >> 16);
        else
          word = word & 0xFFFF;
        bool ok =
            ((word >> 8) & 0xF) == Readback_settings[Readback] &&
            ((word >> 12) & 0xF) == unsigned(roc->roc());
        if (!ok) {
          std::cout << "problem with " << *roc << " : " << std::bitset<16>(word) << "\n";
          word = 1000 * (word >> 8) + (word & 0xFF);
        } else
          word = word & 0xFF;

        if (results.find(*roc) == results.end())
          results[*roc].assign(5, 0);
        results[*roc][Readback] = word;
      }
    }
  }
  return results;
}

void PixelDelay25VsReadbackerCalibration::setDACDDR(unsigned int dacAddress, unsigned int dacValue) {
  Attribute_Vector pars(2);
  pars[0].name_  = "DACAddress";
  pars[0].value_ = itoa(dacAddress);
  pars[1].name_  = "DACValue";
  pars[1].value_ = itoa(dacValue);
  commandToAllFECCrates("SetDACsEnMassDDR", pars);
}

unsigned int PixelDelay25VsReadbackerCalibration::getTBMPLLsetting(unsigned int setting_160, unsigned int setting_400){
    unsigned int tbmpll;
    tbmpll = setting_160;
    tbmpll = tbmpll<<3;
    tbmpll = tbmpll | setting_400;
    tbmpll = tbmpll<<2;
    return tbmpll;
}

/*
void PixelDelay25VsReadbackerCalibration::SendSoapParallelParam(Supervisors::iterator &s, std::string &message, std::string &replynormal, boost::promise<bool> &p, boost::promise<std::string> &ps, Attribute_Vector &parameters) {
    std::string replyOutside = replynormal;
    std::string reply = "";
    try {
        reply = Send(s->second, message, parameters);
    }
    catch (xdaq::exception::Exception &e) {
        //std::string const msg_error_hmy = "Failure while sending Start SOAP to Supervisors. Exception: " + string(e.what());
        //LOG4CPLUS_ERROR(sv_logger_, msg_error_hmy);
        std::cout<<"Failure while sending Start SOAP to Supervisors. Exception: "<<string(e.what())<<std::endl;
        replyOutside = message + "Failed";
    }

    bool isOK = true;
    //check if reply ok
    if (reply != replynormal) {
        std::string msg_error = s->second->getClassName() + " instance #" + stringF(s->first) + " execute " + message;
        //LOG4CPLUS_ERROR(sv_logger_, msg_error);
        std::cout<<s->second->getClassName()<<" instance #"<<stringF(s->first)<<" execute "<<message<<std::endl;
        isOK = false;
        replyOutside = msg_error;
    }
    ps.set_value(replyOutside);
    p.set_value(isOK);
}

std::string PixelDelay25VsReadbackerCalibration::SendSoapBlock(Supervisors &SuperV, std::string message, std::string replynormal, Attribute_Vector &parameters) {
    std::vector<boost::promise<bool> *> allPromises;
    std::vector<boost::promise<std::string> *> allPromisesString;
    boost::thread_group allTreads;
    Supervisors::iterator s;
    for (s = SuperV.begin(); s != SuperV.end(); ++s) {
        boost::promise<bool> *p = new boost::promise<bool>();
        boost::promise<std::string> *ps = new boost::promise<std::string>();
        boost::thread *this_thread = new boost::thread(&PixelDelay25VsReadbackerCalibration::SendSoapParallelParam, this, s, message, replynormal, boost::ref(*p), boost::ref(*ps), parameters);
        allPromises.push_back(p);
        allPromisesString.push_back(ps);
        allTreads.add_thread(this_thread);
    }
    allTreads.join_all();

    std::string reply = "";
    std::vector<boost::promise<bool> *>::iterator i;
    std::vector<boost::promise<std::string> *>::iterator is;
    s = SuperV.begin();
    for (is = allPromisesString.begin(); is != allPromisesString.end(); is++, s++) {
        string thisreply = (*is)->get_future().get();
        if (thisreply != replynormal) {
            reply += "Problem in Supervisor " + stringF(s->first) + "\n" + thisreply + "\n";
        }
    }
    if (reply == "") {
        reply = replynormal;
    }
    return reply;
}

*/
