#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelFEDTBMDelayCalibrationWithScores.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include <iomanip>



PixelFEDTBMDelayCalibrationWithScores::PixelFEDTBMDelayCalibrationWithScores(const PixelFEDSupervisorConfiguration &tempConfiguration, const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper), rootf(0) {
}

void PixelFEDTBMDelayCalibrationWithScores::initializeFED() {
    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    HeadersOnly = tempCalibObject->parameterValue("HeadersOnly") == "yes";
    IgnoreWordCount = tempCalibObject->parameterValue("IgnoreWordCount") == "yes";
    OrChannels = tempCalibObject->parameterValue("OrChannels") == "yes";
    DoFifo1 = tempCalibObject->parameterValue("DoFifo1") == "yes";
    CheckReadback = tempCalibObject->parameterValue("CheckReadback") == "yes";

    const std::string OverrideFifo1Fiber_str = tempCalibObject->parameterValue("OverrideFifo1Fiber");
    OverrideFifo1Fiber = -1;
    if (OverrideFifo1Fiber_str.size()) {
        OverrideFifo1Fiber = strtoul(OverrideFifo1Fiber_str.c_str(), 0, 10);
        if (OverrideFifo1Fiber < 0 || OverrideFifo1Fiber > 23) {
            std::cout << "OverrideFifo1Fiber " << OverrideFifo1Fiber << " not understood" << std::endl;
            assert(0);
        }
    }

    PhaseFindingNeeded = tempCalibObject->containsScan("TBMPLL");

    typedef std::set<std::pair<unsigned int, unsigned int> > colrow_t;
    const colrow_t colrows = tempCalibObject->pixelsWithHits(0);
    if (colrows.size() != 1) {
        std::cout << "must use exactly one pixel for score scan!\n";
        assert(0);
    }
    the_col = int(colrows.begin()->first);
    the_row = int(colrows.begin()->second);
    const int dc = the_col / 2;
    const int pxl = 160 - 2 * the_row + the_col % 2;
    std::cout << "Expected hit in col " << the_col << " row " << the_row << " -> dc " << dc << " pxl " << pxl << "\n";

    const std::vector<std::pair<unsigned, std::vector<unsigned> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);
    for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const unsigned fednumber = fedsAndChannels[ifed].first;
        const unsigned long vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];
        if (PhaseFindingNeeded)
            iFED->setAutoPhases(false);
        iFED->disableBE(true);
        iFED->setPixelForScore(dc, pxl);
        if (OverrideFifo1Fiber >= 0)
            iFED->setChannelOfInterest(OverrideFifo1Fiber);
    }
}

namespace {
unsigned dist_pll(unsigned a, unsigned b) {
    return unsigned(abs(int(a >> 5) - int(b >> 5)) + abs(int((a & 0x1c) >> 2) - int((b & 0x1c) >> 2)));
}

unsigned dist_roc(unsigned a, unsigned b) {
    return unsigned(abs(int(a & 7) - int(b & 7)) + abs(int((a >> 3) & 7) - int((b >> 3) & 7)));
}
}

xoap::MessageReference PixelFEDTBMDelayCalibrationWithScores::beginCalibration(xoap::MessageReference msg) {
    std::cout << "In PixelFEDTBMDelayCalibrationWithScores::beginCalibration()" << std::endl;

    ncalls = 0;

    readtimer.setName("FED readout");
    osdtimer.setName("OSD readout");
    othertimer.setName("FED rest-of");

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    tempCalibObject->writeASCII(outputDir());

    Dumps = tempCalibObject->parameterValue("Dumps") == "yes";

    for (unsigned dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        std::vector<unsigned int> dacvals = tempCalibObject->scanValues(dacname);
        if (dacvals.size() > 1) {
            dacsToScan.push_back(dacname);
            assert(dacname == "TBMPLL" || dacname == "TBMADelay" || dacname == "TBMBDelay");
        }
    }

    if (dacsToScan.empty() && tempCalibObject->parameterValue("NoScanOK") != "yes") {
        cout << "no dacs in scan?" << endl;
        assert(0);
    }

    rootf = new TFile(TString::Format("%s/TBMDelay.root", outputDir().c_str()), "create");
    assert(rootf->IsOpen());

    return makeSOAPMessageReference("BeginCalibrationDone");
}

xoap::MessageReference PixelFEDTBMDelayCalibrationWithScores::execute(xoap::MessageReference msg) {
    vector<string> parameters_names(2);
    parameters_names.push_back( "WhatToDo");
    parameters_names.push_back( "StateNum");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, parameters_names);

    const unsigned state = atoi(parameters[ "StateNum"].c_str());

    if (parameters[ "WhatToDo"] == "RetrieveData")
        RetrieveData(state);
    else if (parameters[ "WhatToDo"] == "Analyze")
        Analyze();
    else {
        cout << "ERROR: PixelFEDTBMDelayCalibrationWithScores::execute() does not understand the WhatToDo command, " << parameters[ "WhatToDo"] << ", sent to it.\n";
        assert(0);
    }

    return makeSOAPMessageReference("FEDCalibrationsDone");
}

xoap::MessageReference PixelFEDTBMDelayCalibrationWithScores::endCalibration(xoap::MessageReference msg) {
    std::cout << "In PixelFEDTBMDelayCalibrationWithScores::endCalibration()" << std::endl;
    readtimer.printStats();
    osdtimer.printStats();
    othertimer.printStats();

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    typedef std::vector<std::pair<unsigned, std::vector<unsigned> > > fedsAndChannels_t;
    const fedsAndChannels_t &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);

    for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const unsigned fednumber = fedsAndChannels[ifed].first;
        const unsigned long vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];
        if (PhaseFindingNeeded)
            iFED->setAutoPhases(true);
        iFED->disableBE(false);
    }

    for (fedsAndChannels_t::const_iterator fednumber_channels = fedsAndChannels.begin(); fednumber_channels != fedsAndChannels.end(); ++fednumber_channels) {
        const unsigned fednumber = fednumber_channels->first;
        for (std::vector<unsigned>::const_iterator fedchannel = fednumber_channels->second.begin(); fedchannel != fednumber_channels->second.end(); ++fedchannel) {
            if (*fedchannel % 2 != 0)
                continue;

            const int fiber = *fedchannel / 2;
            const pos::PixelChannel &pxch = theNameTranslation_->ChannelFromFEDChannel(fednumber, *fedchannel);
            const pos::PixelModuleName &mod = pxch.module();

            pos::PixelTBMSettings *tbm = 0;
            PixelConfigInterface::get(tbm, "pixel/tbm/" + mod.modulename(), *theGlobalKey_);
            assert(tbm != 0);

            std::map<std::string, unsigned> old_vals;
            old_vals["TBMPLL"] = tbm->getTBMPLLDelay();
            old_vals["TBMADelay"] = tbm->getTBMADelay();
            old_vals["TBMBDelay"] = tbm->getTBMBDelay();

            if (dacsToScan.size() == 1) {
                const bool TBMPLL = dacsToScan[0] == "TBMPLL";
                const bool TBMADelay = dacsToScan[0] == "TBMADelay";
                const bool TBMBDelay = dacsToScan[0] == "TBMBDelay";
                assert(TBMPLL + TBMADelay + TBMBDelay > 0);
                const unsigned old_val = old_vals[dacsToScan[0]];

                TH1F *h = dynamic_cast<TH1F *>(scans[Key(fednumber, -fiber, "ScoreOK")][0]);
                int best_v = -1;
                int best_dist = 1000000;
                for (int ibin = 1; ibin <= h->GetNbinsX(); ++ibin) {
                    const unsigned c = round(h->GetBinContent(ibin));
                    if (c != tempCalibObject->nTriggersPerPattern()) // could not require perfection I guess
                        continue;
                    const int v = h->GetXaxis()->GetBinLowEdge(ibin);
                    int dist = 0;
                    if (TBMPLL)
                        dist = dist_pll(old_val, v);
                    else if (TBMADelay || TBMBDelay)
                        dist = dist_roc(old_val, v);
                    else
                        assert(0);

                    if (dist < best_dist) {
                        //std::cout << "new best dist " << dist << " at " << v << std::endl;
                        best_v = v;
                        best_dist = dist;
                    }
                }

                if (best_dist != 1000000) {
                    assert(best_v >= 0 && best_v <= 255);
                    if (best_dist != 0)
                        std::cout << "NEW TBM settings for " << mod << " ADelay: " << int(tbm->getTBMADelay()) << " BDelay: " << int(tbm->getTBMBDelay()) << " PLL: " << int(tbm->getTBMPLLDelay()) << "\n"
                                  << "   " << dacsToScan[0] << " -> value " << best_v << std::endl;

                    if (TBMPLL)
                        tbm->setTBMPLLDelay((unsigned char)(best_v));
                    else if (TBMADelay)
                        tbm->setTBMADelay((unsigned char)(best_v));
                    else if (TBMBDelay)
                        tbm->setTBMBDelay((unsigned char)(best_v));
                    else
                        assert(0);
                } else
                    std::cout << "no good " << dacsToScan[0] << " value found for " << mod << " !"
                              << "\n";
            } else {
                std::cout << "new setting finding not implemented for n-d scan\n";
            }

            tbm->writeASCII(outputDir());
            delete tbm; // we own it?
        }
    }

    return makeSOAPMessageReference("EndCalibrationDone");
}

void PixelFEDTBMDelayCalibrationWithScores::RetrieveData(unsigned state) {
    othertimer.start();

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    typedef std::set<std::pair<unsigned int, unsigned int> > colrow_t;
    const colrow_t colrows = tempCalibObject->pixelsWithHits(state);
    if (colrows.size() != 1) { // checked in initialize fed but need to keep checking
        std::cout << "must use exactly one pixel for score scan!\n";
        assert(0);
    } else if (int(colrows.begin()->first) != the_col || int(colrows.begin()->second) != the_row) {
        std::cout << "must use same pixel always for score scan!\n"; // JMTBAD or we could re-set the pixel in the fed
        assert(0);
    }

    if (Dumps) {
        std::cout << "New FEDTBMDelay event " << event_ << " state " << state << " ";
        for (unsigned dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
            const std::string &dacname = tempCalibObject->scanName(dacnum);
            const unsigned dacvalue = tempCalibObject->scanValue(tempCalibObject->scanName(dacnum), state);
            std::cout << dacname << " -> " << dacvalue << " ";
        }
        std::cout << std::endl;
    }

    ++ncalls;
    const bool read_osd_now = ncalls % 32 == 0;
    const unsigned osd_roc = (ncalls - 1) / 32 % 8;
    //std::cout << " read osd now is " << read_osd_now << " roc " << osd_roc << "\n";

    const std::vector<std::pair<unsigned, std::vector<unsigned> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);

    for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const unsigned fednumber = fedsAndChannels[ifed].first;
        std::vector<bool> channels_used(49, 0);
        for (unsigned ich = 0; ich < fedsAndChannels[ifed].second.size(); ++ich)
            channels_used[fedsAndChannels[ifed].second[ich]] = true;

        if (Dumps)
            std::cout << "FED NUMBER " << fednumber << "\n";
        const unsigned long vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface *fed = dynamic_cast<PixelPh1FEDInterface *>(FEDInterface_[vmeBaseAddress]);
        const pos::PixelFEDCard &fedcard = FEDInterface_[vmeBaseAddress]->getPixelFEDCard();
        const int F1fiber = OverrideFifo1Fiber != -1 ? OverrideFifo1Fiber : fedcard.TransScopeCh + 1;
        std::vector<int> fibers_OK(25, 0);
        Key key(fednumber);

        readtimer.start();
        std::vector<uint32_t> scores = fed->getScores();
        readtimer.stop();

        std::vector<uint32_t> osds(24);
        if (CheckReadback && read_osd_now) {
            osdtimer.start();
            osds = fed->readOSDValues();
            osdtimer.stop();
        }

        for (int fiber = 1; fiber <= 24; ++fiber) {
            const int chA = fiber * 2 - 1;
            const int chB = fiber * 2;

            const bool chAused = channels_used[chA];
            const bool chBused = channels_used[chB];

            if (!chAused && !chBused)
                continue;

            const uint32_t scoreA = scores[chA - 1];
            const uint32_t scoreB = scores[chB - 1];

            const uint32_t AOSD = osds[fiber - 1] & 0xFFFF;
            const uint32_t BOSD = osds[fiber - 1] >> 16;

            if (Dumps) {
                // const uint32_t chs[2] = { chA, chB };
                const uint32_t chs[2] = { static_cast<uint32_t>(chA), static_cast<uint32_t>(chB) };
                const bool chused[2] = { chAused, chBused };
                const uint32_t scores[2] = { scoreA, scoreB };
                const uint32_t osds[2] = { AOSD, BOSD };
                std::cout << "fiber " << fiber << " scores:\n";
                for (size_t i = 0; i < 2; ++i)
                    if (chused[i])
                        std::cout << "ch " << std::setw(2) << chs[i] << ": wc DDDDDDDD rrrrrrrr RRRRRRRR TH\n"
                                  << "       " << std::setw(2) << (scores[i] >> 26) << " "
                                  << std::bitset<8>((scores[i] >> 18) & 0xFF) << " "
                                  << std::bitset<8>((scores[i] >> 10) & 0xFF) << " "
                                  << std::bitset<8>((scores[i] >> 2) & 0xFF) << " "
                                  << std::bitset<2>(scores[i] & 0x3)
                                  << "\n";

                std::cout << "fiber " << fiber << " osds:\n";
                for (size_t i = 0; i < 2; ++i)
                    if (chused[i])
                        std::cout << "ch " << std::setw(2) << chs[i] << ": iiii rrrr oooooooo\n"
                                  << "       "
                                  << std::bitset<4>((osds[i] >> 12) & 0xF) << " "
                                  << std::bitset<4>((osds[i] >> 8) & 0xF) << " "
                                  << std::bitset<8>(osds[i] & 0xFF)
                                  << "\n";
            }

            // format is ccccccDDDDDDDDrrrrrrrrRRRRRRRRTH
            // c counts the number of words (0-63)
            // the D bits are one per roc, 1 if has the right pixel hit
            // r should all be 0 if no additional roc headers
            // R is whether that roc header was there
            // T for tbm trailer
            // H for tbm header
            bool AOK, BOK;
            const uint32_t perfect_score = 0x4bfc03ff;
            const uint32_t perfect_score_ignore_wordcount = 0x4bfc03ff & 0x3ffffff;
            if (HeadersOnly) {
                // so perfect headers is (score & 0x3ffff) == 0x3ff
                AOK = (scoreA & 0x3ffff) == 0x3ff;
                BOK = (scoreB & 0x3ffff) == 0x3ff;
            } else if (IgnoreWordCount) {
                // want 18 words, 8 hits, 8 roc headers, tbm header, trailer
                AOK = (scoreA & 0x3ffffff) == perfect_score_ignore_wordcount;
                BOK = (scoreB & 0x3ffffff) == perfect_score_ignore_wordcount;
            } else {
                // want 18 words, 8 hits, 8 roc headers, tbm header, trailer
                AOK = scoreA == perfect_score;
                BOK = scoreB == perfect_score;
            }

            if (CheckReadback && read_osd_now) {
                // JMTBAD get readback register setting from dacs
                const bool AOSD_OK = (AOSD >> 12) == osd_roc;     // && ((AOSD >> 8) & 0xF) == 12;
                const bool BOSD_OK = (BOSD >> 12) == osd_roc + 8; // && ((BOSD >> 8) & 0xF) == 12;
                AOK = AOK && AOSD_OK;
                BOK = BOK && BOSD_OK;
                if (chAused)
                    FillEm(state, key(chA, "OSDOK"), AOSD_OK);
                if (chBused)
                    FillEm(state, key(chB, "OSDOK"), BOSD_OK);
            }

            if (!chAused)
                AOK = OrChannels ? false : true;
            if (!chBused)
                BOK = OrChannels ? false : true;

            const bool fiber_OK = OrChannels ? AOK || BOK : AOK && BOK;
            if (fiber_OK)
                fibers_OK[fiber] = 1;
            if (chAused)
                FillEm(state, key(chA, "ScoreOK"), AOK);
            if (chBused)
                FillEm(state, key(chB, "ScoreOK"), BOK);
            FillEm(state, key(-fiber, "ScoreOK"), fiber_OK);
        }

        // also look at the one fiber in fifo1 for a cross check

        if (DoFifo1) {
            const int F1chA = F1fiber * 2 - 1;
            const int F1chB = F1fiber * 2;

            if (Dumps) {
                fed->readTransparentFIFO();
                fed->readSpyFIFO();
            }

            PixelPh1FEDInterface::digfifo1 d = fed->readFIFO1(Dumps);
            if (Dumps)
                printf("n tbm h a: %i b: %i  tbm t a: %i b: %i  roc h a: %i b: %i\n", d.a.n_tbm_h, d.b.n_tbm_h, d.a.n_tbm_t, d.b.n_tbm_t, d.a.n_roc_h, d.b.n_roc_h);

            FillEm(state, key(-F1fiber, "nF1TBMHeaders"), d.a.n_tbm_h + d.b.n_tbm_h);
            FillEm(state, key(-F1fiber, "nF1TBMTrailers"), d.a.n_tbm_t + d.b.n_tbm_t);
            FillEm(state, key(-F1fiber, "nF1ROCHeaders"), d.a.n_roc_h + d.b.n_roc_h);
            FillEm(state, key(-F1fiber, "nF1Hits"), d.a.hits.size() + d.b.hits.size());

            int correct[2] = { 0 };
            int wrong[2] = { 0 };
            for (int aorb = 0; aorb < 2; ++aorb) {
                const PixelPh1FEDInterface::encfifo1 &z = d.aorb(aorb);
                for (size_t i = 0; i < z.hits.size(); ++i) {
                    const PixelPh1FEDInterface::encfifo1hit &h = z.hits[i];
                    if (colrows.find(std::make_pair(h.col, h.row)) != colrows.end())
                        ++correct[aorb];
                }
            }
            wrong[0] = d.a.hits.size() - correct[0];
            wrong[1] = d.b.hits.size() - correct[1];

            FillEm(state, key(-F1fiber, "nF1CorrectHits"), correct[0] + correct[1]);
            FillEm(state, key(-F1fiber, "nF1WrongHits"), wrong[0] + wrong[1]);

            FillEm(state, key(F1chA, "nF1TBMAHeaders"), d.a.n_tbm_h);
            FillEm(state, key(F1chA, "nF1TBMATrailers"), d.a.n_tbm_t);
            FillEm(state, key(F1chA, "nF1ROCAHeaders"), d.a.n_roc_h);
            FillEm(state, key(F1chA, "nF1AHits"), d.a.hits.size());
            FillEm(state, key(F1chA, "nF1ACorrectHits"), correct[0]);
            FillEm(state, key(F1chA, "nF1AWrongHits"), wrong[0]);

            FillEm(state, key(F1chB, "nF1TBMBHeaders"), d.b.n_tbm_h);
            FillEm(state, key(F1chB, "nF1TBMBTrailers"), d.b.n_tbm_t);
            FillEm(state, key(F1chB, "nF1ROCBHeaders"), d.b.n_roc_h);
            FillEm(state, key(F1chB, "nF1BHits"), d.b.hits.size());
            FillEm(state, key(F1chB, "nF1BCorrectHits"), correct[1]);
            FillEm(state, key(F1chB, "nF1BWrongHits"), wrong[1]);

            const int shouldbe = 8 * int(colrows.size());
            const bool isF1OK =
                d.a.n_tbm_h == 1 && d.a.n_tbm_t == 1 && d.a.n_roc_h == 8 &&
                d.b.n_tbm_h == 1 && d.b.n_tbm_t == 1 && d.b.n_roc_h == 8 &&
                correct[0] == shouldbe &&
                correct[1] == shouldbe &&
                wrong[0] == 0 &&
                wrong[1] == 0;

            FillEm(state, key(-F1fiber, "F1OK"), isF1OK);

            if (Dumps)
                printf("correct out of %i: a: %i b: %i   wrong: a: %i b: %i  F1OK? %i thatscore? %i agree? %i\n", shouldbe, correct[0], correct[1], wrong[0], wrong[1], isF1OK, fibers_OK[F1fiber], isF1OK == fibers_OK[F1fiber]);
        }

        if (Dumps) {
            printf("fibers OK by score:");
            for (int fiber = 1; fiber <= 24; ++fiber)
                if (fibers_OK[fiber])
                    printf(" %i", fiber);
            printf("\n");
        }
    }

    othertimer.stop();
}

void PixelFEDTBMDelayCalibrationWithScores::Analyze() {
    if (rootf) {
        rootf->Write();
        rootf->Close();
        delete rootf;
    }
}

void PixelFEDTBMDelayCalibrationWithScores::BookEm(const Key &key) {
    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    TString keyname = key.name();

    for (size_t i = 0; i < dacsToScan.size(); ++i) {
        const std::string &iname = dacsToScan[i];
        const TString itname(iname.c_str());
        std::vector<unsigned> ivals = tempCalibObject->scanValues(iname);
        std::sort(ivals.begin(), ivals.end());
        const size_t ni = ivals.size();
        std::vector<double> ibins(ni + 1);
        for (size_t k = 0; k < ni; ++k)
            ibins[k] = double(ivals[k]);
        ibins[ni] = ibins[ni - 1] + (ibins[ni - 1] - ibins[ni - 2]);

        if (dacsToScan.size() == 1) {
            TH1F *h = new TH1F(keyname + "_" + itname, keyname + ";" + itname + ";", ni, &ibins[0]);
            h->SetStats(0);
            scans[key].push_back(h);
        } else
            for (size_t j = i + 1; j < dacsToScan.size(); ++j) {
                const std::string jname = dacsToScan[j];
                const TString jtname(jname.c_str());
                std::vector<unsigned> jvals = tempCalibObject->scanValues(jname);
                std::sort(jvals.begin(), jvals.end());
                const size_t nj = jvals.size();
                std::vector<double> jbins(nj + 1);
                for (size_t k = 0; k < nj; ++k)
                    jbins[k] = double(jvals[k]);
                jbins[nj] = jbins[nj - 1] + (jbins[nj - 1] - jbins[nj - 2]);

                TH2F *h2 = new TH2F(keyname + "_" + jtname + "V" + itname, keyname + ";" + itname + ";" + jtname, ni, &ibins[0], nj, &jbins[0]);
                h2->SetStats(0);
                scans[key].push_back(h2);
            }
    }
}

void PixelFEDTBMDelayCalibrationWithScores::FillEm(unsigned state, const Key &key, float c) {
    if (scans.find(key) == scans.end())
        BookEm(key);

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    int k2 = 0;
    for (size_t i = 0; i < dacsToScan.size(); ++i) {
        const std::string &iname = dacsToScan[i];
        const double ival(tempCalibObject->scanValue(iname, state));

        if (dacsToScan.size() == 1)
            scans[key][i]->Fill(ival, c);
        else
            for (size_t j = i + 1; j < dacsToScan.size(); ++j, ++k2) {
                const std::string jname = dacsToScan[j];
                const double jval(tempCalibObject->scanValue(jname, state));
                dynamic_cast<TH2F *>(scans[key][k2])->Fill(ival, jval, c);
            }
    }
}
