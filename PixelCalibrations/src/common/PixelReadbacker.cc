#include "TTree.h"
#include "PixelCalibrations/include/PixelReadbacker.h"
#include "PixelUtilities/PixelTestStandUtilities/include/PixelTimer.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACSettings.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelUtilities/PixeluTCAUtilities/include/PixelAMC13Interface.h"

namespace {
  const bool dump=false;
}
using namespace pos;
typedef std::vector<pos::PixelROCName> roclist_t;

PixelReadbacker::PixelReadbacker(const PixelSupervisorConfiguration &tempConfiguration, SOAPCommander *mySOAPCmdr)
    : PixelCalibrationBase(tempConfiguration, *mySOAPCmdr),
      SkipProgramReadback(false), npoints_(25) {
}

void PixelReadbacker::beginCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    SkipProgramReadback = tempCalibObject->parameterValue("SkipProgramReadback") == "yes";
    ScanVana = tempCalibObject->parameterValue("ScanVana") == "yes";
    ReadDCU = tempCalibObject->parameterValue("ReadDCU") == "yes"; // all-inclusive DCU snapshot with all portcards

    if (ScanVana) {
      const std::string &NPoints = tempCalibObject->parameterValue("NPoints");
      if (NPoints != "") {
	int NPointsval = atoi(NPoints.c_str());
	if (NPointsval > 0)
	  npoints_ = NPointsval;
      }

        const unsigned step = 255 / npoints_;
        for (unsigned vana = 0; vana < 255; vana += step) {
            vanas.push_back(vana);
        }
        nvanas = vanas.size();

        for (std::vector<PixelModuleName>::const_iterator module = theDetectorConfiguration_->getModuleList().begin(), end = theDetectorConfiguration_->getModuleList().end(); module != end; ++module) {
            PixelDACSettings *dacs = 0;
            PixelConfigInterface::get(dacs, "pixel/dac/" + module->modulename(), *theGlobalKey_);
            assert(dacs != 0);
            dacsettings_[*module] = dacs;
        }
    } else {nvanas = 1;}
    
    
    const roclist_t &rocs = tempCalibObject->rocList();
    for (roclist_t::const_iterator roc = rocs.begin(), roce = rocs.end(); roc != roce; ++roc) {
      const PixelHdwAddress *hdwAddress = theNameTranslation_->getHdwAddress(*roc);
      const unsigned fednumber = hdwAddress->fednumber();
      feds_needed[std::make_pair(theFEDConfiguration_->crateFromFEDNumber(fednumber),
				 theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber))] = true;
    }
    
}

bool PixelReadbacker::execute() {
    timer.start();
    if(dump) cout<<"execute"<<endl;
    listMakeTimer.start();
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    const roclist_t &rocs = tempCalibObject->rocList();

    std::map<PixelROCName, int> roc_codes;
    int roc_code = 0;

    for (roclist_t::const_iterator roc = rocs.begin(), roce = rocs.end(); roc != roce; ++roc, ++roc_code) {
        roc_codes[*roc] = roc_code;
    }

    const unsigned Readback_settings[5] = { 8, 9, 10, 11, 12 };
    const char *Readback_names[5] = { "vd", "va", "vana", "vbg", "iana" };

    std::ofstream out(TString::Format("%s/Readbacker.dat", outputDir().c_str()));
    assert(out.is_open());
    out << "# Readback order:";
    for (int Readback = 0; Readback < 5; ++Readback)
        out << " " << Readback_names[Readback];
    out << "\n# roc codes:\n";
    for (roclist_t::const_iterator roc = rocs.begin(), roce = rocs.end(); roc != roce; ++roc) {
        out << "# " << *roc << " " << roc_codes[*roc] << "\n";
    }
    listMakeTimer.stop();

    if (ScanVana) {
        for (int rocNum = 0; rocNum < 16; ++rocNum) {
            for (size_t ivana = 0; ivana < nvanas; ++ivana) {
                int vana = mixVana(ivana);
                prog_vana_timer.start();
                for (roclist_t::const_iterator roc = rocs.begin(), roce = rocs.end(); roc != roce; ++roc) {
                    if (roc->roc() != rocNum)
                        continue;
                    out << "Set " << roc_codes[*roc] << " Vana " << vana << "\n";
                    setDAC(*roc, k_DACAddress_Vana, vana);
                }
                prog_vana_timer.stop();

                std::map<PixelROCName, std::vector<unsigned> > rocReadbacks = getRocReadbacks(rocs, Readback_settings);
                print_timer.start();
                for (roclist_t::const_iterator roc = rocs.begin(), roce = rocs.end(); roc != roce; ++roc) {
                    out << roc_codes[*roc];
                    for (int Readback = 0; Readback < 5; ++Readback)
                        out << " " << rocReadbacks[*roc][Readback];
                    out << "\n";
                }
                print_timer.stop();

                reset_settings_timer.start();
                for (roclist_t::const_iterator roc = rocs.begin(), roce = rocs.end(); roc != roce; ++roc) {
                    if (roc->roc() != rocNum)
                        continue;
                    setDAC(*roc, k_DACAddress_Vana, dacsettings_[PixelModuleName(*roc)]->getDACSettings(*roc)->getVana());
                }
                reset_settings_timer.stop();
            }
        }
    } else {

        std::map<PixelROCName, std::vector<unsigned> > rocReadbacks = getRocReadbacks(rocs, Readback_settings);

        print_timer.start();
        for (roclist_t::const_iterator roc = rocs.begin(), roce = rocs.end(); roc != roce; ++roc) {
            out << roc_codes[*roc];
            for (int Readback = 0; Readback < 5; ++Readback)
                out << " " << rocReadbacks[*roc][Readback];
            out << "\n";
	    if(dump) {
	      cout << roc_codes[*roc];
	      for (int Readback = 0; Readback < 5; ++Readback)
                cout << " " << rocReadbacks[*roc][Readback];
	      cout << "\n";
	    }
        }
        print_timer.stop();
    }
    out.close();

    dcu_timer.start();
    if (ReadDCU) {
        std::ofstream dcuout(TString::Format("%s/DCUReadbacker.dat", outputDir().c_str()));
        assert(dcuout.is_open());

        std::set<unsigned int> crates = tempCalibObject->getTKFECCrates(thePortcardMap_, *getmapNamePortCard(), theTKFECConfiguration_);

        for (std::set<unsigned int>::iterator crate = crates.begin(); crate != crates.end(); ++crate) {
            std::vector<PortCard::AddressDCU> dcuBottom = getDCU(*crate, "bottom");
            std::vector<PortCard::AddressDCU> dcuTop = getDCU(*crate, "top");

            dcuout << "# DCU Channel order: ";
            for (int channel = 0; channel < 8; ++channel) {
                dcuout << dcuBottom[0].dcu_.name_chan(channel) << " ";
            }
            dcuout << "\n";

            for (size_t i = 0; i < dcuBottom.size(); ++i) {
                std::string portcardName = dcuBottom[i].address_.portcardName_;
                dcuout << portcardName << std::setw(8) << " bottom ";
                for (unsigned j = 0; j < 8; ++j) {
                    dcuout << dcuBottom[i].dcu_.GetChan(j) << " ";
                }
                dcuout << "\n";
                for (size_t k = 0; k < dcuTop.size(); ++k) {
                    if (portcardName == dcuTop[k].address_.portcardName_) {
                        dcuout << portcardName << std::setw(8) << " top ";
                        for (unsigned j = 0; j < 8; ++j) {
                            dcuout << dcuTop[k].dcu_.GetChan(j) << " ";
                        }
                        dcuout << "\n";
                    }
                }
            }
        }
    }
    dcu_timer.stop();

    timer.stop();

    return false;
}

void PixelReadbacker::endCalibration() {
    std::cout << "Total Time: " << timer.tottime() << std::endl;
    std::cout << "Time to make ROC code list: " << listMakeTimer.tottime() << std::endl;
    std::cout << "Time to program vana settings: " << prog_vana_timer.tottime() << std::endl;
    std::cout << "Time to program readback settings: " << program_readback_timer.tottime() << std::endl;
    std::cout << "Time to arm OSD FIFO: " << arm_timer.tottime() << std::endl;
    std::cout << "Time to issue triggers: " << trig_timer.tottime() << std::endl;
    std::cout << "Time to get results from FEDs: " << read_timer.tottime() << std::endl;
    std::cout << "Time to parse results from FEDs: " << result_timer.tottime() << std::endl;
    std::cout << "Time to print results into file: " << print_timer.tottime() << std::endl;
    std::cout << "Time to reset vana settings: " << reset_settings_timer.tottime() << std::endl;
    std::cout << "Time to read DCU: " << dcu_timer.tottime() << std::endl;
}

std::map<std::pair<unsigned, unsigned>, std::vector<uint32_t> > PixelReadbacker::getResultsByFEDs() {
    std::map<std::pair<unsigned, unsigned>, std::vector<uint32_t> > results;
    for (feds_needed_t::const_iterator it = feds_needed.begin(), ite = feds_needed.end(); it != ite; ++it) {
        Attribute_Vector pars(1);
        pars[0].name_ = "VMEBaseAddress";
        pars[0].value_ = itoa(it->first.second);
        xoap::MessageReference reply = SendWithSOAPReply(PixelFEDSupervisors_[it->first.first], "ReadOSDValues", pars);

        Attribute_Vector vals(1);
        vals[0].name_ = "Values";
        Receive(reply, vals);
        assert(vals[0].value_ != "problem");

        std::stringstream ss(vals[0].value_);
        uint32_t buf;
        std::vector<uint32_t> values;

        while (ss >> buf) {
            values.push_back(buf);
        }
        assert(values.size() == 24); // one 32-bit word result for each fiber

	if(dump) {
	  cout<<"size "<<values.size()<<hex<<endl;
	  for(unsigned int i=0;i<values.size();++i) cout<<values[i]<<" ";
	  cout<<dec<<endl;
	}
        results[it->first] = values;
    }
    return results;
}

void PixelReadbacker::armOSDFifo(int roc) {
    Attribute_Vector pars(4);
    pars[0].name_ = "VMEBaseAddress";
    pars[0].value_ = "*";
    pars[1].name_ = "Channel";
    pars[1].value_ = "*";
    pars[2].name_ = "RocHi";
    pars[2].value_ = itoa(roc);
    pars[3].name_ = "RocLo";
    pars[3].value_ = itoa(roc);
    commandToAllFEDCrates("ArmOSDFifo", pars);
}

unsigned PixelReadbacker::mixVana(size_t index) {
    unsigned vana;
    if (nvanas % 2 == 1 && index == nvanas - 1) {
        vana = vanas[index];
    } else {
        const size_t newIndex = (index % 2 != 0) * nvanas / 2 + index / 2;
        vana = vanas[newIndex];
    }
    return vana;
}

std::map<pos::PixelROCName, std::vector<unsigned> > PixelReadbacker::getRocReadbacks(const std::vector<pos::PixelROCName> rocs, const unsigned Readback_settings[]) {
    std::map<pos::PixelROCName, std::vector<unsigned> > results;

    for (int Readback = 0; Readback < 5; ++Readback) {
        program_readback_timer.start();
        if(SkipProgramReadback) {
	  cout<<"skip readback programming "<<Readback<<endl;
	  if(Readback>0) sleep(1); // sleep 1sec
	} else setDACDDR(k_DACAddress_Readback, Readback_settings[Readback]);
        program_readback_timer.stop();
        for (int iroc = 0; iroc < 8; ++iroc) {
            arm_timer.start();
            armOSDFifo(iroc);
            arm_timer.stop();

            trig_timer.start();
            sendTTCCalSync(32);
            trig_timer.stop();

            read_timer.start();
            std::map<std::pair<unsigned, unsigned>, std::vector<uint32_t> > results_by_fed = getResultsByFEDs();
            read_timer.stop();

            result_timer.start();
            for (roclist_t::const_iterator roc = rocs.begin(), roce = rocs.end(); roc != roce; ++roc) {
                if ((roc->detsub() == 'B' && roc->layer() == 1 && roc->roc() % 2 != iroc) ||
                    (roc->detsub() == 'B' && roc->layer() == 2 && roc->roc() % 4 != iroc) ||
                    (roc->detsub() == 'B' && roc->layer() == 3 && roc->roc() % 8 != iroc) ||
                    (roc->detsub() == 'B' && roc->layer() == 4 && roc->roc() % 8 != iroc) ||
                    (roc->detsub() == 'F' && roc->roc() % 8 != iroc) ||
                    (roc->detsub() == 'P' && roc->roc() % 8 != iroc))
                    continue;
                const PixelHdwAddress *hdwAddress = theNameTranslation_->getHdwAddress(*roc);
                const unsigned fednumber = hdwAddress->fednumber();
                const unsigned fedcrate = theFEDConfiguration_->crateFromFEDNumber(fednumber);
                const unsigned fedaddr = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
                const unsigned fedchannel = hdwAddress->fedchannel();

                const std::vector<uint32_t> &words = results_by_fed[std::make_pair(fedcrate, fedaddr)];
                uint32_t word = words[(fedchannel - 1) / 2];
                const unsigned tbmch = fedchannel % 2 == 0;
		if(dump) {		  
		  cout<<" fed "<<fednumber<<" channel "<<fedchannel<<" roc "<<roc->roc()
			     <<" tbm "<<tbmch<<" word "<<hex<<word<<dec<<endl;
		}
			     
                if (tbmch == 1)
                    word = (word >> 16);
                else
                    word = word & 0xFFFF;

		int rocNum = (word>>12)&0xf;
		int setting = (word>>8)&0xf;
		int value = (word)&0xff;

		if(dump) {
		  cout<<" word "<<hex<<word<<dec<<" roc "<<rocNum<<" setting "<<setting<<" value "<<value<<endl;
		}

                bool ok = false;
		if(SkipProgramReadback) {
		  ok = (rocNum  == int(roc->roc()));
		} else { // normal operation 
		  ok = (setting == int(Readback_settings[Readback])) &&
                    (rocNum  == int(roc->roc()));
		  //((word >> 8) & 0xF) == Readback_settings[Readback] &&
		  //((word >> 12) & 0xF) == unsigned(roc->roc());
		}
                if (!ok) {
		  cout << "problem with " << *roc << " : " 
		       << std::bitset<16>(word) << " roc "<<rocNum <<" setting "<<setting<<endl;
		  word = 1000 * (word >> 8) + value;
                } else {word = value;}

                if (results.find(*roc) == results.end())
                    results[*roc].assign(5, 0);
                results[*roc][Readback] = word;
            }
            result_timer.stop();
        }
    }
    return results;
}

void PixelReadbacker::setDACDDR(unsigned int dacAddress, unsigned int dacValue) {
    Attribute_Vector pars(2);
    pars[0].name_ = "DACAddress";
    pars[0].value_ = itoa(dacAddress);
    pars[1].name_ = "DACValue";
    pars[1].value_ = itoa(dacValue);
    commandToAllFECCrates("SetDACsEnMassDDR", pars);
}

std::vector<PortCard::AddressDCU> PixelReadbacker::getDCU(unsigned int crate, std::string bottop) {
    Attribute_Vector pars(2);
    pars[0].name_ = "WhichDCU";
    pars[0].value_ = bottop;
    pars[1].name_ = "FromWorkloop";
    pars[1].value_ = "no";
    xoap::MessageReference reply = SendWithSOAPReply(PixelTKFECSupervisors_[crate], "readDCU", pars);

    return PortCard::SOAP_ReadAll::Decode(reply);
}
