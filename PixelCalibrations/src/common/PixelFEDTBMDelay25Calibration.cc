#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
//#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortCardSettingNames.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortcardMap.h"
#include "PixelCalibrations/include/PixelFEDTBMDelay25Calibration.h"
#include "PixelCalibrations/include/PixelPOHBiasCalibration.h"
#include "PixelCalibrations/include/PixelCalibrationBase.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelFEDDataTypes.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ColRowAddrDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigScopeDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigTransDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
//#include "PixelUtilities/PixelFEDDataTools/include/DigFIFO1Decoder.h"
#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"

#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTree.h"
#include <iomanip>
#include <algorithm>

#include "pixel/utils/Utils.h"

///////////////////////////////////////////////////////////////////////////////////////////////
PixelFEDTBMDelay25Calibration::PixelFEDTBMDelay25Calibration(const PixelFEDSupervisorConfiguration &tempConfiguration, const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper) {
          std::cout << "In PixelFEDTBMDelay25Calibration ctor()" << std::endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDTBMDelay25Calibration::initializeFED() {
    //setFEDModeAndControlRegister(0x8, 0x30010);

    cout << " Disable back-end in order to avoid spyFIFO1 filling " << endl;
    for (FEDInterfaceMap::iterator iFED = FEDInterface_.begin(); iFED != FEDInterface_.end();
         ++iFED) {
        iFED->second->disableBE(true);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDTBMDelay25Calibration::beginCalibration(xoap::MessageReference msg) {
    std::cout << "In PixelFEDTBMDelay25Calibration::beginCalibration()" << std::endl;

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    //  tempCalibObject->writeASCII(outputDir());
    DumpFIFOs = tempCalibObject->parameterValue("DumpFIFOs") == "yes";
    CheckFIFOs = tempCalibObject->parameterValue("CheckFIFOs") == "yes";
    CheckRocs = tempCalibObject->parameterValue("CheckRocs") == "yes";
    CheckHits = tempCalibObject->parameterValue("CheckHits") == "yes";
    OrChannels = tempCalibObject->parameterValue("OrChannels") == "yes";
    std::cout << "DumpFIFOs = " << DumpFIFOs << " CheckFIFOs " << CheckFIFOs 
	      <<" CheckRocs "<<CheckRocs<<" CheckHits "<<CheckHits<<" "<<OrChannels
	      << std::endl;

    event_ = 0;

    return makeSOAPMessageReference("BeginCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDTBMDelay25Calibration::execute(xoap::MessageReference msg) {

    vector<string> parameters_names(4);
    parameters_names.push_back( "WhatToDo");
    parameters_names.push_back( "SDA");
    parameters_names.push_back( "fednumber");
    parameters_names.push_back( "channel");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, parameters_names);

    const unsigned SDA = atoi(parameters[ "SDA"].c_str());
    const unsigned fednumber = atoi(parameters[ "fednumber"].c_str());
    const unsigned channel = atoi(parameters[ "channel"].c_str());

    xoap::MessageReference reply = makeSOAPMessageReference("FEDCalibrationsDone");

    if (parameters[ "WhatToDo"] == "RetrieveData") {
        reply = RetrieveData(SDA, fednumber, channel);
    } else {
        cout << "ERROR: PixelFEDTBMDelay25Calibration::execute() does not understand the WhatToDo command, " << parameters[ "WhatToDo"] << ", sent to it.\n";
        assert(0);
    }

    return reply;
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDTBMDelay25Calibration::endCalibration(xoap::MessageReference msg) {

    std::cout << "In PixelFEDTBMDelay25Calibration::endCalibration() " << event_ << std::endl;
    return makeSOAPMessageReference("EndCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDTBMDelay25Calibration::RetrieveData(unsigned SDA, unsigned int fednumber, unsigned int channel) {

    static unsigned int event0 = 0;
    long unsigned int rocSize = 
      (theNameTranslation_->getROCsFromFEDChannel(fednumber, channel)).size();

    if (DumpFIFOs)
        std::cout << "Enter retrieve data @ event = " << event0
                  << " " << channel << " " << rocSize << std::endl;

    //  PixelCalibConfiguration* tempCalibObject = dynamic_cast <PixelCalibConfiguration*> (theCalibObject_);
    //  assert(tempCalibObject!=0);

    const unsigned long vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
    PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];

    bool found_TBMA = false;
    unsigned int _channel = channel;
    if (channel % 2 == 0) _channel = channel - 1;

    int fib = (_channel - 1) / 2;
    if (DumpFIFOs)
        cout << "Reading fiber " << fib << " " << channel << " " << _channel << endl;

    iFED->setFIFO1(fib);
    PixelPh1FEDInterface::digfifo1 d = iFED->readFIFO1(DumpFIFOs);

    if (DumpFIFOs)
        printf("ch a: %i ch b: %i n tbm h a: %i b: %i  tbm t a: %i b: %i  roc h a: %i b: %i\n", d.a.ch, d.b.ch, d.a.n_tbm_h, d.b.n_tbm_h, d.a.n_tbm_t, d.b.n_tbm_t, d.a.n_roc_h, d.b.n_roc_h);

    unsigned int chA = d.a.ch;
    unsigned int chB = d.b.ch;

    if(CheckFIFOs) cout<<"check channels "<<_channel<<" "<<channel<<" "<<chA<<" "<<chB<<endl;

    if ((_channel == chA) && ((_channel + 1) == chB)) {
      if(CheckHits) { // check also hits 
        bool foundTBMs = d.a.n_tbm_h && d.a.n_tbm_t && d.b.n_tbm_h && d.b.n_tbm_t;
	bool foundROCs = ((d.a.n_roc_h==int(rocSize))&&(d.b.n_roc_h==int(rocSize)));

	vector<PixelPh1FEDInterface::encfifo1roc> ra = d.a.rocs;
	vector<PixelPh1FEDInterface::encfifo1roc> rb = d.b.rocs;
	vector<PixelPh1FEDInterface::encfifo1hit> ha = d.a.hits;
	vector<PixelPh1FEDInterface::encfifo1hit> hb = d.b.hits;
 	bool foundHits = ((ra.size()==rocSize)&&(rb.size()==rocSize)&&
			  (ha.size()==rocSize)&&(hb.size()==rocSize));
	if(CheckFIFOs) cout<<"check for hits in channel "<<_channel<<" "
			   <<foundTBMs<<" "<<foundROCs<<" "<<foundHits<<endl;
	found_TBMA= foundTBMs&&foundROCs&&foundHits; // combine 
      } else { // check only tbm headers
        found_TBMA = d.a.n_tbm_h && d.a.n_tbm_t && d.b.n_tbm_h && d.b.n_tbm_t;
      }
    }

    if (DumpFIFOs) {
        // Headers
        cout << " Header A " << d.a.n_tbm_h << " event " << d.a.event << " ch " << d.a.ch << " id " << d.a.id_tbm_h
             << " rocs " << d.a.n_roc_h << hex << " bits8-20 " << d.a.tbm_h << dec << endl;
        cout << " Header B " << d.b.n_tbm_h << " event " << d.b.event << " ch " << d.b.ch << " id " << d.b.id_tbm_h
             << " rocs " << d.b.n_roc_h << hex << " bits8-20 " << d.b.tbm_h << dec << endl;
        // Trailer
        cout << " Trailer A " << d.a.n_tbm_t << " ch " << d.a.ch_tbm_t << " t1 " << d.a.tbm_t1 << " t2 " << d.a.tbm_t2
             << " id " << d.a.id_tbm_t << endl;
        cout << " Trailer B " << d.b.n_tbm_t << " ch " << d.b.ch_tbm_t << " t1 " << d.b.tbm_t1 << " t2 " << d.b.tbm_t2
             << " id " << d.b.id_tbm_t << endl;
    }

    if (CheckFIFOs) {
      bool error = false;

      if (event0 != d.a.event) {
	if ((event_ == 0) || (event0 == 0) || (d.a.event == (event0 + 1))) { // new event number
	  event0 = d.a.event;
	  cout << " new event " << event0 << " SDA "<<hex<<SDA<<dec<<endl;
	} else if (event0 == 255) { // wrap around
	  event0 = 0;
	} else {
	  cout << " something wrong with event number " << d.a.event << " " << event0 << endl;
	  error = true;
	}
      }
      
      if ((d.a.event != event0) || (d.b.event != event0)) {
	cout << " ERROR: Event number wrong " << event0 << " fed " << fednumber << " chan " << channel << endl;
	error=true;
	//int dummy=0;
	//cin>>dummy;
      }
      
      if(!found_TBMA) 
	cout<<"TBMs not found "<<d.a.n_tbm_h<<" "<<d.a.n_tbm_t<<" "<<d.b.n_tbm_h<<" "<<d.b.n_tbm_t
	    <<" "<<d.a.n_roc_h<<" "<<d.b.n_roc_h<<" "<<rocSize
	    << " fed " << fednumber << " chan " << channel
	    <<endl;
      else {
	cout<<"TBMs found "
	    <<d.a.n_roc_h<<" "<<d.b.n_roc_h<<" "<<rocSize
	    << " fed " << fednumber << " chan " << channel;
	if ((d.a.n_roc_h != int(rocSize)) || (d.b.n_roc_h != int(rocSize))) {
	  cout <<" but rocs not found ";
	  error=true;
	}
	cout<<endl;
      }

      if(CheckHits) {
	vector<PixelPh1FEDInterface::encfifo1roc> ra = d.a.rocs;
	vector<PixelPh1FEDInterface::encfifo1roc> rb = d.b.rocs;
	vector<PixelPh1FEDInterface::encfifo1hit> ha = d.a.hits;
	vector<PixelPh1FEDInterface::encfifo1hit> hb = d.b.hits;
	if((ra.size()!=rocSize)||(rb.size()!=rocSize)||
	   (ha.size()!=rocSize)||(hb.size()!=rocSize)) {
	  cout<<" hits not found "
	      <<ra.size()<<" "<<rb.size()<<" "<<ha.size()<<" "<<hb.size()<<endl;
	}
      }

      if ((d.a.n_roc_h != int(rocSize)) || (d.b.n_roc_h != int(rocSize))) {
	cout << " ERROR: Number of ROCs  wrong " 
	     <<d.a.n_roc_h<<" "<<d.b.n_roc_h<<" instead of "
	     << rocSize << " fed " << fednumber << " chan " << channel << endl;
	error=true;
	//int dummy=0;
	//cin>>dummy;
      }
      
      if (((d.a.tbm_t1 & 0xfd) != 0) || ((d.b.tbm_t1 & 0xfd) != 0)) { // trailer not 0
	cout << " ERROR: trailer has nozero bits "
	     << " fed " << fednumber << " chan " << channel << endl;
	error=true;
	//int dummy=0;
	//cin>>dummy;
      }
      if (((d.a.tbm_t2 & 0x1fff) != 0) || ((d.b.tbm_t2 & 0x1fff) != 0)) { // trailer2 not 0
	cout << " ERROR: trailer2 has nozero bits "
	     << " fed " << fednumber << " chan " << channel << endl;
	error=true;
	//int dummy=0;
	//cin>>dummy;
      }
      if(error) {
	cout << " Header A " << d.a.n_tbm_h << " event " << d.a.event << " ch " << d.a.ch << " id " << d.a.id_tbm_h
	     << " rocs " << d.a.n_roc_h << hex << " bits8-20 " << d.a.tbm_h << dec << endl;
	cout << " Header B " << d.b.n_tbm_h << " event " << d.b.event << " ch " << d.b.ch << " id " << d.b.id_tbm_h
	     << " rocs " << d.b.n_roc_h << hex << " bits8-20 " << d.b.tbm_h << dec << endl;
	cout << " Trailer A " << d.a.n_tbm_t << " ch " << d.a.ch_tbm_t << " t1 " << d.a.tbm_t1 << " t2 " << d.a.tbm_t2
	     << " id " << d.a.id_tbm_t << endl;
	cout << " Trailer B " << d.b.n_tbm_t << " ch " << d.b.ch_tbm_t << " t2 " << d.b.tbm_t1 << " t2 " << d.b.tbm_t2
	     << " id " << d.b.id_tbm_t << endl;
      }

    }

    //  PixelFEDInterface* iFED = FEDInterface_[vmeBaseAddress];
    //
    //  uint32_t bufferFifo1[1024];
    //  int statusFifo1 = iFED->drainFifo1(channel, bufferFifo1, 1024);
    //
    //  bool found_TBMA = false;
    //
    //  if (statusFifo1 > 0) {
    //
    //    DigFIFO1Decoder theFIFO1Decoder(bufferFifo1,statusFifo1);
    //
    //    if( (int)theFIFO1Decoder.globalChannel() == (int)channel ){
    //      found_TBMA = theFIFO1Decoder.foundTBM();
    //
    //      if( DumpFIFOs ){
    //	std::cout << "-----------------------------------" << std::endl;
    //	std::cout << "Contents of FIFO 1 for channel " << channel << " (status = " << statusFifo1 << ")" << std::endl;
    //	std::cout << "-----------------------------------" << std::endl;
    //	theFIFO1Decoder.printToStream(std::cout);
    //      }
    //
    //    }
    //  }

    //  sendResets();

    event_++;

    Attributes returnValues;
        returnValues[ "foundTBMA"] = pixel::utils::to_string((int)found_TBMA);

    return makeSOAPMessageReference("FEDCalibrationsDone", returnValues);
}
