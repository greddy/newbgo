#include "PixelCalibrations/include/PixelFEDThresholdCalDelayCalibration.h"
#include "PixelUtilities/PixelFEDDataTools/include/Moments.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"
#include "PixelCalibrations/include/PixelHistoReadWriteFile.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO1Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO2Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelFEDDataTypes.h"

#include "pixel/utils/Utils.h"

#include "iomanip"
#include "TCanvas.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TLine.h"
#include "TH1F.h"
#include "TTree.h"
#include "TDirectory.h"
#include "TStyle.h"

using namespace std;

PixelFEDThresholdCalDelayCalibration::PixelFEDThresholdCalDelayCalibration(const PixelFEDSupervisorConfiguration &tempConfiguration,
                                                                           const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper) {
}

xoap::MessageReference PixelFEDThresholdCalDelayCalibration::execute(xoap::MessageReference msg) {
    const uint state = event_ / tempCalibObject_->nTriggersPerPattern(),
               ithreshold = tempCalibObject_->scanCounter(name1_, state),
               icaldelay = tempCalibObject_->scanCounter(name2_, state);

    uint64_t *buffer64 = new uint64_t[2000000];

    try {
        for (map<uint, set<uint> >::iterator fedAndChannels = fedsAndChannels_.begin(); fedAndChannels != fedsAndChannels_.end(); ++fedAndChannels) {
            const uint fednumber = fedAndChannels->first;

            if (theFEDConfiguration_->crateFromFEDNumber(fednumber) != crate_) {
                continue;
            }

            PixelPh1FEDInterface *iFED = FEDInterface_[theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber)];
            const int status = iFED->spySlink64(buffer64);

            if (status > 0) {
                FIFO3Decoder decode(buffer64, status, nTriggersPer_);

                for (uint ihit = 0; ihit < decode.nhits(); ihit++) {
                    const uint rocid = decode.rocid(ihit);
                    //assert(rocid>0);
                    if (rocid == 0) continue;
                    unsigned int channel = decode.channel(ihit);

                    pos::PixelROCName roc;

                    if (theNameTranslation_->ROCNameFromFEDChannelROCExists(fednumber, channel, rocid - 1))
                        roc = theNameTranslation_->ROCNameFromFEDChannelROC(fednumber,
                                                                            channel,
                                                                            rocid - 1);
                    else
                        cout << "ROC with fednumber=" << fednumber << " channel=" << channel << " rocid=" << rocid << " does not exist in translation!" << endl;

                    map<pos::PixelROCName, PixelEfficiency2DVcThrCalDel>::iterator it = eff_.find(roc);

                    if (it != eff_.end())
                        it->second.add(icaldelay, ithreshold);
                    else {
                        cout << "Could not find ROC " << roc.rocname() << " with fednumber=" << fednumber
                             << " channel=" << channel << " rocid=" << rocid << " in our map" << endl;
                    }
                }
            } else
                cout << "Error reading spySlink64 status=" << status << endl;
        }
    }
    catch (exception& e) {
        diagService_->reportError("*** Unknown exception occurred", DIAGWARN);
    }

    ++event_;

    delete[] buffer64;

    return makeSOAPMessageReference("ThresholdCalDelayDone");
}

void PixelFEDThresholdCalDelayCalibration::initializeFED() {
    setFEDModeAndControlRegister(0x8, 0x30010);
}

xoap::MessageReference PixelFEDThresholdCalDelayCalibration::beginCalibration(xoap::MessageReference msg) {
    tempCalibObject_ = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject_ != 0);

    const string &nTriggersPer_s = tempCalibObject_->parameterValue("TriggersPer");
    nTriggersPer_ = nTriggersPer_s != "" ? atoi(nTriggersPer_s.c_str()) : 1;

    fedsAndChannels_ = tempCalibObject_->getFEDsAndChannels(theNameTranslation_);

    name1_ = tempCalibObject_->scanName(0);
    name2_ = tempCalibObject_->scanName(1);

    assert(tempCalibObject_->numberOfScanVariables() > 1);

    const vector<pos::PixelROCName> ROCnames = tempCalibObject_->rocList();
    assert(ROCnames.size() > 0);
    cout << "Will make dirs:" << ROCnames.size() << endl;

    const uint nThr = tempCalibObject_->nScanPoints(name1_),
               nCal = tempCalibObject_->nScanPoints(name2_);

    const double VcThrStep = tempCalibObject_->scanValueStep(name1_),
           VcThrMinStep = tempCalibObject_->scanValueMin(name1_) - 0.5 * VcThrStep, // VcThrMin - 0.5 * VcThrStep
           VcThrMaxStep = tempCalibObject_->scanValueMax(name1_) + 0.5 * VcThrStep; // VcThrMax + 0.5 * VcThrStep

    const double CalDelStep = tempCalibObject_->scanValueStep(name2_),
                 CalDelMinStep = tempCalibObject_->scanValueMin(name2_) - 0.5 * CalDelStep, //CalDelMin - 0.5 * CalDelStep
                 CalDelMaxStep = tempCalibObject_->scanValueMax(name2_) + 0.5 * CalDelStep; //CalDelMax + 0.5 * CalDelStep

    for (vector<pos::PixelROCName>::const_iterator ROCname = ROCnames.begin(); ROCname < ROCnames.end(); ++ROCname) {
        pos::PixelModuleName module(ROCname->rocname());
        if (crate_ == theFEDConfiguration_->crateFromFEDNumber(theNameTranslation_->firstHdwAddress(module).fednumber())) {
            PixelEfficiency2DVcThrCalDel tmp(ROCname->rocname(), name2_, nCal,
                                             CalDelMinStep,
                                             CalDelMaxStep,
                                             name1_, nThr,
                                             VcThrMinStep,
                                             VcThrMaxStep);

            eff_[*ROCname] = tmp;
        }
    }

    return makeSOAPMessageReference("BeginCalibrationDone");
}

xoap::MessageReference PixelFEDThresholdCalDelayCalibration::endCalibration(xoap::MessageReference msg) {
    //First we need to get the DAC settings for the ROCs
    vector<pos::PixelModuleName> modules = theDetectorConfiguration_->getModuleList();

    map<pos::PixelModuleName, pos::PixelDACSettings *> theDACs;
    for (vector<pos::PixelModuleName>::iterator module_name = modules.begin(); module_name != modules.end(); ++module_name) {
        if (crate_ == theFEDConfiguration_->crateFromFEDNumber(theNameTranslation_->firstHdwAddress(*module_name).fednumber())) {
            pos::PixelDACSettings *tempDACs ;
            PixelConfigInterface::get(tempDACs, "pixel/dac/" + module_name->modulename(), *theGlobalKey_);
            assert(tempDACs != 0);
            theDACs[*module_name] = tempDACs;
        }
    }

    vector<string> rocsname;
    for (map<pos::PixelROCName, PixelEfficiency2DVcThrCalDel>::iterator it = eff_.begin(); it != eff_.end(); ++it)
        rocsname.push_back(it->first.rocname());

    //open the output root file
    outputFile_ = new TFile((outputDir() + "/VcThrCalDel_" + pixel::utils::to_string(crate_) + ".root").c_str(), "recreate", "VcThrCalDel.root");

    branch theBranch;
    branch_sum theBranch_sum;
    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("PassState", "PassState");
    TTree *tree_sum = new TTree("SummaryInfo", "SummaryInfo");

    tree->Branch("PassState", &theBranch, "pass/F:rocName/C", 4096000);
    tree_sum->Branch("SummaryInfo", &theBranch_sum, "new_CalDel/F:delta_CalDel/F:new_VcThr/F:delta_VcThr/F:rocName/C", 4096000);
    outputFile_->cd();

    PixelRootDirectoryMaker rootDirs(rocsname, outputFile_);
    const double numerator = tempCalibObject_->nTriggersPerPattern() * nTriggersPer_ * tempCalibObject_->nPixelPatterns();
    for (map<pos::PixelROCName, PixelEfficiency2DVcThrCalDel>::iterator it = eff_.begin(); it != eff_.end(); ++it) {
        string rocName = it->first.rocname();

        pos::PixelModuleName moduleName(rocName);
        if (crate_ != theFEDConfiguration_->crateFromFEDNumber(theNameTranslation_->firstHdwAddress(moduleName).fednumber()))
            continue;


        theBranch.pass = 0;
        strcpy(theBranch.rocName, rocName.c_str());
        strcpy(theBranch_sum.rocName, rocName.c_str());

        rootDirs.cdDirectory(rocName);

        it->second.findSettings(numerator);

        map<pos::PixelModuleName, pos::PixelDACSettings *>::iterator dacs = theDACs.find(moduleName);

        assert(dacs != theDACs.end());

        pos::PixelROCDACSettings *rocDACs = dacs->second->getDACSettings(it->first);

        assert(rocDACs != 0);

        uint oldVcThr = rocDACs->getVcThr(),
             oldCalDel = rocDACs->getCalDel();

        cout << "Old settings: VcThr=" << oldVcThr << " CalDel=" << oldCalDel << endl;

        uint newVcThr = it->second.getThreshold(),
             newCalDel = it->second.getCalDelay();

        cout << "New settings: VcThr=" << newVcThr << " CalDel=" << newCalDel << endl;

        if (it->second.validSettings()) {
            theBranch.pass = 1;
            theBranch_sum.new_CalDel = newCalDel;
            theBranch_sum.delta_CalDel = (float)oldCalDel - newCalDel;
            theBranch_sum.new_VcThr = newVcThr;
            theBranch_sum.delta_VcThr = (float)oldVcThr - newVcThr;

            tree->Fill();
            tree_sum->Fill();

            rocDACs->setVcThr(newVcThr);
            rocDACs->setCalDel(newCalDel);
        } else {
            cout << "Did not have valid settings for:" << it->first.rocname() << endl;

            theBranch_sum.new_CalDel = oldCalDel;
            theBranch_sum.delta_CalDel = 0.0;
            theBranch_sum.new_VcThr = oldVcThr;
            theBranch_sum.delta_VcThr = 0.0;
            tree->Fill();
            tree_sum->Fill();
        }

        it->second.setOldThreshold(oldVcThr);
        it->second.setOldCalDelay(oldCalDel);

        TCanvas *canvas = new TCanvas((rocName + "_Canvas").c_str(), rocName.c_str(), 800, 600);

        TH2F histo2D = it->second.FillEfficiency(numerator);

        histo2D.GetXaxis()->SetTitle(name2_.c_str());
        histo2D.GetYaxis()->SetTitle(name1_.c_str());
        histo2D.SetMinimum(0.0);
        histo2D.SetMaximum(1.0);
        histo2D.Draw("colz");

        double deltaCalDelay = 0.05 * (it->second.getmax1() - it->second.getmin1()),
               deltaVcThr = 0.05 * (it->second.getmax2() - it->second.getmin2());

        TLine *l1 = new TLine(oldCalDel - deltaCalDelay, oldVcThr,
                              oldCalDel + deltaCalDelay, oldVcThr);
        l1->SetLineColor(38);
        l1->Draw();
        l1 = new TLine(oldCalDel, oldVcThr - deltaVcThr,
                       oldCalDel, oldVcThr + deltaVcThr);
        l1->SetLineColor(38);
        l1->Draw();

        if (it->second.getValid()) {
            TLine *l2 = new TLine(newCalDel - deltaCalDelay, newVcThr, newCalDel + deltaCalDelay, newVcThr);
            l2->SetLineColor(kMagenta);
            l2->Draw();
            l2 = new TLine(newCalDel, newVcThr - deltaVcThr,
                           newCalDel, newVcThr + deltaVcThr);
            l2->SetLineColor(kWhite);
            l2->Draw();
        }

        canvas->Write();
    }

    outputFile_->Write();
    outputFile_->Close();

    //write out root file
    for (map<pos::PixelModuleName, pos::PixelDACSettings *>::iterator dacs = theDACs.begin(); dacs != theDACs.end(); ++dacs)
        dacs->second->writeASCII(outputDir());

    return makeSOAPMessageReference("EndCalibrationDone");
}
