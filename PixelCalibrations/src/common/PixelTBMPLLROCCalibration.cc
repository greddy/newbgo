#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelTBMPLLROCCalibration.h"

//#include <toolbox/convertstring.h>

using namespace pos;
using namespace std;

PixelTBMPLLROCCalibration::PixelTBMPLLROCCalibration(const PixelSupervisorConfiguration &tempConfiguration, SOAPCommander *mySOAPCmdr)
    : PixelCalibrationBase(tempConfiguration, *mySOAPCmdr) {
          std::cout << "Greetings from the PixelTBMPLLROCCalibration copy constructor." << std::endl;
}

void PixelTBMPLLROCCalibration::beginCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    // Check that PixelCalibConfiguration settings make sense.

    // if (!tempCalibObject->singleROC() && tempCalibObject->maxNumHitsPerROC() > 2 && tempCalibObject->parameterValue("OverflowWarning") != "no") {
    //   std::cout << "ERROR:  FIFO3 will overflow with more than two hits on each ROC.  To run this calibration, use 2 or less hits per ROC, or use SingleROC mode.  Now aborting..." << std::endl;
    //   assert(0);
    // }

    // if (!tempCalibObject->containsScan("TBMADelay") && !tempCalibObject->containsScan("TBMBDelay") && !tempCalibObject->containsScan("TBMPLL"))
    //   std::cout << "warning: none of TBMADelay, TBMBDelay, TBMPLLDelay found in scan variable list!" <<std::endl;

    //ToggleChannels = tempCalibObject->parameterValue("ToggleChannels") == "yes";
    //CycleScopeChannels = tempCalibObject->parameterValue("CycleScopeChannels") == "yes";
    DelayBeforeFirstTrigger = tempCalibObject->parameterValue("DelayBeforeFirstTrigger") == "yes";
    DelayEveryTrigger = tempCalibObject->parameterValue("DelayEveryTrigger") == "yes";

    cout << " Parameters :" << ToggleChannels << " " << CycleScopeChannels << " " << DelayBeforeFirstTrigger << " "
         << DelayEveryTrigger << endl;

    // std::map<std::string,int> currentTBMAdelay;
    // std::map<std::string,int> currentTBMBdelay;
    // std::map<std::string,double> nROCsForCurrentROCDelay;
    // std::map<std::string,int> passState;
    // std::map<std::string,int> bestROCDelay;
    // std::map<std::string,double> nROCsForBestROCDelay;

    // const std::vector<std::pair<unsigned, std::vector<unsigned> > >& fedsAndChannels =
    //   tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);

    // //PixelRootDirectoryMaker rootDirs(fedsAndChannels,gDirectory);

    // for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {  // fed loop

    //   //std::map<int,std::vector<TH1F*> > chTBMmap;
    //   //std::map<int,std::vector<TH2F*> > chTBMmap2D;
    //   for( unsigned int ch = 0; ch < fedsAndChannels[ifed].second.size(); ch++ ){

    //     cout<<" fed "<<fedsAndChannels[ifed].first<<" chan "<<(fedsAndChannels[ifed].second)[ch]<<endl;

    //   if (dacsToScan.size() == 0){

    // //find best settings for each module
    // for( std::map<std::string,std::vector<TH2F*> >::iterator it = ROCsHistoSum.begin();
    //      it != ROCsHistoSum.end(); ++it ) {

    //  PixelTBMSettings *TBMSettingsForThisModule=0;
    //  std::string moduleName=it->first;
    //  PixelConfigInterface::get(TBMSettingsForThisModule, "pixel/tbm/"+moduleName, *theGlobalKey_);
    //  assert(TBMSettingsForThisModule!=0);

    //  currentTBMAdelay[moduleName] = TBMSettingsForThisModule->getTBMADelay();
    //  currentTBMBdelay[moduleName] = TBMSettingsForThisModule->getTBMBDelay();

    //  // skip unused modules
    //  if(nROCsPerModule[moduleName] == 0 ){
    //    if(printSummary) cout<<" skip module "<<moduleName<<endl;
    //    passState[moduleName] = 0;
    //    continue;
    //  }

    // }
    // }
}
//
bool PixelTBMPLLROCCalibration::execute() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    const bool firstOfPattern = event_ % tempCalibObject->nTriggersPerPattern() == 0;
    const unsigned state = event_ / (tempCalibObject->nTriggersPerPattern());
    reportProgress(0.05);

    // Configure all TBMs and ROCs according to the PixelCalibConfiguration settings, but only when it's time for a new configuration.
    if (firstOfPattern) {
        //if (ToggleChannels) commandToAllFEDCrates("ToggleChannels");
        //commandToAllFECCrates("CalibRunning");

        Attribute_Vector parametersToFEC(3);
        parametersToFEC[0].name_ = "TBMPLLDelay";
        parametersToFEC[1].name_ = "TBMADelay";
        parametersToFEC[2].name_ = "TBMBDelay";
        parametersToFEC[0].value_ = "unchanged";
        parametersToFEC[1].value_ = "unchanged";
        parametersToFEC[2].value_ = "unchanged";

        parametersToFEC[0].value_ = itoa(0x0);
        parametersToFEC[1].value_ = itoa(0x0);
        parametersToFEC[2].value_ = itoa(0x0);

        // Configure all TBMs with these DAC values.
        //commandToAllFECCrates("SetTBMDACsEnMass", parametersToFEC);

        //Attribute_Vector parametersToFEC(2);
        // parametersToFEC[0].name_ = "DACAddress";  parametersToFEC[0].value_ = itoa(DACInfo.dacchannel());
        //parametersToFEC[1].name_ = "DACValue";    parametersToFEC[1].value_ = "0";
        //parametersToFEC[1].name_ = "DACValue";    parametersToFEC[1].value_ = itoa(DACInfo.value(i_DACSetting));
        //commandToAllFECCrates("SetROCDACsEnMass", parametersToFEC);

        std::cout << "Sleeping 1 seconds for feds to re-acquire phases" << std::endl;
        sleep(1); // 1sec
    }

    // if (CycleScopeChannels) {
    //   const int em36 = event_ % 36;
    //   const int which = em36 / 9;
    //   const int channel = em36 % 9;
    //   std::cout << "fiddling with SetScopeChannel event_ = " << event_ << " % 36 = " << em36 << " which = " << which << " channel = " << channel << std::endl;
    //   Attribute_Vector parametersToFED(2);
    //   parametersToFED[0].name_ = "Which"; parametersToFED[0].value_ = itoa(which);
    //   parametersToFED[1].name_ = "Ch";    parametersToFED[1].value_ = itoa(channel);
    //   commandToAllFEDCrates("SetScopeChannel", parametersToFED);
    // }

    // // should take this out
    // commandToAllFEDCrates("JMTJunk");

    if (DelayBeforeFirstTrigger && firstOfPattern)
        usleep(1000); // 1000 1ms

    if (DelayEveryTrigger)
        usleep(100000); // 100000 100ms

    // Send trigger to all TBMs and ROCs.
    sendTTCCalSync();

    // Read out data from each FED.
    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "RetrieveData";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = itoa(state);
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);

    return event_ + 1 < tempCalibObject->nTriggersTotal();
}

void PixelTBMPLLROCCalibration::endCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    assert(event_ == tempCalibObject->nTriggersTotal());

    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "Analyze";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = "0";
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);
}

std::vector<std::string> PixelTBMPLLROCCalibration::calibrated() {
    std::vector<std::string> tmp;
    tmp.push_back("tbm");
    return tmp;
}
