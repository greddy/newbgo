#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelTBMDelayCalibrationWithScores.h"

using namespace pos;

PixelTBMDelayCalibrationWithScores::PixelTBMDelayCalibrationWithScores(const PixelSupervisorConfiguration &tempConfiguration, SOAPCommander *mySOAPCmdr)
    : PixelCalibrationBase(tempConfiguration, *mySOAPCmdr) {
}

void PixelTBMDelayCalibrationWithScores::beginCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    fec_timer.setName("FEC programming");
    phasefinding_timer.setName("Phase finding");
    osd_timer.setName("OSD arming");
    trig_timer.setName("Triggering");
    fed_timer.setName("FED soaping");

    // Check that PixelCalibConfiguration settings make sense.

    CheckReadback = tempCalibObject->parameterValue("CheckReadback") != "no";
    PhaseFindingNeeded = tempCalibObject->containsScan("TBMPLL");

    if (CheckReadback && tempCalibObject->nTriggersPerPattern() % 256 != 0) {
        std::cout << "error: CheckReadback set, ntriggers should be a multiple of 8 rocs * 32 trigs = 256" << std::endl;
        assert(0);
    }

    if (!tempCalibObject->containsScan("TBMADelay") && !tempCalibObject->containsScan("TBMBDelay") && !tempCalibObject->containsScan("TBMPLL"))
        std::cout << "warning: none of TBMADelay, TBMBDelay, TBMPLLDelay found in scan variable list!" << std::endl;
}

bool PixelTBMDelayCalibrationWithScores::execute() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    const unsigned trigInPattern = event_ % tempCalibObject->nTriggersPerPattern();
    const bool firstOfPattern = trigInPattern == 0;
    const unsigned state = event_ / tempCalibObject->nTriggersPerPattern();
    reportProgress(0.05);

    // Configure all TBMs and ROCs according to the PixelCalibConfiguration settings, but only when it's time for a new configuration.
    if (firstOfPattern) {
        std::cout << "New TBM delay state " << state << std::endl;
        fec_timer.start();
        commandToAllFECCrates("CalibRunning");
        fec_timer.stop();

        // reset TBM, sleep for safety
        sendTTCTBMReset();
        usleep(1000.);

        if (PhaseFindingNeeded) {
            phasefinding_timer.start();
            commandToAllFEDCrates("FindPhasesNow");
            phasefinding_timer.stop();
        }
    }

    if (CheckReadback && trigInPattern % 32 == 0) {
        osd_timer.start();
        Attribute_Vector parametersToFED_arm(4);
        parametersToFED_arm[0].name_ = "VMEBaseAddress";
        parametersToFED_arm[0].value_ = "*";
        parametersToFED_arm[1].name_ = "Channel";
        parametersToFED_arm[1].value_ = "*";
        const std::string roc = itoa((trigInPattern / 32) % 8);
        parametersToFED_arm[2].name_ = "RocHi";
        parametersToFED_arm[2].value_ = roc;
        parametersToFED_arm[3].name_ = "RocLo";
        parametersToFED_arm[3].value_ = roc;
        commandToAllFEDCrates("ArmOSDFifo", parametersToFED_arm);
        osd_timer.stop();
    }

    // Send trigger to all TBMs and ROCs.
    trig_timer.start();
    sendTTCCalSync();
    trig_timer.stop();

    // Read out data from each FED.
    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "RetrieveData";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = itoa(state);
    fed_timer.start();
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);
    fed_timer.stop();

    return event_ + 1 < tempCalibObject->nTriggersTotal();
}

void PixelTBMDelayCalibrationWithScores::endCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    assert(event_ == tempCalibObject->nTriggersTotal());

    fec_timer.printStats();
    phasefinding_timer.printStats();
    trig_timer.printStats();
    osd_timer.printStats();
    fed_timer.printStats();

    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "Analyze";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = "0";
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);
}

std::vector<std::string> PixelTBMDelayCalibrationWithScores::calibrated() {
    return std::vector<std::string>(1, "tbm");
}
