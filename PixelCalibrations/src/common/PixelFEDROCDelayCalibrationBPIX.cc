#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelFEDROCDelayCalibrationBPIX.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelFEDDataTypes.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ColRowAddrDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigScopeDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigTransDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"
#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"
#include "TTree.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include <iomanip>
#include <algorithm>

#include "pixel/utils/Utils.h"

namespace {
const bool DO_DEBUG = false;
}

#define NO

///////////////////////////////////////////////////////////////////////////////////////////////
PixelFEDROCDelayCalibrationBPIX::PixelFEDROCDelayCalibrationBPIX(const PixelFEDSupervisorConfiguration &tempConfiguration, const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper), rootf(0) {
                                                                   std::cout << "In PixelFEDROCDelayCalibrationBPIX copy ctor()" << std::endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDROCDelayCalibrationBPIX::initializeFED() {
    //setFEDModeAndControlRegister(0x8, 0x30010);

    cout << " Disable back-end in order to avoid spyFIFO1 filling " << endl;
    for (FEDInterfaceMap::iterator iFED = FEDInterface_.begin(); iFED != FEDInterface_.end();
         ++iFED) {
      iFED->second->setAutoPhases(false); // added d.k. 8/23
      iFED->second->disableBE(true);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDROCDelayCalibrationBPIX::beginCalibration(xoap::MessageReference msg) {
    std::cout << "In PixelFEDROCDelayCalibrationBPIX::beginCalibration()" << std::endl;

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    tempCalibObject->writeASCII(outputDir());
    ncalls = 0;
    MinGoodBins = 2;
    MinEfficiency = 0.99;
    DumpFIFOs = tempCalibObject->parameterValue("DumpFIFOs") == "yes";
    ReadFifo1 = tempCalibObject->parameterValue("ReadFifo1") == "yes";
    HeadersOnly = tempCalibObject->parameterValue("HeadersOnly") == "yes";
    TBMHeadersOnly = tempCalibObject->parameterValue("TBMHeadersOnly") == "yes";
    IgnoreWordCount = tempCalibObject->parameterValue("IgnoreWordCount") == "yes";
    CheckReadback = tempCalibObject->parameterValue("CheckReadback") != "no";
    OrChannels = tempCalibObject->parameterValue("OrChannels") == "yes";
    findRange = tempCalibObject->parameterValue("findRange") == "yes";

    int MinGoodBins0 = atoi(tempCalibObject->parameterValue("MinGoodBins").c_str());
    float MinEfficiency0 = atof(tempCalibObject->parameterValue("MinEfficiency").c_str());

    // Set these 2 parameters only if they are defined in calib.dat
    if (MinGoodBins0 > 0)
        MinGoodBins = MinGoodBins0;
    if (MinEfficiency0 > 0)
        MinEfficiency = MinEfficiency0;

    forcePort1EqualToPort0 = (tempCalibObject->parameterValue("ForcePort1EqualToPort0") == "yes");
    keep400MhzFixed = (tempCalibObject->parameterValue("Keep400MhzFixed") == "yes");
    keepTTBits = (tempCalibObject->parameterValue("KeepTTBits") == "yes");

    inject_ = false;
    const std::vector<std::vector<unsigned int> > cols = tempCalibObject->columnList();
    const std::vector<std::vector<unsigned int> > rows = tempCalibObject->rowList();
    if (cols[0].size() != 0 && rows[0].size() != 0)
        inject_ = true;

    cout << " scan rocdelays " << tempCalibObject->numberOfScanVariables()
         << " variables, require hits " << inject_ << " MinGoodBins " << MinGoodBins
         << " MinEfficiency for best bin " << MinEfficiency << endl;

    for (unsigned dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        std::vector<unsigned int> dacvals = tempCalibObject->scanValues(dacname);
        if (dacvals.size() > 1)
            dacsToScan.push_back(dacname);
        if (DumpFIFOs) {
            std::cout << " scan dacnum " << dacnum << " name " << dacname << " size " << dacvals.size() << std::endl;
            for (unsigned int i = 0; i < dacvals.size(); ++i)
                std::cout << " dac value " << i << " is " << dacvals[i] << std::endl;
        } // if DumpFIFOs
    }

    if (dacsToScan.empty() && tempCalibObject->parameterValue("NoScanOK") != "yes") {
        cout << "no dacs in scan?" << endl;
        assert(0);
    }

    if (dacsToScan.size() < 3)
        BookEm("");

#ifdef NO
    // Setup FEDs if needed
    if (!ReadFifo1) {

        typedef std::set<std::pair<unsigned int, unsigned int> > colrow_t;
        const colrow_t colrows = tempCalibObject->pixelsWithHits(0);
        if (colrows.size() != 1) {
            std::cout << "must use exactly one pixel for score scan!\n";
            cout << " assert commented out " << endl;
            //assert(0);
        }
        the_col = int(colrows.begin()->first);
        the_row = int(colrows.begin()->second);
        const int dc = the_col / 2;
        const int pxl = 160 - 2 * the_row + the_col % 2;
        std::cout << "Expected hit in col " << the_col << " row " << the_row << " -> dc " << dc << " pxl " << pxl << "\n";

        const std::vector<std::pair<unsigned, std::vector<unsigned> > > &fedsAndChannels =
            tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);
        for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
            const unsigned fednumber = fedsAndChannels[ifed].first;
            const unsigned long vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
            PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];
            iFED->setPixelForScore(dc, pxl);
        }
    }

#endif

    return makeSOAPMessageReference("BeginCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDROCDelayCalibrationBPIX::execute(xoap::MessageReference msg) {
    vector<string> parameters_names(2);
    parameters_names.push_back( "WhatToDo");
    parameters_names.push_back( "StateNum");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, parameters_names);

    const unsigned state = atoi(parameters[ "StateNum"].c_str());

    if (parameters[ "WhatToDo"] == "RetrieveData")
        if (ReadFifo1) {
            RetrieveData(state);
        } else {
            RetrieveDataScores(state);
        }
    else if (parameters[ "WhatToDo"] == "Analyze")
        Analyze();
    else {
        cout << "ERROR: PixelFEDROCDelayCalibrationBPIX::execute() does not understand the WhatToDo command, " << parameters[ "WhatToDo"] << ", sent to it.\n";
        assert(0);
    }

    return makeSOAPMessageReference("FEDCalibrationsDone");
}

xoap::MessageReference PixelFEDROCDelayCalibrationBPIX::endCalibration(xoap::MessageReference msg) {

    std::cout << "In PixelFEDROCDelayCalibrationBPIX::endCalibration()" << std::endl;

    for (FEDInterfaceMap::iterator iFED = FEDInterface_.begin(); iFED != FEDInterface_.end();
         ++iFED) {
      iFED->second->setAutoPhases(true); // added d.k. 8/23
      iFED->second->disableBE(false);
    }

    return makeSOAPMessageReference("EndCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDROCDelayCalibrationBPIX::RetrieveDataScores(unsigned state) {
#ifdef NO

    static unsigned int state0 = 0, count = 0, count1 = 0;
    const bool DO_DEBUG = false;
    bool Dumps = DumpFIFOs;

    if (state != state0) { // new state
        state0 = state;
        count = 0;
        event_ = 0;
    }

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    // take care of this later
    // typedef std::set< std::pair<unsigned int, unsigned int> > colrow_t;
    // const colrow_t colrows = tempCalibObject->pixelsWithHits(state);
    // if (colrows.size() != 1) { // checked in initialize fed but need to keep checking
    //   std::cout << "must use exactly one pixel for score scan!\n";
    //   assert(0);
    // }
    // else if (int(colrows.begin()->first) != the_col || int(colrows.begin()->second) != the_row) {
    //   std::cout << "must use same pixel always for score scan!\n"; // JMTBAD or we could re-set the pixel in the fed
    //   assert(0);
    // }

    if (Dumps) {
        std::cout << "New FEDTBMDelay event " << event_ << " state " << state << " ";
        for (unsigned dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
            const std::string &dacname = tempCalibObject->scanName(dacnum);
            const unsigned dacvalue = tempCalibObject->scanValue(tempCalibObject->scanName(dacnum), state);
            std::cout << dacname << " -> " << dacvalue << " ";
        }
        std::cout << std::endl;
    }

    ++count;
    ++ncalls;
    const bool read_osd_now = ncalls % 32 == 0;
    //const unsigned osd_roc = (ncalls - 1) / 32 % 8;
    //std::cout << " read osd now is " << read_osd_now << " roc " << osd_roc << "\n";

    const std::vector<std::pair<unsigned, std::vector<unsigned> > > &fedsAndChannels =
        tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);

    for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const unsigned fednumber = fedsAndChannels[ifed].first;
        std::vector<bool> channels_used(49, 0);
        for (unsigned ich = 0; ich < fedsAndChannels[ifed].second.size(); ++ich)
            channels_used[fedsAndChannels[ifed].second[ich]] = true;

        if (Dumps)
            std::cout << "FED NUMBER " << fednumber << endl;
        const unsigned long vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface *fed = dynamic_cast<PixelPh1FEDInterface *>(FEDInterface_[vmeBaseAddress]);
        //const PixelFEDCard& fedcard = FEDInterface_[vmeBaseAddress]->getPixelFEDCard();
        //const int F1fiber = OverrideFifo1Fiber != -1 ? OverrideFifo1Fiber : fedcard.TransScopeCh + 1;
        std::vector<int> fibers_OK(25, 0);
        //Key key(fednumber);

        // Read scores
        std::vector<uint32_t> scores = fed->getScores();

        // Read ODS
        std::vector<uint32_t> osds(24);
        if (CheckReadback && read_osd_now) {
            osds = fed->readOSDValues();
        }

        for (int fiber = 1; fiber <= 24; ++fiber) {
            const int chA = fiber * 2 - 1;
            const int chB = fiber * 2;

            const bool chAused = channels_used[chA];
            const bool chBused = channels_used[chB];

            if (!chAused && !chBused)
                continue;

            //const std::vector<pos::PixelROCName>& rocsA = theNameTranslation_->getROCsFromFEDChannel(fedsAndChannels[ifed].first, (fedsAndChannels[ifed].second)[ch]);
            unsigned int rocSizeA = (theNameTranslation_->getROCsFromFEDChannel(fednumber, chA)).size();
            unsigned int rocSizeB = (theNameTranslation_->getROCsFromFEDChannel(fednumber, chB)).size();

            //const unsigned osd_roc = ((ncalls - 1) / 32) % 8;
            const unsigned osd_roc = ((ncalls - 1) / 32) % rocSizeA;
            if (DO_DEBUG)
                std::cout << " read osd now is " << read_osd_now << " roc " << osd_roc << "\n";
            if (DO_DEBUG)
                cout << " roc size " << ifed << " " << fiber << " " << chA << " " << rocSizeA << " " << chB << " " << rocSizeB << endl;

            const uint32_t scoreA = scores[chA - 1];
            const uint32_t scoreB = scores[chB - 1];

            const uint32_t AOSD = osds[fiber - 1] & 0xFFFF;
            const uint32_t BOSD = osds[fiber - 1] >> 16;

            if (Dumps) {
                // const uint32_t chs[2] = { chA, chB };
                const uint32_t chs[2] = { static_cast<uint32_t>(chA), static_cast<uint32_t>(chB) };
                const bool chused[2] = { chAused, chBused };
                const uint32_t scores[2] = { scoreA, scoreB };
                //const uint32_t osds[2] = {AOSD, BOSD};
                std::cout << "fiber " << fiber << " scores: " << count << " " << ncalls << endl;
                for (size_t i = 0; i < 2; ++i)
                    if (chused[i])
                        std::cout << "ch " << std::setw(2) << chs[i] << ": wc DDDDDDDD rrrrrrrr RRRRRRRR TH\n"
                                  << "       " << std::setw(2) << (scores[i] >> 26) << " "
                                  << std::bitset<8>((scores[i] >> 18) & 0xFF) << " "
                                  << std::bitset<8>((scores[i] >> 10) & 0xFF) << " "
                                  << std::bitset<8>((scores[i] >> 2) & 0xFF) << " "
                                  << std::bitset<2>(scores[i] & 0x3)
                                  << "\n";

                //   std::cout << "fiber " << fiber << " osds:\n";
                //   for (size_t i = 0; i < 2; ++i)
                // 	  if (chused[i])
                // 	    std::cout << "ch " << std::setw(2) << chs[i] << ": iiii rrrr oooooooo\n"
                // 		      << "       "
                // 		      << std::bitset<4>((osds[i] >> 12) & 0xF) << " "
                // 		      << std::bitset<4>((osds[i] >>  8) & 0xF) << " "
                // 		      << std::bitset<8>(osds[i] & 0xFF)
                // 		      << "\n";

            } // if Dumps

            // format is ccccccDDDDDDDDrrrrrrrrRRRRRRRRTH
            // c counts the number of words (0-63)
            // the D bits are one per roc, 1 if has the right pixel hit
            // r should all be 0 if no additional roc headers
            // R is whether that roc header was there
            // T for tbm trailer
            // H for tbm header
            bool AOK, BOK;
            uint32_t perfect_score = 0x4bfc03ff;
            if (rocSizeA == 4)
                perfect_score = 0x283C003F;
            else if (rocSizeA == 2)
                perfect_score = 0x183C000F;

            const uint32_t perfect_score_ignore_wordcount = perfect_score & 0x3ffffff;
            const uint32_t perfect_score_headers_only = perfect_score & 0x3ff;
            const uint32_t perfect_score_tbmheaders_only = perfect_score & 0x3;

            if (TBMHeadersOnly) {
                // so perfect headers is (score & 0x3ffff) == 0x3
                AOK = (scoreA & 0x3ffff) == perfect_score_tbmheaders_only;
                BOK = (scoreB & 0x3ffff) == perfect_score_tbmheaders_only;
            } else if (HeadersOnly) {
                // so perfect headers is (score & 0x3ffff) == 0x3ff
                AOK = (scoreA & 0x3ffff) == perfect_score_headers_only;
                BOK = (scoreB & 0x3ffff) == perfect_score_headers_only;
            } else if (IgnoreWordCount) {
                // want 18 words, 8 hits, 8 roc headers, tbm header, trailer
                AOK = (scoreA & 0x3ffffff) == perfect_score_ignore_wordcount;
                BOK = (scoreB & 0x3ffffff) == perfect_score_ignore_wordcount;
            } else {
                // want 18 words, 8 hits, 8 roc headers, tbm header, trailer
                AOK = scoreA == perfect_score;
                BOK = scoreB == perfect_score;
            }

            if (CheckReadback && read_osd_now) {
                // JMTBAD get readback register setting from dacs
                if (Dumps || 1)
                    cout << " osd " << osd_roc << "-" << (AOSD >> 12) % rocSizeA << " "
                         << osd_roc << "-" << (BOSD >> 12) % rocSizeA
                         << " for fiber " << fiber << " " << rocSizeA << endl;

                const bool AOSD_OK = ((AOSD >> 12) % rocSizeA == osd_roc); // && ((AOSD >> 8) & 0xF) == 12;
                //const bool BOSD_OK = (BOSD >> 12) == osd_roc + 8; // && ((BOSD >> 8) & 0xF) == 12;
                const bool BOSD_OK = ((BOSD >> 12) % rocSizeA == osd_roc); // && ((BOSD >> 8) & 0xF) == 12;
                AOK = AOK && AOSD_OK;
                BOK = BOK && BOSD_OK;

                //if (chAused) FillEm(state, key(chA, "OSDOK"), AOSD_OK);
                //if (chBused) FillEm(state, key(chB, "OSDOK"), BOSD_OK);
            } // id checkreadback

            if (!chAused)
                AOK = true;
            if (!chBused)
                BOK = true;

            const bool fiber_OK = OrChannels ? AOK || BOK : AOK && BOK;
            if (fiber_OK)
                count1++;
            if (DO_DEBUG)
                cout << AOK << " " << BOK << " " << rocSizeA << " " << hex << scoreA << " " << scoreB << " " << perfect_score << dec << " " << fiber_OK << " " << count1 << endl;

            if (fiber_OK)
                fibers_OK[fiber] = 1;

            // found tbm header & trailer in both channels
            //FillEm(state, fedsAndChannels[ifed].first, chA, 0, fiber_OK );
            //FillEm(state, fedsAndChannels[ifed].first, chB, 0, fiber_OK );
            //if(!TBMHeadersOnly) { // also histogram roc header
            //if(AOK) FillEm(state, fedsAndChannels[ifed].first, chA, 1, rocSizeA);
            //if(BOK) FillEm(state, fedsAndChannels[ifed].first, chB, 1, rocSizeA);
            //}

            if (AOK)
                FillEm(state, fedsAndChannels[ifed].first, chA, 0, rocSizeA, rocSizeA);
            if (BOK)
                FillEm(state, fedsAndChannels[ifed].first, chB, 0, rocSizeA, rocSizeA);

            for (unsigned int r = 0; r < rocSizeA; ++r) {
                if (AOK)
                    FillEm(state, fedsAndChannels[ifed].first, chA, 0, 1, r);
                if (BOK)
                    FillEm(state, fedsAndChannels[ifed].first, chB, 0, 1, r);
            }

        } // for fiber
    }     // for fed

    event_++;

#endif
}
///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDROCDelayCalibrationBPIX::RetrieveData(unsigned state) {
    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    const std::vector<std::pair<unsigned, std::vector<unsigned> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);

    if (DumpFIFOs)
        std::cout << "NEW FEDTBMDelay TRIGGER " << event_ << " state " << state << " ";
    std::map<std::string, unsigned int> currentDACValues;
    for (unsigned dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables(); ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        const unsigned dacvalue = tempCalibObject->scanValue(tempCalibObject->scanName(dacnum), state);
        currentDACValues[dacname] = dacvalue;
        if (DumpFIFOs)
            std::cout << dacname << " " << dacvalue << " ";
    }
    if (DumpFIFOs)
        std::cout << std::endl;

    // Does this mean that it only works for TBMA?
    if (dacsToScan.size() < 2 && currentDACValues["TBMADelay"] != lastTBMADelay) {
        event_ = 0;
        lastTBMADelay = currentDACValues["TBMADelay"];
    }
    //
    for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        const unsigned fednumber = fedsAndChannels[ifed].first;
        const unsigned long vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];

        //iFED->readDigFEDStatus(false, false);
        //const uint32_t fifoStatus = iFED->getFifoStatus();

        /* read fifo1 */
        //if( ReadFifo1 ) { // Presently the only option implemented

        for (unsigned int ch = 0; ch < fedsAndChannels[ifed].second.size(); ch++) {
            unsigned int channel = (fedsAndChannels[ifed].second)[ch];
            //bool found_TBMA = false;
            std::vector<int> chA_decodedROCs;
            std::vector<int> chB_decodedROCs;

            ///*******
            if (channel % 2 == 1) {
                int fib = (channel - 1) / 2;
                //cout << "Reading fiber " << fib << endl;
                iFED->setFIFO1(fib);
                PixelPh1FEDInterface::digfifo1 d = iFED->readFIFO1(DumpFIFOs);
                //printf("ch a: %i ch b: %i n tbm h a: %i b: %i  tbm t a: %i b: %i  roc h a: %i b: %i\n", d.a.ch, d.b.ch, d.a.n_tbm_h, d.b.n_tbm_h, d.a.n_tbm_t, d.b.n_tbm_t, d.a.n_roc_h, d.b.n_roc_h);
                unsigned int chA = d.a.ch;
                unsigned int chB = d.b.ch;

                ///*******
                if (DumpFIFOs) {
                    std::cout << "-----------------------------------" << std::endl;
                    std::cout << "Contents of FIFO 1 for fiber " << fib + 1 << std::endl;
                    cout << " a " << d.a.ch << " tbmh " << d.a.n_tbm_h << " t " << d.a.n_tbm_t << " roch "
                         << d.a.n_roc_h << " rocs " << d.a.rocs.size() << " hits " << d.a.hits.size() << endl;
                    cout << " b " << d.b.ch << " tbmh " << d.b.n_tbm_h << " t " << d.b.n_tbm_t << " roch "
                         << d.b.n_roc_h << " rocs " << d.b.rocs.size() << " hits " << d.b.hits.size() << endl;

                    if (DO_DEBUG) {
                        cout << " rocs size a/b " << d.a.rocs.size() << " " << d.b.rocs.size() << endl;
                        for (unsigned int i = 0; i < d.a.rocs.size(); i++) {
                            cout << i << ": d.a.rocs.roc " << d.a.rocs[i].roc << " d.a.rocs.rb "
                                 << d.a.rocs[i].rb << endl;
                        }
                        for (unsigned int i = 0; i < d.b.rocs.size(); i++) {
                            cout << i << ": d.b.rocs.roc " << d.b.rocs[i].roc << " d.b.rocs.rb "
                                 << d.b.rocs[i].rb << endl;
                        }

                        cout << " hits size a/b " << d.a.hits.size() << " " << d.b.hits.size() << endl;
                        for (unsigned int h = 0; h < d.a.hits.size(); ++h) {
                            cout << "Hit a " << h << ": ROC " << d.a.hits[h].roc << " col "
                                 << d.a.hits[h].col << " row " << d.a.hits[h].row << endl;
                        }
                        for (unsigned int h = 0; h < d.b.hits.size(); ++h) {
                            cout << "Hit b " << h << ": ROC " << d.b.hits[h].roc << " col "
                                 << d.b.hits[h].col << " row " << d.b.hits[h].row << endl;
                        }

                        std::cout << "-----------------------------------" << std::endl;

                        // The tests below work OK only for the valid region
                        //if(d.a.hits.size()!=4 || d.b.hits.size()!=4 ) {
                        if (d.a.hits.size() == 0 || d.b.hits.size() == 0) {
                            cout << " too few rocs/hits " << endl;
                            //int fum=0;
                            //cin>>fum;
                        }
                        //if(d.a.rocs.size()!=4 || d.b.rocs.size()!=4 ||
                        if (d.a.rocs.size() != 8 || d.b.rocs.size() != 8 ||
                            //d.a.n_tbm_h!=1 || d.a.n_tbm_t!=1 ||  d.a.n_roc_h!=4 ||
                            //d.b.n_tbm_h!=1 || d.b.n_tbm_t!=1 ||  d.b.n_roc_h!=4 ) {
                            d.a.n_tbm_h != 1 || d.a.n_tbm_t != 1 || d.a.n_roc_h != 8 ||
                            d.b.n_tbm_h != 1 || d.b.n_tbm_t != 1 || d.b.n_roc_h != 8) {
                            cout << " something wrong " << endl;
                            //int fum=0;
                            //cin>>fum;
                        }

                    } // if DO_DEBUG

                } // if dump

                // check the correctness of TBM header trailer
                if (d.a.n_tbm_h != 1 || d.a.n_tbm_t != 1 ||
                    d.b.n_tbm_h != 1 || d.b.n_tbm_t != 1) {
                    // TBM headers might be messed up if ROC delays are completeley wrong
                    // (multiple headers/trailers)
                    if (DO_DEBUG) {
                        cout << " something wrong with the TBM header/trailer " << endl;
                        cout << "Contents  for fiber " << fib + 1 << " channels " << channel << "/"
                             << (channel + 1) << endl;
                        cout << " a " << d.a.ch << " tbmh " << d.a.n_tbm_h << " t " << d.a.n_tbm_t << " roch "
                             << d.a.n_roc_h << " rocs " << d.a.rocs.size() << " hits " << d.a.hits.size() << endl;
                        cout << " b " << d.b.ch << " tbmh " << d.b.n_tbm_h << " t " << d.b.n_tbm_t << " roch "
                             << d.b.n_roc_h << " rocs " << d.b.rocs.size() << " hits " << d.b.hits.size() << endl;
                        cout << " skip this event " << endl;
                    }
                    continue; // skip this event, does not make sense to look at it
                }

                if ((channel != chA) || ((channel + 1) != chB)) {
                    cout << " wrong channel " << channel << " " << chA << " " << chB << " " << fib + 1 << endl;
                    cout << " a " << d.a.ch << " tbmh " << d.a.n_tbm_h << " t " << d.a.n_tbm_t << " roch "
                         << d.a.n_roc_h << " rocs " << d.a.rocs.size() << " hits " << d.a.hits.size() << endl;
                    cout << " b " << d.b.ch << " tbmh " << d.b.n_tbm_h << " t " << d.b.n_tbm_t << " roch "
                         << d.b.n_roc_h << " rocs " << d.b.rocs.size() << " hits " << d.b.hits.size() << endl;
                    continue;
                }

                //found_TBMA = d.a.n_tbm_h && d.a.n_tbm_t;

                // We should how the roc-header recognition is made when hits are present
                if (!inject_) { // No hits, so just look at header or trailers
                    //cout << "ROCs A " << endl;
                    for (unsigned int i = 0; i < d.a.rocs.size(); i++) {
                        //cout << i << ": d.a.rocs.roc " << d.a.rocs[i].roc << " d.a.rocs.rb " << d.a.rocs[i].rb << endl;
                        chA_decodedROCs.push_back(d.a.rocs[i].roc);
                    }
                    //cout << "ROCs B " << endl;
                    for (unsigned int i = 0; i < d.b.rocs.size(); i++) {
                        //cout << i << ": d.b.rocs.roc " << d.b.rocs[i].roc << " d.b.rocs.rb " << d.b.rocs[i].rb << endl;
                        chB_decodedROCs.push_back(d.b.rocs[i].roc);
                    }

                } else { // use hits
                    if (DumpFIFOs)
                        cout << "Number of hits ROCA:  " << d.a.hits.size() << endl;
                    for (unsigned int h = 0; h < d.a.hits.size(); ++h) {
                        //if (DumpFIFOs ) cout << "Hit " << h << ": ROC " << d.a.hits[h].roc << " col " << d.a.hits[h].col <<  " row " << d.a.hits[h].row  << endl;
                        if (std::find(chA_decodedROCs.begin(), chA_decodedROCs.end(), d.a.hits[h].roc) == chA_decodedROCs.end()) {
                            chA_decodedROCs.push_back(d.a.hits[h].roc);
                        }
                    }

                    if (DumpFIFOs)
                        cout << "Number of hits ROCB:  " << d.b.hits.size() << endl;
                    for (unsigned int h = 0; h < d.b.hits.size(); ++h) {
                        //if (DumpFIFOs ) cout << "Hit " << h << ": ROC " << d.b.hits[h].roc << " col " << d.b.hits[h].col <<  " row " << d.b.hits[h].row  << endl;
                        if (std::find(chB_decodedROCs.begin(), chB_decodedROCs.end(), d.b.hits[h].roc) == chB_decodedROCs.end()) {
                            chB_decodedROCs.push_back(d.b.hits[h].roc);
                        }
                    }
                }

                const std::vector<pos::PixelROCName> &rocsA = theNameTranslation_->getROCsFromFEDChannel(fedsAndChannels[ifed].first, (fedsAndChannels[ifed].second)[ch]);
                const std::vector<pos::PixelROCName> &rocsB = theNameTranslation_->getROCsFromFEDChannel(fedsAndChannels[ifed].first, (fedsAndChannels[ifed].second)[ch + 1]);
                //cout<<state<<" "<<event_<<" "<<chA<<"-"<<chB<<" "<<chA_decodedROCs.size()<<" - "<<chB_decodedROCs.size()<<endl;
                FillEm(state, fedsAndChannels[ifed].first, chA, 0, chA_decodedROCs.size(), rocsA.size());
                FillEm(state, fedsAndChannels[ifed].first, chB, 0, chB_decodedROCs.size(), rocsB.size());

                for (unsigned int r = 0; r < rocsA.size(); ++r) {
                    if (DumpFIFOs)
                        cout << "rocnameA " << rocsA[r].rocname() << endl;
                    bool chA_foundROC = std::find(chA_decodedROCs.begin(), chA_decodedROCs.end(), r + 1) != chA_decodedROCs.end();
                    if (chA_foundROC) {
                        FillEm(state, fedsAndChannels[ifed].first, chA, 0, 1, r);
                        //cout<<chA<<" "<<r<<endl;
                    }
                }
                for (unsigned int r = 0; r < rocsB.size(); ++r) {
                    if (DumpFIFOs)
                        cout << "rocnameB " << rocsB[r].rocname() << endl;
                    bool chB_foundROC = std::find(chB_decodedROCs.begin(), chB_decodedROCs.end(), r + 1) != chB_decodedROCs.end();
                    if (chB_foundROC) {
                        FillEm(state, fedsAndChannels[ifed].first, chB, 0, 1, r);
                        //cout<<chB<<" "<<r<<endl;
                    }
                }
            }
        } // end loop on channels

        //}//end readFifo1

        if (DO_DEBUG) {
            //iFED->autoPhases();
            iFED->getFifoStatus();
            ////iFED->getFifoFillLevel(); // cannot be used
            //iFED->sendResets(1); // cannot be used
        }

        // Special trick to clean FIFO and unblock FIFO1 after some max number of events
        //uint64_t buffer64[pos::fifo3Depth];
        //int status=iFED->spySlink64(buffer64); // read data
        //if(DO_DEBUG) cout<<" status "<<status<<endl;
    }

    event_++;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDROCDelayCalibrationBPIX::Analyze() {
    const bool printSummary = true;
    //bool findRange = true;

    if (printSummary)
        cout << " Run Analysis " << endl;

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    const std::vector<std::pair<unsigned, std::vector<unsigned> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);

    int scanValueMin = tempCalibObject->scanValueMin("TBMADelay");
    scanValueMin = (scanValueMin & 192) >> 6;
    int mask = 0;
    if (scanValueMin == 0)
        mask = 0;
    else if (scanValueMin == 1)
        mask = 64;
    else if (scanValueMin == 2)
        mask = 128;
    else if (scanValueMin == 3)
        mask = 192;

    int ntriggers = event_ - 1;
    if (forcePort1EqualToPort0)
        ntriggers = ntriggers * 8; //  repeated 8 time becaose port1=port0
    if (DumpFIFOs)
        cout << "Analyze " << scanValueMin << " " << mask << " ntrigers " << endl;

    //normalize by number of triggers
    for (std::map<int, std::map<int, std::vector<TH2F *> > >::iterator it1 = scansROCs.begin(); it1 != scansROCs.end(); ++it1) {
        for (std::map<int, std::vector<TH2F *> >::iterator it2 = it1->second.begin(); it2 != it1->second.end(); ++it2) {
            for (unsigned int i = 0; i < it2->second.size(); ++i)
                it2->second[i]->Scale(1. / ntriggers);
        }
    }

    std::map<std::string, std::map<int, int> > nGoodBinsPerModule;
    std::map<std::string, int> nROCsPerModule;
    //fill histo with sum of channels
    for (std::map<int, std::map<int, std::vector<TH2F *> > >::iterator it1 = scansROCs.begin(); it1 != scansROCs.end(); ++it1) {
        std::string moduleName = "";
        pos::PixelChannel theChannel;
        for (std::map<int, std::vector<TH2F *> >::iterator it2 = it1->second.begin(); it2 != it1->second.end(); ++it2) {

            if (theNameTranslation_->FEDChannelExist(it1->first, it2->first)) {
                theChannel = theNameTranslation_->ChannelFromFEDChannel(it1->first, it2->first);
                moduleName = theChannel.modulename();
            }

            const std::vector<pos::PixelROCName> &rocs = theNameTranslation_->getROCsFromFEDChannel(it1->first, it2->first);
            if (nROCsPerModule.find(moduleName) == nROCsPerModule.end()) {
                nROCsPerModule[moduleName] = rocs.size();
            } else {
                nROCsPerModule[moduleName] += rocs.size();
            }

            //count bins with nrocs == 4/8
            int nGoodBins = 0;
            for (int bx = 1; bx < it2->second[rocs.size()]->GetNbinsX() + 1; ++bx) {
                for (int by = 1; by < it2->second[rocs.size()]->GetNbinsY() + 1; ++by) {
                    //cout<<bx<<" "<<by<<" "<<it2->first<<" "
                    //<<it2->second[rocs.size()]->GetBinContent(bx,by)<<" "
                    //<<rocs.size()<<endl;
                    if (it2->second[rocs.size()]->GetBinContent(bx, by) >=
                        (double(rocs.size()) * MinEfficiency)) {
                        nGoodBins++;
                        if (DumpFIFOs)
                            std::cout << " - found bin (" << bx << "," << by << ") nrocs = "
                                      << it2->second[rocs.size()]->GetBinContent(bx, by) << std::endl;
                    }
                } //close loop on by
            }     //close loop on bx
            nGoodBinsPerModule[moduleName][it2->first] = nGoodBins;
            //cout<<" get best bins "<<moduleName<<" "<<nGoodBins<<" "<<it2->first<<" "<<endl;
            ROCsHistoSum[moduleName][0]->Add(it2->second[rocs.size()]);

        } //close loop on channels
    }     //close loop on fed

    std::map<std::string, int> currentTBMAdelay;
    std::map<std::string, int> currentTBMBdelay;
    std::map<std::string, double> nROCsForCurrentROCDelay;
    std::map<std::string, int> passState;
    std::map<std::string, int> bestROCDelay;
    std::map<std::string, double> nROCsForBestROCDelay;

    //find best settings for each module
    for (std::map<std::string, std::vector<TH2F *> >::iterator it = ROCsHistoSum.begin();
         it != ROCsHistoSum.end(); ++it) {

        pos::PixelTBMSettings *TBMSettingsForThisModule = 0;
        std::string moduleName = it->first;
        PixelConfigInterface::get(TBMSettingsForThisModule, "pixel/tbm/" + moduleName, *theGlobalKey_);
        assert(TBMSettingsForThisModule != 0);

        currentTBMAdelay[moduleName] = TBMSettingsForThisModule->getTBMADelay();
        currentTBMBdelay[moduleName] = TBMSettingsForThisModule->getTBMBDelay();

        // skip unused modules
        if (nROCsPerModule[moduleName] == 0) {
	  //if (printSummary)
	  //    cout << " skip module " << moduleName << endl;
            passState[moduleName] = 0;
            continue;
        }

        int port0_A = (currentTBMAdelay[moduleName] & 7);
        int port1_A = (currentTBMAdelay[moduleName] & 56) >> 3;

        nROCsForCurrentROCDelay[moduleName] = (it->second)[0]->GetBinContent(port1_A + 1, port0_A + 1);
        bestROCDelay[moduleName] = currentTBMAdelay[moduleName];
        nROCsForBestROCDelay[moduleName] = nROCsForCurrentROCDelay[moduleName];

        if (DumpFIFOs || printSummary)
            cout << moduleName << " current settings "
                 << currentTBMAdelay[moduleName] << " " << currentTBMBdelay[moduleName] << " "
                 << port0_A << " " << port1_A << " " << nROCsForCurrentROCDelay[moduleName] << " "
                 << bestROCDelay[moduleName] << " "
                 << nROCsPerModule[moduleName] << endl;

        double eff = double(nROCsPerModule[moduleName]) * MinEfficiency; // minimum efficiency we look for
  
	bool rangeFailed=false;
	// Do the findRange 
	if (findRange) {                                                 // look for a good range and place ourselves in the middle
            //cout<< " find range "<<endl;

            int count = 0;
            std::map<int, int> bestBins;
            for (int bx = 1; bx < it->second[0]->GetNbinsX() + 1; ++bx) {
                if (it->second[0]->GetBinContent(bx, bx) >= eff) { // look only at the diagonal
                    bestBins[bx - 1] = 1;
                    count++;
                } else
                    bestBins[bx - 1] = 0;
            }

            //  correct for overlaps
            bestBins[7] = bestBins[0];

            // none found
            if (count < MinGoodBins) { // must be at least 3
                passState[moduleName] = 0;
                if (printSummary)
                    cout << "not enough good bins found "
                         << moduleName << " " << count << " " << passState[moduleName] << endl;
		if(count>0) {cout<<"switch to no range "<<endl; rangeFailed=true;} // switch to no range finding 

            } else { // bestBins.size()>=3

                if (DO_DEBUG) {
                    cout << " bins fine: ";
                    for (int i = 0; i < 8; i++)
                        cout << bestBins[i] << " ";
                    cout << endl;
                }

                passState[moduleName] = 0;
                int bestX = -1, bestY = -1;
                for (int i = 1; i < 8; i++) {
                    if (i < 7 && bestBins[i] == 1 && bestBins[i - 1] == 1 && bestBins[i + 1] == 1) { // found good point
                        bestX = i;
                        bestY = i;
                        passState[moduleName] = 1;
                        if (DO_DEBUG)
                            cout << " found point " << i << " " << bestBins[i] << endl;
                        break;
                    } else if (i == 7 && bestBins[0] == 1 && bestBins[1] == 1 && bestBins[6] == 1 && bestBins[6] == 1) {
                        bestX = 0;
                        bestY = 0;
                        passState[moduleName] = 1;
                        if (DO_DEBUG)
                            cout << " found point " << i << " " << bestBins[i] << endl;
                    } // if
                }

                if (passState[moduleName] == 1) {
                    if (printSummary)
                        cout << " efficiency ok, use new settings " << moduleName << " "
                             << nROCsForBestROCDelay[moduleName] << bestX << " " << bestY << " ";

                    if (keepTTBits) { // use the default TT value, from config
                        unsigned int tt = (currentTBMAdelay[moduleName] & 0xC0);
                        bestX = ((bestX & 0x7) << 3) | tt;
                    } else { // use TT from teh scan
                        bestX = (bestX << 3) | mask;
                    }

                    bestROCDelay[moduleName] = bestX | (bestY & 0x7);
                    if (printSummary)
                        cout << hex << bestROCDelay[moduleName] << dec << passState[moduleName] << " "
                             << nROCsForBestROCDelay[moduleName] << endl;
                } else { // failed
                    if (printSummary)
                        cout <<"no good region with 3 good continuouse bins found  "
                             << moduleName << " " << passState[moduleName] << endl;
		    {cout<<"switch to no range "<<endl; rangeFailed=true;} // switch to no range finding 

                } // end if pass

            } // end if  count

	} // end findRange

	// no range method, redo if the range finding has failed 
        if (!findRange || rangeFailed)  { // old method to just look for the first valid point

            //first check if current roc delay gives all rocs
            if (double(nROCsForCurrentROCDelay[moduleName]) >= eff) {
                passState[moduleName] = 1;
                if (printSummary)
                    cout << " keep old settings (still good)  " << moduleName << " " << passState[moduleName]
                         << " " << nROCsForCurrentROCDelay[moduleName] << endl;

            } else { // if not then find best bins for which nrocs = all

                std::map<int, int> bestBins;
                for (int bx = 1; bx < it->second[0]->GetNbinsX() + 1; ++bx) {
                    for (int by = 1; by < it->second[0]->GetNbinsY() + 1; ++by) {
                        if (it->second[0]->GetBinContent(bx, by) == nROCsPerModule[moduleName])
                            bestBins[bx - 1] = by - 1;
                    }
                }

                if (bestBins.size() == 0) { //if no bins are found with nrocs = 16 find the bins with max nrocs
		  passState[moduleName] = 0;
		  if (printSummary)
		    cout << " keep old settings (no good region found)  " << moduleName << " " << bestBins.size()
			 << " " << passState[moduleName] << endl;
		  
		  double nrocs = 0;
		  int binxbest = 0;
		  int binybest = 0;
		  for (int bx = 1; bx < it->second[0]->GetNbinsX() + 1; ++bx) {
		    for (int by = 1; by < it->second[0]->GetNbinsY() + 1; ++by) {
		      if (it->second[0]->GetBinContent(bx, by) >= nrocs) {
			nrocs = it->second[0]->GetBinContent(bx, by);
			binxbest = bx - 1;
			binybest = by - 1;
		      } //close if
		    }     //close loop on by
		  }         //close loop on bx
		  
		  bestBins[binxbest] = binybest;
		  double maxNrocs = it->second[0]->GetBinContent(binxbest + 1, binybest + 1);
		  
		  if(printSummary) cout<<"best bin "<<binybest<<" "<<maxNrocs<<endl;
		  //check if there are other bins with same max eff
		  for (int bx = 1; bx < it->second[0]->GetNbinsX() + 1; ++bx) {
		    for (int by = 1; by < it->second[0]->GetNbinsY() + 1; ++by) {
		      if (it->second[0]->GetBinContent(bx, by) == maxNrocs) {
			std::map<int, int>::iterator binsIt = bestBins.find(bx - 1);
			if (binsIt->first != binxbest && binsIt->second != binybest)
			  bestBins[bx - 1] = by - 1;
		      }
		      
		    } //close loop on by
		  }     //close loop on bx
		  
		  //}//close bestBins.size() == 0
		  //if( bestBins.size() == 0 ) {
		  passState[moduleName] = 0; //at this point this should not be true
		  cout << " size 0 , passed 0 " << moduleName << endl;

                } else { // bestBins.size()>0

                    int bestX = 0, bestY = 0;
                    for (std::map<int, int>::iterator binsIt = bestBins.begin(); binsIt != bestBins.end(); ++binsIt) {
                        if (binsIt->first >= 0 && binsIt->first <= 7 && binsIt->second >= 0 && binsIt->second <= 7) {
                            bestX = binsIt->first;
                            bestY = binsIt->second;
                            break;
                        }
                    }

                    if (bestX == 0 && bestY == 0) {
                        bestX = (bestBins.begin())->first;
                        bestY = (bestBins.begin())->second;
                    }

                    nROCsForBestROCDelay[moduleName] = it->second[0]->GetBinContent(bestX + 1, bestY + 1);
                    if (nROCsForBestROCDelay[moduleName] < eff) {
                        passState[moduleName] = 0;
                        if (printSummary)
                            cout << " efficiency low, use old settings " << moduleName << " "
                                 << nROCsForBestROCDelay[moduleName] << endl;

                    } else { // OK

                        if (printSummary)
                            cout << " efficiency ok, use new settings " << moduleName << " "
                                 << nROCsForBestROCDelay[moduleName] << bestX << " " << bestY << " ";
                        passState[moduleName] = 1;

                        //check that the module has enough good bins for each fed channel otherwise set flag = 0
                        for (std::map<int, int>::iterator gb = nGoodBinsPerModule[moduleName].begin();
                             gb != nGoodBinsPerModule[moduleName].end(); ++gb) {
                            cout << " counts " << moduleName << " " << gb->second << " " << endl;
                            if (gb->second < MinGoodBins) {
                                passState[moduleName] = 0;
                                cout << " too few counts " << gb->second << " " << passState[moduleName] << endl;
                                break;
                            } // if
                        }     // for

                    } // if eff

                    if (keepTTBits) { // use the default TT value, from config
                        unsigned int tt = (currentTBMAdelay[moduleName] & 0xC0);
                        bestX = ((bestX & 0x7) << 3) | tt;
                    } else { // use TT from teh scan
                        bestX = (bestX << 3) | mask;
                    }

                    bestROCDelay[moduleName] = bestX | (bestY & 0x7);
                    if (printSummary && passState[moduleName] == 1)
                        cout << hex << bestROCDelay[moduleName] << dec << passState[moduleName] << " "
                             << nROCsForBestROCDelay[moduleName] << endl;

                } //found nROCs==16

            } //close case nROCS!=16 for current settings

        } // close find 

    } //close loop on modules

    rootf->cd();
    branch theBranch;
    branch_sum theBranch_sum;
    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("PassState", "PassState");
    TTree *tree_sum = new TTree("SummaryInfo", "SummaryInfo");

    tree->Branch("PassState", &theBranch, "pass/F:moduleName/C", 4096000);
    tree_sum->Branch("SummaryInfo", &theBranch_sum, "deltaTBMAdelayX/I:deltaTBMAdelayY/I:deltaTBMBdelayX/I:deltaTBMBdelayY/I:newTBMAdelayX/I:newTBMAdelayY/I:newTBMBdelayX/I:newTBMBdelayX/I:nROCs/D:moduleName/C", 4096000);
    rootf->cd();

    string s = pixel::utils::to_string(crate_);
    ofstream out((outputDir() + "/summary_" + s + ".txt").c_str());

    //ofstream out((outputDir()+"/summary.txt").c_str()); //leave the file method intact for now
    assert(out.good());

    std::vector<pos::PixelModuleName> modules = theDetectorConfiguration_->getModuleList();
    for (std::map<std::string, std::vector<TH2F *> >::iterator it = ROCsHistoSum.begin(); it != ROCsHistoSum.end(); ++it) {

      std::string moduleName = it->first;
      // find the crate number
      long unsigned crate=0;
      std::vector<pos::PixelModuleName>::iterator m1;
      for (m1 = modules.begin(); m1 != modules.end(); m1++) {
	if (m1->modulename() == moduleName) {
	  crate = theFEDConfiguration_->crateFromFEDNumber(theNameTranslation_->firstHdwAddress(*m1).fednumber() );
	  //cout<<"found crate "<<crate<<" for "<<moduleName<<endl;
	  break;
	}
      }

      //cout << moduleName << " 0 " << passState[moduleName] <<" ";
      //cout<<crate_<<" "<<crate<<endl;

      //prevent the calibration to overwrite ASCII output files which are not of this FEDSupervisor's business  
      if(crate_ != crate) {continue;} // skip module from other crates 


      //THIS DOES NOT WORK
      //std::vector<pos::PixelModuleName>::iterator m;
      //for (m = modules.begin(); m != modules.end(); m++)
      //if (m->modulename() == moduleName)
      // if (crate_ != theFEDConfiguration_->crateFromFEDNumber(theNameTranslation_->firstHdwAddress(*m).fednumber()))  {continue;}
 
      if ( (DumpFIFOs || printSummary) ) 
	cout <<moduleName <<" pass "<< passState[moduleName] <<" crate "<<crate_
	     <<" old "<<currentTBMAdelay[moduleName]
	     <<" new "<<bestROCDelay[moduleName]<<endl;
      
      int oldTBMADelayX = (currentTBMAdelay[moduleName] & 56) >> 3;
      int oldTBMADelayY = (currentTBMAdelay[moduleName] & 7);
      int oldTBMBDelayX = (currentTBMBdelay[moduleName] & 56) >> 3;
      int oldTBMBDelayY = (currentTBMBdelay[moduleName] & 7);
      
      int newTBMADelayX = (bestROCDelay[moduleName] & 56) >> 3;
      int newTBMADelayY = (bestROCDelay[moduleName] & 7);
      int newTBMBDelayX = (bestROCDelay[moduleName] & 56) >> 3;
      int newTBMBDelayY = (bestROCDelay[moduleName] & 7);
      
      if (passState[moduleName] == 0) { // keep the same old settings if failed
	bestROCDelay[moduleName] = currentTBMAdelay[moduleName];
	newTBMADelayX = (currentTBMAdelay[moduleName] & 56) >> 3;
	newTBMADelayY = (currentTBMAdelay[moduleName] & 7);
	newTBMBDelayX = (currentTBMBdelay[moduleName] & 56) >> 3;
	newTBMBDelayY = (currentTBMBdelay[moduleName] & 7);
      }
      
      if ( (DumpFIFOs || printSummary) && (passState[moduleName] == 1) ) {
	cout << moduleName << " " << passState[moduleName] << endl;
	cout << " old " << oldTBMADelayX << " " << oldTBMADelayY << " " << oldTBMBDelayX << " " << oldTBMBDelayY
	     << " a/b " << currentTBMAdelay[moduleName] << "/" << currentTBMBdelay[moduleName]
	     << " a/b " << hex << currentTBMAdelay[moduleName] << "/" << currentTBMBdelay[moduleName] << dec
	     << endl;
	cout << " new " << newTBMADelayX << " " << newTBMADelayY << " " << newTBMBDelayX
	     << " " << newTBMBDelayY
	     << " a/b " << bestROCDelay[moduleName] << "/" << bestROCDelay[moduleName]
	     << " a/b " << hex << bestROCDelay[moduleName] << "/" << bestROCDelay[moduleName] << dec
	     << endl;
      }
      
      theBranch_sum.deltaTBMAdelayX = newTBMADelayX - oldTBMADelayX;
      theBranch_sum.deltaTBMAdelayY = newTBMADelayY - oldTBMADelayY;
      theBranch_sum.deltaTBMBdelayX = newTBMBDelayX - oldTBMBDelayX;
      theBranch_sum.deltaTBMBdelayY = newTBMBDelayY - oldTBMBDelayY;
      theBranch_sum.newTBMAdelayX = newTBMADelayX;
      theBranch_sum.newTBMAdelayY = newTBMADelayY;
      theBranch_sum.newTBMBdelayX = newTBMBDelayX;
      theBranch_sum.newTBMBdelayY = newTBMBDelayY;
      theBranch_sum.nROCs = nROCsForBestROCDelay[moduleName];
      strcpy(theBranch_sum.moduleName, moduleName.c_str());
      theBranch.pass = passState[moduleName];
      strcpy(theBranch.moduleName, moduleName.c_str());
      
      tree->Fill();
      tree_sum->Fill();

      pos::PixelTBMSettings *TBMSettingsForThisModule = 0;
      PixelConfigInterface::get(TBMSettingsForThisModule, "pixel/tbm/" + moduleName, *theGlobalKey_);
      assert(TBMSettingsForThisModule != 0);
      
      cout<<moduleName<<" "<<passState[moduleName]<<" "
	  <<currentTBMAdelay[moduleName]<<" "<<bestROCDelay[moduleName]<<endl;
      
      if (passState[moduleName] == 0) { // keep the same old settings if failed
	if ( (DumpFIFOs || printSummary) ) 
	  cout<<"failed, write old settings "<<moduleName<<" "<<currentTBMAdelay[moduleName]<<endl;
	TBMSettingsForThisModule->setTBMADelay(currentTBMAdelay[moduleName]);
	TBMSettingsForThisModule->setTBMBDelay(currentTBMBdelay[moduleName]);
      } else { // for the new point use common values for A&B
	if ( (DumpFIFOs || printSummary) ) 
	  cout<<"passed, write new settings "<<moduleName<<" "<<bestROCDelay[moduleName]<<endl;
	TBMSettingsForThisModule->setTBMADelay(bestROCDelay[moduleName]);
	TBMSettingsForThisModule->setTBMBDelay(bestROCDelay[moduleName]);
      }
      TBMSettingsForThisModule->writeASCII(outputDir());
      //std::cout << "Wrote TBM settings for module:" << moduleName << endl;
      delete TBMSettingsForThisModule;
    }  // for modules
    
    CloseRootf();

    //now print summary and save it on text file
    std::map<std::string, std::vector<int> > FEDchannelsPerModule;
    for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {
        std::string moduleName = "";
        for (unsigned int ch = 0; ch < fedsAndChannels[ifed].second.size(); ch++) {
            pos::PixelChannel theChannel = theNameTranslation_->ChannelFromFEDChannel(fedsAndChannels[ifed].first, (fedsAndChannels[ifed].second)[ch]);
            moduleName = theChannel.modulename();
            FEDchannelsPerModule[moduleName].push_back((fedsAndChannels[ifed].second)[ch]);
        }
    }

    out << "Module                        | FED channels | port 0 phase | port 1 phase | DeltaROCDelay | # ROCs | Pass | \n";
    cout << "Module                        | FED channels | port 0 phase | port 1 phase | DeltaROCDelay | # ROCs | Pass | \n";

    for (std::map<std::string, std::vector<int> >::iterator it = FEDchannelsPerModule.begin(); it != FEDchannelsPerModule.end(); ++it) {
        out << std::setw(33) << std::left << it->first << " | ";
        cout << std::setw(33) << std::left << it->first << " | "; // module
        for (unsigned int i = 0; i < (it->second).size(); ++i) {
            out << std::setw(2) << (it->second)[i] << " ";
            cout << std::setw(2) << (it->second)[i] << " "; // channels
        }
        out << " | ";
        out << std::setw(6) << (bestROCDelay[it->first] & 7);
        out << " | ";
        out << std::setw(6) << ((bestROCDelay[it->first] & 56) >> 3);
        out << " | ";
        out << std::setw(6) << (bestROCDelay[it->first] - currentTBMAdelay[it->first]);
        out.precision(4);
        out << " | ";
        out << std::setw(6) << nROCsForBestROCDelay[it->first];
        out << " | ";
        out << std::setw(6) << passState[it->first];
        out << " | \n";

        cout << " | ";
        cout << std::setw(6) << (bestROCDelay[it->first] & 7);
        cout << " | ";
        cout << std::setw(6) << ((bestROCDelay[it->first] & 56) >> 3);
        cout << " | ";
        cout << std::setw(6) << (bestROCDelay[it->first] - currentTBMAdelay[it->first]);
        cout.precision(4);
        cout << " | ";
        cout << std::setw(6) << nROCsForBestROCDelay[it->first];
        cout << " | ";
        cout << std::setw(6) << passState[it->first];
        cout << " | ";
        cout << std::setw(6) << hex << bestROCDelay[it->first] << "/" << dec << bestROCDelay[it->first];
        cout << endl;

        /*cout << " | " << (bestROCDelay[it->first]&0x07) << "            | " << ((bestROCDelay[it->first]&0x38)>>3);
   cout << "            | " << bestROCDelay[it->first] - currentTBMAdelay[it->first];
   cout.precision(4);
   cout << "            | " << nROCsForBestROCDelay[it->first] << "  | " << passState[it->first] << "    | "
   <<hex<<bestROCDelay[it->first]<<"/"<<dec<<bestROCDelay[it->first]<<endl;*/
    }
    cout << endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDROCDelayCalibrationBPIX::CloseRootf() {
    if (rootf) {
        rootf->Write();
        rootf->Close();
        delete rootf;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDROCDelayCalibrationBPIX::BookEm(const TString &path) {

    scansROCs.clear();
    ROCsHistoSum.clear();

    TString root_fn;
    if (path == "")
        root_fn.Form("%s/ROCDelay_%lu.root", outputDir().c_str(), crate_);
    else
        root_fn.Form("%s/ROCDelay_%s_%lu.root", outputDir().c_str(), path.Data(), crate_);
    cout << "writing histograms to file " << root_fn << endl;
    CloseRootf();
    rootf = new TFile(root_fn, "create");
    assert(rootf->IsOpen());

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    const std::vector<std::pair<unsigned, std::vector<unsigned> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);

    PixelRootDirectoryMaker rootDirs(fedsAndChannels, gDirectory);

    for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {

        //std::map<int,std::vector<TH1F*> > chTBMmap;
        std::map<int, std::vector<TH2F *> > chTBMmap2D;
        for (unsigned int ch = 0; ch < fedsAndChannels[ifed].second.size(); ch++) {

            rootDirs.cdDirectory(fedsAndChannels[ifed].first, (fedsAndChannels[ifed].second)[ch]);

            std::vector<TH2F *> histosROCs;
            TH2F *h_nROCHeaders = 0;

            const std::vector<pos::PixelROCName> &rocs = theNameTranslation_->getROCsFromFEDChannel(fedsAndChannels[ifed].first, (fedsAndChannels[ifed].second)[ch]);
            for (unsigned int r = 0; r < rocs.size(); ++r) {
                TString hname(rocs[r].rocname());
                h_nROCHeaders = new TH2F(hname, hname, 8, 0, 8, 8, 0, 8);
                histosROCs.push_back(h_nROCHeaders);
            }
            TString hname;
            hname.Form("Ch%i", (fedsAndChannels[ifed].second)[ch]);
            h_nROCHeaders = new TH2F(hname + "_nROCHeaders", hname + "_nROCHeaders", 8, 0, 8, 8, 0, 8);
            histosROCs.push_back(h_nROCHeaders);
            chTBMmap2D[(fedsAndChannels[ifed].second)[ch]] = histosROCs;

        } //close loop on channels

        scansROCs[fedsAndChannels[ifed].first] = chTBMmap2D;

    } //close loop on feds

    rootf->cd();

    std::set<pos::PixelChannel> names = theNameTranslation_->getChannels();
    cout << " size " << names.size() << endl;
    PixelRootDirectoryMaker rootDirsModules(names, gDirectory);
    for (std::set<pos::PixelChannel>::iterator PixelChannel_itr = names.begin(), PixelChannel_itr_end = names.end();
         PixelChannel_itr != PixelChannel_itr_end; ++PixelChannel_itr) {

        // creates a module histogram for each channel, so many time the same histo
        //cout << " create histos "
      //   << (*PixelChannel_itr).modulename() << " "
      //     << (*PixelChannel_itr).channelname() << " "
      //     << (*PixelChannel_itr).TBMChannelStringFull() << " "
      //     << endl;

        rootDirsModules.cdDirectory((*PixelChannel_itr));
        std::string moduleName = (*PixelChannel_itr).modulename();
        std::vector<TH2F *> histosROC;
        TString hname(moduleName);
        TH2F *h_nROCHeaders = new TH2F(hname, hname, 8, 0, 8, 8, 0, 8);
        //h_nROCHeaders->SetXTitle("Port 1 phase");
        //h_nROCHeaders->SetYTitle("Port 0 phase");
        histosROC.push_back(h_nROCHeaders);
        ROCsHistoSum[moduleName] = histosROC;
    }

    rootf->cd(0);
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDROCDelayCalibrationBPIX::FillEm(unsigned state, int fedid, int ch, int which, float c, int roc) {
    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    if (event_ == 0)
        return; // is this to skip the first event in each patter?

    const std::string &iname = dacsToScan[0];
    const double ival(tempCalibObject->scanValue(iname, state));
    uint32_t tmp = ival;
    int delay1 = (tmp >> 3) & 0x7;
    int delay2 = tmp & 0x7;
    //cout<<state<<" "<<ch<<" "<<roc<<" "<<iname<<" "<<c<<" "<<delay1<<" "<<delay2<<" ";
    if (forcePort1EqualToPort0)
        delay1 = delay2; //
    //cout<<delay1<<endl;
    scansROCs[fedid][ch][roc]->Fill(delay1, delay2, c);
}
