#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelIanaCalibrationBPIX.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
//#include <toolbox/convertstring.h>

using namespace pos;
using namespace std;

PixelIanaCalibrationBPIX::PixelIanaCalibrationBPIX(const PixelSupervisorConfiguration &tempConfiguration, SOAPCommander *mySOAPCmdr)
    : PixelCalibrationBase(tempConfiguration, *mySOAPCmdr) {
          std::cout << "Greetings from the PixelIanaCalibrationBPIX constructor." << std::endl;
}

void PixelIanaCalibrationBPIX::beginCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
}

bool PixelIanaCalibrationBPIX::execute() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    const vector<PixelROCName> &rocs = tempCalibObject->rocList();
    const bool firstOfPattern = event_ % tempCalibObject->nTriggersPerPattern() == 0;
    const unsigned state = event_ / (tempCalibObject->nTriggersPerPattern());
    reportProgress(0.05);

    //cout<<"IanaCalibration: "<<event_<<" "<<firstOfPattern<<" "<<state<<" "<<tempCalibObject->mode()<<endl;

    // Configure all TBMs and ROCs according to the PixelCalibConfiguration settings, but only when it's time for a new configuration.
    if (firstOfPattern)
        commandToAllFECCrates("CalibRunning");

    // Take care of the special case of Vall
    int modEvent=-1;
    if (tempCalibObject->mode() == "VallBPIX") {
      // switch the readout type
      if ((event_ % 16 == 0)) {

	//cout<<"VallBPIX"<<endl;
	int modEvent = event_ / (tempCalibObject->nTriggersTotal() / 4);
	//cout << "event_=" << event_ << " modEvent=" << modEvent 
	//   <<" "<<tempCalibObject->nTriggersTotal()<<" "<<state<<endl;

	if (modEvent==0) {  //Vdig
	  cout<<"VdigBPIX program readback"<<endl;
	  for (unsigned int i = 0; i < rocs.size(); i++)
            setDAC(rocs[i], pos::k_DACAddress_Readback, 8);
	} else if (modEvent==1) { // Va
	  cout<<"VaBPIX program readback"<<endl;
	  for (unsigned int i = 0; i < rocs.size(); i++)
            setDAC(rocs[i], pos::k_DACAddress_Readback, 9);
	} else if (modEvent==2) { //Vbg
	  cout<<"VbgBPIX program readback"<<endl;
	  for (unsigned int i = 0; i < rocs.size(); i++)
            setDAC(rocs[i], pos::k_DACAddress_Readback, 11);
	} else if (modEvent==3) { // Vana
	  cout<<"VanaBPIX program readback"<<endl;
	  for (unsigned int i = 0; i < rocs.size(); i++)
            setDAC(rocs[i], pos::k_DACAddress_Readback, 10);
	} // end modEvent

      } // end if event

    } else { // normal cases 

      if (tempCalibObject->mode() == "IanaBPIX" && firstOfPattern) {
	cout<<"IanaBPIX program readback"<<endl;
	for (unsigned int i = 0; i < rocs.size(); i++)
	  setDAC(rocs[i], pos::k_DACAddress_Readback, 12);
      }
      
      if (tempCalibObject->mode() == "VdigBPIX" && (event_ % 16 == 0)) {
	cout<<"VdigBPIX program readback"<<endl;
        for (unsigned int i = 0; i < rocs.size(); i++)
            setDAC(rocs[i], pos::k_DACAddress_Readback, 8);
      }

      if (tempCalibObject->mode() == "VaBPIX" && (event_ % 16 == 0)) {
	cout<<"VaBPIX program readback"<<endl;
        for (unsigned int i = 0; i < rocs.size(); i++)
	  setDAC(rocs[i], pos::k_DACAddress_Readback, 9);
      }
      
      if (tempCalibObject->mode() == "VbgBPIX" && (event_ % 16 == 0)) {
	cout<<"VbgBPIX program readback"<<endl;
        for (unsigned int i = 0; i < rocs.size(); i++)
	  setDAC(rocs[i], pos::k_DACAddress_Readback, 11);
      }
      
      if (tempCalibObject->mode() == "VanaBPIX" && (event_ % 16 == 0)) {
	cout<<"VanaBPIX program readback"<<endl;
	for (unsigned int i = 0; i < rocs.size(); i++)
	  setDAC(rocs[i], pos::k_DACAddress_Readback, 10);
      }

    } // end if mode

    sendTTCCalSync();
    usleep(1000);

    // Read out data from each FED.
    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "RetrieveData";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = itoa(state);
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);

    return event_ + 1 < tempCalibObject->nTriggersTotal();
}

void PixelIanaCalibrationBPIX::endCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    assert(event_ == tempCalibObject->nTriggersTotal());

    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "Analyze";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = "0";
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);
}

std::vector<std::string> PixelIanaCalibrationBPIX::calibrated() {
    std::vector<std::string> tmp;
    tmp.push_back("dac");
    return tmp;
}
