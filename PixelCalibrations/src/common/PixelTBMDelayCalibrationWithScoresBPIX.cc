#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelTBMDelayCalibrationWithScoresBPIX.h"

#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

using namespace pos;

PixelTBMDelayCalibrationWithScoresBPIX::PixelTBMDelayCalibrationWithScoresBPIX(const PixelSupervisorConfiguration &tempConfiguration, SOAPCommander *mySOAPCmdr)
: PixelCalibrationBase(tempConfiguration, *mySOAPCmdr), logger_(app_->getApplicationLogger()) {
}

void PixelTBMDelayCalibrationWithScoresBPIX::beginCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    fecTimer_.setName("FEC programming");
    phasefindingTimer_.setName("Phase finding");
    osdTimer_.setName("OSD arming");
    trigTimer_.setName("Triggering");
    fedTimer_.setName("FED soaping");
    allTimer_.setName("Total time");
    allTimer_.start();

    // Check that PixelCalibConfiguration settings make sense.

    checkReadback_ = tempCalibObject->parameterValue("CheckReadback") == "yes";
    phaseFindingNeeded_ = tempCalibObject->containsScan("TBMPLL");

    if (checkReadback_ && tempCalibObject->nTriggersPerPattern() % 256 != 0) {
        LOG4CPLUS_FATAL(logger_, "CheckReadback set, ntriggers should be a multiple of 8 rocs * 32 trigs = 256");
        assert(0);
    }

    if (!tempCalibObject->containsScan("TBMADelay") && !tempCalibObject->containsScan("TBMBDelay") && !tempCalibObject->containsScan("TBMPLL")) {
        LOG4CPLUS_FATAL(logger_, "None of TBMADelay, TBMBDelay, TBMPLLDelay found in scan variable list!");
        assert(0);
    }

    if (checkReadback_) {
        osdTimer_.start();
        Attribute_Vector parametersToFEC_readback(2);
        parametersToFEC_readback[0].name_ = "DACAddress";
        parametersToFEC_readback[0].value_ = std::to_string(k_DACAddress_Readback);
        parametersToFEC_readback[1].name_ = "DACValue";
        parametersToFEC_readback[1].value_ = "1"; // try to readback last pixel row
        commandToAllFECCrates("SetDACsEnMassDDR", parametersToFEC_readback);
        osdTimer_.stop();
    }
}

bool PixelTBMDelayCalibrationWithScoresBPIX::execute() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    const unsigned trigInPattern = event_ % tempCalibObject->nTriggersPerPattern();
    const bool firstOfPattern = trigInPattern == 0;
    const unsigned state = event_ / tempCalibObject->nTriggersPerPattern();
    reportProgress(0.05);

    // Configure all TBMs and ROCs according to the PixelCalibConfiguration settings, but only when it's time for a new configuration.
    if (firstOfPattern) {
        LOG4CPLUS_DEBUG(logger_, LOG4CPLUS_TEXT("New TBM delay state ") << state);
        fecTimer_.start();
        commandToAllFECCrates("CalibRunning"); // calls PixelCalibConfiguration::nextFECState()
        fecTimer_.stop();

        // reset TBM, sleep for safety
        sendTTCTBMReset();
        usleep(1000.);

        if (phaseFindingNeeded_) {
            phasefindingTimer_.start();
            commandToAllFEDCrates("FindPhasesNow");
            phasefindingTimer_.stop();
        }
    }

    if (checkReadback_ && trigInPattern % 32 == 0) {
        osdTimer_.start();
        Attribute_Vector parametersToFED_arm(4);
        parametersToFED_arm[0].name_ = "VMEBaseAddress";
        parametersToFED_arm[0].value_ = "*";
        parametersToFED_arm[1].name_ = "Channel";
        parametersToFED_arm[1].value_ = "*";
        const std::string roc = std::to_string((trigInPattern / 32) % 8);
        parametersToFED_arm[2].name_ = "RocHi";
        parametersToFED_arm[2].value_ = roc;
        parametersToFED_arm[3].name_ = "RocLo";
        parametersToFED_arm[3].value_ = roc;
        commandToAllFEDCrates("ArmOSDFifo", parametersToFED_arm);
        osdTimer_.stop();
    }

    // Send trigger to all TBMs and ROCs.
    trigTimer_.start();
    sendTTCCalSync();
    trigTimer_.stop();

    // Read out data from each FED.
    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "RetrieveData";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = std::to_string(state);
    fedTimer_.start();
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);
    fedTimer_.stop();

    return event_ + 1 < tempCalibObject->nTriggersTotal();
}

void PixelTBMDelayCalibrationWithScoresBPIX::endCalibration() {
    PixelCalibConfiguration *tempCalibObject = dynamic_cast<PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    assert(event_ == tempCalibObject->nTriggersTotal());

    Attribute_Vector parametersToFED(2);
    parametersToFED[0].name_ = "WhatToDo";
    parametersToFED[0].value_ = "Analyze";
    parametersToFED[1].name_ = "StateNum";
    parametersToFED[1].value_ = "0";
    commandToAllFEDCrates("FEDCalibrations", parametersToFED);
    
    fecTimer_.printStats();
    phasefindingTimer_.printStats();
    trigTimer_.printStats();
    if (checkReadback_)
        osdTimer_.printStats();
    fedTimer_.printStats();
    
    allTimer_.stop();
    allTimer_.printStats();
}

std::vector<std::string> PixelTBMDelayCalibrationWithScoresBPIX::calibrated() {
    return std::vector<std::string>(1, "tbm");
}
