#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "PixelCalibrations/include/PixelFEDVCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelIanaAnalysisBPIX.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"
#include "PixelUtilities/PixelFEDDataTools/include/ColRowAddrDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigScopeDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigTransDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelFEDDataTypes.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"

#include "TAxis.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TLine.h"
#include "TTree.h"

#include <algorithm>
#include <iomanip>

namespace {
  const bool print = false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
PixelFEDVCalibrationBPIX::PixelFEDVCalibrationBPIX(
    const PixelFEDSupervisorConfiguration &tempConfiguration,
    const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper), rootf(0) {
                              std::cout << "In PixelFEDVCalibrationBPIX ctor()" << std::endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::initializeFED() {
    // setFEDModeAndControlRegister(0x8, 0x30010);

    cout << " Disable back-end in order to avoid spyFIFO1 filling " << endl;
    for (FEDInterfaceMap::iterator iFED = FEDInterface_.begin();
         iFED != FEDInterface_.end(); ++iFED) {
        iFED->second->disableBE(true);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference
PixelFEDVCalibrationBPIX::beginCalibration(xoap::MessageReference msg) {
    std::cout << "In PixelFEDVCalibrationBPIX::beginCalibration()"
              << std::endl;

    pos::PixelCalibConfiguration *tempCalibObject =
        dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    tempCalibObject->writeASCII(outputDir());

    vector<pos::PixelModuleName>::const_iterator module_name =
        theDetectorConfiguration_->getModuleList().begin();
    for (; module_name != theDetectorConfiguration_->getModuleList().end();
         ++module_name) {
        // First we need to get the DAC settings for the ROCs on this module.
        pos::PixelDACSettings *dacs = 0;
        string modulePath = module_name->modulename();
        PixelConfigInterface::get(dacs, "pixel/dac/" + modulePath, *theGlobalKey_);
        assert(dacs != 0);
        dacsettings_[*module_name] = dacs;
    }

    for (unsigned dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables();
         ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        std::vector<unsigned int> dacvals = tempCalibObject->scanValues(dacname);
        if (dacvals.size() > 1)
            dacsToScan.push_back(dacname);

        for (unsigned int i = 0; i < dacvals.size(); ++i)
            std::cout << " dac value " << i << " is " << dacvals[i] << std::endl;
    }

    string init_str = "unknown";
    if (tempCalibObject->mode() == "VdigBPIX")
        init_str = "VdigBPIX calibration";
    else if (tempCalibObject->mode() == "VaBPIX")
        init_str = "VaBPIX calibration";
    else if (tempCalibObject->mode() == "VbgBPIX")
        init_str = "VbgBPIX calibration";
    else if (tempCalibObject->mode() == "VanaBPIX")
        init_str = "VanaBPIX calibration";
    else if (tempCalibObject->mode() == "VallBPIX")
        init_str = "VallBPIX calibration";

    BookEm("");

    return
        makeSOAPMessageReference("BeginCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference
PixelFEDVCalibrationBPIX::execute(xoap::MessageReference msg) {
    vector<string> parameters_names(2);
    parameters_names.push_back( "WhatToDo");
    parameters_names.push_back( "StateNum");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, parameters_names);

    const unsigned state = atoi(parameters[ "StateNum"].c_str());

    if (parameters[ "WhatToDo"] == "RetrieveData") {
        pos::PixelCalibConfiguration *tempCalibObject =
            dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);

        if (tempCalibObject->mode() == "VallBPIX") { //special Vall case 

            int modEvent = event_ / (tempCalibObject->nTriggersTotal() / 4);
            //cout << "newpattern7 before event event_=" << event_ << " modEvent=" << modEvent <<" "<<tempCalibObject->nTriggersTotal()<<" "<<state<<endl;

            //cout << "newpattern8 before setMode: " << tempCalibObject->mode()<<endl;;
            if (modEvent == 0) {
	      //std::cout << lastDACValue << " "<< event_ << std::endl;
	      tempCalibObject->setMode("VdigBPIX");
	      RetrieveData(state, 4, tempCalibObject);
            } else if (modEvent == 1) {
	      //std::cout << lastDACValue <<" "<< event_ << endl;
	      tempCalibObject->setMode("VaBPIX");
	      RetrieveData(state, 4, tempCalibObject);
            } else if (modEvent == 2) {
	      //std::cout << lastDACValue <<" "<< event_ <<endl;
	      tempCalibObject->setMode("VbgBPIX");
	      RetrieveData(state, 4, tempCalibObject);
            } else if (modEvent == 3) {
	      //std::cout << lastDACValue <<" "<< event_ <<endl;
	      tempCalibObject->setMode("VanaBPIX");
	      RetrieveData(state, 4, tempCalibObject);
            }
            //cout << "newpattern9 after setMode: " << tempCalibObject->mode()
	    //	 <<" "<<event_<<endl;

            if ((event_ % (tempCalibObject->nTriggersTotal() / 4)) == 0) {
	      //cout<<"newpatter10 "<<event_<<" "<<modEvent<<endl;
	      switch (modEvent) {
	      case 0:
		values4.values0 = values;
		break;
	      case 1:
		values4.values1 = values;
		break;
	      case 2:
		values4.values2 = values;
		break;
	      case 3:
		values4.values3 = values;
		break;
	      }
	      values.clear();
	      last_dacs.clear(); // add for Vall, 10/21 d.k.
            }

            tempCalibObject->setMode("VallBPIX");
            //cout << "newpattern12 after rest to vall setMode: " << tempCalibObject->mode()<<endl;

        } else {  // individual readouts

            RetrieveData(state, 1, tempCalibObject);

        } // end if type

    } else if (parameters[ "WhatToDo"] == "Analyze") {
        Analyze();

    } else {
        cout << "ERROR: PixelFEDVCalibrationBPIX::execute() does not understand "
                "the WhatToDo command, "
             << parameters[ "WhatToDo"] << ", sent to it.\n";
        assert(0);
    }

    return makeSOAPMessageReference("FEDCalibrationsDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference
PixelFEDVCalibrationBPIX::endCalibration(xoap::MessageReference msg) {

    std::cout << "In PixelFEDVCalibrationBPIX::endCalibration()" << std::endl;
    return makeSOAPMessageReference("EndCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::RetrieveData(
    unsigned state, int n_of_voltage_types,
    pos::PixelCalibConfiguration *tempCalibObject) {
    // PixelCalibConfiguration* tempCalibObject =
    // dynamic_cast<PixelCalibConfiguration*>(theCalibObject_);
    assert(tempCalibObject != 0);

    // lastDACValue = 0;

    std::map<std::string, unsigned int> currentDACValues;
    for (unsigned dacnum = 0; dacnum < tempCalibObject->numberOfScanVariables();
         ++dacnum) {
        const std::string &dacname = tempCalibObject->scanName(dacnum);
        const unsigned dacvalue =
            tempCalibObject->scanValue(tempCalibObject->scanName(dacnum), state);
        currentDACValues[dacname] = dacvalue;
        //cout << "Vana "<<currentDACValues["Vana"] << "Vdd "<<currentDACValues["Vdd"] << endl;
    }
    //cout<<"newpattern1 "<<tempCalibObject->mode()<<endl;
    if (tempCalibObject->mode() == "VdigBPIX" &&
        currentDACValues["Vdd"] != lastDACValue) {
        event_ = 0;
        lastDACValue = currentDACValues["Vdd"];
        last_dacs.clear();
        //cout << "newpattern4 "<<event_<<" "<<lastDACValue<<endl;
    }
    if (tempCalibObject->mode() == "VaBPIX" &&
        currentDACValues["Vana"] != lastDACValue) {
        event_ = 0;
        lastDACValue = currentDACValues["Vana"];
        last_dacs.clear();
        //cout << "newpattern4";
    }
    if (tempCalibObject->mode() == "VbgBPIX" &&
        currentDACValues["Vana"] != lastDACValue) {
        event_ = 0;
        lastDACValue = currentDACValues["Vana"];
        last_dacs.clear();
        //cout << "newpattern4";
    }
    if (tempCalibObject->mode() == "VanaBPIX" &&
        currentDACValues["Vana"] != lastDACValue) {
        event_ = 0;
        lastDACValue = currentDACValues["Vana"];
        last_dacs.clear();
        //cout << "newpattern4";
    }

    const std::vector<std::pair<unsigned, std::vector<unsigned> > > &fedsAndChannels =
        tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_,
                                             theFEDConfiguration_,
                                             theDetectorConfiguration_);

    for (unsigned ifed = 0; ifed < fedsAndChannels.size(); ++ifed) {

        const unsigned fednumber = fedsAndChannels[ifed].first;
        const unsigned long vmeBaseAddress =
            theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
        // PixelFEDInterface* iFED = FEDInterface_[vmeBaseAddress];
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];

        for (unsigned int ch = 0; ch < fedsAndChannels[ifed].second.size(); ch++) {

            unsigned int channel = (fedsAndChannels[ifed].second)[ch];
            //***
            if (channel % 2 == 1) {
                int fib = (channel - 1) / 2;
                iFED->setFIFO1(fib);
                PixelPh1FEDInterface::digfifo1 d = iFED->readFIFO1(false);
                // printf("ch a: %i ch b: %i n tbm h a: %i b: %i  tbm t a: %i b: %i  roc
                // h a: %i b: %i\n", d.a.ch, d.b.ch, d.a.n_tbm_h, d.b.n_tbm_h,
                // d.a.n_tbm_t, d.b.n_tbm_t, d.a.n_roc_h, d.b.n_roc_h);
                unsigned int chA = d.a.ch;
                unsigned int chB = d.b.ch;

                if ((channel != chA) || ((channel + 1) != chB))
                    continue;

                //**********
                // cout << "ROCs A " << endl;
                for (unsigned int i = 0; i < d.a.rocs.size(); i++) {
                    // cout << i << ": d.a.rocs.roc " << d.a.rocs[i].roc << " d.a.rocs.rb
                    // " << d.a.rocs[i].rb << endl;
                    unsigned int decCh = d.a.rocs[i].ch; // roc.ch
                    if (decCh != channel)
                        continue;
                    uint32_t mk = d.a.rocs[i].roc; // roc.roc
                    uint32_t f8 = d.a.rocs[i].rb;  // roc.rb
		    //cout<<"readA "<<decCh<<" "<<mk<<" "<<f8<<endl;
                    if (mk <= 8) {
                        last_dacs[fedsAndChannels[ifed].first][channel][mk]
                            .push_back(f8);
                    }
                }
                // cout << "ROCs B " << endl;
                for (unsigned int i = 0; i < d.b.rocs.size(); i++) {
                    // cout << i << ": d.b.rocs.roc " << d.b.rocs[i].roc << " d.b.rocs.rb
                    // " << d.b.rocs[i].rb << endl;
                    unsigned int decCh = d.b.rocs[i].ch; // roc.ch
                    if (decCh != (channel + 1))
                        continue;
                    uint32_t mk = d.b.rocs[i].roc; // roc.roc
                    uint32_t f8 = d.b.rocs[i].rb;  // roc.rb
		    //cout<<"readB "<<decCh<<" "<<mk<<" "<<f8<<endl;
                    if (mk <= 8) {
                        last_dacs[fedsAndChannels[ifed].first][channel + 1]
                                 [mk]
                                     .push_back(f8);
                    }
                }

            } // close if channel%2
        }     // close loop on channels

    } // close loop on feds
    //cout<<"newpattern2 "<<event_<<" "<<n_of_voltage_types<<" "<<tempCalibObject->nTriggersPerPattern()
    //	<<tempCalibObject->mode()<<endl;
    if ((event_ % (tempCalibObject->nTriggersPerPattern() / n_of_voltage_types)) ==
        (tempCalibObject->nTriggersPerPattern() / n_of_voltage_types - 1)) {
      //cout <<"newpattern3(1) "<< event_ << endl;
      if (tempCalibObject->mode() == "VdigBPIX")
	ReadVdig();
      else if (tempCalibObject->mode() == "VaBPIX")
	ReadVdig();
      else if (tempCalibObject->mode() == "VbgBPIX")
	ReadVdig();
      else if (tempCalibObject->mode() == "VanaBPIX")
	ReadVdig();
    }

    event_++;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::Analyze() {
    pos::PixelCalibConfiguration *tempCalibObject =
        dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);
    std::string mode = tempCalibObject->mode();

    if (tempCalibObject->mode() == "VdigBPIX")
        VdigAnalysis(tempCalibObject);
    else if (tempCalibObject->mode() == "VaBPIX")
        VaAnalysis(tempCalibObject);
    else if (tempCalibObject->mode() == "VbgBPIX")
        VbgAnalysis(tempCalibObject);
    else if (tempCalibObject->mode() == "VanaBPIX")
        VanaAnalysis(tempCalibObject);
    else if (tempCalibObject->mode() == "VallBPIX")
        VallAnalysis(tempCalibObject);
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::VdigAnalysis(
    pos::PixelCalibConfiguration *tempCalibObject) {

    string cmd = "";

    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("Summary", "Summary");

    branch_sum_vdig_incl_adc theBranch;
    if (tempCalibObject->parameterValue("include_adc") == "yes") {
        tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:ADC/F:rocName/C", 4096000);
    } else {
        tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:rocName/C", 4096000);
    }

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tempCalibObject->rocList(), gDirectory);

    for (std::map<int, std::map<int, std::map<int, std::map<int, int> > > >::iterator
	   it = values.begin(); it != values.end(); ++it) {
      
      for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 =
	     (it->second).begin();
	   it2 != (it->second).end(); ++it2) {
	
	const std::vector<pos::PixelROCName> &rocs =
	  theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
	pos::PixelChannel theChannel =
	  theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);
	
	static std::string directory;
	directory = getenv("PIXELCONFIGURATIONBASE");
	
	std::ifstream in((directory + "/iana/0/ROC_Iana_" +
			  theChannel.modulename() + ".dat").c_str());
	
	for (std::map<int, std::map<int, int> >::iterator it3 =
	       (it2->second).begin();
	     it3 != (it2->second).end(); ++it3) {
	  
	  if (theNameTranslation_->ROCNameFromFEDChannelROCExists(
								  it->first, it2->first, it3->first - 1)) {
	    
	    pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(
										     it->first, it2->first, it3->first - 1);
	    pos::PixelModuleName theModule(theROC.rocname());
	    
	    std::string tmp;
	    std::string tag;
	    float par0vd, par1vd;

	    bool rocFound=false;
	    if(print) std::cout<<"for module "<<theModule.modulename()<<" for roc "<<theROC.rocname(); // d.k.
	    while(!rocFound || (in.good() && !in.eof()) ) { // loop until roc found or end of file 
	      in >> tag; // ROC
	      if(tag != "ROC:") std::cout<<"PixelFEDIanaCalibrationBPIX:IanaAnalysis - iana file corrupted "<<tag<<std::endl;
	      in >> tmp;
	      if( tmp != theROC.rocname() ) { // skip wrong rocs 
		in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;
		in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;  
		continue; 
	      } 
	      if(print) std::cout<<" found roc "<<tmp<<std::endl;
	      rocFound=true;
	      in >> tag;in >> tmp; par0vd = atof(tmp.c_str()); // par0vd
	      in >> tag;in >> tmp; par1vd = atof(tmp.c_str()); //par1vd
	      in>>tag;in>>tmp; // par0va
	      in>>tag;in>>tmp; // par1va
	      in>>tag;in>>tmp; // par0rbia
	      in>>tag;in>>tmp; // par1rbia
	      in>>tag;in>>tmp; // par0tbia
	      in>>tag;in>>tmp; // par1tbia
	      in>>tag;in>>tmp; // par2tbia
	      in>>tag;in>>tmp; // par0ia
	      in>>tag;in>>tmp; // par1ia
	      in>>tag;in>>tmp; // par2ia
	      //std::cout << " par0vd " << par0vd << " par1vd " << par1vd<< std::endl;
	      break;
	    } // end while 

	    strcpy(theBranch.rocName, theROC.rocname().c_str());
	    rootDirs.cdDirectory(theROC);
	    TH1F *hVa = new TH1F(theROC.rocname().c_str(),
				 theROC.rocname().c_str(), 1000, 0, 10);
	    TH1F *hVa_adc = new TH1F((theROC.rocname() + "_ADC").c_str(),
				     theROC.rocname().c_str(), 256, 0, 255);
	    TF1 *froc = new TF1("froc", "[0] + [1]*x", 0, 200);
	    froc->SetParameters(par0vd, par1vd);
	    
	    float aveAdc=0, aveV=0; int count=0;
	    for (std::map<int, int>::iterator it4 = (it3->second).begin();
		 it4 != (it3->second).end(); ++it4) {
	      
	      if (rocids[it->first][it2->first][it3->first][it4->first] ==
		  rocs[it3->first - 1].roc() &&
		  rbreg[it->first][it2->first][it3->first][it4->first] == 8) {
		float v = froc->GetX(it4->second); // convert adc to V
		hVa->Fill(v);
		count++; aveAdc+=it4->second;aveV+=v;
		if (tempCalibObject->parameterValue("include_adc") == "yes") {hVa_adc->Fill(it4->second);}
	      }
	      
	    } // close loop on vdig values
	    
	    delete froc;
	    
	    if(count>0) {
	      aveAdc = aveAdc/float(count); aveV=aveV/float(count);
	      cout<<theROC.rocname()<<" adc "<<aveAdc<<" V "<<aveV<<endl;
	    }
	    
	    hVa->Draw("HIST");
	    hVa->Write();
	    
	    theBranch.Mean = hVa->GetMean();
	    theBranch.RMS = hVa->GetRMS();
	    
	    if (tempCalibObject->parameterValue("include_adc") == "yes") {
	      hVa_adc->Draw("HIST");
	      hVa_adc->Write();
	      theBranch.ADC = hVa_adc->GetMean();
	      //theBranch.Type = 8;
	    }
	    
	    tree->Fill();
	  }
	  
	} // close loop on rocs per fed channel
	in.close();
      } // close loop on fed channels
    }     // close loop on feds
    
    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::VaAnalysis(
    pos::PixelCalibConfiguration *tempCalibObject) {

    string cmd = "";

    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("Summary", "Summary");

    branch_sum_vdig_incl_adc theBranch;
    if (tempCalibObject->parameterValue("include_adc") == "yes") {
        tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:ADC/F:rocName/C", 4096000);
    } else {
        tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:rocName/C", 4096000);
    }

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tempCalibObject->rocList(), gDirectory);

    for (std::map<int, std::map<int, std::map<int, std::map<int, int> > > >::iterator
             it = values.begin();
         it != values.end(); ++it) {
      
      for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 =
	     (it->second).begin();
	   it2 != (it->second).end(); ++it2) {
	
	const std::vector<pos::PixelROCName> &rocs =
	  theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
	pos::PixelChannel theChannel =
	  theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);
	
	static std::string directory;
	directory = getenv("PIXELCONFIGURATIONBASE");
	
	std::ifstream in((directory + "/iana/0/ROC_Iana_" +
			  theChannel.modulename() + ".dat").c_str());
	
	for (std::map<int, std::map<int, int> >::iterator it3 =
	       (it2->second).begin();
	     it3 != (it2->second).end(); ++it3) {
	  
	  if (theNameTranslation_->ROCNameFromFEDChannelROCExists(
								  it->first, it2->first, it3->first - 1)) {
	    
	    pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(
										     it->first, it2->first, it3->first - 1);
	    pos::PixelModuleName theModule(theROC.rocname());
	    
	    std::string tmp;
	    std::string tag;
	    float par0va, par1va;
	    
	    bool rocFound=false;
	    if(print) std::cout<<"for module "<<theModule.modulename()<<" for roc "<<theROC.rocname(); // d.k.
	    while(!rocFound || (in.good() && !in.eof()) ) { // loop until roc found or end of file 
	      in >> tag; // ROC
	      if(tag != "ROC:") std::cout<<"PixelFEDIanaCalibrationBPIX:IanaAnalysis - iana file corrupted "<<tag<<std::endl;
	      in >> tmp;
	      if( tmp != theROC.rocname() ) { // skip wrong rocs 
		in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;
		in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;  
		continue; 
	      } 
	      if(print) std::cout<<" found roc "<<tmp<<std::endl;
	      rocFound=true;
	      in >> tag;in >> tmp; // par0vd
	      in >> tag;in >> tmp; //par1vd
	      in>>tag;in>>tmp; par0va = atof(tmp.c_str()); //par0va
	      in>>tag;in>>tmp; par1va = atof(tmp.c_str()); // par1va
	      in>>tag;in>>tmp; // par0rbia
	      in>>tag;in>>tmp; // par1rbia
	      in>>tag;in>>tmp; // par0tbia
	      in>>tag;in>>tmp; // par1tbia
	      in>>tag;in>>tmp; // par2tbia
	      in>>tag;in>>tmp; // par0ia
	      in>>tag;in>>tmp; // par1ia
	      in>>tag;in>>tmp; // par2ia
	      //std::cout << " par0va " << par0va << " par1va " << par1va<< std::endl;
	      break;
	    } // end while 

	    strcpy(theBranch.rocName, theROC.rocname().c_str());
	    rootDirs.cdDirectory(theROC);
	    TH1F *hVa = new TH1F(theROC.rocname().c_str(),
				 theROC.rocname().c_str(), 1000, 0, 10);
	    TH1F *hVa_adc = new TH1F((theROC.rocname() + "_ADC").c_str(),
				     theROC.rocname().c_str(), 256, 0, 255);
	    TF1 *froc = new TF1("froc", "[0] + [1]*x", 0, 200);
	    froc->SetParameters(par0va, par1va);
	    float aveAdc=0, aveV=0; int count=0;
	    for (std::map<int, int>::iterator it4 = (it3->second).begin();
		 it4 != (it3->second).end(); ++it4) {
	      
	      if (rocids[it->first][it2->first][it3->first][it4->first] ==
		  rocs[it3->first - 1].roc() &&
		  rbreg[it->first][it2->first][it3->first][it4->first] == 9) {
		float v = froc->GetX(it4->second); // convert adc to V
		hVa->Fill(v);
		count++; aveAdc+=it4->second;aveV+=v;
		if (tempCalibObject->parameterValue("include_adc") == "yes") {
		  hVa_adc->Fill(it4->second);
		}
	      }
	      
	    } // close loop on vdig values
	    
	    delete froc;
	    if(count>0) {
	      aveAdc = aveAdc/float(count); aveV=aveV/float(count);
	      cout<<theROC.rocname()<<" adc "<<aveAdc<<" V "<<aveV<<endl;
	    }

	    hVa->Draw("HIST");
	    hVa->Write();
	    
	    theBranch.Mean = hVa->GetMean();
	    theBranch.RMS = hVa->GetRMS();
	    
	    if (tempCalibObject->parameterValue("include_adc") == "yes") {
	      hVa_adc->Draw("HIST");
	      hVa_adc->Write();
	      theBranch.ADC = hVa_adc->GetMean();
	      //theBranch.Type = 9;
	    }
	    
	    tree->Fill();
	  }
	  
	} // close loop on rocs per fed channel
	in.close();
      } // close loop on fed channels
    }     // close loop on feds
    
    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::VbgAnalysis(
    pos::PixelCalibConfiguration *tempCalibObject) {

    string cmd = "";

    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("Summary", "Summary");

    branch_sum_vdig_incl_adc theBranch;
    if (tempCalibObject->parameterValue("include_adc") == "yes") {
      //tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:ADC/F:Type/I:rocName/C", 4096000);
      tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:ADC/F:rocName/C", 4096000);
    } else {
        tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:rocName/C", 4096000);
    }

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tempCalibObject->rocList(), gDirectory);

    for (std::map<int, std::map<int, std::map<int, std::map<int, int> > > >::iterator
	   it = values.begin();
         it != values.end(); ++it) {
      
      for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 =
	     (it->second).begin();
	   it2 != (it->second).end(); ++it2) {
	
	const std::vector<pos::PixelROCName> &rocs =
	  theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
	pos::PixelChannel theChannel =
	  theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);
	
	static std::string directory;
	directory = getenv("PIXELCONFIGURATIONBASE");
	
	std::ifstream in((directory + "/iana/0/ROC_Iana_" +
			  theChannel.modulename() + ".dat").c_str());
	
	for (std::map<int, std::map<int, int> >::iterator it3 =
	       (it2->second).begin();
	     it3 != (it2->second).end(); ++it3) {
	  
	  if (theNameTranslation_->ROCNameFromFEDChannelROCExists(
								  it->first, it2->first, it3->first - 1)) {
	    
	    pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(
										     it->first, it2->first, it3->first - 1);
	    pos::PixelModuleName theModule(theROC.rocname());
	    
	    std::string tmp;
	    std::string tag;
	    float par0vd, par1vd;
	    bool rocFound=false;
	    if(print) std::cout<<"for module "<<theModule.modulename()<<" for roc "<<theROC.rocname(); // d.k.
	    while(!rocFound || (in.good() && !in.eof()) ) { // loop until roc found or end of file 
	      in >> tag; // ROC
	      if(tag != "ROC:") std::cout<<"PixelFEDIanaCalibrationBPIX:IanaAnalysis - iana file corrupted "<<tag<<std::endl;
	      in >> tmp;
	      if( tmp != theROC.rocname() ) { // skip wrong rocs 
		in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;
		in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;  
		continue; 
	      } 
	      if(print) std::cout<<" found roc "<<tmp<<std::endl;
	      rocFound=true;
	      in >> tag;in >> tmp; par0vd = atof(tmp.c_str()); // par0vd
	      in >> tag;in >> tmp; par1vd = atof(tmp.c_str()); //par1vd
	      in>>tag;in>>tmp; // par0va
	      in>>tag;in>>tmp; // par1va
	      in>>tag;in>>tmp; // par0rbia
	      in>>tag;in>>tmp; // par1rbia
	      in>>tag;in>>tmp; // par0tbia
	      in>>tag;in>>tmp; // par1tbia
	      in>>tag;in>>tmp; // par2tbia
	      in>>tag;in>>tmp; // par0ia
	      in>>tag;in>>tmp; // par1ia
	      in>>tag;in>>tmp; // par2ia
	      //std::cout << " par0vd " << par0vd << " par1vd " << par1vd<< std::endl;
	      break;
	    } // end while 

	    strcpy(theBranch.rocName, theROC.rocname().c_str());
	    //cout << "newpattern5Vbg theRoc.rocname().c_str() " << theROC.rocname().c_str() << endl;
	    rootDirs.cdDirectory(theROC);
	    TH1F *hVa = new TH1F(theROC.rocname().c_str(),
				 theROC.rocname().c_str(), 1000, 0, 10);
	    TH1F *hVa_adc = new TH1F((theROC.rocname() + "_ADC").c_str(),
				     theROC.rocname().c_str(), 256, 0, 255);
	    TF1 *froc = new TF1("froc", "[0] + [1]*x", 0, 200);
	    froc->SetParameters(par0vd, par1vd);

	    float aveAdc=0, aveV=0; int count=0;
	    for (std::map<int, int>::iterator it4 = (it3->second).begin();
		 it4 != (it3->second).end(); ++it4) {
	      
	      //cout << "newpattern2 rocids " << rocids[it->first][it2->first][it3->first][it4->first] << " type: " << typeid(rocids[it->first][it2->first][it3->first][it4->first]).name();
	      //cout << "newpattern2 rocs   " << rocs[it3->first - 1].roc() << " type: " << typeid(rocs[it3->first - 1].roc()).name();
	      //cout << "newpattern2 rbreg  " << rbreg[it->first][it2->first][it3->first][it4->first] << " type: " << typeid(rbreg[it->first][it2->first][it3->first][it4->first]).name();
	      //cout << " type of 11: " << typeid(11).name() << endl;
	      if (rocids[it->first][it2->first][it3->first][it4->first] ==
		  rocs[it3->first - 1].roc() &&
		  rbreg[it->first][it2->first][it3->first][it4->first] == 11) {
		
		//cout << "hVa should be filled with: " << froc->GetX(it4->second) << endl;
		float v = (froc->GetX(it4->second))/2.0; // convert adc to V, divide by 2
		hVa->Fill(v);
		count++; aveAdc+=it4->second;aveV+=v;
		if (tempCalibObject->parameterValue("include_adc") == "yes") {hVa_adc->Fill(it4->second);}
	      }
	      
	    } // close loop on vdig values
	    
	    delete froc;
	    if(count>0) {
	      aveAdc = aveAdc/float(count); aveV=aveV/float(count);
	      cout<<theROC.rocname()<<" adc "<<aveAdc<<" V "<<aveV<<endl;
	    }
	    
	    hVa->Draw("HIST");
	    hVa->Write();
	    
	    theBranch.Mean = hVa->GetMean();
	    theBranch.RMS = hVa->GetRMS();
	    
	    if (tempCalibObject->parameterValue("include_adc") == "yes") {
	      hVa_adc->Draw("HIST");
	      hVa_adc->Write();
	      theBranch.ADC = hVa_adc->GetMean();
	      //theBranch.Type = 11;
	    }
	    
	    tree->Fill();
	  }
	  
	} // close loop on rocs per fed channel
	in.close();
      } // close loop on fed channels
    }     // close loop on feds

    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
// NOT FIXED YET
void PixelFEDVCalibrationBPIX::VallAnalysis(
    pos::PixelCalibConfiguration *tempCalibObject) {

    string cmd = "";

    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("Summary", "Summary");

    branch_sum_vdig_incl_type theBranch;
    if (tempCalibObject->parameterValue("include_adc") == "yes") {
        tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:ADC/F:Type/I:rocName/C", 4096000);
    } else {
        tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:rocName/C", 4096000);
    }

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tempCalibObject->rocList(), gDirectory);

    // for (std::map<int,std::map<int,std::map<int,std::map<int,int> > >
    // >::iterator it0 = values.begin(); it0 != values.end(); ++it0) {
    //   std::cout << it0->first << '\n';
    for (int voltage_type = 0; voltage_type < 4; voltage_type++) {
        values.clear();
        switch (voltage_type) {
        case 0:
            values = values4.values0;
            break;
        case 1:
            values = values4.values1;
            break;
        case 2:
            values = values4.values2;
            break;
        case 3:
            values = values4.values3;
            break;
        }
        //cout << "newpattern3_outside for" << endl;
        for (std::map<int,
                      std::map<int, std::map<int, std::map<int, int> > > >::iterator
                 it = values.begin();
             it != values.end(); ++it) {
	  //cout << "newpattern3_inside for" << endl;
            for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 =
                     (it->second).begin();
                 it2 != (it->second).end(); ++it2) {
	      //cout << "newpattern3_inside for2" << endl;
                const std::vector<pos::PixelROCName> &rocs =
                    theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
                pos::PixelChannel theChannel =
                    theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);

                static std::string directory;
                directory = getenv("PIXELCONFIGURATIONBASE");

                std::ifstream in((directory + "/iana/0/ROC_Iana_" +
                                  theChannel.modulename() + ".dat").c_str());

                for (std::map<int, std::map<int, int> >::iterator it3 =
                         (it2->second).begin();
                     it3 != (it2->second).end(); ++it3) {
		  //cout << "newpattern3_inside for3" << endl;

                    if (theNameTranslation_->ROCNameFromFEDChannelROCExists(
                            it->first, it2->first, it3->first - 1)) {
		      //cout << "newpattern3_inside in" << endl;
                        pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(
                            it->first, it2->first, it3->first - 1);
                        pos::PixelModuleName theModule(theROC.rocname());

                        std::string tmp;
                        std::string tag;
                        float par0vd, par1vd, par0va, par1va;
                        in >> tag; // ROC
                        in >> tmp;
                        // std::cout << " ROC: " << tmp;
                        in >> tag; // par0vd
                        in >> tmp;
                        par0vd = atof(tmp.c_str()); //stof(tmp);
                        in >> tag;                  // par1vd
                        in >> tmp;
                        par1vd = atof(tmp.c_str()); //stof(tmp);
                        in >> tag;                  // par0va
                        in >> tmp;
                        par0va = atof(tmp.c_str()); //stof(tmp);
                        in >> tag;                  // par1va
                        in >> tmp;
                        par1va = atof(tmp.c_str()); //stof(tmp);
                        in >> tag;                  // par0rbia
                        in >> tmp;
                        in >> tag; // par1rbia
                        in >> tmp;
                        in >> tag; // par0tbia
                        in >> tmp;
                        in >> tag; // par1tbia
                        in >> tmp;
                        in >> tag; // par2tbia
                        in >> tmp;
                        in >> tag; // par0ia
                        in >> tmp;
                        in >> tag; // par1ia
                        in >> tmp;
                        in >> tag; // par2ia
                        in >> tmp;
                        // std::cout << " par0va " << par0va << " par1va " << par1va
                        //           << std::endl;

                        strcpy(theBranch.rocName, theROC.rocname().c_str());
                        rootDirs.cdDirectory(theROC);
                        //cout << "newpattern5 theRoc.rocname().c_str() " << theROC.rocname().c_str() << endl;
                        std::string voltage_formula;
                        std::string voltage_name;
                        switch (voltage_type) {
                        case 0: // Vdig
                            voltage_formula = "[0] + [1]*x";
                            voltage_name = "_Vdig";
                            break;
                        case 1: // Va
                            voltage_formula = "[2] + [3]*x";
                            voltage_name = "_Va";
                            break;
                        case 2: // Vbg
                            voltage_formula = "([0] + [1]*x) * 2";
                            voltage_name = "_Vbg";
                            break;
                        case 3: // Vana
                            voltage_formula = "[2] + [3]*x";
                            voltage_name = "_Vana";
                            break;
                        }

                        TH1F *hVa = new TH1F((theROC.rocname() + voltage_name).c_str(),
                                             theROC.rocname().c_str(), 1000, 0, 10);
                        TH1F *hVa_adc =
                            new TH1F((theROC.rocname() + voltage_name + "_ADC").c_str(),
                                     theROC.rocname().c_str(), 256, 0, 255);
                        TF1 *froc = new TF1("froc", voltage_formula.c_str(), 0, 200);
                        froc->SetParameters(par0vd, par1vd, par0va, par1va);
                        for (std::map<int, int>::iterator it4 = (it3->second).begin();
                             it4 != (it3->second).end(); ++it4) {

                            if (rocids[it->first][it2->first][it3->first][it4->first] ==
                                rocs[it3->first - 1].roc()) {
                                //rbreg[it->first][it2->first][it3->first][it4->first] == 11) {

                                hVa->Fill(froc->GetX(it4->second));
                                if (tempCalibObject->parameterValue("include_adc") == "yes") {
                                    hVa_adc->Fill(it4->second);
                                }
                            }

                        } // close loop on vdig values

                        delete froc;

                        hVa->Draw("HIST");
                        hVa->Write();

                        theBranch.Mean = hVa->GetMean();
                        theBranch.RMS = hVa->GetRMS();

                        if (tempCalibObject->parameterValue("include_adc") == "yes") {
                            hVa_adc->Draw("HIST");
                            hVa_adc->Write();
                            theBranch.ADC = hVa_adc->GetMean();
			    theBranch.Type = voltage_type;
                        }

                        tree->Fill();
                    }

                } // close loop on rocs per fed channel
                in.close();
            } // close loop on fed channels
        }     // close loop on feds
    }

    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::VanaAnalysis(
    pos::PixelCalibConfiguration *tempCalibObject) {

    string cmd = "";

    TDirectory *dirSummaries = gDirectory->mkdir("SummaryTrees", "SummaryTrees");
    dirSummaries->cd();

    TTree *tree = new TTree("Summary", "Summary");

    branch_sum_vdig_incl_adc theBranch;
    if (tempCalibObject->parameterValue("include_adc") == "yes") {
        tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:ADC/F:rocName/C", 4096000);
    } else {
        tree->Branch("Summary", &theBranch, "Mean/F:RMS/F:rocName/C", 4096000);
    }

    rootf->cd();

    PixelRootDirectoryMaker rootDirs(tempCalibObject->rocList(), gDirectory);

    for (std::map<int, std::map<int, std::map<int, std::map<int, int> > > >::iterator
	   it = values.begin();
         it != values.end(); ++it) {
      
      for (std::map<int, std::map<int, std::map<int, int> > >::iterator it2 =
	     (it->second).begin();
	   it2 != (it->second).end(); ++it2) {
	
	const std::vector<pos::PixelROCName> &rocs =
	  theNameTranslation_->getROCsFromFEDChannel(it->first, it2->first);
	pos::PixelChannel theChannel =
	  theNameTranslation_->ChannelFromFEDChannel(it->first, it2->first);
	
	static std::string directory;
	directory = getenv("PIXELCONFIGURATIONBASE");
	
	std::ifstream in((directory + "/iana/0/ROC_Iana_" +
			  theChannel.modulename() + ".dat").c_str());
	
	for (std::map<int, std::map<int, int> >::iterator it3 =
	       (it2->second).begin();
	     it3 != (it2->second).end(); ++it3) {
	  
	  if (theNameTranslation_->ROCNameFromFEDChannelROCExists(
								  it->first, it2->first, it3->first - 1)) {
	    
	    pos::PixelROCName theROC = theNameTranslation_->ROCNameFromFEDChannelROC(
										     it->first, it2->first, it3->first - 1);
	    pos::PixelModuleName theModule(theROC.rocname());
	    
	    std::string tmp;
	    std::string tag;
	    float par0va, par1va;
	    bool rocFound=false;
	    if(print) std::cout<<"for module "<<theModule.modulename()<<" for roc "<<theROC.rocname(); // d.k.
	    while(!rocFound || (in.good() && !in.eof()) ) { // loop until roc found or end of file 
	      in >> tag; // ROC
	      if(tag != "ROC:") std::cout<<"PixelFEDIanaCalibrationBPIX:IanaAnalysis - iana file corrupted "<<tag<<std::endl;
	      in >> tmp;
	      if( tmp != theROC.rocname() ) { // skip wrong rocs 
		in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;
		in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;in>>tag;in>>tmp;  
		continue; 
	      } 
	      if(print) std::cout<<" found roc "<<tmp<<std::endl;
	      rocFound=true;
	      in >> tag;in >> tmp; // par0vd
	      in >> tag;in >> tmp; //par1vd
	      in>>tag;in>>tmp; par0va = atof(tmp.c_str()); //par0va
	      in>>tag;in>>tmp; par1va = atof(tmp.c_str()); // par1va
	      in>>tag;in>>tmp; // par0rbia
	      in>>tag;in>>tmp; // par1rbia
	      in>>tag;in>>tmp; // par0tbia
	      in>>tag;in>>tmp; // par1tbia
	      in>>tag;in>>tmp; // par2tbia
	      in>>tag;in>>tmp; // par0ia
	      in>>tag;in>>tmp; // par1ia
	      in>>tag;in>>tmp; // par2ia
	      //std::cout << " par0va " << par0va << " par1va " << par1va<< std::endl;
	      break;
	    } // end while 

	    strcpy(theBranch.rocName, theROC.rocname().c_str());
	    rootDirs.cdDirectory(theROC);
	    TH1F *hVa = new TH1F(theROC.rocname().c_str(),
				 theROC.rocname().c_str(), 1000, 0, 10);
	    TH1F *hVa_adc = new TH1F((theROC.rocname() + "_ADC").c_str(),
				     theROC.rocname().c_str(), 256, 0, 255);
	    TF1 *froc = new TF1("froc", "[0] + [1]*x", 0, 200);
	    froc->SetParameters(par0va, par1va);
	    float aveAdc=0, aveV=0; int count=0;
	    for (std::map<int, int>::iterator it4 = (it3->second).begin();
		 it4 != (it3->second).end(); ++it4) {
	      
	      if (rocids[it->first][it2->first][it3->first][it4->first] ==
		  rocs[it3->first - 1].roc() &&
		  rbreg[it->first][it2->first][it3->first][it4->first] == 10) {
		
		float v = (froc->GetX(it4->second))/2.0; // convert adc to V, divide by 2
		hVa->Fill(v);
		count++; aveAdc+=it4->second;aveV+=v;
		if (tempCalibObject->parameterValue("include_adc") == "yes") {hVa_adc->Fill(it4->second);}
	      }
	      
	    } // close loop on vdig values
	    
	    delete froc;
	    if(count>0) {
	      aveAdc = aveAdc/float(count); aveV=aveV/float(count);
	      cout<<theROC.rocname()<<" adc "<<aveAdc<<" V "<<aveV<<endl;
	    }
	    
	    hVa->Draw("HIST");
	    hVa->Write();
	    
	    theBranch.Mean = hVa->GetMean();
	    theBranch.RMS = hVa->GetRMS();
	    
	    if (tempCalibObject->parameterValue("include_adc") == "yes") {
	      hVa_adc->Draw("HIST");
	      hVa_adc->Write();
	      theBranch.ADC = hVa_adc->GetMean();
	      //theBranch.Type = 10;
	    }
	    
	    tree->Fill();
	  }
	  
	} // close loop on rocs per fed channel
	in.close();
      } // close loop on fed channels
    }     // close loop on feds
    
    CloseRootf();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::CloseRootf() {
    if (rootf) {
        rootf->Write();
        rootf->Close();
        delete rootf;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::BookEm(const TString &path) {

    values.clear();
    //?

    pos::PixelCalibConfiguration *tempCalibObject =
        dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    TString root_fn;
    if (tempCalibObject->mode() == "VdigBPIX")
        // root_fn.Form("%s/Vd.root", outputDir().c_str());
        root_fn.Form("%s/Vd_%lu.root", outputDir().c_str(), crate_);
    else if (tempCalibObject->mode() == "VaBPIX")
        // root_fn.Form("%s/Vd.root", outputDir().c_str());
        root_fn.Form("%s/Va_%lu.root", outputDir().c_str(), crate_);
    else if (tempCalibObject->mode() == "VbgBPIX")
        // root_fn.Form("%s/Vd.root", outputDir().c_str());
        root_fn.Form("%s/Vbg_%lu.root", outputDir().c_str(), crate_);
    else if (tempCalibObject->mode() == "VanaBPIX")
        // root_fn.Form("%s/Vd.root", outputDir().c_str());
        root_fn.Form("%s/Vana_%lu.root", outputDir().c_str(), crate_);
    else if (tempCalibObject->mode() == "VallBPIX")
        // root_fn.Form("%s/Vd.root", outputDir().c_str());
        root_fn.Form("%s/Vall_%lu.root", outputDir().c_str(), crate_);

    cout << "writing histograms to file " << root_fn << endl;
    CloseRootf();
    rootf = new TFile(root_fn, "create");
    assert(rootf->IsOpen());
    rootf->cd(0);
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDVCalibrationBPIX::ReadVdig(void) {  
  //cout << "Read " << lastDACValue << std::endl;
  int type=0;
  pos::PixelCalibConfiguration *tempCalibObject =
    dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
  if (tempCalibObject->mode() == "VdigBPIX")     type=8;
  else if (tempCalibObject->mode() == "VaBPIX")  type=9;
  else if (tempCalibObject->mode() == "VbgBPIX") type=11;
  else if (tempCalibObject->mode() == "VanaBPIX") type=10;
  else if (tempCalibObject->mode() == "IanaBPIX") type=12;

  for (std::map<int, std::map<int, std::map<int, std::vector<int> > > >::iterator
	 it = last_dacs.begin();
       it != last_dacs.end(); ++it) {
    
    for (std::map<int, std::map<int, std::vector<int> > >::iterator it2 =
	   (it->second).begin();
	 it2 != (it->second).end(); ++it2) {
      
      for (std::map<int, std::vector<int> >::iterator it3 =
	     (it2->second).begin();
	   it3 != (it2->second).end(); ++it3) {
	
	std::vector<int> start;
	start.push_back(-1);
	std::vector<int> stop;
	stop.push_back(-1);
	
	int ii = 0;
	for (unsigned int i = 0; i < (it3->second).size(); ++i) {
	  
	  if ((((it3->second)[i]) & (0x2)) == 2 && start[ii] == -1) {
	    start[ii] = i;
	  } else if ((((it3->second)[i]) & (0x2)) == 2 && start[ii] != -1) {
	    stop[ii] = i;
	    start.push_back(i);
	    stop.push_back(i);
	    ii++;
	  }
	}
	
	start.pop_back();
	stop.pop_back();
	//std::cout << " - channel#" << it2->first << " ROC#" << it3->first <<std::endl; 
	//std::cout << "*************** start size " << start.size()
	//<< " stop size " << stop.size() << std::endl;
	for (unsigned int i = 0; i < start.size(); ++i) {
	  
	  // std::cout << "    (found start =  " << start[i] << " and stop = "
	  // << stop[i] << " ) " << std::endl;
	  int rbvalue = -1;
	  int rocid = -1;
	  int rbregvalue = -1;
	  std::vector<int> bits;
	  
	  if (start[i] != stop[i] && (stop[i] - start[i]) > 13) {
	    
	    // now put together the 16 bits
	    for (int b = start[i] + 1; b <= stop[i]; ++b) {
	      bits.push_back((it3->second)[b] & 0x1);
	    }
	    
	    // now get Iana
	    // for( unsigned int b=0; b<bits.size(); ++b ){ std::cout <<
	    // bits[b];}  std::cout << std::endl;
	    rbvalue = 0;
	    for (unsigned int b = 8; b < bits.size(); ++b)
	      rbvalue += (bits[b] << (15 - b)); // std::cout << bits[b];
	    rocid = 0;
	    for (unsigned int b = 0; b < 4; ++b)
	      rocid += (bits[b] << (3 - b));
	    rbregvalue = 0;
	    for (unsigned int b = 4; b < 8; ++b)
	      rbregvalue += (bits[b] << (7 - b));
	  }
	  
	  if(print) {
	    cout<<"For "<< type;
	    cout<<" fed# "<<it->first<<" channel# "<<it2->first<<" ROC# "
		<<it3->first; 
	    cout<<" V: " << rbvalue<< " rocid "<<rocid << " rbregvalue "
		<< rbregvalue << std::endl;
	  }
	  if(type != rbregvalue) 
	    cout<<"readback "<<type<<" type wrong for "
		<<" fed# "<<it->first<<" channel# "<<it2->first<<" ROC# "<<it3->first 
		<<" V: " << rbvalue<< " rocid "<<rocid << " rbregvalue "
		<< rbregvalue <<endl;
	  // could also use the rocid to test, woyld need access to right roc number 

	  values[it->first][it2->first][it3->first][i] = rbvalue;
	  rocids[it->first][it2->first][it3->first][i] = rocid;
	  rbreg[it->first][it2->first][it3->first][i] = rbregvalue;	  
	  
	} // close loop on starts and stops
      }
    }
  } // close loop
}
