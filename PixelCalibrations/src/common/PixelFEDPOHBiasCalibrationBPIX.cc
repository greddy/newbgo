#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
//#include "CalibFormats/SiPixelObjects/interface/PixelDACNames.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortCardSettingNames.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortcardMap.h"
#include "PixelCalibrations/include/PixelFEDPOHBiasCalibrationBPIX.h"
#include "PixelCalibrations/include/PixelPOHBiasCalibration.h"
#include "PixelCalibrations/include/PixelCalibrationBase.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelFEDDataTypes.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ColRowAddrDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigScopeDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/DigTransDecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
//#include "PixelUtilities/PixelFEDDataTools/include/DigFIFO1Decoder.h"
#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"

#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TTree.h"
#include <iomanip>
#include <algorithm>
#include "PixelCalibrations/include/PixelPOHBiasCalibrationParametersBPIX.h"

#include "pixel/utils/Utils.h"


using namespace PixelPOHBiasCalibrationParametersBPIX;

///////////////////////////////////////////////////////////////////////////////////////////////
PixelFEDPOHBiasCalibrationBPIX::PixelFEDPOHBiasCalibrationBPIX(const PixelFEDSupervisorConfiguration &tempConfiguration, const pixel::utils::SOAPER *soaper)
    : PixelFEDCalibrationBase(tempConfiguration, *soaper) {
          std::cout << "In PixelFEDPOHBiasCalibrationBPIX copy ctor()" << std::endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PixelFEDPOHBiasCalibrationBPIX::initializeFED() {
    //setFEDModeAndControlRegister(0x8, 0x30010);

    cout << " Disable back-end in order to avoid spyFIFO1 filling " << endl;
    for (FEDInterfaceMap::iterator iFED = FEDInterface_.begin(); iFED != FEDInterface_.end();
         ++iFED) {
        iFED->second->disableBE(true);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDPOHBiasCalibrationBPIX::beginCalibration(xoap::MessageReference msg) {
    std::cout << "In PixelFEDPOHBiasCalibrationBPIX::beginCalibration()" << std::endl;

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    assert(tempCalibObject != 0);

    //  tempCalibObject->writeASCII(outputDir());
    DumpFIFOs = tempCalibObject->parameterValue("DumpFIFOs") == "yes";

    std::cout << "DumpFIFOs = " << DumpFIFOs << std::endl;

    //  setFIFO1Mode();//jen

    //  BookEm("");

    return makeSOAPMessageReference("BeginCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDPOHBiasCalibrationBPIX::execute(xoap::MessageReference msg) {
    vector<string> parameters_names(5);
    parameters_names.push_back( "WhatToDo");
    parameters_names.push_back( "AOHBias");
    parameters_names.push_back( "AOHGain");
    parameters_names.push_back( "fednumber");
    parameters_names.push_back( "channel");
    Attributes parameters = getSomeSOAPCommandAttributes(msg, parameters_names);

    const unsigned AOHBias = atoi(parameters[ "AOHBias"].c_str());
    const unsigned AOHGain = atoi(parameters[ "AOHGain"].c_str());
    const unsigned fednumber = atoi(parameters[ "fednumber"].c_str());
    const unsigned channel = atoi(parameters[ "channel"].c_str());

    xoap::MessageReference reply = makeSOAPMessageReference("FEDCalibrationsDone");

    if (parameters[ "WhatToDo"] == "RetrieveData")
        reply = RetrieveData(AOHBias, AOHGain, fednumber, channel);
    else {
        cout << "ERROR: PixelFEDPOHBiasCalibrationBPIX::execute() does not understand the WhatToDo command, " << parameters[ "WhatToDo"] << ", sent to it.\n";
        assert(0);
    }

    return reply;
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDPOHBiasCalibrationBPIX::endCalibration(xoap::MessageReference msg) {

    std::cout << "In PixelFEDPOHBiasCalibrationBPIX::endCalibration()" << std::endl;
    return makeSOAPMessageReference("EndCalibrationDone");
}

///////////////////////////////////////////////////////////////////////////////////////////////
xoap::MessageReference PixelFEDPOHBiasCalibrationBPIX::RetrieveData(unsigned AOHBias, unsigned AOHGain, unsigned int fednumber, unsigned int channel) {

    //  std::cout << "Enter retrieve data @ event = " << event_ << std::endl;
    //  std::cout << "AOHBias = " << AOHBias << ", AOHGain = " << AOHGain << " fednumber = " << fednumber << ", channel = " << channel << std::endl;

    //  PixelCalibConfiguration* tempCalibObject = dynamic_cast <PixelCalibConfiguration*> (theCalibObject_);
    //  assert(tempCalibObject!=0);

    const unsigned long vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
    PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddress];

    bool found_TBMA = false;

    unsigned int _channel = channel;
    if (channel % 2 == 0)
        _channel = channel - 1;

    //  if (channel%2==1) {
    int fib = (_channel - 1) / 2;
    if (DumpFIFOs)
        cout << "Reading fiber " << fib << endl;
    iFED->setFIFO1(fib);

    PixelPh1FEDInterface::digfifo1 d = iFED->readFIFO1(false);
    if (DumpFIFOs)
        printf("ch a: %i ch b: %i n tbm h a: %i b: %i  tbm t a: %i b: %i  roc h a: %i b: %i\n", d.a.ch, d.b.ch, d.a.n_tbm_h, d.b.n_tbm_h, d.a.n_tbm_t, d.b.n_tbm_t, d.a.n_roc_h, d.b.n_roc_h);
    unsigned int chA = d.a.ch;
    unsigned int chB = d.b.ch;

    if ((_channel == chA) && ((_channel + 1) == chB)) {
        found_TBMA = d.a.n_tbm_h && d.a.n_tbm_t && d.b.n_tbm_h && d.b.n_tbm_t;
    }
    //bool ch_foundHit = ((d.a.n_roc_h ) && ())
    //  }

    //  PixelFEDInterface* iFED = FEDInterface_[vmeBaseAddress];
    //
    //  uint32_t bufferFifo1[1024];
    //  int statusFifo1 = iFED->drainFifo1(channel, bufferFifo1, 1024);
    //
    //  bool found_TBMA = false;
    //
    //  if (statusFifo1 > 0) {
    //
    //    DigFIFO1Decoder theFIFO1Decoder(bufferFifo1,statusFifo1);
    //
    //    if( (int)theFIFO1Decoder.globalChannel() == (int)channel ){
    //      found_TBMA = theFIFO1Decoder.foundTBM();
    //
    //      if( DumpFIFOs ){
    //	std::cout << "-----------------------------------" << std::endl;
    //	std::cout << "Contents of FIFO 1 for channel " << channel << " (status = " << statusFifo1 << ")" << std::endl;
    //	std::cout << "-----------------------------------" << std::endl;
    //	theFIFO1Decoder.printToStream(std::cout);
    //      }
    //
    //    }
    //  }

    event_++;
    //  sendResets();

    Attributes returnValues;
    returnValues[ "foundTBMA"] = pixel::utils::to_string((int)found_TBMA);

    return makeSOAPMessageReference("FEDCalibrationsDone", returnValues);
}
