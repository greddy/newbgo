#ifndef _PixelFEDVCalibrationBPIX_h_
#define _PixelFEDVCalibrationBPIX_h_

#include "CalibFormats/SiPixelObjects/interface/PixelROCName.h"
#include "PixelCalibrations/include/PixelFEDCalibrationBase.h"
#include "PixelUtilities/PixelFEDDataTools/include/Moments.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelScanRecord.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"

#include <fstream>

#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>

//class TFile;
//class TH1F;
//class TH2F;
//class TH3F;

class PixelFEDVCalibrationBPIX : public PixelFEDCalibrationBase {
  public:
    PixelFEDVCalibrationBPIX(const PixelFEDSupervisorConfiguration &, const pixel::utils::SOAPER *);

    virtual void initializeFED();
    virtual xoap::MessageReference beginCalibration(xoap::MessageReference msg);
    virtual xoap::MessageReference execute(xoap::MessageReference msg);
    virtual xoap::MessageReference endCalibration(xoap::MessageReference msg);

  private:
    void RetrieveData(unsigned int state, int voltage_type, pos::PixelCalibConfiguration *tempCalibObject);
    void Analyze();
    void VdigAnalysis(pos::PixelCalibConfiguration *tmpCalib);
    void VaAnalysis(pos::PixelCalibConfiguration *tmpCalib);
    void VbgAnalysis(pos::PixelCalibConfiguration *tmpCalib);
    void VallAnalysis(pos::PixelCalibConfiguration *tmpCalib);
    void VanaAnalysis(pos::PixelCalibConfiguration *tmpCalib);
    void CloseRootf();
    void BookEm(const TString &path);
    void ReadVdig(void);

    struct branch {
        float pass;
        float efficiency;
        char rocName[38];
    };

    struct branch_sum {
        float deltaVana;
        float newVana;
        float newIana;
        //float maxIana;
        float fitChisquare;
        float oldVana;
        float oldIana;
        float pass;
        char rocName[38];
    };

    struct branch_sum_vdig {
        float Mean;
        float RMS;
        char rocName[38];
    };

    struct branch_sum_vdig_incl_adc {
        float Mean;
        float RMS;
        float ADC;
        char rocName[38];
    };
    struct branch_sum_vdig_incl_type {
        float Mean;
        float RMS;
        float ADC;
        int Type;
        char rocName[38];
    };

    std::map<pos::PixelModuleName, pos::PixelDACSettings *> dacsettings_;
    TFile *rootf;
    unsigned int lastDACValue;
    std::vector<std::string> dacsToScan;
    std::map<int, std::map<int, std::map<int, std::map<int, int> > > > values;

    struct values_of_different_types {
        std::map<int, std::map<int, std::map<int, std::map<int, int> > > > values0;
        std::map<int, std::map<int, std::map<int, std::map<int, int> > > > values1;
        std::map<int, std::map<int, std::map<int, std::map<int, int> > > > values2;
        std::map<int, std::map<int, std::map<int, std::map<int, int> > > > values3;
    };

    values_of_different_types values4;

    // std::map<int,std::map<int,std::map<int,std::vector<int> > > > last_dacs;
    std::map<int, std::map<int, std::map<int, std::vector<int> > > > last_dacs;

    std::map<int, std::map<int, std::map<int, std::map<int, int> > > > rocids;
    std::map<int, std::map<int, std::map<int, std::map<int, int> > > > rbreg;
};

#endif
