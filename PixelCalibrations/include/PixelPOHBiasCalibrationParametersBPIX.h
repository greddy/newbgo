/*************************************************************************
 * Component of XDAQ Application for Pixel Online Software               *
 * Copyright (C) 2007, Cornell University	                         *
 * All rights reserved.                                                  *
 * Authors: Souvik Das and Anders Ryd                                    *
 *************************************************************************/

#ifndef _PixelPOHBiasCalibrationParametersBPIX_h_
#define _PixelPOHBiasCalibrationParametersBPIX_h_

namespace PixelPOHBiasCalibrationParametersBPIX {

// Parameter defaults
const unsigned int k_ScanMin_default = 20;
const unsigned int k_ScanMax_default = 25;
const unsigned int k_ScanStepSize_default = 1;
const unsigned int k_GainMin_default = 1;
const unsigned int k_GainMax_default = 3;
const unsigned int k_GainStepSize_default = 1;
const unsigned int k_nTriggersPerPOHBias_default = 8;
const float k_AllowPlateau_default = 0.95;
const unsigned int k_MinimumPOH_default = 20;
};

#endif
