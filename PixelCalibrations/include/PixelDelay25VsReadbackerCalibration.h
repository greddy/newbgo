#ifndef _PixelDelay25VsReadbackerCalibration_h_
#define _PixelDelay25VsReadbackerCalibration_h_

#include "PixelCalibrations/include/PixelCalibrationBase.h"
// #include "PixelUtilities/PixelRootUtilities/include/PixelElogMaker.h"
// #include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"
#include "toolbox/Event.h"
#include "toolbox/exception/Handler.h"

// #include "CalibFormats/SiPixelObjects/interface/PixelChannel.h"
#include "CalibFormats/SiPixelObjects/interface/PixelROCName.h"
// #include "PixelUtilities/PixelFEDDataTools/include/Moments.h"

#include <fstream>
#include <boost/thread.hpp>

#include<TString.h>

class PixelDelay25VsReadbackerCalibration : public PixelCalibrationBase {
  public:
    PixelDelay25VsReadbackerCalibration(const PixelSupervisorConfiguration&, SOAPCommander* mySOAPCmdr);

    void beginCalibration();
    virtual bool execute();
    void endCalibration();
    
    virtual std::vector<std::string> calibrated() { return std::vector<std::string>(); }

    private : 
    
    void armOSDFifo(int roc);
    std::map<pos::PixelROCName, std::vector<unsigned> > getRocReadbacks(const std::vector<pos::PixelROCName> &rocs, const unsigned Readback_settings[]);
    std::map<std::pair<unsigned, unsigned>, std::vector<uint32_t> > getResultsByFEDs();
    void setDACDDR(unsigned int dacAddress, unsigned int dacValue);
    unsigned int getTBMPLLsetting(unsigned int setting_160, unsigned int setting_400);
    //std::string SendSoapBlock(Supervisors &, std::string, std::string, Attribute_Vector &);
    //void SendSoapParallelParam(Supervisors::iterator &, std::string &, std::string &, boost::promise<bool> &, boost::promise<std::string> &, Attribute_Vector &);
    
    const unsigned Readback_settings[5];
    const char* Readback_names[5];
    std::ofstream out;
    
    bool writeElog;
    TString outtext;

    unsigned int SDAMin;
    unsigned int SDAMax;
    unsigned int SDAStepSize;
    unsigned int nTriggersPerPoint;
    typedef std::map<std::pair<unsigned, unsigned>, bool> feds_needed_t;
    feds_needed_t feds_needed;

    std::map<pos::PixelROCName, std::string> roc_codes;

    bool scanTrig;
    bool scan_TBMPLL;

};

#endif
