#ifndef _PixelFEDTBMDelayCalibrationWithScoresBPIX_h_
#define _PixelFEDTBMDelayCalibrationWithScoresBPIX_h_

#include "CalibFormats/SiPixelObjects/interface/PixelROCName.h"
#include "PixelCalibrations/include/PixelFEDCalibrationBase.h"
#include "PixelUtilities/PixelTestStandUtilities/include/PixelTimer.h"
#include "TString.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include <fstream>

class PixelFEDTBMDelayCalibrationWithScoresBPIX : public PixelFEDCalibrationBase {
  public:
    PixelFEDTBMDelayCalibrationWithScoresBPIX(const PixelFEDSupervisorConfiguration &, const pixel::utils::SOAPER *);

    virtual void initializeFED();
    virtual xoap::MessageReference beginCalibration(xoap::MessageReference msg);
    virtual xoap::MessageReference execute(xoap::MessageReference msg);
    virtual xoap::MessageReference endCalibration(xoap::MessageReference msg);

    pos::PixelCalibConfiguration *getCalibObject() {
        pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
        assert(tempCalibObject != 0);
        return tempCalibObject;
    }
    
    const std::vector<std::pair<uint32_t, std::vector<uint32_t> > > &getFedsAndChannels() {
        return getCalibObject()->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);
    }

  private:
    void RetrieveData(uint32_t state);
    void Analyze();
    
    bool headersOnly_;
    bool ignoreWordCount_;
    bool orChannels_;
    bool checkReadback_;

    uint32_t theCol_, theRow_;

    bool dumps_;
    bool forcePort1EqualToPort0_;
    bool keepTTBits_;

    std::vector<std::string> dacsToScan_;
    TFile *rootf_;

    std::map<uint32_t, std::map<uint32_t, TH2F*> > scanResults_;
    std::map<std::string, TH2F*> TBMsHistoSum_;
    TH2F* nTrigsPerPoint_;

    void BookEm();
    void GetCurrentScanValues(uint32_t& tbm160delay, uint32_t& rocdelay_port0, uint32_t& rocdelay_port1, uint32_t& ttBits, uint32_t state);
    void FillTrigger(uint32_t state);
    void FillEm(uint32_t state, float c, uint32_t fed, uint32_t ch);

    PixelTimer readTimer_, osdTimer_, otherTimer_;

    uint32_t nCalls_;
    
    log4cplus::Logger &logger_;
};

#endif
