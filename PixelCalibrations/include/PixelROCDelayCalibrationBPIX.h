#ifndef _PixelROCDelayCalibrationBPIX_h_
#define _PixelROCDelayCalibrationBPIX_h_

#include "PixelCalibrations/include/PixelCalibrationBase.h"

class PixelROCDelayCalibrationBPIX : public PixelCalibrationBase {
  public:
    PixelROCDelayCalibrationBPIX(const PixelSupervisorConfiguration &, SOAPCommander *);

    void beginCalibration();
    virtual bool execute();
    void endCalibration();
    virtual std::vector<std::string> calibrated();

    bool ToggleChannels;
    bool CycleScopeChannels;
    bool DelayBeforeFirstTrigger;
    bool DelayEveryTrigger;
};

#endif
