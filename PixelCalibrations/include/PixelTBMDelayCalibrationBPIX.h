#ifndef _PixelTBMDelayCalibrationBPIX_h_
#define _PixelTBMDelayCalibrationBPIX_h_

#include "PixelCalibrations/include/PixelCalibrationBase.h"

class PixelTBMDelayCalibrationBPIX : public PixelCalibrationBase {
  public:
    PixelTBMDelayCalibrationBPIX(const PixelSupervisorConfiguration &, SOAPCommander *);

    void beginCalibration();
    virtual bool execute();
    void endCalibration();
    virtual std::vector<std::string> calibrated();

    bool delayBeforeFirstTrigger_;
    bool delayEveryTrigger_;

  private:
    PixelTimer fecTimer_, phasefindingTimer_, trigTimer_, fedTimer_, allTimer_;
    log4cplus::Logger &logger_;
};

#endif
