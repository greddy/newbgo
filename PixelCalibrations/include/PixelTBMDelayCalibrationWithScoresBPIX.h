#ifndef _PixelTBMDelayCalibrationWithScoresBPIX_h_
#define _PixelTBMDelayCalibrationWithScoresBPIX_h_

#include "PixelCalibrations/include/PixelCalibrationBase.h"

class PixelTBMDelayCalibrationWithScoresBPIX : public PixelCalibrationBase {
  public:
    PixelTBMDelayCalibrationWithScoresBPIX(const PixelSupervisorConfiguration &, SOAPCommander *);

    void beginCalibration();
    virtual bool execute();
    void endCalibration();
    virtual std::vector<std::string> calibrated();

  private:
    PixelTimer fecTimer_, phasefindingTimer_, trigTimer_, fedTimer_, osdTimer_, allTimer_;
    bool checkReadback_;
    bool phaseFindingNeeded_;
    
    log4cplus::Logger &logger_;
};

#endif
