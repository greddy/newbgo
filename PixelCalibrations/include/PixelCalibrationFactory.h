#ifndef _PixelCalibrationFactory_h_
#define _PixelCalibrationFactory_h_

#include <string>

#include "PixelCalibrations/include/PixelCalibrationBase.h"
#include "PixelCalibrations/include/PixelFEDCalibrationBase.h"
#include "PixelCalibrations/include/PixelTKFECCalibrationBase.h"
#include "PixelUtilities/PixelDCSUtilities/include/PixelDCSSOAPCommander.h"
#include "PixelUtilities/PixelDCSUtilities/include/PixelDCSPVSSCommander.h"

class PixelCalibrationFactory {
  public:
    PixelCalibrationFactory() {};

    virtual ~PixelCalibrationFactory() {};

    PixelCalibrationBase *getCalibration(const std::string &calibName,
                                         const PixelSupervisorConfiguration *pixSupConfPtr,
                                         SOAPCommander *soapCmdrPtr,
                                         PixelDCSSOAPCommander *dcsSoapCommanderPtr,
                                         PixelDCSPVSSCommander *pvssCommanderPtr) const;

    PixelFEDCalibrationBase *getFEDCalibration(const std::string &calibName,
                                               const PixelFEDSupervisorConfiguration *pixFEDSupConfPtr,
                                               const pixel::utils::SOAPER *soaper) const;

    PixelTKFECCalibrationBase *getTKFECCalibration(const std::string &calibName,
                                                   const PixelTKFECSupervisorConfiguration *pixTKFECSupConfPtr,
                                                   pixel::utils::SOAPER *soaperPtr) const;
};

#endif
