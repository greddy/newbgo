#ifndef _PixelReadbacker_h_
#define _PixelIanaBCalibration_h_

#include "toolbox/exception/Handler.h"
#include "toolbox/Event.h"
#include "PixelCalibrations/include/PixelCalibrationBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelROCName.h"
#include "CalibFormats/SiPixelObjects/interface/PixelModuleName.h"
#include "PixelUtilities/PixelTKFECDataTools/include/PortCardDCU.h"

class PixelReadbacker : public PixelCalibrationBase {
  public:
    PixelReadbacker(const PixelSupervisorConfiguration &, SOAPCommander *);
    virtual ~PixelReadbacker() {};

    virtual bool execute();
    virtual void beginCalibration();
    virtual void endCalibration();

    virtual std::vector<std::string> calibrated() {
        return std::vector<std::string>();
    }

  private:
    bool ReadDCU;
    bool ScanVana;
    bool SkipProgramReadback;

    unsigned npoints_;
    std::map<pos::PixelModuleName, pos::PixelDACSettings *> dacsettings_;

    std::vector<unsigned> vanas;
    size_t nvanas;

    typedef std::map<std::pair<unsigned, unsigned>, bool> feds_needed_t;
    feds_needed_t feds_needed;

    PixelTimer timer, listMakeTimer, prog_vana_timer, arm_timer, read_timer, trig_timer, result_timer, reset_settings_timer, print_timer, program_readback_timer, dcu_timer;

    unsigned mixVana(size_t index);

    void armOSDFifo(int roc);

    std::map<pos::PixelROCName, std::vector<unsigned> > getRocReadbacks(const std::vector<pos::PixelROCName> rocs, const unsigned Readback_settings[]);
    std::map<std::pair<unsigned, unsigned>, std::vector<uint32_t> > getResultsByFEDs();

    void setDACDDR(unsigned int dacAddress, unsigned int dacValue);
    std::vector<PortCard::AddressDCU> getDCU(unsigned int crate, std::string bottop);
};

#endif
