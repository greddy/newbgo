#ifndef _PixelTBMDelay25Calibration_h_
#define _PixelTBMDelay25Calibration_h_

#include "PixelCalibrations/include/PixelCalibrationBase.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelRootDirectoryMaker.h"
#include "PixelUtilities/PixelRootUtilities/include/PixelElogMaker.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/Event.h"

#include "CalibFormats/SiPixelObjects/interface/PixelChannel.h"
#include "PixelUtilities/PixelFEDDataTools/include/Moments.h"

//class TFile;
//class TTree;
//class TH1F;
//class TH2F;
//class TString;
//class TInt;

//#include <cstdint>
#include <fstream>

#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TLine.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>

class PixelTBMDelay25Calibration : public PixelCalibrationBase {
  public:
    PixelTBMDelay25Calibration(const PixelSupervisorConfiguration &, SOAPCommander *);

    void beginCalibration();
    virtual bool execute();
    void endCalibration();
    virtual std::vector<std::string> calibrated();

  private:
    bool writeElog;

    void Analyze();
    void CloseRootf();
    //  void CloseRoots();
    void BookEm();

    TFile *rootf;
    //  TFile* roots;

    std::map<int, TH2F *> summary;

    //  std::map<int,std::map<int, std::map<int, TH1F*> > > effmap;
    //    std::map<int, std::map<int, TH1F*> > effmap;

    //  int s_channel;
    //  int s_fednumber;
    //  int s_isPass;
    //  int s_isStable;
    //  int s_isFastPlateau;
    //  int s_plateauX;

    unsigned int SDAMin;
    unsigned int SDAMax;
    unsigned int SDAStepSize;
    //unsigned int AOHGainMin;
    //unsigned int AOHGainMax;
    //unsigned int AOHGainStepSize;
    unsigned int nTriggersPerPoint;
    //float AllowPlateau;
    bool scanTrig;

    TString root_fn;
    TString outtext;
    //  TString root_fs;

    //  TString cname;
    //  TString cname_start;
    //  TString cname_end;
    //  std::ofstream ofs;

    PixelRootDirectoryMaker *rootDirs;
    //  PixelElogMaker* elog;
    std::vector<std::string> vectorOfPortcards;
    std::vector<std::string> portcard_;
    std::vector<int> aohnumber_;
    std::vector<int> channel_;
    std::vector<int> fednumber_;

    //  std::map<std::string, std::map<unsigned, unsigned>> bias_values_by_portcard_and_aoh_new;
    std::map<std::string, unsigned> sda_values_by_portcard;

    std::map<int, std::map<int, TH1F *> > histos;
    TH1F *histo_total;

    TH1F *histo_layer[2];
    //  std::map<int, std::map<unsigned, unsigned>> layers;
};

#endif
