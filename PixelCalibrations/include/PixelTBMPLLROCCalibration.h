#ifndef _PixelTBMPLLROCCalibration_h_
#define _PixelTBMPLLROCCalibration_h_

#include "PixelCalibrations/include/PixelCalibrationBase.h"

class PixelTBMPLLROCCalibration : public PixelCalibrationBase {
  public:
    PixelTBMPLLROCCalibration(const PixelSupervisorConfiguration &, SOAPCommander *);

    void beginCalibration();
    virtual bool execute();
    void endCalibration();
    virtual std::vector<std::string> calibrated();

    bool ToggleChannels;
    bool CycleScopeChannels;
    bool DelayBeforeFirstTrigger;
    bool DelayEveryTrigger;
};

#endif
