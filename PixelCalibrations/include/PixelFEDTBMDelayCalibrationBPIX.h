#ifndef _PixelFEDTBMDelayCalibrationBPIX_h_
#define _PixelFEDTBMDelayCalibrationBPIX_h_

#include "CalibFormats/SiPixelObjects/interface/PixelROCName.h"
#include "PixelCalibrations/include/PixelFEDCalibrationBase.h"
#include "PixelUtilities/PixelFEDDataTools/include/Moments.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelScanRecord.h"

//#include <cstdint>
#include <fstream>

class TFile;
class TH1F;
class TH2F;
class TH3F;

class PixelFEDTBMDelayCalibrationBPIX : public PixelFEDCalibrationBase {
  public:
    PixelFEDTBMDelayCalibrationBPIX(const PixelFEDSupervisorConfiguration &, const pixel::utils::SOAPER *);

    virtual void initializeFED();
    virtual xoap::MessageReference beginCalibration(xoap::MessageReference msg);
    virtual xoap::MessageReference execute(xoap::MessageReference msg);
    virtual xoap::MessageReference endCalibration(xoap::MessageReference msg);
    
    pos::PixelCalibConfiguration *getCalibObject() {
        pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
        assert(tempCalibObject != 0);
        return tempCalibObject;
    }
    
    const std::vector<std::pair<uint32_t, std::vector<uint32_t> > > &getFedsAndChannels() {
        return getCalibObject()->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);
    }

  private:
    void RetrieveDataScores(unsigned int state);
    void RetrieveData(unsigned int state);
    void Analyze();
    void CloseRootf();
    void BookEm(const TString &path);
    void FillEm(uint32_t state, uint32_t fedid, uint32_t ch, uint32_t which, float c);

    bool dumpFIFOs_;
    bool readFifo1_;
    std::vector<std::string> dacsToScan_;
    uint32_t lastTBMPLL_;
    TFile *rootf_;
    bool inject_;
    bool keep400MhzFixed_;
    bool keep160MhzFixed_;
    uint32_t nCalls_, theCol_, theRow_;

    bool TBMHeadersOnly_;
    bool headersOnly_;
    bool forceNoTokenPass_;
    bool ignoreWordCount_;
    bool checkReadback_;
    bool orChannels_;
    bool swapSettings_;
    bool findRange_;
    bool ignoreBadSettings_;

    std::map<uint32_t, std::map<uint32_t, std::vector<TH1F *> > > nTrigsTBM_;
    std::map<uint32_t, std::map<uint32_t, std::vector<TH2F *> > > scansTBM_;
    std::map<uint32_t, std::map<uint32_t, TH2F*> > nTrigsPerPoint_;
    std::map<std::string, std::vector<TH2F *> > TBMsHistoSum_;
    std::map<std::string, uint32_t> currentTBMPLLdelay_;

    struct branch {
        float pass;
        char moduleName[38];
    };

    struct branch_sum {
        int32_t deltaTBMPLLdelayX;
        int32_t deltaTBMPLLdelayY;
        uint32_t newTBMPLLdelayX;
        uint32_t newTBMPLLdelayY;
        double efficiency;
        char moduleName[38];
    };
    
    log4cplus::Logger &logger_;
};

#endif
