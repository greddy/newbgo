/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _PixelPowerCycleCalibration_h_
#define _PixelPowerCycleCalibration_h_

#include <boost/thread.hpp>

#include "toolbox/exception/Handler.h"
#include "toolbox/Event.h"

#include "PixelCalibrations/include/PixelCalibrationBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelLowVoltageMap.h"
#include "PixelUtilities/PixelDCSUtilities/include/PixelDCSPVSSCommander.h"

namespace log4cplus {
class Logger;
}

class PixelPowerCycleCalibration : public PixelCalibrationBase {
  public:
    // PixelPowerCycleCalibration Constructor
    //PixelPowerCycleCalibration();

    PixelPowerCycleCalibration(const PixelSupervisorConfiguration &, SOAPCommander *);

    virtual ~PixelPowerCycleCalibration() {};

    virtual void beginCalibration();

    virtual bool execute();

    virtual void endCalibration();

    virtual std::vector<std::string> calibrated();

  private:
    std::set<std::string> moduleToDP(std::set<std::string> moduleList);
    void doPowercycle(std::set<std::string> moduleList);
    void switchDP(std::string DP, std::string desiredState);
    void setDP(std::string DP, std::string value, float desiredValue);
    void setDPs(std::set<std::string> dpList, std::string value, std::string desiredValue);
    float getDP(std::string DP, std::string value);
    std::string getDPs(std::set<std::string> dpList, std::string value);
    void replaceAll(string &s, const string &search, const string &replace);
    void sentUserCommandToAMC13(std::string command, std::string value);

    std::string SendSoapBlock(Supervisors &, std::string, std::string);
    std::string SendSoapBlock(Supervisors &, std::string, std::string, Attribute_Vector &);
    void SendSoapParallelConst(Supervisors::const_iterator &, std::string &, std::string &, boost::promise<bool> &, boost::promise<std::string> &);
    void SendSoapParallelParam(Supervisors::iterator &, std::string &, std::string &, boost::promise<bool> &, boost::promise<std::string> &, Attribute_Vector &);

    std::set<std::string> maskedModules;
    std::map<std::string, std::string> note_module_map;
    pos::PixelLowVoltageMap *theLowVoltageMap_;
    PixelDCSPVSSCommander *pvssCommander_;
    log4cplus::Logger &sv_logger_;
};

#endif
