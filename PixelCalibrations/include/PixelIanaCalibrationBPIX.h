#ifndef _PixelIanaCalibrationBPIX_h_
#define _PixelIanaCalibrationBPIX_h_

#include "PixelCalibrations/include/PixelCalibrationBase.h"

class PixelIanaCalibrationBPIX : public PixelCalibrationBase {
  public:
    PixelIanaCalibrationBPIX(const PixelSupervisorConfiguration &, SOAPCommander *);

    void beginCalibration();
    virtual bool execute();
    void endCalibration();
    virtual std::vector<std::string> calibrated();

    /*bool ToggleChannels;
  bool CycleScopeChannels;
  bool DelayBeforeFirstTrigger;
  bool DelayEveryTrigger;*/
};

#endif
