#ifndef ConfigDataManager_h
#define ConfigDataManager_h

#include "PixelConfigDBInterface/include/PixelConfigInterface.h"

class ConfigDataManager {
  public:
    ConfigDataManager();
    ~ConfigDataManager();

    int insertData(std::vector<char *> args);
    int insertDataSet(std::vector<char *> args);

    int dumpFile(std::vector<char *> args);
    int dumpFileByKey(std::vector<char *> args);

  private:
    void getVersionsFromPhysicsKey(std::string config, std::vector<pos::PixelConfigBase *> &objects, int &version);
    std::vector<pos::PixelConfigBase *> getConfigByKey(std::string config, pos::PixelConfigKey *key);
    std::vector<pos::PixelConfigBase *> getConfigByVersion(std::string config, int version);
    void mkdirIfNot(std::string dir);
};

#endif
