#ifndef AliasManager_h
#define AliasManager_h

#include "PixelConfigDBInterface/include/PixelConfigInterface.h"

class AliasManager {
  public:
    AliasManager();
    ~AliasManager();
    int printConfigAliasList();
    int printConfigKeyInfo(std::vector<char *> args);
    int printConfigAliasInfo(std::vector<char *> args);
    int printVersionAliasList(std::vector<char *> args);

    int insertVersionAlias(std::vector<char *> args);
    int insertConfigAlias(std::vector<char *> args);
};

#endif
