#ifndef HelpPrinter_h
#define HelpPrinter_h

class HelpPrinter {
  public:
    HelpPrinter();
    ~HelpPrinter();
    static void printExamples();
    static void printFull();
};

#endif
