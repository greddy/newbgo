#ifndef ListBuilder_h
#define ListBuilder_h
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"

class ListBuilder {
  public:
    ListBuilder();
    ~ListBuilder();
    static std::vector<std::string> getListOfModules();
    static std::vector<std::string> getListOfPortcards();
    static std::vector<std::string> getListOfFEDs();
};

#endif
