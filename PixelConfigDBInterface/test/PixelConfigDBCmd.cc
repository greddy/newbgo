#include "PixelConfigDBInterface/test/include/HelpPrinter.h"
#include "PixelConfigDBInterface/test/include/AliasManager.h"
#include "PixelConfigDBInterface/test/include/ConfigDataManager.h"
#include <iomanip>

using namespace std;

int main(int argc, char *argv[]) {
    if (argc < 2) {
        HelpPrinter::printExamples();
        exit(0);
    }

    std::string cmd = argv[1];
    std::vector<char *> argvec;
    for (int i = 2; i < argc; ++i) {
        argvec.push_back(argv[i]);
    }

    AliasManager aliasMgr;
    ConfigDataManager configMgr;

    if (cmd == "--insertVersionAlias") {
        return aliasMgr.insertVersionAlias(argvec);
    } else if (cmd == "--insertConfigAlias") {
        return aliasMgr.insertConfigAlias(argvec);
    } else if (cmd == "--printKeyAliasList") {
        return aliasMgr.printConfigAliasList();
    } else if (cmd == "--printKeyAliasInfo") {
        return aliasMgr.printConfigAliasInfo(argvec);
    } else if (cmd == "--printKeyInfo") {
        return aliasMgr.printConfigKeyInfo(argvec);
    } else if (cmd == "--printVersionAliasList") {
        return aliasMgr.printVersionAliasList(argvec);
    } else if (cmd == "--insertData") {
        return configMgr.insertData(argvec);
    } else if (cmd == "--insertDataSet") {
        return configMgr.insertDataSet(argvec);
    } else if (cmd == "--dumpFile") {
        return configMgr.dumpFile(argvec);
    } else if (cmd == "--dumpFileByKey") {
        return configMgr.dumpFileByKey(argvec);
    } else if (cmd == "--help") {
        HelpPrinter::printFull();
        return 0;
    } else {
        std::cerr << "Unknown option:" << cmd << std::endl;
        return 1;
    }
    return 0;
}
