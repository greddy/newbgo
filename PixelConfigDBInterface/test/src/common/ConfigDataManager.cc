#include "PixelConfigDBInterface/test/include/ConfigDataManager.h"
#include "PixelConfigDBInterface/test/include/ListBuilder.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigKey.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTBMSettings.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACSettings.h"
#include "CalibFormats/SiPixelObjects/interface/PixelNameTranslation.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDetectorConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFECConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFEDConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTKFECConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortcardMap.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortCardConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDelay25Calib.h"
#include "CalibFormats/SiPixelObjects/interface/PixelLTCConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigFile.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTTCciConfig.h"
#include <sys/stat.h>
#include <iomanip>
#include <string>

ConfigDataManager::ConfigDataManager() { ; }
ConfigDataManager::~ConfigDataManager() { ; }

int ConfigDataManager::insertData(std::vector<char *> args) {
    if (args.size() < 2) {
        std::cerr << "not enough arguments for --insertData\n";
        return 1;
    }

    std::string path = args[0];
    std::string filename = args[1];
    pos::PixelConfigBase *object = 0;
    if (path == "nametranslation") {
        object = new pos::PixelNameTranslation(filename);
    } else if (path == "calib") {
        object = new pos::PixelCalibConfiguration(filename);
    } else if (path == "detconfig") {
        object = new pos::PixelDetectorConfig(filename);
    } else if (path == "ltcconfig") {
        object = new pos::PixelLTCConfig(filename);
    } else if (path == "ttcciconfig") {
        object = new pos::PixelTTCciConfig(filename);
    } else if (path == "fecconfig") {
        object = new pos::PixelFECConfig(filename);
    } else if (path == "tkfecconfig") {
        object = new pos::PixelTKFECConfig(filename);
    } else if (path == "fedconfig") {
        object = new pos::PixelFEDConfig(filename);
    } else if (path == "portcardmap") {
        object = new pos::PixelPortcardMap(filename);
    } else if (path == "amc13") {
        object = new pos::PixelAMC13Config(filename);
    } else if (path == "lowvoltagemap") {
        object = new pos::PixelLowVoltageMap(filename);
    } else if (path == "globaldelay25") {
        object = new pos::PixelGlobalDelay25(filename);
    } else if (path == "dcdc") {
        object = new pos::PixelDCDCConverterConfig(filename);
    } else if (path == "powermap") {
        object = new pos::PixelPowerMap(filename);
    } else {
        std::cout << "Unknown path: " << path << std::endl;
        assert(0);
    }

    unsigned int version = PixelConfigInterface::put(object, path);
    PixelConfigInterface::commit(0);

    std::cout << "Inserted as version: " << version << std::endl;
    if (args.size() == 3) {
        std::string alias = args[2];
        PixelConfigInterface::addVersionAlias(path, version, alias);
        PixelConfigInterface::commit(0);
        std::cout << "Aliased as: " << alias << std::endl;
    }
    return 0;
}

int ConfigDataManager::insertDataSet(std::vector<char *> args) {
    if (args.size() < 2) {
        std::cerr << "wrong number of arguments for --insertDataSet\n";
        return 1;
    }
    std::string path = args[0];
    std::string filename = args[1];
    std::vector<pos::PixelConfigBase *> objects;
    ifstream in(filename.c_str());
    assert(in.good());
    std::string datafilename;
    in >> datafilename;
    while (!in.eof()) {
        cout << "Will insert file: " << datafilename << endl;
        if (path == "dac") {
            objects.push_back(new pos::PixelDACSettings(datafilename));
        } else if (path == "tbm") {
            objects.push_back(new pos::PixelTBMSettings(datafilename));
        } else if (path == "portcard") {
            objects.push_back(new pos::PixelPortCardConfig(datafilename));
        } else if (path == "trim") {
            objects.push_back(new pos::PixelTrimAllPixels(datafilename));
        } else if (path == "mask") {
            objects.push_back(new pos::PixelMaskAllPixels(datafilename));
        } else if (path == "fedcard") {
            objects.push_back(new pos::PixelFEDCard(datafilename));
        } else {
            std::cout << "Unknown path:" << path << std::endl;
            assert(0);
        }
        in >> datafilename;
    }
    unsigned int version = PixelConfigInterface::put(objects, path);
    PixelConfigInterface::commit(0);

    std::cout << "Inserted as version: " << version << std::endl;

    if (args.size() == 3) {
        std::string alias = args[2];
        PixelConfigInterface::addVersionAlias(path, version, alias);
        PixelConfigInterface::commit(0);
        std::cout << "Aliased as: " << alias << std::endl;
    }
    return 0;
}

int ConfigDataManager::dumpFile(std::vector<char *> args) {
    if (args.size() < 1 || args.size() > 2) {
        std::cerr << "wrong number of arguments for --dumpFile\n";
        return 1;
    }

    std::string config = args[0];
    int version;
    std::vector<pos::PixelConfigBase *> objects;

    if (args.size() < 2) {
        getVersionsFromPhysicsKey(config, objects, version);
    } else {
        version = atoi(args[1]);
        objects = getConfigByVersion(config, version);
    }

    mkdirIfNot(config);

    std::stringstream directory;
    directory << config;
    directory << "/";
    directory << version;

    mkdirIfNot(directory.str());

    for (unsigned int i = 0; i < objects.size(); ++i) {
        objects[i]->writeASCII(directory.str());
    }

    std::cout << "Dumped configuration files for \e[1m" << config << "\e[0m into " << directory.str() << std::endl;
    return 0;
}

int ConfigDataManager::dumpFileByKey(std::vector<char *> args) {
    if (args.size() < 1 || args.size() > 2) {
        std::cerr << "wrong number of arguments for --dumpFileByKey\n";
        return 1;
    }

    std::string config = args[0];
    int key = atoi(args[1]);
    std::vector<pos::PixelConfigBase *> objects;

    pos::PixelConfigKey *globalKey = new pos::PixelConfigKey(key);
    objects = getConfigByKey(config, globalKey);

    mkdirIfNot(config);

    for (unsigned int i = 0; i < objects.size(); ++i) {
        objects[i]->writeASCII(config);
    }

    return 0;
}

void ConfigDataManager::getVersionsFromPhysicsKey(std::string config, std::vector<pos::PixelConfigBase *> &objects, int &version) {
    static std::map<std::string, unsigned int> aliasMap = PixelConfigInterface::getAliases_map();
    unsigned int key = aliasMap["Physics"];
    pos::PixelConfigKey *globalKey = new pos::PixelConfigKey(key);
    std::vector<std::pair<std::string, unsigned int> > versions = PixelConfigInterface::getVersions(*globalKey);
    for (unsigned int i = 0; i < versions.size(); ++i) {
        if (versions[i].first == config) {
            version = versions[i].second;
            break;
        }
    }
    objects = getConfigByKey(config, globalKey);
}

std::vector<pos::PixelConfigBase *> ConfigDataManager::getConfigByVersion(std::string config, int version) {
    std::string path = "pixel/" + config + "/";
    std::vector<pos::PixelConfigBase *> objectList;

    if (config == "nametranslation") {
        pos::PixelNameTranslation *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "fedconfig") {
        pos::PixelFEDConfig *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "fecconfig") {
        pos::PixelFECConfig *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "tkfecconfig") {
        pos::PixelTKFECConfig *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "detconfig") {
        pos::PixelDetectorConfig *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "amc13") {
        pos::PixelAMC13Config *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "portcardmap") {
        pos::PixelPortcardMap *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "dcdc") {
        pos::PixelDCDCConverterConfig *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "powermap") {
        pos::PixelPowerMap *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "globaldelay25") {
        pos::PixelGlobalDelay25 *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "ltcconfig") {
        pos::PixelLTCConfig *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "ttcciconfig") {
        pos::PixelTTCciConfig *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "lowvoltagemap") {
        pos::PixelLowVoltageMap *object = 0;
        PixelConfigInterface::get(object, path, version);
        objectList.push_back(object);
    } else if (config == "dac") {
        std::vector<std::string> modList = ListBuilder::getListOfModules();
        for (unsigned int i = 0; i < modList.size(); ++i) {
            pos::PixelDACSettings *tmp = 0;
            std::string modpath = path + modList[i];
            PixelConfigInterface::get(tmp, modpath, version);
            objectList.push_back(tmp);
        }
    } else if (config == "trim") {
        std::vector<std::string> modList = ListBuilder::getListOfModules();
        for (unsigned int i = 0; i < modList.size(); ++i) {
            pos::PixelTrimBase *tmp = 0;
            std::string modpath = path + modList[i];
            PixelConfigInterface::get(tmp, modpath, version);
            objectList.push_back(tmp);
        }
    } else if (config == "mask") {
        std::vector<std::string> modList = ListBuilder::getListOfModules();
        for (unsigned int i = 0; i < modList.size(); ++i) {
            pos::PixelMaskBase *tmp = 0;
            std::string modpath = path + modList[i];
            PixelConfigInterface::get(tmp, modpath, version);
            objectList.push_back(tmp);
        }
    } else if (config == "tbm") {
        std::vector<std::string> modList = ListBuilder::getListOfModules();
        for (unsigned int i = 0; i < modList.size(); ++i) {
            pos::PixelTBMSettings *tmp = 0;
            std::string modpath = path + modList[i];
            PixelConfigInterface::get(tmp, modpath, version);
            objectList.push_back(tmp);
        }
    } else if (config == "portcard") {
        std::vector<std::string> prtList = ListBuilder::getListOfPortcards();
        for (unsigned int i = 0; i < prtList.size(); ++i) {
            pos::PixelPortCardConfig *tmp = 0;
            std::string prtpath = path + prtList[i];
            PixelConfigInterface::get(tmp, prtpath, version);
            objectList.push_back(tmp);
        }
    } else if (config == "fedcard") {
        std::vector<std::string> fedList = ListBuilder::getListOfFEDs();
        for (unsigned int i = 0; i < fedList.size(); ++i) {
            pos::PixelFEDCard *tmp = 0;
            std::string fedpath = path + fedList[i];
            PixelConfigInterface::get(tmp, fedpath, version);
            objectList.push_back(tmp);
        }
    } else {
        cout << "Unknown path:" << path << endl;
        assert(0);
    }
    return objectList;
}

std::vector<pos::PixelConfigBase *> ConfigDataManager::getConfigByKey(std::string config, pos::PixelConfigKey *key) {
    std::string path = "pixel/" + config + "/";
    std::vector<pos::PixelConfigBase *> objectList;

    if (config == "nametranslation") {
        pos::PixelNameTranslation *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "fedconfig") {
        pos::PixelFEDConfig *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "fecconfig") {
        pos::PixelFECConfig *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "tkfecconfig") {
        pos::PixelTKFECConfig *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "detconfig") {
        pos::PixelDetectorConfig *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "amc13") {
        pos::PixelAMC13Config *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "portcardmap") {
        pos::PixelPortcardMap *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "dcdc") {
        pos::PixelDCDCConverterConfig *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "powermap") {
        pos::PixelPowerMap *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "globaldelay25") {
        pos::PixelGlobalDelay25 *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "ltcconfig") {
        pos::PixelLTCConfig *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "ttcciconfig") {
        pos::PixelTTCciConfig *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "lowvoltagemap") {
        pos::PixelLowVoltageMap *object = 0;
        PixelConfigInterface::get(object, path, *key);
        objectList.push_back(object);
    } else if (config == "dac") {
        std::vector<std::string> modList = ListBuilder::getListOfModules();
        for (unsigned int i = 0; i < modList.size(); ++i) {
            pos::PixelDACSettings *tmp = 0;
            std::string modpath = path + modList[i];
            PixelConfigInterface::get(tmp, modpath, *key);
            objectList.push_back(tmp);
        }
    } else if (config == "trim") {
        std::vector<std::string> modList = ListBuilder::getListOfModules();
        for (unsigned int i = 0; i < modList.size(); ++i) {
            pos::PixelTrimBase *tmp = 0;
            std::string modpath = path + modList[i];
            PixelConfigInterface::get(tmp, modpath, *key);
            objectList.push_back(tmp);
        }
    } else if (config == "mask") {
        std::vector<std::string> modList = ListBuilder::getListOfModules();
        for (unsigned int i = 0; i < modList.size(); ++i) {
            pos::PixelMaskBase *tmp = 0;
            std::string modpath = path + modList[i];
            PixelConfigInterface::get(tmp, modpath, *key);
            objectList.push_back(tmp);
        }
    } else if (config == "tbm") {
        std::vector<std::string> modList = ListBuilder::getListOfModules();
        for (unsigned int i = 0; i < modList.size(); ++i) {
            pos::PixelTBMSettings *tmp = 0;
            std::string modpath = path + modList[i];
            PixelConfigInterface::get(tmp, modpath, *key);
            objectList.push_back(tmp);
        }
    } else if (config == "portcard") {
        std::vector<std::string> prtList = ListBuilder::getListOfPortcards();
        for (unsigned int i = 0; i < prtList.size(); ++i) {
            pos::PixelPortCardConfig *tmp = 0;
            std::string prtpath = path + prtList[i];
            PixelConfigInterface::get(tmp, prtpath, *key);
            objectList.push_back(tmp);
        }
    } else if (config == "fedcard") {
        std::vector<std::string> fedList = ListBuilder::getListOfFEDs();
        for (unsigned int i = 0; i < fedList.size(); ++i) {
            pos::PixelFEDCard *tmp = 0;
            std::string fedpath = path + fedList[i];
            PixelConfigInterface::get(tmp, fedpath, *key);
            objectList.push_back(tmp);
        }
    } else {
        cout << "Unknown path:" << path << endl;
        assert(0);
    }
    return objectList;
}

void ConfigDataManager::mkdirIfNot(std::string dir) {
    struct stat stbuf;

    if (stat(dir.c_str(), &stbuf) != 0) {
        mkdir(dir.c_str(), 0777);
    }
}
