#include "PixelConfigDBInterface/test/include/HelpPrinter.h"
#include <iostream>
#include <string>

using namespace std;

HelpPrinter::HelpPrinter() { ; }
HelpPrinter::~HelpPrinter() { ; }

void HelpPrinter::printExamples() {
    std::string bold_on = "\e[1m";
    std::string bold_off = "\e[0m";

    cout << bold_on << "Examples:" << bold_off << endl;
    cout << "    $ PixelConfigDBCmd.exe --help" << endl;
    cout << "    $ PixelConfigDBCmd.exe --printKeyAliasList" << endl;
    cout << "    $ PixelConfigDBCmd.exe --printKeyInfo 123748" << endl;
    cout << "    $ PixelConfigDBCmd.exe --printKeyAliasInfo Physics" << endl;
    cout << "    $ PixelConfigDBCmd.exe --printVersionAliasList portcard" << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertVersionAlias tbm 42 Default" << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertConfigAlias PixelAlive 9000" << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertConfigAlias Physics dac Default detconfig Default nametranslation Default ... " << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertData nametranslation translation.dat" << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertDataSet dac list_of_dac_files.txt" << endl;
    cout << "    $ PixelConfigDBCmd.exe --dumpFile portcardmap" << endl;
    cout << "    $ PixelConfigDBCmd.exe --dumpFileByKey nametranslation 323758" << endl;
}

void HelpPrinter::printFull() {
    std::string bold_on = "\e[1m";
    std::string bold_off = "\e[0m";

    cout << "" << endl;
    cout << bold_on << "Usage: PixelConfigDBCmd.exe" << bold_off << " [OPTION] [ARGUMENTS]..." << endl;
    cout << "" << endl;
    cout << bold_on << "Description:" << bold_off << endl;
    cout << "   Tool to read and assign aliases to global keys and configuration versions. It also" << endl;
    cout << "   acts as an interface to alternate between the configuration files and the database." << endl;
    cout << "   All input arguments are position-dependent as given in the following." << endl;
    cout << "" << endl;
    cout << bold_on << "Options:" << bold_off << endl;
    cout << "   " << bold_on << "Key and Version Alias Management" << bold_off << endl;
    cout << "" << endl;
    cout << "      --help                                             Prints extended help output" << endl;
    cout << "" << endl;
    cout << "      --printKeyAliasList                                Prints list of existing global configuration key aliases" << endl;
    cout << "" << endl;
    cout << "      --printKeyInfo [KEY_NUM]                           Prints the version numbers assigned to key number provided." << endl;
    cout << "" << endl;
    cout << "      --printKeyAliasInfo [KEY_ALIAS]                    Prints the version aliases assigned to key alias provided." << endl;
    cout << "" << endl;
    cout << "      --printVersionAliasList [CONF]                     Prints the version aliases defined for the given configuration." << endl;
    cout << "" << endl;
    cout << "      --insertVersionAlias [CONF] [VER_NUM] [VER_ALIAS]  Assigns the version number to the version alias for the specified configuration." << endl;
    cout << "" << endl;
    cout << "      --insertConfigAlias [KEY_ALIAS] [KEY_NUM]          Assigns a global key number directly to the given key alias." << endl;
    cout << "" << endl;
    cout << "      --insertConfigAlias [KEY_ALIAS] [CONF_1] [VER_ALIAS_1] ... [CONF_N] [VER_ALIAS_N]" << endl;
    cout << "                                                         Assigns a set of configuration version aliases to the given key alias. " << endl;
    cout << "" << endl;
    cout << "   " << bold_on << "Configuration Data Management" << bold_off << endl;
    cout << "" << endl;
    cout << "      --insertData [CONFIG] [FILE]                       Uploads the given configuration file for the specified configuration to the database." << endl;
    cout << "" << endl;
    cout << "      --insertDataSet [CONFIG] [FILE_WITH_FILENAMES]     For configs with multiple files (e.g. dac), filenames written in given file are uploaded to DB." << endl;
    cout << "" << endl;
    cout << "      --dumpFile [CONFIG] [OPTIONAL: VER_NUM]            Dumps configuration associated to 'Physics' key to file unless version number is specified." << endl;
    cout << "" << endl;
    cout << "      --dumpFileByKey [CONFIG] [KEY_NUM]                 Dumps configuration associated to given key number specified." << endl;
    cout << "" << endl;
    cout << bold_on << "Examples:" << bold_off << endl;
    cout << "    $ PixelConfigDBCmd.exe --help" << endl;
    cout << "    $ PixelConfigDBCmd.exe --printKeyAliasList" << endl;
    cout << "    $ PixelConfigDBCmd.exe --printKeyInfo 123748" << endl;
    cout << "    $ PixelConfigDBCmd.exe --printKeyAliasInfo Physics" << endl;
    cout << "    $ PixelConfigDBCmd.exe --printVersionAliasList portcard" << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertVersionAlias tbm 42 Default" << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertConfigAlias PixelAlive 9000" << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertConfigAlias Physics dac Default detconfig Default nametranslation Default ... " << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertData nametranslation translation.dat" << endl;
    cout << "    $ PixelConfigDBCmd.exe --insertDataSet dac list_of_dac_files.txt" << endl;
    cout << "    $ PixelConfigDBCmd.exe --dumpFile portcardmap" << endl;
    cout << "    $ PixelConfigDBCmd.exe --dumpFileByKey nametranslation 323758" << endl;
}
