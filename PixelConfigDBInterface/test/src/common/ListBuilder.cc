#include "PixelConfigDBInterface/test/include/ListBuilder.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

ListBuilder::ListBuilder() { ; }
ListBuilder::~ListBuilder() { ; }

std::vector<std::string> ListBuilder::getListOfModules() {
    std::vector<std::string> list;
    for (int bpm = 0; bpm < 2; bpm++) {
        stringstream s_bplusminus;
        string side = bpm != 0 ? "m" : "p";
        s_bplusminus << "FPix_B" << side;
        for (int bm = 0; bm < 2; bm++) {
            stringstream bmname;
            string side = bm != 0 ? "I" : "O";
            bmname << s_bplusminus.str() << side;
            for (int di = 1; di <= 3; di++) {
                stringstream diskname;
                diskname << bmname.str() << "_D" << di;
                for (int bi = 1; bi <= 17; bi++) {
                    stringstream bladename;
                    bladename << diskname.str() << "_BLD" << bi;
                    for (int pi = 1; pi <= 2; pi++) {
                        stringstream panelname;
                        panelname << bladename.str() << "_PNL" << pi;
                        for (int rng = 1; rng <= 2; rng++) {
                            if (rng == 1 && bi > 11)
                                continue;
                            stringstream rngname;
                            rngname << panelname.str() << "_RNG" << rng;
                            list.push_back(rngname.str());
                        }
                    }
                }
            }
        }
    }
    for (int bpm = 0; bpm < 2; bpm++) {
        stringstream s_bplusminus;
        string side = bpm != 0 ? "m" : "p";
        s_bplusminus << "BPix_B" << side;
        for (int bm = 0; bm < 2; bm++) {
            stringstream bmname;
            string side = bm != 0 ? "I" : "O";
            bmname << s_bplusminus.str() << side;
            for (int isect = 1; isect <= 8; isect++) {
                stringstream sector;
                sector << bmname.str() << "_SEC" << isect;
                for (int blay = 1; blay <= 4; blay++) {
                    stringstream layer;
                    layer << sector.str() << "_LYR" << blay;
                    int nladders = 0;
                    switch (blay) {
                    case 1:
                        if (isect == 1 || isect == 4 || isect == 5 || isect == 8)
                            nladders = 1;
                        else
                            nladders = 2;
                        break;
                    case 2:
                        if (isect == 3 || isect == 6)
                            nladders = 1;
                        else
                            nladders = 2;
                        break;
                    case 3:
                        if (isect == 1 || isect == 8)
                            nladders = 2;
                        else
                            nladders = 3;
                        break;
                    case 4:
                        nladders = 4;
                        break;
                    }
                    for (int iladd = 1; iladd <= nladders; iladd++) {
                        int realIndex = 0;
                        switch (blay) {
                        case 1:
                            if (isect == 1)
                                realIndex = iladd;
                            if (isect == 2 || isect == 3)
                                realIndex = isect + iladd - 2;
                            if (isect == 4 || isect == 5)
                                realIndex = isect - 1;
                            if (isect == 6 || isect == 7)
                                realIndex = isect - iladd;
                            if (isect == 8)
                                realIndex = isect - 2;
                            break;
                        case 2:
                            if (isect == 1 || isect == 2)
                                realIndex = 2 * (isect - 1) + iladd;
                            if (isect == 3)
                                realIndex = isect + 2;
                            if (isect == 4 || isect == 5)
                                realIndex = 2 * (isect - 1) + iladd - 1;
                            if (isect == 6)
                                realIndex = isect + 4;
                            if (isect == 7 || isect == 8)
                                realIndex = 2 * (isect - 1) + iladd - 2;
                            break;
                        case 3:
                            realIndex = 3 * (isect - 1) + iladd - 1;
                            if (isect == 1)
                                ++realIndex;
                            break;
                        case 4:
                            realIndex = 4 * (isect - 1) + iladd;
                            break;
                        }
                        stringstream ladder;
                        ladder << layer.str() << "_LDR" << realIndex << "F";
                        for (int imod = 1; imod <= 4; imod++) {
                            if (blay == 1) {
                                switch (imod) {
                                case 1:
                                    if (!(isect == 2 && realIndex == 1) &&
                                        !(isect == 3 && realIndex == 2) &&
                                        !(isect == 4 && realIndex == 3) &&
                                        !(isect == 6 && realIndex == 4) &&
                                        !(isect == 7 && realIndex == 5) &&
                                        !(isect == 8 && realIndex == 6))
                                        continue;
                                    break;
                                case 2:
                                    if (!(isect == 1 && realIndex == 1) &&
                                        !(isect == 3 && realIndex == 2) &&
                                        !(isect == 4 && realIndex == 3) &&
                                        !(isect == 5 && realIndex == 4) &&
                                        !(isect == 7 && realIndex == 5) &&
                                        !(isect == 8 && realIndex == 6))
                                        continue;
                                    break;
                                case 3:
                                    if (!(isect == 1 && realIndex == 1) &&
                                        !(isect == 2 && realIndex == 2) &&
                                        !(isect == 4 && realIndex == 3) &&
                                        !(isect == 5 && realIndex == 4) &&
                                        !(isect == 6 && realIndex == 5) &&
                                        !(isect == 8 && realIndex == 6))
                                        continue;
                                    break;
                                case 4:
                                    if (!(isect == 1 && realIndex == 1) &&
                                        !(isect == 2 && realIndex == 2) &&
                                        !(isect == 3 && realIndex == 3) &&
                                        !(isect == 5 && realIndex == 4) &&
                                        !(isect == 6 && realIndex == 5) &&
                                        !(isect == 7 && realIndex == 6))
                                        continue;
                                    break;
                                }
                            }
                            stringstream module;
                            module << ladder.str() << "_MOD" << imod;
                            list.push_back(module.str());
                            stringstream hmodule;
                            if (blay == 1) {
                                std::string replacement = "H";
                                hmodule << module.str().replace(module.str().find("F"), 1, replacement);
                                list.push_back(hmodule.str());
                            }
                        }
                    }
                }
            }
        }
    }
    return list;
}

std::vector<std::string> ListBuilder::getListOfPortcards() {
    std::vector<std::string> list;
    for (int bpm = 0; bpm < 2; bpm++) {
        stringstream s_bplusminus;
        string side = bpm != 0 ? "m" : "p";
        s_bplusminus << "FPix_B" << side;
        for (int bm = 0; bm < 2; bm++) {
            stringstream bmname;
            string side = bm != 0 ? "I" : "O";
            bmname << s_bplusminus.str() << side;
            for (int di = 1; di <= 3; di++) {
                stringstream diskname;
                diskname << bmname.str() << "_D" << di;
                for (int iprt = 1; iprt <= 4; iprt++) {
                    stringstream pcname;
                    pcname << diskname.str() << "_PRT" << iprt;
                    list.push_back(pcname.str());
                }
            }
        }
    }
    for (int bpm = 0; bpm < 2; bpm++) {
        stringstream s_bplusminus;
        string side = bpm != 0 ? "m" : "p";
        s_bplusminus << "BPix_B" << side;
        for (int bm = 0; bm < 2; bm++) {
            stringstream bmname;
            string side = bm != 0 ? "I" : "O";
            bmname << s_bplusminus.str() << side;
            for (int isect = 1; isect <= 8; isect++) {
                stringstream sector;
                sector << bmname.str() << "_SEC" << isect;
                for (int iprt = 1; iprt <= 2; iprt++) {
                    stringstream pcname;
                    pcname << sector.str() << "_PRT" << iprt;
                    list.push_back(pcname.str());
                }
            }
        }
    }
    return list;
}

std::vector<std::string> ListBuilder::getListOfFEDs() {
    std::vector<std::string> fedNums;
    for (int i = 1200; i < 1339; ++i) {
        if (!(i > 1209 && i < 1212) && !(i > 1221 && i < 1224) && !(i > 1233 && i < 1236) && !(i > 1245 && i < 1248) &&
            !(i > 1257 && i < 1260) && !(i > 1269 && i < 1272) && !(i > 1281 && i < 1284) && !(i > 1293 && i < 1296) &&
            !(i > 1302 && i < 1308) && !(i > 1314 && i < 1320) && !(i > 1326 && i < 1332)) {
            stringstream strconv;
            strconv << i;
            fedNums.push_back(strconv.str());
        }
    }
    return fedNums;
}
