#include "PixelConfigDBInterface/test/include/AliasManager.h"
#include <iomanip>

AliasManager::AliasManager() { ; }
AliasManager::~AliasManager() { ; }

int AliasManager::printConfigAliasList() {
    std::vector<std::pair<std::string, unsigned int> > aliases = PixelConfigInterface::getAliases();
    std::cout << "Currently defined aliases:" << std::endl << std::endl;
    unsigned int maxAliasLength = 11;
    for (unsigned int i = 0; i < aliases.size(); i++) {
        if (aliases[i].first.size() > maxAliasLength) {
            maxAliasLength = aliases[i].first.size();
        }
    }
    std::cout << setw(maxAliasLength) << std::left << "Config Alias"
              << " | "
              << "Global Key" << std::endl;
    std::cout << std::string(2 * maxAliasLength + 3, '=') << std::endl;
    for (unsigned int i = 0; i < aliases.size(); i++) {
        std::cout << setw(maxAliasLength) << std::left << aliases[i].first << " | " << aliases[i].second << std::endl;
    }
    return 0;
}

int AliasManager::printConfigKeyInfo(std::vector<char *> args) {
    if (args.size() < 1) {
        std::cerr << "wrong number of arguments for --keyInfo\n";
        return 1;
    }
    unsigned int key = atoi(args[0]);
    std::vector<std::pair<std::string, unsigned int> > versions;
    versions = PixelConfigInterface::getVersions(pos::PixelConfigKey(key));
    std::cout << "************ GLOBAL KEY " << key << " **************" << std::endl << std::endl;
    int count = 1;
    std::cout << std::setw(23) << "KOC"
              << "\t"
              << "version" << std::endl << std::endl;
    for (std::vector<std::pair<std::string, unsigned int> >::iterator it = versions.begin();
         it != versions.end(); it++) {
        std::cout << std::setw(2) << count << "]" << std::setw(20) << (*it).first << "\t" << (*it).second << std::endl;
        count++;
    }
    std::cout << "______________________________________________" << std::endl;
    return 0;
}

int AliasManager::printConfigAliasInfo(std::vector<char *> args) {
    if (args.size() < 1) {
        std::cerr << "wrong number of arguments for --printKeyAliasInfo\n";
        return 1;
    }
    std::string alias = args[0];
    unsigned int key;
    std::vector<std::pair<std::string, std::string> > versionAliases;
    bool exists = PixelConfigInterface::getVersionAliases(alias, key, versionAliases);
    if (exists) {
        cout << alias << " (key=" << key << ")";
        if (versionAliases.size() > 0) {
            cout << ": ";
            for (unsigned int i = 0; i < versionAliases.size(); ++i) {
                cout << versionAliases[i].first << " " << versionAliases[i].second << " ";
            }
        }
        std::cout << endl;
    } else {
        std::cout << "The configuration alias '" << alias << "' does not exist." << std::endl;
    }
    return 0;
}

int AliasManager::printVersionAliasList(std::vector<char *> args) {
    if (args.size() < 1) {
        std::cerr << "wrong number of arguments for --printVersionAlias\n";
        return 1;
    }
    std::string path = args[0];
    std::map<std::string, std::vector<std::pair<unsigned int, std::string> > > numbersAndAliases = PixelConfigInterface::getVersionData(path);
    unsigned int maxAliasLength = 7; // length of word "Version"

    for (unsigned int i = 0; i < numbersAndAliases[path].size(); i++) {
        if (numbersAndAliases[path][i].second.size() > maxAliasLength) {
            maxAliasLength = numbersAndAliases[path][i].second.size();
        }
    }

    std::cout << "Currently aliased versions for data type: " << path << std::endl << std::endl;
    std::cout << setw(maxAliasLength) << std::left << "Alias"
              << " | "
              << "Version" << std::endl;
    std::cout << std::string(2 * maxAliasLength + 3, '=') << std::endl;
    for (unsigned int i = 0; i < numbersAndAliases[path].size(); ++i) {
        std::cout << setw(maxAliasLength) << std::left << numbersAndAliases[path][i].second << " | " << numbersAndAliases[path][i].first << std::endl;
    }
    return 0;
}

int AliasManager::insertVersionAlias(std::vector<char *> args) {
    if (args.size() < 3) {
        std::cerr << "wrong number of arguments for --insertVersionAlias\n";
        return 1;
    }
    std::string path = args[0];
    unsigned int version = atoi(args[1]);
    std::string alias = args[2];
    PixelConfigInterface::addVersionAlias(path, version, alias);
    PixelConfigInterface::commit(0);
    return 0;
}

int AliasManager::insertConfigAlias(std::vector<char *> args) {
    if (args.size() < 2) {
        std::cerr << "wrong number of arguments for --insertConfigAlias\n";
        return 1;
    } else if (args.size() == 2) {
        std::string alias = args[0];
        unsigned int key = atoi(args[1]);
        PixelConfigInterface::addAlias(alias, key);
        PixelConfigInterface::commit(0);
    } else {
        std::string alias = args[0];
        unsigned int argcounter = 1;
        std::vector<std::pair<std::string, unsigned int> > versionNumbers;
        std::vector<std::pair<std::string, std::string> > versionAliases;
        while (argcounter + 1 < args.size()) {
            std::string path = args[argcounter];
            std::string alias = args[argcounter + 1];
            unsigned int ver;
            if (isdigit(*args[argcounter + 1])) {
                ver = atoi(args[argcounter + 1]);
            } else {
                std::pair<std::string, std::string> apair(path, alias);
                versionAliases.push_back(apair);
                ver = PixelConfigInterface::getVersion(path, alias);
            }
            std::pair<std::string, unsigned int> apair(path, ver);
            versionNumbers.push_back(apair);
            argcounter += 2;
        }

        unsigned int key = PixelConfigInterface::makeKey(versionNumbers);

        PixelConfigInterface::addAlias(alias, key, versionAliases);
        PixelConfigInterface::commit(0);
    }
    return 0;
}
