#ifndef PixelConfigDBInterface_h
#define PixelConfigDBInterface_h

#include "PixelConfigDBInterface/include/PixelSQLCommand.h"
#include "PixelConfigDBInterface/include/PixelOracleDatabase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigKey.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACSettings.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTrimAllPixels.h"
#include "CalibFormats/SiPixelObjects/interface/PixelMaskAllPixels.h"
#include "CalibFormats/SiPixelObjects/interface/PixelAMC13Config.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFECConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFEDConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFEDCard.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTKFECConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortcardMap.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDetectorConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelLowVoltageMap.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortCardConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTBMSettings.h"
#include "CalibFormats/SiPixelObjects/interface/PixelNameTranslation.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTTCciConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelLTCConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFEDTestDAC.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDCDCConverterConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPowerMap.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDelay25Calib.h"
#include "CalibFormats/SiPixelObjects/interface/PixelGlobalDelay25.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFEDTestDAC.h"
#include <string>
#include <assert.h>
#include <iostream>
#include <typeinfo>
#include <sstream>
#include <vector>
#include "CalibFormats/SiPixelObjects/interface/PixelConfigFile.h"

#include "xgi/Method.h"

using namespace std;

class PixelConfigDBInterface {

  public:
    PixelConfigDBInterface();
    virtual ~PixelConfigDBInterface();
    bool isConnected();
    bool connect();
    void disconnect();

    void dumpTableInfo(const char *table_name, std::map<std::string, std::string> where, bool bForUpdate = false, bool like = false);
    vector<vector<string> > getTable(std::string path, pos::PixelConfigKey key);
    vector<vector<string> > getTable(std::string path, unsigned int version);
    std::vector<std::pair<std::string, unsigned int> > getAliases();
    std::vector<std::pair<std::string, unsigned int> > getVersions(pos::PixelConfigKey key);
    void addAlias(std::string alias, unsigned int key, std::vector<std::pair<std::string, std::string> > versionaliases);
    void addAuthor(std::string author);
    void addComment(std::string comment);
    unsigned int makeKey(std::vector<std::pair<std::string, unsigned int> > versions);
    void addVersionAlias(std::string path, unsigned int version, std::string alias);
    void addAlias(std::string alias, unsigned int key);
    bool getVersionAliases(std::string configAlias, unsigned int &key, std::vector<std::pair<std::string, std::string> > &versionAliases);
    std::string uploadStatus(std::string);
    std::vector<std::string> getVersionAliases(std::string path);
    std::string commitToDB(unsigned int globalKey);
    std::map<std::string, unsigned int> getAliases_map();
    std::map<std::string, std::vector<std::pair<unsigned int, std::string> > > getVersionData();
    std::map<std::string, std::vector<std::pair<unsigned int, std::string> > > getVersionData(std::string koc);
    std::set<unsigned int> getExistingVersions(std::string koc);
    unsigned int getNextAvailableVersion(std::string sequence);
    unsigned int clone(unsigned int oldkey, std::string path, unsigned int version);
    void setKeyAlias(pos::PixelConfigKey key, std::string alias);

    template <class T>
    void get(T *&data, std::string path, pos::PixelConfigKey key) {
        data = 0;
        vector<vector<string> > databaseTable;
        databaseTable = getTable(path, key);
        if (databaseTable.empty()) {
            return;
        }
        try {
            if (typeid(data) == typeid(pos::PixelDACSettings *)) {
                data = (T *)new pos::PixelDACSettings(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTrimBase *)) {
                data = (T *)new pos::PixelTrimAllPixels(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelMaskAllPixels *)) {
                data = (T *)new pos::PixelMaskAllPixels(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelMaskBase *)) {
                data = (T *)new pos::PixelMaskAllPixels(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelAMC13Config *)) {
                data = (T *)new pos::PixelAMC13Config(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelDCDCConverterConfig *)) {
                data = (T *)new pos::PixelDCDCConverterConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelPowerMap *)) {
                data = (T *)new pos::PixelPowerMap(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelFECConfig *)) {
                data = (T *)new pos::PixelFECConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelFEDConfig *)) {
                data = (T *)new pos::PixelFEDConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelFEDCard *)) {
                data = (T *)new pos::PixelFEDCard(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelPortcardMap *)) {
                data = (T *)new pos::PixelPortcardMap(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelDetectorConfig *)) {
                data = (T *)new pos::PixelDetectorConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelLowVoltageMap *)) {
                data = (T *)new pos::PixelLowVoltageMap(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelPortCardConfig *)) {
                data = (T *)new pos::PixelPortCardConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTBMSettings *)) {
                data = (T *)new pos::PixelTBMSettings(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelNameTranslation *)) {
                data = (T *)new pos::PixelNameTranslation(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTKFECConfig *)) {
                data = (T *)new pos::PixelTKFECConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTTCciConfig *)) {
                data = (T *)new pos::PixelTTCciConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelLTCConfig *)) {
                data = (T *)new pos::PixelLTCConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelGlobalDelay25 *)) {
                data = (T *)new pos::PixelGlobalDelay25(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelCalibBase *)) {
                std::string calibType_ = "noCalib";
                if (databaseTable.size() != 1) {
                    calibType_ = databaseTable[1][6];
                } else {
                    data = 0;
                }
                if (calibType_ == "calib") {
                    data = (T *)new pos::PixelCalibConfiguration(databaseTable);
                } else if (calibType_ == "delay25") {
                    data = (T *)new pos::PixelDelay25Calib(databaseTable);
                } else if (calibType_ == "fedtestdac") {
                    data = (T *)new pos::PixelFEDTestDAC(databaseTable);
                } else {
                    data = 0;
                }
            } else {
                cout << __LINE__ << "No matching object with typeid " << typeid(data).name() << std::endl;
                assert(0);
                data = 0;
            }
            return;
        }
        catch (std::bad_typeid&) {
            cerr << "Bad typeid" << endl;
            assert(0);
        }
        catch (std::bad_alloc&) {
            cerr << "Error allocating memory." << endl;
            assert(0);
        }
        catch (std::exception &e) {
            cout << __LINE__ << "Exception raised: " << e.what() << endl;
            string err_message = string(e.what()) + string(typeid(data).name());
            assert(0);
        }
    }

    template <class T>
    void getByVersion(T *&data, std::string path, unsigned int version) {
        std::string mthn = "]\t[PixelConfigDBInterface::getByVersion()]\t\t\t\t    ";
        data = 0;
        vector<vector<string> > databaseTable;
        databaseTable = getTable(path, version);
        try {
            if (typeid(data) == typeid(pos::PixelDACSettings *)) {
                data = (T *)new pos::PixelDACSettings(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTrimBase *)) {
                data = (T *)new pos::PixelTrimAllPixels(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelMaskAllPixels *)) {
                data = (T *)new pos::PixelMaskAllPixels(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelMaskBase *)) {
                data = (T *)new pos::PixelMaskAllPixels(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelAMC13Config *)) {
                data = (T *)new pos::PixelAMC13Config(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelDCDCConverterConfig *)) {
                data = (T *)new pos::PixelDCDCConverterConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelPowerMap *)) {
                data = (T *)new pos::PixelPowerMap(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelFECConfig *)) {
                data = (T *)new pos::PixelFECConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelFEDConfig *)) {
                data = (T *)new pos::PixelFEDConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelFEDCard *)) {
                data = (T *)new pos::PixelFEDCard(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelPortcardMap *)) {
                data = (T *)new pos::PixelPortcardMap(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelDetectorConfig *)) {
                data = (T *)new pos::PixelDetectorConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelLowVoltageMap *)) {
                data = (T *)new pos::PixelLowVoltageMap(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelPortCardConfig *)) {
                data = (T *)new pos::PixelPortCardConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTBMSettings *)) {
                data = (T *)new pos::PixelTBMSettings(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelNameTranslation *)) {
                data = (T *)new pos::PixelNameTranslation(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTKFECConfig *)) {
                data = (T *)new pos::PixelTKFECConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTTCciConfig *)) {
                data = (T *)new pos::PixelTTCciConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelLTCConfig *)) {
                data = (T *)new pos::PixelLTCConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelGlobalDelay25 *)) {
                data = (T *)new pos::PixelGlobalDelay25(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelCalibBase *)) {
                std::string calibType_ = "noCalib";
                if (databaseTable.size() != 1) {
                    calibType_ = databaseTable[1][6];
                } else {
                    data = 0;
                }
                if (calibType_ == "calib") {
                    data = (T *)new pos::PixelCalibConfiguration(databaseTable);
                } else if (calibType_ == "delay25") {
                    data = (T *)new pos::PixelDelay25Calib(databaseTable);
                } else if (calibType_ == "fedtestdac") {
                    data = (T *)new pos::PixelFEDTestDAC(databaseTable);
                } else {
                    data = 0;
                }
            } else {
                std::cout << "Sorry, that config has not been implemented into getByVersion for PixelConfigDBInterface yet" << std::endl;
                data = 0;
            }
            return;
        }
        catch (std::bad_typeid&) {
            cerr << mthn << "Bad typeid" << endl;
            assert(0);
        }
        catch (std::bad_alloc&) {
            cerr << mthn << "Error allocating memory." << endl;
            assert(0);
        }
        catch (std::exception &e) {
            cout << __LINE__ << mthn << "Exception raised: " << e.what() << endl;
            string err_message = string(e.what()) + string(typeid(data).name());
            assert(0);
        }
    }

    template <class T>
    bool configurationDataExists(T *&data, std::string path, pos::PixelConfigKey key) {
        std::string mthn = "]\t[PixelConfigDBInterface::configurationDataExists()]\t\t    ";

        vector<vector<string> > databaseTable;
        databaseTable = getTable(path, key);
        if (databaseTable.empty()) {
            return false;
        }
        try {
            if (typeid(data) == typeid(pos::PixelDACSettings *)) {
                data = (T *)new pos::PixelDACSettings(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTrimBase *)) {
                data = (T *)new pos::PixelTrimAllPixels(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelMaskAllPixels *)) {
                data = (T *)new pos::PixelMaskAllPixels(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelMaskBase *)) {
                data = (T *)new pos::PixelMaskAllPixels(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelAMC13Config *)) {
                data = (T *)new pos::PixelAMC13Config(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelDCDCConverterConfig *)) {
                data = (T *)new pos::PixelDCDCConverterConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelPowerMap *)) {
                data = (T *)new pos::PixelPowerMap(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelFECConfig *)) {
                data = (T *)new pos::PixelFECConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelFEDConfig *)) {
                data = (T *)new pos::PixelFEDConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelFEDCard *)) {
                data = (T *)new pos::PixelFEDCard(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelPortcardMap *)) {
                data = (T *)new pos::PixelPortcardMap(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelDetectorConfig *)) {
                data = (T *)new pos::PixelDetectorConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelLowVoltageMap *)) {
                data = (T *)new pos::PixelLowVoltageMap(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelPortCardConfig *)) {
                data = (T *)new pos::PixelPortCardConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTBMSettings *)) {
                data = (T *)new pos::PixelTBMSettings(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelNameTranslation *)) {
                data = (T *)new pos::PixelNameTranslation(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTKFECConfig *)) {
                data = (T *)new pos::PixelTKFECConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelTTCciConfig *)) {
                data = (T *)new pos::PixelTTCciConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelLTCConfig *)) {
                data = (T *)new pos::PixelLTCConfig(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelGlobalDelay25 *)) {
                data = (T *)new pos::PixelGlobalDelay25(databaseTable);
            } else if (typeid(data) == typeid(pos::PixelCalibBase *)) {
                std::string calibType_ = databaseTable[1][6];
                if (calibType_ == "calib") {
                    data = (T *)new pos::PixelCalibConfiguration(databaseTable);
                } else if (calibType_ == "delay25") {
                    data = (T *)new pos::PixelDelay25Calib(databaseTable);
                } else if (calibType_ == "fedtestdac") {
                    data = (T *)new pos::PixelFEDTestDAC(databaseTable);
                } else {
                    std::cout << __LINE__ << mthn << "Unknown calibration type: " << calibType_ << std::endl;
                    assert(0);
                }
            } else {
                std::cout << __LINE__ << mthn << "No matching object with typeid " << typeid(data).name() << std::endl;
                assert(0);
                data = 0;
            }
            if (databaseTable.size() > 1)
                return true;
            return false;
        }
        catch (std::bad_typeid&) {
            std::cerr << __LINE__ << mthn << "Bad typeid" << std::endl;
            assert(0);
        }
        catch (std::bad_alloc&) {
            std::cerr << mthn << "Error allocating memory." << std::endl;
            assert(0);
        }
        catch (std::exception &e) {
            cout << __LINE__ << mthn << "Exception raised: " << e.what() << endl;
            string err_message = string(e.what()) + string(typeid(data).name());
            assert(0);
        }
    }

    template <class T>
    int put(T *&data) {
        std::stringstream s;
        s << "[PixelConfigDBInterface::put()]\t\t\t\t    ";
        std::string mthn = s.str();

        char *cpath = getenv("PIXELCONFIGURATIONSPOOLAREA");
        if (cpath == NULL) {
            cout << __LINE__ << mthn << "FATAL: PIXELCONFIGURATIONSPOOLAREA is undefined" << endl;
            assert(0);
        }
        std::string path(cpath);
        int version = getNextAvailableVersion(getSequenceName(data));
        cout << __LINE__ << mthn << "Type of data to write : " << typeid(data).name() << endl;

        if (dynamic_cast<pos::PixelFEDCard *>(data)) {
            data->writeASCII(cpath);
        }
        ofstream *outstream = new ofstream();
        data->writeXMLHeader(version, path, outstream);
        data->writeXML(outstream);
        data->writeXMLTrailer(outstream);
        delete outstream;
        cout << __LINE__ << mthn << "Data written!" << endl;
        return version;
    }

    template <class T>
    int put(std::vector<T *> &data) {
        std::stringstream s;
        s << "[PixelConfigDBInterface::put()]\t\t\t\t    ";
        std::string mthn = s.str();

        char *cpath = getenv("PIXELCONFIGURATIONSPOOLAREA");
        if (cpath == NULL) {
            cout << __LINE__ << mthn << "FATAL: PIXELCONFIGURATIONSPOOLAREA is undefined" << endl;
            assert(0);
        }
        std::string path(cpath);
        int version = getNextAvailableVersion(getSequenceName(data[0]));
        typename std::vector<T *>::iterator dataIt = data.begin();

        if (dynamic_cast<pos::PixelFEDCard *>(*dataIt)) {
            for (; dataIt != data.end(); dataIt++) {
                (*dataIt)->writeASCII(cpath);
            }
            dataIt = data.begin();
        }
        ofstream *out = new ofstream();
        (*dataIt)->writeXMLHeader(version, path, out);
        for (; dataIt != data.end(); dataIt++) {
            (*dataIt)->writeXML(out);
        }
        dataIt = data.begin();
        (*dataIt)->writeXMLTrailer(out);
        delete out;

        cout << __LINE__ << mthn << "Data written!" << endl;
        return version;
    }

    template <class T>
    string getSequenceName(T *&data) {
        std::string mthn = "\t[PixelConfigDBInterface::getSequencesName(T*&)]\t\t    ";
        try {
            if (dynamic_cast<pos::PixelDACSettings *>(data)) {
                return "ROC DAC Settings Col";
            } else if (dynamic_cast<pos::PixelTrimBase *>(data)) {
                return "ROC Trim Bits";
            } else if (dynamic_cast<pos::PixelMaskBase *>(data)) {
                return "ROC Mask Bits";
            } else if (dynamic_cast<pos::PixelFECConfig *>(data)) {
                return "Pixel FEC Parameters";
            } else if (dynamic_cast<pos::PixelFEDConfig *>(data)) {
                return "Pixel FED Crate Configuration";
            } else if (dynamic_cast<pos::PixelFEDCard *>(data)) {
                return "Pixel FED Configuration";
            } else if (dynamic_cast<pos::PixelPortcardMap *>(data)) {
                return "Pixel Port Card Map";
            } else if (dynamic_cast<pos::PixelDetectorConfig *>(data)) {
                return "Pixel Detector Configuration";
            } else if (dynamic_cast<pos::PixelLowVoltageMap *>(data)) {
                return "XDAQ Low Voltage Map";
            } else if (dynamic_cast<pos::PixelPortCardConfig *>(data)) {
                return "Pixel Port Card Settings";
            } else if (dynamic_cast<pos::PixelTBMSettings *>(data)) {
                return "Pixel TBM Parameters";
            } else if (dynamic_cast<pos::PixelNameTranslation *>(data)) {
                return "Pixel Name Translation";
            } else if (dynamic_cast<pos::PixelTKFECConfig *>(data)) {
                return "Tracker FEC Parameters";
            } else if (dynamic_cast<pos::PixelTTCciConfig *>(data)) {
                return "TTC Configuration Parameters";
            } else if (dynamic_cast<pos::PixelLTCConfig *>(data)) {
                return "LTC Configuration Parameters";
            } else if (dynamic_cast<pos::PixelCalibBase *>(data)) {
                return "Calibration Object Clob";
            } else if (dynamic_cast<pos::PixelCalibConfiguration *>(data)) {
                return "Calibration Object Clob";
            } else if (dynamic_cast<pos::PixelGlobalDelay25 *>(data)) {
                return "Pixel Global Delay25";
            } else if (dynamic_cast<pos::PixelAMC13Config *>(data)) {
                return "Pixel AMC13 Settings";
            } else if (dynamic_cast<pos::PixelDCDCConverterConfig *>(data)) {
                return "Pixel DCDC Configuration";
            } else if (dynamic_cast<pos::PixelPowerMap *>(data)) {
                return "Pixel Power Map";
            } else {
                cout << __LINE__ << mthn << "No matching object with typeid " << typeid(data).name() << std::endl;
                assert(0);
                data = 0;
            }
            return "";
        } //end try
        catch (std::bad_typeid&) {
            cerr << mthn << "Bad typeid" << endl;
            assert(0);
        }
        catch (std::bad_alloc&) {
            cerr << mthn << "Error allocating memory." << endl;
            assert(0);
        }
        catch (std::exception &e) {
            cout << __LINE__ << mthn << "Exception raised: " << e.what() << endl;
            string err_message = string(e.what()) + string(typeid(data).name());
            assert(0);
        }
    }

  private:
    PixelOracleDatabase database_;
    PixelSQLCommand *makeQueryByKey(const char *configName, unsigned int globalKey);
    PixelSQLCommand *makeQueryByVersion(const char *configName, unsigned int version);
    PixelSQLCommand *makeQueryWhere(const char *configName, map<string, string> where);
    bool configHasManyObjects(const char *configName);
    vector<vector<string> > getTableByQuery(PixelSQLCommand &query);
    void dumpGeneralTable(vector<vector<string> > &table);
    void fillHashes();

    std::map<std::string, std::string> conversionKOC;
    std::map<std::string, std::string> conversionNameKOC;
    std::map<std::string, std::string> conversionETNKOC;
    std::string theAlias_;
    std::string author_;
    std::string comment_;
    std::string theTrueDBKey_; // filled by the method setKeyAlias(key, alias) ;

    map<string, unsigned int> cacheIndex;
    map<string, vector<vector<string> > > cacheTrim;
    map<string, vector<vector<string> > > cacheMask;
    map<string, vector<vector<string> > > cacheDAC;
    map<string, vector<vector<string> > > cacheTBM;
    map<string, vector<vector<string> > > cachePortcard;
    map<string, vector<vector<string> > > cacheFEDcard;

    bool dataInCache(string configName, unsigned int globalKey);
    vector<vector<string> > getTableFromCache(string configName, string objectName);
    void resetCache(string);
    void fillCache(string configName, unsigned int globalKey, vector<vector<string> > &databaseTable);
    void fillTrimCache(vector<vector<string> > &dt);
    void fillMaskCache(vector<vector<string> > &dt);
    void fillDACCache(vector<vector<string> > &dt);
    void fillTBMCache(vector<vector<string> > &dt);
    void fillPortcardCache(vector<vector<string> > &dt);
    void fillFEDcardCache(vector<vector<string> > &dt);
    void fillCacheIndex(string configName, unsigned int globalKey);
    int colNumFromName(vector<string> columns, string name);
    string moduleNameFromROCName(std::string &rocName);

    std::vector<std::string> split(const string &s, char delim) {
        stringstream ss(s);
        string item;
        vector<string> tokens;
        while (getline(ss, item, delim)) {
            tokens.push_back(item);
        }
        return tokens;
    };
};

#endif
