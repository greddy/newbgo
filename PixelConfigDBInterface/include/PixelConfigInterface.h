#ifndef PixelConfigInterface_h
#define PixelConfigInterface_h

#include <stdlib.h>
#include <iostream>
#include <string>
#include <set>
#include "PixelConfigDBInterface/include/PixelConfigDBInterface.h" // was DB
#include "CalibFormats/SiPixelObjects/interface/PixelConfigFile.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigKey.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigDoc.info"
#include <cstdlib>

class PixelConfigInterface {

  public:
    PixelConfigInterface() {
        getMode();
    }

    PixelConfigInterface(bool mode) {
        setMode(mode);
    }

    static void setMode(bool m) {
        if (getMode() != m) {
            getMode() = m;
            if (getMode() == true) {
                pixelConfigDB().connect();
            } else {
                pixelConfigDB().disconnect();
            }
        }
    }

    static void setKeyAlias(pos::PixelConfigKey key, std::string theAlias) {
        if (getMode()) {
            pixelConfigDB().setKeyAlias(key, theAlias);
        } else {
            ;
        }
    }

    static void setGlobalKey(pos::PixelConfigKey key) {
        if (getGlobalKey().key() != key.key()) {
            getGlobalKey() = key;
        }
    }

    template <class T>
    static void get(T *&pixelObject, std::string path, pos::PixelConfigKey key) {
        setGlobalKey(key);
        if (getMode()) {
            pixelConfigDB().get(pixelObject, path, key);
        } else {
            pos::PixelConfigFile::get(pixelObject, path, key);
        }
    }

    template <class T>
    static void get(T *&pixelObject, std::string path, unsigned int version) {
        if (getMode()) {
            pixelConfigDB().getByVersion(pixelObject, path, version);
        } else {
            pos::PixelConfigFile::get(pixelObject, path, version);
        }
    }

    template <class T>
    static bool configurationDataExists(T *&pixelObject, std::string path, pos::PixelConfigKey key) {
        bool retStatus = false;
        if (getMode()) {
            retStatus = pixelConfigDB().configurationDataExists(pixelObject, path, key);
        } else {
            retStatus = pos::PixelConfigFile::configurationDataExists(pixelObject, path, key);
        }
        return retStatus;
    }

    static std::vector<std::pair<std::string, unsigned int> > getAliases() {
        std::vector<std::pair<std::string, unsigned int> > aliases;
        if (getMode()) {
            aliases = pixelConfigDB().getAliases();
        } else {
            aliases = pos::PixelConfigFile::getAliases();
        }
        return aliases;
    }

    static std::vector<std::vector<std::string> > getTable(std::string path, pos::PixelConfigKey key) {
        std::vector<std::vector<std::string> > databaseTable;
        if (getMode()) {
            databaseTable = pixelConfigDB().getTable(path, key);
        } else {
            std::cout << "No tables for files" << std::endl;
            assert(0);
        }
        return databaseTable;
    }

    static std::vector<std::vector<std::string> > getTable(std::string path, unsigned int version) {
        std::vector<std::vector<std::string> > databaseTable;
        if (getMode()) {
            databaseTable = pixelConfigDB().getTable(path, version);
        } else {
            std::cout << "No tables for files" << std::endl;
            assert(0);
        }
        return databaseTable;
    }
    //==============================================================================
    static bool getVersionAliases(std::string configAlias,
                                  unsigned int &key,
                                  std::vector<std::pair<std::string, std::string> > &versionAliases) {
        bool retStatus = false;
        if (getMode()) {
            retStatus = pixelConfigDB().getVersionAliases(configAlias, key, versionAliases);
        } else {
            retStatus = pos::PixelConfigFile::getVersionAliases(configAlias, key, versionAliases);
        }
        return retStatus;
    }
    //==============================================================================
    static void forceAliasesReload(bool mode) {
        if (getMode()) {
            cout << __LINE__ << "]\t[PixelConfigInterface::forceAliasesReload()]\t\t    No such function implemented yet for DB access" << endl;
        } else {
            pos::PixelConfigFile::forceAliasesReload(mode);
        }
    }
    //==============================================================================
    static std::map<std::string, unsigned int> getAliases_map() {
        std::map<std::string, unsigned int> aliasMap;
        if (getMode()) {
            aliasMap = pixelConfigDB().getAliases_map();
        } else {
            aliasMap = pos::PixelConfigFile::getAliases_map();
        }
        return aliasMap;
    }

    //==============================================================================
    static void addAlias(std::string alias, unsigned int key) {
        if (getMode()) {
            pixelConfigDB().addAlias(alias, key);
        } else {
            pos::PixelConfigFile::addAlias(alias, key);
        }
        return;
    }

    //==============================================================================
    static void addAlias(std::string alias, unsigned int key, std::vector<std::pair<std::string, std::string> > versionaliases) {
        if (getMode()) {
            pixelConfigDB().addAlias(alias, key, versionaliases);
        } else {
            pos::PixelConfigFile::addAlias(alias, key, versionaliases);
        }
        return;
    }

    //==============================================================================
    static void addVersionAlias(std::string path, unsigned int version, std::string alias) {
        if (getMode()) {
            pixelConfigDB().addVersionAlias(path, version, alias);
        } else {
            pos::PixelConfigFile::addVersionAlias(path, version, alias);
        }
        return;
    }

    static void addComment(std::string comment) {
        if (getMode()) {
            pixelConfigDB().addComment(comment);
        } else {
            std::cout << __LINE__ << "]\t[PixelConfigInterface::addComment()]\t\t    Not implemented for file-based repository" << std::endl;
        }
        return;
    }

    static void addAuthor(std::string author) {
        if (getMode()) {
            pixelConfigDB().addAuthor(author);
        } else {
            std::cout << __LINE__ << "]\t[PixelConfigInterface::addAuthor()]\t\t    Not implemented for file-based repository" << std::endl;
        }
        return;
    }

    static unsigned int clone(unsigned int oldkey, std::string path, unsigned int version) {
        unsigned int newkey;
        if (getMode()) {
            newkey = pixelConfigDB().clone(oldkey, path, version);
        } else {
            pos::PixelConfigList iList = pos::PixelConfigFile::getConfig();
            newkey = iList.clone(oldkey, path, version);
            iList.writefile();
        }
        return newkey;
    }

    static unsigned int makeKey(std::vector<std::pair<std::string, unsigned int> > versions) {
        unsigned int newkey;
        if (getMode()) {
            newkey = pixelConfigDB().makeKey(versions);
        } else {
            newkey = pos::PixelConfigFile::makeKey(versions);
        }
        return newkey;
    }

    static unsigned int getVersion(std::string path, std::string alias) {
        if (getMode()) {
            pos::pathVersionAliasMmap vData;
            vData = pixelConfigDB().getVersionData();
            for (pos::pathVersionAliasMmap::iterator it = vData.begin(); it != vData.end(); it++) {
                if (it->first == path) {
                    for (pos::vectorVAPairs::iterator va = it->second.begin(); va != it->second.end(); va++) {
                        if (va->second == alias)
                            return va->first;
                    }
                }
            }
            std::cout << __LINE__ << "]\t[PixelConfigInterface::getVersion(path, alias)]\t Fatal, no version found for path "
                      << "|" << path << "|"
                      << " and alias |" << alias << "|" << std::endl;
            assert(0);
        } else {
            return pos::PixelConfigFile::getVersion(path, alias);
        }
    }

    static std::vector<std::pair<std::string, unsigned int> > getVersions(pos::PixelConfigKey key) {
        std::vector<std::pair<std::string, unsigned int> > versions;
        if (getMode()) {
            versions = pixelConfigDB().getVersions(key);
        } else {
            versions = pos::PixelConfigFile::getVersions(key);
        }
        return versions;
    }

    static std::string uploadStatus(std::string uploadedFile) {
        if (getMode()) {
            return pixelConfigDB().uploadStatus(uploadedFile);
        } else {
            return std::string("true");
        }
    }

    template <class T>
    static int put(T *pixelObject, int configurationFileVersion, std::string path) {
        int retStatus = 0;
        if (getMode()) {
            retStatus = pixelConfigDB().put(pixelObject);
        } else {
            retStatus = put(pixelObject, path);
        }
        return retStatus;
    }

    template <class T>
    static int put(T *pixelObject, std::string label) {
        int retStatus = 0;
        if (getMode()) {
            retStatus = pixelConfigDB().put(pixelObject);
        } else {
            retStatus = pos::PixelConfigFile::put(pixelObject, label);
        }
        return retStatus;
    }

    static std::vector<std::string> getVersionAliases(std::string path) {
        if (getMode()) {
            return pixelConfigDB().getVersionAliases(path);
        } else {
            return pos::PixelConfigFile::getVersionAliases(path);
        }
    }

    template <class T>
    static int put(std::vector<T *> objects, std::string label) {
        int retStatus = 0;
        if (getMode()) {
            retStatus = pixelConfigDB().put(objects);
        } else {
            retStatus = pos::PixelConfigFile::put(objects, label);
        }
        return retStatus;
    }

    template <class T>
    static int put(std::vector<T *> objects, int configurationFileVersion, std::string path) {
        int retStatus = 0;
        if (getMode()) {
            retStatus = pixelConfigDB().put(objects);
        } else {
            retStatus = put(objects, path);
        }
        return retStatus;
    }

    static pos::pathVersionAliasMmap getVersionData() {
        pos::pathVersionAliasMmap vData;
        if (getMode()) {
            vData = pixelConfigDB().getVersionData();
        } else {
            vData = pos::PixelConfigFile::getVersionData();
        }
        return vData;
    }

    static pos::pathVersionAliasMmap getVersionData(string koc) {
        pos::pathVersionAliasMmap vData;
        if (getMode()) {
            vData = pixelConfigDB().getVersionData(koc);
        } else {
            vData = pos::PixelConfigFile::getVersionData(koc);
        }
        return vData;
    }

    static std::set<unsigned int> getExistingVersions(string koc) {
        std::set<unsigned int> result;
        if (getMode()) {
            result = pixelConfigDB().getExistingVersions(koc);
        } else {
            pos::PixelConfigList iList = pos::PixelConfigFile::getConfig();
            for (unsigned int i = 0; i < iList.size(); i++) {
                unsigned int version;
                if (iList[i].find(koc, version) != -1) {
                    result.insert(version);
                }
            }
        }
        return result;
    }

    static std::vector<pos::pathAliasPair> getConfigAliases(std::string path) {
        if (getMode()) {
            assert(0);
        }
        //else {
        return pos::PixelConfigFile::getConfigAliases(path);
        //}
    }

    static bool &getMode() {
        static bool mode = std::string(getenv("PIXELCONFIGURATIONBASE")) == "DB";
        if (mode)
            pixelConfigDB().connect();
        return mode;
    }

    static pos::PixelConfigKey &getGlobalKey() {
        static pos::PixelConfigKey lastUsedKey_(0); //for now make -1 initial value
        return lastUsedKey_;
    }

    static std::string commit(int newKey) {
        if (getMode()) {
            return pixelConfigDB().commitToDB(newKey);
        } else {
            return ("");
        }
    }

  private:
    static PixelConfigDBInterface &pixelConfigDB() {
        static PixelConfigDBInterface aPixelConfigDB;
        return aPixelConfigDB;
    }
};

#endif
