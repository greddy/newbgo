#include "xdaq/exception/Exception.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigKey.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTimeFormatter.h"
#include "PixelConfigDBInterface/include/PixelConfigDBInterface.h"
#include <iomanip>
#include <occi.h>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <set>
#include <algorithm>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

PixelConfigDBInterface::PixelConfigDBInterface() {
    comment_ = "";
}

PixelConfigDBInterface::~PixelConfigDBInterface() {
    ;
}

bool PixelConfigDBInterface::isConnected() {
    return database_.getNumberOfConnections();
}

bool PixelConfigDBInterface::connect() {
    if (!isConnected()) {
        return database_.connect();
    }
    return true;
}

void PixelConfigDBInterface::disconnect() {
    if (isConnected()) {
        database_.disconnect();
    }
}

std::vector<std::vector<std::string> > PixelConfigDBInterface::getTable(std::string path, pos::PixelConfigKey key) {
    std::vector<std::vector<std::string> > databaseTable;

    std::vector<std::string> splitPath = split(path, '/');
    const char *configName = splitPath[1].c_str();
    unsigned int globalKey = key.key();

    if (configHasManyObjects(configName)) {
        if (dataInCache(configName, globalKey)) {
            string objectName = splitPath[2];
            databaseTable = getTableFromCache(configName, objectName);
            return databaseTable;
        }
    }

    PixelSQLCommand *query = makeQueryByKey(configName, globalKey);
    databaseTable = getTableByQuery(*query);
    delete query;

    if (configHasManyObjects(configName)) {
        fillCache(configName, globalKey, databaseTable);
        string objectName = splitPath[2];
        databaseTable = getTableFromCache(configName, objectName);
    }
    return databaseTable;
}

std::vector<std::vector<std::string> > PixelConfigDBInterface::getTable(std::string path, unsigned int version) {
    std::vector<std::vector<std::string> > databaseTable;

    std::vector<std::string> splitPath = split(path, '/');
    const char *configName = splitPath[1].c_str();

    if (configHasManyObjects(configName)) {
        if (dataInCache(configName, version)) {
            string objectName = splitPath[2];
            databaseTable = getTableFromCache(configName, objectName);
            return databaseTable;
        }
    }
    PixelSQLCommand *query = makeQueryByVersion(configName, version);
    databaseTable = getTableByQuery(*query);
    delete query;

    if (configHasManyObjects(configName)) {
        fillCache(configName, version, databaseTable);
        string objectName = splitPath[2];
        databaseTable = getTableFromCache(configName, objectName);
    }
    return databaseTable;
}

PixelSQLCommand *PixelConfigDBInterface::makeQueryByKey(const char *configName, unsigned int globalKey) {
    map<string, string> where;

    stringstream keyToString;
    keyToString << globalKey;

    where["CONFIG_KEY"] = keyToString.str();
    return makeQueryWhere(configName, where);
}

PixelSQLCommand *PixelConfigDBInterface::makeQueryByVersion(const char *configName, unsigned int version) {
    map<string, string> where;

    stringstream keyToString;
    keyToString << version;

    where["VERSION"] = keyToString.str();
    std::string tableName(configName);
    tableName.append("_version");
    return makeQueryWhere(tableName.c_str(), where);
}

PixelSQLCommand *PixelConfigDBInterface::makeQueryWhere(const char *tableName, std::map<std::string, std::string> where) {
    PixelSQLCommand *query = new PixelSQLCommand(database_);
    query->openTable(tableName, where, false, false);
    return query;
}

std::vector<std::vector<std::string> > PixelConfigDBInterface::getTableByQuery(PixelSQLCommand &query) {
    std::vector<std::vector<std::string> > databaseTable;
    databaseTable.push_back(query.getNamesOfColumns());
    while (query.hasNextRow()) {
        databaseTable.push_back(query.getRow());
    }
    return databaseTable;
}

void PixelConfigDBInterface::dumpGeneralTable(vector<vector<string> > &table) {
    for (unsigned int r = 0; r < table.size(); r++) {
        if (table[r].size() == 0) {
            cout << __LINE__ << "]\t"
                 << "__________________ NEW TABLE __________________\n" << endl;
            continue;
        }
        for (vector<string>::iterator it = table[r].begin(); it != table[r].end(); it++) {
            cout << setw(15) << *it << "\t";
        }
        cout << endl;
    }
}

unsigned int PixelConfigDBInterface::getNextAvailableVersion(std::string sequence) {
    PixelSQLCommand query(database_);
    int nextKey = query.getNextAvailableVersion(sequence);
    return nextKey;
}

unsigned int PixelConfigDBInterface::clone(unsigned int oldkey, std::string path, unsigned int version) {
    assert(0);
}

std::vector<std::pair<std::string, unsigned int> > PixelConfigDBInterface::getAliases() {
    PixelSQLCommand query(database_);
    map<string, string> where;
    vector<vector<string> > table;
    std::vector<std::pair<std::string, unsigned int> > result;

    query.openTable("config_keys", where, false, true, "CONFIG_ALIAS", true);

    table = getTableByQuery(query);

    pair<string, unsigned int> tmp;
    for (vector<vector<string> >::iterator it = table.begin() + 1; it != table.end(); it++) {
        tmp.first = it->operator[](3);
        tmp.second = atoi((it->operator[](1)).c_str());
        result.push_back(tmp);
    }
    return result;
}

std::vector<std::pair<std::string, unsigned int> > PixelConfigDBInterface::getVersions(pos::PixelConfigKey key) {
    PixelSQLCommand query(database_);
    map<string, string> where;
    vector<vector<string> > table;
    std::vector<std::pair<std::string, unsigned int> > result;
    stringstream clause;

    fillHashes();
    clause.str("");
    clause << key.key();
    where["KEY_NAME"] = clause.str();
    std::string inHistory("F");
    where["IS_MOVED_TO_HISTORY"] = inHistory;

    query.openTable("dataset_map", where, false, false);

    table = getTableByQuery(query);

    pair<string, unsigned int> tmp;
    for (vector<vector<string> >::iterator it = table.begin() + 1; it != table.end(); it++) {
        tmp.first = conversionKOC[it->operator[](4)];
        tmp.second = atoi((it->operator[](5)).c_str());
        result.push_back(tmp);
    }
    return result;
}

void PixelConfigDBInterface::addAuthor(std::string author) {
    if (author == "")
        author = "Unknown author";
    author_ = author;
}

void PixelConfigDBInterface::addComment(std::string comment) {
    if (comment == "")
        comment = "Tm8gY29tbWVudCB3YXMgc3BlY2lmaWVkLg=="; // base64 for "No comment was specified"
    comment_ = comment;
}

void PixelConfigDBInterface::addAlias(std::string alias, unsigned int key,
                                      std::vector<std::pair<std::string, std::string> > versionaliases) {
    char *cpath = getenv("PIXELCONFIGURATIONSPOOLAREA");
    std::stringstream fullPath;
    fullPath << cpath << "/addAlias_" << pos::PixelTimeFormatter::getmSecTime() << ".xma";
    std::ofstream aliasfile(fullPath.str().c_str());

    fillHashes();

    aliasfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
    aliasfile << "" << endl;
    aliasfile << "<!-- " << __LINE__ << "] " << __PRETTY_FUNCTION__ << " -->" << endl;
    aliasfile << "" << endl;
    aliasfile << "<ROOT>" << endl;
    aliasfile << "  <MAPS>" << endl;
    aliasfile << "   <KEY_ALIAS>" << endl;
    aliasfile << "     <NAME>" << alias << "</NAME>" << endl;
    aliasfile << "       <KEY mode='keep'>" << endl;
    aliasfile << "         <NAME>" << key << "</NAME>" << endl;
    for (unsigned int i = 0; i < versionaliases.size(); i++) {
        aliasfile << endl;
        aliasfile << "         <VERSION_ALIAS>" << endl;
        aliasfile << "           <NAME>" << versionaliases[i].second << "</NAME>" << endl;
        aliasfile << "           <KIND_OF_CONDITION>" << endl;
        aliasfile << "             <EXTENSION_TABLE_NAME>" << conversionETNKOC[versionaliases[i].first] << "</EXTENSION_TABLE_NAME>" << endl;
        aliasfile << "             <NAME>" << conversionNameKOC[versionaliases[i].first] << "</NAME>" << endl;
        aliasfile << "           </KIND_OF_CONDITION>" << endl;
        aliasfile << "         </VERSION_ALIAS>" << endl;
    }

    aliasfile << "    </KEY>" << endl;
    aliasfile << "   </KEY_ALIAS>" << endl;
    aliasfile << "  </MAPS>" << endl;
    aliasfile << "</ROOT>" << endl;
    aliasfile.close();
}

unsigned int PixelConfigDBInterface::makeKey(std::vector<std::pair<std::string, unsigned int> > versions) {
    char *cpath = getenv("PIXELCONFIGURATIONSPOOLAREA");
    std::stringstream fullPath;
    fullPath << cpath << "/makeKey_" << pos::PixelTimeFormatter::getmSecTime() << ".xma";
    std::ofstream aliasfile(fullPath.str().c_str());

    PixelSQLCommand query(database_);
    int nextKey;
    nextKey = query.getNextAvailableVersion("default");
    cout << __LINE__ << "]\t[PixelConfigDBInterface::makeKey()]\t\t    New global key reserved: |" << nextKey << "|" << endl;
    fillHashes();

    aliasfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
    aliasfile << "" << endl;
    aliasfile << "<!-- " << __LINE__ << "] " << __PRETTY_FUNCTION__ << " -->" << endl;
    aliasfile << "" << endl;
    aliasfile << "<ROOT>" << endl;
    aliasfile << "  <MAPS>" << endl;
    aliasfile << "    <KEY>" << endl;
    aliasfile << "      <NAME>" << nextKey << "</NAME>" << endl;
    aliasfile << "      <COMMENT_DESCRIPTION>" << comment_ << "</COMMENT_DESCRIPTION>" << endl;
    aliasfile << "      <CREATED_BY_USER>" << author_ << "</CREATED_BY_USER>" << endl;
    for (unsigned int i = 0; i < versions.size(); i++) {
        aliasfile << endl;
        aliasfile << "      <CONFIGURATION>" << endl;
        aliasfile << "       <PART>" << endl;
        aliasfile << "         <PART_ID>1000</PART_ID>" << endl;
        aliasfile << "       </PART>" << endl;
        aliasfile << "       <KIND_OF_CONDITION>" << endl;
        aliasfile << "           <EXTENSION_TABLE_NAME>" << conversionETNKOC[versions[i].first] << "</EXTENSION_TABLE_NAME>" << endl;
        aliasfile << "           <NAME>" << conversionNameKOC[versions[i].first] << "</NAME>" << endl;
        aliasfile << "       </KIND_OF_CONDITION>" << endl;
        aliasfile << "       <DATA_SET>" << endl;
        aliasfile << "         <VERSION>" << versions[i].second << "</VERSION>" << endl;
        aliasfile << "         <CREATED_BY_USER>" << author_ << "</CREATED_BY_USER>" << endl;
        aliasfile << "         <COMMENT_DESCRIPTION>" << comment_ << "</COMMENT_DESCRIPTION>" << endl;
        aliasfile << "       </DATA_SET>" << endl;
        aliasfile << "    </CONFIGURATION>" << endl;
    }
    aliasfile << "    </KEY>" << endl;
    aliasfile << "  </MAPS>" << endl;
    aliasfile << "</ROOT>" << endl;

    aliasfile.close();

    comment_ = "";

    return nextKey;
}

void PixelConfigDBInterface::addVersionAlias(std::string path, unsigned int version, std::string alias) {
    char *cpath = getenv("PIXELCONFIGURATIONSPOOLAREA");
    std::stringstream fullPath;
    fullPath << cpath << "/addVersionAlias_" << path << "_" << version << pos::PixelTimeFormatter::getmSecTime() << ".xma";
    std::ofstream aliasfile(fullPath.str().c_str());

    fillHashes();

    aliasfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
    aliasfile << "" << endl;
    aliasfile << "<!-- " << __LINE__ << "] " << __PRETTY_FUNCTION__ << " -->" << endl;
    aliasfile << "" << endl;
    aliasfile << "<ROOT>" << endl;
    aliasfile << "  <MAPS>" << endl;
    aliasfile << "    <VERSION_ALIAS>" << endl;
    aliasfile << "      <NAME>" << alias << "</NAME>" << endl;
    aliasfile << "	<CONFIGURATION>" << endl;
    aliasfile << "	  <PART>" << endl;
    aliasfile << "              <PART_ID>1000</PART_ID>" << endl;
    aliasfile << "	  </PART>" << endl;
    aliasfile << "	  <KIND_OF_CONDITION>" << endl;
    aliasfile << "	      <EXTENSION_TABLE_NAME>" << conversionETNKOC[path] << "</EXTENSION_TABLE_NAME>" << endl;
    aliasfile << "	      <NAME>" << conversionNameKOC[path] << "</NAME>" << endl;
    aliasfile << "	  </KIND_OF_CONDITION>" << endl;
    aliasfile << "	  <DATA_SET>" << endl;
    aliasfile << "	    <VERSION>" << version << "</VERSION>" << endl;
    aliasfile << "	  </DATA_SET>" << endl;
    aliasfile << "	</CONFIGURATION>" << endl;
    aliasfile << "    </VERSION_ALIAS>" << endl;
    aliasfile << "  </MAPS>" << endl;
    aliasfile << "</ROOT>" << endl;
    aliasfile.close();
}

void PixelConfigDBInterface::addAlias(std::string alias, unsigned int key) {
    char *cpath = getenv("PIXELCONFIGURATIONSPOOLAREA");
    std::stringstream fullPath;
    fullPath << cpath << "/addAlias_" << pos::PixelTimeFormatter::getmSecTime() << ".xma";
    std::ofstream aliasfile(fullPath.str().c_str());

    fillHashes();

    aliasfile << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << endl;
    aliasfile << "" << endl;
    aliasfile << "<!-- " << __LINE__ << "] " << __PRETTY_FUNCTION__ << " -->" << endl;
    aliasfile << "" << endl;
    aliasfile << "<ROOT>" << endl;
    aliasfile << "  <MAPS>" << endl;
    aliasfile << "   <KEY_ALIAS>" << endl;
    aliasfile << "     <NAME>" << alias << "</NAME>" << endl;
    aliasfile << "       <KEY mode='keep'>" << endl;
    aliasfile << "         <NAME>" << key << "</NAME>" << endl;
    aliasfile << "    </KEY>" << endl;
    aliasfile << "   </KEY_ALIAS>" << endl;
    aliasfile << "  </MAPS>" << endl;
    aliasfile << "</ROOT>" << endl;

    aliasfile.close();
}

std::vector<std::string> PixelConfigDBInterface::getVersionAliases(std::string koc) {

    std::map<std::string, std::vector<std::pair<unsigned int, std::string> > > tmp = getVersionData(koc);
    std::vector<std::string> result;

    for (std::map<std::string, std::vector<std::pair<unsigned int, std::string> > >::iterator it = tmp.begin();
         it != tmp.end(); it++) {
        for (std::vector<std::pair<unsigned int, std::string> >::iterator it2 = it->second.begin();
             it2 != it->second.end(); it2++) {
            result.push_back(it2->second);
        }
    }
    return result;
}

bool PixelConfigDBInterface::getVersionAliases(std::string configAlias,
                                               unsigned int &key,
                                               std::vector<std::pair<std::string, std::string> > &versionAliases) {
    fillHashes();
    PixelSQLCommand query(database_);
    map<string, string> where;
    vector<vector<string> > table;
    std::vector<std::pair<std::string, unsigned int> > result;

    where["CONFIG_ALIAS"] = configAlias;

    query.openTable("config_keys", where, false, false);
    table = getTableByQuery(query);

    if (table.size() == 1)
        return false;
    std::string theKey = table[1][1];
    std::string theTrueKey = table[1][2];
    key = atoi(theKey.c_str());

    table.clear();
    where.clear();

    where["KEY_ALIAS_ID"] = theTrueKey;

    query.openTable("ka_va", where, false, false);
    table = getTableByQuery(query);

    std::pair<std::string, std::string> tmp;
    for (vector<vector<string> >::iterator it = table.begin() + 1; it != table.end(); it++) {
        tmp.first = conversionKOC[it->operator[](8)];
        tmp.second = it->operator[](6);
        versionAliases.push_back(tmp);
    }

    return true;
}

std::map<std::string, unsigned int> PixelConfigDBInterface::getAliases_map() {
    std::map<std::string, unsigned int> result;
    std::vector<std::pair<std::string, unsigned int> > tmp = getAliases();
    for (std::vector<std::pair<std::string, unsigned int> >::iterator it = tmp.begin();
         it != tmp.end(); it++) {
        result[(*it).first] = (*it).second;
    }
    return result;
}

std::map<std::string, std::vector<std::pair<unsigned int, std::string> > > PixelConfigDBInterface::getVersionData() {
    PixelSQLCommand query(database_);
    map<string, string> where;
    vector<vector<string> > table;
    std::map<std::string, std::vector<std::pair<unsigned int, std::string> > > result;

    fillHashes();

    query.openVersionAliasTable();
    table = getTableByQuery(query);
    std::pair<unsigned int, std::string> tmp;
    for (vector<vector<string> >::iterator it = table.begin() + 1; it != table.end(); it++) {
        tmp.first = atoi(it->operator[](1).c_str());
        tmp.second = it->operator[](0);
        result[conversionKOC[it->operator[](2)]].push_back(tmp);
    }

    return result;
}

std::map<std::string, std::vector<std::pair<unsigned int, std::string> > > PixelConfigDBInterface::getVersionData(std::string koc) {
    PixelSQLCommand query(database_);
    map<string, string> where;
    vector<vector<string> > table;
    std::map<std::string, std::vector<std::pair<unsigned int, std::string> > > result;
    fillHashes();

    query.openVersionAliasTable(conversionNameKOC[koc]);
    table = getTableByQuery(query);
    std::pair<unsigned int, std::string> tmp;
    for (vector<vector<string> >::iterator it = table.begin() + 1; it != table.end(); it++) {
        tmp.first = atoi(it->operator[](1).c_str());
        tmp.second = it->operator[](0);

        result[conversionKOC[it->operator[](5)]].push_back(tmp);
    }
    return result;
}

std::string PixelConfigDBInterface::uploadStatus(std::string uploadedFile) {
    std::string mthn("[PixelConfigDBInterface::uploadStatus(string)]\t");

    PixelSQLCommand query(database_);
    map<string, string> where;
    vector<vector<string> > table;
    bool result = 1;

    query.openConditionDataAuditlog(uploadedFile);
    table = getTableByQuery(query);
    if (table.size() == 1)
        return std::string("progress");
    for (vector<vector<string> >::iterator it = table.begin() + 1; it != table.end(); it++) {
        if ((*it)[1].find("Success") != std::string::npos) {
            //result *= 1;
            result = result && true;
        } else {
            //result *= 0;
            result = result && false;
        }
    }

    if (result == 1) {
        for (vector<vector<string> >::iterator it = table.begin() + 1; it != table.end(); it++) {
            if ((*it)[1] == "Success")
                return std::string("true");
        }
        return std::string("progress");
    }

    cout << __LINE__ << mthn << uploadedFile << " " << result << endl;
    return std::string("false");
}

std::set<unsigned int> PixelConfigDBInterface::getExistingVersions(std::string koc) {
    PixelSQLCommand query(database_);
    map<string, string> where;
    vector<vector<string> > table;
    set<unsigned int> result;

    fillHashes();

    query.openExistingVersionsTable(conversionETNKOC[koc]);
    table = getTableByQuery(query);
    std::pair<unsigned int, std::string> tmp;
    for (vector<vector<string> >::iterator it = table.begin() + 1; it != table.end(); it++) {
        result.insert((unsigned int)atoi(it->operator[](0).c_str()));
    }
    return result;
}

void PixelConfigDBInterface::setKeyAlias(pos::PixelConfigKey key, std::string theAlias) {
    PixelSQLCommand query(database_);
    map<string, string> where;
    vector<vector<string> > table;
    std::stringstream whereBuilder;

    whereBuilder << key.key();
    where["CONFIG_KEY"] = whereBuilder.str();
    where["CONFIG_ALIAS"] = theAlias;
    query.openTable("config_keys", where, false, false);
    table = getTableByQuery(query);

    theTrueDBKey_ = table[1][2];
}

void PixelConfigDBInterface::fillHashes() {

    conversionKOC["Ph1 Pixel LTC Configuration Parameters"] = "ltcconfig";
    conversionKOC["Ph1 Pixel Port Card Map"] = "portcardmap";
    conversionKOC["Ph1 Pixel ROC Trims"] = "trim";
    conversionKOC["Ph1 Pixel Detector Configuration"] = "detconfig";
    conversionKOC["Ph1 Pixel Port Card Setting"] = "portcard";
    conversionKOC["Ph1 Pixel FEC Configuration"] = "fecconfig";
    conversionKOC["Ph1 Pixel FED Configuration"] = "fedconfig";
    conversionKOC["Ph1 Pixel Name Translation"] = "nametranslation";
    conversionKOC["Ph1 Pixel TTC Parameters"] = "ttcciconfig";
    conversionKOC["Ph1 Pixel ROC DAC Settings"] = "dac";
    conversionKOC["Ph1 Pixel Tracker FEC Config"] = "tkfecconfig";
    conversionKOC["Ph1 Pixel TBM"] = "tbm";
    conversionKOC["Ph1 Pixel FED Card"] = "fedcard";
    conversionKOC["Calibration Object Clob"] = "calib";
    conversionKOC["Ph1 Pixel ROC Mask Bits"] = "mask";
    conversionKOC["Ph1 Pixel Low Voltage Map"] = "lowvoltagemap";
    conversionKOC["Ph1 Pixel Global Delay25"] = "globaldelay25";
    conversionKOC["Ph1 Pixel AMC13 Settings"] = "amc13";
    conversionKOC["Ph1 Pixel DCDC Configuration"] = "dcdc";
    conversionKOC["Ph1 Pixel Power Map"] = "powermap";

    conversionNameKOC["globaldelay25"] = "Ph1 Pixel Global Delay25";
    conversionNameKOC["lowvoltagemap"] = "Ph1 Pixel Low Voltage Map";
    conversionNameKOC["ltcconfig"] = "Ph1 Pixel LTC Configuration Parameters";
    conversionNameKOC["portcardmap"] = "Ph1 Pixel Port Card Map";
    conversionNameKOC["trim"] = "Ph1 Pixel ROC Trims";
    conversionNameKOC["detconfig"] = "Ph1 Pixel Detector Configuration";
    conversionNameKOC["portcard"] = "Ph1 Pixel Port Card Setting";
    conversionNameKOC["fecconfig"] = "Ph1 Pixel FEC Configuration";
    conversionNameKOC["fedconfig"] = "Ph1 Pixel FED Configuration";
    conversionNameKOC["nametranslation"] = "Ph1 Pixel Name Translation";
    conversionNameKOC["ttcciconfig"] = "Ph1 Pixel TTC Parameters";
    conversionNameKOC["dac"] = "Ph1 Pixel ROC DAC Settings";
    conversionNameKOC["tkfecconfig"] = "Ph1 Pixel Tracker FEC Config";
    conversionNameKOC["tbm"] = "Ph1 Pixel TBM";
    conversionNameKOC["fedcard"] = "Ph1 Pixel FED Card";
    conversionNameKOC["calib"] = "Calibration Object Clob";
    conversionNameKOC["mask"] = "Ph1 Pixel ROC Mask Bits";
    conversionNameKOC["amc13"] = "Ph1 Pixel AMC13 Settings";
    conversionNameKOC["dcdc"] = "Ph1 Pixel DCDC Configuration";
    conversionNameKOC["powermap"] = "Ph1 Pixel Power Map";

    conversionETNKOC["globaldelay25"] = "PH1_PXL_GLOBAL_DELAY25";
    conversionETNKOC["lowvoltagemap"] = "PH1_PXL_LOW_VOLTAGE_MAP";
    conversionETNKOC["ltcconfig"] = "PH1_PXL_LTC_CONFIG";
    conversionETNKOC["portcardmap"] = "PH1_PXL_PORT_CARD_MAP";
    conversionETNKOC["trim"] = "PH1_PXL_ROC_TRIMS";
    conversionETNKOC["detconfig"] = "PH1_PXL_DETECTOR_CONFIG";
    conversionETNKOC["portcard"] = "PH1_PXL_PORT_CARD_SETTING";
    conversionETNKOC["fecconfig"] = "PH1_PXL_PXLFEC_CONFIG";
    conversionETNKOC["fedconfig"] = "PH1_PXL_FED_CONFIG";
    conversionETNKOC["nametranslation"] = "PH1_PXL_NAME_TRANSLATION";
    conversionETNKOC["ttcciconfig"] = "PH1_PXL_TTC_CONFIG";
    conversionETNKOC["dac"] = "PH1_PXL_ROC_DAC_SETTINGS";
    conversionETNKOC["tkfecconfig"] = "PH1_PXL_TKFEC_CONFIG";
    conversionETNKOC["tbm"] = "PH1_PXL_TBM";
    conversionETNKOC["fedcard"] = "PH1_PXL_FED_CARD";
    conversionETNKOC["calib"] = "PIXEL_CALIB_CLOB";
    conversionETNKOC["mask"] = "PH1_PXL_ROC_MASKS";
    conversionETNKOC["amc13"] = "PH1_PXL_AMC13";
    conversionETNKOC["dcdc"] = "PH1_PXL_DCDC_CONF";
    conversionETNKOC["powermap"] = "PH1_PXL_POWER_MAP";
}

std::string PixelConfigDBInterface::commitToDB(unsigned int globalKey) {
    string mthn = "[PixelConfigDBInterface::commitToDB()]\t\t\t    ";
    std::stringstream s;
    int time = 0;      // in seconds
    int maxTime = 300; // in seconds
    int interval = 5;  // in seconds
    std::string timestamp(pos::PixelTimeFormatter::getmSecTime());

    std::string zipperfile;
    if (getenv("BUILD_HOME") == 0) { //for running from RPM
        zipperfile = std::string(getenv("XDAQ_ROOT")) + "/util/PixelConfigDBGUI/util/zipper.pl";
    } else { //for non-rpm running
        zipperfile = std::string(getenv("BUILD_HOME")) + "/pixel/PixelConfigDBGUI/util/zipper.pl";
    }

    s << zipperfile << " " << globalKey << " " << timestamp;

    unsigned int retStatus = system(s.str().c_str());
    if (retStatus != 0) {
        XCEPT_RAISE(xdaq::exception::Exception, "Database error: A fatal error occurred while shipping zip file to final spoolarea");
    }
    s.str("");
    s << "globalKey_" << globalKey << "_" << timestamp << ".zip";
    cout << __LINE__ << mthn << "Querying DB for success [" << time << "/" << maxTime << "]" << endl;
    string result = uploadStatus(s.str());
    cout << __LINE__ << mthn << "Querying DB for success [" << time << "/" << maxTime << "]" << result << endl;

    while (result != "true" && time < maxTime) {
        sleep(interval);
        time += interval;
        result = uploadStatus(s.str());
        cout << __LINE__ << mthn << "Querying DB for success [" << time << "/" << maxTime << "]" << result << endl;
    }

    if (time >= maxTime) {
        XCEPT_RAISE(xdaq::exception::Exception, "Database error: Did not receive success from database within timeout ");
    }
    return s.str();
}

bool PixelConfigDBInterface::dataInCache(string configName, unsigned int globalKey) {
    bool cached = false;
    if (cacheIndex.find(configName) != cacheIndex.end()) {
        if (cacheIndex[configName] == globalKey) {
            cached = true;
        }
    }
    return cached;
}

std::vector<std::vector<std::string> > PixelConfigDBInterface::getTableFromCache(string configName, string objectName) {
    std::vector<std::vector<std::string> > databaseTable;
    if (configName.find("trim") != std::string::npos) {
        if (cacheTrim.find(objectName) != cacheTrim.end())
            databaseTable = cacheTrim[objectName];
    }
    if (configName.find("mask") != std::string::npos) {
        if (cacheMask.find(objectName) != cacheMask.end())
            databaseTable = cacheMask[objectName];
    }
    if (configName.find("dac") != std::string::npos) {
        if (cacheDAC.find(objectName) != cacheDAC.end())
            databaseTable = cacheDAC[objectName];
    }
    if (configName.find("tbm") != std::string::npos) {
        if (cacheTBM.find(objectName) != cacheTBM.end())
            databaseTable = cacheTBM[objectName];
    }
    if (configName.find("portcard") != std::string::npos) {
        if (cachePortcard.find(objectName) != cachePortcard.end())
            databaseTable = cachePortcard[objectName];
    }
    if (configName.find("fedcard") != std::string::npos) {
        if (cacheFEDcard.find(objectName) != cacheFEDcard.end())
            databaseTable = cacheFEDcard[objectName];
    }
    return databaseTable;
}

void PixelConfigDBInterface::resetCache(string configName) {
    if (configName.find("trim") != std::string::npos) {
        cacheTrim.clear();
    }
    if (configName.find("mask") != std::string::npos) {
        cacheMask.clear();
    }
    if (configName.find("dac") != std::string::npos) {
        cacheDAC.clear();
    }
    if (configName.find("tbm") != std::string::npos) {
        cacheTBM.clear();
    }
    if (configName.find("portcard") != std::string::npos) {
        cachePortcard.clear();
    }
    if (configName.find("fedcard") != std::string::npos) {
        cacheFEDcard.clear();
    }
}

void PixelConfigDBInterface::fillCacheIndex(string configName, unsigned int globalKey) {
    cacheIndex[configName] = globalKey;
}

void PixelConfigDBInterface::fillCache(string configName, unsigned int globalKey, vector<vector<string> > &databaseTable) {
    resetCache(configName);
    if (configName.find("trim") != std::string::npos) {
        fillTrimCache(databaseTable);
        fillCacheIndex(configName, globalKey);
    }
    if (configName.find("mask") != std::string::npos) {
        fillMaskCache(databaseTable);
        fillCacheIndex(configName, globalKey);
    }
    if (configName.find("dac") != std::string::npos) {
        fillDACCache(databaseTable);
        fillCacheIndex(configName, globalKey);
    }
    if (configName.find("tbm") != std::string::npos) {
        fillTBMCache(databaseTable);
        fillCacheIndex(configName, globalKey);
    }
    if (configName.find("portcard") != std::string::npos) {
        fillPortcardCache(databaseTable);
        fillCacheIndex(configName, globalKey);
    }
    if (configName.find("fedcard") != std::string::npos) {
        fillFEDcardCache(databaseTable);
        fillCacheIndex(configName, globalKey);
    }
}

void PixelConfigDBInterface::fillTrimCache(vector<vector<string> > &table) {
    std::vector<std::vector<std::string> > emptyTable;
    int rocNameCol = colNumFromName(table[0], "ROC_NAME");
    for (unsigned int i = 1; i < table.size(); i++) {
        std::string rocName = (table[i][rocNameCol]);
        std::string module = moduleNameFromROCName(rocName);
        if (cacheTrim.find(module) != cacheTrim.end())
            cacheTrim[module].push_back(table[i]);
        else {
            cacheTrim[module] = emptyTable;
            cacheTrim[module].push_back(table[0]);
            cacheTrim[module].push_back(table[i]);
        }
    }
}

void PixelConfigDBInterface::fillMaskCache(vector<vector<string> > &table) {
    std::vector<std::vector<std::string> > emptyTable;
    int rocNameCol = colNumFromName(table[0], "ROC_NAME");
    for (unsigned int i = 1; i < table.size(); i++) {
        std::string rocName = (table[i][rocNameCol]);
        std::string module = moduleNameFromROCName(rocName);
        if (cacheMask.find(module) != cacheMask.end())
            cacheMask[module].push_back(table[i]);
        else {
            cacheMask[module] = emptyTable;
            cacheMask[module].push_back(table[0]);
            cacheMask[module].push_back(table[i]);
        }
    }
}

void PixelConfigDBInterface::fillDACCache(vector<vector<string> > &table) {
    std::vector<std::vector<std::string> > emptyTable;
    int rocNameCol = colNumFromName(table[0], "ROC_NAME");
    for (unsigned int i = 1; i < table.size(); i++) {
        std::string rocName = (table[i][rocNameCol]);
        std::string module = moduleNameFromROCName(rocName);
        if (cacheDAC.find(module) != cacheDAC.end())
            cacheDAC[module].push_back(table[i]);
        else {
            cacheDAC[module] = emptyTable;
            cacheDAC[module].push_back(table[0]);
            cacheDAC[module].push_back(table[i]);
        }
    }
}

void PixelConfigDBInterface::fillTBMCache(vector<vector<string> > &table) {
    std::vector<std::vector<std::string> > emptyTable;
    int modNameCol = colNumFromName(table[0], "TBM_NAME");
    for (unsigned int i = 1; i < table.size(); i++) {
        std::string module = (table[i][modNameCol]);
        if (cacheTBM.find(module) != cacheTBM.end())
            cacheTBM[module].push_back(table[i]);
        else {
            cacheTBM[module] = emptyTable;
            cacheTBM[module].push_back(table[0]);
            cacheTBM[module].push_back(table[i]);
        }
    }
}

void PixelConfigDBInterface::fillPortcardCache(vector<vector<string> > &table) {
    std::vector<std::vector<std::string> > emptyTable;
    int prtNameCol = colNumFromName(table[0], "PORT_CARD_NAME");
    for (unsigned int i = 1; i < table.size(); i++) {
        std::string portcard = (table[i][prtNameCol]);
        if (cachePortcard.find(portcard) != cachePortcard.end())
            cachePortcard[portcard].push_back(table[i]);
        else {
            cachePortcard[portcard] = emptyTable;
            cachePortcard[portcard].push_back(table[0]);
            cachePortcard[portcard].push_back(table[i]);
        }
    }
}

void PixelConfigDBInterface::fillFEDcardCache(vector<vector<string> > &table) {
    std::vector<std::vector<std::string> > emptyTable;
    int fedNumCol = colNumFromName(table[0], "FED_NUMBER");
    for (unsigned int i = 1; i < table.size(); i++) {
        std::string fedNumber = (table[i][fedNumCol]);
        if (cacheFEDcard.find(fedNumber) != cacheFEDcard.end())
            cacheFEDcard[fedNumber].push_back(table[i]);
        else {
            cacheFEDcard[fedNumber] = emptyTable;
            cacheFEDcard[fedNumber].push_back(table[0]);
            cacheFEDcard[fedNumber].push_back(table[i]);
        }
    }
}

int PixelConfigDBInterface::colNumFromName(vector<string> columns, string name) {
    unsigned int col = 0;
    for (unsigned int i = 0; i < columns.size(); ++i) {
        if (columns[i] == name) {
            col = i;
            break;
        }
    }
    return col;
}

bool PixelConfigDBInterface::configHasManyObjects(const char *configName) {
    bool many = false;
    if (strcmp(configName, "dac") == 0 ||
        strcmp(configName, "tbm") == 0 ||
        strcmp(configName, "trim") == 0 ||
        strcmp(configName, "mask") == 0 ||
        strcmp(configName, "portcard") == 0 ||
        strcmp(configName, "fedcard") == 0) {
        many = true;
    }
    return many;
}

void PixelConfigDBInterface::dumpTableInfo(const char *table_name, std::map<std::string, std::string> where, bool bForUpdate, bool like) {
    PixelSQLCommand query(database_);
    query.openTable(table_name, where, bForUpdate, like);
    std::cout << __LINE__ << "Summary for Table/View: " << table_name << std::endl;
    query.dumpTableInfo();
}

std::string PixelConfigDBInterface::moduleNameFromROCName(std::string &rocName) {
    std::string module = rocName.replace(rocName.rfind("_ROC"), rocName.length(), "");
    return module;
}
