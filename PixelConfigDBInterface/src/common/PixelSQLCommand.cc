#include "PixelConfigDBInterface/include/PixelSQLCommand.h"
#include "PixelConfigDBInterface/include/PixelOracleDatabase.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdlib.h>
#include <cstring>

using namespace std;
using namespace oracle::occi;

PixelSQLCommand::PixelSQLCommand(PixelOracleDatabase *pdb) {
    myPdb_ = pdb;
    init();
}

PixelSQLCommand::PixelSQLCommand(PixelOracleDatabase &db) {
    myPdb_ = &db;
    init();
}

void PixelSQLCommand::init() {
    connection_ = myPdb_->getConnection();
    statement_ = 0;
    result_ = 0;
    m_bError = 0;
    load_variable("sql_connection_dev", tnsOracleName_);
    for (std::string::iterator i = tnsOracleName_.begin(); i != tnsOracleName_.end(); ++i)
        *i = tolower(*i);
    fillViewDictionary();
}

void PixelSQLCommand::fillViewDictionary() {
    viewDictionary_["globaldelay25"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_GLOBAL_DELAY25_V";
    viewDictionary_["lowvoltagemap"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_LOW_VOLTAGE_V";
    viewDictionary_["ltcconfig"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_LTC_CONFIG_V";
    viewDictionary_["portcardmap"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_PORTCARD_MAP_V";
    viewDictionary_["trim"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_ROC_TRIMS_V";
    viewDictionary_["detconfig"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_DET_CONFIG_V";
    viewDictionary_["portcard"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_PORTCARD_V";
    viewDictionary_["fecconfig"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_PXLFEC_CONFIG_V";
    viewDictionary_["fedconfig"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_FED_CONFIG_V";
    viewDictionary_["nametranslation"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_NAME_TRANS_V";
    viewDictionary_["ttcciconfig"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_TTC_CONFIG_V";
    viewDictionary_["dac"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_ROCDAC_V";
    viewDictionary_["tkfecconfig"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_TKFEC_CONFIG_V";
    viewDictionary_["tbm"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_PIXEL_TBM_V";
    viewDictionary_["fedcard"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_FED_CARD_V";
    viewDictionary_["amc13"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_AMC13_V";
    viewDictionary_["dcdc"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_DCDC_V";
    viewDictionary_["powermap"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_POWER_MAP_V";
    viewDictionary_["calib"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PIXEL_CALIB_V";
    viewDictionary_["mask"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_PH1_ROC_MASKS_V";

    viewDictionary_["globaldelay25_version"] = "CMS_PXL_PIXEL_VIEW.PH1_GLOBAL_DELAY25_VERSIONS_V";
    viewDictionary_["detconfig_version"] = "CMS_PXL_PIXEL_VIEW.PH1_DET_CONFIG_VERSIONS_V";
    viewDictionary_["dac_version"] = "CMS_PXL_PIXEL_VIEW.PH1_ROCDAC_VERSIONS_V";
    viewDictionary_["portcard_version"] = "CMS_PXL_PIXEL_VIEW.PH1_PORTCARD_VERSIONS_V";
    viewDictionary_["tbm_version"] = "CMS_PXL_PIXEL_VIEW.PH1_PIXEL_TBM_VERSIONS_V";
    viewDictionary_["lowvoltagemap_version"] = "CMS_PXL_PIXEL_VIEW.PH1_LOW_VOLTAGE_VERSIONS_V";
    viewDictionary_["ltcconfig_version"] = "CMS_PXL_PIXEL_VIEW.PH1_LTC_CONFIG_VERSIONS_V";
    viewDictionary_["portcardmap_version"] = "CMS_PXL_PIXEL_VIEW.PH1_PORTCARD_MAP_VERSIONS_V";
    viewDictionary_["trim_version"] = "CMS_PXL_PIXEL_VIEW.PH1_ROC_TRIMS_VERSIONS_V";
    viewDictionary_["fecconfig_version"] = "CMS_PXL_PIXEL_VIEW.PH1_PXLFEC_CONFIG_VERSIONS_V";
    viewDictionary_["fedconfig_version"] = "CMS_PXL_PIXEL_VIEW.PH1_FED_CONFIG_VERSIONS_V";
    viewDictionary_["nametranslation_version"] = "CMS_PXL_PIXEL_VIEW.PH1_NAME_TRANS_VERSIONS_V";
    viewDictionary_["ttcciconfig_version"] = "CMS_PXL_PIXEL_VIEW.PH1_TTC_CONFIG_VERSIONS_V";
    viewDictionary_["tkfecconfig_version"] = "CMS_PXL_PIXEL_VIEW.PH1_TKFEC_CONFIG_VERSIONS_V";
    viewDictionary_["fedcard_version"] = "CMS_PXL_PIXEL_VIEW.PH1_FED_CARD_VERSIONS_V";
    viewDictionary_["amc13_version"] = "CMS_PXL_PIXEL_VIEW.PH1_AMC13_VERSIONS_V";
    viewDictionary_["dcdc_version"] = "CMS_PXL_PIXEL_VIEW.PH1_DCDC_VERSIONS_V";
    viewDictionary_["powermap_version"] = "CMS_PXL_PIXEL_VIEW.PH1_POWER_MAP_VERSIONS_V";
    viewDictionary_["calib_version"] = "CMS_PXL_PIXEL_VIEW.PIXEL_CALIB_VERSIONS_V";
    viewDictionary_["mask_version"] = "CMS_PXL_PIXEL_VIEW.PH1_ROC_MASKS_VERSIONS_V";

    viewDictionary_["config_keys"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_CONFIG_KEYS_V";
    viewDictionary_["dataset_map"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_DATASET_MAP_V";
    viewDictionary_["ka_va"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_ALIAS_VERSION_ALIAS_T";
    viewDictionary_["conf_given_koc"] = "CMS_PXL_PIXEL_VIEW.CONF_KEY_KINDS_OF_COND_V";
    viewDictionary_["auditlog"] = "CMS_PXL_CORE_MANAGEMNT.CONDITIONS_DATA_AUDITLOG";
    viewDictionary_["ver_alias"] = "CMS_PXL_CORE_IOV_MGMNT.CONFIG_VERSION_ALIASES";
    viewDictionary_["ver_alias_map"] = "CMS_PXL_CORE_IOV_MGMNT.CONFIG_VERSION_ALIAS_MAPS";
    viewDictionary_["sequence"] = "CMS_PXL_CORE_IOV_MGMNT.get_sequence_nextval";
    viewDictionary_["dataset"] = "CMS_PXL_CORE_COND.COND_DATA_SETS";
    viewDictionary_["koc"] = "CMS_PXL_CORE_COND.KINDS_OF_CONDITIONS";
}

PixelSQLCommand::~PixelSQLCommand() {
    terminateStatement();
    myPdb_->releaseConnection(connection_);
}

void PixelSQLCommand::createStatement() {
    if (!statement_) {
        statement_ = connection_->createStatement();
    }
}

void PixelSQLCommand::terminateStatement() {
    if (statement_) {
        connection_->terminateStatement(statement_);
        statement_ = 0;
    }
}

void PixelSQLCommand::reCreateStatement() {
    terminateStatement();
    createStatement();
}

PixelSQLCommand &PixelSQLCommand::setSql() {
    if (statement_) {
        statement_->setSQL(command_.str());
        command_.str("");
    }
    return *this;
}

PixelSQLCommand &PixelSQLCommand::commit() {
    if (myPdb_ && connection_)
        connection_->commit();
    return *this;
}

PixelSQLCommand &PixelSQLCommand::exec(bool bAutoCommit) {
    try {
        if (command_.str() != "") {
            result_ = statement_->executeQuery(command_.str());
        } else
            result_ = statement_->executeQuery();
        if (bAutoCommit)
            commit();
    }
    catch (oracle::occi::SQLException& e) {
        std::cout << __LINE__ << "]\t[PixelSQLCommand::exec()]\t\t\t\t    " << command_.str() << std::endl;
        std::cout << __LINE__ << "]\t[PixelSQLCommand::exec()]\t\t\t\t    " << statement_ << std::endl;
        std::cout << __LINE__ << "]\t[PixelSQLCommand::exec()]\t\t\t\t    " << result_ << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::exec()]\t\t\t\t    " << e.getMessage() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::exec()]\t\t\t\t    when trying to execute: " << command_.str() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::exec()]\t\t\t\t    rethrowing exception..." << std::endl;
        throw;
    }
    command_.str("");
    return *this;
}

PixelSQLCommand &PixelSQLCommand::execUpdate() {
    try {
        if (command_.str() != "")
            statement_->executeUpdate(command_.str());
        else
            statement_->executeUpdate();
        connection_->commit();
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::execUpdate()]\t\t    " << e.getMessage() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::execUpdate()]\t\t    when trying to execute: " << command_.str() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::execUpdate()]\t\t    rethrowing exception..." << std::endl;
        throw;
    }
    command_.str("");

    return *this;
}

PixelSQLCommand &PixelSQLCommand::startOver() {
    command_.str("");
    return *this;
}

PixelSQLCommand &PixelSQLCommand::openTable(const char *table_name,
                                            std::map<std::string, std::string> &where,
                                            bool bForUpdate,
                                            bool like,
                                            string orderByColumn,
                                            bool ordered) {
    startOver();

    if (where.empty()) {
        command_ << "SELECT * FROM " << viewDictionary_[table_name];
        if (ordered) {
            command_ << " ORDER BY " << orderByColumn;
        }
        reCreateStatement();
    } else {
        command_ << "SELECT * FROM " << viewDictionary_[table_name];
        if (!like) {
            int i = 1;
            for (map<string, string>::iterator it = where.begin(); it != where.end(); it++) {
                if (it == where.begin()) {
                    command_ << " WHERE " << it->first << "=:" << i;
                } else {
                    command_ << " and " << it->first << "=:" << i;
                }
                i++;
            }
            if (ordered) {
                command_ << " ORDER BY " << orderByColumn;
            }

            reCreateStatement();
            setSql();

            int j = 1;
            for (map<string, string>::iterator it2 = where.begin(); it2 != where.end(); it2++) {
                setField(j, it2->second);
                j++;
            }
        } else {
            for (map<string, string>::iterator it = where.begin(); it != where.end(); it++) {
                if (it == where.begin()) {
                    command_ << " WHERE " << it->first << " like "
                             << "'%" << it->second << "%'";
                } else {
                    command_ << " and " << it->first << " like "
                             << "'%" << it->second << "%'";
                }
            }
            if (ordered) {
                command_ << " ORDER BY " << orderByColumn;
            }
            reCreateStatement();
        }
    }

    if (bForUpdate) {
        command_ << " FOR UPDATE ";
    }

    bool bAutoCommit = !bForUpdate;
    setPrefetch(50000);
    exec(bAutoCommit);

    return *this;
}

int PixelSQLCommand::hasNextRow() {
    int returnValue;
    if (result_) {
        returnValue = result_->next();
        return returnValue;
    } else {
        return false;
    }
}

vector<std::string> PixelSQLCommand::getRow() {
    std::vector<string> rowVector;
    int nCol = getNumberOfColumns();

    setPrefetch(50000);
    for (int c = 1; c <= nCol; c++) {
        if (metadata_[c - 1].getInt(MetaData::ATTR_DATA_TYPE) == OCCI_SQLT_CLOB) {
            rowVector.push_back(getAndDecodeClob(c));
        } else {
            rowVector.push_back(getStringField(c));
        }
    }
    return rowVector;
}

std::string PixelSQLCommand::getAndDecodeClob(int index) {
    oracle::occi::Clob clob;
    getField(index, clob);
    oracle::occi::Stream *stream = clob.getStream(1, 0);
    char *buffer = (char *)malloc(clob.length() * sizeof(char) + 1);
    memset(buffer, 0, clob.length());
    buffer[clob.length()] = '\000';
    stream->readBuffer(buffer, clob.length());

    return buffer;
}

unsigned int PixelSQLCommand::getNumArrayRows() {
    if (result_) {
        return result_->getNumArrayRows();
    } else {
        return 0;
    }
}

void PixelSQLCommand::setPrefetch(unsigned int prefetch) {
    if (!statement_) {
        createStatement();
    }

    statement_->setPrefetchRowCount(20000);
    statement_->setPrefetchMemorySize(2000000); // 1 Megabyte (in bytes)!!!
}

void PixelSQLCommand::getField(int index, std::string &str) {
    try {
        if (result_) {
            str = result_->getString(index);
        }
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getField()]\t\t\t    " << e.getMessage() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getField()]\t\t\t    when trying to execute PixelSQLCommand::getString(int index) " << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getField()]\t\t\t    rethrowing exception..." << std::endl;
    }
}

void PixelSQLCommand::getField(int index, int &i) {
    try {
        if (result_) {
            i = result_->getInt(index);
        }
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getField()]\t\t\t    " << e.getMessage() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getField()]\t\t\t    when trying to execute PixelSQLCommand::getInt(int index) " << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getField()]\t\t\t    rethrowing exception..." << std::endl;
    }
}

std::string PixelSQLCommand::getStringField(int index) {
    try {
        if (result_) {
            if (metadata_[index - 1].getInt(MetaData::ATTR_DATA_TYPE) == OCCI_SQLT_TIMESTAMP_TZ) {
                return result_->getTimestamp(index).toText(string("MM/DD/IYYY HH24:MI:SS"), 2);
            } else {
                return result_->getString(index);
            }
        }
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getStringField()]\t\t\t    " << e.getMessage() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getStringField()]\t\t\t    when trying to execute PixelSQLCommand::getString(int index) " << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getStringField()]\t\t\t    rethrowing exception..." << std::endl;
    }
    return "";
}

int PixelSQLCommand::getIntField(int index) {
    try {
        if (result_) {
            return result_->getInt(index);
        } else {
            return -1;
        }
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getIntField()]\t\t\t    " << e.getMessage() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getIntField()]\t\t\t    when trying to execute PixelSQLCommand::getInt(int index) " << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getIntField()]\t\t\t    rethrowing exception..." << std::endl;
    }
    return 0;
}

void PixelSQLCommand::getField(int index, oracle::occi::Clob &clob) {
    try {
        if (result_) {
            clob = result_->getClob(index);
        }
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getField()]\t\t\t    " << e.getMessage() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getField()]\t\t\t    when trying to execute PixelSQLCommand::getClob( int index) " << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::getField()]\t\t\t    rethrowing exception..." << std::endl;
    }
}

void PixelSQLCommand::setField(int index, std::string &str) {
    try {
        if (statement_) {
            statement_->setString(index, str);
        }
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::setField()]\t\t\t    " << e.getMessage() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::setField()]\t\t\t    when trying to execute PixelSQLCommand::setString( int index , string str) " << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::setField()]\t\t\t    rethrowing exception..." << std::endl;
    }
}

void PixelSQLCommand::setField(int index, int i) {
    try {
        if (statement_) {
            statement_->setInt(index, i);
        }
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::setField()]\t\t\t    " << e.getMessage() << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::setField()]\t\t\t    when trying to execute PixelSQLCommand::setInt( int index , int i) " << std::endl;
        std::cerr << __LINE__ << "]\t[PixelSQLCommand::setField()]\t\t\t    rethrowing exception..." << std::endl;
    }
}

PixelSQLCommand &exec(PixelSQLCommand &sql_cmd) {
    return sql_cmd.exec();
}

PixelSQLCommand &execWithoutCommit(PixelSQLCommand &sql_cmd) {
    return sql_cmd.exec(false);
}

PixelSQLCommand &execUpdate(PixelSQLCommand &sql_cmd) {
    return sql_cmd.execUpdate();
}

PixelSQLCommand &startOver(PixelSQLCommand &sql_cmd) {
    return sql_cmd.startOver();
}

PixelSQLCommand &setSql(PixelSQLCommand &sql_cmd) {
    return sql_cmd.setSql();
}

int PixelSQLCommand::getNextAvailableVersion(std::string sequence) {
    startOver();
    command_ << "select " << viewDictionary_["sequence"] << "('" << sequence << "') as val from dual";
    reCreateStatement();

    setSql();
    exec(true); //execute procedure

    hasNextRow();
    int offSet = 200000;
    int tmp = getIntField(1);

    return tmp + offSet;
}

int PixelSQLCommand::getNumberOfColumns() {
    metadata_.clear();
    metadata_ = result_->getColumnListMetaData();
    return (int)metadata_.size();
}

void PixelSQLCommand::dumpTableInfo() {

    metadata_.clear();
    metadata_ = result_->getColumnListMetaData();
    for (unsigned int i = 0; i < metadata_.size(); i++) {
        cout << " Column Name: " << setw(30) << metadata_[i].getString(MetaData::ATTR_NAME);
        cout << "\tData Type:  " << (printType(metadata_[i].getInt(MetaData::ATTR_DATA_TYPE))) << endl;
    }
}

std::string PixelSQLCommand::getNameOfColumns(int index) {
    string returnString = "";
    try {
        metadata_.clear();
        metadata_ = result_->getColumnListMetaData();
        MetaData mv = metadata_[index - 1];
        returnString = mv.getString(MetaData::ATTR_NAME);
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << e.getMessage() << std::endl;
        std::cerr << "when trying to get name of columns " << std::endl;
    }
    return returnString;
}

vector<std::string> PixelSQLCommand::getNamesOfColumns() {
    vector<string> returnVector;
    try {
        metadata_.clear();
        metadata_ = result_->getColumnListMetaData();
        for (unsigned int j = 0; j < metadata_.size(); j++) {
            returnVector.push_back(metadata_[j].getString(MetaData::ATTR_NAME));
        }
    }
    catch (oracle::occi::SQLException& e) {
        std::cerr << e.getMessage() << std::endl;
        std::cerr << "when trying to get name of columns " << std::endl;
    }
    return returnVector;
}

void PixelSQLCommand::setDataBuffer(int index, void *buffer, sb4 size, ub2 *length, sb2 *ind = NULL, ub2 *rc = NULL) {
    result_->setDataBuffer(index, buffer, OCCI_SQLT_STR, size, length, ind, rc);
}

PixelSQLCommand &PixelSQLCommand::openConditionDataAuditlog(std::string fileToSearchFor) {
    startOver();

    command_ << "SELECT C.ARCHVE_FILE_NAME,";
    command_ << "       C.UPLOAD_STATUS  ";
    command_ << " FROM  " << viewDictionary_["auditlog"] << " C ";
    command_ << " WHERE C.ARCHVE_FILE_NAME LIKE  '" << fileToSearchFor << "%'";

    std::cout << command_.str() << std::endl;
    reCreateStatement();
    setSql();
    setPrefetch(50000);
    exec(true);
    return *this;
}

PixelSQLCommand &PixelSQLCommand::openVersionAliasTable() {
    startOver();

    command_ << "SELECT AL_VERS.NAME VERSION_ALIAS,";
    command_ << "      CONDDS.VERSION,";
    command_ << "      KOC.NAME KIND_OF_CONDITION";
    command_ << " FROM  " << viewDictionary_["ver_alias"] << " AL_VERS";
    command_ << " INNER JOIN " << viewDictionary_["ver_alias_map"] << "  AL_MAP";
    command_ << "	   ON AL_MAP.CONFIG_VERSION_ALIAS_ID = AL_VERS.CONFIG_VERSION_ALIAS_ID";
    command_ << " INNER JOIN " << viewDictionary_["dataset"] << "  CONDDS";
    command_ << "    ON CONDDS.CONDITION_DATA_SET_ID = AL_MAP.CONDITION_DATA_SET_ID";
    command_ << " INNER JOIN " << viewDictionary_["koc"] << " KOC";
    command_ << "	   ON CONDDS.KIND_OF_CONDITION_ID = KOC.KIND_OF_CONDITION_ID";
    command_ << "	 WHERE CONDDS.IS_RECORD_DELETED = 'F'";
    command_ << "   AND KOC.IS_RECORD_DELETED = 'F'";
    command_ << "	  AND AL_MAP.IS_MOVED_TO_HISTORY = 'F'";
    command_ << "	  AND AL_VERS.IS_RECORD_DELETED = 'F'";
    command_ << " ORDER BY KIND_OF_CONDITION,";
    command_ << "          VERSION_ALIAS,";
    command_ << "          VERSION";

    reCreateStatement();
    setSql();
    setPrefetch(50000);
    exec(true);

    return *this;
}

PixelSQLCommand &PixelSQLCommand::openVersionAliasTable(string koc) {
    startOver();

    command_ << "SELECT AL_VERS.NAME VERSION_ALIAS,";
    command_ << "      CONDDS.VERSION,";
    command_ << "      CONDDS.COMMENT_DESCRIPTION,";
    command_ << "      CONDDS.RECORD_INSERTION_TIME,";
    command_ << "      CONDDS.CREATED_BY_USER,";
    command_ << "      KOC.NAME KIND_OF_CONDITION";
    command_ << " FROM  " << viewDictionary_["ver_alias"] << " AL_VERS";
    command_ << " INNER JOIN " << viewDictionary_["ver_alias_map"] << " AL_MAP";
    command_ << "	   ON AL_MAP.CONFIG_VERSION_ALIAS_ID = AL_VERS.CONFIG_VERSION_ALIAS_ID";
    command_ << " INNER JOIN " << viewDictionary_["dataset"] << " CONDDS";
    command_ << "    ON CONDDS.CONDITION_DATA_SET_ID = AL_MAP.CONDITION_DATA_SET_ID";
    command_ << " INNER JOIN " << viewDictionary_["koc"] << " KOC";
    command_ << "	   ON CONDDS.KIND_OF_CONDITION_ID = KOC.KIND_OF_CONDITION_ID";
    command_ << "	 WHERE CONDDS.IS_RECORD_DELETED = 'F'";
    command_ << "   AND KOC.IS_RECORD_DELETED = 'F'";
    command_ << "	  AND AL_MAP.IS_MOVED_TO_HISTORY = 'F'";
    command_ << "	  AND AL_VERS.IS_RECORD_DELETED = 'F'";
    command_ << "   AND KOC.NAME LIKE '%" << koc << "%'";
    command_ << " ORDER BY KIND_OF_CONDITION,";
    command_ << "          VERSION_ALIAS,";
    command_ << "          VERSION";

    reCreateStatement();
    setSql();
    setPrefetch(50000);
    exec(true);
    return *this;
}

PixelSQLCommand &PixelSQLCommand::openKeyAliasKeyVersions() {
    startOver();
    command_ << "select " << viewDictionary_["ka_va"] << ".key_name     	   gk,";
    command_ << "       " << viewDictionary_["ka_va"] << ".key_alias     	   key_alias,";
    command_ << "       " << viewDictionary_["ka_va"] << ".version_alias 	   va,";
    command_ << "       " << viewDictionary_["ka_va"] << ".comment_description cd,";
    command_ << "       " << viewDictionary_["ka_va"] << ".insert_time  	   it,";
    command_ << "       " << viewDictionary_["ka_va"] << ".author       	   au,";
    command_ << "       " << viewDictionary_["ka_va"] << ".kind_of_condition   koc";
    command_ << "       from " << viewDictionary_["ka_va"];
    command_ << "       order by gk ASC";

    reCreateStatement();
    setSql();
    setPrefetch(50000);
    exec(true);
    return *this;
}

PixelSQLCommand &PixelSQLCommand::openExistingVersionsTable(string koc) {
    startOver();

    command_ << "select distinct version from " << viewDictionary_["dataset"] << " where extension_table_name = '";
    command_ << koc << "' and is_record_deleted = 'F'";
    reCreateStatement();
    setSql();
    setPrefetch(50000);
    exec(true);
    return *this;
}

string PixelSQLCommand::printType(int type) {
    switch (type) {
    case OCCI_SQLT_CHR:
        return "VARCHAR2";
        break;
    case OCCI_SQLT_NUM:
        return "NUMBER";
        break;
    case OCCIINT:
        return "INTEGER";
        break;
    case OCCIFLOAT:
        return "FLOAT";
        break;
    case OCCI_SQLT_STR:
        return "STRING";
        break;
    case OCCI_SQLT_VNU:
        return "VARNUM";
        break;
    case OCCI_SQLT_LNG:
        return "LONG";
        break;
    case OCCI_SQLT_VCS:
        return "VARCHAR";
        break;
    case OCCI_SQLT_RID:
        return "ROWID";
        break;
    case OCCI_SQLT_DAT:
        return "DATE";
        break;
    case OCCI_SQLT_VBI:
        return "VARRAW";
        break;
    case OCCI_SQLT_BIN:
        return "RAW";
        break;
    case OCCI_SQLT_LBI:
        return "LONG RAW";
        break;
    case OCCIUNSIGNED_INT:
        return "UNSIGNED INT";
        break;
    case OCCI_SQLT_LVC:
        return "LONG VARCHAR";
        break;
    case OCCI_SQLT_LVB:
        return "LONG VARRAW";
        break;
    case OCCI_SQLT_AFC:
        return "CHAR";
        break;
    case OCCI_SQLT_AVC:
        return "CHARZ";
        break;
    case OCCI_SQLT_RDD:
        return "ROWID";
        break;
    case OCCI_SQLT_NTY:
        return "NAMED DATA TYPE";
        break;
    case OCCI_SQLT_REF:
        return "REF";
        break;
    case OCCI_SQLT_CLOB:
        return "CLOB";
        break;
    case OCCI_SQLT_BLOB:
        return "BLOB";
        break;
    case OCCI_SQLT_FILE:
        return "BFILE";
        break;
    case OCCI_SQLT_TIMESTAMP_LTZ:
        return "TIMESTAMP WITH LOCAL TIMEZONE";
        break;
    case OCCI_SQLT_TIMESTAMP:
        return "TIMESTAMP";
        break;
    case OCCI_SQLT_TIMESTAMP_TZ:
        return "TIMESTAMP WITH TIMEZONE";
        break;
    }
    return "Unknown data type";
}
