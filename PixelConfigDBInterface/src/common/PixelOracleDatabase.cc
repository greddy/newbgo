#include <fstream>
#include <iostream>

#include "PixelConfigDBInterface/include/PixelOracleDatabase.h"
#include "PixelConfigDBInterface/include/PixelDatabaseUtilities.h"

using namespace std;
using namespace oracle::occi;

Environment *PixelOracleDatabase::environment_ = 0;
StatelessConnectionPool *PixelOracleDatabase::connectionPool_ = 0;
int PixelOracleDatabase::numberOfConnects_ = 0;

PixelOracleDatabase::PixelOracleDatabase() {
    connections_ = 0;
    pthread_mutex_t lock = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
    environment_ = 0;
    connectionPool_ = 0;
    mutexLock_ = lock;
    numberOfConnects_ = 0;
}

PixelOracleDatabase::~PixelOracleDatabase() {
    disconnect();
}

bool PixelOracleDatabase::connect() {
    std::string mthn = "]\t[PixelOracleDatabase::connect()]\t\t\t    ";
    //cout << __LINE__ << mthn << endl;
    pthread_mutex_lock(&mutexLock_);
    if (numberOfConnects_ == 0) {
        string connection;
        string username;
        string password;
        load_variable("sql_connection_dev", connection);
        load_variable("sql_username_dev", username);
        load_variable("sql_password_dev", password);
        //cout << __LINE__ << mthn << "Connection Information:"       << endl;
        //cout << __LINE__ << mthn << "  connection = " << connection << endl;
        //cout << __LINE__ << mthn << "  username   = " << username   << endl;
        //cout << __LINE__ << mthn << "Connecting..."  	       << endl;
        environment_ = Environment::createEnvironment(Environment::THREADED_UNMUTEXED);
        //cout << __LINE__ << mthn << "Environment Created"	       << endl;
        bool timeout = false;
        while (!timeout) {
            try {
                connectionPool_ = environment_->createStatelessConnectionPool(username, password, connection, 10, 1, 1, StatelessConnectionPool::HOMOGENEOUS);
                timeout = true;
            }
            catch (SQLException& sqle) {
                std::cout << __LINE__ << mthn << sqle.getMessage() << std::endl;
            }
        }
        connectionPool_->setBusyOption(StatelessConnectionPool::NOWAIT);
    }

    numberOfConnects_++;
    pthread_mutex_unlock(&mutexLock_);
    return true;
}

Connection *PixelOracleDatabase::getConnection() {
    return connectionPool_->getConnection();
}

void PixelOracleDatabase::releaseConnection(Connection *conn) {
    connectionPool_->releaseConnection(conn);
}

void PixelOracleDatabase::disconnect() {
    if (connections_) {
        this->releaseConnection(connections_);
        connections_ = 0;
    }
    pthread_mutex_lock(&mutexLock_);
    if (--numberOfConnects_ == 0) {
        if (environment_) {
            Environment::terminateEnvironment(environment_);
            environment_ = 0;
        }
    }
    pthread_mutex_unlock(&mutexLock_);
}
