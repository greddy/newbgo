#!/usr/bin/perl

$source = "ext-base.js" ;
$dest   = "ext-base-hacked.js" ;

open(IN,  "$source") ;
open(OUT, ">$dest" ) ;

while(<IN>) 
{
 s/,/,\n/g ;
# s/\{/{\n/g ;
# s/\}/}\n/g ;
 s/\;/;\n/g ;
 print OUT ;
}

close(IN) ;
close(OUT) ;
