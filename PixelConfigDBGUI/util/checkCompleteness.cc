#include "CalibFormats/SiPixelObjects/interface/PixelConfigBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigAlias.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelVersionAlias.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigKey.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTrimBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTrimAllPixels.h"
#include "CalibFormats/SiPixelObjects/interface/PixelMaskBase.h"
#include "CalibFormats/SiPixelObjects/interface/PixelMaskAllPixels.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDACSettings.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTBMSettings.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDetectorConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelLowVoltageMap.h"
#include "CalibFormats/SiPixelObjects/interface/PixelMaxVsf.h"
#include "CalibFormats/SiPixelObjects/interface/PixelNameTranslation.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFEDCard.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortCardConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelPortcardMap.h"
#include "CalibFormats/SiPixelObjects/interface/PixelDelay25Calib.h"
#include "CalibFormats/SiPixelObjects/interface/PixelROCName.h"
#include "CalibFormats/SiPixelObjects/interface/PixelModuleName.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFECConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTKFECConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFEDConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelTTCciConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelLTCConfig.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFEDTestDAC.h"
#include "CalibFormats/SiPixelObjects/interface/PixelGlobalDelay25.h"

#include <string>
#include "CompositeEquipment.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "mcheck.h"
#include <sys/time.h>
#include <stdlib.h>
#include <iomanip>
#include <algorithm>

PixelConfigInterface Iconfig_;
pos::PixelConfigList Ilist_;
CompositeEquipment *bSectors_;
CompositeEquipment *fDisks_;
CompositeEquipment *theROCs_;

map<string, vector<string> > missingComponents;
map<string, map<string, string> > commentToMissingComponent;

PixelTracker *buildTracker();
int getVersion(pos::PixelConfigKey *theKey, std::string koc);
void endJob(struct timeval &start_time);
CompositeEquipment *buildFeds(void);
void dumpAnalysisResults(int, string);

int main(int argc, char *argv[]) {
    //  mtrace() ;
    pos::PixelCalibConfiguration *calib_ = NULL;
    pos::PixelDelay25Calib *calib25_ = NULL;
    pos::PixelDetectorConfig *detconfig_ = NULL;
    pos::PixelFECConfig *FECConfig_ = NULL;
    pos::PixelFEDConfig *FEDConfig_ = NULL;
    pos::PixelLowVoltageMap *LVMap_ = NULL;
    pos::PixelNameTranslation *nameTranslationModule_ = NULL;
    pos::PixelPortcardMap *portCardMap_ = NULL;
    pos::PixelTKFECConfig *TKFECConfig_ = NULL;
    pos::PixelTTCciConfig *ttc_ = NULL;
    pos::PixelLTCConfig *ltc_ = NULL;
    pos::PixelMaxVsf *maxvsf_ = NULL;
    pos::PixelGlobalDelay25 *globaldelay25_ = NULL;

    pos::PixelConfigKey *theGlobalKey_ = NULL;

    std::vector<std::string> modList;
    std::map<std::string, std::string> commentMap;
    map<string, bool> consistencyVerified;

    theROCs_ = new CompositeEquipment("theROCs", 15840);
    string user(getenv("USER"));
    string comment("Initial upload from file-based to DB repository.");
    unsigned int version;

    std::string mthn = "]\t[checkCompleteness::main()]\t\t\t\t\t    ";
    struct timeval start_time;

    if (argc < 2) {
        cout << __LINE__ << mthn << "Usage: " << endl << endl;
        cout << __LINE__ << mthn << "./checkCompleteness GK KOC" << endl;
        cout << __LINE__ << mthn << "where:                    " << endl;
        cout << __LINE__ << mthn << "       GK : any valid global key number" << endl;
        cout << __LINE__ << mthn << "       KOC: a KOC name or the keyworw 'all'" << endl << endl;
        return -1;
    }

    PixelTracker *tracker_ = buildTracker();
    CompositeEquipment *feds_ = buildFeds();
    unsigned int globalKey = atoi(argv[1]);
    std::string koc = argv[2];

    bool fromDB = false;
    gettimeofday(&start_time, (struct timezone *)0);

    theGlobalKey_ = new pos::PixelConfigKey(globalKey);

    modList.clear();
    commentMap.clear();
    if (koc == "fedcard" || koc == "all") {
        cout << __LINE__ << mthn << "Working on fedcard " << endl;
        Iconfig_.setMode(fromDB);
        vector<pos::PixelFEDCard *> fedsV_;
        VIterator<Equipment *> it_pm = feds_->CreateIterator();
        for (it_pm.First(); !it_pm.IsDone(); it_pm.Next()) {
            pos::PixelFEDCard *tmp;
            if (Iconfig_.configurationDataExists(tmp,
                                                 "pixel/fedcard/" + std::string(it_pm.CurrentItem()->Name()),
                                                 *theGlobalKey_)) {
                if (!fromDB)
                    Iconfig_.get(tmp, "pixel/fedcard/" + std::string(it_pm.CurrentItem()->Name()), *theGlobalKey_);
            } else {
                modList.push_back(std::string(it_pm.CurrentItem()->Name()));
                commentMap[std::string(it_pm.CurrentItem()->Name())] = "Fedcard not found!";
            }
        }
        missingComponents["fedcard"] = modList;
        commentToMissingComponent["fedcard"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "calib" || koc == "all") {
        cout << __LINE__ << mthn << "Working on calib " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists((pos::PixelCalibBase *&)calib_, "pixel/calib/", *theGlobalKey_)) {
            modList.push_back("Calib object");
            commentMap["Calib object"] = "Calib object not found";
        }
        missingComponents["calib"] = modList;
        commentToMissingComponent["calib"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "calib" || koc == "all") {
        cout << __LINE__ << mthn << "Working on calib " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists((pos::PixelCalibBase *&)calib25_, "pixel/calib/", *theGlobalKey_)) {
            modList.push_back("Calib object");
            commentMap["Calib object"] = "Calib object not found";
        }
        missingComponents["calib"] = modList;
        commentToMissingComponent["calib"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "detconfig" || koc == "all") {
        cout << __LINE__ << mthn << "Working on detconfig " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(detconfig_, "pixel/detconfig/", *theGlobalKey_)) {
            modList.push_back("detconfig");
            commentMap["detconfig"] = "Detconfig not found";
        }
        Iconfig_.get(detconfig_, "pixel/detconfig/", *theGlobalKey_);
        version = getVersion(theGlobalKey_, "detconfig");
        map<pos::PixelROCName, pos::PixelROCStatus> activeROCs_;
        vector<pos::PixelModuleName> activeModules_;
        activeModules_ = detconfig_->getModuleList();
        activeROCs_ = detconfig_->getROCsList();
        VIterator<Equipment *> rocit = theROCs_->CreateIterator();
        for (rocit.First(); !rocit.IsDone(); rocit.Next()) {
            if (activeROCs_.find(pos::PixelROCName(rocit.CurrentItem()->Name())) == activeROCs_.end()) {
                modList.push_back(rocit.CurrentItem()->Name());
                commentMap[rocit.CurrentItem()->Name()] = "Member of a partially complete module";
            }
        }
        missingComponents["detconfig"] = modList;
        commentToMissingComponent["detconfig"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "globaldelay25" || koc == "all") {
        cout << __LINE__ << mthn << "Working on globaldelay25 " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(globaldelay25_, "pixel/globaldelay25/", *theGlobalKey_)) {
            modList.push_back("globaldelay25");
            commentMap["globaldelay25"] = "globaldelay25 not found";
        }
        Iconfig_.get(globaldelay25_, "pixel/globaldelay25/", *theGlobalKey_);
        version = getVersion(theGlobalKey_, "globaldelay25");
        missingComponents["globaldelay25"] = modList;
        commentToMissingComponent["globaldelay25"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "fecconfig" || koc == "all") {
        cout << __LINE__ << mthn << "Working on fecconfig " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(FECConfig_, "pixel/fecconfig/", *theGlobalKey_)) {
            modList.push_back("fecconfig");
            commentMap["fecconfig"] = "FECconfig not found";
        }
        Iconfig_.get(FECConfig_, "pixel/fecconfig/", *theGlobalKey_);
        version = getVersion(theGlobalKey_, "fecconfig");
        int configuredFECs = FECConfig_->getNFECBoards();
        int present[6] = { 0, 0, 0, 0, 0, 0 };
        if (configuredFECs == 0) {
            modList.push_back("fec");
            commentMap["fec"] = "FECs are completely missing!";
        } else if (configuredFECs < 6) {
            for (int j = 0; j < configuredFECs; ++j) {
                present[FECConfig_->getFECNumber(j) - 1] = 1;
            }
            for (int fec = 0; fec < 6; fec++) {
                if (!present[fec]) {
                    stringstream feccard;
                    feccard.str("");
                    feccard << "FEC " << fec + 1;
                    modList.push_back(feccard.str());
                    commentMap[feccard.str()] = "Missing!";
                }
            }
        }
        missingComponents["fecconfig"] = modList;
        commentToMissingComponent["fecconfig"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "fedconfig" || koc == "all") {
        cout << __LINE__ << mthn << "Working on fedconfig " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(FEDConfig_, "pixel/fedconfig/", *theGlobalKey_)) {
            modList.push_back("fedconfig");
            commentMap["fedconfig"] = "FEDconfig not found";
        }
        Iconfig_.get(FEDConfig_, "pixel/fedconfig/", *theGlobalKey_);
        version = getVersion(theGlobalKey_, "fedconfig");
        stringstream fedconfig;
        int theFEDBoards = FEDConfig_->getNFEDBoards();
        int expectedFEDs = 40;
        if (theFEDBoards != expectedFEDs) {
            fedconfig.str("");
            fedconfig << "Fedcards != " << expectedFEDs << " as expected";
            modList.push_back(fedconfig.str());
            commentMap[fedconfig.str()] = fedconfig.str();
        } else {
            vector<int> theFEDs;
            for (int fed = 0; fed < expectedFEDs; fed++) {
                theFEDs.push_back(FEDConfig_->getFEDNumber(fed));
            }
            std::sort(theFEDs.begin(), theFEDs.end());
            for (vector<int>::iterator fedIt = theFEDs.begin(); fedIt != theFEDs.end(); fedIt++) {
                bool found = false;
                for (int fed = 0; fed < expectedFEDs; fed++) {
                    if (*fedIt == fed)
                        found = true;
                }
                if (!found) {
                    fedconfig.str("");
                    fedconfig << "FED " << *fedIt;
                    modList.push_back(fedconfig.str());
                    commentMap[fedconfig.str()] = "Missing!";
                }
            }
        }
        missingComponents["fedconfig"] = modList;
        commentToMissingComponent["fedconfig"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "lowvoltagemap" || koc == "all") {
        cout << __LINE__ << mthn << "Working on lowvoltagemap " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(LVMap_, "pixel/lowvoltagemap/", *theGlobalKey_)) {
            modList.push_back("Low voltage map file");
            commentMap["Low voltage map file"] = "Data are completely missing!";
        } else {
            Iconfig_.get(LVMap_, "pixel/lowvoltagemap/", *theGlobalKey_);
            version = getVersion(theGlobalKey_, "lowvoltagemap");
            if (LVMap_) {
                VIterator<Equipment *> rocit = theROCs_->CreateIterator();
                for (rocit.First(); !rocit.IsDone(); rocit.Next()) {
                    Equipment *module = tracker_->getBasicAssemblyByFullName(rocit.CurrentItem()->Name());
                    if (module) {
                        std::string dpNameIana = LVMap_->dpNameIana(pos::PixelModuleName(module->Name()));
                        std::string dpNameIdigi = LVMap_->dpNameIdigi(pos::PixelModuleName(module->Name()));
                        if (dpNameIana == "") {
                            modList.push_back(module->Name());
                            commentMap[module->Name()] = "Module has Iana missing!";
                        }
                        if (dpNameIdigi == "") {
                            modList.push_back(module->Name());
                            commentMap[module->Name()] = "Module has Idigi missing!";
                        }
                    } else {
                        modList.push_back(module->Name());
                        commentMap[module->Name()] = "Wrong Module Name Supplied!";
                    }
                }
            }
        }
        missingComponents["lowvoltagemap"] = modList;
        commentToMissingComponent["lowvoltagemap"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "mask" || koc == "all") {
        cout << __LINE__ << mthn << "Working on mask " << endl;
        Iconfig_.setMode(fromDB);
        vector<pos::PixelMaskBase *> masksV_;
        VIterator<Equipment *> it_pm = tracker_->getChild(0)->CreateIterator();
        for (it_pm.First(); !it_pm.IsDone(); it_pm.Next()) {
            VIterator<Equipment *> it_IO = it_pm.CurrentItem()->CreateIterator();
            for (it_IO.First(); !it_IO.IsDone(); it_IO.Next()) {
                VIterator<Equipment *> it_Disk = it_IO.CurrentItem()->CreateIterator();
                for (it_Disk.First(); !it_Disk.IsDone(); it_Disk.Next()) {
                    VIterator<Equipment *> it_Blade = it_Disk.CurrentItem()->CreateIterator();
                    for (it_Blade.First(); !it_Blade.IsDone(); it_Blade.Next()) {
                        VIterator<Equipment *> it_Panel = it_Blade.CurrentItem()->CreateIterator();
                        for (it_Panel.First(); !it_Panel.IsDone(); it_Panel.Next()) {
                            string panelId = it_Panel.CurrentItem()->Name();
                            pos::PixelMaskBase *tmp;
                            if (Iconfig_.configurationDataExists(tmp,
                                                                 "pixel/mask/" + panelId,
                                                                 *theGlobalKey_)) {
                                if (!fromDB)
                                    Iconfig_.get(tmp, "pixel/mask/" + panelId, *theGlobalKey_);
                                VIterator<Equipment *> it_Plq = it_Panel.CurrentItem()->CreateIterator();
                                for (it_Plq.First(); !it_Plq.IsDone(); it_Plq.Next()) {
                                    VIterator<Equipment *> it_Roc = it_Plq.CurrentItem()->CreateIterator();
                                    for (it_Roc.First(); !it_Roc.IsDone(); it_Roc.Next()) {
                                        pos::PixelROCName theROC(it_Roc.CurrentItem()->Name());
                                        pos::PixelROCMaskBits *aPixelMask = tmp->getMaskBits(theROC);
                                        if (!aPixelMask) {
                                            modList.push_back(it_Roc.CurrentItem()->Name());
                                            commentMap[it_Roc.CurrentItem()->Name()] = "Kill-Mask was not found for this ROC!";
                                        }
                                    }
                                }
                            } else {
                                modList.push_back(it_Panel.CurrentItem()->Name());
                                commentMap[it_Panel.CurrentItem()->Name()] = "Kill-Mask was not found for the entire module!";
                            }
                        }
                    }
                }
            }
        }
        VIterator<Equipment *> bpmit = tracker_->getChild(1)->CreateIterator();
        for (bpmit.First(); !bpmit.IsDone(); bpmit.Next()) {
            VIterator<Equipment *> bIOit = bpmit.CurrentItem()->CreateIterator();
            for (bIOit.First(); !bIOit.IsDone(); bIOit.Next()) {
                VIterator<Equipment *> bsecit = bIOit.CurrentItem()->CreateIterator();
                for (bsecit.First(); !bsecit.IsDone(); bsecit.Next()) {
                    VIterator<Equipment *> blayit = bsecit.CurrentItem()->CreateIterator();
                    for (blayit.First(); !blayit.IsDone(); blayit.Next()) {
                        VIterator<Equipment *> bladit = blayit.CurrentItem()->CreateIterator();
                        for (bladit.First(); !bladit.IsDone(); bladit.Next()) {
                            VIterator<Equipment *> bmodit = bladit.CurrentItem()->CreateIterator();
                            for (bmodit.First(); !bmodit.IsDone(); bmodit.Next()) {
                                string moduleId = bmodit.CurrentItem()->Name();
                                pos::PixelMaskBase *tmp;
                                if (Iconfig_.configurationDataExists(tmp,
                                                                     "pixel/mask/" + moduleId,
                                                                     *theGlobalKey_)) {
                                    if (!fromDB)
                                        Iconfig_.get(tmp, "pixel/mask/" + moduleId, *theGlobalKey_);
                                    VIterator<Equipment *> brocit = bmodit.CurrentItem()->CreateIterator();
                                    for (brocit.First(); !brocit.IsDone(); brocit.Next()) {
                                        pos::PixelROCName theROC(brocit.CurrentItem()->Name());
                                        pos::PixelROCMaskBits *aPixelMask = tmp->getMaskBits(theROC);
                                        if (!aPixelMask) {
                                            modList.push_back(brocit.CurrentItem()->Name());
                                            commentMap[brocit.CurrentItem()->Name()] = "Kill-Mask was not found for this ROC!";
                                        }
                                    }
                                } else {
                                    modList.push_back(bmodit.CurrentItem()->Name());
                                    commentMap[bmodit.CurrentItem()->Name()] = "Kill-Mask was not found for the entire module!";
                                }
                            } // bmod
                        }     // blad
                    }         // blay
                }             // bsecit
            }                 // bIOit
        }                     // bpmit
        missingComponents["mask"] = modList;
        commentToMissingComponent["mask"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "nametranslation" || koc == "all") {
        cout << __LINE__ << mthn << "Working on nametranslation " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(nameTranslationModule_, "pixel/nametranslation/", *theGlobalKey_)) {
            modList.push_back("nametranslation");
            commentMap["nametranslation"] = "Nametranslation not found";
        }
        Iconfig_.get(nameTranslationModule_, "pixel/nametranslation/", *theGlobalKey_);
        version = getVersion(theGlobalKey_, "nametranslation");

        VIterator<Equipment *> rocit = theROCs_->CreateIterator();
        for (rocit.First(); !rocit.IsDone(); rocit.Next()) {
            if (!nameTranslationModule_->checkROCExistence(pos::PixelROCName(rocit.CurrentItem()->Name()))) {
                string parent;
                parent = tracker_->getBasicAssemblyByFullName(rocit.CurrentItem()->Name())->Name();
                stringstream s;
                s.str("");
                s << "Module: " << parent;
                if (commentMap.find(s.str()) == commentMap.end()) {
                    modList.push_back(s.str());
                    commentMap[s.str()] = "Module has missing ROCs (whole or in part)!";
                }
                s.str("");
                s << "ROC: " << rocit.CurrentItem()->Name();
                modList.push_back(s.str());
                commentMap[s.str()] = "ROC is missing!";
            }
        }
        missingComponents["nametranslation"] = modList;
        commentToMissingComponent["nametranslation"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "portcard" || koc == "all") {
        cout << __LINE__ << mthn << "Working on portcard " << endl;
        Iconfig_.setMode(fromDB);
        vector<pos::PixelPortCardConfig *> portCardV_;
        VIterator<Equipment *> bsect = bSectors_->CreateIterator();
        for (bsect.First(); !bsect.IsDone(); bsect.Next()) {
            pos::PixelPortCardConfig *tmp;
            string sectId = bsect.CurrentItem()->Name();
            if (Iconfig_.configurationDataExists(tmp,
                                                 "pixel/portcard/" + sectId + "_PRT1",
                                                 *theGlobalKey_)) {
                if (!fromDB)
                    Iconfig_.get(tmp, "pixel/portcard/" + sectId + "_PRT1", *theGlobalKey_);
            } else {
                std::stringstream s;
                modList.push_back("Missing Barrel Portcard");
                commentMap["Missing Barrel Portcard"] = std::string(sectId + "_PRT1");
            }
            if (Iconfig_.configurationDataExists(tmp,
                                                 "pixel/portcard/" + sectId + "_PRT2",
                                                 *theGlobalKey_)) {
                if (!fromDB)
                    Iconfig_.get(tmp, "pixel/portcard/" + sectId + "_PRT2", *theGlobalKey_);
            } else {
                std::stringstream s;
                modList.push_back("Missing Barrel Portcard");
                commentMap["Missing Barrel Portcard"] = std::string(sectId + "_PRT2");
            }
        }
        VIterator<Equipment *> fdisk = fDisks_->CreateIterator();
        for (fdisk.First(); !fdisk.IsDone(); fdisk.Next()) {
            pos::PixelPortCardConfig *tmp;
            string diskId = fdisk.CurrentItem()->Name();
            if (Iconfig_.configurationDataExists(tmp,
                                                 "pixel/portcard/" + diskId + "_PRT1",
                                                 *theGlobalKey_)) {
                if (!fromDB)
                    Iconfig_.get(tmp, "pixel/portcard/" + diskId + "_PRT1", *theGlobalKey_);
                tmp->setComment(comment);
                tmp->setAuthor(user);
                portCardV_.push_back(tmp);
            } else {
                std::stringstream s;
                modList.push_back("Missing Forward Portcard");
                commentMap["Missing Forward Portcard"] = std::string(diskId + "_PRT1");
            }
            if (Iconfig_.configurationDataExists(tmp,
                                                 "pixel/portcard/" + diskId + "_PRT2",
                                                 *theGlobalKey_)) {
                if (!fromDB)
                    Iconfig_.get(tmp, "pixel/portcard/" + diskId + "_PRT2", *theGlobalKey_);
            } else {
                std::stringstream s;
                modList.push_back("Missing Forward Portcard");
                commentMap["Missing Forward Portcard"] = std::string(diskId + "_PRT2");
            }
            if (Iconfig_.configurationDataExists(tmp,
                                                 "pixel/portcard/" + diskId + "_PRT3",
                                                 *theGlobalKey_)) {
                if (!fromDB)
                    Iconfig_.get(tmp, "pixel/portcard/" + diskId + "_PRT3", *theGlobalKey_);
            } else {
                std::stringstream s;
                modList.push_back("Missing Forward Portcard");
                commentMap["Missing Forward Portcard"] = std::string(diskId + "_PRT3");
            }
            if (Iconfig_.configurationDataExists(tmp,
                                                 "pixel/portcard/" + diskId + "_PRT4",
                                                 *theGlobalKey_)) {
                if (!fromDB)
                    Iconfig_.get(tmp, "pixel/portcard/" + diskId + "_PRT4", *theGlobalKey_);
            } else {
                std::stringstream s;
                modList.push_back("Missing Forward Portcard");
                commentMap["Missing Forward Portcard"] = std::string(diskId + "_PRT4");
            }
        }
        missingComponents["portcard"] = modList;
        commentToMissingComponent["portcard"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "portcardmap" || koc == "all") {
        cout << __LINE__ << mthn << "Working on portcardmap " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(portCardMap_, "pixel/portcardmap/", *theGlobalKey_)) {
            modList.push_back("portcardmap");
            commentMap["portcardmap"] = "PortCardMap not found";
        }
        Iconfig_.get(portCardMap_, "pixel/portcardmap/", *theGlobalKey_);
        version = getVersion(theGlobalKey_, "portcardmap");
        std::string portcardName = "";
        map<string, string> cmps;
        VIterator<Equipment *> rocit = theROCs_->CreateIterator();
        for (rocit.First(); !rocit.IsDone(); rocit.Next()) {

            if (cmps.find(tracker_->getBasicAssemblyByFullName(rocit.CurrentItem()->Name())->Name()) == cmps.end()) {
                cmps[tracker_->getBasicAssemblyByFullName(rocit.CurrentItem()->Name())->Name()] = tracker_->getBasicAssemblyByFullName(rocit.CurrentItem()->Name())->Name();
            }
        }
        for (std::map<std::string, string>::iterator it = cmps.begin(); it != cmps.end(); it++) {
            if (!portCardMap_->getName(it->first, portcardName)) {
                stringstream s;
                s.str("");
                s << "Portcard " << portcardName << " is missing" << endl;
                modList.push_back(it->first);
                commentMap[it->first] = s.str();
            }
        }
        missingComponents["portcardmap"] = modList;
        commentToMissingComponent["portcardmap"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "tbm" || koc == "all") {
        cout << __LINE__ << mthn << "Working on tbm " << endl;
        Iconfig_.setMode(fromDB);
        vector<pos::PixelTBMSettings *> tbmSettingsV_;
        VIterator<Equipment *> it_pm = tracker_->getChild(0)->CreateIterator();
        for (it_pm.First(); !it_pm.IsDone(); it_pm.Next()) {
            VIterator<Equipment *> it_IO = it_pm.CurrentItem()->CreateIterator();
            for (it_IO.First(); !it_IO.IsDone(); it_IO.Next()) {
                VIterator<Equipment *> it_Disk = it_IO.CurrentItem()->CreateIterator();
                for (it_Disk.First(); !it_Disk.IsDone(); it_Disk.Next()) {
                    VIterator<Equipment *> it_Blade = it_Disk.CurrentItem()->CreateIterator();
                    for (it_Blade.First(); !it_Blade.IsDone(); it_Blade.Next()) {
                        VIterator<Equipment *> it_Panel = it_Blade.CurrentItem()->CreateIterator();
                        for (it_Panel.First(); !it_Panel.IsDone(); it_Panel.Next()) {
                            string panelId = it_Panel.CurrentItem()->Name();
                            pos::PixelTBMSettings *tmp;
                            if (Iconfig_.configurationDataExists(tmp,
                                                                 "pixel/tbm/" + panelId,
                                                                 *theGlobalKey_)) {
                                if (!fromDB)
                                    Iconfig_.get(tmp, "pixel/tbm/" + panelId, *theGlobalKey_);
                            } else {
                                modList.push_back(panelId);
                                commentMap[panelId] = "TBM settings are missing";
                            }
                        }
                    }
                }
            }
        }
        VIterator<Equipment *> bpmit = tracker_->getChild(1)->CreateIterator();
        for (bpmit.First(); !bpmit.IsDone(); bpmit.Next()) {
            VIterator<Equipment *> bIOit = bpmit.CurrentItem()->CreateIterator();
            for (bIOit.First(); !bIOit.IsDone(); bIOit.Next()) {
                VIterator<Equipment *> bsecit = bIOit.CurrentItem()->CreateIterator();
                for (bsecit.First(); !bsecit.IsDone(); bsecit.Next()) {
                    VIterator<Equipment *> blayit = bsecit.CurrentItem()->CreateIterator();
                    for (blayit.First(); !blayit.IsDone(); blayit.Next()) {
                        VIterator<Equipment *> bladit = blayit.CurrentItem()->CreateIterator();
                        for (bladit.First(); !bladit.IsDone(); bladit.Next()) {
                            VIterator<Equipment *> bmodit = bladit.CurrentItem()->CreateIterator();
                            for (bmodit.First(); !bmodit.IsDone(); bmodit.Next()) {
                                string moduleId = bmodit.CurrentItem()->Name();
                                pos::PixelTBMSettings *tmp;
                                if (Iconfig_.configurationDataExists(tmp,
                                                                     "pixel/tbm/" + moduleId,
                                                                     *theGlobalKey_)) {
                                    if (!fromDB)
                                        Iconfig_.get(tmp, "pixel/tbm/" + moduleId, *theGlobalKey_);
                                } else {
                                    modList.push_back(moduleId);
                                    commentMap[moduleId] = "TBM settings are missing";
                                }
                            } // bmod
                        }     // blad
                    }         // blay
                }             // bsecit
            }                 // bIOit
        }                     // bpmit
        missingComponents["tbm"] = modList;
        commentToMissingComponent["tbm"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "tkfecconfig" || koc == "all") {
        cout << __LINE__ << mthn << "Working on tkfecconfig " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(TKFECConfig_, "pixel/tkfecconfig/", *theGlobalKey_)) {
            modList.push_back("tkfecconfig");
            commentMap["tkfecconfig"] = "TKFECconfig not found";
        }
        Iconfig_.get(TKFECConfig_, "pixel/tkfecconfig/", *theGlobalKey_);
        version = getVersion(theGlobalKey_, "tkfecconfig");
        if (!TKFECConfig_) {
            modList.push_back("TKFECConfig data");
            commentMap["TKFECConfig data"] = "Missing!";
        }
        missingComponents["tkfecconfig"] = modList;
        commentToMissingComponent["tkfecconfig"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "trim" || koc == "all") {
        cout << __LINE__ << mthn << "Working on trim " << endl;
        Iconfig_.setMode(fromDB);
        vector<pos::PixelTrimBase *> trimsV_;
        VIterator<Equipment *> it_pm = tracker_->getChild(0)->CreateIterator();
        for (it_pm.First(); !it_pm.IsDone(); it_pm.Next()) {
            VIterator<Equipment *> it_IO = it_pm.CurrentItem()->CreateIterator();
            for (it_IO.First(); !it_IO.IsDone(); it_IO.Next()) {
                VIterator<Equipment *> it_Disk = it_IO.CurrentItem()->CreateIterator();
                for (it_Disk.First(); !it_Disk.IsDone(); it_Disk.Next()) {
                    VIterator<Equipment *> it_Blade = it_Disk.CurrentItem()->CreateIterator();
                    for (it_Blade.First(); !it_Blade.IsDone(); it_Blade.Next()) {
                        VIterator<Equipment *> it_Panel = it_Blade.CurrentItem()->CreateIterator();
                        for (it_Panel.First(); !it_Panel.IsDone(); it_Panel.Next()) {
                            string panelId = it_Panel.CurrentItem()->Name();
                            pos::PixelTrimBase *tmp;
                            if (Iconfig_.configurationDataExists((pos::PixelTrimBase *&)tmp,
                                                                 "pixel/trim/" + panelId,
                                                                 *theGlobalKey_)) {
                                if (!fromDB)
                                    Iconfig_.get((pos::PixelTrimBase *&)tmp, "pixel/trim/" + panelId, *theGlobalKey_);
                                VIterator<Equipment *> it_Plq = it_Panel.CurrentItem()->CreateIterator();
                                for (it_Plq.First(); !it_Plq.IsDone(); it_Plq.Next()) {
                                    VIterator<Equipment *> it_Roc = it_Plq.CurrentItem()->CreateIterator();
                                    for (it_Roc.First(); !it_Roc.IsDone(); it_Roc.Next()) {
                                        pos::PixelROCName theROC(it_Roc.CurrentItem()->Name());
                                        pos::PixelROCTrimBits *aPixelTrim = tmp->getTrimBits(theROC);
                                        if (!aPixelTrim) {
                                            modList.push_back(it_Roc.CurrentItem()->Name());
                                            commentMap[it_Roc.CurrentItem()->Name()] = "Trims was not found for this ROC!";
                                        }
                                    }
                                }
                            } else {
                                modList.push_back(panelId);
                                commentMap[panelId] = "Trim settings are missing";
                            }
                        }
                    }
                }
            }
        }
        VIterator<Equipment *> bpmit = tracker_->getChild(1)->CreateIterator();
        for (bpmit.First(); !bpmit.IsDone(); bpmit.Next()) {
            VIterator<Equipment *> bIOit = bpmit.CurrentItem()->CreateIterator();
            for (bIOit.First(); !bIOit.IsDone(); bIOit.Next()) {
                VIterator<Equipment *> bsecit = bIOit.CurrentItem()->CreateIterator();
                for (bsecit.First(); !bsecit.IsDone(); bsecit.Next()) {
                    VIterator<Equipment *> blayit = bsecit.CurrentItem()->CreateIterator();
                    for (blayit.First(); !blayit.IsDone(); blayit.Next()) {
                        VIterator<Equipment *> bladit = blayit.CurrentItem()->CreateIterator();
                        for (bladit.First(); !bladit.IsDone(); bladit.Next()) {
                            VIterator<Equipment *> bmodit = bladit.CurrentItem()->CreateIterator();
                            for (bmodit.First(); !bmodit.IsDone(); bmodit.Next()) {
                                string moduleId = bmodit.CurrentItem()->Name();
                                pos::PixelTrimBase *tmp;
                                if (Iconfig_.configurationDataExists((pos::PixelTrimBase *&)tmp,
                                                                     "pixel/trim/" + moduleId,
                                                                     *theGlobalKey_)) {
                                    if (!fromDB)
                                        Iconfig_.get((pos::PixelTrimBase *&)tmp, "pixel/trim/" + moduleId, *theGlobalKey_);
                                    VIterator<Equipment *> brocit = bmodit.CurrentItem()->CreateIterator();
                                    for (brocit.First(); !brocit.IsDone(); brocit.Next()) {
                                        pos::PixelROCName theROC(brocit.CurrentItem()->Name());
                                        pos::PixelROCTrimBits *aPixelTrim = tmp->getTrimBits(theROC);
                                        if (!aPixelTrim) {
                                            modList.push_back(brocit.CurrentItem()->Name());
                                            commentMap[brocit.CurrentItem()->Name()] = "Trim was not found for this ROC!";
                                        }
                                    }
                                } else {
                                    modList.push_back(moduleId);
                                    commentMap[moduleId] = "Trim settings are missing";
                                }
                            } // bmod
                        }     // blad
                    }         // blay
                }             // bsecit
            }                 // bIOit
        }                     // bpmit
        missingComponents["trim"] = modList;
        commentToMissingComponent["trim"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "ttcciconfig" || koc == "all") {
        cout << __LINE__ << mthn << "Working on ttcciconfig " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(ttc_, "pixel/ttcciconfig/", *theGlobalKey_))
            exit(-1);
        Iconfig_.get(ttc_, "pixel/ttcciconfig/", *theGlobalKey_);
        version = getVersion(theGlobalKey_, "ttcciconfig");
        if (!ttc_) {
            modList.push_back("TTCCIConfig data");
            commentMap["TTCCIConfig data"] = "Missing!";
        }
        missingComponents["ttcciconfig"] = modList;
        commentToMissingComponent["ttcciconfig"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "ltcconfig" || koc == "all") {
        cout << __LINE__ << mthn << "Working on ltcconfig " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(ltc_, "pixel/ltcconfig/", *theGlobalKey_)) {
            modList.push_back("ltcconfig");
            commentMap["ltcconfig"] = "Ltcconfig not found";
        } else {
            Iconfig_.get(ltc_, "pixel/ltcconfig/", *theGlobalKey_);
            version = getVersion(theGlobalKey_, "ltcconfig");
            if (!ltc_) {
                modList.push_back("LTCConfig data");
                commentMap["LTCConfig data"] = "Missing!";
            }
        }
        missingComponents["ltcconfig"] = modList;
        commentToMissingComponent["ltcconfig"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "dac" || koc == "all") {
        cout << __LINE__ << mthn << "Working on dac " << endl;
        Iconfig_.setMode(fromDB);
        vector<pos::PixelDACSettings *> dacsV_;
        VIterator<Equipment *> it_pm = tracker_->getChild(0)->CreateIterator();
        for (it_pm.First(); !it_pm.IsDone(); it_pm.Next()) {
            VIterator<Equipment *> it_IO = it_pm.CurrentItem()->CreateIterator();
            for (it_IO.First(); !it_IO.IsDone(); it_IO.Next()) {
                VIterator<Equipment *> it_Disk = it_IO.CurrentItem()->CreateIterator();
                for (it_Disk.First(); !it_Disk.IsDone(); it_Disk.Next()) {
                    VIterator<Equipment *> it_Blade = it_Disk.CurrentItem()->CreateIterator();
                    for (it_Blade.First(); !it_Blade.IsDone(); it_Blade.Next()) {
                        VIterator<Equipment *> it_Panel = it_Blade.CurrentItem()->CreateIterator();
                        for (it_Panel.First(); !it_Panel.IsDone(); it_Panel.Next()) {
                            string panelId = it_Panel.CurrentItem()->Name();
                            pos::PixelDACSettings *tmp;
                            if (Iconfig_.configurationDataExists(tmp,
                                                                 "pixel/dac/" + panelId,
                                                                 *theGlobalKey_)) {
                                if (!fromDB)
                                    Iconfig_.get(tmp, "pixel/dac/" + panelId, *theGlobalKey_);
                                VIterator<Equipment *> it_Plq = it_Panel.CurrentItem()->CreateIterator();
                                for (it_Plq.First(); !it_Plq.IsDone(); it_Plq.Next()) {
                                    VIterator<Equipment *> it_Roc = it_Plq.CurrentItem()->CreateIterator();
                                    for (it_Roc.First(); !it_Roc.IsDone(); it_Roc.Next()) {
                                        pos::PixelROCName theROC(it_Roc.CurrentItem()->Name());
                                        pos::PixelROCDACSettings *dacSettingsReadOutChip = NULL;
                                        dacSettingsReadOutChip = tmp->getDACSettings(theROC);
                                        if (!dacSettingsReadOutChip) {
                                            modList.push_back(it_Roc.CurrentItem()->Name());
                                            commentMap[it_Roc.CurrentItem()->Name()] = "Kill-Mask was not found for this ROC!";
                                        }
                                    }
                                }
                            } else {
                                modList.push_back(panelId);
                                commentMap[panelId] = "Member of a completely missing module";
                            }
                        }
                    }
                }
            }
        }
        VIterator<Equipment *> bpmit = tracker_->getChild(1)->CreateIterator();
        for (bpmit.First(); !bpmit.IsDone(); bpmit.Next()) {
            VIterator<Equipment *> bIOit = bpmit.CurrentItem()->CreateIterator();
            for (bIOit.First(); !bIOit.IsDone(); bIOit.Next()) {
                VIterator<Equipment *> bsecit = bIOit.CurrentItem()->CreateIterator();
                for (bsecit.First(); !bsecit.IsDone(); bsecit.Next()) {
                    VIterator<Equipment *> blayit = bsecit.CurrentItem()->CreateIterator();
                    for (blayit.First(); !blayit.IsDone(); blayit.Next()) {
                        VIterator<Equipment *> bladit = blayit.CurrentItem()->CreateIterator();
                        for (bladit.First(); !bladit.IsDone(); bladit.Next()) {
                            VIterator<Equipment *> bmodit = bladit.CurrentItem()->CreateIterator();
                            for (bmodit.First(); !bmodit.IsDone(); bmodit.Next()) {
                                string moduleId = bmodit.CurrentItem()->Name();
                                pos::PixelDACSettings *tmp;
                                if (Iconfig_.configurationDataExists(tmp,
                                                                     "pixel/dac/" + moduleId,
                                                                     *theGlobalKey_)) {
                                    if (!fromDB)
                                        Iconfig_.get(tmp, "pixel/dac/" + moduleId, *theGlobalKey_);
                                    VIterator<Equipment *> brocit = bmodit.CurrentItem()->CreateIterator();
                                    for (brocit.First(); !brocit.IsDone(); brocit.Next()) {
                                        pos::PixelROCName theROC(brocit.CurrentItem()->Name());
                                        pos::PixelROCDACSettings *dacSettingsReadOutChip = NULL;
                                        dacSettingsReadOutChip = tmp->getDACSettings(theROC);
                                        if (!dacSettingsReadOutChip) {
                                            modList.push_back(brocit.CurrentItem()->Name());
                                            commentMap[brocit.CurrentItem()->Name()] = "Dac Settings not found for this ROC!";
                                        }
                                    }
                                } else {
                                    modList.push_back(moduleId);
                                    commentMap[moduleId] = "Member of a completely missing module";
                                }
                            } // bmod
                        }     // blad
                    }         // blay
                }             // bsecit
            }                 // bIOit
        }                     // bpmit
        missingComponents["dac"] = modList;
        commentToMissingComponent["dac"] = commentMap;
    }

    modList.clear();
    commentMap.clear();
    if (koc == "maxvsf" || koc == "all") {
        cout << __LINE__ << mthn << "Working on maxvsf " << endl;
        Iconfig_.setMode(fromDB);
        if (!Iconfig_.configurationDataExists(maxvsf_, "pixel/maxvsf/", *theGlobalKey_)) {
            modList.push_back("maxvsf");
            commentMap["maxvsf"] = "Maxvsf not found";
        } else {
            Iconfig_.get(maxvsf_, "pixel/maxvsf/", *theGlobalKey_);
            version = getVersion(theGlobalKey_, "maxvsf");
            if (!maxvsf_) {
                modList.push_back("MaxVsf data");
                commentMap["MaxVsf data"] = "Data block is missing!";
            }
        }
        missingComponents["maxvsf"] = modList;
        commentToMissingComponent["maxvsf"] = commentMap;
    }

    dumpAnalysisResults(globalKey, koc);
    exit(0);
}

//==========================================================================================================================
//map<string, vector< string> >      missingComponents       ;
//map<string, bool>      consistencyVerified        ;
//map<string, map<string, string> >     commentToMissingComponent  ;
void dumpAnalysisResults(int GK, string koc) {

    stringstream filename;
    filename << "checkCompleteness_" << GK << "_" << koc << ".log";
    ofstream outfile(filename.str().c_str());
    std::string mthn = "]\t[checkCompleteness::dumpAnalysisResults()]\t\t    ";

    bool completelyFailed = false;
    bool partiallyFailed = false;

    cout << __LINE__ << mthn << "Status      " << std::setw(15) << "KOC name\t" << std::setw(6) << "Missing" << endl;
    for (map<string, vector<string> >::iterator it = missingComponents.begin(); it != missingComponents.end(); it++) {
        if (it->second.size() > 0) {
            cout << __LINE__ << mthn << "Incomplete: " << std::setw(15) << it->first << "\t" << std::setw(6) << it->second.size() << endl;
            outfile << "Incomplete: " << std::setw(15) << it->first << "\t" << std::setw(6) << it->second.size() << endl;
        }
    }
    for (map<string, vector<string> >::iterator it = missingComponents.begin(); it != missingComponents.end(); it++) {
        if (it->second.size() <= 0) {
            cout << __LINE__ << mthn << "Complete  : " << std::setw(15) << it->first << endl;
            outfile << "Complete  : " << std::setw(15) << it->first << endl;
        }
    }
    outfile << endl << endl;

    for (map<string, vector<string> >::iterator it = missingComponents.begin(); it != missingComponents.end(); it++) {
        bool first = true;
        //      outfile << it->first << "]\tTotal Missing Components: " << it->second.size() << endl ;
        //      for(vector< string>::iterator jt=it->second.begin(); jt!=it->second.end(); jt++)
        //	{
        //	  outfile << " " << *jt << endl ;
        //	}

        //      outfile << it->first << "]\tComments to Missing Components: " << commentToMissingComponent[it->first].size() << endl ;
        for (map<string, string>::iterator kt = commentToMissingComponent[it->first].begin(); kt != commentToMissingComponent[it->first].end(); kt++) {
            if (it->first == "maxvsf" || it->first == "lowvoltagemap") {
                partiallyFailed = true;
            } else {
                completelyFailed = true;
            }
            if (first) {
                first = false;
                outfile << "************************ " << it->first << " ************************" << endl;
            }
            outfile << " " << (*kt).first << " --> " << (*kt).second << endl;
        }
    }

    string newFilename(filename.str());
    if (completelyFailed) {
        outfile << "                                     " << endl;
        outfile << "The GK " << GK << " is incomplete!!!!" << endl;
        outfile << "                                     " << endl;
        newFilename.replace(newFilename.find(".log"), newFilename.length(), "_incomplete.log");
    } else if (partiallyFailed) {
        outfile << "                                     " << endl;
        outfile << "The GK " << GK << " is partially incomplete (only maxsvf or lowvoltagemap are missing)" << endl;
        outfile << "                                     " << endl;
        newFilename.replace(newFilename.find(".log"), newFilename.length(), "_partial.log");
    } else {
        outfile << "                                     " << endl;
        outfile << "The GK " << GK << " is complete!!!!" << endl;
        outfile << "                                     " << endl;
        outfile.close();
        newFilename.replace(newFilename.find(".log"), newFilename.length(), "_complete.log");
    }
    outfile.close();
    string command("mv " + filename.str() + " " + newFilename);
    system(command.c_str());
}

//=============================================================================================
PixelTracker *buildTracker() {
    std::string mthn = "[main::buildTracker()]";
    PixelTracker *tracker_ = new PixelTracker("PixelTracker", 2);
    FPix *fpix = new FPix("FPix", 2);

    fDisks_ = new CompositeEquipment("disks", 8);

    for (int bpm = 0; bpm < 2; bpm++) {
        stringstream s_bplusminus;
        string side = bpm != 0 ? "m" : "p";
        s_bplusminus << "FPix_B" << side;
        CompositeEquipment *bplusminus = new CompositeEquipment(s_bplusminus.str().c_str(), 2);
        for (int bm = 0; bm < 2; bm++) {
            stringstream bmname;
            string side = bm != 0 ? "I" : "O";
            bmname << s_bplusminus.str() << side;
            CompositeEquipment *bmi = new CompositeEquipment(bmname.str().c_str(), 2);
            for (int di = 1; di <= 2; di++) {
                stringstream diskname;
                diskname << bmname.str() << "_D" << di;
                FDisk *bmi_d = new FDisk(diskname.str().c_str(), 12);
                for (int bi = 1; bi <= 12; bi++) {
                    stringstream bladename;
                    bladename << diskname.str() << "_BLD" << bi;
                    CompositeEquipment *blade = new CompositeEquipment(bladename.str().c_str(), 2);
                    for (int pi = 1; pi <= 2; pi++) {
                        stringstream panelname;
                        panelname << bladename.str() << "_PNL" << pi;
                        int children = pi == 1 ? 4 : 3;
                        CompositeEquipment *panel = new CompositeEquipment(panelname.str().c_str(), children);
                        for (int plaq = 1; plaq <= children; plaq++) {
                            stringstream plaqname;
                            plaqname << panelname.str() << "_PLQ" << plaq;
                            int nrocs = 0;
                            if (children == 3) // we are on panel 2 ==> type 3
                            {
                                switch (plaq) {
                                case 1:
                                    nrocs = 6;
                                    break;
                                case 2:
                                    nrocs = 8;
                                    break;
                                case 3:
                                    nrocs = 10;
                                    break;
                                }
                            } else if (children == 4) // we are on panel 1 ==> type 4
                            {
                                switch (plaq) {
                                case 1:
                                    nrocs = 2;
                                    break;
                                case 2:
                                    nrocs = 6;
                                    break;
                                case 3:
                                    nrocs = 8;
                                    break;
                                case 4:
                                    nrocs = 5;
                                    break;
                                }
                            }
                            CompositeEquipment *plaquette = new CompositeEquipment(plaqname.str().c_str(), nrocs);
                            for (int irocs = 0; irocs < nrocs; irocs++) {
                                stringstream rocname;
                                rocname << plaqname.str() << "_ROC" << irocs;
                                //                              cout << rocname.str() << endl ;
                                Roc *roc = new Roc(rocname.str().c_str(), 0);
                                plaquette->Add(roc);
                                theROCs_->AddNoFather(roc);
                            }
                            panel->Add(plaquette);
                        }
                        blade->Add(panel);
                    }
                    bmi_d->Add(blade);
                }
                bmi->Add(bmi_d);
                fDisks_->AddNoFather(bmi_d);
            }
            bplusminus->Add(bmi);
        }
        fpix->Add(bplusminus);
    }
    //Now Building composite Structure on top of the main Forward tracker to reflect svg file hierarchy.
    //
    // BARREL TRACKER
    //

    bSectors_ = new CompositeEquipment("BarrelSectors", 32);
    BPix *bpix = new BPix("BPix_", 2);
    for (int bpm = 0; bpm < 2; bpm++) {
        stringstream s_bplusminus;
        string side = bpm != 0 ? "m" : "p";
        s_bplusminus << "BPix_B" << side;
        CompositeEquipment *bplusminus = new CompositeEquipment(s_bplusminus.str().c_str(), 2);
        for (int bm = 0; bm < 2; bm++) {
            stringstream bmname;
            string side = bm != 0 ? "I" : "O";
            bmname << s_bplusminus.str() << side;
            CompositeEquipment *bmi = new CompositeEquipment(bmname.str().c_str(), 8);
            for (int isect = 1; isect <= 8; isect++) {
                stringstream sector;
                sector << bmname.str() << "_SEC" << isect;
                CompositeEquipment *bmi_sec = new CompositeEquipment(sector.str().c_str(), 3);
                for (int blay = 1; blay <= 3; blay++) {
                    stringstream layer;
                    layer << sector.str() << "_LYR" << blay;
                    int nladders = 0;
                    switch (blay) {
                    case 1:
                        if (isect == 1 || isect == 8) {
                            nladders = 2;
                        } else {
                            nladders = 1;
                        }
                        break;
                    case 2:
                        nladders = 2;
                        break;
                    case 3:
                        if (isect == 4 || isect == 5) {
                            nladders = 2;
                        } else {
                            nladders = 3;
                        }
                        break;
                    }
                    CompositeEquipment *bmi_lay = new CompositeEquipment(layer.str().c_str(), nladders);
                    for (int iladd = 1; iladd <= nladders; iladd++) {
                        int realIndex = 0;
                        if (blay == 1) {
                            if (isect == 1)
                                realIndex = iladd;
                            if (isect != 1)
                                realIndex = isect + iladd;
                        }
                        if (blay == 2) {
                            realIndex = (2 * (isect - 1)) + iladd;
                        }
                        if (blay == 3) {
                            realIndex = (3 * (isect - 1)) + iladd;
                            if (isect == 5)
                                --realIndex;
                            if (isect >= 6)
                                realIndex -= 2;
                        }
                        stringstream ladder;
                        ladder << layer.str() << "_LDR" << realIndex;
                        if (blay == 1 && (realIndex == 1 || realIndex == 10))
                            ladder << "H";
                        if (blay == 2 && (realIndex == 1 || realIndex == 16))
                            ladder << "H";
                        if (blay == 3 && (realIndex == 1 || realIndex == 22))
                            ladder << "H";
                        if (ladder.str().find("H") == string::npos)
                            ladder << "F";
                        CompositeEquipment *bmi_ladd = new CompositeEquipment(ladder.str().c_str(), 4);
                        for (int imod = 1; imod <= 4; imod++) {
                            int nrocs = 0;
                            stringstream module;
                            module << ladder.str() << "_MOD" << imod;
                            if (module.str().find("H") != string::npos) {
                                nrocs = 8;
                            } else {
                                nrocs = 16;
                            }
                            CompositeEquipment *bmi_mod = new CompositeEquipment(module.str().c_str(), nrocs);
                            for (int irocs = 0; irocs < nrocs; irocs++) {
                                stringstream rocname;
                                if (nrocs == 8 && module.str().find("Bm") != std::string::npos) {
                                    rocname << module.str() << "_ROC" << 8 + irocs;
                                } else {
                                    rocname << module.str() << "_ROC" << irocs;
                                }
                                //                              cout << rocname.str() << endl ;
                                Roc *roc = new Roc(rocname.str().c_str(), 0);
                                bmi_mod->Add(roc);
                                theROCs_->AddNoFather(roc);
                            } // rocs: roc
                            bmi_ladd->Add(bmi_mod);
                        } // modules: bmi_mod
                        bmi_lay->Add(bmi_ladd);
                    } // Ladders: bmi_ladd
                    bmi_sec->Add(bmi_lay);
                } // Layers: bmi_lay
                bmi->Add(bmi_sec);
                bSectors_->AddNoFather(bmi_sec);
            } // sectors: bmi_sec
            bplusminus->Add(bmi);
        } // Inner or Outer: bmi
        bpix->Add(bplusminus);
    } // plus or minus: bplusminus

    tracker_->Add(fpix);
    tracker_->Add(bpix);

    return tracker_;
}

//==========================================================================================================================
CompositeEquipment *buildFeds() {
    int totFeds = 40;
    CompositeEquipment *feds_ = new CompositeEquipment("FED", totFeds);
    for (int fed = 0; fed < totFeds; fed++) {
        stringstream fedname;
        fedname << fed;
        CompositeEquipment *fedsingle = new CompositeEquipment(fedname.str().c_str(), 3);
        for (int slot = 1; slot <= 3; slot++) {
            stringstream slotname;
            slotname << "slot_" << slot;
            CompositeEquipment *slotsingle = new CompositeEquipment(slotname.str().c_str(), 12);
            for (int channel = (slot - 1) * 12 + 1; channel <= slot * 12; channel++) {
                stringstream channelname;
                channelname << "channel_" << channel;
                CompositeEquipment *channel = new CompositeEquipment(channelname.str().c_str(), 16); //Al massimo, se la TBM controlla tutto un modulo F

                slotsingle->Add(channel);
            }
            fedsingle->Add(slotsingle);
        }
        feds_->Add(fedsingle);
    }
    //   feds_->dump(" ") ;
    return feds_;
}
//=========================================================================================================
void endJob(struct timeval &start_time) {
    struct timeval end_time;
    gettimeofday(&end_time, (struct timezone *)0);
    int total_usecs = (end_time.tv_sec - start_time.tv_sec) * 1000000 + (end_time.tv_usec - start_time.tv_usec);
    cout << "Time taken : " << total_usecs / 1000000. << " secs" << endl;
    return;
}

//=========================================================================================================
int getVersion(pos::PixelConfigKey *theKey, std::string koc) {
    std::string mthn = "]\t[checkCompleteness::getVersion()]\t\t\t    ";
    vector<pair<std::string, unsigned int> > KOCVersions;

    KOCVersions = PixelConfigInterface::getVersions(*theKey);

    for (vector<pair<std::string, unsigned int> >::iterator KVIt = KOCVersions.begin(); KVIt != KOCVersions.end(); KVIt++) {
        if ((*KVIt).first == koc) {
            cout << __LINE__ << mthn << "KOC: " << (*KVIt).first << "\tversion: " << (*KVIt).second << endl;
            return (*KVIt).second;
        }
    }

    std::cout << __LINE__ << mthn << "Error: non existent version for GK "
              << theKey->key()
              << " and KOC: " << koc
              << std::endl;
    exit(-1);
}

/* Emacs specific customization
   ;;; Local Variables:     ***
   ;;; indent-tabs-mode:nil ***
   ;;; c-set-style:gnu      ***
   ;;; End:                 ***
*/
