#!/usr/bin/perl

$CONFIGURATIONS = $ENV{"PIXELCONFIGURATIONBASE"} . "/configurations.txt" ;
$firstGK = "-1" ;

open(IN, $CONFIGURATIONS) || die "Cannot open $CONFIGURATIONS" ;

while(<IN>) 
{
 chomp ;
 if(m/key\s+(\d+)/) 
 {
  my $key = $1 ;
  unless( $key > $firstGK ) {next;}
  &checkIt($key) ;
 }
}

close(IN) ;


#================================================================================
sub checkIt
{
 my $key = $_[0] ;
 
 $cmd = "./checkCompleteness ${key} all" ;
 print("${cmd}\n") ;
 open(PIPE, "${cmd} 2>&1 |" ) ;
 while(<PIPE>)
 {
  if( m/Assertion/ ) 
  {
   my $logname  ="checkCompleteness_${key}_all.log" ;
   open( OUT, ">$logname" ) ;
   print OUT ("The GK ${key} is incomplete!!!! (incorrect check-word)\n") ;
   close OUT ;
  }
  print ;
 }
 close(PIPE) ;
 print("=========== Done ==============\n\n") ;
}
