#!/usr/bin/perl -w
#
#$Author: menasce $
#$Date: 2010/04/28 15:49:46 $
#$Revision: 1.15 $

require 'sys/syscall.ph' ;

# print("zipper.pl aborting: \n") ; exit(0) ;

my $mthn = "[zipper.pl::main()]\t\t\t\t\t    " ;

$spool = $ENV{'PIXELCONFIGURATIONSPOOLAREA'} ;
$home  = $ENV{'BUILD_HOME'} ;

&checkConsistency() ;

print("${mthn}\n") ;

#@fileList = `ls ${spool}/*` ;

$globalKey = $ARGV[0] ;
$timestamp = $ARGV[1] ;

my $zipFile = "globalKey_${globalKey}_${timestamp}.zip" ;
my $zipArea = "spoolArea_${globalKey}_${timestamp}" ;

&execCmd("mkdir -p ${spool}/${zipArea}" ); # make timestamped folder to contain zip file

@list = `ls -1F ${spool} | grep -v /` ;
foreach $file (@list) 
{
    chomp($file) ;
    &execCmd("mv ${spool}/${file} ${spool}/${zipArea}/." ); # move files into destination
}

&execCmd("cd ${spool}/${zipArea}; zip ${zipFile} *" ); # make zip file

#&cleanUp() ; # Clean up spoolarea

&deployFile("${spool}/${zipArea}/${zipFile}") ;

#========================================================================
sub checkConsistency
{
 my $mthn = "[zipper.pl::checkConsistency()]\t\t\t\t    " ;
 unless( $spool =~ m/\w+/ )  {print("${mthn}FATAL: undefined PIXELCONFIGURATIONSPOOLAREA\n") ;exit(-1) ;}
 unless( $home  =~ m/\w+/ ) 
 {
  $home = $ENV{"XDAQ_ROOT"} ;
  unless( $home  =~ m/\w+/ ) {print("${mthn}FATAL: undefined BUILD_HOME\n")                  ;exit(-1) ;}
 }

# unless( -e "${home}/pixel/PixelConfigDBGUI/dbacc" ) 
# {
#  print <<EOB ;
#
#
#${mthn}FATAL: missing ${home}/pixel/PixelConfigDBGUI/dbacc file
#Provide one or send an email to dario.menasce\@mib.infn.it or marco.rovere\@mib.infn.it
#
#
#EOB
#  exit(-1) ;
# }
}
#========================================================================
sub cleanUp
{
 my $cmd = "rm " ;
 foreach $file (@list)
 {
  chomp $file ;
  $cmd .= "${file} " ;
 }
 &execCmd("$cmd");
}
#========================================================================
sub deployFile
{
 my $mthn      = "[zipper.pl::deployFile()]\t\t\t\t    " ;
 my $copierZip = $_[0] ;

 my $copierExe = "" ;
 my $copierPar = "" ;
 my $copierDes = "" ;
 my $copierCmd = "" ;

 if( $ENV{HOSTNAME} eq "pcmagni" )
 {
   # pcmagni Specific stuff!!
   $copierExe = "${home}/pixel/PixelConfigDBGUI/util/sshpass-1.02/sshpass" ;
   $copierPar = "-f ${home}/pixel/PixelConfigDBGUI/dbacc scp -P 10888" ;
   $copierDes = "dbvalfpix\@localhost:conditions/." ;
   $copierCmd = "${copierExe} ${copierPar} ${copierZip} ${copierDes}" ;
 } else {
   # P5 Specific Stuff!!
   $copierExe = "scp " ;
   $copierPar = "-p " ;
   $copierDes = "kvm-s3562-1-ip151-87.cms:/var/spool/xmlloader/pixel/prod/conditions/" ; # production db
#   $copierDes = "kvm-s3562-1-ip151-87.cms:/var/spool/xmlloader/pixel/valid/conditions/" ; # integration db
   $copierCmd = "${copierExe} ${copierPar} ${copierZip} ${copierDes}" ;
 }

 &execCmd("${copierCmd}");
}
#========================================================================
sub execCmd
{
 my $mthn = "[zipper.pl::execCmd()]\t\t\t\t    " ;
 my $cmd  = $_[0];
 
 print("87]\t${mthn}${cmd}\n") ;
 print("88]\t${mthn}      \n") ;
 
 open(PIPE, "${cmd} 2>&1 |"  ) ;
 while(<PIPE>)
 {
  print ;
 }
 close(PIPE) ;
 print("96]\t${mthn}      \n") ;
}
