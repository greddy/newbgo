#include <iostream>
#include <iomanip>
#include "PixelConfigDBInterface/include/PixelConfigDBInterface.h"

int main(int argc, char *argv[]) {

    PixelConfigDBInterface *db = new PixelConfigDBInterface();

    db->connect();

    /**
     Configuration Views
   */
    std::cout << " *                                                                                 * " << std::endl;
    std::cout << " *                                CONFIGURATION VIEWS                              * " << std::endl;
    std::cout << " *                                                                                 * " << std::endl << std::endl;
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_CONFIG_KEYS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_DATASET_MAP_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_ALIAS_VERSION_ALIAS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_ALIAS_VERSION_ALIAS_T");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_ALIAS_KEY_DATASET_MAP_V");

    /**
     Configuration Tables
   */
    std::cout << "\f" << std::endl;
    std::cout << " *                                                                                 * " << std::endl;
    std::cout << " *                                CONFIGURATION TABLES                             * " << std::endl;
    std::cout << " *                                                                                 * " << std::endl << std::endl;
    db->dumpTableInfo("CMS_PXL_CORE_IOV_MGMNT_OWNER.CONFIG_KEYS");
    db->dumpTableInfo("CMS_PXL_CORE_IOV_MGMNT_OWNER.CONFIG_KEY_ALIAS_MAPS");
    db->dumpTableInfo("CMS_PXL_CORE_IOV_MGMNT_OWNER.CONFIG_KEY_ALIASES");
    db->dumpTableInfo("CMS_PXL_CORE_IOV_MGMNT_OWNER.CONFIG_KEY_VERSION_MAPS");
    db->dumpTableInfo("CMS_PXL_CORE_IOV_MGMNT_OWNER.CONFIG_VERSION_ALIAS_MAPS");
    db->dumpTableInfo("CMS_PXL_CORE_IOV_MGMNT_OWNER.CONFIG_VERSION_ALIASES");
    db->dumpTableInfo("CMS_PXL_CORE_IOV_MGMNT_OWNER.CONFIG_KEY_VERSION_ALIAS_MAPS");
    db->dumpTableInfo("CMS_PXL_CORE_CONDITION_OWNER.COND_DATA_SETS");
    db->dumpTableInfo("CMS_PXL_CORE_CONDITION_OWNER.KINDS_OF_CONDITIONS");

    /**
     Data Views
   */
    std::cout << "\f" << std::endl;
    std::cout << " *                                                                                 * " << std::endl;
    std::cout << " *                                     DATA VIEWS                                  * " << std::endl;
    std::cout << " *                                                                                 * " << std::endl << std::endl;
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_DET_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_NAME_TRANS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_ROCDAC_COL_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_PIXEL_CALIB_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_ROC_MASKS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_ROC_TRIMS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_LTC_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_TTC_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_PIXEL_TBM_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_PIXEL_FEC_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_TRACKER_FEC_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_XDAQ_LOW_VOLTAGE_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_GLOBAL_DELAY25_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_FED_CRATE_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_PORTCARD_MAP_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_PORTCARD_SETTINGS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_FED_CONFIGURATION_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_ROC_LEVELS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_TBM_LEVELS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_KEY_ROC_MAXVSF_V");

    /**
     Data Views
   */
    std::cout << "\f" << std::endl;
    std::cout << " *                                                                                 * " << std::endl;
    std::cout << " *                                 DATA VERSION VIEWS                              * " << std::endl;
    std::cout << " *                                                                                 * " << std::endl << std::endl;
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_DET_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_NAME_TRANS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_ROCDAC_COL_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_PIXEL_CALIB_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_ROC_MASKS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_ROC_TRIMS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_LTC_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_TTC_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_PIXEL_TBM_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_TRACKER_FEC_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_PIXEL_FEC_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_XDAQ_LOW_VOLTAGE_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_GLOBAL_DELAY25_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_FED_CRATE_CONFIG_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_PORTCARD_MAP_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_PORTCARD_SETTINGS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_FED_CONFIGURATION_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_ROC_LEVELS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_TBM_LEVELS_V");
    db->dumpTableInfo("CMS_PXL_PIXEL_VIEW_OWNER.CONF_ROC_MAXVSF_V");

    return 0;
}
