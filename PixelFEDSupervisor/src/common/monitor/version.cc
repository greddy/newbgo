#include "PixelFEDSupervisor/include/monitor/pixfed/version.h"

#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"

#include "PixelFEDSupervisor/include/monitor/exception/version.h"
#include "pixel/hwlayer/version.h"
#include "pixel/utils/version.h"

GETPACKAGEINFO(pixelmonitorpixfed)

void
pixelmonitorpixfed::checkPackageDependencies() {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(tcdsexception);
    CHECKDEPENDENCY(pixelmonitorhwlayer);
    CHECKDEPENDENCY(pixelmonitorutilslayer);
    CHECKDEPENDENCY(toolbox);
    CHECKDEPENDENCY(xcept);
    CHECKDEPENDENCY(xdaq);
}

std::set<std::string, std::less<std::string> >
pixelmonitorpixfed::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, tcdsexception);
    ADDDEPENDENCY(dependencies, pixelmonitorhwlayer);
    ADDDEPENDENCY(dependencies, pixelmonitorutilslayer);
    ADDDEPENDENCY(dependencies, toolbox);
    ADDDEPENDENCY(dependencies, xcept);
    ADDDEPENDENCY(dependencies, xdaq);

    return dependencies;
}
