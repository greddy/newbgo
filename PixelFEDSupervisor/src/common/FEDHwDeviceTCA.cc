#include "PixelFEDSupervisor/include/FEDHwDeviceTCA.h"

#include <climits>
#include <sstream>
#include <time.h>
#include <unistd.h>


#include "uhal/definitions.hpp"
#include "uhal/HwInterface.hpp"
#include "uhal/log/exception.hpp"
#include "uhal/Node.hpp"
#include "uhal/ProtocolControlHub.hpp"
#include "uhal/utilities/bits.hpp"
#include "uhal/ValMem.hpp"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "pixel/exception/Exception.h"

#include "pixel/utils/Utils.h"


pixel::hwlayertca::FEDHwDeviceTCA::FEDHwDeviceTCA()
    : hwP_(nullptr) {
        isConnected=false;
}

pixel::hwlayertca::FEDHwDeviceTCA::FEDHwDeviceTCA(RegManager *const regManager, unsigned long fedid_)
    : hwP_(regManager) {
    hwConnect();
    fedid=pixel::utils::to_string(fedid_);
}

pixel::hwlayertca::FEDHwDeviceTCA::~FEDHwDeviceTCA() {
}

void
pixel::hwlayertca::FEDHwDeviceTCA::setHw(RegManager *const regManager, unsigned long fedid_) {
    hwP_=regManager;
    fedid=pixel::utils::to_string(fedid_);
}

void
pixel::hwlayertca::FEDHwDeviceTCA::hwConnect() {
    
    if(!isHwConnected()){
        isConnected=true;
    }
}

void
pixel::hwlayertca::FEDHwDeviceTCA::hwRelease() {
    isConnected=false;
}

bool
pixel::hwlayertca::FEDHwDeviceTCA::isHwConnected() const {
    return (isConnected and hwP_);
}

uint32_t
pixel::hwlayertca::FEDHwDeviceTCA::readRegister(std::string const &regName) const {
    uint32_t res=0;
    if(not isHwConnected()){
        return res;
    }
    try {
        uhal::ValWord<uint32_t> val = hwP_->ReadReg(regName);
        res=val.value();
    }
    catch (uhal::exception::exception const &err) {
        // NOTE: The uhal exceptions are terribly verbose, so those
        // messages are only logged and not propagated in the
        // exceptions.
        std::string msgBase = toolbox::toString("Could not read register '%s'", regName.c_str());
        std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
        XCEPT_RAISE(pixel::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
    catch (std::exception const &err){
        std::string msgBase = toolbox::toString("Could really not read register '%s' (std::exception)", regName.c_str());
        std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
        XCEPT_RAISE(pixel::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
        
    }
    
    return res;
}

void
pixel::hwlayertca::FEDHwDeviceTCA::writeRegister(std::string const &regName,
                                                  uint32_t const regVal) const {
    
    bool status=false;
    try {
        status =hwP_->WriteReg(regName,regVal);
    }
    catch(uhal::exception::exception const &err) {
        // NOTE: The uhal exceptions are terribly verbose, so those
        // messages are only logged and not propagated in the
        // exceptions.
        std::string msgBase = toolbox::toString("Could not write register '%s'", regName.c_str());
        std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
        XCEPT_RAISE(pixel::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
    if (!status) {
        std::string msgBase = toolbox::toString("Could not write register '%s'", regName.c_str());
        std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), "Return status is 'invalid'.");
        XCEPT_RAISE(pixel::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
}

std::vector<uint32_t>
pixel::hwlayertca::FEDHwDeviceTCA::readBlock(std::string const &regName,
                                              uint32_t const nWords) const {
    
    std::vector<uint32_t> var;
    try {
        var= hwP_->ReadBlockRegValue(regName,nWords);
    }catch(uhal::exception::exception const &err) {
        // NOTE: The uhal exceptions are terribly verbose, so those
        // messages are only logged and not propagated in the
        // exceptions.
        std::string msgBase = toolbox::toString("Could not write register '%s'", regName.c_str());
        std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
        XCEPT_RAISE(pixel::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
    return var;
}

std::vector<uint32_t>
pixel::hwlayertca::FEDHwDeviceTCA::readBlock(std::string const &regName) const {
    std::vector<uint32_t> var;
    try {
        var=readBlock(regName, getBlockSize(regName));
    }catch(pixel::exception::HardwareProblem &err) {
        std::string msgBase = toolbox::toString("Could not write register '%s'", regName.c_str());
        std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
        XCEPT_RETHROW(pixel::exception::HardwareProblem, msg, err);
    }
    return var;
}

void
pixel::hwlayertca::FEDHwDeviceTCA::writeBlock(std::string const &regName,
                                               std::vector<uint32_t> const regVal) const {
    hwP_->WriteBlockReg(regName,regVal);
}

uint32_t
pixel::hwlayertca::FEDHwDeviceTCA::readModifyWriteRegister(std::string const &regName,
                                                            uint32_t const regVal) const {
    // Performs an IPbus read-modify-write-bits operation.

    uhal::HwInterface &hw = hwP_->getHwInterface();
    uint32_t const address = hw.getNode(regName).getAddress();
    uint32_t const mask = hw.getNode(regName).getMask();
    uint32_t const shiftSize = uhal::utilities::TrailingRightBits(mask);
    uint32_t const bitShiftedSource = (regVal << shiftSize);

    if ((bitShiftedSource >> shiftSize) != regVal) {
        std::string msg = toolbox::toString("Could not (read-modify-)write register '%s'. Value to write has bits set outside the mask.", regName.c_str());
        XCEPT_RAISE(pixel::exception::RuntimeProblem, toolbox::toString("%s.", msg.c_str()));
    }

    uint32_t const overlap = (bitShiftedSource & ~mask);

    if (overlap != 0) {
        std::string msg = toolbox::toString("Could not (read-modify-)write register '%s'. Value to write has bits set outside the mask.", regName.c_str());
        XCEPT_RAISE(pixel::exception::RuntimeProblem, toolbox::toString("%s.", msg.c_str()));
    }

    uint32_t tmp;
    try {
        uhal::ValWord<uint32_t> val =
            hw.getClient().rmw_bits(address, ~mask, (bitShiftedSource & mask));
        hw.dispatch();
        tmp = val.value();
    }
    catch (uhal::exception::exception const &err) {
        // NOTE: The uhal exceptions are terribly verbose, so those
        // messages are only logged and not propagated in the
        // exceptions.
        std::string msgBase =
            toolbox::toString("Could not (read-modify-)write register '%s'", regName.c_str());
        std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
        XCEPT_RAISE(pixel::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
    uint32_t const res = ((tmp & mask) >> shiftSize);
    return res;
}

uint32_t
pixel::hwlayertca::FEDHwDeviceTCA::getBlockSize(std::string const &regName) const {
    try {
        return hwP_->getHwInterface().getNode(regName).getSize();
    }
    catch (uhal::exception::exception const &err) {
        // NOTE: The uhal exceptions are terribly verbose, so those
        // messages are only logged and not propagated in the
        // exceptions.
        std::string msgBase = toolbox::toString("Could not get size of register '%s'", regName.c_str());
        std::string msg = toolbox::toString("%s: %s.", msgBase.c_str(), err.what());
        XCEPT_RAISE(pixel::exception::HardwareProblem, toolbox::toString("%s.", msg.c_str()));
    }
}

std::vector<std::string>
pixel::hwlayertca::FEDHwDeviceTCA::getRegisterNames() const {
    return hwP_->getHwInterface().getNodes();
}

pixel::hwlayer::RegisterInfo::RegInfoVec
pixel::hwlayertca::FEDHwDeviceTCA::getRegisterInfos() const {
    uhal::HwInterface &hw = hwP_->getHwInterface();
    std::vector<std::string> const regNames = getRegisterNames();
    pixel::hwlayer::RegisterInfo::RegInfoVec regInfos;
    for (std::vector<std::string>::const_iterator regName = regNames.begin();
         regName != regNames.end();
         ++regName) {
        uhal::Node const &node = hw.getNode(*regName);
        // In order to avoid overlaps, include only end-point (or
        // 'leaf') registers.
        if (node.getNodes().size() == 0) {
            uhal::defs::NodePermission permission = node.getPermission();
            bool const isReadable = ((permission == uhal::defs::READ) ||
                                     (permission == uhal::defs::READWRITE));
            bool const isWritable = ((permission == uhal::defs::WRITE) ||
                                     (permission == uhal::defs::READWRITE));
            pixel::hwlayer::RegisterInfo regInfo(*regName, isReadable, isWritable);
            regInfos.push_back(regInfo);
        }
    }
    return regInfos;
}

pixel::hwlayer::RegDumpVec
pixel::hwlayertca::FEDHwDeviceTCA::dumpRegisterContents() const {
    pixel::hwlayer::RegDumpVec res;

    pixel::hwlayer::RegisterInfo::RegInfoVec regInfos = getRegisterInfos();
    for (pixel::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo = regInfos.begin();
         regInfo != regInfos.end();
         ++regInfo) {
        if (regInfo->isReadable()) {
            std::vector<uint32_t> regVals;
            // NOTE: Some care is required here. For single-word reads
            // the appropriate mask (specified in the address table) is
            // applied. Block reads don't know about masks.
            uint32_t const size = getBlockSize(regInfo->name());
            if (size == 1) {
                regVals.push_back(readRegister(regInfo->name()));
            } else {
                regVals = readBlock(regInfo->name());
            }
            res.push_back(std::make_pair(regInfo->name(), regVals));
        }
    }
    return res;
}

pixel::hwlayertca::FEDHwDeviceTCA::RegContentsVec
pixel::hwlayertca::FEDHwDeviceTCA::readHardwareConfiguration(pixel::hwlayer::RegisterInfo::RegInfoVec const &regInfos) const {
    pixel::hwlayertca::FEDHwDeviceTCA::RegContentsVec res;
    return res;
}

std::string 
pixel::hwlayertca::FEDHwDeviceTCA::getFEDid() const {
    return fedid;
}
