#include "PixelFEDSupervisor/include/TCADeviceFED.h"

pixel::TCADeviceFED::TCADeviceFED()
    :pixel::hwlayertca::TCADeviceBase(std::unique_ptr<pixel::hwlayertca::TCACarrierBase>(new pixel::hwlayertca::TCACarrierFC7(hwDevice_))){
}

pixel::TCADeviceFED::~TCADeviceFED(){
}
