#include "PixelFEDSupervisor/include/PixelFEDSupervisor.h"

#include "pixel/utils/Utils.h"
#include "CalibFormats/SiPixelObjects/interface/PixelROCName.h"
#include "PixelConfigDBInterface/include/PixelConfigInterface.h"
#include "CalibFormats/SiPixelObjects/interface/PixelCalibConfiguration.h"
#include "CalibFormats/SiPixelObjects/interface/PixelConfigurationVerifier.h"

#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/FIFO2Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelFEDDataTypes.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelDecodedFEDRawData.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelSLinkHeader.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelSLinkTrailer.h"
#include "PixelCalibrations/include/PixelCalibrationFactory.h"
#include "PixelFEDInterface/include/PixelFEDFifoData.h"

#include "pixel/exception/Exception.h"

#include "PixelFEDSupervisor/include/monitor/FEDConfigurationInfoSpaceHandler.h"
#include "PixelFEDSupervisor/include/monitor/PixFEDInfoSpaceHandler.h"
#include "PixelFEDSupervisor/include/monitor/PixFEDInfoSpaceUpdater.h"

#include "toolbox/fsm/FailedEvent.h"

#define DO_TEST

using namespace std;
namespace {
    const bool printFIFO = false; // print data fifo content for each event 
    const bool writeStatus = false; //write status file for each FED
    const bool clearBlacklistAtResume = true; 
    const bool clearBlacklistAtStart = true; 
    const bool clearBlacklistAtConfigure = true; 
    const bool Print = false;  // additional debug printout
  const int waitForTTSPrintout = 6; // number of loops before TTS state is printed, def=60->10min 
}

XDAQ_INSTANTIATOR_IMPL(pixel::PixelFEDSupervisor)

pixel::PixelFEDSupervisor::PixelFEDSupervisor(xdaq::ApplicationStub *const stub)
    : pixel::utils::XDAQAppWithFSMPixelBase(stub, unique_ptr<pixel::hwlayer::DeviceBase>(new pixel::TCADeviceFED())),
    PixelFEDSupervisorConfiguration(&runNumber_, &outputDir_, this),
    sv_logger_(getApplicationLogger()),
    FSMStateSummaryUpdaterP_(nullptr),
    FSMStateSummaryP_(nullptr)
    {

    phlock_ = new toolbox::BSem(toolbox::BSem::FULL, true);
    workloopContinue_ = false;

    // Create the InfoSpace holding all configuration information.
    cfgInfoSpaceP_ = unique_ptr<pixel::FEDConfigurationInfoSpaceHandler>(new pixel::FEDConfigurationInfoSpaceHandler(this));
    // Instance is automatically linked to a certain crate with equal number
    // by definition
    crate_ = this->getApplicationDescriptor()->getInstance();
    stub->getDescriptor()->setAttribute("icon", "PixelFEDSupervisor/html/PixelFEDSupervisor.bmp");
    maximumActiveFEDs=10;
    getApplicationInfoSpace()->fireItemAvailable("maximumActiveFEDs", &maximumActiveFEDs);
    console_ = new stringstream();
    eventNumber_ = 0;
    countLoopsThisRun_ = -1;

    theGlobalKey_ = 0;
    theNameTranslation_ = 0;
    theDetectorConfiguration_ = 0;
    theFEDConfiguration_ = 0;
    theCalibObject_ = 0;
    theGlobalDelay25_ = 0;

    PixelSupervisor_ = 0;
    PixelDCStoFEDDpInterface_ = 0;

    theFEDCalibrationBase_ = 0;

    physicsRunningSentRunningDegraded = false;
    physicsRunningSentSoftErrorDetected = false;

    buffer64 = new uint64_t[2000000];

    // Miscellaneous variables initialised
    if (getenv("BUILD_HOME") == 0) {
        htmlbase_ = string(getenv("XDAQ_ROOT")) + "/htdocs/PixelFEDSupervisor/html/";
        datbase_ = string(getenv("XDAQ_ROOT")) + "/dat/PixelFEDInterface/dat/";
    } else {
        htmlbase_ = string(getenv("BUILD_HOME")) + "/pixel/PixelFEDSupervisor/html/";
        datbase_ = string(getenv("BUILD_HOME")) + "/pixel/PixelFEDInterface/dat/";
    }

    // PhysicsRunning
    readSpyFifo3 = false;
    readErrorFifo = true;
    fifo3errcntr = false;
    readFifoStatusAndLFF = true;
    useSEURecovery = false;
    doSEURecovery = false;
    doOOSRefresh = true;
    OOSRefreshPeriod = 30.; // period in seconds, default 60

    // Initialize the WorkLoop
    workloop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop("PixelFEDSupervisorWorkLoop", "waiting");
    physicsRunning_ = toolbox::task::bind(this, &PixelFEDSupervisor::PhysicsRunning, "PhysicsRunning");
    physicsrunning_workloop_sleep_ = 10;  // in sec. must be =< than OOS refresh
    timesphysicsrunningcalled=0;
    
    monitor_workloop_ = toolbox::task::getWorkLoopFactory()->getWorkLoop("PixelFEDSupervisorMonitorWorkLoop", "waiting");
    monitorUpdate_ = toolbox::task::bind(this, &PixelFEDSupervisor::monitorUpdate, "monitorUpdate");
    monitorUpdate_workloop_sleep_ = 5;
    times_monitorUpdatecalled=0;


    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::ReadOSDValues, "ReadOSDValues", XDAQ_NS_URI);

    // Recover
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::Recover, "Recover", XDAQ_NS_URI);
    //
    b2in::nub::bind(this, &PixelFEDSupervisor::b2inEvent);

    // SOAP Bindings to Low Level Commands and Specific Algorithms
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::ReadDataAndErrorFIFO, "ReadDataAndErrorFIFO", XDAQ_NS_URI);
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::ReadFIFO, "ReadFIFO", XDAQ_NS_URI);
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::ReadErrorFIFO, "ReadErrorFIFO", XDAQ_NS_URI);
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::SetAutoPhases, "SetAutoPhases", XDAQ_NS_URI);
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::FindPhasesNow, "FindPhasesNow", XDAQ_NS_URI);
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::SetCalibNEvents, "SetCalibNEvents", XDAQ_NS_URI);
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::ReadRSSI, "ReadRSSI", XDAQ_NS_URI);
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::ReadOSDFifo, "ReadOSDFifo", XDAQ_NS_URI);
    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::SendBackProblemChannels, "SendBackProblemChannels", XDAQ_NS_URI);

    xoap::bind<PixelFEDSupervisor>(this, &PixelFEDSupervisor::FEDCalibrations, "FEDCalibrations", XDAQ_NS_URI);
}

pixel::PixelFEDSupervisor::~PixelFEDSupervisor() {
    hwReleaseImpl();
}


void
pixel::PixelFEDSupervisor::hwConnectImpl() {
}

void
pixel::PixelFEDSupervisor::hwReleaseImpl() {
}


void
pixel::PixelFEDSupervisor::setupInfoSpaces() {
   // Make sure the configuration settings are up-to-date.
   
   for(long unsigned int genericFEDid=0; genericFEDid<maximumActiveFEDs; genericFEDid++){
       fedHwDeviceMap[genericFEDid] = new pixel::hwlayertca::FEDHwDeviceTCA();
    }
    for(fedHWMap::iterator it=fedHwDeviceMap.begin(); it!=fedHwDeviceMap.end(); it++){
        fedUpdaterMap[it->first] = fedInfoSpaceUpdaterPtr(new fedInfoSpaceUpdater(*(it->second), *this));
        fedHandlerMap[it->first] = fedInfoSpaceHandlerPtr(new fedInfoSpaceHandler(this,"pixel-fed-monitor-"+pixel::utils::to_string(it->first), fedUpdaterMap[it->first].get(),it->first));
    }
    cfgInfoSpaceP_->readInfoSpace();
    FSMStateSummaryUpdaterP_ = std::unique_ptr<pixel::FSMStateSummaryUpdater>(new pixel::FSMStateSummaryUpdater(*this));
    FSMStateSummaryP_ = std::unique_ptr<pixel::utils::FSMStateSummaryHandler>(new pixel::utils::FSMStateSummaryHandler(*this, FSMStateSummaryUpdaterP_.get()));
    // Register all InfoSpaceItems with the Monitor
    cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
    appStateInfoSpace_.registerItemSets(monitor_, webServer_);
    FSMStateSummaryP_->registerItemSets(monitor_, webServer_);
    for(auto it=fedHandlerMap.begin(); it!=fedHandlerMap.end(); it++){
        it->second->registerItemSets(monitor_, webServer_);
    }
}

bool pixel::PixelFEDSupervisor::isFEDchannelActive(unsigned int iFED,unsigned int ichannel){
    auto ifed_iter=activeFEDChannelMap.find(iFED);
    if (ifed_iter!=activeFEDChannelMap.end()){
        if (ifed_iter->second.find(ichannel)!=ifed_iter->second.end()){
            return true;
        }
    }
    return false;
}


void
pixel::PixelFEDSupervisor::DetectSoftError(string const channel_info,
                                           int const fedinfo) {
    try {
        if (PixelSupervisor_ != 0 && !physicsRunningSentSoftErrorDetected) {
            Attributes parameters;
            parameters["Supervisor"] = "PixelFEDSupervisor";
            parameters["Instance"] = pixel::utils::to_string(crate_);
            parameters["FEDid"] = pixel::utils::to_string(fedinfo);
            parameters["ChannelInfo"] = channel_info;
            pixel::utils::SOAPER::send(PixelSupervisor_, "DetectSoftError", parameters);
            physicsRunningSentSoftErrorDetected = true;
        }
    }
    catch (xcept::Exception &ex) {
        ERROR("PixelFEDSupervisor::DetectSoftError - Failed to send DetectSoftError to PixelSupervisor. Exception: " << ex.what());
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, "PixelFEDSupervisor::DetectSoftError - Failed to send DetectSoftError to PixelSupervisor.", ex);
        this->notifyQualified("fatal", f);
    }
}

// Write Error FIFO and Data FIFO 3 contents to files
bool pixel::PixelFEDSupervisor::PhysicsRunning(toolbox::task::WorkLoop *w1) {
    //not update every time
    double sleepphysicsrunning_workloop = physicsrunning_workloop_sleep_; // in seconds
    const double sleepunits = 1e6;   // in microseconds, so 1sec
    if (timesphysicsrunningcalled < (sleepphysicsrunning_workloop * 1e6 / sleepunits)) {
      usleep(sleepunits); // sleep 1 sec
      timesphysicsrunningcalled++;  // count time in seconds 
      return true;
    } else {
      timesphysicsrunningcalled = 0;
    }
    bool OOSRefreshNow = false;
    if(Print) cout<<"PhysicsFEDSupervisor::PhysicsRunning - physics workloop "<<endl;

    pixel::utils::Timer wlTimer,
                        statusTimer, statusTimerHW,
                        spyTimer, spyTimerHW,
                        errTimer, errTimerHW;
    wlTimer.start();

    // Skip if there are no active feds in this fed
    if (vmeBaseAddressAndFEDNumberAndChannels_.size() == 0)
        return true;

    try { //hardware and SOAP
        DEBUG(" loops " << countLoopsThisRun_);
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddressAndFEDNumberAndChannels_.begin()->first.first];
        const int newEventNumber = iFED->readEventCounter();
	    // If the Event Number of any FED incremented, it'd mean an increment for all FEDs - sdas
        const bool newEvent = (newEventNumber != eventNumber_), 
	    firstCallThisRun = errorCountMap.empty() && lffMap.empty() && fifoStatusMap.empty();

        DEBUG(" event " << newEventNumber << " " << eventNumber_ << " " << newEvent << " " << firstCallThisRun);
 
#ifdef DO_TEST
        if(Print) 
            cout<<"PhysicsFEDSupervisor::PhysicsRunning - event " << newEventNumber << " " << eventNumber_ << " " << newEvent << " " << firstCallThisRun<<endl;
#endif // DO_TEST
 
        // logic to spy one FED on each entry to workloop, rotating through
        static uint lastFEDSpied;
        static bool spyNextFED; //Flag to indicate updated baseline calibration information available.
        static int countLoops = -1;
        if (firstCallThisRun) {
            lastFEDSpied = 32768; //nonsense number
            spyNextFED = true;
            DEBUG(" spy " << lastFEDSpied << " " << spyNextFED);
            OOSRefreshTimer_.start();
        }

        DEBUG("countLoops=" << countLoops << ", countLoopsThisRun_=" << countLoopsThisRun_ << ", newEventNumber=" << newEventNumber); //for making sure the counters work for the above line

        ++countLoops;
        ++countLoopsThisRun_;
        uint counter = 1;

        DEBUG(" FED loop: event " << newEventNumber << " countLoops " << countLoops
           << " countLoopsThisRun " << countLoopsThisRun_
           << " counter " << counter);

        // Do periodic reset of cummulative channel OOS counters
        if (doOOSRefresh) {
            if (OOSRefreshTimer_.elapsedtime() > OOSRefreshPeriod) {
                OOSRefreshNow = true;
                OOSRefreshTimer_.restart();
                INFO("Timer expired Refreshing OOS_nb_CH counters n=" << OOSRefreshTimer_.ntimes()
                  << " Avg time between refresh now =" << OOSRefreshTimer_.avgtime());
            } else
                OOSRefreshNow = false;
        }

        map<pair<ulong, uint>, set<uint> >::iterator vmeBaseAddress_FEDNumber_channel;
        for (vmeBaseAddress_FEDNumber_channel=vmeBaseAddressAndFEDNumberAndChannels_.begin(); vmeBaseAddress_FEDNumber_channel!=vmeBaseAddressAndFEDNumberAndChannels_.end(); ++vmeBaseAddress_FEDNumber_channel) {
            //check if we want to immediately kill this workloop
            //return 'true' because we don't want workloop_->cancel to throw an exception
            phlock_->take();
            if (!workloopContinue_) {
                phlock_->give();
                return true;
            }
            phlock_->give();

            const ulong vmeBaseAddress = vmeBaseAddress_FEDNumber_channel->first.first;
            const uint fednumber = vmeBaseAddress_FEDNumber_channel->first.second;
            iFED = FEDInterface_[vmeBaseAddress];

#ifdef DO_TEST
	        if(Print) {
	            cout<<"PhysicsFEDSupervisor::PhysicsRunning AUTO registers for FED "
		            <<fednumber<<" event "<<newEventNumber<<endl;
	            iFED->checkAutoMaskRegisters(); // print raw SEU registers 
	            //if(countLoops==0) {iFED->disableBE(true); cout<<"disable BE"<<endl;} // disable backend for testing only
	        }
#endif // DO_TEST

            if (OOSRefreshNow)  {
#ifdef DO_TEST // additional printout
	            if(Print) cout<<"PhysicsFEDSupervisor::PhysicsRunning - reset OOS counters for FED "
			    <<fednumber<<" event "<<newEventNumber<<endl;
	            iFED->checkAutoMaskRegisters(); // print raw SEU registers 
#endif // DO_TEST 
	            iFED->resetOOSCounters(false); //true reset all, false unmasked only
	        }

            // SEU stuff. We want to check here if we're already in RunningDegraded
            if (doSEURecovery && useSEURecovery && iFED->runDegraded() && !physicsRunningSentRunningDegraded && getCurrentStateName() == "Running") {
                INFO("FED " << fednumber << " tells us to go to RunningDegraded");
		        if(Print) cout<<"PhysicsFEDSupervisor::PhysicsRunning - do SEU Recovery"<<endl;

                try {
                    if (PixelSupervisor_ != 0) {
                        Attributes parameters;
                        parameters["Supervisor"] = "PixelFEDSupervisor";
                        parameters["Instance"] = pixel::utils::to_string(crate_);
                        pixel::utils::SOAPER::send(PixelSupervisor_, "DetectDegradation", parameters);
                        physicsRunningSentRunningDegraded = true;
                    }
                }
                catch (xcept::Exception &ex) {
                    ERROR("PixelFEDSupervisor::PhysicsRunning - Failed to send DetectDegradation to PixelSupervisor. Exception: " << ex.what());
                    XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, "PixelFEDSupervisor::PhysicsRunning - Failed to send DetectDegradation to PixelSupervisor.", ex);
                    this->notifyQualified("fatal", f);
                }
            } // end if

            // Check for SEUs (auto masked channels)
            if (useSEURecovery) {
	            if(Print) cout<<"PhysicsFEDSupervisor::PhysicsRunning - use SEU Recovery"<<endl;
	            // Check for channels that got turned off, likely due to SEUs
	            if (iFED->checkFEDChannelSEU()) {
		            WARN("Detected soft error using FED channel monitoring");
		            //place to put a threshold per FED? kme
		            bitset<48> channelsSEU = iFED->getFEDChannelSEU();
		            if (doSEURecovery && !physicsRunningSentSoftErrorDetected) {
		                INFO("Will trigger the DetectSoftError.");
		                DetectSoftError(channelsSEU.to_string(), fednumber); // send a message to PixelSupervisor
		            }
	            }
            } // if useSEURecovery
	    
            statusTimer.start();

            // Read the FIFO status register and LFF
            uint32_t LFFbit = 0;        //want to keep this in wider scope so that we can see it later
            if (readFifoStatusAndLFF) { // LFF and fifo status appear to be available at 40 MHz
	            if(Print) cout<<"PhysicsFEDSupervisor::PhysicsRunning - read status and LFF"<<endl;
	            // Read fifo status
	            statusTimerHW.start();
	            uint fstat = iFED->getFifoStatus();
	            statusTimerHW.stop();
	            fstat = fstat & 0x3ff;
	            DEBUG("ReadFifoStatus: stat " << hex << fstat << dec);
	            // accumulate statistics in map of moments
	            for (uint ibit=0; ibit<10; ++ibit)
		            fifoStatusMap[fednumber][ibit].push_back((fstat >> ibit) & 0x1LL);
	      
	            // Read event counter register (including Link Full Flag)
	            //ReadSpyPause[31]    SLinkLFF[30]   all zero [29..24]   EventCounter[23..0]
	            statusTimerHW.start();
	            LFFbit = iFED->linkFullFlag();
	            statusTimerHW.stop();
	            DEBUG("LFFbit = " << LFFbit);
	            lffMap[fednumber].push_back(LFFbit);
	      
	            // debug printout
	            if (lffMap[fednumber].count() >= waitForTTSPrintout) {  //= 60 every 10 min  d.k. 
		            toolbox::TimeVal timeOfDay = toolbox::TimeVal::gettimeofday();
		            cout<< "Status FED"<< fednumber << " " << runNumber_ 
		                << " Time: " << timeOfDay.toString("%c", timeOfDay.tz())<< endl 
		                << " FED=" << fednumber << " LFF status=" << 100 * lffMap[fednumber].mean() << "% (Since last printout)" << endl
		                << "FIFO I Almost Full: N(1-9)=" << 100 * fifoStatusMap[fednumber][0].mean() 
		                << "% NC(10-18)=" << 100 * fifoStatusMap[fednumber][2].mean()
		                << "% SC(19-27)=" << 100 * fifoStatusMap[fednumber][4].mean() 
		                << "% S(27-36)=" << 100 * fifoStatusMap[fednumber][6].mean() << "%" << endl
		                << "FIFO II Nearly Full: N(1-9)=" << 100 * fifoStatusMap[fednumber][1].mean() 
		                << "% NC(10-18)=" << 100 * fifoStatusMap[fednumber][3].mean()
		                << "% SC(19-27)=" << 100 * fifoStatusMap[fednumber][5].mean() 
		                << "% S(27-36)=" << 100 * fifoStatusMap[fednumber][7].mean() << "%" << endl
		                << "FIFO III Almost Full: UP=" << 100 * fifoStatusMap[fednumber][8].mean() 
		                << "% DOWN=" << 100 * fifoStatusMap[fednumber][9].mean() << "%" << endl 
		                <<" Number of errors read=" << errorCountMap[fednumber][0] << " including resets" << endl;

		            if(writeStatus) {  // write status files
		  /*
		   *dataFileS_[fednumber]
		   << "Status FED"<< fednumber << " " << runNumber_ 
		   << " Time: " << timeOfDay.toString("%c", timeOfDay.tz())<< endl 
		   << " FED=" << fednumber << " LFF status=" << 100 * lffMap[fednumber].mean() << "% (Since last printout)" << endl
		   << "FIFO I Almost Full: N(1-9)=" << 100 * fifoStatusMap[fednumber][0].mean() << "% NC(10-18)=" << 100 * fifoStatusMap[fednumber][2].mean()
		   << "% SC(19-27)=" << 100 * fifoStatusMap[fednumber][4].mean() << "% S(27-36)=" << 100 * fifoStatusMap[fednumber][6].mean() << "%" << endl
		   << "FIFO II Nearly Full: N(1-9)=" << 100 * fifoStatusMap[fednumber][1].mean() << "% NC(10-18)=" << 100 * fifoStatusMap[fednumber][3].mean()
		   << "% SC(19-27)=" << 100 * fifoStatusMap[fednumber][5].mean() << "% S(27-36)=" << 100 * fifoStatusMap[fednumber][7].mean() << "%" << endl
		   << "FIFO III Almost Full: UP=" << 100 * fifoStatusMap[fednumber][8].mean() << "% DOWN=" << 100 * fifoStatusMap[fednumber][9].mean() << "%" << endl 
		   <<" Number of errors read=" << errorCountMap[fednumber][0] << " including resets" << endl;
		  */
		            } // if write status 

		            // add the file readout
		            timeOfDay = toolbox::TimeVal::gettimeofday();
		            INFO("Time: " << timeOfDay.toString("%c", timeOfDay.tz()));
		            iFED->printTTSState(); // dumps to log file; implement nicer scheme
		
		            if(0) { // does it do anything?
		                INFO(" Error report");
		                for (int iw = 1; iw < 37; ++iw) {
		                    uint32_t ondata = iFED->getErrorReport(iw - 1); // FIXME - PixelPh1FEDInterface's empty method
		                    cout<<" IS THIS EVER DONE? " <<endl;
		                    if ((ondata & 0x3fff) > 0) {
		                        INFO(" chnl: " << iw << " Num. of OOS: " << (ondata & 0x3fff));
		                    }
		                    if ((ondata & 0xffffc000) > 0)
		                        INFO(" chnl: " << iw << " Num. of NOR Errs: " << ((ondata & 0xffffc000) >> 14));
		                    if (fifo3errcntr) // Add the timeouts
		                        ondata = iFED->getTimeoutReport(iw - 1); // FIXME - PixelPh1FEDInterface's empty method
		    
		                    if ((ondata & 0x3fff) > 0)
			                    INFO(" chnl: " << iw << " Num. of Timeouts: " << (ondata & 0x3fff));
		                } // end for
		            } // end if(0)
		    
		            lffMap[fednumber].clear();
		            for (uint ii=0; ii<10; ++ii)
		                fifoStatusMap[fednumber][ii].clear();
		
	            } // if count 
	            statusTimer.stop();
	        } // if readfifo

            // Online Readbacker implementation:
	        //determines how often the readbackvalues are saved
            const int nReadbackerWoorkLoops = 60; //60*10sec=10min 
            if ((countLoops % nReadbackerWoorkLoops) == 0) {      // not every woorloop round
		        if(Print) cout<<"PhysicsFEDSupervisor::PhysicsRunning - do readback"<<endl;
                const uint roc = countLoops / nReadbackerWoorkLoops % 8; // cycle through rocs
                Attributes pars;
                pars["VMEBaseAddress"] = pixel::utils::to_string(vmeBaseAddress);
                pars["Channel"] = "*";
                pars["RocHi"] = pixel::utils::to_string(roc);
                pars["RocLo"] = pixel::utils::to_string(roc);
                ArmOSDFifo(makeSOAPMessageReference("armOSDFifo", pars));

                Attributes pars2;
                pars2["VMEBaseAddress"] = pixel::utils::to_string(vmeBaseAddress);

                vector<string> names(1, "Values");
                string valuesAttr = getSomeSOAPCommandAttributes(ReadOSDValues(makeSOAPMessageReference("ReadOSDValues", pars2)), names)["Values"];
                if (valuesAttr=="problem")
                    XCEPT_RAISE(pixel::exception::Exception, "PixelFEDSupervisor::PhysicsRunning - Failed to ReadOSDValues");


                stringstream ss(valuesAttr);
                uint32_t buf;
                vector<uint32_t> values;

                while (ss >> buf)
                    values.push_back(buf);

                if (values.size()!=24) // one 32-bit word result for each fiber
                    XCEPT_RAISE(pixel::exception::Exception, "PixelFEDSupervisor::PhysicsRunning - Failed to extract values");

                toolbox::TimeVal timeOfDay = toolbox::TimeVal::gettimeofday();
                const string time = timeOfDay.toString("%c", timeOfDay.tz());

                for (uint i=0; i<2*values.size(); ++i) {
                    uint32_t word = values.at(i/2);
                    const bool tbmch = (i+1)%2==0;
                    word = (tbmch ? (word >> 16) : word & 0xFFFF);
                    if (!theNameTranslation_->ROCNameFromFEDChannelROCExists(fednumber, (i + 1), 0))
                        continue;

                    const pos::PixelROCName &tempname = theNameTranslation_->ROCNameFromFEDChannelROC(fednumber, (i + 1), 0);

                    const uint n = (tempname.detsub() == 'B' && (tempname.layer()==1 || (tempname.layer()==2)) ?  2*tempname.layer(): 8);
                    if(!theNameTranslation_->ROCNameFromFEDChannelROCExists(fednumber, i+1, ((word >> 12) & 0xF) % n) ||
                        (n == 2 && roc > 1) ||
                        (n == 4 && roc > 3))
                        continue;
                    const bool rocnumberConsistent = ((((word >> 12) & 0xF) % n + 1) % n) == roc;
                    string rocname_string = theNameTranslation_->ROCNameFromFEDChannelROC(fednumber, i+1, ((word >> 12) & 0xF) % n).rocname();

                    string ReadBacksetting;
                    switch((word >> 8) & 0xF) {
                      case 8 :
                         ReadBacksetting = "vd  ";
                         break;
                      case 9 :
                         ReadBacksetting = "va  ";
                         break;
                      case 10 :
                         ReadBacksetting = "vana";
                         break;
                      case 11 :
                         ReadBacksetting = "vbg ";
                         break;
                      case 12 :
                         ReadBacksetting = "iana";
                         break;
                      default :
                         ReadBacksetting = "bad ";
                   }
                    ReadBacksetting.resize(4, ' ');
                    ofstream readbackerFile;
                    rocname_string.resize(36, ' ');
                    readbackerFile.open((outputDir() + "/ReadbackerFED_" + pixel::utils::to_string(fednumber) + "_" + runNumber_ + ".txt").c_str(), ios::app);
                    readbackerFile << time << '|' << fednumber << '|' << rocname_string << '|' << roc << '|' << rocnumberConsistent << '|' << ReadBacksetting << '|' << (word & 0xFF) << '\n';
                    readbackerFile.close();
                }
            }  // end readback loop 

#ifdef DO_TEST
            if(printFIFO) {
	            if(Print) cout<<"PhysicsFEDSupervisor::PhysicsRunning - readFIFO "<<newEvent<<endl;
	            if (newEvent) {  
	                if(0) { // read fifo1 
		                cout<<"PhysicsFEDSupervisor::PhysicsRunning - fifo1 " <<endl;
		                iFED->setFIFO1(8);  // start from 0
		                PixelPh1FEDInterface::digfifo1 d = iFED->readFIFO1(true);
		                iFED->setFIFO1(9);
		                d = iFED->readFIFO1(true);
		                iFED->setFIFO1(11);
		                d = iFED->readFIFO1(true);
		            } // if ifio1
		            if(1) { // read fifo3
		                const int status = iFED->spySlink64(buffer64);
		                cout<<"PhysicsFEDSupervisor::PhysicsRunning - fifo3 length " << status<<endl;
		  
		                if (status > 0) { 
		                    cout << "Contents of Spy FIFO 3" << endl;
		                    cout << "----------------------" << endl;
		                    pos::PixelSLinkHeader h(buffer64[0]);
		                    cout<<"header: event "<<h.getLV1_id()<<" bx "<<h.getBX_id()<<" type "<<h.getEvt_ty()<<endl;
		                    pos::PixelSLinkTrailer t(buffer64[status-1]);
		                    cout<<"trailer: stat "<<t.getEVT_stat()<<" len "<<t.getEvt_lgth()<<" tts "<<t.getTTS()<<endl;
		                    //for (int i=0; i<status; ++i) {
		                    //cout << "Clock " << i << " = 0x" << hex << buffer64[i] << dec << endl;
		                    //	if(i==0) {  // header 
		                    //	} else if (i==(status-1)) {  //trailer 
		                    //	}
		                    // }
		    
		                    FIFO3Decoder decode3(buffer64);
		                    //for (int i = 0; i <= status; ++i)
		                    //cout<< "Clock " << setw(2) << i << " = 0x " << hex << setw(8) << (buffer64[i] >> 32) << " " << setw(8) << (buffer64[i] & 0xFFFFFFFF) << dec << endl;
		                    cout<< "FIFO3Decoder thinks:\n"<< "nhits: " << decode3.nhits() << endl;
		                    for (unsigned i = 0; i < decode3.nhits(); ++i) {
		                        cout << "#" << i << ": ch: " << decode3.channel(i)
			                        << " rocid: " << decode3.rocid(i)
			                        << " dcol: " << decode3.dcol(i)
			                        << " pxl: " << decode3.pxl(i) << " pulseheight: " << decode3.pulseheight(i)
			                        << " col: " << decode3.column(i) << " row: " << decode3.row(i) << endl;
		                    } // for loop 		    
		                } // status 
		            } // if(0)
	            } // new event
	        } // printFIFO
#endif
	        // Drain DataFIFO 3 and write to file
	        if (readSpyFifo3 && newEvent && ((countLoops % 10) == 0) ) {  // presently not used
		
	            // Drain DataFIFO 3 and write to file
	            spyTimerHW.start();
	            if(Print) cout<<"PhysicsFEDSupervisor::PhysicsRunning - enableSpyFifo" <<endl;
	            if (iFED->isNewEvent(1)) { // FIXME - PixelPh1FEDInterface's empty method
		            //this checks for zeros - 0=New event, spy fifo not ready ready to be read
		            iFED->enableSpyMemory(0);
		            //if (iFED->isWholeEvent(1)) { //this checks for 1's - spy fifo ready to be read
		            const int dataLength = iFED->spySlink64(buffer64);
		            spyTimerHW.stop();
		            DEBUG(" fifo3 length " << dataLength);
		
		            if (dataLength > 0) { // Add protection from Will
		                fwrite(buffer64, sizeof(uint64_t), dataLength, dataFile_[fednumber]);
		  
		                // Debug
		                stringstream linfo;
		                FIFO3Decoder decode3(buffer64);
		                for (int i = 0; i <= dataLength; ++i)
		                    linfo << "Clock " << setw(2) << i << " = 0x " << hex << setw(8) << (buffer64[i] >> 32) << " " << setw(8) << (buffer64[i] & 0xFFFFFFFF) << dec << endl;
		                linfo << "FIFO3Decoder thinks:\n"<< "nhits: " << decode3.nhits() << endl;
		                for (unsigned i = 0; i < decode3.nhits(); ++i) {
		                    linfo << "#" << i << ": ch: " << decode3.channel(i)
			                        << " rocid: " << decode3.rocid(i)
			                        << " dcol: " << decode3.dcol(i)
			                        << " pxl: " << decode3.pxl(i) << " pulseheight: " << decode3.pulseheight(i)
			                        << " col: " << decode3.column(i) << " row: " << decode3.row(i) << endl;
		                } // for loop
		                DEBUG(linfo.str());
		            } else {
		                stringstream fedinfo;
		                fedinfo << "FED ID = " << fednumber << " SpySlink read Failure"
			            << ", VME Base Address = 0x" << hex << vmeBaseAddress << dec
			            << ", Event Number = " << newEventNumber << " Wrong data length " << dataLength << endl;
		                // dump first 10 words
		                for (int i = 0; i <= 4; ++i)
		                    fedinfo << " " << i << " = 0x" << hex << buffer64[i] << dec << endl;
		  
		                INFO(fedinfo.str());
		            } // end protection from Will
		
		            // Compare FED ID from SLink Header and present FED Number
		            const uint fednumber_header = (buffer64[0] & 0x00000000000fff00) >> 8;
		            if (fednumber_header != fednumber) {
		            INFO("FED ID = " << fednumber << " while FED ID from SLink Header = " << fednumber_header
		                <<", VME Base Address = 0x" << vmeBaseAddress
		                << ", Event Number = " << newEventNumber);
		            }
		            ////} else {
		            ////INFO("PixelFEDSupervisor::PhysicsRunning - Whole event not found in FED ID " << fednumber << " after 1 second");
		            ////spyTimerHW.stop();
		            ////}
	            } else { // if new event
		            INFO("PixelFEDSupervisor::PhysicsRunning - Spy-fifo3 New Event never seen for FED ID " << fednumber << " after 1 second");
		            spyTimerHW.stop();
	            }  // end if new event
	            spyTimerHW.start();
	            iFED->enableSpyMemory(1); //prevent always disabled
	            spyTimerHW.stop();
	            spyTimer.stop();
	            spyNextFED = (lastFEDSpied == fednumber || lastFEDSpied == 32768); //we'll spy the next FED, but only if !spiedFED on this entrance
	        } // if readSpyFifo3

            ++counter;

            //Drain Error FIFO and write to file (Do this every time workloop is called)
            errTimer.start();
            if (readErrorFifo) {  // read error fifo
	            if(Print) cout<<"PhysicsFEDSupervisor::PhysicsRunning - readErrorFifo" <<endl;
	            uint32_t errBuffer[(8 * pos::errorDepth)];
	            errTimerHW.start();
	            const uint errorLength = iFED->drainErrorFifo(errBuffer);
	            errTimerHW.stop();
	            if (errorLength > 0) {
#ifdef DO_TEST
		      if(Print) {
			cout<<"Error fifo not empty for FED " << fednumber << " " << errorLength<<endl;
			for (uint i_err=0; i_err<errorLength; ++i_err) {
			  PixelFEDFifoData decoder;
			  if(i_err<50) decoder.decodeErrorFifo(errBuffer[i_err]);  
			}
		      } // if Print
#endif // DO_TEST
		      
		      // add time and error length
		      struct timeval tv;
		      gettimeofday(&tv, NULL);
		      fwrite(&tv.tv_sec, sizeof(double), 1, errorFile_[fednumber]);
		            // sizeof(unsigned long) -> sizeof(unsigned int)
		            fwrite(&errorLength, sizeof(uint), 1, errorFile_[fednumber]);
		            fwrite(errBuffer, sizeof(uint32_t), errorLength, errorFile_[fednumber]);
		
		            //fill map
		            errorCountMap[fednumber][0] += errorLength;
		            // could unpack the errors and count by channel
	            } else if (firstCallThisRun) {
	                errorCountMap[fednumber][0] = 0;
	            }  // if errorlength>0
	        } // if readErrorFifo
	        errTimer.stop();
        } // End FED loop

        // End and update the event counter
        eventNumber_ = newEventNumber;
    
        // tuning output from timers
        wlTimer.stop();
    
        DEBUG("PhysicsRunning total time=" << (wlTimer.tottime()) << endl
	    << "PhysicsRunning readFifoStatus times=" << (statusTimer.ntimes()) << " total time =" << (statusTimer.tottime())
	    << " avg time =" << (statusTimer.avgtime()) << endl
	    << "PhysicsRunning readSpy times=" << (spyTimer.ntimes()) << " total time =" << (spyTimer.tottime())
	    << " avg time =" << (spyTimer.avgtime()) << endl
	    << "PhysicsRunning readErrorFifo=" << (errTimer.ntimes()) << " total time =" << (errTimer.tottime())
	    << " avg time =" << (errTimer.avgtime()) << endl
	    << "PhysicsRunning readFifoStatusHW times=" << (statusTimerHW.ntimes()) << " total time =" << (statusTimerHW.tottime())
	    << " avg time =" << (statusTimerHW.avgtime()) << endl
	    << "PhysicsRunning readSpyHW times=" << (spyTimerHW.ntimes()) << " total time =" << (spyTimerHW.tottime())
	    << " avg time =" << (spyTimerHW.avgtime()) << endl
	    << "PhysicsRunning readErrorFifoHW times=" << (errTimerHW.ntimes()) << " total time =" << (errTimerHW.tottime())
	    << " avg time =" << (errTimerHW.avgtime()) << endl);

    }
    catch (HAL::BusAdapterException &hardwareError) {
        string const msg_error_vat = "Hardware error in the FED Physics workloop. Message: " + string(hardwareError.what());
        ERROR(msg_error_vat);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_vat, hardwareError);
        this->notifyQualified("fatal", f);
    }
    catch (xcept::Exception &xdaqError) {
        string const msg_error_tdp = "XDAQ error in the FED Physics workloop. Message: " + string(xdaqError.what());
        ERROR(msg_error_tdp);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_tdp, xdaqError);
        this->notifyQualified("fatal", f);
    }
    catch (...) {
        string const msg_error_kun = "Unknown exception caught in the FED Physics workloop.";
        ERROR(msg_error_kun);
        pixel::exception::Exception trivial_exception("PixelFEDSupervisorException", "module", msg_error_kun, 3147, "PixelFEDSupervisor::PhysicsRunning(toolbox::task::WorkLoop*)");
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_kun, trivial_exception);
        this->notifyQualified("fatal", f);
    }

    wlTimer.stop();
    return true;
}

bool 
pixel::PixelFEDSupervisor::monitorUpdate(toolbox::task::WorkLoop *w2){
    //not update every time
    double sleep_monitorUpdate_workloop = monitorUpdate_workloop_sleep_; // in seconds
    double sleepunits = 1e5;                                  // in microseconds
    if (times_monitorUpdatecalled < (sleep_monitorUpdate_workloop * 1e6 / sleepunits)) {
        usleep(sleepunits);
        times_monitorUpdatecalled++;
        return true;
    }
    else {
        times_monitorUpdatecalled = 0;
    }
    for(std::map<unsigned long, fedInfoSpaceHandlerPtr>::iterator it=fedHandlerMap.begin(); it!=fedHandlerMap.end(); it++){
        fedUpdaterMap[it->first]->updateInfoSpaceTrigger(fedHandlerMap[it->first].get());
    }
    DEBUG("Updated FED Monitor");
    return true;
}

xoap::MessageReference
pixel::PixelFEDSupervisor::ArmOSDFifo(xoap::MessageReference msg) {
    vector<string> names;
    names.push_back("VMEBaseAddress");
    ////names.push_back("Channel");
    names.push_back("RocHi");
    names.push_back("RocLo");
    Attributes parametersReceived = getAllSOAPCommandAttributes(msg, names);

    if (parametersReceived[ "VMEBaseAddress"]=="*") {
        pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
        if (tempCalibObject == 0) {
            string const msg_err_calib = "PixelFEDSupervisor::ArmOSDFifo - Calib object is not specified!";
            ERROR(msg_err_calib);
            XCEPT_RAISE(pixel::exception::Exception, msg_err_calib);
        }
        const vector<pair<unsigned, vector<unsigned> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_,
                                                                                                                 theNameTranslation_,
                                                                                                                 theFEDConfiguration_,
                                                                                                                 theDetectorConfiguration_);
        for (vector<pair<unsigned, vector<unsigned> > >::const_iterator fac = fedsAndChannels.begin(), face = fedsAndChannels.end(); fac != face; ++fac) {
            FEDInterface_[theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fac->first)]->armOSDFifo(0,
                                                                                                     atoi(parametersReceived["RocHi"].c_str()),
                                                                                                     atoi(parametersReceived["RocLo"].c_str())); // utca fed only, one register for all ch
        }
    } else
        FEDInterface_[atoi(parametersReceived["VMEBaseAddress"].c_str())]->armOSDFifo(atoi(parametersReceived["Channel"].c_str()),
                                                                                      atoi(parametersReceived["RocHi"].c_str()),
                                                                                      atoi(parametersReceived["RocLo"].c_str()));

    return makeSOAPMessageReference("ArmOSDFifoDone");
}

xoap::MessageReference
pixel::PixelFEDSupervisor::ReadOSDValues(xoap::MessageReference msg) {
    vector<string> names;
    names.push_back("VMEBaseAddress");
    Attributes parametersReceived = getSomeSOAPCommandAttributes(msg, names);

    Attributes returnValues;
    returnValues["Values"] = "problem";

    vector<uint32_t> values = FEDInterface_[atoi(parametersReceived["VMEBaseAddress"].c_str())]->readOSDValues();
    stringstream ss;
    try {
        ss << values.at(0);
        for (size_t i=1; i<24; ++i)
            ss << " " << values.at(i);
    }
    catch (const out_of_range& e) {
        string const msg_info_jzw = "PixelFEDSupervisor::ReadOSDValues - Failed to extract values";
        ERROR(msg_info_jzw);
        XCEPT_RAISE(pixel::exception::Exception, msg_info_jzw);
    }

    returnValues["Values"] = ss.str();
    return makeSOAPMessageReference("ReadOSDValuesDone", returnValues);
}


void
pixel::PixelFEDSupervisor::initializeActionImpl(toolbox::Event::Reference event) {
    INFO("--- INITIALIZE ---");

    // Detect PixelSupervisor
    try {
        PixelSupervisor_ = getApplicationContext()->getDefaultZone()->getApplicationGroup("daq")->getApplicationDescriptor("PixelSupervisor", 0);
        INFO("PixelFEDSupervisor::initializeActionImpl - Instance 0 of PixelSupervisor found.");
    }
    catch (xdaq::exception::Exception &e) {
        string const msg_error_tly = "PixelFEDSupervisor::initializeActionImpl - Instance 0 of PixelSupervisor found.";
        ERROR(msg_error_tly);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_tly, e);
        this->notifyQualified("fatal", f);
    }

    // Detect PixelDCStoFEDDpInterface
    try {
        PixelDCStoFEDDpInterface_ = getApplicationContext()->getDefaultZone()->getApplicationGroup("dcs")->getApplicationDescriptor("PixelDCStoFEDDpInterface", 0);
        INFO("PixelFEDSupervisor::initializeActionImpl - 1 PixelDCStoFEDDpInterface descriptor found");
    }
    catch (xdaq::exception::Exception &e) {
        string const msg_info_jzw = "PixelFEDSupervisor::initializeActionImpl - No PixelDCStoFEDDpInterface descriptor found";
        ERROR(msg_info_jzw);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_info_jzw, e);
    }

    INFO("--- INIZIALIZATION DONE ---");
}

void
pixel::PixelFEDSupervisor::transitionHaltedToConfiguringActionImpl(toolbox::Event::Reference event){
    // check Global Key
    const string global_key = cfgInfoSpaceP_->getString("GlobalKey");

    if (global_key=="-1"){
        ERROR("Global Key not provided for command Configure");
        XCEPT_RAISE(xcept::Exception, "Global Key not provided for Configure command");
    }
    theGlobalKey_ = new pos::PixelConfigKey(atoi((global_key).c_str()));
    if (theGlobalKey_ == 0) {
        string const msg_error_etn = "Failure to create GlobalKey";
        ERROR(msg_error_etn);
        XCEPT_RAISE(xcept::Exception, msg_error_etn);
    }
    INFO("PixelFEDSupervisor::transitionHaltedToConfiguringActionImpl -- Global Key : " << global_key);


    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::transitionHaltedToConfiguringActionImpl(event);
}

void
pixel::PixelFEDSupervisor::configureActionImpl(toolbox::Event::Reference event) {
    try {
        pixel::utils::Timer totalTimer;
        totalTimer.start();

        // Extract the Calib object corresponding to the Global Key
        INFO("Retrieving Calib object from DataBase...");
        PixelConfigInterface::get(theCalibObject_, "pixel/calib/", *theGlobalKey_);
        INFO("Retrieving Calib object from DataBase done.");

        // Using the Global Key configure the FED boards
        jobConfigure(); // jobConfigure needs theCalibObject_ to be already loaded
        physicsRunningSentSoftErrorDetected = false;

        if (dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_) != 0)
            (dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_))->buildROCAndModuleLists(theNameTranslation_, theDetectorConfiguration_);

        // From the Calib object's mode
        // decide what the Control and Mode registers for the various chips on the FEDs must be
        // decide which if any jobs to submit to the main workloop
        submit_workloops();

        totalTimer.stop();

        INFO("PixelFEDSupervisor::configureActionImpl - FED configuration: total time=" + pixel::utils::to_string(totalTimer.tottime()));
        INFO("PixelFEDSupervisor::configureActionImpl - Triggering transition to the Configured state.");
    }
    catch (std::exception &e) {
        string const msg_error_pdr = "Failed to configure FED with exception: " + string(e.what());
        ERROR(msg_error_pdr);
        pixel::exception::Exception *new_exception = dynamic_cast<pixel::exception::Exception *>(&e);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_pdr, *new_exception);
        this->notifyQualified("fatal", f);
    }
    if (theCalibObject_ == 0) { // start the monitor if it is a physics run!!
        if (!monitor_workloop_->isActive())
            monitor_workloop_->activate();
        INFO("Monitoring workloop activated.");
        //To make sure we clear the fifos
        EndOfRunFEDReset(); //reset FED, clear SLink (at Start despite its name)
        if(clearBlacklistAtConfigure)  blacklistFedChannelMap_.clear();
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::configureActionImpl(event);
}

void
pixel::PixelFEDSupervisor::startActionImpl(toolbox::Event::Reference event){
    INFO("--- START ---");

    runNumber_ = pixel::utils::to_string(cfgInfoSpaceP_->getUInt32("runNumber"));
    if (runNumber_ == "0"){
        ERROR("Run Number not provided for command Start");
        XCEPT_RAISE(xcept::Exception, "Run Number not provided for Start command");
    }
    INFO("PixelFEDSupervisor::startActionImpl -- Run Number : "<<runNumber_);

    uint runNumberInt = atoi(runNumber_.c_str());
    if (runNumberInt > numeric_limits<xdata::UnsignedInteger32>::max() || runNumberInt < 0) {
        string const msg_err_rn = "PixelFEDSupervisor::startActionImpl - Run Number = " + runNumber_ + "is greater than numerical limit for xdata::UnsignedInteger32: " + pixel::utils::to_string(numeric_limits<xdata::UnsignedInteger32>::max()) + " or negative";
        ERROR(msg_err_rn);
        XCEPT_RAISE(xcept::Exception, msg_err_rn);
    }

    countLoopsThisRun_ = -1;

    setupOutputDir();

    if (theCalibObject_ == 0) {
        SEUCountReset(); // added 9/10/18 KP
        if(clearBlacklistAtStart)  blacklistFedChannelMap_.clear();

        map<pair<ulong, uint>, set<unsigned int> >::iterator vmeBaseAddressAndFEDNumberAndChannel;
        for (vmeBaseAddressAndFEDNumberAndChannel = vmeBaseAddressAndFEDNumberAndChannels_.begin(); vmeBaseAddressAndFEDNumberAndChannel != vmeBaseAddressAndFEDNumberAndChannels_.end(); ++vmeBaseAddressAndFEDNumberAndChannel) {
            uint fednumber = vmeBaseAddressAndFEDNumberAndChannel->first.second;

            if(readSpyFifo3) dataFile_[fednumber] = fopen((outputDir_ + "/PhysicsDataFED_" + pixel::utils::to_string(fednumber) + "_" + runNumber_ + ".dmp").c_str(), "wb");
            if(writeStatus) dataFileS_[fednumber] = fopen((outputDir_ + "/StatusFED_" + pixel::utils::to_string(fednumber) + "_" + runNumber_ + ".txt").c_str(), "w");
            //dataFileT_[fednumber] = fopen((outputDir_ + "/TransFifo_" + pixel::utils::to_string(fednumber) + "_" + runNumber_ + ".dmp").c_str(), "wb");
            //dataFileS_[fednumber] = fopen((outputDir_ + "/ScopeFifo_" + pixel::utils::to_string(fednumber) + "_" + runNumber_ + ".dmp").c_str(), "wb");
            //timestampFile_[fednumber] = fopen((outputDir_ + "/Timestamp_" + pixel::utils::to_string(fednumber) + "_" + runNumber_ + ".dmp").c_str(), "wb");
            errorFile_[fednumber] = fopen((outputDir_ + "/ErrorDataFED_" + pixel::utils::to_string(fednumber) + "_" + runNumber_ + ".err").c_str(), "wb");
            setbuf(errorFile_[fednumber], NULL); //disable buffering
            uint64_t runNumber64 = atol(runNumber_.c_str());
            uint32_t runNumber32 = atol(runNumber_.c_str());

            if(readSpyFifo3) fwrite(&runNumber64, sizeof(uint64_t), 1, dataFile_[fednumber]);
            ofstream readbackerFile;
            readbackerFile.open((outputDir() + "/ReadbackerFED_" + pixel::utils::to_string(fednumber) + "_" + runNumber_ + ".txt").c_str(), ios::app);
            readbackerFile << "Time|FEDNumber|ROCName|armedROC|rocnumberConsistent|readbacker setting|value \n";
            readbackerFile.close();
            fwrite(&runNumber32, sizeof(uint32_t), 1, errorFile_[fednumber]);
        }

        //EndOfRunFEDReset(); //reset FED, clear SLink (at Start despite its name)

        start_workloops();

        INFO("PixelFEDSupervisor::startActionImpl - Calib object == 0. Physics data taking workloop activated.");
    }

    // Create the FEDCalibration object.
    if (theCalibObject_ != 0) {
        //Create pointers to give to the calibration object
        PixelFEDSupervisorConfiguration *pixFEDSupConfPtr = dynamic_cast<PixelFEDSupervisorConfiguration *>(this);
        SOAPER *soaperPtr = dynamic_cast<SOAPER *>(this);

        PixelCalibrationFactory calibFactory;
        theFEDCalibrationBase_ = calibFactory.getFEDCalibration(theCalibObject_->mode(),
                                                                pixFEDSupConfPtr,
                                                                soaperPtr);

        if (theFEDCalibrationBase_ != 0) {
            //Set control and mode register
            theFEDCalibrationBase_->initializeFED();
        }
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::startActionImpl(event);
}

void
pixel::PixelFEDSupervisor::stopActionImpl(toolbox::Event::Reference event) {
    INFO("--- STOP ---");
    if (theCalibObject_ == 0) {
        stop_workloops();

        for (map<pair<ulong, uint>, set<uint> >::iterator vmeBaseAddressAndFEDNumberAndChannel = vmeBaseAddressAndFEDNumberAndChannels_.begin(); vmeBaseAddressAndFEDNumberAndChannel != vmeBaseAddressAndFEDNumberAndChannels_.end(); ++vmeBaseAddressAndFEDNumberAndChannel) {
            uint fednumber = vmeBaseAddressAndFEDNumberAndChannel->first.second;
            DEBUG("About to close files for fednumber=" << fednumber);
            if(readSpyFifo3) {fclose(dataFile_[fednumber]); dataFile_[fednumber] = 0;}
            //fclose(dataFileT_[fednumber]);
            //dataFileT_[fednumber] = 0;
            if(writeStatus) {fclose(dataFileS_[fednumber]); dataFileS_[fednumber] = 0;}
            //fclose(timestampFile_[fednumber]);
            //timestampFile_[fednumber] = 0;
            fclose(errorFile_[fednumber]);
            errorFile_[fednumber] = 0;
        }
        INFO("PixelFEDSupervisor::stopActionImpl - Closing output files done");

        EndOfRunFEDReset(); //reset FED, clear SLink
    }

    closeOutputFiles();
    reportStatistics();

    if (theFEDCalibrationBase_) {
        INFO("PixelFEDSupervisor::stopActionImpl - There was a FEDCalibrationBase_");
        delete theFEDCalibrationBase_;
        theFEDCalibrationBase_ = 0;
        INFO("But not anymore!");
    }

    physicsRunningSentSoftErrorDetected = false;
    physicsRunningSentRunningDegraded = false;

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::stopActionImpl(event);
}

void
pixel::PixelFEDSupervisor::pauseActionImpl(toolbox::Event::Reference event) {
    INFO("--- PAUSE ---");

    if (theCalibObject_ == 0)
        stop_workloops();

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::pauseActionImpl(event);
}

void
pixel::PixelFEDSupervisor::resumeActionImpl(toolbox::Event::Reference event){
    INFO("--- RESUME ---");

    if (theCalibObject_==0) {
        start_workloops();
        // clear blacklist
        if(clearBlacklistAtResume)  blacklistFedChannelMap_.clear();
        // reset the SEU counters
        SEUCountReset();
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::resumeActionImpl(event);
}

xoap::MessageReference
pixel::PixelFEDSupervisor::Recover(xoap::MessageReference msg) {
    INFO("-- RECOVER --");
    if (getCurrentStateName() != "Failed")
        return makeSOAPMessageReference("RecoverFailed");

    if (workloop_ != 0)
        remove_workloops();

    //in HALT this is done only for Physics running (theCalibObject_==0).
    //now added code to always set these file pointers to 0 immediately after fclose.
    //this code should then be safe to run anytime
    for (map<pair<ulong, uint>, set<uint> >::iterator vmeBaseAddressAndFEDNumberAndChannel = vmeBaseAddressAndFEDNumberAndChannels_.begin(); vmeBaseAddressAndFEDNumberAndChannel != vmeBaseAddressAndFEDNumberAndChannels_.end(); ++vmeBaseAddressAndFEDNumberAndChannel) {
        uint fednumber = vmeBaseAddressAndFEDNumberAndChannel->first.second;
        if (readSpyFifo3 && dataFile_.find(fednumber)!=dataFile_.end() && dataFile_[fednumber]!=0)
                fclose(dataFile_[fednumber]);

        //if (dataFileT_.find(fednumber)!=dataFileT_.end() && dataFileT_[fednumber]!=0)
	//      fclose(dataFileT_[fednumber]);

        if(writeStatus) {fclose(dataFileS_[fednumber]);}

        //if (dataFileS_.find(fednumber)!=dataFileS_.end() && dataFileS_[fednumber]!=0)
	//      fclose(dataFileS_[fednumber]);

        //if (timestampFile_.find(fednumber)!=timestampFile_.end() && timestampFile_[fednumber]!=0)
	//      fclose(timestampFile_[fednumber]);

        if (errorFile_.find(fednumber)!=errorFile_.end() && errorFile_[fednumber]!=0)
                fclose(errorFile_[fednumber]);
    }

    //this is safe to run anytime
    closeOutputFiles();
    reportStatistics();

    if (theFEDCalibrationBase_ != 0) {
        DEBUG("PixelFEDSupervisor::Recover - There was a FEDCalibrationBase_");
        delete theFEDCalibrationBase_;
        theFEDCalibrationBase_ = 0;
        DEBUG("But not anymore!");
    }

    //HALT does not usually clean up theCalibObject_. I guess it doesn't really matter
    delete theCalibObject_;
    theCalibObject_ = 0;

    xoap::MessageReference reply = makeSOAPMessageReference("RecoverDone");

    //in case of hardware failure, deleteHardware will throw an exception
    //The exception will mean that deleteHardware is not fully executed
    //If that happends, then we fail to recover. The user can either try again or fix the hardware problem
    try {
        deleteHardware();
        pixel::utils::XDAQAppWithFSMPixelBase::halt();
    }
    catch (toolbox::fsm::exception::Exception &e) {
        string const msg_error_xsj = "PixelFEDSupervisor::Recover - Failed to transition to Halted state! message: " + string(e.what());
        ERROR(msg_error_xsj);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_xsj, e);
        this->notifyQualified("fatal", f);
        reply = makeSOAPMessageReference("RecoverFailed");
    }
    catch (std::exception &er) {
        string const msg_error_bfd = "PixelFEDSupervisor::Recover - Returning to Error state. Recovery failed with exception: " + string(er.what());
        ERROR(msg_error_bfd);
        pixel::exception::Exception *new_exception = dynamic_cast<pixel::exception::Exception *>(&er);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_bfd, *new_exception);
        this->notifyQualified("fatal", f);
        reply = makeSOAPMessageReference("RecoverFailed");
    }

    INFO("-- Exit RECOVER --");
    return reply;
}

void
pixel::PixelFEDSupervisor::reconfigureActionImpl(toolbox::Event::Reference event){
    INFO("--- RECONFIGURE ---");

    try {
        // Extract the Calib object corresponding to the Global Key
        INFO("Retrieving Calib object from DataBase...");
        PixelConfigInterface::get(theCalibObject_, "pixel/calib/", *theGlobalKey_);
        INFO("Retrieving Calib object from DataBase done.");

        jobConfigure();
        if (theCalibObject_ == 0)
          EndOfRunFEDReset(); //reset FED, clear SLink

    }
    catch (std::exception &e) {
        string const msg_error_upo = "Reconfiguration failed with exception: " + string(e.what());
        ERROR(msg_error_upo);
        pixel::exception::Exception *new_exception = dynamic_cast<pixel::exception::Exception *>(&e);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_upo, *new_exception);
        this->notifyQualified("fatal", f);
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::reconfigureActionImpl(event);
}

void
pixel::PixelFEDSupervisor::transitionConfiguredToHaltingActionImpl(toolbox::Event::Reference event) {
    if (theCalibObject_ == 0) {
        workloop_->remove(physicsRunning_);
        INFO("PixelFEDSupervisor::transitionConfiguredToHaltingActionImpl - Removed Physics data taking job from workloop.");
    }
}

void
pixel::PixelFEDSupervisor::transitionRunningOrPausedToHaltingActionImpl(toolbox::Event::Reference event) {
    if (theCalibObject_ == 0) {
        remove_workloops();

        for (map<pair<ulong, uint>, set<uint> >::iterator vmeBaseAddressAndFEDNumberAndChannel = vmeBaseAddressAndFEDNumberAndChannels_.begin(); vmeBaseAddressAndFEDNumberAndChannel != vmeBaseAddressAndFEDNumberAndChannels_.end(); ++vmeBaseAddressAndFEDNumberAndChannel) {
            const uint fednumber = vmeBaseAddressAndFEDNumberAndChannel->first.second;
            DEBUG("About to close output file for FED#" << fednumber);
            if(readSpyFifo3) {fclose(dataFile_[fednumber]); dataFile_[fednumber] = 0;}
            //fclose(dataFileT_[fednumber]);
            //dataFileT_[fednumber] = 0;
            if(writeStatus) {fclose(dataFileS_[fednumber]); dataFileS_[fednumber] = 0;}
            //fclose(dataFileS_[fednumber]);
            //dataFileS_[fednumber] = 0;
            //fclose(timestampFile_[fednumber]);
            //timestampFile_[fednumber] = 0;
            fclose(errorFile_[fednumber]);
            errorFile_[fednumber] = 0;
        }

        if (getCurrentStateName()=="Running")
            INFO("PixelFEDSupervisor::transitionRunningOrPausedToHaltingActionImpl [Halt from Running] - Cancelled Physics data taking workloop, removed job from it and closed files.");
        else
            INFO("PixelFEDSupervisor::transitionRunningOrPausedToHaltingActionImpl [Halt from Paused] - Removed Physics data taking job from workloop and closed files.");
    }
}


void
pixel::PixelFEDSupervisor::haltActionImpl(toolbox::Event::Reference event) {
    INFO("--- HALT ---");

    if (theCalibObject_ == 0)
        EndOfRunFEDReset(); //reset FED, clear SLink

    // Clear the masks for all the feds
    // Loop through FEDs
    blacklistFedChannelMap_.clear();

    if (!FEDInterface_.empty()){
        INFO("PixelFEDSupervisor::haltActionImpl - Updating the mask of the to-be-masked FED channels.");
        for (map<pair<ulong, uint>, set<uint> >::iterator vmeBaseAddressAndFEDNumberAndChannel = vmeBaseAddressAndFEDNumberAndChannels_.begin(); vmeBaseAddressAndFEDNumberAndChannel != vmeBaseAddressAndFEDNumberAndChannels_.end(); ++vmeBaseAddressAndFEDNumberAndChannel) {
            // vmeBaseAddress = vmeBaseAddressAndFEDNumberAndChannel->first.first;
            PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddressAndFEDNumberAndChannel->first.first];
            bitset<48> bitsetAllzero;
            blacklistFedChannelMap_[iFED->getPixelFEDCard().fedNumber] = bitsetAllzero;
            iFED->updateMask(blacklistFedChannelMap_[iFED->getPixelFEDCard().fedNumber]);
            //iFED->updateMask(bitsetAllzero);
            iFED->resetEnbableBits();
            //iFED->resetXYCount();
        }
    }

    closeOutputFiles();
    reportStatistics();

    if (theFEDCalibrationBase_) {
        LOG4CPLUS_TRACE(sv_logger_, "PixelFEDSupervisor::haltActionImpl - There was a FEDCalibrationBase_");
        delete theFEDCalibrationBase_;
        theFEDCalibrationBase_ = 0;
        LOG4CPLUS_TRACE(sv_logger_, "But not anymore!");
    }

    // There is hardware access here
    try {
        deleteHardware();
    }
    catch (HAL::BusAdapterException &hwe) {
        string const msg_error_zka = "PixelFEDSupervisor::haltActionImpl - Hardware error while Halting the FEDs. Exception: " + string(hwe.what());
        ERROR(msg_error_zka);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_zka, hwe);
        this->notifyQualified("fatal", f);
    }
    catch (xcept::Exception &xdaqe) {
        string const msg_error_fvc = "PixelFEDSupervisor::haltActionImpl - XDAQ error while Halting the FEDs. Exception: " + string(xdaqe.what());
        ERROR(msg_error_fvc);
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_fvc, xdaqe);
        this->notifyQualified("fatal", f);
    }
    catch (...) {
        string const msg_error_jxx = "PixelFEDSupervisor::haltActionImpl - Unknown error while Halting the FEDs";
        ERROR(msg_error_jxx);
        pixel::exception::Exception trivial_exception("PixelFEDSupervisorException", "module", msg_error_jxx, 1707, "PixelFEDSupervisor::Halt(xoap::MessageReference)");
        XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_jxx, trivial_exception);
        this->notifyQualified("fatal", f);
    }

    // To do the usual stuff
    pixel::utils::XDAQAppWithFSMPixelBase::haltActionImpl(event);
}

xoap::MessageReference
pixel::PixelFEDSupervisor::changeStateImpl(xoap::MessageReference msg) {
    // Extract the command name from the SOAP message
    string commandName = "undefined";
    try {
        commandName = extractSOAPCommandName(msg);
    }
    catch (xcept::Exception &err) {
        string const msgBase = "Failed to extract command name from SOAP message";
        ERROR(msgBase + ": '" + xcept::stdformat_exception_history(err) + "'.");
        string errMsg = msgBase + ": '" + err.message() + "'.";

        // Somehow we don't understand the SOAP message. Log an error
        // and flag that we are having trouble.
        string histMsg = "Ignoring ununderstood FSM SOAP command '" + commandName + "'";
        appStateInfoSpace_.addHistoryItem(histMsg);

        ERROR(histMsg + " " + errMsg);
        xoap::MessageReference reply =
            makeSOAPFaultReply(msg,
                               pixel::utils::SOAPFaultCodeSender, // faultCode,
                               histMsg.c_str(), //faultString,
                               errMsg.c_str()); //faultDetail);
        return reply;
    }

    if(commandName=="FixSoftError"){
        // Extract the names and values of the variables
        Variables elements = getSOAPCommandVariables(msg);

        // FEDs and associated channels
        blacklistFedChannelMap_.clear();
        try {
            for (size_t i=0; i<elements["FED"].size(); ++i) {
                blacklistFedChannelMap_[atoi(elements["FED"].at(i).c_str())] = bitset<48>(elements["Channel"].at(i));
                INFO("Received the following FED with FixSoftError message: " << elements["FED"].at(i));
                INFO("Received the following channel to be blacklisted with FixSoftError message: " << elements["Channel"].at(i));
            }
        }
            catch(out_of_range &oor) {
                LOGERROR(sv_logger_, "Failed to extract variables from SOAP message.");
                XCEPT_RAISE(xoap::exception::Exception, "Failed to extract variables from SOAP message.");
        }
        ////// Unknown elements
        ////for(Variables::iterator element = elements.begin(); element != elements.end(); element++){
            ////if(element->first != "FED" && element->first != "Channel"){
                ////for(vector<string>::iterator val = element->second.begin(); val != element->second.end(); val++){
                    ////WARN("Received an unknown element with FixSoftError message: " << element->first << ": " << *val);
                ////}
            ////}
        ////}

        physicsRunningSentSoftErrorDetected = false;
    }

    return pixel::utils::XDAQAppWithFSMPixelBase::changeStateImpl(msg);
}

void
pixel::PixelFEDSupervisor::fixSoftErrorActionImpl(toolbox::Event::Reference event) {
    INFO("--- FixSoftError ---");

    physicsRunningSentSoftErrorDetected = false;

    //Code from Pause/Resume with space for any needed function.  For now just a state transition.....
    if (theCalibObject_ == 0)
        stop_workloops();

    // Loop through FEDs
    for (map<pair<ulong, uint>, set<uint> >::iterator vmeBaseAddressAndFEDNumberAndChannel = vmeBaseAddressAndFEDNumberAndChannels_.begin();
         vmeBaseAddressAndFEDNumberAndChannel != vmeBaseAddressAndFEDNumberAndChannels_.end();
         ++vmeBaseAddressAndFEDNumberAndChannel) {
        // vmeBaseAddress = vmeBaseAddressAndFEDNumberAndChannel->first.first;
        INFO("Updating the mask of the to-be-masked FED channels.");
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddressAndFEDNumberAndChannel->first.first];
        iFED->updateMask(blacklistFedChannelMap_[iFED->getPixelFEDCard().fedNumber]);
        //iFED->checkSEUCounters(2);
        iFED->resetEnbableBits();
    }

    INFO("--- FixSoftError DONE ---");
}

void
pixel::PixelFEDSupervisor::resumeFromSoftErrorActionImpl(toolbox::Event::Reference event){
    if (theCalibObject_ == 0)
        start_workloops();
}

void
pixel::PixelFEDSupervisor::jobConfigure() {
    pixel::utils::Timer getFEDCardTimer, configBoardTimer;
    
    //stop the monitor
    for(auto&& fedmonHw : fedHwDeviceMap){
        fedmonHw.second->hwRelease();
    }

    LOG4CPLUS_TRACE(sv_logger_, "Will now get Name translation");
    PixelConfigInterface::get(theNameTranslation_, "pixel/nametranslation/", *theGlobalKey_);
    if (theNameTranslation_ == 0)
        XCEPT_RAISE(xdaq::exception::Exception, "Failed to load name translation!");

    LOG4CPLUS_TRACE(sv_logger_, "Will now get detector configuration");
    PixelConfigInterface::get(theDetectorConfiguration_, "pixel/detconfig/", *theGlobalKey_);
    if (theDetectorConfiguration_ == 0)
        XCEPT_RAISE(xdaq::exception::Exception, "Failed to load detconfig!");

    LOG4CPLUS_TRACE(sv_logger_, "Will now get fed configuration");
    PixelConfigInterface::get(theFEDConfiguration_, "pixel/fedconfig/", *theGlobalKey_);
    if (theFEDConfiguration_ == 0)
        XCEPT_RAISE(xdaq::exception::Exception, "Failed to load the FED Configuration!");

    LOG4CPLUS_TRACE(sv_logger_,"Will now get global delay25 configuration");
    PixelConfigInterface::get(theGlobalDelay25_, "pixel/globaldelay25/", *theGlobalKey_);

    const map<uint, set<uint> > fedsAndChannels = theDetectorConfiguration_->getFEDsAndChannels(theNameTranslation_);
    boost::thread_group allTreads;
    configBoardTimer.start();
    //we need to reserve the number of feds at instantiating, so we have to use a generic number (now it is 10)
    ulong iFED_connected=0;
    for (map<uint, set<uint> >::const_iterator fedAndChannel=fedsAndChannels.begin(); fedAndChannel!=fedsAndChannels.end(); ++fedAndChannel) {
        const ulong fednumber = fedAndChannel->first;
        if (theFEDConfiguration_->crateFromFEDNumber(fednumber) == crate_) {
            ulong vmeBaseAddress = theFEDConfiguration_->VMEBaseAddressFromFEDNumber(fednumber);
            set<uint> channels = fedAndChannel->second;
            vmeBaseAddressAndFEDNumberAndChannels_.insert(make_pair(make_pair(vmeBaseAddress, fednumber), channels));

            const string fedtype = theFEDConfiguration_->typeFromFEDNumber(fednumber);
            if (fedtype == "CTA") {
                char boardid[32];
                snprintf(boardid, 32, "FED%02lu", fednumber);
                RegMgr_[vmeBaseAddress] = new RegManager(boardid,
                                                         theFEDConfiguration_->URIFromFEDNumber(fednumber),
                                                         "file://" + datbase_ + "address_table.xml");

                FEDInterface_[vmeBaseAddress] = new PixelPh1FEDInterface(RegMgr_[vmeBaseAddress], datbase_);
                //we just use the FEDHwDeviceTCA as a wrapper for the Reg manager, also it is nice if the HW knows the fedid it is on.
                if(fedHwDeviceMap.find(iFED_connected) != fedHwDeviceMap.end()){
                    fedHwDeviceMap[iFED_connected]->setHw( RegMgr_[vmeBaseAddress], fednumber);
                }
                iFED_connected++;
            }
            else
                XCEPT_RAISE(xcept::Exception, "PixelFEDSupervisor::jobConfigure - Incorrect FED type");

            FEDInterfaceFromFEDnumber_[fednumber] = FEDInterface_[vmeBaseAddress];

            dataFIFO1_[vmeBaseAddress] = new stringstream();
            dataFIFO2_[vmeBaseAddress] = new stringstream();
            dataFIFO3_[vmeBaseAddress] = new stringstream();
            errorFIFO_[vmeBaseAddress] = new stringstream();
            tempFIFO_[vmeBaseAddress] = new stringstream();

            getFEDCardTimer.start();
            pos::PixelFEDCard *theFEDCard;

            //PixelConfigInterface::setMode(true);
            PixelConfigInterface::get(theFEDCard, "pixel/fedcard/" + pixel::utils::to_string(fednumber), *theGlobalKey_);
            getFEDCardTimer.stop();
            if (theFEDCard == 0)
                XCEPT_RAISE(xdaq::exception::Exception, "Failed to load FED card for FED=" + pixel::utils::to_string(fednumber));

            //Check consistency of the configuration
            pos::PixelConfigurationVerifier theVerifier;
            theVerifier.checkChannelEnable(theFEDCard,
                                           theNameTranslation_,
                                           theDetectorConfiguration_);

            //this actually does the configuration of the feds
            boost::thread *this_thread = new boost::thread(&pixel::PixelFEDSupervisor::setupFEDInterface, this, FEDInterface_[vmeBaseAddress], theFEDCard);
            allTreads.add_thread(this_thread);
        }
    }
    activeFEDChannelMap = theDetectorConfiguration_->getFEDsAndChannels(theNameTranslation_);
    allTreads.join_all();
    
    //start the monitor
    for(auto&& fedmonHw : fedHwDeviceMap){
        fedmonHw.second->hwConnect();
    }
    
    configBoardTimer.stop();

    INFO("FED getFEDBoards total calls:" + pixel::utils::to_string(getFEDCardTimer.ntimes()) + " total time:" + pixel::utils::to_string(getFEDCardTimer.tottime()) + " avg time:" + pixel::utils::to_string(getFEDCardTimer.avgtime()));

    INFO("FED card configure :" + pixel::utils::to_string(configBoardTimer.ntimes()) + " total time:" + pixel::utils::to_string(configBoardTimer.tottime()) + " avg time:" + pixel::utils::to_string(configBoardTimer.avgtime()));
}

void
pixel::PixelFEDSupervisor::setupFEDInterface(PixelPh1FEDInterface *interf, pos::PixelFEDCard *theFEDCard) {
    interf->setup(*theFEDCard, &lock_FEDinterfaceSetup);
    interf->resetSlink();
    ////interf->testTTSbits(0, 0);
    interf->storeEnbableBits();

    interf->getBoardInfo();
}

void
pixel::PixelFEDSupervisor::SEUCountReset() {
    INFO("SEU Counter reset FED ");

    for (map<pair<ulong, uint>, set<uint> >::iterator vmeBaseAddressAndFEDNumberAndChannel = vmeBaseAddressAndFEDNumberAndChannels_.begin(); vmeBaseAddressAndFEDNumberAndChannel != vmeBaseAddressAndFEDNumberAndChannels_.end(); ++vmeBaseAddressAndFEDNumberAndChannel) {
        // vmeBaseAddress = vmeBaseAddressAndFEDNumberAndChannel->first.first;
        PixelPh1FEDInterface *iFED = FEDInterface_[vmeBaseAddressAndFEDNumberAndChannel->first.first];

        // clear the SEU related stuff
        bitset<48> emptymask;
        iFED->updateMask(emptymask);
        iFED->resetEnbableBits();
    }
}

void
pixel::PixelFEDSupervisor::EndOfRunFEDReset() {
    if (!FEDInterface_.empty()) {
        for (map<pair<ulong, uint>, set<uint> >::iterator vmeBaseAddressAndFEDNumberAndChannel = vmeBaseAddressAndFEDNumberAndChannels_.begin(); vmeBaseAddressAndFEDNumberAndChannel != vmeBaseAddressAndFEDNumberAndChannels_.end(); ++vmeBaseAddressAndFEDNumberAndChannel) {
            ulong vmeBaseAddress = vmeBaseAddressAndFEDNumberAndChannel->first.first;
            uint fedNumber = vmeBaseAddressAndFEDNumberAndChannel->first.second;
            try {
                FEDInterface_[vmeBaseAddress]->resetSlink();

                INFO("Sending resets to FED " << fedNumber << " done");
            }
            catch (HAL::BusAdapterException &hwe) {
                string const msg_error_cix = "Caught HAL exception while resetting FED " + pixel::utils::to_string(fedNumber) + " :" + string(hwe.what());
                ERROR(msg_error_cix);
                XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_cix, hwe);
                this->notifyQualified("fatal", f);
            }
            catch (xcept::Exception &err) { //i've got no idea what kind of exception might be thrown
                string const msg_error_zih = "Caught XDAQ exception while resetting FED " + pixel::utils::to_string(fedNumber) + " :" + string(err.what());
                ERROR(msg_error_zih);
                XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_zih, err);
                this->notifyQualified("fatal", f);
            }
            catch (...) {
                string const msg_error_dmb = "Caught unknown exception while resetting FED " + pixel::utils::to_string(fedNumber);
                ERROR(msg_error_dmb);
                pixel::exception::Exception trivial_exception("PixelFEDSupervisorException", "module", msg_error_dmb, 4180, "PixelFEDSupervisor::EndOfRunFEDReset()");
                XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_dmb, trivial_exception);
                this->notifyQualified("fatal", f);
            }
        }
    }
}

void
pixel::PixelFEDSupervisor::closeOutputFiles() {
    DEBUG("-- Closing FED output files --");

    for (FEDInterfaceMap::iterator iFED = FEDInterface_.begin(); iFED != FEDInterface_.end(); ++iFED) {
        ulong vmeBaseAddress = iFED->first;
        if (fileopen_[vmeBaseAddress]) { //close output file
            INFO("Output file is open for vme address " << vmeBaseAddress);
            fclose(fout_[vmeBaseAddress]); // C I/o
            fileopen_[vmeBaseAddress] = false;
            INFO("Output file is closed");
        }
        if (errBufferOpen_[vmeBaseAddress]) { //close error file
            INFO("Error file is open for vme address " << vmeBaseAddress);
            fclose(errorFile2_[vmeBaseAddress]);
            errBufferOpen_[vmeBaseAddress] = false;
            INFO("Error file is closed");
        }
    }

    fout_.clear();
    fileopen_.clear();
    errorFile2_.clear();
    errBufferOpen_.clear();

    DEBUG("-- Closing FED output files done--");
}

void
pixel::PixelFEDSupervisor::reportStatistics() {
    // prints end of run summary for monitoring

    if (theNameTranslation_ != 0) {
        map<uint, set<uint> > fedsAndChannels = theDetectorConfiguration_->getFEDsAndChannels(theNameTranslation_);
        for (map<uint, set<uint> >::iterator fedAndChannel = fedsAndChannels.begin(); fedAndChannel != fedsAndChannels.end(); ++fedAndChannel) {
            ulong fednumber = fedAndChannel->first;
            if ( theFEDConfiguration_->crateFromFEDNumber(fednumber) == crate_) {
                stringstream infofed;
                if (!lffMap.empty()) {
                    infofed << "FED=" << fednumber << " LFF status=" << 100 * lffMap[fednumber].mean() << "%" << endl;
                    infofed << "FIFO I Almost Full: N(1-9)=" << 100 * fifoStatusMap[fednumber][0].mean() << "% NC(10-18)=" << 100 * fifoStatusMap[fednumber][2].mean()
                              << "% SC(19-27)=" << 100 * fifoStatusMap[fednumber][4].mean() << "% S(27-36)=" << 100 * fifoStatusMap[fednumber][6].mean() << "%" << endl;
                    infofed << "FIFO II Nearly Full: N(1-9)=" << 100 * fifoStatusMap[fednumber][1].mean() << "% NC(10-18)=" << 100 * fifoStatusMap[fednumber][3].mean()
                              << "% SC(19-27)=" << 100 * fifoStatusMap[fednumber][5].mean() << "% S(27-36)=" << 100 * fifoStatusMap[fednumber][7].mean() << "%" << endl;
                    infofed << "FIFO III Almost Full: UP=" << 100 * fifoStatusMap[fednumber][8].mean() << "% DOWN=" << 100 * fifoStatusMap[fednumber][9].mean() << "%" << endl;
                }
                if (!errorCountMap.empty()) {
                    infofed << "Number of errors read=" << errorCountMap[fednumber][0] << endl;
                }
                if(!infofed.str().empty()) INFO(infofed.str());
            }
        } //fedAndChannels
    }
}


void
pixel::PixelFEDSupervisor::deleteHardware() {
    FEDInterfaceMap::iterator iFED;
    for (iFED = FEDInterface_.begin(); iFED != FEDInterface_.end(); ++iFED) {
        if (iFED->second != 0) { //in case the hardware access throws an exception and then this is run again
            iFED->second->setControlRegister(0x8);
            iFED->second->setModeRegister(0x1);
            delete iFED->second;
            iFED->second = 0;
        }
    }
    FEDInterface_.clear();

    for (VMEPtrMap::iterator iVME=VMEPtr_.begin(); iVME!=VMEPtr_.end(); ++iVME) {
        delete iVME->second;
        iVME->second = 0;
    }
    VMEPtr_.clear();

    for (RegMgrMap::iterator iRegMgr=RegMgr_.begin(); iRegMgr!=RegMgr_.end(); ++iRegMgr) {
        delete iRegMgr->second;
        iRegMgr->second = 0;
    }
    RegMgr_.clear();
}

void
pixel::PixelFEDSupervisor::b2inEvent(toolbox::mem::Reference *msg, xdata::Properties &plist) {
    string action = plist.getProperty("action"),
                    receiveMsg;

    plist.setProperty("urn:b2in-protocol:lid", pixel::utils::to_string(PixelSupervisor_->getLocalId()));

    const xoap::MessageReference soapMsg = ((action != "BeginCalibration" && action != "EndCalibration") ? makeSOAPMessageReference(action, plist) :
                                                                                                           makeSOAPMessageReference(action));

    if (action == "ReadDataAndErrorFIFO")
        receiveMsg = extractSOAPCommandName(ReadDataAndErrorFIFO(soapMsg));
    else if (action == "FEDCalibrations")
        receiveMsg = extractSOAPCommandName(FEDCalibrations(soapMsg));
    else if (action == "SetAutoPhases")
        receiveMsg = extractSOAPCommandName(SetAutoPhases(soapMsg));
    else if (action == "FindPhasesNow")
        receiveMsg = extractSOAPCommandName(FindPhasesNow(soapMsg));
    else if (action == "ArmOSDFifo")
        receiveMsg = extractSOAPCommandName(ArmOSDFifo(soapMsg));
    else if (action == "SetCalibNEvents")
        receiveMsg = extractSOAPCommandName(SetCalibNEvents(soapMsg));
    else if (theFEDCalibrationBase_ != 0) {
        if(action == "BeginCalibration")
            theFEDCalibrationBase_->beginCalibration(soapMsg);
        else if (action == "EndCalibration")
            theFEDCalibrationBase_->endCalibration(soapMsg);
    }

    plist.setProperty("returnValue", receiveMsg);

    sendReply(plist);
}

xoap::MessageReference
pixel::PixelFEDSupervisor::ReadDataAndErrorFIFO(xoap::MessageReference msg) {
    string reply_string = "ReadDataAndErrorFIFODone";

    static uint counter = 0;
    //const int prescale=17;
    const int prescale = 999999; // disable error&baseline readout to check the time savings

    if (extractSOAPCommandName(ReadFIFO(msg)) != "ReadFIFODone") {
        reply_string = "ReadDataAndErrorFIFOFailed";
        ERROR("PixelFEDSupervisor::ReadDataAndErrorFIFO - Reading data FIFO failed!");
    }

    if (counter % prescale == 0) {
        if (extractSOAPCommandName(ReadErrorFIFO(msg)) != "ReadErrorFIFODone") {
            reply_string = "ReadDataAndErrorFIFOFailed";
            ERROR("PixelFEDSupervisor::ReadDataAndErrorFIFO - Reading error FIFO failed!");
        }
    }

    ++counter;

    return makeSOAPMessageReference(reply_string);
}


xoap::MessageReference
pixel::PixelFEDSupervisor::ReadFIFO(xoap::MessageReference msg) {
    vector<string> names(4);
    names.push_back("VMEBaseAddress");
    names.push_back("Channel");
    names.push_back("ShipTo");
    names.push_back("FIFO");
    ////names.push_back("Mode");
    ////names.push_back("Filename");
    ////names.push_back("Additional");
    ////names.push_back("Time");
    Attributes parameters = getAllSOAPCommandAttributes(msg, names);

    FEDInterfaceMap::const_iterator beginFED, endFED;

    if (parameters["VMEBaseAddress"] == "*") {
        beginFED = FEDInterface_.begin();
        endFED = FEDInterface_.end();
    } else {
        beginFED = FEDInterface_.find(atoi(parameters["VMEBaseAddress"].c_str()));

        if (beginFED == FEDInterface_.end())
            XCEPT_RAISE(pixel::exception::Exception, "PixelFEDSupervisor::ReadFIFO - FED with VMEBaseAddress (Key) " + parameters["VMEBaseAddress"] + " not found.");

        endFED = boost::next(beginFED,1);
    }


    for (FEDInterfaceMap::const_iterator iterFED = beginFED; iterFED != endFED; ++iterFED) {
        try {
            PixelPh1FEDInterface *iFED = iterFED->second;
            ulong vmeBaseAddress = iterFED->first;

            switch(atoi(parameters["FIFO"].c_str())) {
                case 1:
                    if (parameters["Mode"] == "Transparent") {
                        if (parameters["Channel"] == "*")
                            ERROR("Reading FIFO 1 of all channels in transparent mode is not implemented yet!");
                        else {
                            uint32_t buffer[pos::fifo1TranspDepth];
                            uint channel = atoi(parameters["Channel"].c_str());

                            // PixelFEDInterface *iFEDold = dynamic_cast<PixelFEDInterface *>(iFED);
                            // if (iFEDold)
                            //    iFEDold->drain_transBuffer(channel, buffer);


                            if (pos::fifo1TranspDepth < 0)
                                ERROR("The contents of SpyFIFO 1, channel " << channel << " transparent mode could not be drained into the buffer.");


                            if (parameters["ShipTo"] == "Screen") {
                                // HACK -- Take out later! FIXME
                                ofstream fout;
                                fout.open("/tmp/DebugFIFO1Data.txt", ios::app);
                                for (uint i=0; i<pos::fifo1TranspDepth; ++i) {
                                    fout << "FED channel=" << channel << "buffer[" << i << "]=0x" << hex << buffer[i] << dec << " , ADC[i" << i << "]=" << ((buffer[i] & 0xffc00000) >> 22) << endl;
                                }
                                fout << "----------------------------------------------------------------------------" << endl;
                                fout.close();
                                ///////////////////

                                dataFIFO1_[vmeBaseAddress] = new stringstream();
                                *(dataFIFO1_[vmeBaseAddress]) << "Contents of Spy FIFO 1 in Transparent Mode" << endl;
                                *(dataFIFO1_[vmeBaseAddress]) << "----------------------" << endl;
                                for (uint i=0; i<pos::fifo1TranspDepth; ++i)
                                    *(dataFIFO1_[vmeBaseAddress]) << "Word at clock #" << i << " = 0x" << hex << buffer[i] << dec << ", ADC= " << ((buffer[i] & 0xffc00000) >> 22) << endl;
                                *(dataFIFO1_[vmeBaseAddress]) << "----------------------" << endl;

                            } else if (parameters["ShipTo"] == "Console") {
                                INFO("Contents of Spy FIFO 1 in Transparent Mode");
                                cout << "----------------------" << endl;

                                for (uint i = 0; i<pos::fifo1TranspDepth; ++i) {
                                    // diagService_->reportError("Word at clock #"+stringF(i)+" = 0x"+htoa(buffer[i])+", ADC= "+itoa((buffer[i] & 0xffc00000)+22)<, DIAGINFO);
                                    cout << "Word at clock #" << i << " = 0x" << hex << buffer[i] << dec << ", ADC= " << ((buffer[i] & 0xffc00000) >> 22) << endl;
                                }

                                cout << "----------------------" << endl;
                            } else if (parameters["ShipTo"] == "File") {
                                ofstream fout;
                                if (parameters["Time"] == "True")
                                    fout.open(("/tmp/" + parameters["Filename"]).c_str(), ios::trunc);
                                else if (parameters["Time"] == "False")
                                    fout.open(("/tmp/" + parameters["Filename"]).c_str(), ios::app);
                                for (uint i = 0; i < pos::fifo1TranspDepth; ++i) {
                                    fout << parameters["Additional"] << " " << parameters["Channel"] << " " << i << " " << dec << ((buffer[i] & 0xffc00000) >> 22) << endl;
                                }
                                fout.close();
                            } else if (parameters["ShipTo"] == "GIF") {
                                PixelDecodedFEDRawData decodedRawData(buffer, 100., 100., 150., 0., 100., 0., 150.);
                                // tbmSignalFilename = parameters["Filename"];
                                decodedRawData.drawToFile("/tmp/" + parameters["Filename"]);
                            } else
                                ERROR("Sorry, shipping FIFO 1 Transparent mode to " << parameters["ShipTo"] << " has not been implemented");
                        }
                    } else if (parameters["Mode"] == "Normal") {
                        if (parameters["Channel"] == "*")
                            ERROR("Reading FIFO 1 of all channels in normal mode is not implemented yet!");
                        else {
                            uint32_t buffer[pos::fifo1NormalDepth];
                            uint channel = atoi(parameters["Channel"].c_str());

                            // PixelFEDInterface *iFEDold = dynamic_cast<PixelFEDInterface *>(iFED);
                            // if (iFEDold)
                            //    iFEDold->drainFifo1(channel, buffer);

                            if (pos::fifo1NormalDepth < 0)
                                ERROR("The contents of SpyFIFO 1, channel " << channel << "could not be drained into the buffer.");


                            if (parameters["ShipTo"] == "Screen") {
                                *(dataFIFO1_[vmeBaseAddress]) << "Contents of Spy FIFO 1 in Normal Mode" << endl;
                                *(dataFIFO1_[vmeBaseAddress]) << "----------------------" << endl;

                                for (uint i=0; i<pos::fifo1NormalDepth; ++i)
                                    *(dataFIFO1_[vmeBaseAddress]) << "FIFO 1 word at position " << i << " = 0x" << hex << buffer[i] << dec << endl;

                                *(dataFIFO1_[vmeBaseAddress]) << "----------------------" << endl;
                            } else if (parameters["ShipTo"] == "Console") {
                                INFO("This is FED VME=0x" << vmeBaseAddress << ", channel " << channel << " on Spy FIFO 1 in Normal Mode.");
                                PixelFEDFifoData::decodeNormalData(buffer, 255);
                            } else if (parameters["ShipTo"] == "File") {
                                ofstream fout(("/tmp/" + parameters["Filename"]).c_str(), ios::app);
                                for (uint i=0; i<pos::fifo1NormalDepth; ++i) {
                                    fout << "FED VME=0x" << hex << vmeBaseAddress << dec << ", Channel " << channel << ", word " << i << " is 0x" << hex << buffer[i] << endl;
                                }
                                fout.close();
                            } else
                                ERROR("Sorry, shipping FIFO 1 Normal Mode to " << parameters["ShipTo"] << " has not been implemented");
                        }
                    }
                break;
                case 2:
                {
                    uint32_t buffer[pos::fifo2Depth];

                    // PixelFEDInterface *iFEDold = dynamic_cast<PixelFEDInterface *>(iFED);
                    // if (iFEDold)
                    //    iFEDold->drainDataFifo2(buffer);

                    if (pos::fifo2Depth > 0) {
                        FIFO2Decoder decodedFIFO2data(buffer, pos::fifo2Depth);

                        if (parameters["ShipTo"] == "Screen") {
                            dataFIFO2_[vmeBaseAddress]->str() = "";
                            decodedFIFO2data.printToStream(*(dataFIFO2_[vmeBaseAddress]));
                        } else if (parameters["ShipTo"] == "Console")
                            decodedFIFO2data.printToStream(cout);
                    } else
                        ERROR("PixelFEDSupervisor::ReadFIFO -- Cannot read FIFO2!");
                }
                break;
                case 3:
                {
                    if (parameters["ShipTo"] == "Screen") {
                        int status = iFED->spySlink64(buffer64);

                        if (status > 0) {
                            dataFIFO3_[vmeBaseAddress]->str() = "";
                            *(dataFIFO3_[vmeBaseAddress]) << "Contents of Spy FIFO 3" << endl;
                            *(dataFIFO3_[vmeBaseAddress]) << "----------------------" << endl;
                            for (int i=0; i<status; ++i) {
                                *(dataFIFO3_[vmeBaseAddress]) << "Clock " << i << " = 0x" << hex << buffer64[i] << dec << endl;
                            }
                        }
                    } else if (parameters["ShipTo"] == "File") {
                        if (fileopen_.find(vmeBaseAddress) == fileopen_.end())
                            fileopen_[vmeBaseAddress] = false;

                        if (!fileopen_[vmeBaseAddress]) {
                            fout_[vmeBaseAddress] = fopen((outputDir() + "/" + parameters["Filename"] + "_" + pixel::utils::to_string(iFED->getPixelFEDCard().fedNumber) + "_" + runNumber_ + ".dmp").c_str(), "wb"); // C I/O

                            INFO("outputDir: " << outputDir());

                            (dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_))->writeASCII(outputDir());
                            theNameTranslation_->writeASCII(outputDir());
                            theDetectorConfiguration_->writeASCII(outputDir());
                            uint64_t runNumber = atol(runNumber_.c_str());
                            fwrite(&runNumber, sizeof(uint64_t), 1, fout_[vmeBaseAddress]);
                            fileopen_[vmeBaseAddress] = true;
                        }

                        int status = iFED->spySlink64(buffer64);

                        if (status >= 0 && fileopen_[vmeBaseAddress])
                            fwrite(buffer64, sizeof(uint64_t), status, fout_[vmeBaseAddress]);

                        if (parameters["Time"] == "Last" && fileopen_[vmeBaseAddress]) {
                            fclose(fout_[vmeBaseAddress]); // C I/o
                            fileopen_[vmeBaseAddress] = false;
                        }
                    }
                }
                break;
            }
        }
        catch (HAL::HardwareAccessException &e) {
            string const msg_error_vsk = "Caught HAL::HardwareAccessException : " + string(e.what());
            ERROR(msg_error_vsk);
            XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_vsk, e);
            this->notifyQualified("fatal", f);
        }
        catch (std::exception& e) {
            string const msg_warn_yzn = "*** Unknown exception occurred";
            WARN(msg_warn_yzn);
            std::exception *error_ptr = &e;
            pixel::exception::Exception *new_exception = dynamic_cast<pixel::exception::Exception *>(error_ptr);
            XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_warn_yzn, *new_exception);
            this->notifyQualified("fatal", f);
        }
    }

    return makeSOAPMessageReference("ReadFIFODone");
}

xoap::MessageReference
pixel::PixelFEDSupervisor::ReadErrorFIFO(xoap::MessageReference msg) {
    string reply = "ReadErrorFIFODone";

    vector<string> names(4);
    names.push_back("ShipTo");
    ////names.push_back("Filename");
    names.push_back("VMEBaseAddress");
    ////names.push_back("Time");
    Attributes parameters = getAllSOAPCommandAttributes(msg, names);

    FEDInterfaceMap::const_iterator beginFED, endFED;

    if (parameters["VMEBaseAddress"] == "*") {
        beginFED = FEDInterface_.begin();
        endFED = FEDInterface_.end();
    } else {
        beginFED = FEDInterface_.find(atoi(parameters["VMEBaseAddress"].c_str()));

        if (beginFED == FEDInterface_.end())
            XCEPT_RAISE(pixel::exception::Exception, "PixelFEDSupervisor::ReadErrorFIFO - FED with VMEBaseAddress (Key) " + parameters["VMEBaseAddress"] + " not found.");

        endFED = boost::next(beginFED,1);
    }

    for (FEDInterfaceMap::const_iterator iterFED=beginFED; iterFED!=endFED; ++iterFED) {
        PixelPh1FEDInterface *iFED = iterFED->second;

        ulong vmeBaseAddress = iterFED->first;
        uint32_t errBuffer[(36 * 1024)]; //max size?

        if (errBufferOpen_.find(vmeBaseAddress) == errBufferOpen_.end())
            errBufferOpen_[vmeBaseAddress] = false;

        int errCount = iFED->drainErrorFifo(errBuffer);

        if (parameters["ShipTo"] == "File") {
            if (!errBufferOpen_[vmeBaseAddress]) {
                // fedid = iFED->getPixelFEDCard().fedNumber
                errorFile2_[vmeBaseAddress] = fopen((outputDir_ + "/" + parameters["Filename"] + "_" + pixel::utils::to_string(iFED->getPixelFEDCard().fedNumber) + "_" + runNumber_ + ".err").c_str(), "wb");
                ulong runNumber = atol(runNumber_.c_str());
                fwrite(&runNumber, sizeof(unsigned long), 1, errorFile2_[vmeBaseAddress]);
                errBufferOpen_[vmeBaseAddress] = true;
            }

            if (errCount >= 0 && errBufferOpen_[vmeBaseAddress]) {
                fwrite(errBuffer, sizeof(ulong), errCount, errorFile2_[vmeBaseAddress]);

                for (int i_err=0; i_err<errCount; ++i_err) {
                    PixelFEDFifoData decoder;
                    decoder.decodeErrorFifo(errBuffer[i_err]);

                    switch ((errBuffer[i_err] & 0x03e00000) >> 21) {
                        case 0x1f || 0x1d || 0x1c || 0x1b: //CHECK
                            decoder.decodeErrorFifo(errBuffer[i_err]);
                            break;
                        case 0x1a:
                            INFO("Gap.");
                            break;
                    }
                }
            }

            if (parameters["Time"] == "Last" && errBufferOpen_[vmeBaseAddress]) {
                fclose(errorFile2_[vmeBaseAddress]);
                errBufferOpen_[vmeBaseAddress] = false;
            }
        } else if (parameters["ShipTo"] == "Screen") {
            ErrorFIFODecoder decodedErrorFIFO(errBuffer, errCount); // Suppress disabled channels later
            decodedErrorFIFO.printToStream(*(errorFIFO_[vmeBaseAddress]));
        } else if (parameters["ShipTo"] == "Console") {
            ErrorFIFODecoder decodedErrorFIFO(errBuffer, errCount); // Suppress disabled channels later
            decodedErrorFIFO.printToStream(cout);
        } else {
            ERROR("PixelFEDSupervisor::ReadErrorFIFO: Ship To = " + parameters["ShipTo"] + " not recognised.");

            string reply = "ReadErrorFIFOFailed";
        }
    }

    return makeSOAPMessageReference(reply);
}

xoap::MessageReference
pixel::PixelFEDSupervisor::SetAutoPhases(xoap::MessageReference msg) {
    vector<string> name(1, "Enable");
    const bool enable = getSomeSOAPCommandAttributes(msg, name)["Enable"]=="enable";

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    if (tempCalibObject == 0) {
        string const msg_err_calib = "PixelFEDSupervisor::SetAutoPhases - Calib object is not specified!";
        ERROR(msg_err_calib);
        XCEPT_RAISE(pixel::exception::Exception, msg_err_calib);
    }

    const vector<pair<uint, vector<uint> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_); // FIXME - use a map
    for (vector<pair<uint, vector<uint> > >::const_iterator ifedAndChannels = fedsAndChannels.begin(); ifedAndChannels < fedsAndChannels.end(); ++ifedAndChannels)
        FEDInterface_[theFEDConfiguration_->VMEBaseAddressFromFEDNumber(ifedAndChannels->first)]->setAutoPhases(enable);

    return makeSOAPMessageReference("SetAutoPhasesDone");
}

xoap::MessageReference
pixel::PixelFEDSupervisor::FindPhasesNow(xoap::MessageReference msg) {
    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    if (tempCalibObject == 0) {
        string const msg_err_calib = "PixelFEDSupervisor::FindPhasesNow - Calib object is not specified!";
        ERROR(msg_err_calib);
        XCEPT_RAISE(pixel::exception::Exception, msg_err_calib);
    }

    const vector<pair<uint, vector<uint> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);
    for (vector<pair<uint, vector<uint> > >::const_iterator ifedAndChannels = fedsAndChannels.begin(); ifedAndChannels < fedsAndChannels.end(); ++ifedAndChannels)
        FEDInterface_[theFEDConfiguration_->VMEBaseAddressFromFEDNumber(ifedAndChannels->first)]->findPhasesNow();

    return makeSOAPMessageReference("FindPhasesNowDone");
}

xoap::MessageReference
pixel::PixelFEDSupervisor::SetCalibNEvents(xoap::MessageReference msg) {
    vector<string> name(1, "NEvents");
    int NEvents = atoi(getSomeSOAPCommandAttributes(msg, name)["NEvents"].c_str());

    if (NEvents <= 0) {
        string const msg_err_calib = "PixelFEDSupervisor::SetCalibNEvents - NEvents is less than or equal to zero";
        ERROR(msg_err_calib);
        XCEPT_RAISE(pixel::exception::Exception, msg_err_calib);
    }

    pos::PixelCalibConfiguration *tempCalibObject = dynamic_cast<pos::PixelCalibConfiguration *>(theCalibObject_);
    if (tempCalibObject == 0) {
        string const msg_err_calib = "PixelFEDSupervisor::SetCalibNEvents - Calib object is not specified!";
        ERROR(msg_err_calib);
        XCEPT_RAISE(pixel::exception::Exception, msg_err_calib);
    }

    const vector<pair<uint, vector<uint> > > &fedsAndChannels = tempCalibObject->fedCardsAndChannels(crate_, theNameTranslation_, theFEDConfiguration_, theDetectorConfiguration_);
    for (vector<pair<uint, vector<uint> > >::const_iterator ifedAndChannels = fedsAndChannels.begin(); ifedAndChannels < fedsAndChannels.end(); ++ifedAndChannels)
        FEDInterface_[theFEDConfiguration_->VMEBaseAddressFromFEDNumber(ifedAndChannels->first)]->setCalibNEvents(NEvents);

    return makeSOAPMessageReference("SetCalibNEventsDone");
}

xoap::MessageReference
pixel::PixelFEDSupervisor::ReadRSSI(xoap::MessageReference msg) {
    vector<string> parametersReceived_names(2);
    parametersReceived_names.push_back("VMEBaseAddress");
    parametersReceived_names.push_back("Fiber");
    Attributes parametersReceived = getSomeSOAPCommandAttributes(msg, parametersReceived_names);

    Attributes returnValues;
    returnValues["Value"] = pixel::utils::to_string(FEDInterface_[atoi(parametersReceived["VMEBaseAddress"].c_str())]->ReadRSSI(atoi(parametersReceived["Fiber"].c_str())));

    return makeSOAPMessageReference("ReadRSSIDone", returnValues);
}

xoap::MessageReference
pixel::PixelFEDSupervisor::ReadOSDFifo(xoap::MessageReference msg) {
    vector<string> parametersReceived_names(2);
    parametersReceived_names.push_back("VMEBaseAddress");
    parametersReceived_names.push_back("Channel");
    Attributes parametersReceived = getSomeSOAPCommandAttributes(msg, parametersReceived_names),
               returnValues;
    // data = FEDInterface_[VMEBaseAddress]->readOSDFifo(atoi(parametersReceived["Channel"].c_str()))
    returnValues["Value"] = pixel::utils::to_string(FEDInterface_[atoi(parametersReceived["VMEBaseAddress"].c_str())]->readOSDFifo(atoi(parametersReceived["Channel"].c_str())));
    return makeSOAPMessageReference("ReadOSDFifoDone", returnValues);
}

xoap::MessageReference
pixel::PixelFEDSupervisor::SendBackProblemChannels(xoap::MessageReference msg) {
    INFO("Start sending back the information about masked channels.");
    VariablesWithType variables;

    //be sure we don't interfere with the workloop
    phlock_->take();
    for (map<pair<ulong, uint>, set<uint> >::iterator i_vmeBaseAddressAndFEDNumberAndChannels = vmeBaseAddressAndFEDNumberAndChannels_.begin();
         i_vmeBaseAddressAndFEDNumberAndChannels != vmeBaseAddressAndFEDNumberAndChannels_.end();
         ++i_vmeBaseAddressAndFEDNumberAndChannels) {
        //  vmeBaseAddress = i_vmeBaseAddressAndFEDNumberAndChannels->first.first;
        PixelPh1FEDInterface *iFED = FEDInterface_[i_vmeBaseAddressAndFEDNumberAndChannels->first.first];
#ifdef DO_TEST
	cout<<"Print Masking registers from PixelFEDSupervisor::SendBackProblemChannels"<<endl;
	iFED->checkAutoMaskRegisters(); // print raw SEU registers 
#endif // DO_TEST
        if (iFED->checkFEDChannelSEU()) {
            bitset<48> tmpset(iFED->getFEDChannelSEU());
            if (tmpset.count()) {
                string fednumber = pixel::utils::to_string(i_vmeBaseAddressAndFEDNumberAndChannels->first.second);
                INFO("FED: " << fednumber << "  ChannelInfo: " << tmpset.to_string());
                variables["String"]["FED"].push_back(fednumber);
                variables["String"]["ChannelInfo"].push_back(tmpset.to_string());
            }
        }
    }
    phlock_->give();

    Attributes parameters;
    parameters["Supervisor"] = "PixelFEDSupervisor";

    vector<string>  alterVars;
    alterVars.push_back("FED");
    alterVars.push_back("ChannelInfo");
    return makeSOAPMessageReference("SendChannels", parameters, variables, alterVars);
}


xoap::MessageReference
pixel::PixelFEDSupervisor::FEDCalibrations(xoap::MessageReference msg) {
    if (theFEDCalibrationBase_ == 0) {
        string const msg_err_calib = "PixelFEDSupervisor::FEDCalibrations - theFEDCalibrationBase_ object is not specified!";
        ERROR(msg_err_calib);
        XCEPT_RAISE(pixel::exception::Exception, msg_err_calib);
    }

    return theFEDCalibrationBase_->execute(msg);
}

xoap::MessageReference
pixel::PixelFEDSupervisor::beginCalibration(xoap::MessageReference msg) {
    if (theFEDCalibrationBase_ != 0)
        return theFEDCalibrationBase_->beginCalibration(msg);

    return makeSOAPMessageReference("PixelFEDSupervisor::beginCalibration Default");
}

xoap::MessageReference
pixel::PixelFEDSupervisor::endCalibration(xoap::MessageReference msg) {
    if (theFEDCalibrationBase_ != 0)
        return theFEDCalibrationBase_->endCalibration(msg);

    return makeSOAPMessageReference("PixelFEDSupervisor::endCalibration Default");
}


void
pixel::PixelFEDSupervisor::submit_workloops() {
    phlock_->take();
    workloopContinue_ = true;
    phlock_->give();
    if (theCalibObject_ == 0) { // This must be a Physics Run
        workloop_->submit(physicsRunning_);
        INFO("Physics data taking workloop submitted");
        monitor_workloop_->submit(monitorUpdate_);
        INFO("Monitoring workloop submitted");
    }
}

void
pixel::PixelFEDSupervisor::start_workloops() {
    phlock_->take();
    workloopContinue_ = true;
    physicsRunningSentSoftErrorDetected = false;
    physicsRunningSentRunningDegraded = false;
    phlock_->give();
    if (!workloop_->isActive())
        workloop_->activate();
    INFO("Physics data taking workloop activated.");
    if (!monitor_workloop_->isActive())
        monitor_workloop_->activate();
    INFO("Monitoring workloop activated.");
}

void
pixel::PixelFEDSupervisor::stop_workloops() {
    phlock_->take();
    workloopContinue_ = false;
    phlock_->give();
    if (workloop_->isActive())
        workloop_->cancel();
    INFO("Physics data taking workloop cancelled");
    if (monitor_workloop_->isActive())
        monitor_workloop_->cancel();
    INFO("Monitoring workloop cancelled");
}

void
pixel::PixelFEDSupervisor::remove_workloops() {
    phlock_->take();
    workloopContinue_ = false;
    phlock_->give();
    if (workloop_->isActive()) {
        workloop_->cancel();
        if (theCalibObject_ == 0) { // a Physics Run
            try {
                workloop_->remove(physicsRunning_);
            }
            catch (xcept::Exception &e) { //will happen if the task was never submitted
                string const msg_error_ogq = "Failed to remove FED physics workloop: " + string(e.what());
                DEBUG(msg_error_ogq);
                XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_ogq, e);
                this->notifyQualified("fatal", f);
            }
            INFO("Physics data taking workloop removed");
        }
    }
    if (monitor_workloop_->isActive()) {
        monitor_workloop_->cancel();
        if (theCalibObject_ == 0) { // a Physics Run
            try {
                monitor_workloop_->remove(monitorUpdate_);
            }
            catch (xcept::Exception &e) { //will happen if the task was never submitted
                string const msg_error_ogq = "Failed to remove FED monitor workloop: " + string(e.what());
                DEBUG(msg_error_ogq);
                XCEPT_DECLARE_NESTED(pixel::exception::Exception, f, msg_error_ogq, e);
                this->notifyQualified("fatal", f);
            }
            INFO("Monitoring workloop removed");
        }
    }
}

/**
 * Define what we expect in terms of parameters for each SOAP FSM command.
 */
vector<pixel::utils::FSMSOAPParHelper>
pixel::PixelFEDSupervisor::expectedFSMSoapParsImpl(string const &commandName) const {
    vector<pixel::utils::FSMSOAPParHelper> params;

    if (commandName == "FixSoftError") {
        params.push_back(pixel::utils::FSMSOAPParHelper(commandName, "FED", false));
        params.push_back(pixel::utils::FSMSOAPParHelper(commandName, "Channel", false));
    }

    return params;
}
