#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "pixel/PixelFEDSupervisor/version.h"

GETPACKAGEINFO(PixelFEDSupervisor)

void PixelFEDSupervisor::checkPackageDependencies()
    {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(xcept);
    CHECKDEPENDENCY(xdaq);
}

std::set<std::string, std::less<std::string> >
PixelFEDSupervisor::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, xcept);
    ADDDEPENDENCY(dependencies, xdaq);

    return dependencies;
}
