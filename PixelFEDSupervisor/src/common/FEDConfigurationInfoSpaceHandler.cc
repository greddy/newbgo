#include "PixelFEDSupervisor/include/monitor/FEDConfigurationInfoSpaceHandler.h"

#include <cstdlib>

#include "toolbox/string.h"

#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"

pixel::FEDConfigurationInfoSpaceHandler::FEDConfigurationInfoSpaceHandler(xdaq::Application *xdaqApp)
    : pixel::utils::ConfigurationInfoSpaceHandler(*xdaqApp) {
    createString("connectionName",
                 "FED41",
                 "",
                 pixel::utils::InfoSpaceItem::NOUPDATE,
                 true);
    createString("connectionsFileName",
                 "file://" + std::string(std::getenv("BUILD_HOME")) + "/pixel/PixelMonitor/xml/pixfed_connections.xml",
                 "",
                 pixel::utils::InfoSpaceItem::NOUPDATE,
                 true);
    //createString("connectionURI",
    //             "chtcp-2.0://cmsuppixch:10203?target=devfed:50001",
    //             "",
    //             pixel::utils::InfoSpaceItem::NOUPDATE,
    //             true);
    //createString("addressTable",
    //             "file://"+std::string(std::getenv("BUILD_HOME"))+"/pixel/PixelMonitor/dat/address_table.xml",
    //             "",
    //             pixel::utils::InfoSpaceItem::NOUPDATE,
    //             true);
}

pixel::FEDConfigurationInfoSpaceHandler::~FEDConfigurationInfoSpaceHandler() {
}

void
pixel::FEDConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(pixel::utils::Monitor &monitor) {
    pixel::utils::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(monitor);
    monitor.addItem("Application configuration",
                    "connectionName",
                    "uTCA HW connected",
                    this);
    monitor.addItem("Application configuration",
                    "connectionsFileName",
                    "FEDs connection file",
                    this);
    //monitor.addItem("Application configuration",
    //                "connectionURI",
    //                "HW URI",
    //                this);
    //monitor.addItem("Application configuration",
    //                "addressTable",
    //                "Full path of address table of HW",
    //                this);
}

std::string
pixel::FEDConfigurationInfoSpaceHandler::formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const &item) const {
    std::string res = pixel::utils::escapeAsJSONString(pixel::utils::InfoSpaceHandler::kInvalidItemString);
    //std::string name = item->name();
    //std::string const val = getString(name);
    //res = pixel::utils::escapeAsJSONString(val);
    res = InfoSpaceHandler::formatItem(item);
    return res;
}
