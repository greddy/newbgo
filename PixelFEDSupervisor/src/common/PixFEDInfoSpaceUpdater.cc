#include "PixelFEDSupervisor/include/monitor/PixFEDInfoSpaceUpdater.h"

#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <iomanip>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "PixelFEDSupervisor/include/monitor/FEDReader.h"
#include "pixel/exception/Exception.h"
#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/Utils.h"

pixel::PixFEDInfoSpaceUpdater::PixFEDInfoSpaceUpdater(pixel::hwlayertca::FEDHwDeviceTCA const &hw,
                                                                     pixel::PixelFEDSupervisor &xdaqApp)
    : pixel::utils::InfoSpaceUpdater(xdaqApp),
      hw_(hw),
      xdaqApp_(xdaqApp),
      reader(pixel::FEDReader(hw_)){
}

pixel::PixFEDInfoSpaceUpdater::~PixFEDInfoSpaceUpdater() {
}

void
pixel::PixFEDInfoSpaceUpdater::updateInfoSpaceImpl(pixel::utils::InfoSpaceHandler *const infoSpaceHandler) {
    // do nothing here since we want to update from the Supervisor
}

void
pixel::PixFEDInfoSpaceUpdater::updateInfoSpaceTrigger(pixel::utils::InfoSpaceHandler *const infoSpaceHandler) {
    
    if (getHw().isHwConnected()) {
        pixel::utils::InfoSpaceHandler::ItemVec &items = infoSpaceHandler->getItems();
        pixel::utils::InfoSpaceHandler::ItemVec::iterator iter;

        for (iter = items.begin(); iter != items.end(); ++iter) {
            try {
                updateInfoSpaceItem(*iter, infoSpaceHandler);
            }
            catch (...) {
                iter->setInvalid();
                throw;
            }
        }

        // Now sync everything from the cache to the InfoSpace itself.
        infoSpaceHandler->writeInfoSpace();
    } else {
        infoSpaceHandler->setInvalid();
    }
}


bool
pixel::PixFEDInfoSpaceUpdater::updateInfoSpaceItem(pixel::utils::InfoSpaceItem &item,
                                                                  pixel::utils::InfoSpaceHandler *const infoSpaceHandler) {
    bool updated = false;
    if (hw_.isHwConnected()) {
        std::string name = item.name();
        pixel::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
        pixel::utils::InfoSpaceItem::ItemType itemType = item.type();
        if (updateType == pixel::utils::InfoSpaceItem::HW32) {
            // TODO need to be changed later!
            if (itemType == pixel::utils::InfoSpaceItem::UINT32) {
                // ------------- UPDATE ERROR FOR CHANNELS -------------
                if (name == "TransitionsToTTSReady") {
                    infoSpaceHandler->setUInt32(name, reader.getTransitionsToTTSReady());
                } else if (name == "TransitionsToTTSBusy") {
                    infoSpaceHandler->setUInt32(name, reader.getTransitionsToTTSBusy());
                } else if (name == "TransitionsToTTSOOS") {
                    infoSpaceHandler->setUInt32(name, reader.getTransitionsToTTSOOS());
                } else if (name == "TransitionsToTTSWarn") {
                    infoSpaceHandler->setUInt32(name, reader.getTransitionsToTTSWarn());
                } else if (name == "L1ACount") {
                    infoSpaceHandler->setUInt32(name, reader.getL1ACount());
                }

                // ------------- UPDATE PLL STATUS -------------
                else if (name == "PLL_400MHz") {
                    infoSpaceHandler->setUInt32(name, reader.getPLLlockedInfo()[0]);
                } else if (name == "PLL_200MHz") {
                    infoSpaceHandler->setUInt32(name, reader.getPLLlockedInfo()[1]);
                } else if (name == "PLL_200MHz_idelay") {
                    infoSpaceHandler->setUInt32(name, reader.getPLLlockedInfo()[2]);
                }

                // ------------- UPDATE TOTAL ERROR STATUS -------------
                else if (name == "DisabledChTot") {
                    unsigned long channelEnableStatusUl = strtoul(infoSpaceHandler->getString("channelEnableStatus").c_str(), NULL, 2);
                    std::bitset<48> tmp(channelEnableStatusUl);
                    infoSpaceHandler->setUInt32(name,
                                                tmp.count());
                } else if (name == "TbmMaskChTot") {
                    infoSpaceHandler->setUInt32(name,
                                                reader.getTBMMask().count());
                } else if (name == "ChMaskedTot") {
                    infoSpaceHandler->setUInt32(name,
                                                reader.getChannelMask().count());
                } else if (name == "EvtErrNumTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getEvtErrNumVec()));
                } else if (name == "EvtTmoNumTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getEvtTmoNumVec()));
                } else if (name == "CntEvtRsyTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getCntEvtRsyVec()));
                } else if (name == "CntTbmHidTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getCntTbmHidVec()));
                } else if (name == "CntTrlErrTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getCntTrlErrVec()));
                } else if (name == "PkamRstTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getPkamRstVec()));
                } else if (name == "NoTknPssTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getNoTknPssVec()));
                } else if (name == "RocErrNumTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getRocErrNumVec()));
                } else if (name == "OvfNumTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getOvfNumVec()));
                } else if (name == "TbmAtoRstTot") {
                    infoSpaceHandler->setUInt32(name,
                                                pixel::sumVecOfUint32(reader.getTbmAtoRstVec()));
                }

                updated = true;
            }
            // else if (itemType == pixel::utils::InfoSpaceItem::BOOL)
            // {
            //     InfoSpaceHandler->setBool(name, (tmp != 0));
            //     updated = true;
            // }
        } else if (updateType == pixel::utils::InfoSpaceItem::HW64) {
            if (itemType == pixel::utils::InfoSpaceItem::UINT64) {
                // ------------- UPDATE TTS TIMES -------------
                if (name == "TimeInTTSReady") {
                    infoSpaceHandler->setUInt64(name, reader.getTimeInTTSReady());
                } else if (name == "TimeInTTSBusy") {
                    infoSpaceHandler->setUInt64(name, reader.getTimeInTTSBusy());
                } else if (name == "TimeInTTSOOS") {
                    infoSpaceHandler->setUInt64(name, reader.getTimeInTTSOOS());
                } else if (name == "TimeInTTSWarn") {
                    infoSpaceHandler->setUInt64(name, reader.getTimeInTTSWarn());
                }
                updated = true;
            }
        } else if (updateType == pixel::utils::InfoSpaceItem::PROCESS) {
            if (itemType == pixel::utils::InfoSpaceItem::STRING) {
                // ------------- UPDATE TTS STATE -------------
                if (name == "BoardId") {
                    infoSpaceHandler->setString(name, reader.getBoardId());
                } else if (name == "RevId") {
                    infoSpaceHandler->setString(name, reader.getRevId());
                } else if (name == "BoardCode") {
                    infoSpaceHandler->setString(name, reader.getBoardCode());
                } else if (name == "MACAddress") {
                    infoSpaceHandler->setString(name, reader.getMACAddress());
                } else if (name == "FWIPHCVersion") {
                    infoSpaceHandler->setString(name, reader.getFwIPHCVersion());
                } else if (name == "FWIPHCDate") {
                    infoSpaceHandler->setString(name, reader.getFwIPHCDate());
                } else if (name == "FWHEPHYVersion") {
                    infoSpaceHandler->setString(name, reader.getFwHEPHYVersion());
                } else if (name == "FWHEPHYDate") {
                    infoSpaceHandler->setString(name, reader.getFwHEPHYDate());
                } else if (name == "TTSState") {
                    infoSpaceHandler->setString(name, reader.getTTSState());
                } else if (name == "connectionName") {
                    std::string res = getHw().getFEDid();
                    infoSpaceHandler->setString(name, res);
                } else if (name == "stateName") {
                    std::string res = xdaqApp_.getApplicationStateInfoSpaceHandler().getString("stateName");
                    infoSpaceHandler->setString(name, res);
                } else if (name == "lid") {
                    std::string res = toolbox::toString("%d", xdaqApp_.getApplicationDescriptor()->getLocalId());
                    infoSpaceHandler->setString(name, res);
                } else if (name == "tbmMask") {
                    std::bitset<48> tbmmask_ = reader.getTBMMask();

                    // if TBM_MASK is inconsistent with previous..
                    if (tbmmask_.to_string() != infoSpaceHandler->getString(name)) {
                        unsigned long prevTbmMaskUl = strtoul(infoSpaceHandler->getString(name).c_str(), NULL, 2);
                        std::bitset<48> prevTbmmask_(prevTbmMaskUl);
                        std::bitset<48> altered_ = tbmmask_ ^ prevTbmmask_;
                        // logging..
                        std::string const fedid = getHw().getFEDid();
                        std::string const prevMsg = fedid + " TBM Mask was: " + prevTbmmask_.to_string();
                        LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), prevMsg);
                        xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(prevMsg);
                        for (std::size_t i = 0; i != altered_.size(); ++i) {
                            // mask bit changed AND channged to MASK
                            if (altered_[i] && tbmmask_[i]) {
                                std::stringstream tmp;
                                tmp << "Channel " << std::setfill('0') << std::setw(2) << (i + 1) << " is masked in TBM_MASK";
                                LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), tmp.str());
                                xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(tmp.str());
                            }
                        }
                        std::string const postMsg = fedid + " TBM Mask now: " + tbmmask_.to_string();
                        LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), postMsg);
                        xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(postMsg);

                        infoSpaceHandler->setString(name, tbmmask_.to_string());
                    }
                } else if (name == "channelMask") {
                    std::bitset<48> channelmask_ = reader.getChannelMask();

                    // if MASK_CH is inconsistent with previous..
                    if (channelmask_.to_string() != infoSpaceHandler->getString(name)) {
                        // finding the differences
                        unsigned long prevChannelmaskUl = strtoul(infoSpaceHandler->getString(name).c_str(), NULL, 2);
                        std::bitset<48> prevChannelmask_(prevChannelmaskUl);
                        unsigned long tbmmaskUl = strtoul(infoSpaceHandler->getString("tbmMask").c_str(), NULL, 2);
                        std::bitset<48> tbmmask_(tbmmaskUl);
                        std::bitset<48> altered_ = channelmask_ ^ prevChannelmask_;
                        // logging..
                        std::string const fedid = getHw().getFEDid();
                        std::string const prevMsg = fedid + " CHs Mask was: " + prevChannelmask_.to_string();
                        LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), prevMsg);
                        xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(prevMsg);
                        for (std::size_t i = 0; i != altered_.size(); ++i) {
                            // mask bit changed and changed to MASK
                            if (altered_[i] && channelmask_[i]) {
                                std::stringstream tmp;
                                tmp << "Channel " << std::setfill('0') << std::setw(2) << (i + 1) << " is masked MASK_CH";
                                LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), tmp.str());
                                xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(tmp.str());
                            }
                            // mask bit changed AND changed to UNMASK AND tbmbit was UNMASK
                            if (altered_[i] && prevChannelmask_[i] && !tbmmask_[i]) {
                                std::stringstream tmp;
                                tmp << "Channel " << std::setfill('0') << std::setw(2) << (i + 1) << " is recovered";
                                LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), tmp.str());
                                xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(tmp.str());
                            }
                        }
                        std::string const postMsg = fedid + " CHs Mask now: " + channelmask_.to_string();
                        LOG4CPLUS_INFO(xdaqApp_.getApplicationLogger(), postMsg);
                        xdaqApp_.getApplicationStateInfoSpaceHandler().addHistoryItem(postMsg);

                        // write into infospace
                        infoSpaceHandler->setString(name, channelmask_.to_string());
                    }
                }else if (name == "fedErr_info") {
                    std::string res;
                    for (uint32_t i = 1; i <= 48; ++i) {
                        if (!res.empty()) {
                            res += ", ";
                        }
                        res += "{";

                        std::stringstream ss;
                        ss<< getHw().getFEDid();
                        res +=
                            pixel::utils::escapeAsJSONString("fedid") + ": " +
                            pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");
                        ss << "CH" << std::setfill('0') << std::setw(2) << i;
                        res += ", " +
                            pixel::utils::escapeAsJSONString("chName") + ": " +
                            pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");
                        ss << reader.getNGoodPhaseOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("ngp") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");
                        ss << ! (xdaqApp_.isFEDchannelActive(std::stoul(getHw().getFEDid()),i));
                        res += ", " +
                               pixel::utils::escapeAsJSONString("me") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getTBMMask()[i - 1];
                        res += ", " +
                               pixel::utils::escapeAsJSONString("mt") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getChannelMask()[i - 1];
                        res += ", " +
                               pixel::utils::escapeAsJSONString("mc") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getEvtErrNumOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("ene") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getEvtTmoNumOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("etoe") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getCntTrlErrOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("mte") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getOvfNumOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("ofn") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getNoTknPssOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("ntp") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getCntEvtRsyOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("lb4t") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getRocErrNumOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("re") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getTbmAtoRstOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("tar") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getCntTbmHidOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("the") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        ss << reader.getPkamRstOfChannel(i);
                        res += ", " +
                               pixel::utils::escapeAsJSONString("tpr") + ": " +
                               pixel::utils::escapeAsJSONString(ss.str());
                        ss.str("");

                        // End of JSON
                        res += "}";
                    }

                    res = "[" + res + "]";

                    infoSpaceHandler->setString(name, res);
                }
                updated = true;
            }
        }
        if (!updated && updateType != pixel::utils::InfoSpaceItem::NOUPDATE) {
            std::string msg = toolbox::toString("Updating not implemented for item with name '%s'.",
                                                item.name().c_str());
            XCEPT_RAISE(pixel::exception::SoftwareProblem, msg);
        }
    }

    if (updated) {
        item.setValid();
    } else {
        item.setInvalid();
    }

    return updated;
}

pixel::hwlayertca::FEDHwDeviceTCA const &
pixel::PixFEDInfoSpaceUpdater::getHw() const {
    return hw_;
}

uint32_t
pixel::sumVecOfUint32(std::vector<uint32_t> vec) {
    uint32_t res(0);
    for (std::vector<uint32_t>::iterator iter = vec.begin();
         iter != vec.end();
         ++iter) {
        res += *iter;
    }
    return res;
}
