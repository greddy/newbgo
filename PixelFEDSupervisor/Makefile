include $(XDAQ_ROOT)/build/mfAutoconf.rules
include $(XDAQ_ROOT)/build/mfDefs.$(XDAQ_OS)

include $(ENV_CMS_TK_ONLINE_ROOT)/FecHeader.linux
include $(ENV_CMS_TK_ONLINE_ROOT)/config/FecRules.linux

# As a transition measure between XDAQ14 (gcc 4.8, C++98) and XDAQ15
# (gcc 7.2, C++14), let's build in some wiggle room.
# Useful choices should be one of:
# - XDAQ_TARGET_XDAQ14
# - XDAQ_TARGET_XDAQ15
ifndef XDAQ_TARGET
#  export XDAQ_TARGET=XDAQ_TARGET_XDAQ14
  export XDAQ_TARGET=XDAQ_TARGET_XDAQ15
endif

Project = pixel
Package = $(Project)/PixelFEDSupervisor
Library = PixelFEDSupervisor

PIXEL_EXCEPTION_PREFIX = $(BUILD_HOME)/$(Package)/../exception
PIXEL_EXCEPTION_INCLUDE_PREFIX = $(PIXEL_EXCEPTION_PREFIX)/include
PIXEL_EXCEPTION_LIB_PREFIX = $(PIXEL_EXCEPTION_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

PIXEL_HWLAYER_PREFIX = $(BUILD_HOME)/$(Package)/../hwlayer
PIXEL_HWLAYER_INCLUDE_PREFIX = $(PIXEL_HWLAYER_PREFIX)/include
PIXEL_HWLAYER_LIB_PREFIX = $(PIXEL_HWLAYER_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

PIXEL_HWTCALAYER_PREFIX = $(BUILD_HOME)/$(Package)/../hwlayertca
PIXEL_HWTCALAYER_INCLUDE_PREFIX = $(PIXEL_HWTCALAYER_PREFIX)/include
PIXEL_HWTCALAYER_LIB_PREFIX = $(PIXEL_HWTCALAYER_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

PIXEL_UTILS_PREFIX = $(BUILD_HOME)/$(Package)/../utils
PIXEL_UTILS_INCLUDE_PREFIX = $(PIXEL_UTILS_PREFIX)/include
PIXEL_UTILS_LIB_PREFIX = $(PIXEL_UTILS_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

Sources = \
	DiagWrapper.cc \
	PixelFEDSupervisor.cc \
	version.cc \
	PixFEDInfoSpaceHandler.cc \
	PixFEDInfoSpaceUpdater.cc \
	FEDReader.cc \
	WebTableFedErrInfo.cc \
	FEDConfigurationInfoSpaceHandler.cc \
	TCADeviceFED.cc \
	FEDHwDeviceTCA.cc

#Include directories
IncludeDirs = \
	$(BUILD_HOME)/$(Project) \
	$(XDAQ_ROOT)/include \
	$(XDAQ_ROOT)/include/hal \
	$(PIXEL_UTILS_INCLUDE_PREFIX) \
	$(PIXEL_EXCEPTION_INCLUDE_PREFIX) \
	$(PIXEL_HWLAYER_INCLUDE_PREFIX) \
	$(PIXEL_HWTCALAYER_INCLUDE_PREFIX) \
	$(BUILD_HOME)/pixel/PixelFEDSpySupervisor/include \
	$(BUILD_HOME)/pixel/PixelUtilities/PixelFEDDataTools/include \
	$(CACTUS_ROOT)/include \
	$(XDAQ_ROOT)/include/i2o \
	$(XDAQ_ROOT)/include/i2o/shared \
	$(XDAQ_ROOT)/include/interface/evb \
	$(ENV_CMS_TK_ONLINE_ROOT)/generic/include \
	$(ENV_CMS_TK_ONLINE_ROOT)/FecPciDeviceDriver/include \
	$(ENV_CMS_TK_ONLINE_ROOT)/ThirdParty/APIConsoleDebugger/include \
	$(shell root-config --incdir) 


LibraryDirs = \
	$(ENV_CMS_TK_ONLINE_ROOT)/lib

UserSourcePath = \
	$(XDAQ_ROOT)/daq/xdaq/src/linux/common/

UserCFlags = -O
#UserCCFlags = -D_FILE_OFFSET_BITS=64 $(BUSADAPTERC++FLAG) -DLINUX
UserCCFlags = -g -O -Wno-long-long -DBT1003 ${BUSADAPTERC++FLAG} -DLINUX -Wno-error=parentheses -Werror
UserCCFlags += -D$(XDAQ_TARGET)
UserDynamicLinkFlags =

#ifeq ($(XDAQ_TARGET), XDAQ13)
#    UserCCFlags += -DXDAQ13
#endif

UserStaticLinkFlags =
UserExecutableLinkFlags =

# These libraries can be platform specific and
# potentially need conditional processing
DependentLibraryDirs = $(ENV_CMS_TK_ONLINE_ROOT)/lib
#DependentLibraries = APIFecVme
DependentLibraries = 

#ifeq ($(VMEDUMMY), yes)
#UserCCFlags += -DVMEDUMMY
#endif

Libraries =

DynamicLibrary = $(Library)
StaticLibrary =

include $(XDAQ_ROOT)/build/Makefile.rules
include $(BUILD_HOME)/$(Project)/RPM.version
include $(XDAQ_ROOT)/build/mfRPM.rules
