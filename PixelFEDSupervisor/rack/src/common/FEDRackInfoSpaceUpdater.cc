#include "PixelFEDSupervisor/include/monitor/rack/FEDRackInfoSpaceUpdater.h"

#include <set>
#include <vector>
#include <tr1/unordered_map>
#include <bitset>
#include <cstdlib>
#include <iomanip>
#include <utility>
#include <algorithm>

#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"
#include "xdata/Table.h"
#include "xdata/Serializable.h"
#include "xdata/TimeVal.h"
#include "xdaq/ContextDescriptor.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationContext.h"

#include "PixelFEDSupervisor/include/monitor/rack/FEDRack.h"
#include "PixelFEDSupervisor/include/monitor/rack/Utils.h"
#include "pixel/exception/Exception.h"
#include "pixel/utils/ConfigurationInfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/LogMacros.h"
#include "pixel/utils/Utils.h"

pixel::rack::FEDRackInfoSpaceUpdater::FEDRackInfoSpaceUpdater(pixel::rack::FEDRack &xdaqApp)
    : pixel::utils::InfoSpaceUpdater(),
      xdaqApp_(xdaqApp) {
}

pixel::rack::FEDRackInfoSpaceUpdater::~FEDRackInfoSpaceUpdater() {
}

bool
pixel::rack::FEDRackInfoSpaceUpdater::updateInfoSpaceItem(pixel::utils::InfoSpaceItem &item,
                                                                 pixel::utils::InfoSpaceHandler *const infoSpaceHandler) {
    bool updated = false;
    std::string const name = item.name();
    pixel::utils::InfoSpaceItem::UpdateType const updateType = item.updateType();
    if (updateType == pixel::utils::InfoSpaceItem::PROCESS) {
        if (name == "fl_info") {
            std::string newVal = getFlInfo();
            infoSpaceHandler->setString(name, newVal);
        }
        if (name == "mask_info") {
            std::string newVal = getMaskInfo();
            infoSpaceHandler->setString(name, newVal);
        }
        updated = true;
    }
    if (!updated) {
        updated = pixel::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
    }
    if (updated) {
        item.setValid();
    }
    return updated;
}

std::string
pixel::rack::FEDRackInfoSpaceUpdater::getFlInfo() const {
    std::string const lasURL =
        xdaqApp_.getConfigurationInfoSpaceHandler().getString("lasURL");
    std::string const lasServiceName =
        xdaqApp_.getConfigurationInfoSpaceHandler().getString("lasServiceName");
    std::string const flashlistName =
        xdaqApp_.getConfigurationInfoSpaceHandler().getString("flashlistName");
    xdata::Table flashlist =
        pixel::rack::getFlashList(lasURL, lasServiceName, flashlistName);

/* get a map of context-Application map, not sure if it's useful or not  */
/** NOT USED
    std::vector<xdaq::ContextDescriptor*> contextVec_ =
        xdaqApp_.getApplicationContext()->getContextTable()->getContextDescriptors();
    std::tr1::unordered_map<xdaq::ContextDescriptor*, std::vector<xdaq::ApplicationDescriptor*> > contextApplicationMap_;
    std::vector<xdaq::ContextDescriptor*>::iterator contextIter;
    for (contextIter = contextVec_.begin();
         contextIter!= contextVec_.end();
         ++contextIter)
    {
        std::set<xdaq::ApplicationDescriptor*> applicatonVec_ =
            xdaqApp_.getApplicationContext()->getDefaultZone()->getApplicationDescriptors(*contextIter);
        std::set<xdaq::ApplicationDescriptor*>::iterator applicationIter;
        for (applicationIter = applicatonVec_.begin();
             applicationIter!= applicatonVec_.end();
             ++applicationIter)
        {
            if ((*applicationIter)->getClassName() == "pixel::pixfed::FEDMonitor")
            {
                contextApplicationMap_[*contextIter].push_back(*applicationIter);
            }
        }
    }
    **/
/*---------------------------------------------------------------------*/

#ifdef XDAQ13
    std::set<xdaq::ApplicationDescriptor *> const applicatonSet_ =
        xdaqApp_.getApplicationContext()->getDefaultZone()->getApplicationDescriptors("pixel::pixfed::FEDMonitor");
#else
    std::set<xdaq::ApplicationDescriptor const *> const applicatonSet_ =
        xdaqApp_.getApplicationContext()->getDefaultZone()->getApplicationDescriptors("pixel::pixfed::FEDMonitor");
#endif
    std::set<xdaq::ApplicationDescriptor const *, pixel::rack::compareApplications> apps;
    apps.insert(applicatonSet_.begin(), applicatonSet_.end());

    std::string res;
    std::set<xdaq::ApplicationDescriptor const *>::const_iterator appIter;
    for (appIter = apps.begin();
         appIter != apps.end();
         ++appIter) {
        if (!res.empty()) {
            res += ", ";
        }
        res += "{";

// Full application URL
#ifdef XDAQ13
        std::string url = const_cast<xdaq::ApplicationDescriptor *>(*appIter)->getContextDescriptor()->getURL();
        unsigned int const lid = const_cast<xdaq::ApplicationDescriptor *>(*appIter)->getLocalId();
        std::string const lidStr = toolbox::toString("%d", lid);
        std::string const urn = const_cast<xdaq::ApplicationDescriptor *>(*appIter)->getURN();
#else
        std::string url = (*appIter)->getContextDescriptor()->getURL();
        unsigned int const lid = (*appIter)->getLocalId();
        std::string const lidStr = toolbox::toString("%d", lid);
        std::string const urn = (*appIter)->getURN();
#endif

        if (url[url.size() - 1] == '/') {
            url = url.substr(0, url.size() - 1);
        }
        std::string const fullUrl = url + "/" + urn;
        //std::cout<<"\nurl: "<<url<<std::endl;
        //std::cout<<" urn: "<<urn<<std::endl;
        //std::cout<<"FullURL: "<<fullUrl<<std::endl;
        res +=
            pixel::utils::escapeAsJSONString("url") +
            ": " +
            pixel::utils::escapeAsJSONString(fullUrl);

        std::string stateName = "unknown";
        std::string timestamp = "unknown";
        std::string BoardCode = "unknown";
        std::string FwVer = "unknown";
        std::string MACAddress = "unknown";
        std::string TTSstate = "unknown";
        std::string FEDid = "-";

        std::string L1A = "-1";
        std::string pll_200MHz = "-1";
        std::string pll_200MHz_idelay = "-1";
        std::string pll_400MHz = "-1";
        std::string inBSY = "-1";
        std::string inOOS = "-1";
        std::string inRDY = "-1";
        std::string inWRN = "-1";
        std::string toBSY = "-1";
        std::string toOOS = "-1";
        std::string toRDY = "-1";
        std::string toWRN = "-1";
        //std::string maskedCh          = "-1";

        for (size_t j = 0; j != flashlist.getRowCount(); ++j) {
            xdaq::ContextDescriptor contextDesc(flashlist.getValueAt(j, "context")->toString());
            bool urlsMatch = false;
            try {
                // NOTE: matchURL() method will fail for
                // non-existent host names.
                urlsMatch = contextDesc.matchURL(url);
            }
            catch (xcept::Exception &err) {
                std::cout << "Failed to match url. Does this host exist?" << std::endl;
            }
            if (urlsMatch &&
                (flashlist.getValueAt(j, "lid")->toString() == lidStr)) {
                xdata::Serializable *timestampTmp = flashlist.getValueAt(j, "timestamp");
                xdata::TimeVal *timestampVal = dynamic_cast<xdata::TimeVal *>(timestampTmp);
                timestamp = timestampVal->value_.toString("%F %T", toolbox::TimeVal::loc); // local time zone

                stateName = flashlist.getValueAt(j, "stateName")->toString();
                BoardCode = flashlist.getValueAt(j, "BoardCode")->toString();
                FwVer = flashlist.getValueAt(j, "FWIPHCVersion")->toString();
                MACAddress = flashlist.getValueAt(j, "MACAddress")->toString();
                TTSstate = flashlist.getValueAt(j, "TTSState")->toString();
                FEDid = flashlist.getValueAt(j, "connectionName")->toString();

                L1A = flashlist.getValueAt(j, "L1ACount")->toString();
                pll_200MHz = flashlist.getValueAt(j, "PLL_200MHz")->toString();
                pll_200MHz_idelay = flashlist.getValueAt(j, "PLL_200MHz_idelay")->toString();
                pll_400MHz = flashlist.getValueAt(j, "PLL_400MHz")->toString();
                inBSY = flashlist.getValueAt(j, "TimeInTTSBusy")->toString();
                inOOS = flashlist.getValueAt(j, "TimeInTTSOOS")->toString();
                inRDY = flashlist.getValueAt(j, "TimeInTTSReady")->toString();
                inWRN = flashlist.getValueAt(j, "TimeInTTSWarn")->toString();
                toBSY = flashlist.getValueAt(j, "TransitionsToTTSBusy")->toString();
                toOOS = flashlist.getValueAt(j, "TransitionsToTTSOOS")->toString();
                toRDY = flashlist.getValueAt(j, "TransitionsToTTSReady")->toString();
                toWRN = flashlist.getValueAt(j, "TransitionsToTTSWarn")->toString();
                //maskedCh          = flashlist.getValueAt(j, "MaskChannelTot")->toString();

                break;
            }
        }

        res += ", " +
               pixel::utils::escapeAsJSONString("stateName") +
               ": " + pixel::utils::escapeAsJSONString(stateName);
        res += ", " +
               pixel::utils::escapeAsJSONString("BoardCode") +
               ": " + pixel::utils::escapeAsJSONString(BoardCode);
        res += ", " +
               pixel::utils::escapeAsJSONString("FwVer") +
               ": " + pixel::utils::escapeAsJSONString(FwVer);
        res += ", " +
               pixel::utils::escapeAsJSONString("MACAddress") +
               ": " + pixel::utils::escapeAsJSONString(MACAddress);
        res += ", " +
               pixel::utils::escapeAsJSONString("TTS State") +
               ": " + pixel::utils::escapeAsJSONString(TTSstate);
        res += ", " +
               pixel::utils::escapeAsJSONString("L1A") +
               ": " + pixel::utils::escapeAsJSONString(L1A);
        res += ", " +
               pixel::utils::escapeAsJSONString("FEDid") +
               ": " + pixel::utils::escapeAsJSONString(FEDid);

        res += ", " +
               pixel::utils::escapeAsJSONString("pll_200MHz") +
               ": " + pixel::utils::escapeAsJSONString(pll_200MHz);
        res += ", " +
               pixel::utils::escapeAsJSONString("pll_200MHz_idelay") +
               ": " + pixel::utils::escapeAsJSONString(pll_200MHz_idelay);
        res += ", " +
               pixel::utils::escapeAsJSONString("pll_400MHz") +
               ": " + pixel::utils::escapeAsJSONString(pll_400MHz);
        res += ", " +
               pixel::utils::escapeAsJSONString("inBSY") +
               ": " + pixel::utils::escapeAsJSONString(inBSY);
        res += ", " +
               pixel::utils::escapeAsJSONString("inOOS") +
               ": " + pixel::utils::escapeAsJSONString(inOOS);
        res += ", " +
               pixel::utils::escapeAsJSONString("inRDY") +
               ": " + pixel::utils::escapeAsJSONString(inRDY);
        res += ", " +
               pixel::utils::escapeAsJSONString("inWRN") +
               ": " + pixel::utils::escapeAsJSONString(inWRN);
        res += ", " +
               pixel::utils::escapeAsJSONString("toBSY") +
               ": " + pixel::utils::escapeAsJSONString(toBSY);
        res += ", " +
               pixel::utils::escapeAsJSONString("toOOS") +
               ": " + pixel::utils::escapeAsJSONString(toOOS);
        res += ", " +
               pixel::utils::escapeAsJSONString("toRDY") +
               ": " + pixel::utils::escapeAsJSONString(toRDY);
        res += ", " +
               pixel::utils::escapeAsJSONString("toWRN") +
               ": " + pixel::utils::escapeAsJSONString(toWRN);
        //res += ", " +
        //    pixel::utils::escapeAsJSONString("maskedCh") +
        //    ": " + pixel::utils::escapeAsJSONString(maskedCh);

        res += ", " +
               pixel::utils::escapeAsJSONString("timestamp") +
               ": " + pixel::utils::escapeAsJSONString(timestamp);

        // End of JSON
        res += "}";
    }

    res = "[" + res + "]"; // [{},{}, ...{}]
    return res;
}

std::string
pixel::rack::FEDRackInfoSpaceUpdater::getMaskInfo() {
    std::string const lasURL =
        xdaqApp_.getConfigurationInfoSpaceHandler().getString("lasURL");
    std::string const lasServiceName =
        xdaqApp_.getConfigurationInfoSpaceHandler().getString("lasServiceName");
    std::string const flashlistName =
        xdaqApp_.getConfigurationInfoSpaceHandler().getString("flashlistName");
    xdata::Table flashlist =
        pixel::rack::getFlashList(lasURL, lasServiceName, flashlistName);

#ifdef XDAQ13
    std::set<xdaq::ApplicationDescriptor *> const applicatonSet_ =
        xdaqApp_.getApplicationContext()->getDefaultZone()->getApplicationDescriptors("pixel::pixfed::FEDMonitor");
#else
    std::set<xdaq::ApplicationDescriptor const *> const applicatonSet_ =
        xdaqApp_.getApplicationContext()->getDefaultZone()->getApplicationDescriptors("pixel::pixfed::FEDMonitor");
#endif
    std::set<xdaq::ApplicationDescriptor const *, pixel::rack::compareApplications> apps;
    apps.insert(applicatonSet_.begin(), applicatonSet_.end());

    std::string res;
    std::set<xdaq::ApplicationDescriptor const *>::const_iterator appIter;
    for (appIter = apps.begin();
         appIter != apps.end();
         ++appIter) {
        if (!res.empty()) {
            res += ", ";
        }
        res += "{";

// Full application URL
#ifdef XDAQ13
        std::string url = const_cast<xdaq::ApplicationDescriptor *>(*appIter)->getContextDescriptor()->getURL();
        unsigned int const lid = const_cast<xdaq::ApplicationDescriptor *>(*appIter)->getLocalId();
        std::string const lidStr = toolbox::toString("%d", lid);
        std::string const urn = const_cast<xdaq::ApplicationDescriptor *>(*appIter)->getURN();
#else
        std::string url = (*appIter)->getContextDescriptor()->getURL();
        unsigned int const lid = (*appIter)->getLocalId();
        std::string const lidStr = toolbox::toString("%d", lid);
        std::string const urn = (*appIter)->getURN();
#endif

        if (url[url.size() - 1] == '/') {
            url = url.substr(0, url.size() - 1);
        }
        std::string const fullUrl = url + "/" + urn;

        res +=
            pixel::utils::escapeAsJSONString("url") +
            ": " +
            pixel::utils::escapeAsJSONString(fullUrl);

        std::string FEDid = "-";
        std::bitset<48> channelEnableStatus;
        std::bitset<48> channelMask;
        std::bitset<48> tbmMask;

        for (size_t j = 0; j != flashlist.getRowCount(); ++j) {
            xdaq::ContextDescriptor contextDesc(flashlist.getValueAt(j, "context")->toString());
            bool urlsMatch = false;
            try {
                // NOTE: matchURL() method will fail for
                // non-existent host names.
                urlsMatch = contextDesc.matchURL(url);
            }
            catch (xcept::Exception &err) {
                std::cout << "Failed to match url. Does this host exist?" << std::endl;
            }
            if (urlsMatch &&
                (flashlist.getValueAt(j, "lid")->toString() == lidStr)) {
                std::string channelEnableStatusStr = flashlist.getValueAt(j, "channelEnableStatus")->toString();
                std::string channelMaskStr = flashlist.getValueAt(j, "channelMask")->toString();
                std::string tbmMaskStr = flashlist.getValueAt(j, "tbmMask")->toString();

                unsigned long channelEnableStatusUl = strtoul(channelEnableStatusStr.c_str(), NULL, 2);
                unsigned long channelMaskUl = strtoul(channelMaskStr.c_str(), NULL, 2);
                unsigned long tbmMaskUl = strtoul(tbmMaskStr.c_str(), NULL, 2);

                std::bitset<48> channelEnableStatus_(channelEnableStatusUl);
                std::bitset<48> channelMask_(channelMaskUl);
                std::bitset<48> tbmMask_(tbmMaskUl);

                channelEnableStatus = channelEnableStatus_;
                channelMask = channelMask_;
                tbmMask = tbmMask_;
                FEDid = flashlist.getValueAt(j, "connectionName")->toString();

                break;
            }
        }

        res += ", " +
               pixel::utils::escapeAsJSONString("FEDid") +
               ": " + pixel::utils::escapeAsJSONString(FEDid);

        for (int i = 1; i <= 48; ++i) {
            std::stringstream ss;
            ss << "CH" << std::setfill('0') << std::setw(2) << i;
            std::stringstream maskbit;
            maskbit << channelEnableStatus[i - 1] << tbmMask[i - 1] << channelMask[i - 1];
            res += ", " +
                   pixel::utils::escapeAsJSONString(ss.str()) +
                   ": " + pixel::utils::escapeAsJSONString(maskbit.str());

            // inserting vals if infomation is not there
            std::pair<int, int> fedChPair = std::make_pair(atoi(FEDid.c_str()), i);
            if (fedCh2Roc_.count(fedChPair) == 0) {
                const char *env_p = std::getenv("XDAQ_ROOT");
                //const char* env_p = std::getenv("BUILD_HOME");
                //std::string const cmd = toolbox::toString("%s/pixel/PixelMonitor/utilslayer/scripts/getRocFromFedCh.py %s %d&",
                std::string const cmd = toolbox::toString("%s/bin/getRocFromFedCh.py %s %d &",
                                                          env_p, FEDid.c_str(), i);
                std::string rocInfo = pixel::utils::exec(cmd.c_str());
                rocInfo.erase(std::remove(rocInfo.begin(), rocInfo.end(), '\n'), rocInfo.end());
                rocInfo = "FED" + FEDid + ss.str() + ": " + rocInfo;
                fedCh2Roc_.insert(std::make_pair(fedChPair, rocInfo));
            }

            std::stringstream ssm;
            ssm << "roc" << i;
            res += ", " +
                   pixel::utils::escapeAsJSONString(ssm.str()) +
                   ": " + pixel::utils::escapeAsJSONString(fedCh2Roc_[fedChPair]);
        }
        // End of JSON
        res += "}";
    }

    res = "[" + res + "]"; // [{},{}, ...{}]
    return res;
}
