#include "PixelFEDSupervisor/include/monitor/rack/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xoap/version.h"
#include "xoap/filter/version.h"

#include "PixelFEDSupervisor/include/monitor/exception/version.h"
#include "pixel/hwlayer/version.h"
#include "pixel/utils/version.h"

GETPACKAGEINFO(pixelmonitorrack)

void
pixelmonitorrack::checkPackageDependencies() {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(tcdsexception);
    CHECKDEPENDENCY(pixelmonitorhwlayer);
    CHECKDEPENDENCY(pixelmonitorutilslayer);
    CHECKDEPENDENCY(xcept);
    CHECKDEPENDENCY(xdaq);
    CHECKDEPENDENCY(xoap);
    CHECKDEPENDENCY(xoapfilter);
}

std::set<std::string, std::less<std::string> >
pixelmonitorrack::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, tcdsexception);
    ADDDEPENDENCY(dependencies, pixelmonitorhwlayer);
    ADDDEPENDENCY(dependencies, pixelmonitorutilslayer);
    ADDDEPENDENCY(dependencies, xcept);
    ADDDEPENDENCY(dependencies, xdaq);
    ADDDEPENDENCY(dependencies, xoap);
    ADDDEPENDENCY(dependencies, xoapfilter);

    return dependencies;
}
