#include "PixelFEDSupervisor/include/monitor/rack/FEDRackConfigurationInfoSpaceHandler.h"

#include <string>
#include <vector>
#include "xdaq/Application.h"
#include "xdaq/ContextDescriptor.h"
#include "xdaq/ApplicationContext.h"

#include "pixel/utils/InfoSpaceItem.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/WebServer.h"

pixel::rack::FEDRackConfigurationInfoSpaceHandler::FEDRackConfigurationInfoSpaceHandler(xdaq::Application *const xdaqApp)
    : pixel::utils::ConfigurationInfoSpaceHandler(xdaqApp) {
    createString("lasURL",
                 "unknown",
                 "",
                 pixel::utils::InfoSpaceItem::NOUPDATE,
                 true);
    createString("lasServiceName",
                 "xmaslas2g",
                 "",
                 pixel::utils::InfoSpaceItem::NOUPDATE,
                 true);
    createString("flashlistName",
                 "FEDMonitor",
                 "",
                 pixel::utils::InfoSpaceItem::NOUPDATE,
                 true);
    createString("SOAPCommandFromSupervisor",
                 "n/a",
                 "",
                 pixel::utils::InfoSpaceItem::NOUPDATE,
                 true);
    contextVec_ =
        xdaqApp->getApplicationContext()->getContextTable()->getContextDescriptors();
    std::vector<xdaq::ContextDescriptor *>::const_iterator contextVecIter;
    for (contextVecIter = contextVec_.begin();
         contextVecIter != contextVec_.end();
         ++contextVecIter) {
        //std::cout<<(*contextVecIter)->getURL()<<std::endl;
        std::string url = (*contextVecIter)->getURL();
        if (url[url.size() - 1] == '/') {
            url = url.substr(0, url.size() - 1);
        }
        createString(url,
                     "Halted",
                     "",
                     pixel::utils::InfoSpaceItem::NOUPDATE,
                     true);
    }
}

void
pixel::rack::FEDRackConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(pixel::utils::Monitor &monitor) {
    pixel::utils::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(monitor);
    std::string itemSetName = "Application configuration"; //"FEDRack-configIs-itemset";
    //monitor.newItemSet(itemSetName);
    monitor.addItem(itemSetName,
                    "lasURL",
                    "LAS URL",
                    this);
    monitor.addItem(itemSetName,
                    "lasServiceName",
                    "LAS service name",
                    this);
    monitor.addItem(itemSetName,
                    "flashlistName",
                    "Flashlist Name",
                    this);
    monitor.addItem(itemSetName,
                    "SOAPCommandFromSupervisor",
                    "SOAP Command From Supervisor",
                    this);
    std::vector<xdaq::ContextDescriptor *>::const_iterator contextVecIter;
    for (contextVecIter = contextVec_.begin();
         contextVecIter != contextVec_.end();
         ++contextVecIter) {
        std::string url = (*contextVecIter)->getURL();
        if (url[url.size() - 1] == '/') {
            url = url.substr(0, url.size() - 1);
        }
        monitor.addItem(itemSetName,
                        url,
                        url,
                        this);
    }
}

/*--
void
pixel::rack::FEDRackConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
                                                                                        pixel::utils::Monitor& monitor)
{
    std::string const tabName = "Configuration";
    webServer.registerTab(tabName,
                          "Configuration parameters",
                          2);
    webServer.registerTable("Application configuration",
                            "Application configuration parameters",
                            monitor,
                            "FEDRack-configIs-itemset",
                            tabName);
}
--*/
