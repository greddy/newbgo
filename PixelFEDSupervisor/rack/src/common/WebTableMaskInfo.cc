#include "PixelFEDSupervisor/include/monitor/rack/WebTableMaskInfo.h"

#include <sstream>
#include <string>
#include <vector>
#include <iomanip>

#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/WebObject.h"

pixel::rack::WebTableMaskInfo::WebTableMaskInfo(std::string const &name,
                                                       std::string const &description,
                                                       pixel::utils::Monitor const &monitor,
                                                       std::string const &itemSetName,
                                                       std::string const &tabName,
                                                       size_t const colSpan)
    : pixel::utils::WebObject(name, description, monitor, itemSetName, tabName, colSpan) {
}

std::string
pixel::rack::WebTableMaskInfo::getHTMLString() const {
    std::stringstream res;
    res << "<div class=\"tcds-item-table-wrapper\">"
        << "\n";

    res << "<p class=\"tcds-item-table-title\">"
        << getName()
        << "</p>";
    res << "\n";

    res << "<p class=\"tcds-item-table-description\">"
        << getDescription()
        << "</p>";
    res << "\n";

    // And now the actual table contents.
    pixel::utils::Monitor::StringPairVector items =
        monitor_.getFormattedItemSet(itemSetName_);
    // ASSERT ASSERT ASSERT
    assert(items.size() == 1);
    // ASSERT ASSERT ASSERT end

    std::string const itemName = items.at(0).first;
    std::string const tmp = "[\"" + itemSetName_ + "\"][\"" + itemName + "\"]";
    std::string const invalidVal = pixel::utils::WebObject::kStringInvalidItem;

    res << "<script type=\"text/x-dot-template\">";
    // Case 0: invalid item ('-').
    res << "{{? it" << tmp << " == '" << invalidVal << "'}}"
        << "<span>" << invalidVal << "</span>"
        << "\n";
    // Case 1: a (hopefully non-empty) array.
    res << "{{??}}"
        << "\n"
        << "<table id=\"rackTable_mask\" class=\"xdaq-table tcds-item-table sortable pixelmonitor-table-compact\">"
        << "\n"
        << "<thead>"
        << "<tr>"
        << "<th>FED</th>"
        << "<th>URL</th>";
    for (int i = 1; i <= 48; ++i) {
        std::stringstream ss;
        ss << "CH" << std::setfill('0') << std::setw(2) << i;
        res << "<th>" << ss.str() << "</th>";
    }
    res << "</tr>"
        << "</thead>"
        << "\n"
        << "<tbody>"
        << "\n"
        << "{{~it" << tmp << " :value:index}}"
        << "\n"
        << "<tr>"
        << "\n"
        << "<td class=\"firstColumn\">{{=value[\"FEDid\"]}}</td>"
        << "<td><a href=\"{{=value[\"url\"]}}\" target=\"_blank\">ClickMe</a></td>";
    for (int i = 1; i <= 48; ++i) {
        std::stringstream ss;
        ss << "CH" << std::setfill('0') << std::setw(2) << i;
        std::stringstream ssm;
        ssm << "roc" << i;
        res << "<td class=\"xdaq-tooltip\">"
            << "<span class=\"xdaq-tooltip-msg\">{{=value[\"" << ssm.str() << "\"]}}</span>"
            << "<span class=\"ms\">{{=value[\"" << ss.str() << "\"]}}</span>"
            << "</td>";
    }
    res << "\n"
        << "</tr>"
        << "\n"
        << "{{~}}"
        << "\n"
        << "</tbody>"
        << "\n"
        << "</table>"
        << "\n";
    res << "{{?}}"
        << "\n";
    res << "</script>"
        << "\n"
        << "<div class=\"target\"></div>"
        << "\n";

    return res.str();
}
