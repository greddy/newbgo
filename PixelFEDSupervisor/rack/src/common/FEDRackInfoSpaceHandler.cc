#include "PixelFEDSupervisor/include/monitor/rack/FEDRackInfoSpaceHandler.h"

#include "PixelFEDSupervisor/include/monitor/rack/WebTableFlInfo.h"
#include "PixelFEDSupervisor/include/monitor/rack/WebTableMaskInfo.h"
#include "pixel/utils/Monitor.h"
#include "pixel/utils/Utils.h"
#include "pixel/utils/WebServer.h"

pixel::rack::FEDRackInfoSpaceHandler::FEDRackInfoSpaceHandler(xdaq::Application *xdaqApp,
                                                                     pixel::utils::InfoSpaceUpdater *updater)
    : InfoSpaceHandler(xdaqApp, "pixel-rack-monitor", updater) {
    // hold flashlist information
    createString("fl_info", "", "json-object");
    createString("mask_info", "", "json-object");
}

pixel::rack::FEDRackInfoSpaceHandler::~FEDRackInfoSpaceHandler() {
}

void
pixel::rack::FEDRackInfoSpaceHandler::registerItemSetsWithMonitor(pixel::utils::Monitor &monitor) {
    std::string const itemSetName_0 = "fs-itemset";
    monitor.newItemSet(itemSetName_0);
    monitor.addItem(itemSetName_0,
                    "fl_info",
                    "flashlist information",
                    this);
    std::string const itemSetName_1 = "ms-itemset";
    monitor.newItemSet(itemSetName_1);
    monitor.addItem(itemSetName_1,
                    "mask_info",
                    "channel mask information",
                    this);
}

void
pixel::rack::FEDRackInfoSpaceHandler::registerItemSetsWithWebServer(pixel::utils::WebServer &webServer,
                                                                           pixel::utils::Monitor &monitor) {
    std::string const tabName_0 = "FED-MONITOR-LIST";
    std::string const tabDesc_0 = "<ul> \
                                      <li><span class=\"xdaq-green\">FEDMonitor</span>  RDY</li> \
                                      <li><span class=\"xdaq-orange\">FEDMonitor</span>  WRN</li> \
                                      <li><span class=\"xdaq-yellow\">FEDMonitor</span>  OOS</li> \
                                      <li><span class=\"xdaq-red\">FEDMonitor</span>  BSY</li> \
                                   </ul>";
    webServer.registerTab(tabName_0,
                          tabDesc_0,
                          1);
    webServer.registerWebObject<WebTableFlInfo>("FED Rack table",
                                                "",
                                                monitor,
                                                "fs-itemset",
                                                tabName_0);

    std::string const tabName_1 = "CHANNEL-MASK-STATUS";
    std::string const tabDesc_1 = "<ul> \
                                      <li><span class=\"xdaq-green\">Mask</span> Enabled, not masked</li> \
                                      <li><span class=\"xdaq-yellow\">Mask</span> Enabled, auto-masked</li> \
                                      <li><span class=\"xdaq-red\">Mask</span> Enabled, tbm-masked</li> \
                                      <li><span class=\"xdaq-darkgrey\">Mask</span>  Disabled</li> \
                                   </ul>";
    webServer.registerTab(tabName_1,
                          tabDesc_1,
                          1);
    webServer.registerWebObject<WebTableMaskInfo>("FED channel masking status table",
                                                  "",
                                                  monitor,
                                                  "ms-itemset",
                                                  tabName_1);
}

std::string
pixel::rack::FEDRackInfoSpaceHandler::formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const &item) const {
    std::string res = pixel::utils::escapeAsJSONString(kInvalidItemString_);
    if (item->isValid()) {
        std::string name = item->name();
        if (name == "fl_info" || name == "mask_info") {
            res = getString(name);
        } else {
            res = InfoSpaceHandler::formatItem(item);
        }
    }
    return res;
}
