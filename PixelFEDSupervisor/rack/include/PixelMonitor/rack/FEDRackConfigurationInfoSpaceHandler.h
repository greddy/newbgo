#ifndef _PixelFEDSupervisor_PixelMonitor_rack_FEDRackConfigurationInfoSpaceHandler_h
#define _PixelFEDSupervisor_PixelMonitor_rack_FEDRackConfigurationInfoSpaceHandler_h

#include <vector>

#include "xdaq/ContextDescriptor.h"
#include "pixel/utils/ConfigurationInfoSpaceHandler.h"

namespace xdaq {
class Application;
}

namespace PixelFEDSupervisor{
namespace PixelMonitor {
namespace utilslayer {
class Monitor;
class WebServer;
}

namespace rack {
class FEDRackConfigurationInfoSpaceHandler : public pixel::utils::ConfigurationInfoSpaceHandler {
  public:
    FEDRackConfigurationInfoSpaceHandler(xdaq::Application *const xdaqApp);

  private:
    virtual void registerItemSetsWithMonitor(pixel::utils::Monitor &monitor);
    std::vector<xdaq::ContextDescriptor *> contextVec_;
    //virtual void registerItemSetsWithWebServer(pixel::utils::WebServer& webServer,
    //                                           pixel::utils::Monitor& monitor);
};
} // namespace rack
} // namespace PixelMonitor
} // namespace PixelFEDSupervisor

#endif // _PixelFEDSupervisor_PixelMonitor_rack_FEDRackConfigurationInfoSpaceHandler_h
