#ifndef _PixelFEDSupervisor_PixelMonitor_rack_FEDRackInfoSpaceHandler_h_
#define _PixelFEDSupervisor_PixelMonitor_rack_FEDRackInfoSpaceHandler_h_

#include <string>

#include "pixel/utils/InfoSpaceHandler.h"

namespace xdaq {
class Application;
}

namespace PixelFEDSupervisor{
namespace PixelMonitor {
namespace utilslayer {
class InfoSpaceHandler;
class Monitor;
class WebServer;
}

namespace rack {
class FEDRackInfoSpaceHandler : public pixel::utils::InfoSpaceHandler {
  public:
    FEDRackInfoSpaceHandler(xdaq::Application *xdaqApp,
                            pixel::utils::InfoSpaceUpdater *updater);
    virtual ~FEDRackInfoSpaceHandler();

  private:
    virtual void registerItemSetsWithMonitor(pixel::utils::Monitor &monitor);
    virtual void registerItemSetsWithWebServer(pixel::utils::WebServer &webServer,
                                               pixel::utils::Monitor &monitor);
    virtual std::string formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const &item) const;
};

} // namespace rack
} // namespace PixelMonitor
} // namespace PixelFEDSupervisor

#endif // _PixelFEDSupervisor_PixelMonitor_rack_FEDRackInfoSpaceHandler_h_
