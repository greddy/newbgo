#ifndef _PixelFEDSupervisor_PixelMonitor_rack_WebTableFlInfo_h
#define _PixelFEDSupervisor_PixelMonitor_rack_WebTableFlInfo_h

#include <cstddef>
#include <string>

#include "pixel/utils/WebObject.h"

namespace PixelFEDSupervisor{
namespace PixelMonitor {
namespace utilslayer {
class Monitor;
}

namespace rack {

class WebTableFlInfo : public pixel::utils::WebObject {
  public:
    WebTableFlInfo(std::string const &name,
                   std::string const &description,
                   pixel::utils::Monitor const &monitor,
                   std::string const &itemSetName,
                   std::string const &tabName,
                   size_t const colSpan);

    std::string getHTMLString() const;
};

} // namespace rack
} // namespace PixelMonitor
} // namespace PixelFEDSupervisor

#endif // _PixelFEDSupervisor_PixelMonitor_rack_WebTableFlInfo_h
