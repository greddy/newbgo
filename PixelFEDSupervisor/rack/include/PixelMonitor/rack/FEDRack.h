#ifndef _PixelFEDSupervisor_PixelMonitor_rack_FEDRack_h_
#define _PixelFEDSupervisor_PixelMonitor_rack_FEDRack_h_

#include <string>
#include <vector>
#include "toolbox/task/WorkLoop.h"
#include "xoap/exception/Exception.h"
#include "xoap/MessageReference.h"

#include "pixel/utils/XDAQAppBase.h"

namespace xdaq {
class ApplicationStub;
}

namespace xgi {
class Input;
class Output;
}

namespace PixelFEDSupervisor{
namespace PixelMonitor {
namespace rack {
class FEDRackInfoSpaceHandler;
class FEDRackInfoSpaceUpdater;

class FEDRack : public pixel::utils::XDAQAppBase {
  public:
    XDAQ_INSTANTIATOR();

    FEDRack(xdaq::ApplicationStub *const stub);
    virtual ~FEDRack();

  protected:
    virtual void setupInfoSpaces();
    xoap::MessageReference enableAll(xoap::MessageReference msg) ;
    xoap::MessageReference disableAll(xoap::MessageReference msg) ;

    virtual void hwConnectImpl();
    virtual void hwReleaseImpl();
    virtual void hwConfigureImpl();

  private:
    bool enableAllCore(toolbox::task::WorkLoop *wl);
    bool disableAllCore(toolbox::task::WorkLoop *wl);
    std::unique_ptr<pixel::rack::FEDRackInfoSpaceHandler> rackHandlerP_;
    std::unique_ptr<pixel::rack::FEDRackInfoSpaceUpdater> rackUpdaterP_;

    std::string targetURLs_;
};

} // namespace rack
} // namespace PixelMonitor
} // namespace PixelFEDSupervisor

#endif // _PixelFEDSupervisor_PixelMonitor_rack_FEDRack_h_
