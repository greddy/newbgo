#ifndef _pixelmonitorrack_version_h_
#define _pixelmonitorrack_version_h

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define PIXELMONITORRACK_VERSION_MAJOR 2
#define PIXELMONITORRACK_VERSION_MINOR 5
#define PIXELMONITORRACK_VERSION_PATCH 3

// If any previous versions available:
#define PIXELMONITORRACK_PREVIOUS_VERSIONS "2.5.2"
// else:
// #undef PIXELMONITORRACK_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define PIXELMONITORRACK_VERSION_CODE PACKAGE_VERSION_CODE(PIXELMONITORRACK_VERSION_MAJOR, PIXELMONITORRACK_VERSION_MINOR, PIXELMONITORRACK_VERSION_PATCH)
#ifndef PIXELMONITORRACK_PREVIOUS_VERSIONS
#define PIXELMONITORRACK_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PIXELMONITORRACK_VERSION_MAJOR, PIXELMONITORRACK_VERSION_MINOR, PIXELMONITORRACK_VERSION_PATCH)
#else
#define PIXELMONITORRACK_FULL_VERSION_LIST PIXELMONITORRACK_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PIXELMONITORRACK_VERSION_MAJOR, PIXELMONITORRACK_VERSION_MINOR, PIXELMONITORRACK_VERSION_PATCH)
#endif

namespace pixelmonitorrack {
const std::string package = "pixelmonitorrack";
const std::string versions = PIXELMONITORRACK_FULL_VERSION_LIST;
const std::string description = "CMS software for the PixelMonitor.";
const std::string authors = "Weinan Si";
const std::string summary = "CMS software for the PixelMonitor.";
const std::string link = "http://wsi.web.cern.ch/wsi/pixelmonitor/";
config::PackageInfo getPackageInfo();
void checkPackageDependencies();
std::set<std::string, std::less<std::string> > getPackageDependencies();
} // namespace pixelmonitorrack

#endif
