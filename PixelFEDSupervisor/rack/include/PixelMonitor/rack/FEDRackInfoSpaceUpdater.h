#ifndef _PixelFEDSupervisor_PixelMonitor_rack_FEDRackInfoSpaceUpdater_h
#define _PixelFEDSupervisor_PixelMonitor_rack_FEDRackInfoSpaceUpdater_h

#include <string>
#include <map>

#include "pixel/utils/InfoSpaceUpdater.h"

namespace PixelFEDSupervisor{
namespace PixelMonitor {
namespace utilslayer {
class InfoSpaceHandler;
class InfoSpaceItem;
}

namespace rack {
class FEDRack;
class FEDRackInfoSpaceUpdater : public pixel::utils::InfoSpaceUpdater {
  public:
    FEDRackInfoSpaceUpdater(pixel::rack::FEDRack &xdaqApp);
    virtual ~FEDRackInfoSpaceUpdater();

    virtual bool updateInfoSpaceItem(pixel::utils::InfoSpaceItem &item,
                                     pixel::utils::InfoSpaceHandler *const infoSpaceHandler);

  private:
    std::string getFlInfo() const;
    std::string getMaskInfo();
    pixel::rack::FEDRack &xdaqApp_;
    std::map<std::pair<int, int>, std::string> fedCh2Roc_;
};
} // namespace rack
} // namespace PixelMonitor
} // namespace PixelFEDSupervisor

#endif // _PixelFEDSupervisor_PixelMonitor_rack_FEDRackInfoSpaceUpdater_h
