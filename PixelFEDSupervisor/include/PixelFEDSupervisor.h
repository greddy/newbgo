#ifndef _pixel_PixelFEDSupervisor_h_
#define _pixel_PixelFEDSupervisor_h_

#include "toolbox/BSem.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "VMEDevice.hh"
#include "VMEAddressTable.hh"
#include "VMEAddressTableASCIIReader.hh"

#include "PixelUtilities/PixeluTCAUtilities/include/RegManager.h"

#include "pixel/utils/Timer.h"
#include "pixel/utils/XDAQAppWithFSMPixelBase.h"

#include "pixel/utils/FSMStateSummaryHandler.h"
#include "pixel/utils/FSMStateSummaryUpdater.h"

#include "PixelCalibrations/include/PixelFEDCalibrationBase.h"

#include "PixelFEDSupervisor/include/FEDHwDeviceTCA.h"
#include "PixelFEDSupervisor/include/TCADeviceFED.h"

#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"
#include "PixelSupervisorConfiguration/include/PixelFEDSupervisorConfiguration.h"

// used for monitoring
#include "PixelUtilities/PixelFEDDataTools/include/Moments.h"

namespace xdaq {
class ApplicationStub;
}

namespace pixel {

class PixFEDInfoSpaceHandler;
class PixFEDInfoSpaceUpdater;

class PixelFEDSupervisor : public pixel::utils::XDAQAppWithFSMPixelBase,
                           public PixelFEDSupervisorConfiguration {
  public:
    XDAQ_INSTANTIATOR();

    PixelFEDSupervisor(xdaq::ApplicationStub* stub);
    virtual ~PixelFEDSupervisor();


    bool PhysicsRunning(toolbox::task::WorkLoop *w1);
    bool monitorUpdate(toolbox::task::WorkLoop *w2);
    void submit_workloops();
    void start_workloops();
    void stop_workloops();
    void remove_workloops();

    std::vector<pixel::utils::FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const &commandName) const;

    virtual void initializeActionImpl(toolbox::Event::Reference event);
    virtual void transitionHaltedToConfiguringActionImpl(toolbox::Event::Reference event);
    virtual void configureActionImpl(toolbox::Event::Reference event);
    virtual void startActionImpl(toolbox::Event::Reference event);
    virtual void stopActionImpl(toolbox::Event::Reference event);
    virtual void pauseActionImpl(toolbox::Event::Reference event);
    virtual void resumeActionImpl(toolbox::Event::Reference event);
    virtual void transitionConfiguredToHaltingActionImpl(toolbox::Event::Reference event);
    virtual void transitionRunningOrPausedToHaltingActionImpl(toolbox::Event::Reference event);
    virtual void haltActionImpl(toolbox::Event::Reference event);
    virtual void reconfigureActionImpl(toolbox::Event::Reference event);
    virtual void fixSoftErrorActionImpl(toolbox::Event::Reference event);
    virtual void resumeFromSoftErrorActionImpl(toolbox::Event::Reference event);

    xoap::MessageReference changeStateImpl(xoap::MessageReference msg);

    xoap::MessageReference Recover(xoap::MessageReference msg);

    xoap::MessageReference ArmOSDFifo(xoap::MessageReference msg);
    xoap::MessageReference ReadOSDValues(xoap::MessageReference msg);
    void b2inEvent(toolbox::mem::Reference *msg, xdata::Properties &plist);

    xoap::MessageReference ReadDataAndErrorFIFO(xoap::MessageReference msg);
    xoap::MessageReference SetAutoPhases(xoap::MessageReference msg);
    xoap::MessageReference FindPhasesNow(xoap::MessageReference msg);
    xoap::MessageReference SetCalibNEvents(xoap::MessageReference msg);
    xoap::MessageReference ResetFEDsEnMass(xoap::MessageReference msg);
    xoap::MessageReference VMETrigger(xoap::MessageReference msg);
    xoap::MessageReference ReadRSSI(xoap::MessageReference msg);
    xoap::MessageReference ReadOSDFifo(xoap::MessageReference msg);

    xoap::MessageReference SendBackProblemChannels(xoap::MessageReference msg);
    xoap::MessageReference FEDCalibrations(xoap::MessageReference msg);
    xoap::MessageReference beginCalibration(xoap::MessageReference msg);
    xoap::MessageReference endCalibration(xoap::MessageReference msg);

    xoap::MessageReference ReadFIFO(xoap::MessageReference msg);
    xoap::MessageReference ReadErrorFIFO(xoap::MessageReference msg);

    void jobConfigure();

    //Soft Error Stuff
    void DetectSoftError(std::string const channel_info = "",
                         int const fedinfo = 0);
                         
    bool isFEDchannelActive(unsigned int iFED,unsigned int ichannel);


  private:
    void deleteHardware();
    void setupFEDInterface(PixelPh1FEDInterface *interf, pos::PixelFEDCard *theFEDCard);
    
    boost::mutex lock_FEDinterfaceSetup;

    typedef std::map<unsigned long, HAL::VMEDevice *> VMEPtrMap;
    VMEPtrMap VMEPtr_;
    typedef std::map<unsigned long, RegManager *> RegMgrMap;
    RegMgrMap RegMgr_;

    typedef std::unique_ptr<pixel::PixFEDInfoSpaceUpdater> fedInfoSpaceUpdaterPtr;
    typedef std::unique_ptr<pixel::PixFEDInfoSpaceHandler> fedInfoSpaceHandlerPtr;
    typedef pixel::PixFEDInfoSpaceUpdater fedInfoSpaceUpdater;
    typedef pixel::PixFEDInfoSpaceHandler fedInfoSpaceHandler;

    std::map<unsigned long, fedInfoSpaceUpdaterPtr> fedUpdaterMap;
    std::map<unsigned long, fedInfoSpaceHandlerPtr> fedHandlerMap;
    
    typedef std::map<unsigned long, pixel::hwlayertca::FEDHwDeviceTCA *> fedHWMap;
    fedHWMap fedHwDeviceMap;

    std::string runNumber_;
    std::string outputDir_;
    log4cplus::Logger &sv_logger_;
    
    std::unique_ptr<pixel::FSMStateSummaryUpdater> FSMStateSummaryUpdaterP_;
    std::unique_ptr<pixel::utils::FSMStateSummaryHandler> FSMStateSummaryP_;

    toolbox::BSem *phlock_;
    bool workloopContinue_;

    // Keep track of when we've sent SOAP messages in physics workloop
    // so we don't send them multiple times
    bool physicsRunningSentRunningDegraded,
         physicsRunningSentSoftErrorDetected;

    xdata::UnsignedInteger64 maximumActiveFEDs;
    int eventNumber_;
    int countLoopsThisRun_;
    
    //toolbox::task::WorkLoop *workloop_;
    //toolbox::task::ActionSignature *tempTransmitter_;
    //toolbox::task::ActionSignature *physicsRunning_;
    int physicsrunning_workloop_sleep_;
    int timesphysicsrunningcalled;
    
    
    toolbox::task::WorkLoop  *monitor_workloop_;
    toolbox::task::ActionSignature *monitorUpdate_;
    int monitorUpdate_workloop_sleep_;
    int times_monitorUpdatecalled;

    // Application Descriptors
    const xdaq::ApplicationDescriptor *PixelDCStoFEDDpInterface_;
    const xdaq::ApplicationDescriptor *PixelSupervisor_;

    PixelFEDCalibrationBase *theFEDCalibrationBase_;

    //std::map<  FEDHwDeviceTCA

    // Maps of error count
    std::map<uint, std::map<uint, uint> > errorCountMap; // [fed][chan] count;  chan=0 is all channels
    // Maps of FIFO status & Link Full Flag
    std::map<uint, Moments> lffMap;                                   // [fed] fraction_ON
    std::map<uint, std::map<ushort, Moments> > fifoStatusMap; // [fed][bit] fraction_ON

    // Maps of baseline correction information.
    ////std::map<uint, std::map<uint, pair<float, float> > > baselineCorrectionMap;
    
    //save the map of channels that are actively used
    std::map<unsigned int, std::set<unsigned int> > activeFEDChannelMap;

    uint64_t *buffer64;

    std::map<ushort, FILE *> dataFile_;
    std::map<ushort, FILE *> dataFileT_;
    std::map<ushort, FILE *> dataFileS_;
    std::map<ushort, FILE *> timestampFile_;
    std::map<ushort, FILE *> errorFile_;

    std::map<ulong, FILE *> fout_; // C I/O
    std::map<ulong, bool> fileopen_;
    std::map<ulong, FILE *> errorFile2_;
    std::map<ulong, bool> errBufferOpen_;

    // PhysicsRunning
    bool readSpyFifo3,
         readErrorFifo,
         fifo3errcntr,
         readFifoStatusAndLFF,
         useSEURecovery,    // Enable SEU recovery mechanism (i.e. detect)
         doSEURecovery,     // Do SEU recovery mechanism (added 6/9, dk.)
         doOOSRefresh;      // do periodic reset ch OOS counters in FED
    double OOSRefreshPeriod; // period in seconds

    //data structure which keeps track of blacklisted channels
    map<int, std::bitset<48> > blacklistFedChannelMap_;

    void closeOutputFiles();
    void reportStatistics();
    void EndOfRunFEDReset();
    void SEUCountReset();

  protected:
    void setupInfoSpaces();

    //pixel::hwlayertca::FEDHwDeviceTCA &getFEDHw() const;

    void hwConnectImpl();
    void hwReleaseImpl();

    std::string htmlbase_, datbase_;
    pixel::utils::Timer OOSRefreshTimer_;
};

} // namespace pixel

#endif // _pixel_PixelFEDSupervisor_h_
