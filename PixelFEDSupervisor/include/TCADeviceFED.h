#ifndef _pixel_TCADeviceFED_h_
#define _pixel_TCADeviceFED_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>
#include "pixel/hwlayertca/TCADeviceBase.h"
#include "pixel/hwlayertca/TCACarrierBase.h"
#include "pixel/hwlayertca/TCACarrierFC7.h"

namespace pixel {

class TCADeviceFED : public pixel::hwlayertca::TCADeviceBase{

    public:
      TCADeviceFED();
      virtual ~TCADeviceFED();



};

} // namespace pixel

#endif // _pixel_TCADeviceFED_h_
