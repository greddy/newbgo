#ifndef _PixelFEDSupervisor_PixelMonitor_FEDConfigurationInfoSpaceHandler_h_
#define _PixelFEDSupervisor_PixelMonitor_FEDConfigurationInfoSpaceHandler_h_

#include <string>
#include <vector>

#include "pixel/utils/ConfigurationInfoSpaceHandler.h"
#include "pixel/utils/InfoSpaceHandler.h"

namespace pixel {
namespace utils {
class Monitor;
} // namespace utils
} // namespace pixel
namespace xdaq {
class Application;
}

namespace pixel {

class FEDConfigurationInfoSpaceHandler : public pixel::utils::ConfigurationInfoSpaceHandler {

  public:
    FEDConfigurationInfoSpaceHandler(xdaq::Application *xdaqApp);
    virtual ~FEDConfigurationInfoSpaceHandler();

  protected:
    virtual void registerItemSetsWithMonitor(pixel::utils::Monitor &monitor);
    virtual std::string formatItem(pixel::utils::InfoSpaceHandler::ItemVec::const_iterator const &item) const;
};

} // namespace pixel

#endif // _PixelFEDSupervisor_PixelMonitor_FEDConfigurationInfoSpaceHandler_h_
