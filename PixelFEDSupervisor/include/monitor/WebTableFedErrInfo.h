#ifndef _PixelFEDSupervisor_PixelMonitor_WebTableFedErrInfo_h
#define _PixelFEDSupervisor_PixelMonitor_WebTableFedErrInfo_h

#include <cstddef>
#include <string>

#include "pixel/utils/WebObject.h"

namespace pixel {
namespace utils {
class Monitor;
} // namespace utils



class WebTableFedErrInfo : public pixel::utils::WebObject {
  public:
    WebTableFedErrInfo(std::string const &name,
                       std::string const &description,
                       pixel::utils::Monitor const &monitor,
                       std::string const &itemSetName,
                       std::string const &tabName,
                       size_t const colSpan);

    std::string getHTMLString() const;
};


} // namespace pixel
#endif // _PixelFEDSupervisor_PixelMonitor_WebTableFedErrInfo_h
