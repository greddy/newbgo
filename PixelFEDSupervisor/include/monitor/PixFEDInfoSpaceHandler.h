#ifndef _PixelFEDSupervisor_PixelMonitor_PixFEDInfoSpaceHandler_h_
#define _PixelFEDSupervisor_PixelMonitor_PixFEDInfoSpaceHandler_h_

#include <string>

#include "pixel/utils/InfoSpaceHandler.h"
#include "PixelFEDSupervisor/include/monitor/FEDReader.h"


namespace pixel {
namespace utils {
class InfoSpaceUpdater;
class Monitor;
class WebServer;
} // namespace utils
} // namespace pixel
namespace xdaq {
class Application;
}

namespace pixel{

class PixFEDInfoSpaceUpdater;
class FEDReader;

class PixFEDInfoSpaceHandler : public utils::InfoSpaceHandler {

  public:
    PixFEDInfoSpaceHandler(xdaq::Application *xdaqApp, std::string infoSpaceName,
                           utils::InfoSpaceUpdater *updater, unsigned long int fednr_incrate);

    virtual ~PixFEDInfoSpaceHandler();

  private:
    virtual void registerItemSetsWithMonitor(utils::Monitor &monitor);
    //virtual void registerItemSetsWithWebServer(utils::WebServer &webServer,
                                               //utils::Monitor &monitor);
    virtual void registerItemSetsWithWebServer(pixel::utils::WebServer &webServer,
                                               pixel::utils::Monitor &monitor,
                                               std::string const &tabName = "");
    virtual std::string formatItem(utils::InfoSpaceHandler::ItemVec::const_iterator const &item) const;
    std::string fednr_incrate_str;
};

} // namespace pixel

#endif // _PixelFEDSupervisor_PixelMonitor_PixFEDInfoSpaceHandler_h_
