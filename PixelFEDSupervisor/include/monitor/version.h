#ifndef _PixelFEDSupervisor_PixelMonitor_version_h_
#define _PixelFEDSupervisor_PixelMonitor_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define PIXELMONITOR_VERSION_MAJOR 1
#define PIXELMONITOR_VERSION_MINOR 3
#define PIXELMONITOR_VERSION_PATCH 8

// If any previous versions available:
#define PIXELMONITOR_PREVIOUS_VERSIONS "1.3.7"
// else:
// #undef PIXELMONITOR_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define PIXELMONITOR_VERSION_CODE PACKAGE_VERSION_CODE(PIXELMONITOR_VERSION_MAJOR, PIXELMONITOR_VERSION_MINOR, PIXELMONITOR_VERSION_PATCH)
#ifndef PIXELMONITOR_PREVIOUS_VERSIONS
#define PIXELMONITOR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PIXELMONITOR_VERSION_MAJOR, PIXELMONITOR_VERSION_MINOR, PIXELMONITOR_VERSION_PATCH)
#else
#define PIXELMONITOR_FULL_VERSION_LIST PIXELMONITOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PIXELMONITOR_VERSION_MAJOR, PIXELMONITOR_VERSION_MINOR, PIXELMONITOR_VERSION_PATCH)
#endif

namespace pixelmonitor {

const std::string package = "pixelmonitor";
const std::string versions = PIXELMONITOR_FULL_VERSION_LIST;
const std::string description = "CMS PIXELMONITOR helper software.";
const std::string authors = "Weinan Si";
const std::string summary = "CMS PixelMonitor helper software";
const std::string link = "-";
config::PackageInfo getPackageInfo();
void checkPackageDependencies();
std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace pixelmonitor

#endif // _PixelFEDSupervisor_PixelMonitor_version_h
