#ifndef _PixelFEDSupervisor_PixelMonitor_FEDReader_h
#define _PixelFEDSupervisor_PixelMonitor_FEDReader_h

#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
#include <bitset>
#include <cstdlib>

#include "pixel/hwlayer/IHwDevice.h"
#include "pixel/hwlayer/RegisterInfo.h"
//#include "pixel/hwlayertca/HwDeviceTCA.h"
#include "PixelFEDSupervisor/include/FEDHwDeviceTCA.h"

namespace pixel {
namespace hwlayer {
class FEDHwDeviceTCA;
} // namespace hwlayer
} // namespace pixel

namespace pixel {

class FEDReader {
  public:
    FEDReader(pixel::hwlayertca::FEDHwDeviceTCA const &hw);
    virtual ~FEDReader();

    bool isHwConnected() const;
    std::string getBoardId() const;
    std::string getRevId() const;
    std::string getBoardCode() const;
    std::string getMACAddress() const;
    std::string getFwIPHCVersion() const;
    std::string getFwHEPHYVersion() const;
    std::string getFwIPHCDate() const;
    std::string getFwHEPHYDate() const;

    std::vector<uint32_t> getNGoodPhaseVec() const;
    uint32_t getNGoodPhaseOfChannel(uint32_t chId) const;
    std::vector<uint32_t> getPLLlockedInfo() const;
    uint64_t getTimeInTTSReady() const;
    uint64_t getTimeInTTSBusy() const;
    uint64_t getTimeInTTSOOS() const;
    uint64_t getTimeInTTSWarn() const;
    uint32_t getTransitionsToTTSReady() const;
    uint32_t getTransitionsToTTSBusy() const;
    uint32_t getTransitionsToTTSOOS() const;
    uint32_t getTransitionsToTTSWarn() const;
    uint32_t getL1ACount() const;
    uint32_t getEvtErrNumOfChannel(uint32_t chId) const;
    uint32_t getEvtTmoNumOfChannel(uint32_t chId) const;
    uint32_t getCntEvtRsyOfChannel(uint32_t chId) const;
    uint32_t getCntTbmHidOfChannel(uint32_t chId) const;
    uint32_t getCntTrlErrOfChannel(uint32_t chId) const;
    uint32_t getMaskStatusOfChannel(uint32_t chId) const;
    uint32_t getPkamRstOfChannel(uint32_t chId) const;
    uint32_t getNoTknPssOfChannel(uint32_t chId) const;
    uint32_t getRocErrNumOfChannel(uint32_t chId) const;
    uint32_t getOvfNumOfChannel(uint32_t chId) const;
    uint32_t getTbmAtoRstOfChannel(uint32_t chId) const;

    std::vector<uint32_t> getEvtErrNumVec() const;
    std::vector<uint32_t> getEvtTmoNumVec() const;
    std::vector<uint32_t> getCntEvtRsyVec() const;
    std::vector<uint32_t> getCntTbmHidVec() const;
    std::vector<uint32_t> getCntTrlErrVec() const;
    std::bitset<48> getMaskStatus() const;
    std::bitset<48> getTBMMask() const;
    std::bitset<48> getChannelMask() const;
    std::vector<uint32_t> getPkamRstVec() const;
    std::vector<uint32_t> getNoTknPssVec() const;
    std::vector<uint32_t> getRocErrNumVec() const;
    std::vector<uint32_t> getOvfNumVec() const;
    std::vector<uint32_t> getTbmAtoRstVec() const;

    std::string getTTSState() const;

  protected:
    std::vector<std::string> getFWInfo(std::string ins) const;

  private:
    pixel::hwlayertca::FEDHwDeviceTCA const &getHw() const;
    pixel::hwlayertca::FEDHwDeviceTCA const &hw_;
    std::bitset<48> getMaskFromTwoRegs(std::string str0_32, std::string str0_16) const;
};

} // namespace pixel
#endif // _PixelFEDSupervisor_PixelMonitor_FEDReader_h
