#ifndef _PixelFEDSupervisor_PixelMonitor_PixFEDInfoSpaceUpdater_h_
#define _PixelFEDSupervisor_PixelMonitor_PixFEDInfoSpaceUpdater_h_

#include "pixel/utils/InfoSpaceUpdater.h"
#include "pixel/hwlayertca/HwDeviceTCA.h"
#include "PixelFEDSupervisor/include/PixelFEDSupervisor.h"
#include "PixelFEDSupervisor/include/monitor/FEDReader.h"

namespace pixel {
namespace utils {
class InfoSpaceHandler;
class InfoSpaceItem;
} // namespace utilslayer
} // namespace pixel

namespace pixel {
namespace hwlayer {
class HwDeviceTCA;
} // namespace hwlayer
} // namespace pixel

namespace pixel {

class PixelFEDSupervisor;
class PixFEDInfoSpaceUpdater : public pixel::utils::InfoSpaceUpdater {
  public:
    PixFEDInfoSpaceUpdater(pixel::hwlayertca::FEDHwDeviceTCA const &hw,
                           pixel::PixelFEDSupervisor &xdaqApp);
    virtual ~PixFEDInfoSpaceUpdater();
    virtual bool updateInfoSpaceItem(pixel::utils::InfoSpaceItem &item,
                                     pixel::utils::InfoSpaceHandler *const infoSpaceHandler);
    void updateInfoSpaceTrigger(pixel::utils::InfoSpaceHandler *const infoSpaceHandler);
    
  protected:
    pixel::hwlayertca::FEDHwDeviceTCA const &getHw() const;
    virtual void updateInfoSpaceImpl(pixel::utils::InfoSpaceHandler *const infoSpaceHandler);

  private:
    pixel::hwlayertca::FEDHwDeviceTCA const &hw_;
    pixel::PixelFEDSupervisor &xdaqApp_;
    pixel::FEDReader reader;
};

uint32_t sumVecOfUint32(std::vector<uint32_t> vec);

} // namespace pixel
#endif // _PixelFEDSupervisor_PixelMonitor_PixFEDInfoSpaceUpdater_h_
