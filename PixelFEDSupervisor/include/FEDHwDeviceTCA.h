#ifndef _pixel_hwlayer_FEDHwDeviceTCA_h_
#define _pixel_hwlayer_FEDHwDeviceTCA_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

#include "pixel/hwlayertca/HwDeviceTCA.h"
#include "pixel/hwlayer/RegisterInfo.h"

#include "PixelUtilities/PixeluTCAUtilities/include/RegManager.h"
// #include "hwlayer/ConfigurationProcessor.h"

namespace uhal {
class HwInterface;
}

namespace pixel {
namespace hwlayertca {

/**
     * Hardware access class for (u)TCA devices.
     */
class FEDHwDeviceTCA : public pixel::hwlayertca::HwDeviceTCA {
  public:
    typedef std::pair<pixel::hwlayer::RegisterInfo, std::vector<uint32_t> > RegContentsPair;
    typedef std::vector<RegContentsPair> RegContentsVec;

    FEDHwDeviceTCA();
    FEDHwDeviceTCA(RegManager *const regManager, unsigned long fedid);
    virtual ~FEDHwDeviceTCA();

    void setHw(RegManager *const regManager, unsigned long fedid);
    void hwConnect();
    void hwRelease();

    bool isHwConnected() const;

    uint32_t readRegister(std::string const &regName) const;
    void writeRegister(std::string const &regName,
                       uint32_t const regVal) const;
    std::vector<uint32_t> readBlock(std::string const &regName,
                                    uint32_t const nWords) const;
    std::vector<uint32_t> readBlock(std::string const &regName) const;
    void writeBlock(std::string const &regName,
                    std::vector<uint32_t> const regVal) const;

    uint32_t readModifyWriteRegister(std::string const &regName,
                                     uint32_t const regVal) const;

    uint32_t getBlockSize(std::string const &regName) const;

    std::vector<std::string> getRegisterNames() const;
    pixel::hwlayer::RegisterInfo::RegInfoVec getRegisterInfos() const;

    // void writeHardwareConfiguration(pixel::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const;
    virtual RegContentsVec readHardwareConfiguration(pixel::hwlayer::RegisterInfo::RegInfoVec const &regInfos) const;

    virtual pixel::hwlayer::RegDumpVec dumpRegisterContents() const;
    
    std::string getFEDid() const;

  private:
    RegManager* hwP_;
    bool isConnected;
    std::string fedid;
};

} // namespace hwlayer
} // namespace pixel

#endif // _pixel_hwlayer_FEDHwDeviceTCA_h_
