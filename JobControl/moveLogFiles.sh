#!/bin/sh
#DTSTAMP=`awk "BEGIN{print strftime(\"%y%b%d_%H%M\");}" `
#echo $DTSTAMP

#dirbase=/nfshome0/pixelpro/TriDAS/pixel/PixelRun/Logs
#dirbase=/pixelscratch/pixelscratch/log0
dirbase=/nfspixelraid/nfspixelraid/log0

echo my hostname is `hostname` and my dirbase is $dirbase

timestamp=`echo $1$2$3_$4_$5 | sed 's/:/-/g'`

if [ ! -d $dirbase/Log_$timestamp ]
then 
  mkdir $dirbase/Log_$timestamp
fi

#new customLogFilePath tricks
mv --backup=numbered /tmp/Pixel*.log $dirbase/Log_$timestamp/ &
#this might be enough

#catch any unrenamed logs
ls -1 /tmp/xdaqjcPID*.log >& /dev/null
if [ "$?" == "0" ]
then
mv /tmp/xdaqjcPID*.log $dirbase/Log_$timestamp &
fi

#find if xml files exist in /tmp
ls -1 /tmp/errorsLogFile*.xml >& /dev/null
if [ "$?" == "0" ]
then
#diagSystem xml files
mv /tmp/errorsLogFile*.xml $dirbase/Log_$timestamp &
fi

#clean up conf files
#this should not be done manually according to Hannes Sakulin
#ls -1 /tmp/xdaqconf*.xml >& /dev/null
#if [ "$?" == "0" ]
#then 
#rm /tmp/xdaqconf*.xml
#fi
