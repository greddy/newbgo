#include "config/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "pixel/PixelSupervisor/version.h"

GETPACKAGEINFO(PixelSupervisor)

void PixelSupervisor::checkPackageDependencies()
    {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(xcept);
    CHECKDEPENDENCY(xdaq);
}

std::set<std::string, std::less<std::string> >
PixelSupervisor::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, xcept);
    ADDDEPENDENCY(dependencies, xdaq);

    return dependencies;
}
