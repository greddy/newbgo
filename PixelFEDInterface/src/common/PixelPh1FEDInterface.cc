//#define JMT_FROMFIFO1 // in order to use FIFO1 uncomment this line

#include <cassert>
#include <iostream>
#include <iomanip>
#include <time.h>
//#include <unistd.h>

#include "PixelFEDInterface/include/PixelPh1FEDInterface.h"
#include "PixelUtilities/PixelTestStandUtilities/include/PixelTimer.h"
#include "CalibFormats/SiPixelObjects/interface/PixelFEDCard.h"

#include "PixelUtilities/PixelFEDDataTools/include/FIFO3Decoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/ErrorFIFODecoder.h"
#include "PixelUtilities/PixelFEDDataTools/include/PixelHit.h"

using namespace std;

#define DO_L1
#define DO_TEST

const int sleep10000 = 10000;
const int sleep1000 = 1000;

namespace {
const bool DO_DEBUG = false;
}

PixelPh1FEDInterface::PixelPh1FEDInterface(RegManager *const rm, const std::string &datbase)
    : regManager(rm),
      fitel_fn_base(datbase),
      slink64calls(0) {
    Printlevel = Printlevel | 16;
    num_SEU.assign(48, 0);
    runDegraded_ = false;
    lock_FEDinterfaceSetup = 0;
}

PixelPh1FEDInterface::~PixelPh1FEDInterface() {
}

int PixelPh1FEDInterface::setup(const string &fn) {
    setPixelFEDCard(pos::PixelFEDCard(fn));
    return setup();
}

int PixelPh1FEDInterface::setup(pos::PixelFEDCard c, boost::mutex *_lock_FEDinterfaceSetup) {
    lock_FEDinterfaceSetup = _lock_FEDinterfaceSetup;
    return setup(c);
}

int PixelPh1FEDInterface::setup(pos::PixelFEDCard c) {
    setPixelFEDCard(c);
    return setup();
}

int PixelPh1FEDInterface::maybeSwapFitelChannels(int ch) {
    assert(1 <= ch && ch <= 12);
    if (pixelFEDCard.swap_Fitel_order)
        return 12 - ch + 1;
    else
        return ch;
}

std::string PixelPh1FEDInterface::fitelChannelName(int ch) {
    const int ch2 = maybeSwapFitelChannels(ch);
    char ch_name[16];
    snprintf(ch_name, 16, "Ch%02d_ConfigReg", ch2);
    return std::string(ch_name);
}

int PixelPh1FEDInterface::setup() {
    std::cout << "--- Begin FED#" << pixelFEDCard.fedNumber << " setup ---" << std::endl;

    if (lock_FEDinterfaceSetup == 0) {
        lock_FEDinterfaceSetup = new boost::mutex();
    }
    // Could possibly want to set different registers for different FMC?
    // And just load the regmaps for both fmc0 and 1 even if we don't have e.g. the upper one
    fRegMapFilename[FMC0_Fitel0] = fitel_fn_base + "/FMCFITEL.txt";
    fRegMapFilename[FMC0_Fitel1] = fitel_fn_base + "/FMCFITEL.txt";
    fRegMapFilename[FMC1_Fitel0] = fitel_fn_base + "/FMCFITEL.txt";
    fRegMapFilename[FMC1_Fitel1] = fitel_fn_base + "/FMCFITEL.txt";

    LoadFitelRegMap(0, 0);
    LoadFitelRegMap(0, 1);
    LoadFitelRegMap(1, 0);
    LoadFitelRegMap(1, 1);

    // Enable the channels that are specified in the control register,
    // same scheme as old FED: channel enabled if bit is 0, channel 1 =
    // LSB.
    //
    // By default, the Fitel config is set to 2 in the reg map = digital
    // power down.  8 means enabled. So go through and turn on the
    // fibers that have a channel enabled.
    std::cout << "FED#" << pixelFEDCard.fedNumber << "cntrl_utca is 0x" << std::hex << pixelFEDCard.cntrl_utca << dec << std::endl;
    assert(pixelFEDCard.which_FMC == 0 || pixelFEDCard.which_FMC == 1);
    fibers_in_use.assign(25, 0);
    for (int channel = 0; channel < 48; ++channel) {
        if (!(pixelFEDCard.cntrl_utca & (1ULL << channel))) {
            const int which_Fitel = channel < 24;
            const int which_map = FitelMapNum(pixelFEDCard.which_FMC, which_Fitel);
            const int fiber = channel / 2 + 1;
            fibers_in_use[fiber] = 1;
            const std::string ch_name = fitelChannelName(channel % 24 / 2 + 1);
            //std::cout << "fed ch " << channel+1 << " Fitel " << which_Fitel << " map " << which_map << " " << ch_name << " -> 0x08" << std::endl;
            fRegMap[which_map][ch_name].fValue = 0x08;
        }
    }

    lock_FEDinterfaceSetup->lock();
    std::cout << "------------begin output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    std::cout << "FED#" << pixelFEDCard.fedNumber << " fibers in use:";
    for (int fiber = 1; fiber <= 24; ++fiber)
        if (fibers_in_use[fiber])
            std::cout << " " << fiber;
    std::cout << std::endl;
    std::cout << "------------end output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    lock_FEDinterfaceSetup->unlock();

    const uint32_t tbm_mask_1((pixelFEDCard.cntrl_utca_override ? pixelFEDCard.cntrl_utca_original : pixelFEDCard.cntrl_utca) & 0xFFFFFFFFULL);
    const uint32_t tbm_mask_2(((pixelFEDCard.cntrl_utca_override ? pixelFEDCard.cntrl_utca_original : pixelFEDCard.cntrl_utca) >> 32) & 0xFFFF);
    std::cout << "FED#" << pixelFEDCard.fedNumber << " TBM mask 1: 0x" << std::hex << tbm_mask_1 << "  2: 0x" << tbm_mask_2 << std::dec << std::endl;

    regManager->WriteReg("pixfed_ctrl_regs.PC_CONFIG_OK", 0);
    usleep(sleep10000);

    // make sure the clocks are set
    regManager->WriteReg("ctrl.ttc_xpoint_A_out3", 0); // Used to set the CLK input to the TTC clock from the BP - 3 is XTAL, 0 is BP
    regManager->WriteReg("ctrl.mgt_xpoint_out1", 2);   // 2 to have 156 MHz clock (obligatory for 10G sling), 3 for 125Mhz clock for 5g on SLink
    usleep(sleep10000);

    // AKBAD for firmware versions < 10.01 the reset order needs to be reversed
    // exept for version 10.02 with the old TTC decoder -.-
    // first the sw_ttc_reset then reset_all_clocks
    regManager->WriteReg("pixfed_ctrl_regs.reset_all_clocks", 1);
    usleep(sleep10000);
    regManager->WriteReg("pixfed_ctrl_regs.reset_all_clocks", 0);
    usleep(sleep10000);

    regManager->WriteReg("pixfed_ctrl_regs.sw_ttc_reset", 1);
    usleep(sleep10000);
    regManager->WriteReg("pixfed_ctrl_regs.sw_ttc_reset", 0);
    usleep(sleep10000);

    std::vector<std::pair<std::string, uint32_t> > cVecReg = pixelFEDCard.paramSetup();
    regManager->WriteStackReg(cVecReg);

    // Program the number of ROCs
    std::vector<uint32_t> cVec(24, 0);
    lock_FEDinterfaceSetup->lock();
    std::cout << "------------begin output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    for (int i = 0; i < 24; ++i) {
        cVec[i] = ((pixelFEDCard.NRocs[2 * i + 1] & 0xf) << 16) | (pixelFEDCard.NRocs[2 * i] & 0xf);
        //if (i>17) cVec[i] = 0x80008;
        std::cout << "fed fiber " << (i + 1) << " NRocs[A] = " << pixelFEDCard.NRocs[2 * i] << " NRocs[B] " << pixelFEDCard.NRocs[2 * i + 1] << " number of roc register: 0x" << std::hex << cVec[i] << std::dec << std::endl;
    }
    std::cout << "------------end output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    lock_FEDinterfaceSetup->unlock();

    regManager->WriteBlockReg("fe_ctrl_regs.number_of_rocs", cVec);

    //only use this setting if a new firmware version is used
    if (regManager->ReadReg("pixfed_stat_regs.user_iphc_fw_id.fw_ver_nb") >= 18) {
        regManager->WriteReg("pixfed_ctrl_regs.THRESH_tbm_fifo_prog_full_TTS", pixelFEDCard.THRESH_tbm_fifo_prog_full_TTS);
        regManager->WriteReg("pixfed_ctrl_regs.THRESH_tbm_fifo_prog_full_DECOD", pixelFEDCard.THRESH_tbm_fifo_prog_full_DECOD);
    }

    usleep(sleep10000);

    if (pixelFEDCard.which_FMC == 0) {
        ConfigureFitel(0, 0, true);
        ConfigureFitel(0, 1, true);
    } else if (pixelFEDCard.which_FMC == 1) {
        ConfigureFitel(1, 0, true);
        ConfigureFitel(1, 1, true);
    }

    fNthAcq = 0;
    cVecReg.clear();
    if (3 == pixelFEDCard.data_type) { //BA Emulated Hits
        regManager->WriteReg("pixfed_ctrl_regs.emul.header_id_from_sw", 0x8);
        regManager->WriteReg("pixfed_ctrl_regs.emul_trailer_id_from_sw", 0x4);
        for (int i = 0; i < 48; ++i) {
            int hits = pixelFEDCard.number_emulated_hits;
            char buff[256];
            snprintf(buff, 256, "pixfed_ctrl_regs.emul.hit_nbr_value.tbm_ch_%02i", i);
            cVecReg.push_back(std::make_pair(std::string(buff), hits));
        }
        regManager->WriteStackReg(cVecReg);
        cVecReg.clear();
    }
    getBoardInfo();

    lock_FEDinterfaceSetup->lock();
    std::cout << "------------begin output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    std::cout << "PLLs locked?:"
              << " 400 MHz: " << regManager->ReadReg("hephy_firmware_version.lockedPLL400")
              << " 200 MHz: " << regManager->ReadReg("hephy_firmware_version.lockedPLL200")
              << " 200 MHz idelay: " << regManager->ReadReg("hephy_firmware_version.lockedPLL200idelay")
              << " TTCready: " << regManager->ReadReg("hephy_firmware_version.TTCready") << std::endl;
    std::cout << "------------end output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    lock_FEDinterfaceSetup->unlock();

    // Find Phases
    if ((pixelFEDCard.cntrl_utca & 0xffffffffffffULL) != 0xffffffffffffULL && pixelFEDCard.data_type != 3) { //skip if no chans or emulated data
        std::vector<decoded_phases> phases = autoPhases();
        //std::vector<decoded_phases> phases = manualPhases();
        lock_FEDinterfaceSetup->lock();
        std::cout << "------------begin output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
        decoded_phases::print_header(std::cout);
        std::vector<int> fibers_ok;
        for (size_t i = 0; i < phases.size(); ++i)
            if (fibers_in_use[i + 1])
                std::cout << phases[i] << "\n";
        std::cout << "------------end output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
        lock_FEDinterfaceSetup->unlock();
    }

#ifdef DO_L1
    // Setup the special data format for L1 modules
    lock_FEDinterfaceSetup->lock();
    std::cout << "------------begin output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    cout << " PixelPh1FEDInterface - setup L1 modules " << endl;
    std::vector<uint32_t> idel_individual_ctrl_default = pixelFEDCard.setupModules();
    std::cout << "------------end output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    lock_FEDinterfaceSetup->unlock();
    regManager->WriteBlockReg("fe_ctrl_regs.idel_individual_ctrl", idel_individual_ctrl_default);

    usleep(sleep1000); // is this needed?
#endif                 // DO_L1

    // drain fifo1s
    for (int fib = 0; fib < 24; ++fib) {
        if (!fibers_in_use[fib + 1])
            continue;
        regManager->WriteReg("fe_ctrl_regs.fifo_1_to_read", fib);
        digfifo1 f = readFIFO1(false);
        if (f.nonzerowords(0) || f.nonzerowords(1))
            std::cout << "fifo1 fiber " << fib << " had non-zero words A: " << f.nonzerowords(0) << " B: " << f.nonzerowords(1) << std::endl;
    }
    // reset front end fifos
    regManager->WriteReg("fe_ctrl_regs.decode_reg_reset", 1);

    regManager->WriteReg("pixfed_ctrl_regs.PC_CONFIG_OK", 1);
    usleep(200000);
    int cDDR3calibrated = regManager->ReadReg("pixfed_stat_regs.ddr3_init_calib_done") & 1;

    lock_FEDinterfaceSetup->lock();
    std::cout << "------------begin output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    PrintSlinkStatus();
    std::cout << "------------end output-FED#" << pixelFEDCard.fedNumber << "----------------" << std::endl;
    lock_FEDinterfaceSetup->unlock();

    //phaseStabilityTest();

    std::cout << "--- End FED#" << pixelFEDCard.fedNumber << " setup ---" << std::endl;

    return cDDR3calibrated;
}

void PixelPh1FEDInterface::setChannelOfInterest(int ch) {
    if (ch < 0 || ch > 23) {
        std::cout << "setChannelOfInterest with ch " << ch << " not allowed" << std::endl;
        assert(0);
    }

    regManager->WriteReg("fe_ctrl_regs.fifo_config.channel_of_interest", ch);
    regManager->WriteReg("fe_ctrl_regs.fifo_1_to_read", ch); // do we always want to keep them in tandem?
}

void PixelPh1FEDInterface::setFIFO1(int ch) {
    if (ch < 0 || ch > 23) {
        std::cout << "set FIFO1 with ch " << ch << " not allowed" << std::endl;
        assert(0);
    }

    regManager->WriteReg("fe_ctrl_regs.fifo_1_to_read", ch);
}
void PixelPh1FEDInterface::setPixelForScore(int dc, int pxl) {
    regManager->WriteReg("fe_ctrl_regs.scan_DC", dc);
    regManager->WriteReg("fe_ctrl_regs.scan_pxl", pxl);
}

void PixelPh1FEDInterface::getBoardInfo() {
    std::cout << "Board info for FED#" << pixelFEDCard.fedNumber
              << " Type: " << regManager->ReadRegAsString("board_id")
              << "Board Use: " << regManager->ReadRegAsString("pixfed_stat_regs.user_ascii_code_01to04")
              << regManager->ReadRegAsString("pixfed_stat_regs.user_ascii_code_05to08")
              << "  MAC Address: " << std::hex << std::setfill('0')
              << std::setw(2) << regManager->ReadReg("mac_b5") << ":"
              << std::setw(2) << regManager->ReadReg("mac_b4") << ":"
              << std::setw(2) << regManager->ReadReg("mac_b3") << ":"
              << std::setw(2) << regManager->ReadReg("mac_b2") << ":"
              << std::setw(2) << regManager->ReadReg("mac_b1") << ":"
              << std::setw(2) << regManager->ReadReg("mac_b0") << std::setfill(' ') << std::dec
              << "  FW versions IPHC : "
              << regManager->ReadReg("pixfed_stat_regs.user_iphc_fw_id.fw_ver_nb") << "." << regManager->ReadReg("pixfed_stat_regs.user_iphc_fw_id.archi_ver_nb")
              << "; Date: "
              << regManager->ReadReg("pixfed_stat_regs.user_iphc_fw_id.fw_ver_day") << "."
              << regManager->ReadReg("pixfed_stat_regs.user_iphc_fw_id.fw_ver_month") << "."
              << regManager->ReadReg("pixfed_stat_regs.user_iphc_fw_id.fw_ver_year")
              << "   HEPHY : "
              << regManager->ReadReg("pixfed_stat_regs.user_hephy_fw_id.fw_ver_nb") << "."
              << regManager->ReadReg("pixfed_stat_regs.user_hephy_fw_id.archi_ver_nb")
              << "; Date: "
              << regManager->ReadReg("pixfed_stat_regs.user_hephy_fw_id.fw_ver_day") << "."
              << regManager->ReadReg("pixfed_stat_regs.user_hephy_fw_id.fw_ver_month") << "."
              << regManager->ReadReg("pixfed_stat_regs.user_hephy_fw_id.fw_ver_year")
              << "  FMCs present: L8: " << regManager->ReadReg("status.fmc_l8_present")
              << " L12: " << regManager->ReadReg("status.fmc_l12_present") << std::endl;
}

void PixelPh1FEDInterface::disableFMCs() {
    std::vector<std::pair<std::string, uint32_t> > cVecReg;
    cVecReg.push_back(std::make_pair("fmc_pg_c2m", 0));
    cVecReg.push_back(std::make_pair("fmc_l12_pwr_en", 0));
    cVecReg.push_back(std::make_pair("fmc_l8_pwr_en", 0));
    regManager->WriteStackReg(cVecReg);
}

void PixelPh1FEDInterface::enableFMCs() {
    std::vector<std::pair<std::string, uint32_t> > cVecReg;
    cVecReg.push_back(std::make_pair("fmc_pg_c2m", 1));
    cVecReg.push_back(std::make_pair("fmc_l12_pwr_en", 1));
    cVecReg.push_back(std::make_pair("fmc_l8_pwr_en", 1));
    regManager->WriteStackReg(cVecReg);
}

PixelPh1FEDInterface::FitelRegItem &PixelPh1FEDInterface::GetFitelRegItem(const std::string &node, int cFMCId, int cFitelId) {
    FitelRegMap::iterator i = fRegMap[FitelMapNum(cFMCId, cFitelId)].find(node);
    assert(i != fRegMap[FitelMapNum(cFMCId, cFitelId)].end());
    return i->second;
}

void PixelPh1FEDInterface::LoadFitelRegMap(int cFMCId, int cFitelId) {
    const std::string &filename = fRegMapFilename[FitelMapNum(cFMCId, cFitelId)];
    std::ifstream file(filename.c_str(), std::ios::in);
    if (!file) {
        std::cerr << "The Fitel Settings File " << filename << " could not be opened!" << std::endl;
        assert(0);
    }

    std::string line, fName, fAddress_str, fDefValue_str, fValue_str, fPermission_str;
    FitelRegItem fRegItem;

    while (getline(file, line)) {
        if (line.find_first_not_of(" \t") == std::string::npos)
            continue;
        if (line[0] == '#' || line[0] == '*')
            continue;
        std::istringstream input(line);
        input >> fName >> fAddress_str >> fDefValue_str >> fValue_str >> fPermission_str;

        fRegItem.fAddress = strtoul(fAddress_str.c_str(), 0, 16);
        fRegItem.fDefValue = strtoul(fDefValue_str.c_str(), 0, 16);
        fRegItem.fValue = strtoul(fValue_str.c_str(), 0, 16);
        fRegItem.fPermission = fPermission_str[0];

        //std::cout << "LoadFitelRegMap(" << cFMCId << "," << cFitelId << "): " << fName << " Addr: 0x" << std::hex << +fRegItem.fAddress << " DefVal: 0x" << +fRegItem.fDefValue << " Val: 0x" << +fRegItem.fValue << std::dec << std::endl;
        fRegMap[FitelMapNum(cFMCId, cFitelId)][fName] = fRegItem;
    }

    file.close();
}

void PixelPh1FEDInterface::EncodeFitelReg(const FitelRegItem &pRegItem, uint8_t pFMCId, uint8_t pFitelId, std::vector<uint32_t> &pVecReq) {
    pVecReq.push_back(pFMCId << 24 | pFitelId << 20 | pRegItem.fAddress << 8 | pRegItem.fValue);
}

void PixelPh1FEDInterface::DecodeFitelReg(FitelRegItem &pRegItem, uint8_t pFMCId, uint8_t pFitelId, uint32_t pWord) {
    //uint8_t cFMCId = ( pWord & 0xff000000 ) >> 24;
    //cFitelId = (  pWord & 0x00f00000   ) >> 20;
    pRegItem.fAddress = (pWord & 0x0000ff00) >> 8;
    pRegItem.fValue = pWord & 0x000000ff;
}

void PixelPh1FEDInterface::i2cRelease(uint32_t pTries) {
    uint32_t cCounter = 0;
    // release
    regManager->WriteReg("pixfed_ctrl_regs.fitel_rx_i2c_req", 0);
    while (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") != 0) {
        if (cCounter > pTries) {
            std::cout << "Error, exceeded maximum number of tries for I2C release!" << std::endl;
            break;
        } else {
            usleep(100);
            cCounter++;
        }
    }
}

bool PixelPh1FEDInterface::polli2cAcknowledge(uint32_t pTries) {
    bool cSuccess = false;
    uint32_t cCounter = 0;
    // wait for command acknowledge
    while (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 0) {
        if (cCounter > pTries) {
            std::cout << "Error, polling for I2C command acknowledge timed out!" << std::endl;
            break;

        } else {
            usleep(100);
            cCounter++;
        }
    }

    // check the value of that register
    if (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 1) {
        cSuccess = true;
    } else if (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 3) {
        cSuccess = false;
    }
    return cSuccess;
}

bool PixelPh1FEDInterface::WriteFitelReg(const std::string &pRegNode, int cFMCId, int cFitelId,
                                         uint8_t pValue, bool pVerifLoop) {

    // warning: this does not change the fRegMap items!

    FitelRegItem cRegItem = GetFitelRegItem(pRegNode, cFMCId, cFitelId);
    std::vector<uint32_t> cVecWrite;
    std::vector<uint32_t> cVecRead;

    cRegItem.fValue = pValue;

    EncodeFitelReg(cRegItem, cFMCId, cFitelId, cVecWrite);

    WriteFitelBlockReg(cVecWrite);

#ifdef COUNT_FLAG
    fRegisterCount++;
    fTransactionCount++;
#endif

    if (pVerifLoop) {
        cRegItem.fValue = 0;

        EncodeFitelReg(cRegItem, cFMCId, cFitelId, cVecRead);

        ReadFitelBlockReg(cVecRead);

        bool cAllgood = false;

        if (cVecWrite != cVecRead) {
            int cIterationCounter = 1;

            while (!cAllgood) {
                if (cAllgood)
                    break;

                std::vector<uint32_t> cWrite_again;
                std::vector<uint32_t> cRead_again;

                FitelRegItem cReadItem;
                FitelRegItem cWriteItem;
                DecodeFitelReg(cWriteItem, cFMCId, cFitelId, cVecWrite.at(0));
                DecodeFitelReg(cReadItem, cFMCId, cFitelId, cVecRead.at(0));
                // pFitel->setReg( pRegNode, cReadItem.fValue );

                if (cIterationCounter == 5) {
                    std::cout << "ERROR !!!\nReadback Value still different after 5 iterations for Register : " << pRegNode << "\n" << std::hex << "Written Value : 0x" << +pValue << "\nReadback Value : 0x" << int(cRegItem.fValue) << std::dec << std::endl;
                    std::cout << "Register Adress : " << int(cRegItem.fAddress) << std::endl;
                    std::cout << "Fitel Id : " << +cFitelId << std::endl << std::endl;
                    std::cout << "Failed to write register in " << cIterationCounter << " trys! Giving up!" << std::endl;
                }

                EncodeFitelReg(cWriteItem, cFMCId, cFitelId, cWrite_again);
                cReadItem.fValue = 0;
                EncodeFitelReg(cReadItem, cFMCId, cFitelId, cRead_again);

                WriteFitelBlockReg(cWrite_again);
                ReadFitelBlockReg(cRead_again);

                if (cWrite_again != cRead_again) {
                    if (cIterationCounter == 5)
                        break;

                    cVecWrite.clear();
                    cVecWrite = cWrite_again;
                    cVecRead.clear();
                    cVecRead = cRead_again;
                    cIterationCounter++;
                } else {
                    std::cout << "Managed to write register correctly in " << cIterationCounter << " Iteration(s)!" << std::endl;
                    cAllgood = true;
                }
            }
        } else
            cAllgood = true;

        if (cAllgood)
            return true;
        else
            return false;
    } else
        return true;
}

bool PixelPh1FEDInterface::WriteFitelBlockReg(std::vector<uint32_t> &pVecReq) {
    regManager->WriteReg("pixfed_ctrl_regs.fitel_i2c_addr", 0x4d);
    bool cSuccess = false;
    // write the encoded registers in the tx fifo
    regManager->WriteBlockReg("fitel_config_fifo_tx", pVecReq);
    // sent an I2C write request
    regManager->WriteReg("pixfed_ctrl_regs.fitel_rx_i2c_req", 1);

    // wait for command acknowledge
    while (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 0)
        usleep(100);

    uint32_t cVal = regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack");
    //if (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 1)
    if (cVal == 1) {
        cSuccess = true;
    }
    //else if (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 3)
    else if (cVal == 3) {
        std::cout << "Error writing Registers!" << std::endl;
        cSuccess = false;
    }

    // release
    i2cRelease(10);
    return cSuccess;
}

bool PixelPh1FEDInterface::ReadFitelBlockReg(std::vector<uint32_t> &pVecReq) {
    regManager->WriteReg("pixfed_ctrl_regs.fitel_i2c_addr", 0x4d);
    bool cSuccess = false;
    //uint32_t cVecSize = pVecReq.size();

    if (DO_DEBUG)
        cout << " 20 " << endl;

    // write the encoded registers in the tx fifo
    regManager->WriteBlockReg("fitel_config_fifo_tx", pVecReq);
    // sent an I2C write request
    if (DO_DEBUG)
        cout << " 21 " << endl;

    regManager->WriteReg("pixfed_ctrl_regs.fitel_rx_i2c_req", 3);

    if (DO_DEBUG)
        cout << " 22 " << endl;

    // wait for command acknowledge
    while (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 0)
        usleep(100);

    if (DO_DEBUG)
        cout << " 23 " << endl;

    uint32_t cVal = regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack");
    //if (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 1)

    if (DO_DEBUG)
        cout << " 24 " << endl;

    if (cVal == 1) {
        cSuccess = true;
    }
    //else if (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 3)
    else if (cVal == 3) {
        cSuccess = false;
        std::cout << "Error reading registers!" << std::endl;
    }

    if (DO_DEBUG)
        cout << " 25 " << endl;

    // release
    i2cRelease(10);

    if (DO_DEBUG)
        cout << " 26 " << pVecReq.size() << endl;

    // clear the vector & read the data from the fifo, comenting this off makes POHbias stop working.
    pVecReq = regManager->ReadBlockRegValue("fitel_config_fifo_rx", pVecReq.size()); // comment out to make fed 1248 work

    if (DO_DEBUG)
        cout << " 27 " << endl;

    return cSuccess;
}

void PixelPh1FEDInterface::ConfigureFitel(int cFMCId, int cFitelId, bool pVerifLoop) {
    FitelRegMap &cFitelRegMap = fRegMap[FitelMapNum(cFMCId, cFitelId)];
    FitelRegMap::iterator cIt = cFitelRegMap.begin();

    while (cIt != cFitelRegMap.end()) {
        std::vector<uint32_t> cVecWrite;
        std::vector<uint32_t> cVecRead;

        uint32_t cCounter = 0;

        for (; cIt != cFitelRegMap.end(); cIt++) {
            if (DO_DEBUG)
                cout << " 1 " << endl;
            if (cIt->second.fPermission == 'w') {
                if (DO_DEBUG)
                    cout << " 2 " << endl;
                EncodeFitelReg(cIt->second, cFMCId, cFitelId, cVecWrite);

                if (pVerifLoop) {
                    if (DO_DEBUG)
                        cout << " 3 " << endl;
                    FitelRegItem cItem = cIt->second;
                    cItem.fValue = 0;

                    EncodeFitelReg(cItem, cFMCId, cFitelId, cVecRead);
                }
#ifdef COUNT_FLAG
                fRegisterCount++;
#endif
                cCounter++;
            }
        }

        if (DO_DEBUG)
            cout << " 4 " << endl;

        WriteFitelBlockReg(cVecWrite);

        if (DO_DEBUG)
            cout << " 5 " << cVecWrite.size() << endl;

        usleep(20000);

#ifdef COUNT_FLAG
        fTransactionCount++;
#endif
        if (pVerifLoop) {
            ReadFitelBlockReg(cVecRead);

            if (DO_DEBUG)
                cout << " 6 " << endl;
            // only if I have a mismatch will i decode word by word and compare
            if (cVecWrite != cVecRead) {
                if (DO_DEBUG)
                    cout << " 7 " << endl;
                bool cAllgood = false;
                int cIterationCounter = 1;
                while (!cAllgood) {
                    if (DO_DEBUG)
                        cout << " 8 " << endl;
                    if (cAllgood)
                        break;

                    std::vector<uint32_t> cWrite_again;
                    std::vector<uint32_t> cRead_again;

                    std::pair<std::vector<uint32_t>::iterator, std::vector<uint32_t>::iterator> cMismatchWord = std::mismatch(cVecWrite.begin(), cVecWrite.end(), cVecRead.begin());

                    while (cMismatchWord.first != cVecWrite.end()) {

                        if (DO_DEBUG)
                            cout << " 9 " << endl;

                        FitelRegItem cRegItemWrite;
                        DecodeFitelReg(cRegItemWrite, cFMCId, cFitelId, *cMismatchWord.first);
                        FitelRegItem cRegItemRead;
                        DecodeFitelReg(cRegItemRead, cFMCId, cFitelId, *cMismatchWord.second);

                        if (cIterationCounter == 5) {
                            std::cout << "\nERROR !!!\nReadback value not the same after 5 Iteration for Register @ Address: 0x" << std::hex << int(cRegItemWrite.fAddress) << "\n"
                                      << "Written Value : 0x" << int(cRegItemWrite.fValue) << "\nReadback Value : 0x" << int(cRegItemRead.fValue) << std::dec << std::endl;
                            std::cout << "Fitel Id : " << int(cFitelId) << std::endl << std::endl;
                            std::cout << "Failed to write register in " << cIterationCounter << " trys! Giving up!" << std::endl;
                            std::cout << "---<-FMC<-fi---------<-a-----<-v" << std::endl;
                            std::cout << static_cast<std::bitset<32> >(*cMismatchWord.first) << std::endl << static_cast<std::bitset<32> >(*cMismatchWord.second) << std::endl << std::endl;
                        }

                        if (DO_DEBUG)
                            cout << " 10 " << endl;

                        cMismatchWord = std::mismatch(++cMismatchWord.first, cVecWrite.end(), ++cMismatchWord.second);

                        EncodeFitelReg(cRegItemWrite, cFMCId, cFitelId, cWrite_again);
                        cRegItemRead.fValue = 0;
                        EncodeFitelReg(cRegItemRead, cFMCId, cFitelId, cRead_again);
                    }

                    WriteFitelBlockReg(cWrite_again);
                    ReadFitelBlockReg(cRead_again);

                    if (DO_DEBUG)
                        cout << " 11 " << endl;

                    if (cWrite_again != cRead_again) {

                        if (DO_DEBUG)
                            cout << " 12 " << endl;

                        if (cIterationCounter == 5) {
                            std::cout << "Failed to configure FITEL in " << cIterationCounter << " Iterations!" << std::endl;
                            break;
                        }
                        cVecWrite.clear();
                        cVecWrite = cWrite_again;
                        cVecRead.clear();
                        cVecRead = cRead_again;
                        cIterationCounter++;
                    } else {
                        if (DO_DEBUG)
                            std::cout << "Managed to write all registers correctly in " << cIterationCounter << " Iteration(s)!" << std::endl;
                        cAllgood = true;

                        if (DO_DEBUG)
                            cout << " 13 " << endl;
                    }
                }
            }
        }
    }
}

void PixelPh1FEDInterface::DumpFitelRegs(int fitel) {
    std::cout << "DumpFitelRegs for fitel " << fitel << ":\n";

    static const uint32_t tmpvec[] = {
        //  std::vector<uint32_t> regs = {
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x07,
        0x3f, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x4b, 0x4f,
        0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x5b, 0x5f,
        0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x6b,
        0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b,
        0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab,
        0xe0, 0xe1, 0xf1, 0xf2, 0xf3
    };
    std::vector<uint32_t> regs(tmpvec, tmpvec + sizeof(tmpvec) / sizeof(tmpvec[0]));

    for (size_t i = 0; i < regs.size(); ++i)
        regs[i] = (fitel << 20) | (regs[i] << 8);

    ReadFitelBlockReg(regs);

    for (size_t i = 0; i < regs.size(); ++i)
        printf("%02x %02x %02x %02x\n", (regs[i] & 0xff000000) >> 24, (regs[i] & 0xff0000) >> 16, (regs[i] & 0xff00) >> 8, (regs[i] & 0xff));
}

double PixelPh1FEDInterface::ReadRSSI(int fiber) {
    assert(fiber >= 1 && fiber <= 24);
    const int fmc = 0;
    const int fitel = fiber < 13;
    const int channel = fiber > 12 ? fiber - 12 : fiber;
    const std::string ch_name = fitelChannelName(channel);
    const uint8_t old_value = fRegMap[FitelMapNum(fmc, fitel)][ch_name].fValue;
    assert(old_value == 0x2 || old_value == 0x8);
    WriteFitelReg(ch_name, fmc, fitel, 0xc, true); // to be set back to what it was in the reg map!

    // the Fitel FMC needs to be set up to be able to read the RSSI on a given Channel:
    // I2C register 0x1: set to 0x4 for RSSI, set to 0x5 for Die Temperature of the Fitel
    // Channel Control Registers: set to 0x02 to disable RSSI for this channel, set to 0x0c to enable RSSI for this channel
    // the ADC always reads the sum of all the enabled channels!
    //initial FW setup
    regManager->WriteReg("pixfed_ctrl_regs.fitel_i2c_cmd_reset", 1);
    regManager->WriteReg("pixfed_ctrl_regs.fitel_i2c_cmd_reset", 0);
    regManager->WriteReg("pixfed_ctrl_regs.fitel_rx_i2c_req", 0);

    //first, write the correct registers to configure the ADC
    //the values are: Address 0x01 -> 0x1<<6 | 0x1f
    //                Address 0x02 -> 0x1

    std::vector<uint32_t> cVecWrite;
    std::vector<uint32_t> cVecRead;
    uint32_t cVal = 0;

    //encode them in a 32 bit word and write, no readback yet
    cVecWrite.push_back(fmc << 24 | fitel << 20 | 0x1 << 8 | 0x5f);
    cVecWrite.push_back(fmc << 24 | fitel << 20 | 0x2 << 8 | 0x01);
    regManager->WriteReg("pixfed_ctrl_regs.fitel_i2c_addr", 0x77);
    regManager->WriteBlockReg("fitel_config_fifo_tx", cVecWrite);
    regManager->WriteReg("pixfed_ctrl_regs.fitel_rx_i2c_req", 1);
    while ((cVal = regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack")) == 0)
        usleep(100);
    if (cVal == 3)
        std::cout << "Error reading registers!" << std::endl;
    i2cRelease(10);

    // poll the status register of the LTC2990 until it comes back ready, or 20 * 10 ms max
    // seems to come back in between 50-60 ms
    for (int i = 0; i < 20; ++i) {
        cVecRead.clear();
        cVecRead.push_back(fmc << 24 | fitel << 20 | 0x0 << 8 | 0);
        regManager->WriteReg("pixfed_ctrl_regs.fitel_i2c_addr", 0x4c);
        regManager->WriteBlockReg("fitel_config_fifo_tx", cVecRead);
        regManager->WriteReg("pixfed_ctrl_regs.fitel_rx_i2c_req", 3);
        while ((cVal = regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack")) == 0)
            usleep(100);
        if (cVal == 3)
            std::cout << "Error reading registers!" << std::endl;
        cVecRead = regManager->ReadBlockRegValue("fitel_config_fifo_rx", 1);
        if (cVecRead.size() != 1)
            std::cout << "status read data size wrong!\n";
        uint32_t st = cVecRead[0];
        cVecRead.clear();
        i2cRelease(10);
        //std::cout << " i = " << i << " polling status size " << cVecRead.size();
        //for (uint32_t a : cVecRead)
        //  std::cout << " got a  " << std::hex << a << std::dec << std::endl;
        if ((st & 0xff) == 0x7e) // bit 0 is a busy bit, we want the rest of the bits
            break;
        usleep(sleep10000);
    }

    //now prepare the read-back of the values
    uint8_t cNWord = 10;
    cVecRead.clear();
    for (uint8_t cIndex = 0; cIndex < cNWord; cIndex++)
        cVecRead.push_back(fmc << 24 | fitel << 20 | (0x6 + cIndex) << 8 | 0);

    regManager->WriteReg("pixfed_ctrl_regs.fitel_i2c_addr", 0x4c);
    regManager->WriteBlockReg("fitel_config_fifo_tx", cVecRead);
    regManager->WriteReg("pixfed_ctrl_regs.fitel_rx_i2c_req", 3);
    while ((cVal = regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack")) == 0)
        usleep(100);
    if (cVal == 3)
        std::cout << "Error reading registers!" << std::endl;
    i2cRelease(10);

    cVecRead = regManager->ReadBlockRegValue("fitel_config_fifo_rx", cNWord);

    // now convert to Voltages!
    std::vector<double> cLTCValues(cNWord / 2, 0);

    double cConstant = 0.00030518;
    // each value is hidden in 2 I2C words
    for (int cMeasurement = 0; cMeasurement < cNWord / 2; cMeasurement++) {
        // build the values
        uint16_t cValue = ((cVecRead.at(2 * cMeasurement) & 0x7F) << 8) + (cVecRead.at(2 * cMeasurement + 1) & 0xFF);
        uint8_t cSign = (cValue >> 14) & 0x1;

        //now the conversions are different for each of the voltages, so check by cMeasurement
        if (cMeasurement == 4)
            cLTCValues.at(cMeasurement) = (cSign == 0b1) ? (-(32768 - cValue) * cConstant + 2.5) : (cValue * cConstant + 2.5);
        else
            cLTCValues.at(cMeasurement) = (cSign == 0b1) ? (-(32768 - cValue) * cConstant) : (cValue * cConstant);
    }

    // now I have all 4 voltage values in a vector of size 5
    // V1 = cLTCValues[0]
    // V2 = cLTCValues[1]
    // V3 = cLTCValues[2]
    // V4 = cLTCValues[3]
    // Vcc = cLTCValues[4]
    //
    // the RSSI value = fabs(V3-V4) / R=150 Ohm [in uAmps]
    const double cADCVal = fabs(cLTCValues.at(2) - cLTCValues.at(3)) / 150.0;

    //std::cout << "raw " << cLTCValues[0] << " " << cLTCValues[1] << " " << cLTCValues[2] << " " << cLTCValues[3] << " " << cLTCValues[4] << "\n";
    //std::cout << "adcval " << cADCVal << std::endl;

    WriteFitelReg(ch_name, fmc, fitel, old_value, true);

    return cADCVal;
}

void PixelPh1FEDInterface::disableBE(bool disable) {
    regManager->WriteReg("fe_ctrl_regs.disable_BE", disable);
}

void PixelPh1FEDInterface::sendResets(unsigned which) {
    if (which == 3) {
        //readPhases(true, true);
        //setup();
        //    regManager->WriteReg("fe_ctrl_regs.decode_reset", 1);
        //    regManager->WriteReg("fe_ctrl_regs.decode_reg_reset", 1);
    }
    if (which == 1) {
        regManager->WriteReg("fe_ctrl_regs.decode_reset", 1);
        regManager->WriteReg("fe_ctrl_regs.decode_reg_reset", 1);
        regManager->WriteReg("fe_ctrl_regs.idel_ctrl_reset", 1);
        usleep(sleep1000);

        regManager->WriteReg("fe_ctrl_regs.initialize_swap", 0);
        regManager->WriteReg("fe_ctrl_regs.decode_reset", 0);
        regManager->WriteReg("fe_ctrl_regs.decode_reg_reset", 0);
        usleep(sleep1000);
    }
}

void PixelPh1FEDInterface::armOSDFifo(int channel, int rochi, int roclo) {
    if (rochi != roclo)
        std::cout << "warning: for uTCA FED the returned words are for the rocs on each tbm core, so only rochi = " << rochi << " will be used\n";
    //std::cout << "OSD Readback enabled for ROC " << rochi << std::endl;
    regManager->WriteReg("fe_ctrl_regs.fifo_config.OSD_ROC_Nr", rochi & 0x1f);
}

uint32_t PixelPh1FEDInterface::readOSDFifo(int channel) {
    assert(channel >= 0 && channel <= 23);
    char buf[32];
    snprintf(buf, 32, "idel_individual_stat.CH%i", channel);
    std::vector<uint32_t> cReadValues = regManager->ReadBlockRegValue(buf, 4); // JMTBAD any faster to just do single read at right addr?

    if (1) {
        const uint16_t cOSD_word_A = cReadValues[3] & 0xffff;
        const uint16_t cOSD_word_B = (cReadValues[3] >> 16) & 0xffff;

        std::cout << "TBM Core A: " << std::endl << "aaaarrrr87654321" << std::endl << std::bitset<16>(cOSD_word_A) << std::endl;
        std::cout << "TBM Core B: " << std::endl << "aaaarrrr87654321" << std::endl << std::bitset<16>(cOSD_word_B) << std::endl;
    }

    return cReadValues[3];
}

std::vector<uint32_t> PixelPh1FEDInterface::readOSDValues() {
    std::vector<uint32_t> v = regManager->ReadBlockRegValue("idel_individual_stat_block", 96);
    //cout<<" readoit OSD "<<hex<<endl;
    std::vector<uint32_t> v2(24);
    for (int i = 0; i < 24; ++i) {
        v2[i] = v[i * 4 + 3];
        //cout<<v[i*4+3];
    }
    //cout<<dec<<endl;

    return v2;
}

PixelPh1FEDInterface::decoded_phases::decoded_phases(int fiber_, uint32_t a0, uint32_t a1, uint32_t a2) {
    fiber = fiber_;

    idelay_ctrl_ready = (a0 >> 10) & 1;
    idelay_tap_set = (a0 >> 5) & 0x1f;
    idelay_tap_read = a0 & 0x1f;

    idelay_tap_scan = a1;

    sampling_clock_swapped = a2 >> 31;
    init_swap_finished = (a2 >> 30) & 1;
    init_init_finished = (a2 >> 29) & 1;

    first_zeros_lo = (a2 >> 8) & 0x1f;
    first_zeros_hi = (a2 >> 13) & 0x1f;
    second_zeros_lo = (a2 >> 18) & 0x1f;
    second_zeros_hi = (a2 >> 23) & 0x1f;
    num_windows = a2 >> 5 & 0x7;

    delay_tap_used = a2 & 0x1f;
}

void PixelPh1FEDInterface::decoded_phases::print_header(std::ostream &o) {
    o << "Fiber RDY SET  RD    pattern:                          S H1 L1 H0 L0  W  R"
      << "\n";
}

std::ostream &operator<<(std::ostream &o, const PixelPh1FEDInterface::decoded_phases &p) {
    o << std::setfill(' ')
      << std::setw(5) << p.fiber << " "
      << std::setw(3) << p.idelay_ctrl_ready << " "
      << std::setw(3) << p.idelay_tap_set << " "
      << std::setw(3) << p.idelay_tap_read << "    "
      << std::bitset<32>(p.idelay_tap_scan) << " "
      << std::setw(2) << p.sampling_clock_swapped << " "
      << std::setw(2) << p.second_zeros_hi << " "
      << std::setw(2) << p.second_zeros_lo << " "
      << std::setw(2) << p.first_zeros_hi << " "
      << std::setw(2) << p.first_zeros_lo << " "
      << std::setw(2) << p.num_windows << " "
      << std::setw(2) << p.delay_tap_used;
    return o;
}

void PixelPh1FEDInterface::setCalibNEvents(int n) {
    printf("setCalibNEvents %i\n", n);
    assert(n >= 1);
    pixelFEDCard.calib_mode_num_events = n; // JMTBAD should we instead keep the fedcard as-is and keep track of this separately since calibs can change it? meh
    regManager->WriteReg("pixfed_ctrl_regs.PC_CONFIG_OK", 0);
    usleep(sleep10000);
    regManager->WriteReg("pixfed_ctrl_regs.acq_ctrl.calib_mode_NEvents", n - 1);
    usleep(sleep10000);
    regManager->WriteReg("pixfed_ctrl_regs.PC_CONFIG_OK", 1);
    usleep(sleep10000);
}

void PixelPh1FEDInterface::setAutoPhases(bool enable) {
    regManager->WriteReg("fe_ctrl_regs.phase_finding.disable", !enable);
}

void PixelPh1FEDInterface::findPhasesNow() {
    uhal::HwInterface &hw = regManager->getHwInterface();
    hw.getNode("fe_ctrl_regs.phase_finding.execute").write(1);
    hw.getNode("fe_ctrl_regs.phase_finding.execute").write(0);
    hw.dispatch();
    usleep(500000);

    const bool printem = false;
    if (printem) {
        std::vector<decoded_phases> phases = readPhases();
        std::cout << "FED#" << pixelFEDCard.fedNumber << "\n";
        decoded_phases::print_header(std::cout);
        for (size_t i = 0; i < phases.size(); ++i)
            if (fibers_in_use[i + 1])
                std::cout << phases[i] << "\n";
    }
}

std::vector<PixelPh1FEDInterface::decoded_phases> PixelPh1FEDInterface::autoPhases() {
    std::vector<std::pair<std::string, uint32_t> > cVecReg;
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.decode_reset", 1));
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.decode_reg_reset", 1));
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.idel_ctrl_reset", 1));
    regManager->WriteStackReg(cVecReg);
    cVecReg.clear();
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.idel_ctrl_reset", 0));
    regManager->WriteStackReg(cVecReg);
    cVecReg.clear();

    std::vector<uint32_t> cValVec;

    // set the parameters for IDELAY scan
    cValVec.assign(24, 0x80000000);
    regManager->WriteBlockReg("fe_ctrl_regs.idel_individual_ctrl", cValVec);

    // set auto_delay_scan and set idel_RST
    cValVec.assign(24, 0xc0000000);
    regManager->WriteBlockReg("fe_ctrl_regs.idel_individual_ctrl", cValVec);

    // set auto_delay_scan and remove idel_RST
    cValVec.assign(24, 0x80000000);
    regManager->WriteBlockReg("fe_ctrl_regs.idel_individual_ctrl", cValVec);

    // initialize Phase Finding
    regManager->WriteReg("fe_ctrl_regs.initialize_swap", 0);
    regManager->WriteReg("fe_ctrl_regs.initialize_swap", 1);
    regManager->WriteReg("fe_ctrl_regs.initialize_swap", 0);

    // JMTBAD need to respect printlevel...
    std::cout << "FED# " << pixelFEDCard.fedNumber << " Init phase finding... " << std::flush;

    PixelTimer timer;
    timer.start();
    while (((regManager->ReadBlockRegValue("idel_individual_stat.CH0", 4).at(2) >> 29) & 0x03) != 0x0)
        usleep(sleep1000);
    timer.stop();

    std::cout << " time: " << timer.tottime() << "; swapping phases... " << std::flush;
    timer.reset();
    timer.start();
    while (((regManager->ReadBlockRegValue("idel_individual_stat.CH0", 4).at(2) >> 29) & 0x03) != 0x2)
        usleep(sleep1000);
    timer.stop();
    std::cout << " add'l time: " << timer.tottime() << "; results: " << std::endl;

    return readPhases();
}

std::vector<PixelPh1FEDInterface::decoded_phases> PixelPh1FEDInterface::manualPhases() {
    std::vector<std::pair<std::string, uint32_t> > cVecReg;
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.decode_reset", 1));
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.decode_reg_reset", 1));
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.idel_ctrl_reset", 1));
    regManager->WriteStackReg(cVecReg);
    cVecReg.clear();
    cVecReg.push_back(std::make_pair("fe_ctrl_regs.idel_ctrl_reset", 0));
    regManager->WriteStackReg(cVecReg);
    cVecReg.clear();

    std::vector<uint32_t> cValVec;

    cValVec.assign(24, 0x40000000 | (15 << 5) | 15);
    regManager->WriteBlockReg("fe_ctrl_regs.idel_individual_ctrl", cValVec);
    cValVec.assign(24, 0x00000000);
    regManager->WriteBlockReg("fe_ctrl_regs.idel_individual_ctrl", cValVec);

    std::vector<PixelPh1FEDInterface::decoded_phases> phases = readPhases();
    return phases;
}

std::vector<PixelPh1FEDInterface::decoded_phases> PixelPh1FEDInterface::readPhases() {
    const uint32_t cNChannel = 24;
    std::vector<uint32_t> cReadValues = regManager->ReadBlockRegValue("idel_individual_stat_block", cNChannel * 4);
    std::vector<decoded_phases> ret;
    for (uint32_t i = 0; i < cNChannel; ++i)
        ret.push_back(decoded_phases(i + 1, cReadValues[i * 4], cReadValues[i * 4 + 1], cReadValues[i * 4 + 2]));
    return ret;
}

void PixelPh1FEDInterface::phaseStabilityTest() {
    std::map<int, std::vector<decoded_phases> > samples;
    const int nsamples = 20;
    for (int isample = 0; isample < nsamples; ++isample) {
        std::vector<decoded_phases> phases = readPhases();
        const size_t nphases = phases.size();
        std::cout << "sample #" << isample << std::endl;
        decoded_phases::print_header(std::cout);
        for (size_t iphase = 0; iphase < nphases; ++iphase) {
            const decoded_phases &p = phases[iphase];
            assert(p.fiber == int(iphase + 1));
            if (!fibers_in_use[p.fiber])
                continue;

            std::cout << p;

            samples[p.fiber].push_back(p);
        }
    }

    for (int fiber = 1; fiber <= 24; ++fiber) {
        if (!fibers_in_use[fiber])
            continue;
        const std::vector<decoded_phases> &phases = samples[fiber];
        typedef std::map<std::string, std::map<unsigned, int> > histos_map;
        histos_map histos;
        for (size_t i = 0; i < phases.size(); ++i) {
            ++histos["idelay_ctrl_ready"][phases[i].idelay_ctrl_ready];
            ++histos["idelay_tap_set"][phases[i].idelay_tap_set];
            ++histos["idelay_tap_read"][phases[i].idelay_tap_read];
            ++histos["sampling_clock_swapped"][phases[i].sampling_clock_swapped];
            ++histos["init_swap_finished"][phases[i].init_swap_finished];
            ++histos["init_init_finished"][phases[i].init_init_finished];
            ++histos["init_init_finished"][phases[i].init_init_finished];
            ++histos["first_zeros_lo"][phases[i].first_zeros_lo];
            ++histos["first_zeros_hi"][phases[i].first_zeros_hi];
            ++histos["second_zeros_lo"][phases[i].second_zeros_lo];
            ++histos["second_zeros_hi"][phases[i].second_zeros_hi];
            ++histos["num_windows"][phases[i].num_windows];
            ++histos["delay_tap_used"][phases[i].delay_tap_used];
        }

        std::cout << "stats for fiber " << fiber << ":\n";
        for (histos_map::const_iterator it = histos.begin(), ite = histos.end(); it != ite; ++it) {
            double mean = 0;
            for (std::map<unsigned, int>::const_iterator it2 = it->second.begin(), it2e = it->second.end(); it2 != it2e; ++it2)
                mean += it2->first * it2->second;
            mean /= nsamples;
            double rms = 0;
            for (std::map<unsigned, int>::const_iterator it2 = it->second.begin(), it2e = it->second.end(); it2 != it2e; ++it2)
                rms += it2->second * pow(it2->first - mean, 2);
            rms /= (nsamples - 1);
            rms = sqrt(rms);

            printf("%-25s (m %4.1f r %4.1f) :", it->first.c_str(), mean, rms);

            for (std::map<unsigned, int>::const_iterator it2 = it->second.begin(), it2e = it->second.end(); it2 != it2e; ++it2)
                std::cout << " " << it2->first << "(" << it2->second << ")";
            std::cout << std::endl;
        }
    }
}

uint8_t PixelPh1FEDInterface::getTTSState() {
    return uint8_t(regManager->ReadReg("pixfed_stat_regs.tts.word") & 0x0000000F);
}

uint64_t PixelPh1FEDInterface::getTimeInTTSBusy() { //read internal counters for 48 bit counter
    uint32_t low = regManager->ReadReg("pixfed_stat_regs.tts.elapsed_time_BUSY_15to0");
    uint32_t hi = regManager->ReadReg("pixfed_stat_regs.tts.elapsed_time_BUSY_47to16");
    uint64_t cnt = ((uint64_t)hi << 16) | low;
    return cnt;
}

uint64_t PixelPh1FEDInterface::getTimeInTTSReady() {
    uint32_t low = regManager->ReadReg("pixfed_stat_regs.tts.elapsed_time_RDY_15to0");
    uint32_t hi = regManager->ReadReg("pixfed_stat_regs.tts.elapsed_time_RDY_47to16");
    uint64_t cnt = ((uint64_t)hi << 16) | low;
    return cnt;
}

uint64_t PixelPh1FEDInterface::getTimeInTTSOOS() {
    uint32_t low = regManager->ReadReg("pixfed_stat_regs.tts.elapsed_time_OOS_15to0");
    uint32_t hi = regManager->ReadReg("pixfed_stat_regs.tts.elapsed_time_OOS_47to16");
    uint64_t cnt = ((uint64_t)hi << 16) | low;
    return cnt;
}

uint64_t PixelPh1FEDInterface::getTimeInTTSWarn() {
    uint32_t low = regManager->ReadReg("pixfed_stat_regs.tts.elapsed_time_WARN_15to0");
    uint32_t hi = regManager->ReadReg("pixfed_stat_regs.tts.elapsed_time_WARN_47to16");
    uint64_t cnt = ((uint64_t)hi << 16) | low;
    return cnt;
}

uint32_t PixelPh1FEDInterface::getTransitionsToTTSReady() {
    return regManager->ReadReg("pixfed_stat_regs.tts.transitions_to_RDY");
}

uint32_t PixelPh1FEDInterface::getTransitionsToTTSBusy() {
    return regManager->ReadReg("pixfed_stat_regs.tts.transitions_to_BUSY");
}

uint32_t PixelPh1FEDInterface::getTransitionsToTTSOOS() {
    return regManager->ReadReg("pixfed_stat_regs.tts.transitions_to_OOS");
}

uint32_t PixelPh1FEDInterface::getTransitionsToTTSWarn() {
    return regManager->ReadReg("pixfed_stat_regs.tts.transitions_to_WARN");
}

void PixelPh1FEDInterface::printTTSState() {
    const uint8_t cWord = getTTSState();
    std::cout << "FED#" << pixelFEDCard.fedNumber << " TTS State: ";
    if (cWord == 8)
        std::cout << "RDY";
    else if (cWord == 4)
        std::cout << "BSY";
    else if (cWord == 2)
        std::cout << "OOS";
    else if (cWord == 1)
        std::cout << "OVF";
    else if (cWord == 0 || cWord == 16)
        std::cout << "DSC";
    else if (cWord == 12)
        std::cout << "ERR";
    std::cout << std::endl;
    std::cout << "TTS_FSM_stage=" << regManager->ReadReg("pixfed_stat_regs.TTS_FSM_STAGE") << std::endl;
    const uint64_t tRDY = getTimeInTTSReady();
    const uint64_t tBSY = getTimeInTTSBusy();
    const uint64_t tOOS = getTimeInTTSOOS();
    const uint64_t tWARN = getTimeInTTSWarn();
    const uint32_t cRDY = getTransitionsToTTSReady();
    const uint32_t cBSY = getTransitionsToTTSBusy();
    const uint32_t cOOS = getTransitionsToTTSOOS();
    const uint32_t cWARN = getTransitionsToTTSWarn();
    std::cout << "times in         "
              << setw(16) << tRDY << " RDY | "
              << setw(16) << tBSY << " BSY | "
              << setw(16) << tOOS << " OOS | "
              << setw(16) << tWARN << " OVF |" << std::endl;
    std::cout << "Transitions into "
              << setw(16) << cRDY << " RDY | "
              << setw(16) << cBSY << " BSY | "
              << setw(16) << cOOS << " OOS | "
              << setw(16) << cWARN << " OVF |" << std::endl;
}

void PixelPh1FEDInterface::getSFPStatus(uint8_t pFMCId) {
    std::cout << "Initializing SFP+ for SLINK" << std::endl;

    regManager->WriteReg("pixfed_ctrl_regs.fitel_i2c_cmd_reset", 1);
    sleep(0.5);
    regManager->WriteReg("pixfed_ctrl_regs.fitel_i2c_cmd_reset", 0);

    std::vector<std::pair<std::string, uint32_t> > cVecReg;
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.fitel_sfp_i2c_req", 0));
    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.fitel_rx_i2c_req", 0));

    cVecReg.push_back(std::make_pair("pixfed_ctrl_regs.fitel_i2c_addr", 0x38));

    regManager->WriteStackReg(cVecReg);

    // Vectors for write and read data!
    std::vector<uint32_t> cVecWrite;
    std::vector<uint32_t> cVecRead;

    //encode them in a 32 bit word and write, no readback yet
    cVecWrite.push_back(pFMCId << 24 | 0x00);
    regManager->WriteBlockReg("fitel_config_fifo_tx", cVecWrite);

    // sent an I2C write request
    // Edit G. Auzinger
    // write 1 to sfp_i2c_reg to trigger an I2C write transaction - Laurent's BS python script is ambiguous....
    regManager->WriteReg("pixfed_ctrl_regs.fitel_sfp_i2c_req", 1);

    // wait for command acknowledge
    while (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 0)
        usleep(100);

    uint32_t cVal = regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack");

    if (cVal == 3)
        std::cout << "Error during i2c write!" << cVal << std::endl;

    //release handshake with I2C
    regManager->WriteReg("pixfed_ctrl_regs.fitel_sfp_i2c_req", 0);

    // wait for command acknowledge
    while (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") != 0)
        usleep(100);

    usleep(500);
    //////////////////////////////////////////////////
    ////THIS SHOULD BE THE END OF THE WRITE OPERATION, BUS SHOULD BE IDLE
    /////////////////////////////////////////////////

    //regManager->WriteReg ("pixfed_ctrl_regs.fitel_i2c_cmd_reset", 1);
    //sleep (0.5);
    //regManager->WriteReg ("pixfed_ctrl_regs.fitel_i2c_cmd_reset", 0);

    //std::vector<std::pair<std::string, uint32_t> > cVecReg;
    //cVecReg.push_back ({"pixfed_ctrl_regs.fitel_sfp_i2c_req", 0});
    //cVecReg.push_back ({"pixfed_ctrl_regs.fitel_rx_i2c_req", 0});
    //cVecReg.push_back ({"pixfed_ctrl_regs.fitel_i2c_addr", 0x38});
    //regManager->WriteStackReg (cVecReg);
    //cVecReg.clear();

    //Vector for the reply
    //std::vector<uint32_t> cVecRead;
    cVecRead.push_back(pFMCId << 24 | 0x00);
    regManager->WriteBlockReg("fitel_config_fifo_tx", cVecRead);
    //this issues an I2C read request via FW
    regManager->WriteReg("pixfed_ctrl_regs.fitel_sfp_i2c_req", 3);

    // wait for command acknowledge
    while (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") == 0)
        usleep(100);

    cVal = regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack");

    if (cVal == 3)
        std::cout << "Error during i2c write!" << cVal << std::endl;

    cVecRead.clear();
    //only read 1 word
    cVecRead = regManager->ReadBlockRegValue("fitel_config_fifo_rx", cVecWrite.size());

    std::cout << "SFP+ Status of FMC " << +pFMCId << std::endl;

    for (size_t i = 0; i < cVecRead.size(); ++i) {
        uint32_t cRead = cVecRead[i];
        cRead = cRead & 0xF;
        std::cout << "-> SFP_MOD_ABS:  " << (cRead >> 0 & 0x1) << std::endl;
        std::cout << "-> SFP_TX_DIS:   " << (cRead >> 1 & 0x1) << std::endl;
        std::cout << "-> SFP_TX_FAULT: " << (cRead >> 2 & 0x1) << std::endl;
        std::cout << "-> SFP_RX_LOS:   " << (cRead >> 3 & 0x1) << std::endl;
    }

    //release handshake with I2C
    regManager->WriteReg("pixfed_ctrl_regs.fitel_sfp_i2c_req", 0);

    // wait for command acknowledge
    while (regManager->ReadReg("pixfed_stat_regs.fitel_i2c_ack") != 0)
        usleep(100);

    printTTSState();
}

void PixelPh1FEDInterface::PrintSlinkStatus() {

    printTTSState();
    getSFPStatus(0);
    //check the link status
    uint8_t sync_loss = regManager->ReadReg("pixfed_stat_regs.slink_core_status.sync_loss");
    uint8_t link_down = regManager->ReadReg("pixfed_stat_regs.slink_core_status.link_down");
    uint8_t link_full = regManager->ReadReg("pixfed_stat_regs.slink_core_status.link_full");
    std::cout << "PixFED SLINK information : ";
    (sync_loss == 1) ? std::cout << "SERDES out of sync" : std::cout << "SERDES OK";
    std::cout << " | ";
    (link_down == 1) ? std::cout << "Link up" : std::cout << "Link down";
    std::cout << " | ";
    (link_full == 1) ? std::cout << "ready for data" : std::cout << "max 16 words left";
    std::cout << std::endl;

    //LINK status
    std::cout << "SLINK core status        : ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0x1);
    //    std::cout << "data_31to0 " <<  regManager->ReadReg("pixfed_stat_regs.slink_core_status.data_31to0") << std::endl;
    uint32_t cLinkStatus = regManager->ReadReg("pixfed_stat_regs.slink_core_status.data_31to0");
    ((cLinkStatus & 0x80000000) >> 31) ? std::cout << "Test mode |" : std::cout << "FED data  |";
    ((cLinkStatus & 0x40000000) >> 30) ? std::cout << " Link up |" : std::cout << " Link down |";
    ((cLinkStatus & 0x20000000) >> 29) ? std::cout << "\0 |" : std::cout << " Link backpressured |";
    ((cLinkStatus & 0x10000000) >> 28) ? std::cout << " >= 1 block free for data |" : std::cout << "\0 |";
    ((cLinkStatus & 0x00000040) >> 6) ? std::cout << " 2 fragments with the same trigger number |" : std::cout << "\0 |";
    ((cLinkStatus & 0x00000020) >> 5) ? std::cout << " Trailer duplicated |" : std::cout << "\0 |";
    ((cLinkStatus & 0x00000010) >> 4) ? std::cout << " Header duplicated |" : std::cout << "\0 |";
    ((cLinkStatus & 0x00000008) >> 3) ? std::cout << "   Between header and trailer |" << std::endl : std::cout << "\0 |";

    if ((cLinkStatus & 0x00000007) == 1)
        std::cout << "   State machine: idle" << std::endl;
    else if ((cLinkStatus & 0x00000007) == 2)
        std::cout << "   State machine: Read data from FED" << std::endl;
    else if ((cLinkStatus & 0x00000007) == 4)
        std::cout << "   State machine: Closing block" << std::endl;
    else
        std::cout << "   State machine: messed up (Return value: " << (cLinkStatus & 0x00000007) << ")" << std::endl;

    //Status CORE
    std::cout << "Status CORE: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0x6);
    cLinkStatus = regManager->ReadReg("pixfed_stat_regs.slink_core_status.data_31to0");
    ((cLinkStatus & 0x80000000) >> 31) ? std::cout << "   Core initialized" << std::endl : std::cout << "   Core not initialized" << std::endl;

    //Data counter
    std::cout << "Input Counters : ";
    std::cout << "Data counter: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0x2);
    uint64_t val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    std::cout << val << " | ";

    //Event counter
    std::cout << "Event counter: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0x3);
    val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    std::cout << val << " | ";

    //Block counter
    std::cout << "Block counter: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0x4);
    val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    std::cout << val << " | ";

    //Packets recieved counter
    std::cout << "Pckt rcv counter: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0x5);
    val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    std::cout << val << std::endl;

    //Packets send counter
    std::cout << "Output Counter : ";
    std::cout << "Pckt send counter: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0x7);
    val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    std::cout << val << " | ";

    //Status build
    std::cout << "Status build: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0x8);
    val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    std::cout << val << " | ";

    //Back pressure counter
    std::cout << "BackP counter: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0x9);
    val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    std::cout << val << std::endl;

    //Version number
    //std::cout << "Version number: ";
    //regManager->WriteReg ("pixfed_ctrl_regs.slink_core_status_addr", 0xa);
    //val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    //std::cout <<  val << std::endl;

    //SERDES status
    //std::cout << "SERDES status: ";
    //regManager->WriteReg ("pixfed_ctrl_regs.slink_core_status_addr", 0xb);
    //val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    //std::cout <<  val << std::endl;

    //Retransmitted packages counter
    std::cout << "Error Counters : ";
    std::cout << "Retrans pkg counter: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0xc);
    val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    std::cout << val << " | ";

    //FED CRC error
    std::cout << "FED CRC error counter: ";
    regManager->WriteReg("pixfed_ctrl_regs.slink_core_status_addr", 0xd);
    val = regManager->ReadRegsAs64("pixfed_stat_regs.slink_core_status.data_63to32", "pixfed_stat_regs.slink_core_status.data_31to0");
    std::cout << val << std::endl;

    std::cout << "Data transfer block status: ";
    std::cout << "      Block1: ";
    ((cLinkStatus & 0x00000080) >> 7) ? std::cout << "ready    " : std::cout << "not ready";
    std::cout << " | Block2: ";
    ((cLinkStatus & 0x00000040) >> 6) ? std::cout << "ready    " : std::cout << "not ready";
    std::cout << " | Block3: ";
    ((cLinkStatus & 0x00000020) >> 5) ? std::cout << "ready    " : std::cout << "not ready";
    std::cout << " | Block4: ";
    ((cLinkStatus & 0x00000010) >> 4) ? std::cout << "ready" : std::cout << "not ready" << std::endl;

    std::cout << "Data transfer block usage: ";
    std::cout << "      Block1: ";
    ((cLinkStatus & 0x00000008) >> 3) ? std::cout << "used    " : std::cout << "not used ";
    std::cout << " | Block2: ";
    ((cLinkStatus & 0x00000004) >> 2) ? std::cout << "used    " : std::cout << "not used ";
    std::cout << " | Block3: ";
    ((cLinkStatus & 0x00000002) >> 1) ? std::cout << "used    " : std::cout << "not used ";
    std::cout << " | Block4: ";
    ((cLinkStatus & 0x00000001) >> 0) ? std::cout << "used    " : std::cout << "not used " << std::endl;
}

std::vector<uint32_t> PixelPh1FEDInterface::readTransparentFIFO() {
    std::vector<uint32_t> cFifoVec = regManager->ReadBlockRegValue("fifo.bit_stream", 512);
    //vectors to pass to the NRZI decoder as reference to be filled by that
    std::vector<uint8_t> c5bSymbol, c5bNRZI, c4bNRZI;
    decodeTransparentSymbols(cFifoVec, c5bSymbol, c5bNRZI, c4bNRZI);
    prettyPrintTransparentFIFO(cFifoVec, c5bSymbol, c5bNRZI, c4bNRZI);
    return cFifoVec;
}

int PixelPh1FEDInterface::drainTransparentFifo(uint32_t *data) {
    const size_t MAX_TRANSPARENT_FIFO = 1024;
    std::vector<uint32_t> v(readTransparentFIFO());
    const int ie(min(v.size(), MAX_TRANSPARENT_FIFO));
    for (int i = 0; i < ie; ++i)
        data[i] = v[i];
    return ie;
}

void PixelPh1FEDInterface::decodeTransparentSymbols(const std::vector<uint32_t> &pInData, std::vector<uint8_t> &p5bSymbol, std::vector<uint8_t> &p5bNRZI, std::vector<uint8_t> &p4bNRZI) {
    //first, clear all the non-const references
    p5bSymbol.clear();
    p5bNRZI.clear();
    p4bNRZI.clear();

    //ok, then internally generate a vector of 5b Symbols and use a uint8_t for that
    for (size_t i = 0; i < pInData.size(); ++i) {
        uint32_t cWord = pInData[i];
        cWord &= 0x0000001f;

        p5bSymbol.push_back(cWord);

        //next, fill the 5bNRZI 4bNRZI with 0 symbols with 0x1f
        p5bNRZI.push_back(0x1f);
        p4bNRZI.push_back(0);
    }

    for (uint32_t i = 1; i < pInData.size(); i++) {

        if (p5bSymbol[i] == 0x1f)
            p5bNRZI[i] = 0x16; //print 10110

        if (p5bSymbol[i] == 0)
            p5bNRZI[i] = 0x16; //print 10110

        //if( i > 0 )
        //{

        if ((p5bSymbol[i] == 0x14) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x1e; //# print '11110'

        if ((p5bSymbol[i] == 0x0e) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x09; //# print '01001'

        if ((p5bSymbol[i] == 0x18) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x14; //# print '10100'

        if ((p5bSymbol[i] == 0x19) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x15; //# print '10101'

        if ((p5bSymbol[i] == 0x0c) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x0a; //# print '01010'

        if ((p5bSymbol[i] == 0x0d) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x0b; //# print '01011'

        if ((p5bSymbol[i] == 0x0b) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x0e; //# print '01110'

        if ((p5bSymbol[i] == 0x0a) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x0f; //# print '01111'

        if ((p5bSymbol[i] == 0x1c) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x12; //# print '10010'

        if ((p5bSymbol[i] == 0x1d) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x13; //# print '10011'

        if ((p5bSymbol[i] == 0x1b) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x16; //# print '10110'

        if ((p5bSymbol[i] == 0x1a) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x17; //# print '10111'

        if ((p5bSymbol[i] == 0x13) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x1a; //# print '11010'

        if ((p5bSymbol[i] == 0x12) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x1b; //# print '11011'

        if ((p5bSymbol[i] == 0x17) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x1c; //# print '11100'

        if ((p5bSymbol[i] == 0x16) && ((p5bSymbol[i - 1] & 0x1) == 0))
            p5bNRZI[i] = 0x1d; //# print '11101'

        if ((p5bSymbol[i] == 0x0b) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x1e; //# print '11110'

        if ((p5bSymbol[i] == 0x11) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x09; //# print '01001'

        if ((p5bSymbol[i] == 0x07) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x14; //# print '10100'

        if ((p5bSymbol[i] == 0x06) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x15; //# print '10101'

        if ((p5bSymbol[i] == 0x13) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x0a; //# print '01010'

        if ((p5bSymbol[i] == 0x12) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x0b; //# print '01011'

        if ((p5bSymbol[i] == 0x14) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x0e; //# print '01110'

        if ((p5bSymbol[i] == 0x15) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x0f; //# print '01111'

        if ((p5bSymbol[i] == 0x03) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x12; //# print '10010'

        if ((p5bSymbol[i] == 0x02) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x13; //# print '10011'

        if ((p5bSymbol[i] == 0x04) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x16; //# print '10110'

        if ((p5bSymbol[i] == 0x05) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x17; //# print '10111'

        if ((p5bSymbol[i] == 0x0c) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x1a; //# print '11010'

        if ((p5bSymbol[i] == 0x0d) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x1b; //# print '11011'

        if ((p5bSymbol[i] == 0x08) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x1c; //# print '11100'

        if ((p5bSymbol[i] == 0x09) && ((p5bSymbol[i - 1] & 0x1) == 1))
            p5bNRZI[i] = 0x1d; //# print '11101'
    }

    for (uint32_t i = 0; i < pInData.size(); i++) {
        if (p5bNRZI[i] == 0x1e)
            p4bNRZI[i] = 0x0; // #

        if (p5bNRZI[i] == 0x09)
            p4bNRZI[i] = 0x1; // #

        if (p5bNRZI[i] == 0x14)
            p4bNRZI[i] = 0x2; // #

        if (p5bNRZI[i] == 0x15)
            p4bNRZI[i] = 0x3; // #

        if (p5bNRZI[i] == 0x0a)
            p4bNRZI[i] = 0x4; // #

        if (p5bNRZI[i] == 0x0b)
            p4bNRZI[i] = 0x5; // #

        if (p5bNRZI[i] == 0x0e)
            p4bNRZI[i] = 0x6; // #

        if (p5bNRZI[i] == 0x0f)
            p4bNRZI[i] = 0x7; // #

        if (p5bNRZI[i] == 0x12)
            p4bNRZI[i] = 0x8; // #

        if (p5bNRZI[i] == 0x13)
            p4bNRZI[i] = 0x9; // #

        if (p5bNRZI[i] == 0x16)
            p4bNRZI[i] = 0xa; // #

        if (p5bNRZI[i] == 0x17)
            p4bNRZI[i] = 0xb; // #

        if (p5bNRZI[i] == 0x1a)
            p4bNRZI[i] = 0xc; // #

        if (p5bNRZI[i] == 0x1b)
            p4bNRZI[i] = 0xd; // #

        if (p5bNRZI[i] == 0x1c)
            p4bNRZI[i] = 0xe; // #

        if (p5bNRZI[i] == 0x1d)
            p4bNRZI[i] = 0xf; // #

        if (p5bNRZI[i] == 0x1f)
            p4bNRZI[i] = 0x0; // #error
    }
}

void PixelPh1FEDInterface::prettyPrintTransparentFIFO(const std::vector<uint32_t> &pFifoVec, const std::vector<uint8_t> &p5bSymbol, const std::vector<uint8_t> &p5bNRZI, const std::vector<uint8_t> &p4bNRZI) {
    std::cout << std::endl << "Transparent FIFO: " << std::endl
              << " Timestamp      5b Symbol " << std::endl
              << "                5b NRZI   " << std::endl
              << "                4b NRZI   " << std::endl
              << "                TBM Core A" << std::endl
              << "                TBM Core B" << std::endl;

    // now print and decode the FIFO data
    for (uint32_t j = 0; j < 32; j++) {
        //first line with the timestamp
        std::cout << "          " << ((pFifoVec.at((j * 16)) >> 20) & 0xfff) << ": ";

        for (uint32_t i = 0; i < 16; i++)
            std::cout << " " << std::bitset<5>(((p5bSymbol.at(i + (j * 16)) >> 0) & 0x1f));

        std::cout << std::endl << "               ";

        //now the line with the 5bNRZI word
        for (uint32_t i = 0; i < 16; i++) {
            if (p5bNRZI.at(i + j * 16) == 0x1f)
                std::cout << " "
                          << "ERROR";
            else
                std::cout << " " << std::bitset<5>(((p5bNRZI.at(i + (j * 16)) >> 0) & 0x1f));
        }

        std::cout << std::endl << "               ";

        //now the line with the 4bNRZI word
        for (uint32_t i = 0; i < 16; i++)
            std::cout << " " << std::bitset<4>(((p4bNRZI.at(i + (j * 16)) >> 0) & 0x1f)) << " ";

        std::cout << std::endl << "               ";

        //now the line with TBM Core A word
        for (uint32_t i = 0; i < 16; i++) {
            std::cout << " " << std::bitset<1>(((p4bNRZI.at(i + (j * 16)) >> 3) & 0x1));
            std::cout << " " << std::bitset<1>(((p4bNRZI.at(i + (j * 16)) >> 1) & 0x1));
        }

        std::cout << std::endl << "               ";

        //now the line with TBM Core B word
        for (uint32_t i = 0; i < 16; i++) {
            std::cout << " " << std::bitset<1>((((p4bNRZI.at(i + (j * 16)) >> 2) & 0x1) ^ 0x1));
            std::cout << " " << std::bitset<1>((((p4bNRZI.at(i + (j * 16)) >> 0) & 0x1) ^ 0x1));
        }

        std::cout << std::endl << std::endl;
    }
}

void prettyprintSpyFIFO(const std::vector<uint32_t> &pVec) {
    for (size_t i = 0; i < pVec.size(); ++i) {
        uint32_t cWord = pVec[i];
        const uint32_t timestamp = (cWord >> 20) & 0xfff;
        const uint32_t marker = (cWord & 0xf0) >> 4;
        if ((cWord & 0xff) != 0)
            std::cout << std::hex << (cWord & 0xff) << std::dec << " ";
        if (marker == 0xb || marker == 6 || marker == 7 || marker == 0xf)
            std::cout << "       " << timestamp << std::endl;
    }
}

std::vector<uint32_t> PixelPh1FEDInterface::readSpyFIFO() {
    std::vector<uint32_t> cSpy[2];
    const size_t N = 4096; //kme was 2048
    for (int i = 0; i < 2; ++i) {
        { //while (1) {
            std::vector<uint32_t> tmp = regManager->ReadBlockRegValue(i == 0 ? "fifo.spy_A" : "fifo.spy_B", N);
            std::vector<uint32_t>::iterator it = std::find(tmp.begin(), tmp.end(), 0);
            //      int l = it - tmp.begin();
            //      if (l == 0)
            //	break;
            cSpy[i].insert(cSpy[i].end(), tmp.begin(), it);
        }
    }

    if (getPrintlevel() & 16) {
        std::cout << std::endl << "TBM_SPY FIFO A (size " << cSpy[0].size() << "):" << std::endl;
        prettyprintSpyFIFO(cSpy[0]);
        std::cout << std::endl << "TBM_SPY FIFO B (size " << cSpy[1].size() << "):" << std::endl;
        prettyprintSpyFIFO(cSpy[1]);
    }

    //append content of Spy Fifo B to A and return
    return std::vector<uint32_t>();
    //    std::vector<uint32_t> cAppendedSPyFifo = cSpyA;
    //    cAppendedSPyFifo.insert(cSpyA.end(), cSpyB.begin(), cSpyB.end());
    //    return cAppendedSPyFifo;
}

int PixelPh1FEDInterface::drainSpyFifo(uint32_t *data) {
    const size_t MAX_SPY_FIFO = 1024;
    std::vector<uint32_t> v(readSpyFIFO());
    const int ie(min(v.size(), MAX_SPY_FIFO));
    for (int i = 0; i < ie; ++i)
        data[i] = v[i];
    return ie;
}

PixelPh1FEDInterface::encfifo1 PixelPh1FEDInterface::decodeFIFO1Stream(const std::vector<uint32_t> &fifo, const std::vector<uint32_t> &markers) {
    encfifo1 f;

    for (size_t i = 0, ie = fifo.size(); i < ie; ++i) {
        const uint32_t word = fifo[i];
        const uint32_t marker = markers[i];

        if (marker == 8) {
            ++f.n_tbm_h;
            f.found = true;
            f.ch = (word >> 26) & 0x3f;
            f.id_tbm_h = (word >> 21) & 0x1f;
            f.tbm_h = (word >> 9) & 0xff;
            f.event = word & 0xff;
        } else if (marker == 12) {
            ++f.n_roc_h;
            encfifo1roc r;
            r.ch = (word >> 26) & 0x3f;
            r.roc = (word >> 21) & 0x1f;
            r.rb = word & 0xff;
            f.rocs.push_back(r);
        } else if (marker == 1) {
            encfifo1hit h;
            h.ch = (word >> 26) & 0x3f;
            h.roc = (word >> 21) & 0x1f;
            h.dcol = (word >> 16) & 0x1f;
            h.pxl = (word >> 8) & 0xff;
            h.col = h.dcol * 2 + h.pxl % 2;
            h.row = 80 - h.pxl / 2;
            h.ph = (word) & 0xff;
            f.hits.push_back(h);
        } else if (marker == 4) {
            ++f.n_tbm_t;
            f.ch_tbm_t = (word >> 26) & 0x3f;
            f.tbm_t1 = word & 0xff;
            //f.tbm_t2 = (word >> 12) & 0xff;
            f.tbm_t2 = (word >> 8) & 0x1fff;
            f.id_tbm_t = (word >> 21) & 0x1f;
        } else if (marker == 6) {
            f.ch_evt_t = (word >> 26) & 0x3f;
            //assert(f.ch == f.ch_evt_t);
            f.id_evt_t = (word >> 21) & 0x1f;
            f.marker = word & 0x1fffff;
        }
    }

    return f;
}

void PixelPh1FEDInterface::prettyprintFIFO1Stream(const std::vector<uint32_t> &pFifoVec, const std::vector<uint32_t> &pMarkerVec) {
    std::cout << "----------------------------------------------------------------------------------" << std::endl;

    for (size_t i = 0, ie = pFifoVec.size(); i < ie; ++i) {
        const uint32_t d = pFifoVec[i];
        const uint32_t m = pMarkerVec[i];
        if (d != 0 || m != 0) {
            std::cout << "word #" << std::setw(2) << i << ": 0x" << std::hex << d << "  marker: 0x" << m << std::dec << "   ";
            if (m == 8)
                std::cout << std::dec << "    Header: "
                          << "CH: " << ((d >> 26) & 0x3f)
                          << " ID: " << ((d >> 21) & 0x1f)
                          << " TBM_H: " << ((d >> 9) & 0xff)
                          << " EVT Nr: " << ((d) & 0xff);
            else if (m == 12)
                std::cout << std::dec << "ROC Header: "
                          << "CH: " << ((d >> 26) & 0x3f)
                          << " ROC Nr: " << ((d >> 21) & 0x1f)
                          << " Status: " << ((d) & 0xff);
            else if (m == 1)
                std::cout << std::dec << "            "
                          << "CH: " << ((d >> 26) & 0x3f)
                          << " ROC Nr: " << ((d >> 21) & 0x1f)
                          << " DC: " << ((d >> 16) & 0x1f)
                          << " PXL: " << ((d >> 8) & 0xff)
                          << " PH: " << ((d) & 0xff);
            else if (m == 4)
                std::cout << std::dec << "   Trailer: "
                          << "CH: " << ((d >> 26) & 0x3f)
                          << " ID: " << ((d >> 21) & 0x1f)
                          << " TBM_T2: " << ((d >> 12) & 0xff)
                          << " TBM_T1: " << ((d) & 0xff)
                          << " NOR: " << ((d >> 8) & 0xf);
            else if (m == 6)
                std::cout << std::dec << "Event Trailer: "
                          << "CH: " << ((d >> 26) & 0x3f)
                          << " ID: " << ((d >> 21) & 0x1f)
                          << " marker: " << ((d) & 0x1fffff);
            std::cout << std::endl;
        }
    }

    std::cout << "----------------------------------------------------------------------------------" << std::endl;
}

PixelPh1FEDInterface::digfifo1 PixelPh1FEDInterface::readFIFO1(bool print) {
    digfifo1 df;

    df.cFifo1A = regManager->ReadBlockRegValue("fifo.spy_1_A", 2048);
    df.cMarkerA = regManager->ReadBlockRegValue("fifo.spy_1_A_marker", 2048);
    df.cFifo1B = regManager->ReadBlockRegValue("fifo.spy_1_B", 2048);
    df.cMarkerB = regManager->ReadBlockRegValue("fifo.spy_1_B_marker", 2048);

    df.a = decodeFIFO1Stream(df.cFifo1A, df.cMarkerA);
    df.b = decodeFIFO1Stream(df.cFifo1B, df.cMarkerB);
    //  assert(df.a.event == 0 || df.b.event == 0 || df.a.event == df.b.event);
    if (print) {
        printTTSState();
        std::cout << "FIFO1 for A:\n";
        prettyprintFIFO1Stream(df.cFifo1A, df.cMarkerA);
        std::cout << "FIFO1 for B:\n";
        prettyprintFIFO1Stream(df.cFifo1B, df.cMarkerB);
    }

    return df;
}

int PixelPh1FEDInterface::drainFifo1(uint32_t *data) {
    //KMEBAD
    regManager->WriteReg("fe_ctrl_regs.fifo_1_to_read", 8); // select fiber  (nasty)
    //KME
    //readFIFO1(true);
    readFIFO1(false);
    return 0;
}

int PixelPh1FEDInterface::drainErrorFifo(uint32_t *data) {
    // JMTBAD printlevel
    const bool force = true;
    //KMEverbose  std::cout << "drainErrorFifo: " << (force ? "forcing read" : "") << "\n";
    if (force)
        regManager->WriteReg("pixfed_ctrl_regs.error_fifo_force_read", 1);

    int swtimeout = 0;
    while (1) {
        const uint32_t rdready = regManager->ReadReg("pixfed_stat_regs.error_fifo_read_rdy");
        if (rdready == 1 || swtimeout++ == 1000) {
            if (swtimeout == 1000) {
                std::cout << " break with rdready = " << rdready << " SWtimeout = " << swtimeout << std::endl;
            } //kmeverbose
            break;
        }
        //KMEverbose    if (rdready != 0) std::cout << " rdready is " << rdready << std::endl;
        usleep(100);
    }

    //KMEverbose  std::cout << "Error FIFO read ready =1! " << std::endl;

    const uint32_t num_words = regManager->ReadReg("pixfed_stat_regs.error_fifo_wr_data_count");
    const std::vector<uint32_t> words = regManager->ReadBlockRegValue("ERROR_fifo", num_words);

    regManager->WriteReg("pixfed_ctrl_regs.error_fifo_read_done", 1);
    while (regManager->ReadReg("pixfed_stat_regs.error_fifo_read_rdy"))
        usleep(100);
    regManager->WriteReg("pixfed_ctrl_regs.error_fifo_read_done", 0);

    if (force)
        regManager->WriteReg("pixfed_ctrl_regs.error_fifo_force_read", 0);

    //kme verbose
    //std::cout << "fed# " << pixelFEDCard.fedNumber << " Error FIFO contains " << num_words << " error words:" << "\n";
    for (size_t i = 0; i < words.size(); ++i) {
        if (data)
            data[i] = words[i];
        //kme verbose
        //std::cout << "word " << std::setw(3) << ": 0x" << std::hex << std::setw(8) << words[i] << std::dec << "\n";
    }

    return int(words.size());
}

void PixelPh1FEDInterface::SelectDaqDDR(uint32_t pNthAcq) {
    fStrDDR = ((pNthAcq % 2 + 1) == 1 ? "DDR0" : "DDR1");
    fStrDDRControl = ((pNthAcq % 2 + 1) == 1 ? "pixfed_ctrl_regs.DDR0_ctrl_sel" : "pixfed_ctrl_regs.DDR1_ctrl_sel");
    fStrFull = ((pNthAcq % 2 + 1) == 1 ? "pixfed_stat_regs.DDR0_full" : "pixfed_stat_regs.DDR1_full");
    fStrReadout = ((pNthAcq % 2 + 1) == 1 ? "pixfed_ctrl_regs.DDR0_end_readout" : "pixfed_ctrl_regs.DDR1_end_readout");
}

void prettyprintTBMFIFO(const std::vector<uint32_t> &pData) {
    std::cout << "Global TBM Readout FIFO: size " << pData.size() << std::endl;
    //now I need to do something with the Data that I read into cData
    int cIndex = 0;
    uint32_t cPreviousWord = 0;
    for (size_t i = 0; i < pData.size(); ++i) {
        uint32_t cWord = pData[i];
        std::cout << std::setw(5) << i << ": " << std::hex << std::setw(8) << cWord << std::dec << std::endl;
        if (cIndex % 2 == 0)
            cPreviousWord = cWord;

        else if (cPreviousWord == 0x1) {
            //std::cout << cWord <<  std::endl;
            std::cout << "    Pixel Hit: CH: " << ((cWord >> 26) & 0x3f) << " ROC: " << ((cWord >> 21) & 0x1f) << " DC: " << ((cWord >> 16) & 0x1f) << " ROW: " << ((cWord >> 8) & 0xff) << " PH: " << (cWord & 0xff) << std::dec << std::endl;
        }
        //else if (cPreviousWord == 0x6)
        //{
        ////std::cout << cWord <<  std::endl;
        //std::cout << "Event Trailer: CH: " << ((cWord >> 26) & 0x3f) << " ID: " << ((cWord >> 21) & 0x1f) << " marker: " << (cWord & 0x1fffff) << " ROW: " << ((cWord >> 8) & 0xff) << " PH: " << (cWord & 0xff) << std::dec << std::endl;
        //}
        else if (cPreviousWord == 0x8) {
            //std::cout << cWord <<  std::endl;
            std::cout << "Event Header: CH: " << ((cWord >> 26) & 0x3f) << " ID: " << ((cWord >> 21) & 0x1f) << " TBM H: " << ((cWord >> 16) & 0x1f) << " ROW: " << ((cWord >> 9) & 0xff) << " EventNumber: " << (cWord & 0xff) << std::dec << std::endl;
        } else if (cPreviousWord == 0xC) {
            //std::cout << cWord <<  std::endl;
            std::cout << " ROC Header: CH: " << ((cWord >> 26) & 0x3f) << " ROC Nr: " << ((cWord >> 21) & 0x1f) << " Status : " << (cWord & 0xff) << std::dec << std::endl;
        } else if (cPreviousWord == 0x4) {
            //std::cout << cWord <<  std::endl;
            std::cout << " TBM Trailer: CH: " << ((cWord >> 26) & 0x3f) << " ID: " << ((cWord >> 21) & 0x1f) << " TBM T2: " << ((cWord >> 12) & 0xff) << " TBM_T1: " << (cWord & 0xff) << std::dec << std::endl;
        }
        cIndex++;
    }
}

std::vector<uint32_t> PixelPh1FEDInterface::ReadData(uint32_t cBlockSize) {
    //    std::cout << "JJJ READ DATA " << cBlockSize << std::endl;
    //std::chrono::milliseconds cWait( 10 );
    // the fNthAcq variable is automatically used to determine which DDR FIFO to read - so it has to be incremented in this method!

    // first find which DDR bank to read
    SelectDaqDDR(fNthAcq);
    std::cout << "Querying " << fStrDDR << " for FULL condition!" << std::endl;

    uhal::ValWord<uint32_t> cVal;
    int tries = 0;
    int maxtries = 100;
    do {
        cVal = regManager->ReadReg(fStrFull);
        if (cVal == 0 && tries++ < maxtries)
            usleep(sleep1000);
    } while (cVal == 0 && tries < maxtries);
    std::cout << fStrDDR << " full: " << regManager->ReadReg(fStrFull) << " after " << tries << " tries " << std::endl;

    // DDR control: 0 = ipbus, 1 = user
    regManager->WriteReg(fStrDDRControl, 0);
    usleep(sleep10000);
    std::cout << "Starting block read of " << fStrDDR << std::endl;

    std::vector<uint32_t> cData;
    for (int i = 0; i < 5; ++i) {
        std::vector<uint32_t> tmp = regManager->ReadBlockRegValue(fStrDDR, cBlockSize);
        cData.insert(cData.end(), tmp.begin(), tmp.end());
    }

    regManager->WriteReg(fStrDDRControl, 1);
    usleep(sleep10000);
    regManager->WriteReg(fStrReadout, 1);
    usleep(sleep10000);

    // full handshake between SW & FW
    while (regManager->ReadReg(fStrFull) == 1)
        usleep(sleep10000);
    regManager->WriteReg(fStrReadout, 0);

    //prettyprintTBMFIFO(cData);
    fNthAcq++;
    return cData;
}

uint32_t PixelPh1FEDInterface::getScore(int channel) {
    static const char *score_nodes[48] = {
        "idel_individual_stat.score_CH0", "idel_individual_stat.score_CH1", "idel_individual_stat.score_CH2",
        "idel_individual_stat.score_CH3", "idel_individual_stat.score_CH4", "idel_individual_stat.score_CH5",
        "idel_individual_stat.score_CH6", "idel_individual_stat.score_CH7", "idel_individual_stat.score_CH8",
        "idel_individual_stat.score_CH9", "idel_individual_stat.score_CH10", "idel_individual_stat.score_CH11",
        "idel_individual_stat.score_CH12", "idel_individual_stat.score_CH13", "idel_individual_stat.score_CH14",
        "idel_individual_stat.score_CH15", "idel_individual_stat.score_CH16", "idel_individual_stat.score_CH17",
        "idel_individual_stat.score_CH18", "idel_individual_stat.score_CH19", "idel_individual_stat.score_CH20",
        "idel_individual_stat.score_CH21", "idel_individual_stat.score_CH22", "idel_individual_stat.score_CH23",
        "idel_individual_stat.score_CH24", "idel_individual_stat.score_CH25", "idel_individual_stat.score_CH26",
        "idel_individual_stat.score_CH27", "idel_individual_stat.score_CH28", "idel_individual_stat.score_CH29",
        "idel_individual_stat.score_CH30", "idel_individual_stat.score_CH31", "idel_individual_stat.score_CH32",
        "idel_individual_stat.score_CH33", "idel_individual_stat.score_CH34", "idel_individual_stat.score_CH35",
        "idel_individual_stat.score_CH36", "idel_individual_stat.score_CH37", "idel_individual_stat.score_CH38",
        "idel_individual_stat.score_CH39", "idel_individual_stat.score_CH40", "idel_individual_stat.score_CH41",
        "idel_individual_stat.score_CH42", "idel_individual_stat.score_CH43", "idel_individual_stat.score_CH44",
        "idel_individual_stat.score_CH45", "idel_individual_stat.score_CH46", "idel_individual_stat.score_CH47"
    };

    assert(channel >= 1 && channel <= 48);
    return regManager->ReadReg(score_nodes[channel - 1]);
}

std::vector<uint32_t> PixelPh1FEDInterface::getScores() {
    return regManager->ReadBlockRegValue("idel_individual_stat.scores", 48);
}

int PixelPh1FEDInterface::spySlink64(uint64_t *data) {
    ++slink64calls;
    //static int event_old;
    //const int maxprints = 5;
    //const bool do_prints = maxprints - int(slink64calls) > 0;
    const bool do_prints = slink64calls % 1000 == 1;
    if (do_prints) {
        std::cout << "fed #" << pixelFEDCard.fedNumber << " slink64call #" << slink64calls << std::endl;
        //readTransparentFIFO();
        //readSpyFIFO();
    }

    //  drainErrorFifo(0);

    size_t ndata64 = 0;

#ifndef JMT_FROMFIFO1
    //usleep(2000);

    uhal::ValWord<uint32_t> v_poll = 0;
    uhal::ValWord<uint32_t> v_nwords = 0;
    //uint32_t mycntword = 0;
    int sleepcnt = 0;
    bool our_timeout = false;

    uhal::HwInterface &hw = regManager->getHwInterface();

    do {
        v_poll = hw.getNode("pixfed_stat_regs.DDR0_full").read();
        v_nwords = hw.getNode("pixfed_stat_regs.cnt_word32from_start").read();
        hw.dispatch();
        if (v_poll == 0)
            usleep(1);
        sleepcnt++;
        //if(sleepcnt > 1000) mycntword = regManager->ReadReg("pixfed_stat_regs.cnt_word32from_start");
        //if(mycntword>5){std::cout<<mycntword<<" words in the ddr"<<std::endl; usleep(300000);}
        if (sleepcnt > 20000) {
            cout << "FED#" << pixelFEDCard.fedNumber << " \033[1m\033[32mTIMEOUT POLLING THE DDR (~1sec)\033[0m" << std::endl;
            our_timeout = true;
            // dump status registers to see why we are stuck
            diagnosticDump();
            break;
        }
    } while (v_poll == 0);

    const uint32_t block_size = v_nwords + (2 * 2 * pixelFEDCard.calib_mode_num_events); // nwords from fed does not include slink headers and trailers

    //std::cout << "sleepcnt " << sleepcnt << " nwords is " << v_nwords << " block size " << cBlockSize << "\n";

    uhal::ValVector<uint32_t> v_data = hw.getNode("DDR0").readBlock(block_size);
    hw.getNode("pixfed_ctrl_regs.DDR0_end_readout").write(1);

    if (our_timeout) {
        hw.getNode("pixfed_ctrl_regs.DDR0_end_readout").write(0);
        hw.dispatch();
        std::cout << "error decoder:\n";
        ///ErrorFIFODecoder ed(&v_data[2], v_data.size()-4);
        //    ed.printToStream(std::cout);
        return 0;
    }

    hw.dispatch();

    while (regManager->ReadReg("pixfed_stat_regs.DDR0_full") == 1)
        usleep(1);

    regManager->WriteReg("pixfed_ctrl_regs.DDR0_end_readout", 0);

    const size_t ndata = v_data.size();
    ndata64 = ndata / 2;
    //std::cout << "block_size " << block_size << " v_data size " << v_data.size() << "\n";
    // fix for 13.12/13.19 DDR odd number of words
    /* --------
  if (ndata % 2 != 0) {
    std::cout << "FED#" << pixelFEDCard.fedNumber << " \033[1m\033[32mdata packet length not even\033[0m" << std::endl;
    return 0;
  }
  ------ */
    //std::vector<uint64_t> data64(ndata64, 0ULL);

    for (size_t j = 0; j < ndata64; ++j)
        data[j] = (uint64_t(v_data[j * 2]) << 32) | v_data[j * 2 + 1];

#if 1
    int nheaders = 0, ntrailers = 0, nevnumcorrect = 0, nbxnumcorrect = 0;
    uint32_t last_evnum = 0xffffffff;
    for (size_t j = 0; j < ndata64; ++j) {
        //    printf("0x%16lx\n", data[j]);
        if ((data[j] >> 56) == 0x50 && (j == 0 || (data[j - 1] >> 46) == 0x28000)) {
            ++nheaders;

            const uint32_t evnum = (data[j] >> 32) & 0xFFFFFF;
            //const uint32_t bxnum = (data[j] & 0xFFFFFFFF) >> 20;

            //lc commented
            if ((j == 0 && evnum != (slink64calls - 1) * pixelFEDCard.calib_mode_num_events + 1) || (j > 0 && evnum != ((last_evnum + 1) & 0xFFFFFF)))
                std::cout << "fed#" << pixelFEDCard.fedNumber << " slink64call #" << slink64calls << "\033[1m\033[31m screwed up event order \033[0m: last_evnum " << last_evnum << " evnum " << evnum << "\n";
            else
                ++nevnumcorrect;
            last_evnum = evnum;

            /*
      if (bxnum != 503)
	std::cout << "fed#" << pixelFEDCard.fedNumber << " slink64call #" << slink64calls << ", fed event #" << evnum << " \033[1m\033[31mWRONG BX\033[0m bx #" << bxnum << "; blocksize: " << block_size << std::endl;
      else
	++nbxnumcorrect;
      */
        }

        if ((data[j] >> 46) == 0x28000)
            ++ntrailers;
    }

    //if (nheaders != ntrailers || nheaders != nevnumcorrect || nheaders != nbxnumcorrect || nheaders != pixelFEDCard.calib_mode_num_events)
    if (nheaders != ntrailers || nheaders != nevnumcorrect || nheaders != pixelFEDCard.calib_mode_num_events)
        std::cout << "fed#" << pixelFEDCard.fedNumber << " slink64call #" << slink64calls << "\033[1m\033[31m something wrong with calib mode reading: \033[0m: nheaders: " << nheaders << " ntrailers: " << ntrailers << " nevnumcorrect: " << nevnumcorrect << " nbxnumcorrect: " << nbxnumcorrect << " expected: " << pixelFEDCard.calib_mode_num_events << "\n";
#endif

#else

    const bool check_baddies = false;
    const char *baddies_names[5] = { "empties", "missed tbm header", "missed tbm trailer", "missed roc headers", "missed cal" };

    data[0] = 0x5000000000000000;
    data[0] |= uint64_t(slink64calls & 0xffffff) << 32;
    data[0] |= uint64_t(pixelFEDCard.fedNumber) << 8;
    size_t j = 1;
    event_old += 1;
    for (int fib = 0; fib < 24; ++fib) {
        if (!fibers_in_use[fib + 1])
            continue;

        regManager->WriteReg("fe_ctrl_regs.fifo_1_to_read", fib);

        if (do_prints)
            std::cout << "fed#" << pixelFEDCard.fedNumber << " fifo1 fiber " << fib + 1 << std::endl;

        digfifo1 f = readFIFO1(do_prints);

        if (check_baddies) {
            int code = -1;

            cout << " event number " << f.a.event << " " << f.b.event << endl;
            if (f.a.event != (event_old)) {
                cout << " something wrong " << f.a.event << " " << (event_old) << endl;
                int dum = 0;
                cin >> dum;
            }

            if (!f.a.ok() || !f.b.ok())
                code = 0;
            if (f.a.n_tbm_h != 1 || f.b.n_tbm_h != 1)
                code = 1;
            if (f.a.n_tbm_t != 1 || f.b.n_tbm_t != 1)
                code = 2;
            //if (f.a.n_roc_h != 8 || f.b.n_roc_h != 8)
            if (f.a.n_roc_h != 4 || f.b.n_roc_h != 4)
                code = 3;
            if ((f.a.tbm_t1 & 2) == 0 || (f.b.tbm_t1 & 2) == 0)
                code = 4;

            if (code != -1) {
                baddies[code].push_back(std::make_pair(fib, slink64calls));

                std::cout << "got a baddie, fed " << pixelFEDCard.fedNumber << " fiber " << fib << " (" << baddies_names[code] << ") at slink64call " << slink64calls << "\n";
                //readTransparentFIFO();
                //readSpyFIFO();
                std::cout << "FIFO1 for A:\n";
                prettyprintFIFO1Stream(f.cFifo1A, f.cMarkerA);
                std::cout << "FIFO1 for B:\n";
                prettyprintFIFO1Stream(f.cFifo1B, f.cMarkerB);
            }

            if (1) { // print
                std::cout << "FIFO1 for A:\n";
                prettyprintFIFO1Stream(f.cFifo1A, f.cMarkerA);
                std::cout << "FIFO1 for B:\n";
                prettyprintFIFO1Stream(f.cFifo1B, f.cMarkerB);
            }
        }

        const bool oldway = true;
        if (oldway) {
            for (size_t i = 0; i < f.a.hits.size(); ++i, ++j) {
                encfifo1hit h = f.a.hits[i];
                data[j] = (h.ch << 26) | (h.roc << 21) | (h.dcol << 16) | (h.pxl << 8) | h.ph;
                data[j] |= uint64_t(0x1b) << 53;
            }
            for (size_t i = 0; i < f.b.hits.size(); ++i, ++j) {
                encfifo1hit h = f.b.hits[i];
                data[j] = (h.ch << 26) | (h.roc << 21) | (h.dcol << 16) | (h.pxl << 8) | h.ph;
                data[j] |= uint64_t(0x1b) << 53;
            }
        } else {
            size_t ii = 0;
            uint32_t last_w = 0;
            for (size_t i = 0; i < f.a.hits.size(); ++i, ++ii) {
                encfifo1hit h = f.a.hits[i];
                uint32_t w = (h.ch << 26) | (h.roc << 21) | (h.dcol << 16) | (h.pxl << 8) | h.ph;
                if (ii % 2 == 1)
                    data[1 + ii / 2] = (uint64_t(w) << 32) | last_w;
                last_w = w;
            }
            if (ii % 2 == 1)
                data[1 + ii / 2] = (uint64_t(0x1b) << 53) | (1ULL << 32) | last_w;

            for (size_t i = 0; i < f.b.hits.size(); ++i, ++ii) {
                encfifo1hit h = f.b.hits[i];
                uint32_t w = (h.ch << 26) | (h.roc << 21) | (h.dcol << 16) | (h.pxl << 8) | h.ph;
                if (ii % 2 == 1)
                    data[1 + ii / 2] = (uint64_t(w) << 32) | last_w;
                last_w = w;
            }
            if (ii % 2 == 1)
                data[1 + ii / 2] = (uint64_t(0x1b) << 53) | (1ULL << 32) | last_w;

            j = ii / 2 + 1;
        }
    }
    data[j] = 0xa000000000000000;
    data[j] |= uint64_t((j + 1) & 0x3fff) << 32;
    ++j;

    ndata64 = j;

    // maybe we trashed the 5 in the msb, it's all the decoder cares about...
    data[0] = 0x5000000000000000 | (data[0] & 0xFFFFFFFFFFFFFFF);

#if 0
  if (do_prints) {
    std::cout << "my fake fifo3:\n" << std::hex;
    for (size_t i = 0; i < j; ++i)
      std::cout << "0x" << std::setw(16) << std::setfill('0') << data[i] << std::setfill(' ') << std::endl;
    std::cout << std::dec;
  }
#endif

    if (check_baddies && slink64calls % 1000 == 1) {
        for (int ii = 0; ii < 5; ++ii) {
            if (baddies[ii].size()) {
                std::cout << baddies_names[ii] << " since last print:\n";
                for (size_t i = 0; i < baddies[ii].size(); ++i)
                    std::cout << "fib " << baddies[ii][i].first << " @ " << baddies[ii][i].second << "\n";
                baddies[ii].clear();
            }
        }
    }

#endif

#if 0
  bxs.push_back(bxnum);
  if (bxs.size() == 1000) {
    printf("fed#%lu last 1000 bxnums: [", pixelFEDCard.fedNumber);
    for (int ibx = 0; ibx < 1000; ++ibx)
      printf("%u,", bxs[ibx]);
    printf("]\n");
    bxs.clear();
  }
#endif

#if 0
  if (do_prints) {
    std::cout << "slink 64-bit words from FW:\n";
    for (size_t j = 0; j < ndata64; ++j)
      std::cout << std::setw(2) << j << " = 0x " << std::hex << std::setw(8) << std::setfill('0') << (data[j]>>32) << " " << std::setw(8) << std::setfill('0') << (data[j] & 0xFFFFFFFF) << std::setfill(' ') << std::dec << "\n";
    std::cout << "saw " << nheaders << " headers and " << ntrailers << " trailers\n";

    FIFO3Decoder decode3(data, ndata64, pixelFEDCard.calib_mode_num_events);
    //for (size_t i = 0; i <= ndata64; ++i)
    std::cout << "FIFO3Decoder thinks:\n" << "nhits: " << decode3.nhits() << std::endl;
    for (unsigned i = 0; i < decode3.nhits(); ++i) {
      //const PixelROCName& rocname = theNameTranslation_->ROCNameFromFEDChannelROC(fednumber, decode3.channel(i), decode3.rocid(i)-1);
      std::cout << "#" << i << ": ch: " << decode3.channel(i)
                << " rocid: " << decode3.rocid(i)
        //    << " (" << rocname << ")"
                << " dcol: " << decode3.dcol(i)
                << " pxl: " << decode3.pxl(i) << " pulseheight: " << decode3.pulseheight(i)
                << " col: " << decode3.column(i) << " row: " << decode3.row(i) << std::endl;
    }
  }
#endif

    if (ndata64 > 2000000)
        std::cout << "NDATA64 BIGGER THAN 163840 " << ndata64 << std::endl;

    //Put in putative rocid=0 protection
    for (size_t ij = 1; ij < ndata64 - 1; ij++) { //first and last words should be header and trailer
        if ((data[ij] >> 46) == 0x28000 || ((data[ij] >> 56) == 0x50 && (data[ij - 1] >> 46) == 0x28000))
            continue;
        if ((0x03e0000000000000 & data[ij]) == 0) {

            // print hit
            unsigned int w1 = data[ij] & 0xffffffff;
            unsigned int w2 = (data[ij] >> 32) & 0xffffffff;
            pos::PixelHit h1(w1);
            pos::PixelHit h2(w2);
            std::cout << "Clock " << ij << " = 0x" << std::hex << data[ij]
                      << " " << w1 << " " << w2 << " " << std::dec
                      << h1.getLink_id() << " " << h1.getROC_id() << " "
                      << h1.getDCol_id();
            if ((h1.getROC_id()) < 25)
                cout << "(" << h1.getColumnL1() << ")";
            cout << " " << h1.getPix_id();
            if ((h1.getROC_id()) < 25)
                cout << "(" << h1.getRowL1() << ")";
            cout << " " << h2.getLink_id() << " " << h2.getROC_id() << " " << h2.getDCol_id();
            if ((h2.getROC_id()) < 25)
                cout << "(" << h2.getColumnL1() << ")";
            cout << " " << h2.getPix_id();
            if ((h2.getROC_id()) < 25)
                cout << "(" << h2.getRowL1() << ")";
            cout << std::endl;

            std::cout << " Before 0x" << hex << data[ij] << " has rocid=0, forcing rocid=9: ";
            data[ij] = data[ij] | 0x0130000000000000;
            std::cout << " After 0x" << hex << data[ij] << dec << "\n";
        }
        if ((0x3e00000 & data[ij]) == 0) {

            // print hit
            unsigned int w1 = data[ij] & 0xffffffff;
            unsigned int w2 = (data[ij] >> 32) & 0xffffffff;
            pos::PixelHit h1(w1);
            pos::PixelHit h2(w2);
            std::cout << "Clock " << ij << " = 0x" << std::hex << data[ij]
                      << " " << w1 << " " << w2 << " " << std::dec
                      << h1.getLink_id() << " " << h1.getROC_id() << " "
                      << h1.getDCol_id();
            if ((h1.getROC_id()) < 25)
                cout << "(" << h1.getColumnL1() << ")";
            cout << " " << h1.getPix_id();
            if ((h1.getROC_id()) < 25)
                cout << "(" << h1.getRowL1() << ")";
            cout << " " << h2.getLink_id() << " " << h2.getROC_id() << " " << h2.getDCol_id();
            if ((h2.getROC_id()) < 25)
                cout << "(" << h2.getColumnL1() << ")";
            cout << " " << h2.getPix_id();
            if ((h2.getROC_id()) < 25)
                cout << "(" << h2.getRowL1() << ")";
            cout << std::endl;

            std::cout << " Before 0x" << hex << data[ij] << " has rocid=0, forcing rocid=9: ";
            data[ij] = data[ij] | 0x1300000;
            std::cout << " After 0x" << hex << data[ij] << dec << "\n";
        }
    }

    return int(ndata64);
}

bool PixelPh1FEDInterface::isNewEvent(uint32_t nTries) {
    cout << "\033[01;32m"
         << "PixelPh1FEDInterface::isNewEvent - FIXME: Empty method. IT NEEDS TO BE IMPLEMENTED." << endl
         << "\033[0m";
    return false;
}

int PixelPh1FEDInterface::enableSpyMemory(const int enable) {
    // card.modeRegister ?
    return setModeRegister(pixelFEDCard.modeRegister);
}

int PixelPh1FEDInterface::loadControlRegister() {
    if (getPrintlevel() & 1)
        cout << "FEDID:" << pixelFEDCard.fedNumber << " Load Control register from DB 0x" << hex << pixelFEDCard.Ccntrl << dec << endl;
    return setControlRegister(pixelFEDCard.Ccntrl);
}

int PixelPh1FEDInterface::setControlRegister(const int value) {
    if (getPrintlevel() & 1)
        cout << "FEDID:" << pixelFEDCard.fedNumber << " Set Control register " << hex << value << dec << endl;
    // write here
    pixelFEDCard.Ccntrl = value; // stored this value
    return false;
}

int PixelPh1FEDInterface::loadModeRegister() {
    if (getPrintlevel() & 1)
        cout << "FEDID:" << pixelFEDCard.fedNumber << " Load Mode register from DB 0x" << hex << pixelFEDCard.Ccntrl << dec << endl;
    return setModeRegister(pixelFEDCard.modeRegister);
}

int PixelPh1FEDInterface::setModeRegister(int value) {
    if (getPrintlevel() & 1)
        cout << "FEDID:" << pixelFEDCard.fedNumber << " Set Mode register " << hex << value << dec << endl;
    // write here
    pixelFEDCard.modeRegister = value; // stored this value
    return false;
}

////int PixelPh1FEDInterface::getModeRegister() {
    ////return 0;
////}

void PixelPh1FEDInterface::resetSlink() {
    uhal::setLogLevelTo(uhal::Warning());
    uhal::HwInterface &fed = regManager->getHwInterface();

    fed.getNode("pixfed_ctrl_regs.slink_core_gtx_reset").write(1);
    fed.dispatch();
    fed.getNode("pixfed_ctrl_regs.slink_core_gtx_reset").write(0);
    fed.dispatch();

    fed.getNode("pixfed_ctrl_regs.slink_core_sys_reset").write(1);
    fed.dispatch();
    fed.getNode("pixfed_ctrl_regs.slink_core_sys_reset").write(0);
    fed.dispatch();
}

// Routine to print FED masking registers 
void PixelPh1FEDInterface::checkAutoMaskRegisters(int mode) {
  cout<<"PixelPh1FEDInterface::checkAutoMaskRegisters"<<endl;
  // Try to read channel registers 
  uint32_t maskStatus1 = regManager->ReadReg("pixfed_stat_regs.MASK_STATUS_CH_32to01");
  uint64_t maskStatus = (uint64_t)maskStatus1; 
  uint32_t maskStatus2 = regManager->ReadReg("pixfed_stat_regs.MASK_STATUS_CH_48to33");
  maskStatus |= ((uint64_t)maskStatus2) << 32;
  cout<<"MASK-STATUS "<<bitset<48>(maskStatus)<<endl;
  
  uint64_t maskConfig = pixelFEDCard.cntrl_utca;
  cout<<"MASK-CONFIG "<<bitset<48>(maskConfig)<<endl;
  
  uint32_t maskTBM1 = regManager->ReadReg("pixfed_ctrl_regs.TBM_MASK_1");
  uint32_t maskTBM2 = regManager->ReadReg("pixfed_ctrl_regs.TBM_MASK_2");
  uint64_t maskTBM = ((uint64_t)maskTBM1) | ((uint64_t)maskTBM2<<32);
  cout<<"MASK-TBM    "<<bitset<48>(maskTBM)<<endl;
}



bool PixelPh1FEDInterface::checkFEDChannelSEU() {
    /*
    Check to see if the channels that are currently on match what we expect. If not
    increment the counter and return true. 
  */
    bool foundSEU = false;
    uint64_t enbable_current_i = 0;
    uint32_t mask_status_1, mask_status_2;
    enbable_current_i = mask_status_1 = regManager->ReadReg("pixfed_stat_regs.MASK_STATUS_CH_32to01");
    mask_status_2 = regManager->ReadReg("pixfed_stat_regs.MASK_STATUS_CH_48to33");
    enbable_current_i |= (uint64_t)mask_status_2 << 32;
    enbable_t enbable_current(enbable_current_i);
    if (0)
        std::cout << "FED: " << pixelFEDCard.fedNumber << " mask_status_1=0x"
                  << std::hex << mask_status_1 << " mask_status_2=0x" << mask_status_2 << std::dec << std::endl;

    // Note: since enbable_expected is bitset<48>, this only compares the first 48 bits

    if (enbable_current != enbable_expected && enbable_current != enbable_last) {
        foundSEU = true;
        cout << "Detected MASK change in FED " << pixelFEDCard.fedNumber << endl;
        cout << "Expected " << enbable_expected << endl
             << "Found    " << enbable_current << endl
             << "Last     " << enbable_last << endl;

	// This logic is (Last XOR Expected) AND Expected 
	if( ((enbable_last^enbable_expected)&enbable_last)!=0) {  // a new masked channel
	  cout<<"New masked channels"<<endl;
	  foundSEU = true; // eventually use only this, comment the to one
	}
	if( ((enbable_last^enbable_expected)&enbable_expected)!=0) //old masked channel was unmasked
	  cout<<"Masked channels recovered"<<endl;

        incrementSEUCountersFromEnbableBits(num_SEU, enbable_current, enbable_last);
    }

    enbable_last = enbable_current;

    return foundSEU;
}

std::bitset<48> PixelPh1FEDInterface::getFEDChannelSEU() {
    //return only those channels that are not expected to be 0 and set to 1
    //this way a channel that is expected to be off will not be reported
    bitset<48> enabled_by_det_config(pixelFEDCard.cntrl_utca);
    enabled_by_det_config.flip();
    return enabled_by_det_config & enbable_last;
}

void PixelPh1FEDInterface::incrementSEUCountersFromEnbableBits(vector<int> &counter, enbable_t current, enbable_t last) {
    cout << "Changed ch(s): ";
    for (size_t i = 0; i < current.size(); i++)
        if (current[i] != last[i]) {
            counter[i]++;
            cout << i + 1 << " ";
        }
    cout << endl;
}

bool PixelPh1FEDInterface::checkSEUCounters(int threshold) {
    /*
    Check to see if any of the channels have more than threshold SEUs.
    If so, return true and set expected enbable bit for that channel
    to off.
    Otherwise, return false
  */
    bool return_val = false;
    cout << "Checking for more than " << threshold << " SEUs in FED " << pixelFEDCard.fedNumber << endl;
    cout << "Channels with too many SEUs: ";
    for (size_t i = 0; i < 48; i++) {
        if (num_SEU[i] >= threshold) {
            enbable_expected[i] = 1; // 1 is off
            cout << " " << 1 + i << "(" << num_SEU[i] << ")";
            return_val = true;
        }
    }
    if (return_val) {
        cout << ". Disabling." << endl;
        cout << "Setting runDegraded flag for FED " << pixelFEDCard.fedNumber << endl;
        runDegraded_ = true;
    } else
        cout << endl;
    return return_val;
}

bool PixelPh1FEDInterface::updateMask(std::bitset<48> newmask) {
    /*
    Check to see if any of the channels have more than threshold SEUs.
    If so, return true and set expected enbable bit for that channel
    to off.
    Otherwise, return false
  */

    bool return_val = false;
    bitset<48> enabled_by_det_config(pixelFEDCard.cntrl_utca);
    enbable_expected = enabled_by_det_config | newmask;
#ifdef DO_TEST
    enbable_last = enbable_expected;  // clear the last status 
    std::cout << "updatemask - FED: " << pixelFEDCard.fedNumber 
	      << std::hex  
	      << "expected " << enbable_expected
	      << "last " << enbable_last
	      << std::dec << std::endl;
#endif //DO_TEST

    if(0) { // not used
    std::bitset<48> xorchannels = enbable_expected ^ enabled_by_det_config;
    if (xorchannels.count() > 9) {
        cout << "Setting runDegraded flag for FED " << pixelFEDCard.fedNumber << " because of the following channels:" << endl;
        cout << xorchannels.to_string() << std::endl;
        runDegraded_ = true;
    } else {
        runDegraded_ = false;
    }
    } // if(0)

    return return_val;
}

void PixelPh1FEDInterface::resetEnbableBits() {
    // called when fixing soft error
    uint64_t enbable_exp = 0;
    for (int i = 0; i < 48; ++i)
        if (enbable_expected[i])
            enbable_exp |= (uint64_t)1 << i;
    uint32_t tbm_mask_1 = (enbable_exp & 0xFFFFFFFF);
    uint32_t tbm_mask_2 = (enbable_exp >> 32) & 0xFFFF;
#ifdef DO_TEST
    enbable_last = enbable_expected;  // clear the last status 
#endif //DO_TEST
    if (1) {
        std::cout << "resetEnbableBits - FED: " << pixelFEDCard.fedNumber 
		  << std::hex << std::setw(12) << std::setfill('0') 
		  << " enbable=0x" << enbable_exp
                  << " tbm_mask_1=0x" << tbm_mask_1 
		  << " tbm_mask_2=0x" << tbm_mask_2 
		  << std::dec << std::setfill(' ') << std::endl;
	cout<< std::hex  
	    << "expected " << enbable_expected
	    << "last " << enbable_last
	    << std::dec << std::endl;
    }
    regManager->WriteReg("pixfed_ctrl_regs.SEU_recovery_valid", 1); //confirm order here
    regManager->WriteReg("pixfed_ctrl_regs.TBM_MASK_1", tbm_mask_1);
    regManager->WriteReg("pixfed_ctrl_regs.TBM_MASK_2", tbm_mask_2);
    // new mask is not applied until a TTC b-channel resync is received.

}

void PixelPh1FEDInterface::storeEnbableBits() {
    enbable_expected = pixelFEDCard.cntrl_utca;
    enbable_last = enbable_expected;
    if (1) {
        std::cout << "storeEnbableBits - FED: " << pixelFEDCard.fedNumber 
		  << std::hex 
		  << "expected " << enbable_expected
		  << "last " << enbable_last
		  << std::dec << std::endl;
    }
}

void PixelPh1FEDInterface::resetSEUCountAndDegradeState(void) {
    cout << "reset SEU counters and the runDegrade flag " << endl;
    // reset the state back to running
    runDegraded_ = false;
    // clear the count flag
    num_SEU.assign(48, 0);
    // reset the expected state to default
    storeEnbableBits();
}

void PixelPh1FEDInterface::testTTSbits(uint32_t data, int enable) {
    //will turn on the test bits indicate in bits 0...3 as long as bit 31 is set
    //As of this writing, the bits indicated are: 0(Warn), 1(OOS), 2(Busy), 4(Ready)
    //Use a 1 or any >1 to enable, a 0 or <0 to disable
    if (enable > 0)
        data = (data | 0x80000000) & 0x8000000f;
    else
        data = data & 0xf;
    // write here
    //kme should be able to do with FW>=10.0
}

int PixelPh1FEDInterface::readEventCounter() {
    //kme
    const uint32_t L1ACnt = regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt");
    return L1ACnt; //bad uint --> int
}

uint32_t PixelPh1FEDInterface::getFifoStatus() {
    //kme - can read status now
    const bool print = false;
    const bool printNotEmpty = false;
    const bool debug_stuck = false;
    const uint32_t mainFIFOfull = regManager->ReadReg("pixfed_stat_regs.L1A_counter.MAIN_FIFO_FULL");
    const uint32_t mainFIFOMT = regManager->ReadReg("pixfed_stat_regs.L1A_counter.MAIN_FIFO_EMPTY");
    const uint32_t mainFIFOMT2 = regManager->ReadReg("pixfed_stat_regs.main_fifo_empty"); // why are there duplicates?
    const uint32_t L1AFIFOfull1 = regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFIN_FULL");
    const uint32_t L1AFIFOMT1 = regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFIN_EMPTY");
    const uint32_t L1AFIFOfull2 = regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFIN_PROG_FULL"); //prog full means > threshold to go busy
    const uint32_t L1AFIFOMT2 = regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFIN_PROG_EMPTY");
    // "pixfed_stat_regs.all_tbm_fifo_empty"
    const uint32_t tbmFIFOfull31to0 = regManager->ReadReg("pixfed_stat_regs.TBM_FIFO_FULL_CH_31to0");
    const uint32_t tbmFIFOfull47to32 = regManager->ReadReg("pixfed_stat_regs.TBM_FIFO_FULL_CH_47to32");
    const uint32_t tbmFIFOmt31to0 = regManager->ReadReg("pixfed_stat_regs.TBM_FIFO_EMPTY_CH_31to0");
    const uint32_t tbmFIFOmt47to32 = regManager->ReadReg("pixfed_stat_regs.TBM_FIFO_EMPTY_CH_47to32");

    if (print || mainFIFOfull || L1AFIFOfull1 || L1AFIFOfull2) {
        std::cout << "FED ID " << pixelFEDCard.fedNumber << " FIFO status " << std::endl;
        std::cout << "| Main FIFO Full " << mainFIFOfull << " Empty " << mainFIFOMT << " " << mainFIFOMT2
                  << " | L1ACnt_BUFIN_FULL " << L1AFIFOfull1 << " Empty " << L1AFIFOMT1
                  << " | L1Acnt_BUFIN_PROG_FULL " << L1AFIFOfull2 << " Empty " << L1AFIFOMT2 << " |" << std::endl;
    }
    if (print || tbmFIFOfull31to0 || tbmFIFOfull47to32) {
        std::cout << "| TBMFIFO31to0 0x" << std::hex << std::setw(8) << std::setfill('0') << tbmFIFOfull31to0
                  << " | TBMFIFO47to32 0x" << std::hex << std::setw(8) << std::setfill('0') << tbmFIFOfull47to32
                  << std::setfill(' ') << std::dec << " | " << std::endl;
        std::cout << "TBMFIFOs full ch=";
        for (int ch = 0; ch < 32; ch++) {
            if ((tbmFIFOfull31to0 >> ch) & 0x1)
                std::cout << " " << ch + 1;
        }
        for (int ch = 32; ch < 48; ch++) {
            if ((tbmFIFOfull47to32 >> (ch - 32)) & 0x1)
                std::cout << " " << ch + 1;
        }
        std::cout << std::endl;
    }
    if (print) {
        std::cout << "TBMFIFOs empty ch=";
        for (int ch = 0; ch < 32; ch++) {
            if ((tbmFIFOmt31to0 >> ch) & 0x1)
                std::cout << " " << ch + 1;
        }
        for (int ch = 32; ch < 48; ch++) {
            if ((tbmFIFOmt47to32 >> (ch - 32)) & 0x1)
                std::cout << " " << ch + 1;
        }
        std::cout << std::endl;
    } else if (print || (printNotEmpty && (tbmFIFOmt31to0 != 0xFFFFFFFF || tbmFIFOmt47to32 != 0xFFFF))) {
        std::cout << "TBMFIFOs not empty ch=";
        for (int ch = 0; ch < 32; ch++) {
            if (((tbmFIFOmt31to0 >> ch) & 0x1) == 0)
                std::cout << " " << ch + 1;
        }
        for (int ch = 32; ch < 48; ch++) {
            if (((tbmFIFOmt47to32 >> (ch - 32)) & 0x1) == 0)
                std::cout << " " << ch + 1;
        }
        std::cout << std::endl;
    }
    // build up return bit-word to mimic legacy FED
    // FIFO 3 bit 9 or 8 : "pixfed_stat_regs.main_fifo_empty"?
    // FIFO 3 bit 9 or 8 : "pixfed_stat_regs.CntWord32Buf_FULL"  ??
    uint32_t status = (mainFIFOfull & 0x1) << 9;
    status = status | ((L1AFIFOfull1 & 0x1) | (L1AFIFOfull2 & 0x1)) << 8;

// FIFO 1 status (bits 0,2,4,6 ) Or'd by group of 9 channels (Phase 0)
// FIFO 2 status (bits 1,3,5,7) - phase 0 four FIFO2
#if (0)
    uint32_t g1full = 0;
    for (int i = 0; i < 12; i++) {
        g1full |= (tbmFIFOfull31to0 >> i) & 0x1;
    }
    uint32_t g2full = 0;
    for (int i = 12; i < 24; i++) {
        g2full |= (tbmFIFOfull31to0 >> i) & 0x1;
    }
    uint32_t g3full = 0;
    for (int i = 24; i < 32; i++) {
        g3full |= (tbmFIFOfull31to0 >> i) & 0x1;
    }
    for (int i = 32; i < 36; i++) {
        g3full |= (tbmFIFOfull47to32 >> (i - 32)) & 0x1;
    }
    uint32_t g4full = 0;
    for (int i = 36; i < 48; i++) {
        g4full |= (tbmFIFOfull47to32 >> (i - 32)) & 0x1;
    }
    uint32_t g5full = 0;
    //  for (int i = 36; i<45; i++) {g5full |= (tbmFIFOfull47to32 >> (i-32)) & 0x1;}
    uint32_t g6full = 0;
    //  for (int i = 45; i<48; i++) {g6full |= (tbmFIFOfull47to32 >> (i-32)) & 0x1;}

    status = status | (g4full << 7) | (g3full << 5) | (g2full << 3) | (g1full);
    status = status | (g6full << 4) | (g5full << 2);
#endif
    //Pilot repurpose bits 0-7 for eight modules
    status = status | ((tbmFIFOfull31to0 >> 12) & 0x1);         //ch13/14
    status = status | ((tbmFIFOfull31to0 >> 13) & 0x1);         //ch13/14
    status = status | (((tbmFIFOfull31to0 >> 14) & 0x1) << 1);  //ch15/16
    status = status | (((tbmFIFOfull31to0 >> 15) & 0x1) << 1);  //ch15/16
    status = status | (((tbmFIFOfull31to0 >> 18) & 0x1) << 2);  //ch 19/20
    status = status | (((tbmFIFOfull31to0 >> 19) & 0x1) << 2);  //ch 19/20
    status = status | (((tbmFIFOfull31to0 >> 20) & 0x1) << 3);  //ch 20/21
    status = status | (((tbmFIFOfull31to0 >> 21) & 0x1) << 3);  //ch 20/21
    status = status | (((tbmFIFOfull47to32 >> 5) & 0x1) << 4);  //ch 37/38
    status = status | (((tbmFIFOfull47to32 >> 6) & 0x1) << 4);  //ch 37/38
    status = status | (((tbmFIFOfull47to32 >> 7) & 0x1) << 5);  //ch 39/40
    status = status | (((tbmFIFOfull47to32 >> 8) & 0x1) << 5);  //ch 39/40
    status = status | (((tbmFIFOfull47to32 >> 11) & 0x1) << 6); //ch 43/44
    status = status | (((tbmFIFOfull47to32 >> 12) & 0x1) << 6); //ch 43/44
    status = status | (((tbmFIFOfull47to32 >> 13) & 0x1) << 7); //ch 43/44
    status = status | (((tbmFIFOfull47to32 >> 14) & 0x1) << 7); //ch 43/44

    //printTTSState();  //reduced logfile; now from workloop every 100 calls
    if (debug_stuck) {
        //KME debug block - if we are stuck in busy, dump registers & scope fifo
        const uint32_t tts = regManager->ReadReg("pixfed_stat_regs.tts.word");
        if (tts != 0x4)
            return status; // require busy
        const uint32_t tbm1 = regManager->ReadReg("pixfed_stat_regs.tbm_trailer_waiting_47to32");
        const uint32_t tbm2 = regManager->ReadReg("pixfed_stat_regs.tbm_trailer_waiting_31to0");
        const uint32_t tbm3 = regManager->ReadReg("pixfed_stat_regs.tbm_header_err_31to0");
        const uint32_t tbm4 = regManager->ReadReg("pixfed_stat_regs.tbm_header_err_47to32");
        if (tbm1 == 0 && tbm2 == 0 && tbm3 == 0 && tbm4 == 0)
            return status; // require FED state machine error
        diagnosticDump();
        std::cout << "Stuck in Busy Dump FED=" << pixelFEDCard.fedNumber << " chan=" << pixelFEDCard.TransScopeCh << std::endl;
        std::cout << "tbm_trailer_waiting 47 to 32 0x" << std::hex << tbm1 << std::dec << std::endl;
        std::cout << "tbm_trailer_waiting 31 to  0 0x" << std::hex << tbm2 << std::dec << std::endl;
        uint32_t data[1024];
        drainSpyFifo(data); // print happens elsewhere
    }                       // debug block end

    return status;
}

uint32_t PixelPh1FEDInterface::linkFullFlag() {
    //KME v10.0
    const uint32_t iphc = regManager->ReadReg("pixfed_stat_regs.slink_core_status.link_full"); // 1 = ready for data!
    if (iphc == 1) {
        return 0;
    } else {
        return 1;
    }
}

uint32_t PixelPh1FEDInterface::getErrorReport(int ch) {
    //cout << "\033[01;32m"
         //<< "PixelPh1FEDInterface::getErrorReport - FIXME: Empty method. IT NEEDS TO BE IMPLEMENTED." << endl
         //<< "\033[0m";
    return 0;
}

uint32_t PixelPh1FEDInterface::getTimeoutReport(int ch) {
    //cout << "\033[01;32m"
         //<< "PixelPh1FEDInterface::getTimeoutReport - FIXME: Empty method. IT NEEDS TO BE IMPLEMENTED." << endl
         //<< "\033[0m";
    return 0;
}

int PixelPh1FEDInterface::TTCRX_I2C_REG_READ(int Register_Nr) {
    if (Register_Nr == 22)
        return 0xe0; //kme mimic a good status register to shut up workloop
    return 0;
}

void PixelPh1FEDInterface::diagnosticDump() {
    std::cout << "----------------------------------------------------------------------" << std::endl;
    std::cout << "PixelPh1FEDInterface::diagnosticDump FEDId=" << pixelFEDCard.fedNumber << std::endl;
    std::cout << " trailer_timeout_valid (1?) " << regManager->ReadReg("pixfed_ctrl_regs.trailer_timeout_valid") << std::endl;
    std::cout << "  trailer_timeout_value (?) " << regManager->ReadReg("pixfed_ctrl_regs.trailer_timeout_value") << std::endl;
    std::cout << "  TBM_H_ID_check_valid (1?) " << regManager->ReadReg("pixfed_ctrl_regs.TBM_H_ID_check_valid") << std::endl;
    std::cout << "   timeout_check_valid (1?) " << regManager->ReadReg("pixfed_ctrl_regs.tts.timeout_check_valid") << std::endl;
    std::cout << " event_cnt_check_valid (1?) " << regManager->ReadReg("pixfed_ctrl_regs.tts.event_cnt_check_valid") << std::endl;

    std::cout << "               event number " << readEventCounter() << std::endl;
    std::cout << "               event number " << regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt32b") << std::endl;
    std::cout << "         L1ACnt_BUFIN_EMPTY 0x" << std::hex << std::setw(8) << std::setfill('0')
              << regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFIN_EMPTY") << std::endl;
    std::cout << "          L1ACnt_BUFIN_FULL 0x" << regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFIN_FULL") << std::endl;
    std::cout << "           L1ACnt_BUFIN_OVF 0x" << regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFIN_OVF") << std::endl;
    std::cout << "    L1ACnt_BUFIN_PROG_EMPTY 0x" << regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFIN_PROG_EMPTY") << std::endl;
    std::cout << "     L1ACnt_BUFIN_PROG_FULL 0x" << regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFIN_PROG_FULL") << std::endl;
    std::cout << "            MAIN_FIFO_EMPTY 0x" << regManager->ReadReg("pixfed_stat_regs.L1A_counter.MAIN_FIFO_EMPTY") << std::endl;
    std::cout << "             MAIN_FIFO_FULL 0x" << regManager->ReadReg("pixfed_stat_regs.L1A_counter.MAIN_FIFO_FULL") << std::endl;
    std::cout << "              MAIN_FIFO_OVF 0x" << regManager->ReadReg("pixfed_stat_regs.L1A_counter.MAIN_FIFO_OVF") << std::endl;
    std::cout << "        L1ACnt_BUFOUT_EMPTY 0x" << regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFOUT_EMPTY") << std::endl;
    std::cout << "         L1ACnt_BUFOUT_FULL 0x" << regManager->ReadReg("pixfed_stat_regs.L1A_counter.L1ACnt_BUFOUT_FULL") << std::endl;
    std::cout << "         CntWord32Buf_EMPTY 0x" << regManager->ReadReg("pixfed_stat_regs.CntWord32Buf_EMPTY") << std::endl;
    std::cout << "          CntWord32Buf_FULL 0x" << regManager->ReadReg("pixfed_stat_regs.CntWord32Buf_FULL") << std::endl;
    std::cout << "           CntWord32Buf_OVF 0x" << regManager->ReadReg("pixfed_stat_regs.CntWord32Buf_OVF") << std::endl;
    //
    std::cout << "            main_fifo_empty 0x" << regManager->ReadReg("pixfed_stat_regs.main_fifo_empty") << std::endl;
    std::cout << "         all_tbm_fifo_empty 0x" << regManager->ReadReg("pixfed_stat_regs.all_tbm_fifo_empty") << std::endl;

    std::cout << "    tbm_fifo_empty_ch_31to0 0x" << std::hex
              << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.TBM_FIFO_EMPTY_CH_31to0") << std::endl;
    std::cout << "   tbm_fifo_empty_ch_47to32 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.TBM_FIFO_EMPTY_CH_47to32") << std::endl;
    std::cout << "     tbm_fifo_full_ch_31to0 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.TBM_FIFO_FULL_CH_31to0") << std::endl;
    std::cout << "    tbm_fifo_full_ch_47to32 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.TBM_FIFO_FULL_CH_47to32") << std::endl;
    std::cout << "                 cnt_pixEvt " << std::dec << std::setfill(' ') << regManager->ReadReg("pixfed_stat_regs.cnt_pixEvt") << std::endl;
    std::cout << "             current_tbm_ch " << regManager->ReadReg("pixfed_stat_regs.current_tbm_ch") << std::endl;
    std::cout << "      tbm_header_err_31to0  0x" << std::hex
              << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.tbm_header_err_31to0") << std::endl;
    std::cout << "      tbm_header_err_47to32 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.tbm_header_err_47to32") << std::endl;
    std::cout << " tbm_trailer_waiting_31to0  0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.tbm_trailer_waiting_31to0") << std::endl;
    std::cout << " tbm_trailer_waiting_47to32 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.tbm_trailer_waiting_47to32") << std::endl;
    std::cout << "      tbm_fifo_rd_en_31to0  0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.tbm_fifo_rd_en_31to0") << std::endl;
    std::cout << "      tbm_fifo_rd_en_47to32 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.tbm_fifo_rd_en_47to32") << std::endl;
    std::cout << "        tbm_fifo_ovf_31to0  0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.tbm_fifo_ovf_31to0") << std::endl;
    std::cout << "        tbm_fifo_ovf_47to32 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.tbm_fifo_ovf_47to32") << std::endl;
    std::cout << "trailer_not_received_31to0  0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.trailer_not_received_31to0") << std::endl;
    std::cout << "trailer_not_received_47to32 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.trailer_not_received_47to32") << std::dec << std::endl;
    std::cout << "       cnt_word32from_start " << regManager->ReadReg("pixfed_stat_regs.cnt_word32from_start") << std::endl;

    std::cout << "       ddr3_init_calib_done 0x" << std::hex
              << std::setw(1) << regManager->ReadReg("pixfed_stat_regs.ddr3_init_calib_done") << std::endl;
    std::cout << "        ddr3_refCLK200_lock 0x" << std::setw(1) << regManager->ReadReg("pixfed_stat_regs.ddr3_refCLK200_lock") << std::endl;
    std::cout << "                  ddr0_full 0x" << std::setw(1) << regManager->ReadReg("pixfed_stat_regs.DDR0_full") << std::endl;
    std::cout << "                  ddr1_full 0x" << std::setw(1) << regManager->ReadReg("pixfed_stat_regs.DDR1_full") << std::endl;
    std::cout << "    main_fifo_ctrl_fsm_flag 0x" << std::setw(2) << regManager->ReadReg("pixfed_stat_regs.fsm_flags.main_fifo_ctrl_fsm_flag") << std::endl;
    std::cout << "                ddr0_active 0x" << std::setw(1) << regManager->ReadReg("pixfed_stat_regs.fsm_flags.ddr0_active") << std::endl;
    std::cout << "                ddr1_active 0x" << std::setw(1) << regManager->ReadReg("pixfed_stat_regs.fsm_flags.ddr1_active") << std::endl;
    std::cout << "        slink_ctrl_fsm_flag 0x" << std::setw(2) << regManager->ReadReg("pixfed_stat_regs.fsm_flags.slink_ctrl_fsm_flag") << std::endl;
    std::cout << "slink_core_status:" << std::endl;
    std::cout << "                data_63to32 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.slink_core_status.data_63to32") << std::endl;
    std::cout << "                 data_31to0 0x" << std::setw(8) << std::setfill('0') << regManager->ReadReg("pixfed_stat_regs.slink_core_status.data_31to0") << std::endl;
    std::cout << "                  sync_loss 0x" << std::setw(1) << regManager->ReadReg("pixfed_stat_regs.slink_core_status.sync_loss") << std::endl;
    std::cout << "                  link_down 0x" << std::setw(1) << regManager->ReadReg("pixfed_stat_regs.slink_core_status.link_down") << std::endl;
    std::cout << "                  link_full 0x" << std::setw(1) << regManager->ReadReg("pixfed_stat_regs.slink_core_status.link_full") << std::endl;
    std::cout << std::dec;
    std::cout << "----------------------------------------------------------------------" << std::endl;
    printTTSState();
}

void PixelPh1FEDInterface::resetOOSCounters(bool all) {
    //software reset of OOS_automask_cnt_tbm_ch_xx counters to restart automask countdown
    //N.B. does not recover channels already masked
    const bool print = false;
    uint32_t mask1 = 0xFFFFFFFF;
    uint32_t mask2 = 0xFFFF;
    if (!all) { //for bookkeeping don't reset channels already masked (bitwise = 1 in mask status)
        mask1 ^= (regManager->ReadReg("pixfed_stat_regs.MASK_STATUS_CH_32to01"));
        mask2 ^= (regManager->ReadReg("pixfed_stat_regs.MASK_STATUS_CH_48to33"));
    }
    if (print)
        std::cout << "reseting OOS_NB_ch32to01 with mask " << std::hex << mask1 << std::dec << endl;
    regManager->WriteReg("pixfed_ctrl_regs.sw_OOS_nb_reset_ch32to01", mask1);
    regManager->WriteReg("pixfed_ctrl_regs.sw_OOS_nb_reset_ch32to01", 0x0);
    if (print)
        std::cout << "reseting OOS_NB_ch48to33 with mask " << std::hex << mask2 << std::dec << endl;
    regManager->WriteReg("pixfed_ctrl_regs.sw_OOS_nb_reset_ch48to33", mask2);
    regManager->WriteReg("pixfed_ctrl_regs.sw_OOS_nb_reset_ch48to33", 0x0);
}
