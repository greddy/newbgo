#ifndef _pixel_exception_Exception_h_
#define _pixel_exception_Exception_h_

#include <string>

#include "xcept/Exception.h"

// Mimick XCEPT_DEFINE_EXCEPTION from xcept/Exception.h.
#define PIXEL_DEFINE_EXCEPTION(EXCEPTION_NAME)                                                                                                                                                                         \
    namespace pixel {                                                                                                                                                                                                  \
    namespace exception {                                                                                                                                                                                              \
    class EXCEPTION_NAME : public pixel::exception::Exception {                                                                                                                                                        \
      public:                                                                                                                                                                                                          \
        EXCEPTION_NAME(std::string name, std::string message, std::string module, int line, std::string function) : pixel::exception::Exception(name, message, module, line, function) {};                             \
        EXCEPTION_NAME(std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception &err) : pixel::exception::Exception(name, message, module, line, function, err) {}; \
    };                                                                                                                                                                                                                 \
    }                                                                                                                                                                                                                  \
    }

// And a little helper to save us some typing.
#define PIXEL_DEFINE_ALARM(ALARM_NAME) \
    PIXEL_DEFINE_EXCEPTION(ALARM_NAME)

namespace pixel {
namespace exception {

class Exception : public xcept::Exception {
  public:
    Exception(std::string name,
              std::string message,
              std::string module,
              int line,
              std::string function);
    Exception(std::string name,
              std::string message,
              std::string module,
              int line,
              std::string function,
              xcept::Exception &err);
};

} // namespace exception
} // namespace pixel

// The PIXEL exceptions.
PIXEL_DEFINE_EXCEPTION(BRILDAQProblem)
PIXEL_DEFINE_EXCEPTION(ConfigurationParseProblem)
PIXEL_DEFINE_EXCEPTION(ConfigurationProblem)
PIXEL_DEFINE_EXCEPTION(ConfigurationValidationProblem)
PIXEL_DEFINE_EXCEPTION(FSMTransitionProblem)
PIXEL_DEFINE_EXCEPTION(HardwareProblem)
PIXEL_DEFINE_EXCEPTION(HwLeaseRenewalProblem)
PIXEL_DEFINE_EXCEPTION(HTTPProblem)
PIXEL_DEFINE_EXCEPTION(I2CBusBusy)
PIXEL_DEFINE_EXCEPTION(I2CProblem)
PIXEL_DEFINE_EXCEPTION(NibbleDAQProblem)
PIXEL_DEFINE_EXCEPTION(MonitoringProblem)
PIXEL_DEFINE_EXCEPTION(RCMSNotificationError)
PIXEL_DEFINE_EXCEPTION(RuntimeProblem)
PIXEL_DEFINE_EXCEPTION(SOAPFormatProblem)
PIXEL_DEFINE_EXCEPTION(SOAPCommandProblem)
PIXEL_DEFINE_EXCEPTION(SoftwareProblem)
PIXEL_DEFINE_EXCEPTION(TTCClockProblem)
PIXEL_DEFINE_EXCEPTION(TTCOrbitProblem)
PIXEL_DEFINE_EXCEPTION(TTCStreamProblem)
PIXEL_DEFINE_EXCEPTION(ValueError)
PIXEL_DEFINE_EXCEPTION(XDataProblem)

// The PIXEL alarms.
PIXEL_DEFINE_ALARM(BRILDAQFailureAlarm)
PIXEL_DEFINE_ALARM(FSMDriverFailureAlarm)
PIXEL_DEFINE_ALARM(FreqMonFailureAlarm)
PIXEL_DEFINE_ALARM(MonitoringFailureAlarm)
PIXEL_DEFINE_ALARM(NibbleDAQFailureAlarm)
PIXEL_DEFINE_ALARM(PIDAQFailureAlarm)
PIXEL_DEFINE_ALARM(PhaseMonFailureAlarm)

#endif // _pixel_exception_Exception_h_
