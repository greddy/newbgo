#ifndef _pixel_exception_version_h_
#define _pixel_exception_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define PIXELEXCEPTION_VERSION_MAJOR 3
#define PIXELEXCEPTION_VERSION_MINOR 13
#define PIXELEXCEPTION_VERSION_PATCH 1

// If any previous versions available:
// #define PIXELEXCEPTION_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef PIXELEXCEPTION_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define PIXELEXCEPTION_VERSION_CODE PACKAGE_VERSION_CODE(PIXELEXCEPTION_VERSION_MAJOR, PIXELEXCEPTION_VERSION_MINOR, PIXELEXCEPTION_VERSION_PATCH)
#ifndef PIXELEXCEPTION_PREVIOUS_VERSIONS
#define PIXELEXCEPTION_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PIXELEXCEPTION_VERSION_MAJOR, PIXELEXCEPTION_VERSION_MINOR, PIXELEXCEPTION_VERSION_PATCH)
#else
#define PIXELEXCEPTION_FULL_VERSION_LIST PIXELEXCEPTION_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PIXELEXCEPTION_VERSION_MAJOR, PIXELEXCEPTION_VERSION_MINOR, PIXELEXCEPTION_VERSION_PATCH)
#endif

namespace pixelexception {

const std::string project = "pixel";
const std::string package = "pixelexception";
const std::string versions = PIXELEXCEPTION_FULL_VERSION_LIST;
const std::string description = "CMS PIXEL helper software.";
const std::string authors = "Pixel DAQ team";
const std::string summary = "CMS PIXEL helper software.";
const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/PixelOnlineSoftware";
config::PackageInfo getPackageInfo();
void checkPackageDependencies();
std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace pixelexception

#endif
