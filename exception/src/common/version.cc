#include "pixel/exception/version.h"

#include "config/version.h"
#include "xcept/version.h"

GETPACKAGEINFO(pixelexception)

void
pixelexception::checkPackageDependencies() {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(xcept);
}

std::set<std::string, std::less<std::string> >
pixelexception::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, xcept);

    return dependencies;
}
