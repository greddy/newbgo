#ifndef _pixel_hwlayertca_version_h_
#define _pixel_hwlayertca_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define PIXEL_HWLAYERTCA_VERSION_MAJOR 3
#define PIXEL_HWLAYERTCA_VERSION_MINOR 13
#define PIXEL_HWLAYERTCA_VERSION_PATCH 1

// If any previous versions available:
// #define PIXEL_HWLAYERTCA_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef PIXEL_HWLAYERTCA_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define PIXEL_HWLAYERTCA_VERSION_CODE PACKAGE_VERSION_CODE(PIXEL_HWLAYERTCA_VERSION_MAJOR,PIXEL_HWLAYERTCA_VERSION_MINOR,PIXEL_HWLAYERTCA_VERSION_PATCH)
#ifndef PIXEL_HWLAYERTCA_PREVIOUS_VERSIONS
#define PIXEL_HWLAYERTCA_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PIXEL_HWLAYERTCA_VERSION_MAJOR,PIXEL_HWLAYERTCA_VERSION_MINOR,PIXEL_HWLAYERTCA_VERSION_PATCH)
#else
#define PIXEL_HWLAYERTCA_FULL_VERSION_LIST PIXEL_HWLAYERTCA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PIXEL_HWLAYERTCA_VERSION_MAJOR,PIXEL_HWLAYERTCA_VERSION_MINOR,PIXEL_HWLAYERTCA_VERSION_PATCH)
#endif

namespace pixel {
  namespace hwlayertca {
    const std::string project = "pixel";
    const std::string package = "hwlayertca";
    const std::string versions = PIXEL_HWLAYERTCA_FULL_VERSION_LIST;
    const std::string description = "CMS PIXEL helper software.";
    const std::string authors = "Jeroen Hegeman";
    const std::string summary = "Part of the CMS PIXEL software.";
    const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies();
    std::set<std::string, std::less<std::string> > getPackageDependencies();
  }
}

#endif
