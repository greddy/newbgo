#ifndef _pixel_hwlayertca_HwDeviceTCA_h_
#define _pixel_hwlayertca_HwDeviceTCA_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

#include "pixel/hwlayer/IHwDevice.h"
#include "pixel/hwlayer/RegisterInfo.h"

namespace uhal {
  class HwInterface;
}

namespace pixel {
  namespace hwlayertca {

    /**
     * Hardware access class for (u)TCA devices.
     */
    class HwDeviceTCA : public pixel::hwlayer::IHwDevice
    {

    public:
      HwDeviceTCA();
      virtual ~HwDeviceTCA();

      void hwConnect(std::string const& connectionsFileName,
                     std::string const& connectionName);
      
      void hwConnect(std::string const &connectionName,
                   std::string const &connectionURI,
                   std::string const &addressTableFn);
                   
      void hwRelease();

      bool isHwConnected() const;

      uint32_t readRegister(std::string const& regName) const;
      void writeRegister(std::string const& regName,
                         uint32_t const regVal) const;
      std::vector<uint32_t> readBlock(std::string const& regName,
                                      uint32_t const nWords) const;
      std::vector<uint32_t> readBlock(std::string const& regName) const;
      std::vector<uint32_t> readBlockOffset(std::string const& regName,
                                            uint32_t const nWords,
                                            uint32_t const offset) const;
      void writeBlock(std::string const& regName,
                      std::vector<uint32_t> const regVal) const;

      uint32_t readModifyWriteRegister(std::string const& regName,
                                       uint32_t const regVal) const;

      uint32_t getBlockSize(std::string const& regName) const;

      std::vector<std::string> getRegisterNames() const;
      pixel::hwlayer::RegisterInfo::RegInfoVec getRegisterInfos() const;

      virtual pixel::hwlayer::RegDumpVec dumpRegisterContents() const;

    protected:
      uhal::HwInterface& getHwInterface() const;

    private:
      std::shared_ptr<uhal::HwInterface> hwP_;

    };

  } // namespace hwlayertca
} // namespace pixel

#endif // _pixel_hwlayertca_HwDeviceTCA_h_
