#ifndef _pixel_hwlayertca_TCADeviceBase_h_
#define _pixel_hwlayertca_TCADeviceBase_h_

#include <functional>
#include <memory>
#include <stdint.h>
#include <string>
#include <vector>

#include "pixel/hwlayer/ConfigurationProcessor.h"
#include "pixel/hwlayer/DeviceBase.h"
#include "pixel/hwlayer/IHwDevice.h"
#include "pixel/hwlayer/RegisterInfo.h"
#include "pixel/hwlayertca/TCACarrierBase.h"
#include "pixel/hwlayertca/HwDeviceTCA.h"

namespace pixel {
  namespace hwlayertca {

    /**
     * Abstract base class for all (u)TCA hardware devices.
     */
    class TCADeviceBase : public pixel::hwlayer::DeviceBase
    {

      friend class BootstrapHelper;

      //--------------------------------------------------

      struct RegNameMatchesA :
        public std::unary_function<std::string&, bool>
        {
        RegNameMatchesA(std::string const& prefix) :
          prefix_(prefix), prefixLen_(prefix.size()) {};
          bool operator() (std::string const& regName) const
          {
            return (regName.compare(0, prefixLen_, prefix_) == 0);
          }
        private:
          std::string const prefix_;
          size_t const prefixLen_;
        };

      struct RegNameMatchesB :
        public std::unary_function<pixel::hwlayer::RegisterInfo const&, bool>
        {
        RegNameMatchesB(std::string const& prefix) :
          prefix_(prefix), prefixLen_(prefix.size()) {};
          bool operator() (pixel::hwlayer::RegisterInfo const& regInfo) const
          {
            return (regInfo.name().compare(0, prefixLen_, prefix_) == 0);
          }
        private:
          std::string const prefix_;
          size_t const prefixLen_;
        };

      struct RegNameUnprefixer
        {
        RegNameUnprefixer(std::string const& prefix) :
          prefix_(prefix), prefixLen_(prefix.size()) {};
          std::string operator() (std::string const& regName) const
          {
            return regName.substr(prefixLen_);
          }
          pixel::hwlayer::RegisterInfo operator() (pixel::hwlayer::RegisterInfo const& regInfo) const
          {
            std::string const name = regInfo.name().substr(prefixLen_);
            pixel::hwlayer::RegisterInfo res(name,
                                            regInfo.isReadable(),
                                            regInfo.isWritable());
            return res;
          }
        private:
          std::string const prefix_;
          size_t const prefixLen_;
        };

      //--------------------------------------------------

    public:
      virtual ~TCADeviceBase();

      /**
       * Connect to the hardware. Does whatever is necessary to find
       * the hardware and setup the connection.
       */
      void hwConnect(std::string const& connectionsFileName,
                     std::string const& connectionName);

      /**
       * The inverse of hwConnect. Disconnects from the hardware,
       * releasing it for use by someone/something else.
       */
      void hwRelease();

      bool isHwConnected() const;

      void selectClockSource(pixel::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const;
      void resetPLL() const;
      void enableSFPs() const;
      void disableSFPs() const;
      void switchAllSFPs(bool const switchOn) const;

      std::string readEUI48() const;
      std::string readMACAddress() const;
      std::string readBoardId() const;
      std::string readSystemId() const;
      std::string readSystemFirmwareVersion() const;
      std::string readUserFirmwareVersion() const;
      std::string readSystemFirmwareDate() const;
      std::string readUserFirmwareDate() const;

      bool isTTCClockUp() const;
      bool isTTCClockStable() const;
      uint32_t readTTCClockUnlockCounter() const;

      // BUG BUG BUG
      // This is a little bit cheating, since this phase monitoring is
      // really only implemented in the FC7s, not in the CPM T1/T2.
      bool isPhaseMonLocked() const;
      void enablePhaseMonitoring() const;
      double readPhaseMonitoring40MHz() const;
      double readPhaseMonitoring160MHz() const;
      // BUG BUG BUG end

      /* using pixel::hwlayer::DeviceBase::readBlock; */
      /* std::vector<uint32_t> readBlock(std::string const& regName) const; */
      /* uint32_t getBlockSize(std::string const& regName) const; */

      std::string regNamePrefix() const;

    protected:
      /**
       * @note
       * Protected constructor since this is an abstract base class.
       * @note
       * The TCADeviceBase class takes ownership of the TCACarrierBase
       * pointer.
       */
      TCADeviceBase(std::unique_ptr<TCACarrierBase> carrier);

      virtual bool isReadyForUseImpl() const;

      uint32_t readModifyWriteRegister(std::string const& regName,
                                       uint32_t const regVal) const;

      bool bootstrapDone() const;
      virtual bool bootstrapDoneImpl() const;

      void runBootstrap() const;
      virtual void runBootstrapImpl() const;

      /**
       * Load the desired firmware image. Choices are 'golden' or
       * 'user'.
       */
      void loadFirmwareImage(std::string const& imageName) const;

      virtual void hwConnectImpl(std::string const& connectionsFileName,
                                 std::string const& connectionName);
      virtual void hwReleaseImpl();

      virtual std::vector<std::string> getRegisterNamesImpl() const;
      virtual pixel::hwlayer::RegisterInfo::RegInfoVec getRegisterInfosImpl() const;

      virtual uint32_t readRegisterImpl(std::string const& regName) const;
      virtual void writeRegisterImpl(std::string const& regName,
                                     uint32_t const regVal) const;
      virtual std::vector<uint32_t> readBlockImpl(std::string const& regName,
                                                  uint32_t const nWords) const;
      virtual std::vector<uint32_t> readBlockOffsetImpl(std::string const& regName,
                                                        uint32_t const nWords,
                                                        uint32_t const offset) const;
      virtual void writeBlockImpl(std::string const& regName,
                                  std::vector<uint32_t> const& regVals) const;

      virtual uint32_t readModifyWriteRegisterImpl(std::string const& regName,
                                                   uint32_t const regVal) const;

      virtual uint32_t getBlockSizeImpl(std::string const& regName) const;

      virtual void writeHardwareConfigurationImpl(pixel::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const;
      virtual pixel::hwlayer::DeviceBase::RegContentsVec readHardwareConfigurationImpl(pixel::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const;

      virtual pixel::hwlayer::RegDumpVec dumpRegisterContentsImpl() const;

      virtual std::string readEUI48Impl() const;
      virtual std::string readMACAddressImpl() const;
      virtual std::string readBoardIdImpl() const;
      virtual std::string readSystemIdImpl() const;
      virtual std::string readSystemFirmwareVersionImpl() const;
      virtual std::string readUserFirmwareVersionImpl() const;
      virtual std::string readSystemFirmwareDateImpl() const;
      virtual std::string readUserFirmwareDateImpl() const;
      virtual bool isTTCClockUpImpl() const;
      virtual bool isTTCClockStableImpl() const;
      virtual uint32_t readTTCClockUnlockCounterImpl() const;

      std::string regNamePrefixed(std::string const& regName) const;
      virtual std::string regNamePrefixImpl() const;

      /*
       * @note
       * The order of hwDevice_ and hwCarrierP_ is important: the
       * former is used when instantiating the latter.
       */
      HwDeviceTCA hwDevice_;
      std::unique_ptr<TCACarrierBase> hwCarrierP_;

    private:
      std::string readFirmwareDate(std::string const& regNamePrefix) const;
      std::string readFirmwareVersion(std::string const& regNamePrefix) const;

      // The measurement duration (i.e., number of iterations) of the
      // White-Rabbit phase measurement.
      static uint32_t const kMeasDuration;
      // The calibration factor (into s) of the White-Rabbit phase
      // measurement.
      static double const kCalFactor;

    };

  } // namespace hwlayertca
} // namespace pixel

#endif // _pixel_hwlayertca_TCADeviceBase_h_
