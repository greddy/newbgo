#ifndef _pixel_hwlayertca_FC7MMCPipe_h_
#define _pixel_hwlayertca_FC7MMCPipe_h_

#include <stdint.h>
#include <string>
#include <vector>

namespace pixel {
  namespace hwlayertca {

    class HwDeviceTCA;

    /**
     * Helper class implementing the communication pipe between the
     * FC7 MMC and the FPGA. Intended to be used in RAII fashion.
     * NOTE: Only the FPGA rebooting is implemented here, not the SD
     * card manipulations.
     */
    class FC7MMCPipe
    {

    public:
      FC7MMCPipe(HwDeviceTCA& device);
      ~FC7MMCPipe();

      void loadFirmwareImage(std::string const& fileName);

    private:
      // There are 31 characters available for file names etc. in the
      // MMC firmware test area.
      static uint32_t const kTextSpaceSize = 31;

      // The definitions of the various command codes understood by
      // the MMC.
      static uint32_t const kPipeCmdEnterSecureMode = 0x00000000;
      static uint32_t const kPipeCmdSetTextSpace = 0x00000001;
      static uint32_t const kPipeCmdSetDummySensor = 0x00000002;
      static uint32_t const kPipeCmdFileFromSD = 0x00000009;
      static uint32_t const kPipeCmdFileToSD = 0x00000010;
      static uint32_t const kPipeCmdRebootFPGA = 0x00000011;
      static uint32_t const kPipeCmdDeleteFromSD = 0x00000012;
      static uint32_t const kPipeCmdListFilesOnSD = 0x00000013;
      static uint32_t const kPipeCmdNuclearReset = 0x00000021;

      // Sleep time (in nanoseconds) for whenever we need to wait for
      // something.
      static uint32_t const kSleepTime = 1000000;

      HwDeviceTCA& hwDevice_;
      uint32_t dataAvailableFPGAToMMC_;
      uint32_t dataAvailableMMCToFPGA_;
      uint32_t spaceAvailableFPGAToMMC_;
      uint32_t spaceAvailableMMCToFPGA_;

      uint32_t fifoSize() const;
      void updateCounters();
      void send(uint32_t const command,
                std::vector<uint32_t> const& data=std::vector<uint32_t>());
      std::vector<uint32_t> receive();
      void enterSecureMode();
      void writeToTextSpace(std::string const& text);

      // Two little helper methods to convert between strings and
      // ASCII-encoded vectors of uint32_t.
      std::string rawToString(std::vector<uint32_t> const& raw) const;
      std::vector<uint32_t> stringToRaw(std::string const& str) const;

    };

  } // namespace hwlayertca
} // namespace pixel

#endif // _pixel_hwlayertca_FC7MMCPipe_h_
