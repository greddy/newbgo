#ifndef _pixel_hwlayertca_TCACarrierFC7_h_
#define _pixel_hwlayertca_TCACarrierFC7_h_

#include <map>
#include <ostream>
#include <stdint.h>
#include <string>
#include <vector>

#include "pixel/hwlayer/I2CAccessor.h"
#include "pixel/hwlayertca/TCACarrierBase.h"

namespace pixel {
  namespace hwlayertca {

    class HwDeviceTCA;

    /**
     * FMC carrier implementation for the FC7 board.
     */
    class TCACarrierFC7 : public TCACarrierBase
    {

    public:
      TCACarrierFC7(HwDeviceTCA& device);
      ~TCACarrierFC7();

    protected:
      virtual std::string registerNamePrefix() const;
      virtual std::vector<std::string> fmcNamesImpl() const;
      virtual void loadFirmwareImageImpl(std::string const& imageName) const;
      virtual void powerUpImpl() const;
      virtual void selectClockSourceImpl(pixel::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const;
      virtual void enableSFPsImpl() const;
      virtual void disableSFPsImpl() const;

      virtual std::string expectedCarrierType() const;

    private:

      enum VOLTAGE_VALUE {VOLTAGE_VALUE_3V3, VOLTAGE_VALUE_12V, VOLTAGE_VALUE_VADJ};
      friend std::ostream& operator<<(std::ostream& os,
                                      VOLTAGE_VALUE const& voltage);

      void powerUpFMC(std::string const& fmcName) const;
      void powerDownFMC(std::string const& fmcName) const;
      void assertFMCSupplyVoltagesAreCorrect() const;

      void resyncCDCE() const;

      pixel::hwlayer::I2CAccessor::I2CBus fmcBusSelect(std::string const& fmcName) const;
      void switchFMCSFPs(std::string const& fmcName, bool const switchOn) const;

      std::map<VOLTAGE_VALUE, double> readFMCSupplyVoltages(std::string const& fmcName) const;
      std::vector<int16_t> readLTC2990RawValues(uint32_t const i2cAddress) const;

    };

  } // namespace hwlayertca
} // namespace pixel

#endif // _pixel_hwlayertca_TCACarrierFC7_h_
