#ifndef _pixel_hwlayertca_Definitions_h_
#define _pixel_hwlayertca_Definitions_h_

#include <stdint.h>

namespace pixel {
  namespace definitions {

    // The measurement duration (i.e., number of iterations) of the
    // White-Rabbit phase measurement.
    uint32_t const kWRMeasDuration = 0x10;
    // The calibration factor (into s) of the White-Rabbit phase
    // measurement.
    double const kWRCalFactor = (1. / 538) * 1.e-9;

  } // namespace definitions
} // namespace tdcs

#endif // _pixel_hwlayertca_Definitions_h_
