#include "pixel/hwlayertca/TCADeviceBase.h"

#include <algorithm>
#include <cassert>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <utility>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "pixel/exception/Exception.h"
#include "pixel/hwlayer/Utils.h"

uint32_t const
pixel::hwlayertca::TCADeviceBase::kMeasDuration = 0x10;

double const
pixel::hwlayertca::TCADeviceBase::kCalFactor = (1. / 538) * 1.e-9;

pixel::hwlayertca::TCADeviceBase::TCADeviceBase(std::unique_ptr<TCACarrierBase> carrier) :
  hwCarrierP_(std::move(carrier))
{
}

pixel::hwlayertca::TCADeviceBase::~TCADeviceBase()
{
}

bool
pixel::hwlayertca::TCADeviceBase::isReadyForUseImpl() const
{
  return isHwConnected();
}

std::string
pixel::hwlayertca::TCADeviceBase::regNamePrefixed(std::string const& regName) const
{
  std::string regNameLocal(regName);
  std::string prefix = regNamePrefix();
  if (!prefix.empty())
    {
      regNameLocal = prefix + regName;
    }
  return regNameLocal;
}

bool
pixel::hwlayertca::TCADeviceBase::bootstrapDone() const
{
  return bootstrapDoneImpl();
}

bool
pixel::hwlayertca::TCADeviceBase::bootstrapDoneImpl() const
{
  // The default is: no bootstrap needed. So return true.
  return true;
}

void
pixel::hwlayertca::TCADeviceBase::runBootstrap() const
{
  runBootstrapImpl();
}

void
pixel::hwlayertca::TCADeviceBase::runBootstrapImpl() const
{
  // The default is: no bootstrap needed. So nothing to do.
}

void
pixel::hwlayertca::TCADeviceBase::loadFirmwareImage(std::string const& imageName) const
{
  hwCarrierP_->loadFirmwareImage(imageName);
}

void
pixel::hwlayertca::TCADeviceBase::hwConnect(std::string const& connectionsFileName,
                                           std::string const& connectionName)
{
  hwConnectImpl(connectionsFileName, connectionName);
}

void
pixel::hwlayertca::TCADeviceBase::hwConnectImpl(std::string const& connectionsFileName,
                                               std::string const& connectionName)
{
  hwDevice_.hwConnect(connectionsFileName, connectionName);

  //----------

  // If we're still on the golden firmware image, jump to the user
  // firmware image.
  std::string systemId = readSystemId();
  std::transform(systemId.begin(), systemId.end(), systemId.begin(), ::tolower);
  if (systemId == "gold")
    {
      try
        {
          loadFirmwareImage("user");
        }
      catch (pixel::exception::Exception& err)
        {
          std::string const msg =
            toolbox::toString("Failed to jump to the 'user' firmware image: '%s'.",
                              err.what());
          XCEPT_RETHROW(pixel::exception::HardwareProblem, msg, err);
        }
      pixel::hwlayer::sleep(3);
      hwDevice_.hwRelease();
      hwDevice_.hwConnect(connectionsFileName, connectionName);
      std::string systemId = readSystemId();
      std::transform(systemId.begin(), systemId.end(), systemId.begin(), ::tolower);
      if (systemId == "gold")
        {
          std::string const msg = "Failed to jump to the 'user' firmware image."
            "Maybe the image is corrupted/absent?";
          XCEPT_RAISE(pixel::exception::HardwareProblem, msg);
        }
    }

  //----------

  // Now we are officially connected to the hardware. Next step is to
  // do some grunt work if no one else has done this yet for us.
  if (!bootstrapDone())
    {
      try
        {
          runBootstrap();
        }
      catch (pixel::exception::Exception& err)
        {
          std::string const msg =
            toolbox::toString("Failed to bootstrap the hardware device: '%s'.",
                              err.what());
          XCEPT_RETHROW(pixel::exception::HardwareProblem, msg, err);
        }
    }
}

void
pixel::hwlayertca::TCADeviceBase::hwRelease()
{
  hwReleaseImpl();
}

void
pixel::hwlayertca::TCADeviceBase::hwReleaseImpl()
{
  hwDevice_.hwRelease();
}

std::vector<std::string>
pixel::hwlayertca::TCADeviceBase::getRegisterNamesImpl() const
{
  std::vector<std::string> res = hwDevice_.getRegisterNames();
  res.erase(std::remove_if(res.begin(),
                           res.end(),
                           std::not1(RegNameMatchesA(regNamePrefix()))),
            res.end());
  // And now remove the prefixes on whatever is left.
  std::transform(res.begin(),
                 res.end(),
                 res.begin(),
                 RegNameUnprefixer(regNamePrefix()));
  return res;
}

pixel::hwlayer::RegisterInfo::RegInfoVec
pixel::hwlayertca::TCADeviceBase::getRegisterInfosImpl() const
{
  pixel::hwlayer::RegisterInfo::RegInfoVec res = hwDevice_.getRegisterInfos();
  // Filter out everything that does not belong to us.
  res.erase(std::remove_if(res.begin(),
                           res.end(),
                           std::not1(RegNameMatchesB(regNamePrefix()))),
            res.end());
  // And now remove the prefixes on whatever is left.
  std::transform(res.begin(),
                 res.end(),
                 res.begin(),
                 RegNameUnprefixer(regNamePrefix()));
  return res;
}

uint32_t
pixel::hwlayertca::TCADeviceBase::readRegisterImpl(std::string const& regName) const
{
  return hwDevice_.readRegister(regNamePrefixed(regName));
}

void
pixel::hwlayertca::TCADeviceBase::writeRegisterImpl(std::string const& regName,
                                                   uint32_t const regVal) const
{
  hwDevice_.writeRegister(regNamePrefixed(regName), regVal);
}

std::vector<uint32_t>
pixel::hwlayertca::TCADeviceBase::readBlockImpl(std::string const& regName,
                                               uint32_t const nWords) const
{
  return hwDevice_.readBlock(regNamePrefixed(regName), nWords);
}

std::vector<uint32_t>
pixel::hwlayertca::TCADeviceBase::readBlockOffsetImpl(std::string const& regName,
                                                     uint32_t const nWords,
                                                     uint32_t const offset) const
{
  return hwDevice_.readBlockOffset(regNamePrefixed(regName), nWords, offset);
}

// std::vector<uint32_t>
// pixel::hwlayertca::TCADeviceBase::readBlock(std::string const& regName) const
// {
//   return readBlock(regName, getBlockSize(regName));
// }

void
pixel::hwlayertca::TCADeviceBase::writeBlockImpl(std::string const& regName,
                                                std::vector<uint32_t> const& regVals) const
{
  hwDevice_.writeBlock(regNamePrefixed(regName), regVals);
}

uint32_t
pixel::hwlayertca::TCADeviceBase::readModifyWriteRegister(std::string const& regName,
                                                         uint32_t const regVal) const
{
  return readModifyWriteRegisterImpl(regName, regVal);
}

uint32_t
pixel::hwlayertca::TCADeviceBase::readModifyWriteRegisterImpl(std::string const& regName,
                                                             uint32_t const regVal) const
{
  return hwDevice_.readModifyWriteRegister(regNamePrefixed(regName), regVal);
}

uint32_t
pixel::hwlayertca::TCADeviceBase::getBlockSizeImpl(std::string const& regName) const
{
  return hwDevice_.getBlockSize(regNamePrefixed(regName));
}

void
pixel::hwlayertca::TCADeviceBase::writeHardwareConfigurationImpl(pixel::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const
{
  for (pixel::hwlayer::ConfigurationProcessor::RegValVec::const_iterator it = cfg.begin();
       it != cfg.end();
       ++it)
    {
      // NOTE: Some care is required here. For single-word writes the
      // appropriate mask (specified in the address table) is
      // applied. Block writes don't know about masks.
      if (it->second.size() == 1)
        {
          writeRegister(it->first, it->second.at(0));
        }
      else
        {
          writeBlock(it->first, it->second);
        }
    }
}

pixel::hwlayer::DeviceBase::RegContentsVec
pixel::hwlayertca::TCADeviceBase::readHardwareConfigurationImpl(pixel::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const
{
  pixel::hwlayer::DeviceBase::RegContentsVec res;
  for (pixel::hwlayer::RegisterInfo::RegInfoVec::const_iterator regInfo = regInfos.begin();
       regInfo != regInfos.end();
       ++regInfo)
    {
      std::vector<uint32_t> regVals;
      // NOTE: Some care is required here. For single-word reads the
      // appropriate mask (specified in the address table) is
      // applied. Block reads don't know about masks.
      uint32_t const size = getBlockSize(regInfo->name());
      if (size == 1)
        {
          regVals.push_back(readRegister(regInfo->name()));
        }
      else
        {
          regVals = readBlock(regInfo->name());
        }

      res.push_back(std::make_pair(*regInfo, regVals));
    }
  return res;
}

pixel::hwlayer::RegDumpVec
pixel::hwlayertca::TCADeviceBase::dumpRegisterContentsImpl() const
{
  return hwDevice_.dumpRegisterContents();
}

bool
pixel::hwlayertca::TCADeviceBase::isHwConnected() const
{
  return hwDevice_.isHwConnected();
}

void
pixel::hwlayertca::TCADeviceBase::selectClockSource(pixel::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const
{
  hwCarrierP_->selectClockSource(clkSrc);
}

void
pixel::hwlayertca::TCADeviceBase::resetPLL() const
{
  hwCarrierP_->resetPLL();
}

void
pixel::hwlayertca::TCADeviceBase::enableSFPs() const
{
  hwCarrierP_->enableSFPs();
}

void
pixel::hwlayertca::TCADeviceBase::disableSFPs() const
{
  hwCarrierP_->disableSFPs();
}

std::string
pixel::hwlayertca::TCADeviceBase::readEUI48() const
{
  return readEUI48Impl();
}

std::string
pixel::hwlayertca::TCADeviceBase::readEUI48Impl() const
{
  return hwCarrierP_->readEUI48();
}

std::string
pixel::hwlayertca::TCADeviceBase::readMACAddress() const
{
  return readMACAddressImpl();
}

std::string
pixel::hwlayertca::TCADeviceBase::readMACAddressImpl() const
{
  return hwCarrierP_->readMACAddress();
}

std::string
pixel::hwlayertca::TCADeviceBase::readBoardId() const
{
  return readBoardIdImpl();
}

std::string
pixel::hwlayertca::TCADeviceBase::readBoardIdImpl() const
{
  // The board ID consists of four characters encoded as a single
  // 32-bit word.
  uint32_t val = hwDevice_.readRegister("system.board_id");
  std::string res = pixel::hwlayer::uint32ToString(val);
  return res;
}

std::string
pixel::hwlayertca::TCADeviceBase::readSystemId() const
{
  return readSystemIdImpl();
}

std::string
pixel::hwlayertca::TCADeviceBase::readSystemIdImpl() const
{
  // The system ID consists of four characters encoded as a single
  // 32-bit word.
  uint32_t val = hwDevice_.readRegister("user.system_id");
  std::string res = pixel::hwlayer::uint32ToString(val);
  return res;
}

std::string
pixel::hwlayertca::TCADeviceBase::readSystemFirmwareVersion() const
{
  return readSystemFirmwareVersionImpl();
}

std::string
pixel::hwlayertca::TCADeviceBase::readSystemFirmwareVersionImpl() const
{
  return readFirmwareVersion("system");
}

std::string
pixel::hwlayertca::TCADeviceBase::readUserFirmwareVersion() const
{
  return readUserFirmwareVersionImpl();
}

std::string
pixel::hwlayertca::TCADeviceBase::readUserFirmwareVersionImpl() const
{
  return readFirmwareVersion("user");
}

std::string
pixel::hwlayertca::TCADeviceBase::readSystemFirmwareDate() const
{
  return readSystemFirmwareDateImpl();
}

std::string
pixel::hwlayertca::TCADeviceBase::readSystemFirmwareDateImpl() const
{
  return readFirmwareDate("system");
}

std::string
pixel::hwlayertca::TCADeviceBase::readUserFirmwareDate() const
{
  return readUserFirmwareDateImpl();
}

std::string
pixel::hwlayertca::TCADeviceBase::readUserFirmwareDateImpl() const
{
  return readFirmwareDate("user");
}

bool
pixel::hwlayertca::TCADeviceBase::isTTCClockUp() const
{
  return isTTCClockUpImpl();
}

bool
pixel::hwlayertca::TCADeviceBase::isTTCClockUpImpl() const
{
  uint32_t const tmp = hwDevice_.readRegister("user.ttc_clock_mon.ttc_clock_up");
  return (tmp != 0x0);
}

bool
pixel::hwlayertca::TCADeviceBase::isTTCClockStable() const
{
  return isTTCClockStableImpl();
}

bool
pixel::hwlayertca::TCADeviceBase::isTTCClockStableImpl() const
{
  uint32_t const tmp = hwDevice_.readRegister("user.ttc_clock_mon.ttc_clock_stable");
  return (tmp != 0x0);
}

uint32_t
pixel::hwlayertca::TCADeviceBase::readTTCClockUnlockCounter() const
{
  return readTTCClockUnlockCounterImpl();
}

uint32_t
pixel::hwlayertca::TCADeviceBase::readTTCClockUnlockCounterImpl() const
{
  return hwDevice_.readRegister("user.ttc_clock_mon.ttc_clock_loss_counter");
}

bool
pixel::hwlayertca::TCADeviceBase::isPhaseMonLocked() const
{
  // Check that both MMCMs used by the phase measurements are locked.
  uint32_t const tmp1 = hwDevice_.readRegister("user.ttc_phase_mon.meas_common.mmcm1_locked");
  uint32_t const tmp2 = hwDevice_.readRegister("user.ttc_phase_mon.meas_common.mmcm2_locked");
  bool const isMMCM1Locked = (tmp1 == 0x1);
  bool const isMMCM2Locked = (tmp2 == 0x1);
  return (isMMCM1Locked && isMMCM2Locked);
}

void
pixel::hwlayertca::TCADeviceBase::enablePhaseMonitoring() const
{
  // Configure the measurement duration.
  // NOTE: Don't put this too high. The monitoring will expect new
  // numbers at least once per second.
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_duration", kMeasDuration);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_duration", kMeasDuration);

  // Enable the measurement.
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_enable", 0x1);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_enable", 0x1);

  // Reset the measurement for good measure (e.g., in case it was
  // already enabled with a different duration before).
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_reset", 0x0);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_reset", 0x1);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_40.measurement_reset", 0x0);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_reset", 0x0);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_reset", 0x1);
  hwDevice_.writeRegister("user.ttc_phase_mon.meas_160.measurement_reset", 0x0);
}

double
pixel::hwlayertca::TCADeviceBase::readPhaseMonitoring40MHz() const
{
  // NOTE: Return value in s.
  uint32_t const regVal = hwDevice_.readRegister("user.ttc_phase_mon.meas_40.measurement_value");
  double const res = kCalFactor * regVal  / kMeasDuration;
  return res;
}

double
pixel::hwlayertca::TCADeviceBase::readPhaseMonitoring160MHz() const
{
  // NOTE: Return value in s.
  uint32_t const regVal = hwDevice_.readRegister("user.ttc_phase_mon.meas_160.measurement_value");
  double const res = kCalFactor * regVal  / kMeasDuration;
  return res;
}

std::string
pixel::hwlayertca::TCADeviceBase::readFirmwareVersion(std::string const& regNamePrefix) const
{
  // This returns the system/user firmware version number.
  std::stringstream res;
  std::stringstream regName;
  regName << regNamePrefix << ".firmware_id.ver_major";
  uint32_t versionMajor = hwDevice_.readRegister(regName.str());
  regName.str("");
  regName << regNamePrefix << ".firmware_id.ver_minor";
  uint32_t versionMinor = hwDevice_.readRegister(regName.str());
  regName.str("");
  regName << regNamePrefix << ".firmware_id.ver_build";
  uint32_t versionBuild = hwDevice_.readRegister(regName.str());
  res << versionMajor << "." << versionMinor << "." << versionBuild;
  return res.str();
}

std::string
pixel::hwlayertca::TCADeviceBase::readFirmwareDate(std::string const& regNamePrefix) const
{
  // This returns the system/user firmware build date.
  std::stringstream res;
  std::stringstream regName;
  regName << regNamePrefix << ".firmware_id.date_yy";
  uint32_t yy = hwDevice_.readRegister(regName.str());
  regName.str("");
  regName << regNamePrefix << ".firmware_id.date_mm";
  uint32_t mm = hwDevice_.readRegister(regName.str());
  regName.str("");
  regName << regNamePrefix << ".firmware_id.date_dd";
  uint32_t dd = hwDevice_.readRegister(regName.str());
  res << "20" << std::setfill('0') << std::setw(2) << yy
      << "-"
      << std::setw(2) << mm
      << "-"
      << std::setw(2) << dd;
  return res.str();
}

std::string
pixel::hwlayertca::TCADeviceBase::regNamePrefix() const
{
  // NOTE: The assumption is that prefixes include the trailing '.'.
  std::string prefix = regNamePrefixImpl();
  if (!prefix.empty())
    {
      if (*prefix.rbegin() != '.')
        {
          prefix.append(".");
        }
    }
  return prefix;
}

std::string
pixel::hwlayertca::TCADeviceBase::regNamePrefixImpl() const
{
  // The default is no prefix at all.
  return "";
}
