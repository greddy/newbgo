#include "pixel/hwlayertca/version.h"

#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"

#include "pixel/exception/version.h"
#include "pixel/hwlayer/version.h"

GETPACKAGEINFO(pixel::hwlayertca)

void
pixel::hwlayertca::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);

  //CHECKDEPENDENCY(pixel::exception);
  //CHECKDEPENDENCY(pixel::hwlayer);
}

std::set<std::string, std::less<std::string> >
pixel::hwlayertca::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);

  //ADDDEPENDENCY(dependencies, pixel::exception);
  //ADDDEPENDENCY(dependencies, pixel::hwlayer);

  return dependencies;
}
