#include "pixel/hwlayertca/BootstrapHelper.h"

#include <stdint.h>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "pixel/exception/Exception.h"
#include "pixel/hwlayer/Utils.h"
#include "pixel/hwlayertca/TCADeviceBase.h"

pixel::hwlayertca::BootstrapHelper::BootstrapHelper(pixel::hwlayertca::TCADeviceBase const& device) :
  device_(device)
{
}

pixel::hwlayertca::BootstrapHelper::~BootstrapHelper()
{
}

bool
pixel::hwlayertca::BootstrapHelper::bootstrapDone() const
{
  uint32_t const val = device_.hwDevice_.readRegister("system.accounting.bootstrap_done");
  bool const res = (val != 0x0);
  return res;
}

void
pixel::hwlayertca::BootstrapHelper::runBootstrap() const
{
  // The settle time in seconds in between the steps of the
  // bootstrapping procedure.
  unsigned int const settleTime = 1;

  if (claimBootstrap())
    {
      try
        {
          // Power-up the uTCA carrier.
          device_.hwCarrierP_->powerUp();
          pixel::hwlayer::sleep(settleTime);

          // Select the backplane clock (FCLKA).
          device_.selectClockSource(hwlayertca::TCACarrierBase::CLOCK_SOURCE_FCLKA);
          pixel::hwlayer::sleep(settleTime);

          // Enable all SFPs.
          device_.enableSFPs();
          pixel::hwlayer::sleep(settleTime);

          // Enable the I2C player that runs the SFP monitoring.
          // BUG BUG BUG
          // This should be done properly.
          device_.hwDevice_.writeRegister("user.i2c.settings.enable", 0x1);
          device_.hwDevice_.writeRegister("user.i2c_player_ctrl.enable", 0x1);
          // BUG BUG BUG end

          setBootstrapDone();
          unsetBootstrapInProgress();
        }
      catch (...)
        {
          unsetBootstrapDone();
          unsetBootstrapInProgress();
          throw;
        }
    }
  else
    {
      // Some other piece of software is already doing the
      // bootstrapping. Let's wait to see if this finishes.
      size_t const secondsPerIteration = 1;
      size_t const maxNumIterations = 20;
      size_t iteration = 0;
      bool bootstrapIsInProgress = bootstrapInProgress();
      while (bootstrapIsInProgress && (iteration < maxNumIterations))
        {
          pixel::hwlayer::sleep(secondsPerIteration);
          ++iteration;
          bootstrapIsInProgress = bootstrapInProgress();
        }
      if (!bootstrapInProgress())
        {
          // Check if bootstrap finished successfully or not.
          if (!bootstrapDone())
            {
              std::string const msg =
                toolbox::toString("Some process other than this one "
                                  "was trying to bootstrap the hardware. "
                                  "Now it has finished, but it seems to have failed.");
              XCEPT_RAISE(pixel::exception::RuntimeProblem, msg.c_str());
            }
        }
      else
        {
          std::string const msg =
            toolbox::toString("Some process other than this one "
                              "is trying to bootstrap the hardware. "
                              "Tried waiting but got bored.");
          XCEPT_RAISE(pixel::exception::RuntimeProblem, msg.c_str());
        }
    }
}

bool
pixel::hwlayertca::BootstrapHelper::setBootstrapDone() const
{
  uint32_t const val = device_.hwDevice_.readModifyWriteRegister("system.accounting.bootstrap_done", 0x1);
  bool const res = (val != 0x0);
  return res;
}

void
pixel::hwlayertca::BootstrapHelper::unsetBootstrapDone() const
{
  device_.hwDevice_.writeRegister("system.accounting.bootstrap_done", 0x0);
}

bool
pixel::hwlayertca::BootstrapHelper::bootstrapInProgress() const
{
  uint32_t const val = device_.hwDevice_.readRegister("system.accounting.bootstrap_in_progress");
  bool const res = (val != 0x0);
  return res;
}

bool
pixel::hwlayertca::BootstrapHelper::claimBootstrap() const
{
  uint32_t const val = device_.hwDevice_.readModifyWriteRegister("system.accounting.bootstrap_in_progress", 0x1);
  bool const nothingInProgress = (val == 0x0);
  bool const done = bootstrapDone();
  bool const res = nothingInProgress && !done;
  return res;
}

void
pixel::hwlayertca::BootstrapHelper::unsetBootstrapInProgress() const
{
  device_.hwDevice_.writeRegister("system.accounting.bootstrap_in_progress", 0x0);
}
