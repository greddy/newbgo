#include "pixel/hwlayer/RegisterInfo.h"

pixel::hwlayer::RegisterInfo::RegisterInfo(std::string const &name,
                                           bool const isReadable,
                                           bool const isWritable)
    : name_(name),
      isReadable_(isReadable),
      isWritable_(isWritable) {
}

pixel::hwlayer::RegisterInfo::~RegisterInfo() {
}

std::string
pixel::hwlayer::RegisterInfo::name() const {
    return name_;
}

bool
pixel::hwlayer::RegisterInfo::isReadable() const {
    return isReadable_;
}

bool
pixel::hwlayer::RegisterInfo::isWritable() const {
    return isWritable_;
}
