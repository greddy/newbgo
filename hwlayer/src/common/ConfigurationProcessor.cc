#include "pixel/hwlayer/ConfigurationProcessor.h"

pixel::hwlayer::ConfigurationProcessor::ConfigurationProcessor() {
}

pixel::hwlayer::ConfigurationProcessor::~ConfigurationProcessor() {
}

pixel::hwlayer::ConfigurationProcessor::RegValVec
pixel::hwlayer::ConfigurationProcessor::parse(std::string const &configurationString) const {
    return parseImpl(configurationString);
}

std::string
pixel::hwlayer::ConfigurationProcessor::compose(pixel::hwlayer::ConfigurationProcessor::RegValVec const &hwConfiguration) const {
    return composeImpl(hwConfiguration);
}
