#include "pixel/hwlayer/Utils.h"

#include <cassert>
#include <climits>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <list>
#include <sstream>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "pixel/exception/Exception.h"

void
pixel::hwlayer::splitString(std::string const &stringIn,
                            char const delim,
                            std::vector<std::string> &pieces) {
    pieces.clear();
    std::stringstream ss(stringIn);
    std::string item;
    while (std::getline(ss, item, delim)) {
        pieces.push_back(item);
    }
}

std::vector<std::string>
pixel::hwlayer::splitString(std::string const &stringIn, char const delim) {
    std::vector<std::string> pieces;
    splitString(stringIn, delim, pieces);
    return pieces;
}

uint32_t
pixel::hwlayer::hexStringToUInt32(std::string const &stringIn) {
    std::stringstream tmp;
    tmp << std::hex << stringIn;
    uint32_t res;
    tmp >> res;
    return res;
}

std::vector<uint32_t>
pixel::hwlayer::hexStringToUInt32Vec(std::string const &stringIn, char const delim) {
    std::vector<std::string> tmp = splitString(stringIn, delim);
    std::vector<uint32_t> res;
    for (std::vector<std::string>::const_iterator it = tmp.begin();
         it != tmp.end();
         ++it) {
        if (it->size()) {
            res.push_back(hexStringToUInt32(*it));
        }
    }
    return res;
}

std::string
pixel::hwlayer::uint32ToString(uint32_t const val) {
    // ASSERT ASSERT ASSERT
    // This only works if chars are at least 8-bit variables.
    assert(CHAR_BIT >= 8);
    // ASSERT ASSERT ASSERT end

    std::stringstream res;
    res << char((val & uint32_t(0xff000000)) / 16777216);
    res << char((val & uint32_t(0x00ff0000)) / 65536);
    res << char((val & uint32_t(0x0000ff00)) / 256);
    res << char((val & uint32_t(0x000000ff)));
    return res.str();
}

std::string
pixel::hwlayer::trimString(std::string const &stringToTrim,
                           std::string const &whitespace) {
    size_t const strBegin = stringToTrim.find_first_not_of(whitespace);
    if (strBegin == std::string::npos) {
        // No content at all in this string.
        return "";
    }

    size_t const strEnd = stringToTrim.find_last_not_of(whitespace);
    size_t const strRange = strEnd - strBegin + 1;

    return stringToTrim.substr(strBegin, strRange);
}

int
pixel::hwlayer::nanosleep(long const nNanoseconds) {
    struct timespec ts;
    struct timespec td;
    ts.tv_sec = 0;
    ts.tv_nsec = nNanoseconds;
    int const res = nanosleep(&ts, &td);
    return res;
}

int
pixel::hwlayer::sleep(time_t const nSeconds) {
    struct timespec ts;
    struct timespec td;
    ts.tv_sec = nSeconds;
    ts.tv_nsec = 0;
    int const res = nanosleep(&ts, &td);
    return res;
}

int
pixel::hwlayer::randomSleep(time_t const nSeconds) {
    double const scale = 1.e9;
    double const nNanoseconds = scale * nSeconds;

    std::srand(time(0));
    double const rnd = nNanoseconds * (std::rand() / double(RAND_MAX));

    time_t const nSec = rnd / scale;
    long const nNanoSec = rnd - (nSec * scale);
    struct timespec tsReq;
    struct timespec tsRem;
    tsReq.tv_sec = nSec;
    tsReq.tv_nsec = nNanoSec;
    return nanosleep(&tsReq, &tsRem);
}

pixel::hwlayer::fedEnableMask
pixel::hwlayer::parseFedEnableMask(std::string const &fedEnableMask) {
    // Instructions on how to interpret the fedEnableMask string can be
    // found in the RCMS documentation:
    // http://cmsdoc.cern.ch/cms/TRIDAS/RCMS/Docs/Manuals/manuals/level1FMFSM_1_10_0.pdf

    // We expect the FED enable mask string to be non-empty. Otherwise:
    // complain.
    if (fedEnableMask.empty()) {
        std::string const msg =
            "Failed to parse the FED enable-mask. Expected a non-empty string.";
        XCEPT_RAISE(pixel::exception::ConfigurationProblem, msg);
    }

    // We expect the FED enable mask string to end with '%'. Otherwise:
    // complain.
    if (!toolbox::endsWith(fedEnableMask, "%")) {
        std::string const msg =
            "Failed to parse the FED enable-mask. Expected a '%' at the end.";
        XCEPT_RAISE(pixel::exception::ConfigurationProblem, msg);
    }

    std::list<std::string> const pieces =
        toolbox::parseTokenList(fedEnableMask, "%");

    pixel::hwlayer::fedEnableMask res;
    for (std::list<std::string>::const_iterator piece = pieces.begin();
         piece != pieces.end();
         ++piece) {
        std::list<std::string> const tmp = toolbox::parseTokenList(*piece, "&");

        // The above splitting on '%' and '&' should have left us with
        // two pieces. If not, raise a stink.
        if (tmp.size() != 2) {
            std::string const msg =
                "Failed to parse the FED enable-mask. Something appears to be wrong with its structure.";
            XCEPT_RAISE(pixel::exception::ConfigurationProblem, msg);
        }

        uint16_t fedId;
        std::stringstream tmpFedId(toolbox::trim(tmp.front()));
        tmpFedId >> fedId;
        if (tmpFedId.fail() || !(tmpFedId >> std::ws).eof()) {
            std::string const msg =
                toolbox::toString("Failed to parse the FED enable-mask. "
                                  "Could not turn '%s' into a FED ID.",
                                  tmpFedId.str().c_str());
            XCEPT_RAISE(pixel::exception::ConfigurationProblem, msg);
        }

        uint16_t mask;
        std::stringstream tmpMask(toolbox::trim(tmp.back()));
        tmpMask >> mask;
        if (tmpMask.fail() || !(tmpMask >> std::ws).eof()) {
            std::string const msg =
                toolbox::toString("Failed to parse the FED enable-mask. "
                                  "Could not turn '%s' into a mask value.",
                                  tmpMask.str().c_str());
            XCEPT_RAISE(pixel::exception::ConfigurationProblem, msg);
        }

        res[fedId] = mask;
    }
    return res;
}
