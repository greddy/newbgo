#include "pixel/hwlayer/DeviceBase.h"

pixel::hwlayer::DeviceBase::DeviceBase() {
}

pixel::hwlayer::DeviceBase::~DeviceBase() {
}

bool
pixel::hwlayer::DeviceBase::isReadyForUse() const {
    return isReadyForUseImpl();
}

bool
pixel::hwlayer::DeviceBase::isReadyForUseImpl() const {
    // The default is to say no. This will force descendant classes to
    // handle this correctly.
    return false;
}

std::vector<std::string>
pixel::hwlayer::DeviceBase::getRegisterNames() const {
    return getRegisterNamesImpl();
}

pixel::hwlayer::RegisterInfo::RegInfoVec
pixel::hwlayer::DeviceBase::getRegisterInfos() const {
    return getRegisterInfosImpl();
}

uint32_t
pixel::hwlayer::DeviceBase::readRegister(std::string const &regName) const {
    return readRegisterImpl(regName);
}

void
pixel::hwlayer::DeviceBase::writeRegister(std::string const &regName, uint32_t const regVal) const {
    writeRegisterImpl(regName, regVal);
}

std::vector<uint32_t>
pixel::hwlayer::DeviceBase::readBlock(std::string const &regName,
                                      uint32_t const nWords) const { return readBlockImpl(regName, nWords);
}

std::vector<uint32_t>
pixel::hwlayer::DeviceBase::readBlock(std::string const &regName) const {
    return readBlock(regName, getBlockSize(regName));
}

std::vector<uint32_t>
pixel::hwlayer::DeviceBase::readBlockOffset(std::string const &regName,
                                            uint32_t const nWords,
                                            uint32_t const offset) const { return readBlockOffsetImpl(regName, nWords, offset);
}

void
pixel::hwlayer::DeviceBase::writeBlock(std::string const &regName,
                                       std::vector<uint32_t> const &regVals) const { writeBlockImpl(regName, regVals);
}

uint32_t
pixel::hwlayer::DeviceBase::getBlockSize(std::string const &regName) const {
    return getBlockSizeImpl(regName);
}

void
pixel::hwlayer::DeviceBase::writeHardwareConfiguration(pixel::hwlayer::ConfigurationProcessor::RegValVec const &cfg) const {
    writeHardwareConfigurationImpl(cfg);
}

pixel::hwlayer::DeviceBase::RegContentsVec
pixel::hwlayer::DeviceBase::readHardwareConfiguration(pixel::hwlayer::RegisterInfo::RegInfoVec const &regInfos) const {
    return readHardwareConfigurationImpl(regInfos);
}

pixel::hwlayer::RegDumpVec
pixel::hwlayer::DeviceBase::dumpRegisterContents() const {
    return dumpRegisterContentsImpl();
}
