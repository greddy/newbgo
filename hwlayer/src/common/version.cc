#include "pixel/hwlayer/version.h"

#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"

#include "pixel/exception/version.h"

GETPACKAGEINFO(pixelhwlayer)

void
pixelhwlayer::checkPackageDependencies() {
    CHECKDEPENDENCY(config);
    CHECKDEPENDENCY(toolbox);
    CHECKDEPENDENCY(xcept);

    CHECKDEPENDENCY(pixelexception);
}

std::set<std::string, std::less<std::string> >
pixelhwlayer::getPackageDependencies() {
    std::set<std::string, std::less<std::string> > dependencies;

    ADDDEPENDENCY(dependencies, config);
    ADDDEPENDENCY(dependencies, toolbox);
    ADDDEPENDENCY(dependencies, xcept);

    ADDDEPENDENCY(dependencies, pixelexception);

    return dependencies;
}
