#ifndef _pixel_hwlayer_Utils_h_
#define _pixel_hwlayer_Utils_h_

/** @file
 * This is just a bunch of little helper routines that don't fit in
 * their own little boxes.
 */

#include <stdint.h>
#include <string>
#include <time.h>
#include <vector>

#include "toolbox/TimeVal.h"

namespace pixel {
namespace hwlayer {

void splitString(std::string const &stringIn,
                 char const delim,
                 std::vector<std::string> &pieces);
std::vector<std::string> splitString(std::string const &stringIn,
                                     char const delim);

// Convert a hexadecimal number string (e.g, 0x8) into a uint32_t.
uint32_t hexStringToUInt32(std::string const &stringIn);
// Convert a hexadecimal number series string (e.g, '0x8, 0x7,
// 0x6') into a vector of uint32_t.
std::vector<uint32_t> hexStringToUInt32Vec(std::string const &stringIn,
                                           char const delim = ' ');

std::string uint32ToString(uint32_t const val);
std::string trimString(std::string const &stringToTrim,
                       std::string const &whitespace = " \t");

/**
     * Two convenience methods for short and long sleeps.
     */
int nanosleep(long const nNanoseconds);
int sleep(time_t const nSeconds);
int randomSleep(time_t const nSeconds);

// Helper method to parse a fedEnableMask string into an easy map.
typedef std::map<uint16_t, uint8_t> fedEnableMask;
fedEnableMask parseFedEnableMask(std::string const &fedEnableMask);

} // namespace hwlayer
} // namespace tdcs

#endif // _pixel_hwlayer_Utils_h_
