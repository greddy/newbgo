#ifndef _pixel_hwlayer_ConfigurationProcessor_h_
#define _pixel_hwlayer_ConfigurationProcessor_h_

#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

namespace pixel {
namespace hwlayer {

/**
     * Abstract base class for all hardware configuration 'parsers'.
     */
class ConfigurationProcessor {

  public:
    typedef std::pair<std::string, std::vector<uint32_t> > RegValPair;
    typedef std::vector<RegValPair> RegValVec;

    virtual ~ConfigurationProcessor();

    RegValVec parse(std::string const &configurationString) const;
    std::string compose(RegValVec const &hwConfiguration) const;

  protected:
    ConfigurationProcessor();

    virtual RegValVec parseImpl(std::string const &configurationString) const = 0;
    virtual std::string composeImpl(RegValVec const &hwConfiguration) const = 0;
};

} // namespace hwlayer
} // namespace pixel

#endif // _pixel_hwlayer_ConfigurationProcessor_h_
