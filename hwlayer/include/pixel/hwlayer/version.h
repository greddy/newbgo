#ifndef _pixel_hwlayer_version_h_
#define _pixel_hwlayer_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define PIXELHWLAYER_VERSION_MAJOR 3
#define PIXELHWLAYER_VERSION_MINOR 13
#define PIXELHWLAYER_VERSION_PATCH 1

// If any previous versions available:
// #define PIXELHWLAYER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef PIXELHWLAYER_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define PIXELHWLAYER_VERSION_CODE PACKAGE_VERSION_CODE(PIXELHWLAYER_VERSION_MAJOR, PIXELHWLAYER_VERSION_MINOR, PIXELHWLAYER_VERSION_PATCH)
#ifndef PIXELHWLAYER_PREVIOUS_VERSIONS
#define PIXELHWLAYER_FULL_VERSION_LIST PACKAGE_VERSION_STRING(PIXELHWLAYER_VERSION_MAJOR, PIXELHWLAYER_VERSION_MINOR, PIXELHWLAYER_VERSION_PATCH)
#else
#define PIXELHWLAYER_FULL_VERSION_LIST PIXELHWLAYER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PIXELHWLAYER_VERSION_MAJOR, PIXELHWLAYER_VERSION_MINOR, PIXELHWLAYER_VERSION_PATCH)
#endif

namespace pixelhwlayer {

const std::string project = "pixel";
const std::string package = "pixelhwlayer";
const std::string versions = PIXELHWLAYER_FULL_VERSION_LIST;
const std::string description = "CMS PIXEL helper software.";
const std::string authors = "Pixel DAQ team";
const std::string summary = "Part of the CMS PIXEL software.";
const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/PixelOnlineSoftware";
config::PackageInfo getPackageInfo();
void checkPackageDependencies();
std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace pixelhwlayer

#endif
