#ifndef _pixel_hwlayer_DeviceBase_h_
#define _pixel_hwlayer_DeviceBase_h_

#include <stdint.h>
#include <string>
#include <utility>
#include <vector>

#include "pixel/hwlayer/ConfigurationProcessor.h"
#include "pixel/hwlayer/RegisterInfo.h"

namespace pixel {
namespace hwlayer {

typedef std::pair<std::string, std::vector<uint32_t> > RegDumpPair;
typedef std::vector<RegDumpPair> RegDumpVec;
/**
     * Abstract base class for all hardware devices.
     */
// TODO TODO TODO
// This class should probably be turned into an interface (instead
// of a base class).
// TODO TODO TODO end
class DeviceBase {

  public:
    typedef std::pair<pixel::hwlayer::RegisterInfo, std::vector<uint32_t> > RegContentsPair;
    typedef std::vector<RegContentsPair> RegContentsVec;

    virtual ~DeviceBase();

    bool isReadyForUse() const;

    /**
       * Some methods to figure out what (kind of) registers exist.
       */
    std::vector<std::string> getRegisterNames() const;
    pixel::hwlayer::RegisterInfo::RegInfoVec getRegisterInfos() const;

    uint32_t readRegister(std::string const &regName) const;
    void writeRegister(std::string const &regName,
                       uint32_t const regVal) const;

    std::vector<uint32_t> readBlock(std::string const &regName,
                                    uint32_t const nWords) const;
    std::vector<uint32_t> readBlock(std::string const &regName) const;
    std::vector<uint32_t> readBlockOffset(std::string const &regName,
                                          uint32_t const nWords,
                                          uint32_t const offset) const;
    void writeBlock(std::string const &regName,
                    std::vector<uint32_t> const &regVal) const;
    uint32_t getBlockSize(std::string const &regName) const;

    /**
       * A method to write the given configuration into the device.
       */
    void writeHardwareConfiguration(pixel::hwlayer::ConfigurationProcessor::RegValVec const &cfg) const;

    /**
       * A method to read back the current hardware configuration.
       */
    RegContentsVec readHardwareConfiguration(pixel::hwlayer::RegisterInfo::RegInfoVec const &regInfos) const;

    /**
       * A method to dump the full board register contents.
       * NOTE: This targets the whole board, not just the device
       * itself. E.g., all FC7-LPM registers, not just the ones for
       * the current iCI.
       */
    RegDumpVec dumpRegisterContents() const;

  protected:
    /**
       * @note
       * Protected constructor since this is an abstract base class.
       */
    DeviceBase();

    virtual bool isReadyForUseImpl() const = 0;

    virtual std::vector<std::string> getRegisterNamesImpl() const = 0;
    virtual pixel::hwlayer::RegisterInfo::RegInfoVec getRegisterInfosImpl() const = 0;

    virtual uint32_t readRegisterImpl(std::string const &regName) const = 0;
    virtual void writeRegisterImpl(std::string const &regName,
                                   uint32_t const regVal) const = 0;

    virtual std::vector<uint32_t> readBlockImpl(std::string const &regName,
                                                uint32_t const nWords) const = 0;
    virtual std::vector<uint32_t> readBlockOffsetImpl(std::string const &regName,
                                                      uint32_t const nWords,
                                                      uint32_t const offset) const = 0;
    virtual void writeBlockImpl(std::string const &regName,
                                std::vector<uint32_t> const &regVals) const = 0;

    virtual uint32_t getBlockSizeImpl(std::string const &regName) const = 0;

    virtual void writeHardwareConfigurationImpl(pixel::hwlayer::ConfigurationProcessor::RegValVec const &cfg) const = 0;
    virtual RegContentsVec readHardwareConfigurationImpl(pixel::hwlayer::RegisterInfo::RegInfoVec const &regInfos) const = 0;

    virtual RegDumpVec dumpRegisterContentsImpl() const = 0;
};

} // namespace hwlayer
} // namespace pixel

#endif // _pixel_hwlayer_DeviceBase_h_
